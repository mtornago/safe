#define EXTERN extern
#include "gdaq_SAFE.h"

void optimize_pedestals(void)
{
  UInt_t command=0;
  Int_t nsample_ped=1000;
  Int_t nsample_saved=daq.nsample;
  Int_t debug_DAQ=daq.debug_DAQ;
  Int_t debug_I2C=daq.debug_I2C;
  Int_t force_G1[N_CHANNEL], force_G10[N_CHANNEL];

  daq.debug_DAQ=0;
  daq.debug_I2C=0;
  Int_t trig_type_saved=daq.trigger_type;
  ValWord<uint32_t> address;
  update_VFE_control(0);

  Int_t max_ped=64;
  Double_t *ped_val_G10[N_CHANNEL];
  Double_t *ped_val_G1[N_CHANNEL];
  Int_t sat_G1[N_CHANNEL], sat_G10[N_CHANNEL];
  uhal::HwInterface hw=devices.front();

  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    ped_val_G1[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    ped_val_G10[ich]=(Double_t*)malloc(max_ped*sizeof(Double_t));
    for(Int_t i=0; i<max_ped; i++)
    {
      ped_val_G1[ich][i]=0.;
      ped_val_G10[ich][i]=0.;
    }
    sat_G1[ich]=-1;
    sat_G10[ich]=-1;
    force_G1[ich]=asic.DTU_force_G1[ich];
    force_G10[ich]=asic.DTU_force_G10[ich];
  }

// Set trigger type to "pedestal"
  daq.trigger_type=0;

  for(int G10=0; G10<=1; G10++)
  {
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      if(G10==0)
      {
        asic.DTU_force_G10[ich]=0;
        asic.DTU_force_G1[ich]=1;
      }
      else
      {
        asic.DTU_force_G10[ich]=1;
        asic.DTU_force_G1[ich]=0;
      }
      update_LiTEDTU_reg(ich,1,3);
    }

    for(Int_t loc_ped=0; loc_ped<max_ped; loc_ped++)
    {
      for(int i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i]-1;
        asic.CATIA_ped_G10[ich]=loc_ped;
        asic.CATIA_ped_G1[ich]=loc_ped;
        update_CATIA_reg(ich,3);
      }
      command = ((nsample_ped+1)<<16)+CAPTURE_STOP;
      hw.getNode("CAP_CTRL").write(command);
      hw.getNode("CAP_ADDRESS").write(0);
      command = ((nsample_ped+1)<<16)+CAPTURE_START;
      hw.getNode("CAP_CTRL").write(command);
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      daq.old_read_address=address.value()>>16;

      // Read 1 pedestal event
      get_event(0,0);

      if(G10==1) printf("DAC %d : ", loc_ped);
      for(int i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i]-1;
        Double_t ped=0.;
        if(daq.DTU_test_mode==1)
        {
          if(G10==1)
          {
            for(int isample=0; isample<daq.all_sample[4]; isample++)
            {
              ped+=(double)(daq.event[4][isample]&0xfff);
            }
            ped/=daq.all_sample[4];
          }
          else
          {
            for(int isample=0; isample<daq.all_sample[5]; isample++)
            {
              ped+=(double)(daq.event[5][isample]&0xfff);
            }
            ped/=daq.all_sample[5];
          }
        }
        else
        {
          for(int isample=0; isample<daq.all_sample[ich]; isample++)
          {
            ped+=(double)(daq.event[ich][isample]&0xfff);
          }
          ped/=daq.all_sample[ich];
        }
        if(G10==0)
          ped_val_G1[ich][loc_ped]=ped;
        else
          ped_val_G10[ich][loc_ped]=ped;
        if(G10==1)printf(" %.3f/%.3f ",ped_val_G1[ich][loc_ped],ped_val_G10[ich][loc_ped]);
      }
      if(G10==1)printf("\n");
    }
  }

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    for(Int_t loc_ped=1; loc_ped<max_ped; loc_ped++)
    {
      if(fabs(ped_val_G1[ich][loc_ped]-ped_val_G1[ich][loc_ped-1])<.2 && sat_G1[ich]<0)
      {
        sat_G1[ich]=loc_ped-1;
        printf("ch %d, G1 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G1[ich][loc_ped-1]);
      }
      if(fabs(ped_val_G10[ich][loc_ped]-ped_val_G10[ich][loc_ped-1])<.2 && sat_G10[ich]<0)
      {
        sat_G10[ich]=loc_ped-1;
        printf("ch %d, G10 saturation at %d : %.3f\n",ich,loc_ped-1,ped_val_G10[ich][loc_ped-1]);
      }
    }
  }


  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(sat_G1[ich]<0)
    {
      sat_G1[ich]=max_ped-1;
      asic.CATIA_ped_G1[ich]=sat_G1[ich];
    }
    else
      asic.CATIA_ped_G1[ich]=sat_G1[ich]-1;
    if(sat_G10[ich]<0)
    {
      sat_G10[ich]=max_ped-1;
      asic.CATIA_ped_G10[ich]=sat_G10[ich];
    }
    else
      asic.CATIA_ped_G10[ich]=sat_G10[ich]-1;

    for(Int_t loc_ped=sat_G1[ich]; loc_ped>=0; loc_ped--)
    {
      if(ped_val_G1[ich][loc_ped]<30.)
      {
        asic.CATIA_ped_G1[ich]=loc_ped;
      }
    }
    if(asic.CATIA_ped_G1[ich]==max_ped)asic.CATIA_ped_G1[ich]--;
    for(Int_t loc_ped=sat_G10[ich]; loc_ped>=0; loc_ped--)
    {
      if(ped_val_G10[ich][loc_ped]<35.)
      {
        asic.CATIA_ped_G10[ich]=loc_ped;
      }
    }
  }
  printf("Final pedestal settings :\n");
  char widget_name[80];
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    printf("ich %d : G10= %d (0x%2x), G1=%d (0x%2x)\n",ich,asic.CATIA_ped_G10[ich],asic.CATIA_ped_G10[ich],asic.CATIA_ped_G1[ich],asic.CATIA_ped_G1[ich]);

    if(ich==daq.current_channel-1)
    {
      sprintf(widget_name,"1_CATIA_ped_G10");
      GtkWidget *widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      //gtk_spin_button_set_value((GtkSpinButton*)widget,0.);
      gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_ped_G10[ich]);
      sprintf(widget_name,"1_CATIA_ped_G1");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      //gtk_spin_button_set_value((GtkSpinButton*)widget,0.);
      gtk_spin_button_set_value((GtkSpinButton*)widget,(double)asic.CATIA_ped_G1[ich]);
    }
    update_CATIA_reg(ich,3);

// Restore DTU Reg1 content
    asic.DTU_force_G10[ich]=force_G10[ich];
    asic.DTU_force_G1[ich]=force_G1[ich];
    update_LiTEDTU_reg(ich,1,3);

// Reinitialize actual pedestal values :
    daq.ped_G10[ich]=-1.;
    daq.ped_G1[ich]=-1.;
  }
  usleep(500000);

// Get one event with final settings to display histograms if debug_draw selected
  Int_t draw=1;
  get_event(1,draw);

  daq.nsample=nsample_saved;
  daq.trigger_type=trig_type_saved;
  daq.debug_DAQ=debug_DAQ;
  daq.debug_I2C=debug_I2C;

// Reset CRC error conters after pedestal optimization :
  get_CRC_errors(1);
}
