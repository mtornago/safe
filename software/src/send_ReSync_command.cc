#define EXTERN extern
#include "gdaq_SAFE.h"
void send_ReSync_command(UInt_t command)
{
  if(daq.daq_initialized<0) return;
  uhal::HwInterface hw=devices.front();
// ReSync commands are sent to all VFEs in parallel :
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
// Send ReSync command according to user wishes :
  hw.getNode("DTU_RESYNC").write(command);
  hw.dispatch();
// Read baseline register :
//  for(int ich=20; ich<=22; ich++)
//  {
//    Int_t bus=asic.I2C_bus[ich];
//    Int_t num=asic.I2C_address[ich];
//    Int_t device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
//    Int_t reg=5; // G10 baseline
//    UInt_t iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G10[ich], 0, 2, daq.debug_I2C);
//    printf("ADC baseline for channel %d : %x %d\n",ich,iret,iret&0xff);
//  }
}
