#define EXTERN extern
#include "gdaq_SAFE.h"

void get_temp(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, command, iret;
  GtkWidget* widget;
  char widget_text[80], widget_name[80];;

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();

// Read FPGA temperature :
  Int_t XADC_Temp_FPGA=0;
  Int_t average=32;
  Double_t loc_temp=0.;
  ValWord<uint32_t> temp;
  Double_t ave_val=0.;
  for(int iave=0; iave<average; iave++)
  {
    command=DRP_WRb*0 | (XADC_Temp_FPGA<<16);
    hw.getNode("DRP_XADC").write(command);
    temp  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    double loc_val=double((temp.value()&0xffff)>>4)/4096.;
    ave_val+=loc_val;
  }
  loc_temp=(ave_val/average)*4096*0.123-273.;
  printf("FPGA temperature : %.2f deg\n", loc_temp);
  sprintf(widget_name,"2_FPGA_temp");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"FPGA %.1f C",loc_temp);
  gtk_label_set_text((GtkLabel*)widget,widget_text);

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich])continue;
    printf("active channel %d : CATIA %d\n",ich,asic.I2C_address[ich]);
  }
// Read CATIA temperature :
// First, Every body in High Z
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich])continue;
    Int_t num=asic.I2C_address[ich];
    Int_t bus=asic.I2C_bus[ich];
    device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+3;
    iret=I2C_RW(hw, device_number, 1, 0x02, 0, 3, daq.debug_I2C);
// Set CATIA outputs with requested levels with no Vref output
    if(asic.CATIA_version[ich]>=14)
    {
      if(daq.CATIA_Vcal_out)
        iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
      else
        iret=I2C_RW(hw, device_number, 5, 0x4000, 1, 3, daq.debug_I2C);
    }
  }
// Then present temperature voltage of each channel
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich])continue;
    asic.CATIA_Vref_out[ich]=0;
    asic.CATIA_temp_out[ich]=1;
    iret=update_CATIA_reg(ich,1);
    usleep(100000); // Let some time for calibration voltages to stabilize

    printf("Measure temp for CATIA ch %d, bus %d, add %d, XADC channel %d\n",ich,asic.I2C_bus[ich],asic.I2C_address[ich], asic.CATIA_XADC_channel[ich]);
// And read Temp line :
    ave_val=0.;
    for(int iave=0; iave<average; iave++)
    {
      command=DRP_WRb*0 | (daq.XADC_Temp[asic.CATIA_XADC_channel[ich]-1]<<16);
      hw.getNode("DRP_XADC").write(command);
      temp  = hw.getNode("DRP_XADC").read();
      hw.dispatch();
      double loc_val=double((temp.value()&0xffff)>>4)/4096.;
      ave_val+=loc_val;
    }
    ave_val/=average;
// Convert to degrees :
    loc_temp=(asic.CATIA_Temp_offset[ich]-ave_val/daq.Tsensor_divider)/0.0021;
    asic.CATIA_Temp_val[ich]=loc_temp;
    printf("CATIA %d temperature : %f %f\n",ich,ave_val,loc_temp);
    sprintf(widget_name,"1_CATIA_Temp");
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    if(ich==daq.current_channel-1)
    {
      sprintf(widget_text,"Temp : %4.1f C",loc_temp);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
    }
// And put back in high Z :
    asic.CATIA_temp_out[ich]=0;
    iret=update_CATIA_reg(ich,1);
  }
}
