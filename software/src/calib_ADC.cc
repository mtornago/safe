#define EXTERN extern
#include "gdaq_SAFE.h"
void calib_ADC(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t device_number, iret;
  ValWord<uint32_t> reg;

// Set which VFEs to calibrate :
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
  hw.dispatch();

// Put calibration voltages to ADC inputs :
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich])continue;
    Int_t bus=asic.I2C_bus[ich];
    Int_t num=asic.I2C_address[ich];
    if(num<=0 || bus<=0)continue;
    device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+3; // CATIA address
    iret=I2C_RW(hw, device_number, 5, 0x7000, 1, 3, daq.debug_I2C);
    printf("Set CATIA %d outputs (bus %d, ch %d) %d  with calibration levels : 0x%4.4x\n\n",num,bus,ich+1,device_number,iret&0xFFFF);
  }
  usleep(100000); // Let some time for calibration voltages to stabilize

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.DTU_status[ich] || asic.DTU_version[ich]==0) continue;
    Int_t bus=asic.I2C_bus[ich];
    Int_t num=asic.I2C_address[ich];
    device_number=bus*1000+(num<<daq.I2C_shift_dev_number); // ADC address
    if(asic.DTU_dither[ich])
    {   
      iret=I2C_RW(hw, device_number+0, 3, 0x01, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number+1, 3, 0x01, 0, 1, daq.debug_I2C);
    }   
    if(asic.DTU_nsample_calib_x4[ich])
    {   
      iret=I2C_RW(hw, device_number+0, 1, 0xfe, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number+1, 1, 0xfe, 0, 1, daq.debug_I2C);
    }   
    if(asic.DTU_global_test[ich])
    {
      iret=I2C_RW(hw, device_number+0, 0, 0x01, 0, 1, daq.debug_I2C);
      iret=I2C_RW(hw, device_number+1, 0, 0x01, 0, 1, daq.debug_I2C);
    }
  }

  send_ReSync_command((LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);
  usleep(1000);
  printf("Launch ADC calibration new !\n");
  send_ReSync_command((LiTEDTU_ADCL_calib<<4) | LiTEDTU_ADCH_calib);
  usleep(10000);

// Put back CATIA output voltages to what they were before calibration
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    UInt_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich])continue;
    update_CATIA_reg(ich,1);
    update_CATIA_reg(ich,3);
    update_CATIA_reg(ich,4);
    update_CATIA_reg(ich,5);
    update_CATIA_reg(ich,6);
  }
// And wait for voltage stabilization
  usleep(100000);
}
