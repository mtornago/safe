#define EXTERN extern
#include "gdaq_SAFE.h"

void take_run(void)
{
  uhal::HwInterface hw=devices.front();
  ValWord<uint32_t> orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  Double_t dv=asic.ADC_dv;
  bool debug_DAQ=daq.debug_DAQ;
  //daq.debug_DAQ=FALSE;
  Int_t TP_level=daq.TP_level;
  Int_t n_TP_step=daq.n_TP_step;
  Int_t loc_TP_step=daq.n_TP_step;
  if(daq.trigger_type!=1)loc_TP_step=1;
  char hname[132], widget_name[132], widget_text[132];
  GtkWidget *widget;
  time_t rawtime=time(NULL);
  struct tm *timeinfo=localtime(&rawtime);
  if(daq.trigger_type==0)
    sprintf(daq.last_fname,"data/ped_data_G%d_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",10-9*asic.DTU_force_G1[daq.channel_number[0]-1],
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==1)
    sprintf(daq.last_fname,"data/TP_data_G%d_%d_%d_%d_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",10-9*asic.DTU_force_G1[daq.channel_number[0]-1],
            daq.n_TP_step,daq.TP_step,daq.TP_level,
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==2)
    sprintf(daq.last_fname,"data/laser_data_G%d_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",10-9*asic.DTU_force_G1[daq.channel_number[0]-1],
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  else if(daq.trigger_type==3)
    sprintf(daq.last_fname,"data/cal_data_G%d_%d%2.2d%2.2d_%2.2d%2.2d%2.2d.root",10-9*asic.DTU_force_G1[daq.channel_number[0]-1],
            timeinfo->tm_year-100,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
  
  printf("Start run recording for %d events with %d samples and trigger type %d\n",daq.nevent, daq.nsample, daq.trigger_type);
  if(daq.trigger_type==1) printf("TP width %d, level %d, step %d, nstep %d\n",daq.TP_width, daq.TP_level, daq.TP_step, daq.n_TP_step);
  printf("Run file name : %s\n",daq.last_fname);

  if(daq.fd != NULL)
  {
    daq.fd->Close();
    daq.fd->~TFile();
  }
  daq.fd=new TFile(daq.last_fname,"recreate");

  if(daq.tdata==NULL)
  {
    daq.tdata=new TTree("tdata","tdata");
    daq.tdata->Branch("timestamp",&daq.timestamp,"timestamp/l");
    for(int ich=0; ich<N_CHANNEL; ich++)
    {
      char bname[80], btype[80];
      sprintf(bname,"ch%d",ich);
      sprintf(btype,"ch%d[%d]/S",ich,daq.nsample*5);
      daq.tdata->Branch(bname,daq.event[ich],btype);
    }
  }

  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    if(daq.hmean[ich]!=NULL)daq.hmean[ich]->~TH1D();
    if(daq.hrms[ich]!=NULL)daq.hrms[ich]->~TH1D();
    if(daq.hdensity[ich]!=NULL)daq.hdensity[ich]->~TH1D();
    sprintf(hname,"mean_ch%d",ich);
    daq.hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    daq.hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"code_density_ch%d",ich);
    daq.hdensity[ich]=new TH1D(hname,hname,4096,-0.5,4095.5);
  }

  if(daq.trigger_type==1)
  {
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      if(!asic.CATIA_DAC1_status[ich] && !asic.CATIA_DAC1_status[ich])  // At least one DAC active
      {
        asic.CATIA_DAC1_status[ich]=TRUE;
        asic.CATIA_DAC2_status[ich]=FALSE;
        update_channel();
      }
    }
  }
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    for(Int_t istep=0; istep<loc_TP_step; istep++)
    {
      if(daq.pshape[istep][ich]!=NULL)daq.pshape[istep][ich]->~TProfile();
      sprintf(hname,"ch_%d_step_%d_%d",ich,istep,daq.TP_level+istep*daq.TP_step);
      if(daq.MEM_mode)
        daq.pshape[istep][ich]=new TProfile(hname,hname,daq.nsample*2,0.,12.5*daq.nsample*2);
      else
        daq.pshape[istep][ich]=new TProfile(hname,hname,daq.nsample*5,0.,6.25*daq.nsample*5);
    }
  }

  for(Int_t istep=0; istep<loc_TP_step; istep++)
  {
    if(daq.trigger_type==1)
    {
      sprintf(widget_name,"3_TP_level");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",daq.TP_level);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      gtk_widget_activate(widget);
      sprintf(widget_name,"3_n_TP_steps");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",istep);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
    }
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
    daq.cur_evt=0;
    Int_t draw=1;
    while(daq.cur_evt<daq.nevent)
    {
      sprintf(widget_name,"3_nevent");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert (widget);
      sprintf(widget_text,"%d",daq.cur_evt);
      gtk_entry_set_text((GtkEntry*)widget,widget_text);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before starting DAQ
// Have we asked to abort the run ?
      if(!daq.daq_running)
      {
        printf("Abort button pressed 1 !\n");
        break;
      }

      get_event(0,draw);
      daq.tdata->Fill();
      for(Int_t ich=0; ich<N_CHANNEL; ich++)
      {
        Double_t mean=0., rms=0.; 
        for(Int_t is=0; is<daq.all_sample[ich]; is++)
        {
          mean+=daq.fevent[ich][is];
          rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
        }
        mean/=daq.all_sample[ich];
        rms/=daq.all_sample[ich];
        rms=sqrt(rms-mean*mean);
        Double_t rms_limit=1.;
        if(asic.DTU_force_G1[ich]==1)rms_limit=0.5;
        if(daq.trigger_type==1 && rms*dv<rms_limit && ich==0)
        {
          orbit_pos=hw.getNode("DEBUG_0").read();
          orbit_pos1=hw.getNode("DEBUG_1").read();
          orbit_pos2=hw.getNode("DEBUG_2").read();
          orbit_pos3=hw.getNode("DEBUG_3").read();
          hw.dispatch();
          //printf("Trigger sent at %d %d %d %d\n",orbit_pos.value(),orbit_pos1.value(),orbit_pos2.value(),orbit_pos3.value());
        }
        if(daq.trigger_type!=1 || rms*dv>rms_limit || istep==0)
        {
          //printf("ch %d, rms = %.3f\n",ich,rms);
          for(Int_t is=0; is<daq.all_sample[ich]; is++)
          {
            daq.hdensity[ich]->Fill((double)daq.event[ich][is]);
            if(daq.MEM_mode)
              daq.pshape[istep][ich]->Fill(12.5*is,daq.fevent[ich][is]);
            else
              daq.pshape[istep][ich]->Fill(6.25*is,daq.fevent[ich][is]);
          }
          daq.hmean[ich]->Fill(mean*dv);
          daq.hrms[ich]->Fill(rms*dv);
          if(draw==1 && asic.channel_status[ich] && asic.VFE_status[ich/5])printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
        }
      }
      if(draw==1)printf("\n");
      draw=0;
    }
    if(!daq.daq_running)
    {
      printf("Abort button pressed 2 !\n");
      break;
    }
    if(daq.trigger_type==1) daq.TP_level+=daq.TP_step;
  }

  if(daq.daq_running)
  {
    daq.c1->Write();
    if(daq.use_LMB)daq.c2->Write();
    daq.tdata->Write();
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<N_CHANNEL; ich++)
    {
      for(Int_t istep=0; istep<loc_TP_step; istep++)
      {
        daq.pshape[istep][ich]->Write();
        daq.pshape[istep][ich]->~TProfile();
      }
      daq.hmean[ich]->Write();
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->Write();
      daq.hrms[ich]->~TH1D();
      daq.hdensity[ich]->Write();
      daq.hdensity[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }
  else
  {
    daq.tdata->~TTree();
    daq.tdata=NULL;
    for(Int_t ich=0; ich<N_CHANNEL; ich++)
    {
      for(Int_t istep=0; istep<loc_TP_step; istep++)
      {
        daq.pshape[istep][ich]->~TProfile();
      }
      daq.hmean[ich]->~TH1D();
      daq.hrms[ich]->~TH1D();
      daq.hdensity[ich]->~TH1D();
    }
    daq.fd->Close();
    daq.fd->~TFile();
    daq.fd=NULL;
  }

  daq.debug_DAQ=debug_DAQ;
  daq.TP_level=TP_level;
  daq.n_TP_step=n_TP_step;
  sprintf(widget_name,"3_TP_level");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.TP_level);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"3_n_TP_steps");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.n_TP_step);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"3_nevent");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  sprintf(widget_text,"%d",daq.nevent);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  sprintf(widget_name,"4_take_run");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
  g_assert (widget);
  if(daq.daq_running)gtk_button_clicked((GtkButton*)widget);
  printf("End of run !\n");
}

void get_single_event(void)
{
  Double_t dv=asic.ADC_dv;

  daq.cur_evt=0;
  get_event(1,1);
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    Double_t mean=0., rms=0.; 
    for(Int_t is=0; is<daq.all_sample[ich]; is++)
    {
      mean+=daq.fevent[ich][is];
      rms+=daq.fevent[ich][is]*daq.fevent[ich][is];
    }
    mean/=daq.all_sample[ich];
    rms/=daq.all_sample[ich];
    rms=sqrt(rms-mean*mean);
    if(asic.channel_status[ich] && asic.VFE_status[ich/5])printf("%d : %.1f %.3f mV, ",ich,mean*dv,rms*dv);
  }
  printf("\n");
}

void get_event(Int_t debug_mode, Int_t draw)
{
  static int trigger_type_old=-1;
  int n_word;
  int n_transfer_max=308;
  int n_transfer;
  int n_last;
  int BC0_pos[N_CHANNEL]={0};
  daq.error      = 0;
  int command    = 0;
  Int_t loc_ch[N_CHANNEL]={ 5,10,15,20,25,
                           24,19,14, 9, 4,
                            3, 8,13,18,23,
                           22,17,12, 7, 2,
                            1, 6,11,16,21};
  Double_t dt=6.25;
  uhal::HwInterface hw=devices.front();

/*
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    Int_t num=asic.DTU_number[ich];
    UInt_t device_number=daq.I2C_LiTEDTU_type*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    printf("Device number : %d 0x%x\n",device_number,device_number);

    UInt_t iret=I2C_RW(hw, device_number, 0, 0, 0, 2, daq.debug_I2C);
    printf("ch %d, DTU test mode : %d, Reg0 content : 0x%2.2x\n",ich, daq.DTU_test_mode, iret&0xff);
  }
*/

// In debug mode, we reinit DAQ buffer at each event :
  if(debug_mode!=0 || daq.cur_evt==0)
  {
    printf("Take avent with %d samples\n",daq.nsample);
    command = ((daq.nsample+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
    hw.getNode("CAP_ADDRESS").write(0);
    command = ((daq.nsample+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
    hw.dispatch();
    daq.old_read_address=-1;
  }

  ValWord<uint32_t> ctrl,address,free_mem,orbit_pos,orbit_pos1,orbit_pos2,orbit_pos3;
  ctrl = hw.getNode("CAP_CTRL").read();
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
  hw.dispatch();
  if(daq.debug_DAQ)printf("Starting with address : %8.8x, free memory %8.8x, trigger_type : %d with frame lemgth : %d\n",
                           address.value(),free_mem.value(),daq.trigger_type,(ctrl.value()>>16));

  ValVector< uint32_t > block;

// For pedestal trigger, send a soft trigger to SAFE
// For TP, send a trigger pulse to CATIA through SAFE if DTU version <2.0
// For DTU version >= 2.0, send a RESYNC command to LiTE-DTU which will send trigger pulse to CATIA
  if(daq.trigger_type !=2 )
  {
    if(daq.trigger_type == 0)
      command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON + GENE_RESYNC*0 + GENE_TP*0 + GENE_TRIGGER*1;
    else if(daq.trigger_type == 1)
      command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON + GENE_RESYNC*0 + GENE_TP*1 + GENE_TRIGGER*0;
    else if(daq.trigger_type == 5)
      command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON + GENE_RESYNC*1 + GENE_TP*0 + GENE_TRIGGER*0;

    if(daq.debug_DAQ)printf("Send trigger with command : 0x%8.8x ",command);
// Read base address and send trigger
    hw.getNode("FW_VER").write(command);
    orbit_pos=hw.getNode("DEBUG_0").read();
    orbit_pos1=hw.getNode("DEBUG_1").read();
    orbit_pos2=hw.getNode("DEBUG_2").read();
    orbit_pos3=hw.getNode("DEBUG_3").read();
    hw.dispatch();
    if(daq.debug_DAQ)printf("at orbit position %d %d %d %d\n",orbit_pos.value(),orbit_pos1.value(),orbit_pos2.value(),orbit_pos3.value());
    //printf("at orbit position %d %d %d %d\n",orbit_pos.value(),orbit_pos.value()/4,orbit_pos1.value(),orbit_pos1.value()/4);
// Read new address and wait for DAQ completion
    int nretry=0, new_write_address=-1, delta_address=-1;
    do
    {  
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      new_write_address=address.value()>>16;
      nretry++;
      delta_address=new_write_address-daq.old_read_address;
      if(delta_address<0)delta_address+=NSAMPLE_MAX;
      if(daq.debug_DAQ) printf("ongoing R/W addresses    : old %d, new %d delta %d\n", daq.old_read_address, new_write_address,delta_address);
    }
    while(delta_address < daq.nsample+1 && nretry<100);
    if(nretry==100)
    {
      printf("Stop waiting for sample capture after %d retries\n",nretry);
      printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", daq.old_read_address, new_write_address, address.value());
      daq.error=1;
    }
  }
  else
  {
    int nretry=0;
    command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + GENE_AWG*1 + TP_MODE*0 + LED_ON*1 + GENE_TP*0 + GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
// Wait for external trigger to fill memory :
    do
    {
      free_mem = hw.getNode("CAP_FREE").read();
      address = hw.getNode("CAP_ADDRESS").read();
      hw.dispatch();
      if(daq.debug_DAQ)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
      usleep(100);
      //if(daq.wait_for_ever)
      //{
      //  usleep(1000);
      //  printf(".");
      //  fflush(stdout);
      //}
      nretry++;
    }
    while((free_mem.value()==NSAMPLE_MAX-1) && (nretry<100 || daq.wait_for_ever));
    //if(daq.wait_for_ever)printf("\n");
    if(nretry>=100 && !daq.wait_for_ever)
    {
      printf("Stop waiting for external trigger after %d retries\n",nretry);
      printf("R/W addresses    : add 0x%8.8x\n",address.value());
      daq.error=1;
    }
  }

  if(daq.debug_DAQ)printf("Event kept with address : %8.8x, free memory %d, trigger_type : %d\n",address.value(),free_mem.value(),daq.trigger_type);
// Keep reading address for next event
  daq.old_read_address=address.value()>>16;
  if(daq.old_read_address==NSAMPLE_MAX-1)daq.old_read_address=-1;

  daq.mem.clear();

// Read event samples from FPGA
  n_word=(daq.nsample+1)*(N_VFE*5+1); // timestamp*NVFE+nsample*(address+NVFE*5) 32 bits words per sample to get the 5*NVFE channels data
  n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1380 bytes
  n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  if(daq.debug_DAQ)printf("Will read %d words in %d transfer of %d + %d in last transfer\n",n_word,n_transfer,MAX_PAYLOAD/4,n_last);
  if(n_transfer > n_transfer_max)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : %d\n",NSAMPLE_MAX);
    printf("Error : ask for too many samples. Number of transfer %d greater than allowed max value %d\n",n_transfer, n_transfer_max);
    daq.error=1;
  }
  for(int itrans=0; itrans<n_transfer; itrans++)
  {
    block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
    hw.dispatch();
    for(int is=0; is<MAX_PAYLOAD/4; is++) daq.mem.push_back(block[is]);
  }
  block = hw.getNode ("CAP_DATA").readBlock(n_last);
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  hw.dispatch();
  if(daq.debug_DAQ)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
  for(int is=0; is<n_last; is++)daq.mem.push_back(block[is]);
  daq.mem.valid(true);

// First sample should have bits 159 downto 64 at 1 and timestamp in bits 63 downto 0
  if(daq.mem[3]!=0xffffffff || daq.mem[4]!=0xffffffff || daq.mem[5]!=0xffffffff)
  {
    printf("First samples not headers : %8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[5], daq.mem[4], daq.mem[3], daq.mem[2], daq.mem[1], daq.mem[0]);
    daq.error=1;
  }
  daq.timestamp=daq.mem[2];
  
  daq.timestamp=(daq.timestamp<<32)+daq.mem[1];
  
  if(daq.debug_DAQ)
  {
    printf("timestamp : %8.8x %8.8x %ld\n",daq.mem[2],daq.mem[1],daq.timestamp);
    Int_t i0=0;
    printf("Headers :\n");
    printf("addr : %8.8x\n",daq.mem[i0+0]);
    i0+=1;
    printf("  data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+4],daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=5;
    printf("  data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+4],daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=5;
    printf("  data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+4],daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=5;
    printf("  data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+4],daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=5;
    printf("  data : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+4],daq.mem[i0+3],daq.mem[i0+2],daq.mem[i0+1],daq.mem[i0+0]);
    i0+=5;
    for(Int_t is=0; is<20; is++)
    {
      printf("sample %d :\n",is);
      printf("addr : %8.8x\n",daq.mem[i0+0]);
      i0+=1;
      printf("  VFE1 : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4]);
      i0+=5;
      printf("  VFE2 : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4]);
      i0+=5;
      printf("  VFE3 : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4]);
      i0+=5;
      printf("  VFE4 : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4]);
      i0+=5;
      printf("  VFE5 : %8.8x %8.8x %8.8x %8.8x %8.8x\n",daq.mem[i0+0],daq.mem[i0+1],daq.mem[i0+2],daq.mem[i0+3],daq.mem[i0+4]);
      i0+=5;
    }
  }
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    daq.all_sample[ich]=0;
    for(int isample=0; isample<NSAMPLE_MAX*5; isample++)
    {
      daq.event[ich][isample]=-1;
      daq.gain[ich][isample]=asic.DTU_force_G1[0];
    }
  }
  Int_t nsample_ped[N_CHANNEL]={0};
  Double_t loc_ped_G10[N_CHANNEL]={0.};

  FILE *fd=NULL;
  if(daq.dump_data && daq.cur_evt==0) fd=fopen("data/dump.txt","w+");
  if(daq.dump_data && daq.cur_evt>0) fclose(fd);

  for(int isample=0; isample<daq.nsample; isample++)
  {
    Int_t j=(isample+1)*(5*N_VFE+1);
    unsigned int loc_mem[N_CHANNEL];
    for(int ich=0; ich<N_CHANNEL; ich++)
    {
      loc_mem[ich]=daq.mem[j+1+ich];
    }
    if(daq.dump_data && isample<100)
    {
      fprintf(fd,"%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",loc_mem[0],loc_mem[1],loc_mem[2],loc_mem[3],loc_mem[4],loc_mem[5]);
    }

// Data in test mode :
    if(daq.DTU_test_mode==1)
    {
      dt=12.5;
      for(Int_t ich=0; ich<N_CHANNEL; ich++)
      {
        daq.event[ich][daq.all_sample[ich]]  =(loc_mem[ich]>>0 )&0xfff;
        daq.event[ich][daq.all_sample[ich]+1]=(loc_mem[ich]>>16)&0xfff;
        daq.fevent[ich][daq.all_sample[ich]+0]=(double)daq.event[ich][daq.all_sample[ich]];
        daq.fevent[ich][daq.all_sample[ich]+1]=(double)daq.event[ich][daq.all_sample[ich]+1];
        daq.all_sample[ich]+=2;
      }
    }
    else
// Data in DTU mode :
    {
      dt=6.25;
      for(int ich=0; ich<N_CHANNEL; ich++)
      {
        if(daq.ped_G10[ich]<0. && nsample_ped[ich]==NSAMPLE_PED)
        {
          daq.ped_G10[ich]=loc_ped_G10[ich]/NSAMPLE_PED;
          printf("G10 pedestal for channel %d : %f\n",ich,daq.ped_G10[ich]);
        }
        UInt_t tmp_mem=loc_mem[ich];
        Int_t type=(tmp_mem>>30);
        if(type==3)
        {
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1) // Frame delimiter
          {
            //if(ich==1)printf("Ch %d, word %d, %8.8x, Frame delimiter %d : %d samples, CRC12= 0x%x\n",ich,isample,tmp_mem, tmp_mem&0xff, (tmp_mem>>20)&0xff, (tmp_mem>>8)&0xfff);
            printf("Ch %d, word %d, %8.8x, Frame delimiter %d : %d samples, CRC12= 0x%x\n",ich,isample,tmp_mem, tmp_mem&0xff, (tmp_mem>>20)&0xff, (tmp_mem>>8)&0xfff);
          }
          else if (sub_type==2) // idle pattern
          {
            if(ich==1)printf("Ch %d, word %d, Idle pattern %8.8x, Pos from BC0 : %d\n",ich,isample,tmp_mem, tmp_mem&0xffff);
          }
          continue;
        }
        Int_t sample_map=5;
        if(type==2)sample_map=(tmp_mem>>24)&0x3F;
        if(type==2 && sample_map>4)
        {
          if(daq.debug_DAQ)printf("Ch %d, Strange sample map : 0x%x, certainly loss of sync !\n",ich,sample_map);
          sample_map=4;
          daq.error|=(1<<(ich+8));
        }
// pedestal data
        if(type==1 || type==2)
        {
          for(Int_t i=0; i<sample_map; i++)
          {
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0x3F;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(!daq.use_LMB && daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
            daq.all_sample[ich]++;
            tmp_mem>>=6;
          }
        }
        else if(!daq.MEM_mode) // 160 MHz mode
  // signal data
        {
          Int_t signal_type=(tmp_mem>>26)&0xF;
          if(signal_type==0xA)
          {
            if(!daq.use_LMB)
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            else
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0x1FFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
// Take first G1 sample as pedestal :
            if(! daq.use_LMB && daq.ped_G1[ich]<0. && daq.gain[ich][daq.all_sample[ich]]==1)
            {
              daq.ped_G1[ich]=daq.fevent[ich][daq.all_sample[ich]];
              printf("G1 pedestal for channel %d : %f\n",ich,daq.ped_G1[ich]);
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
            daq.all_sample[ich]++;

            tmp_mem>>=13;
            if(!daq.use_LMB)
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            else
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0x1FFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];

// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
            daq.all_sample[ich]++;
          }
          else if(signal_type==0xB)
          {
            if(!daq.use_LMB)
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            else
              daq.event[ich][daq.all_sample[ich]]=tmp_mem&0x1FFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            //if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.event[ich][daq.all_sample[ich]]>0xfa0)daq.event[ich][daq.all_sample[ich]]=0;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
// Take first G1 sample as pedestal :
            if(!daq.use_LMB && daq.ped_G1[ich]<0. && daq.gain[ich][daq.all_sample[ich]]==1)
            {
              daq.ped_G1[ich]=daq.fevent[ich][daq.all_sample[ich]];
              printf("G1 pedestal for channel %d : %f\n",ich,daq.ped_G1[ich]);
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G1[ich]>0. && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G1[ich])*10.1+daq.ped_G10[ich];
            daq.all_sample[ich]++;
            tmp_mem>>=13;
            if((tmp_mem&0x1FFF) == 0x1E0F)
            {
              if(daq.debug_DAQ)printf("BC0 marker found for channel %d, word number %d, orbit length : %d\n",ich+1,isample,daq.all_sample[ich]-BC0_pos[ich]);
              BC0_pos[ich]=daq.all_sample[ich];
            }
          }
        }
        else if(daq.MEM_mode) // 80 MHz mode
        {
          dt=12.5;
          Int_t sub_type=(tmp_mem>>28)&0x3;
          if(sub_type==1 || sub_type==3) // 2 samples in word or BC0 in even sample
          {
            daq.event[ich][daq.all_sample[ich]]=tmp_mem&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>12)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G10[ich])*12.5+daq.ped_G10[ich];
            daq.all_sample[ich]++;
          }
          if(sub_type==1 || sub_type==2) // 2 samples in word or BC0 in odd sample
          {
            daq.event[ich][daq.all_sample[ich]]=(tmp_mem>>13)&0xFFF;
            if(asic.DTU_force_G1[ich]==0) daq.gain[ich][daq.all_sample[ich]]=(tmp_mem>>25)&0x1;
            daq.fevent[ich][daq.all_sample[ich]]=(double)daq.event[ich][daq.all_sample[ich]];
            if(daq.ped_G10[ich]<0. && nsample_ped[ich]<NSAMPLE_PED)
            {
              loc_ped_G10[ich]+=daq.fevent[ich][daq.all_sample[ich]];
              nsample_ped[ich]++;
            }
// And correct for gain if needed :
            if(daq.gain[ich][daq.all_sample[ich]]==1 && daq.corgain && daq.ped_G10[ich]>0.)
              daq.fevent[ich][daq.all_sample[ich]]=(daq.fevent[ich][daq.all_sample[ich]]-daq.ped_G10[ich])*12.5+daq.ped_G10[ich];
            daq.all_sample[ich]++;
          }
        }
      }
    }
  }
  if(daq.debug_DAQ)
  {
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      printf("Channel %d : %d samples\n",ich+1,daq.all_sample[ich]);
    }
  }

  if((daq.trigger_type !=trigger_type_old || draw==1) && daq.debug_draw )
  {
    Int_t ch_max=N_CHANNEL;
    for(int ich=0; ich<ch_max; ich++)
    {
      daq.tg[ich]->Set(0);
      daq.tg_g1[ich]->Set(0);
      Double_t ymax=0., ymin=4096.;
      Int_t nsample_G1=0, nsample_G10=0;
      for(int isample=0; isample<daq.all_sample[ich]; isample++)
      {
        if(daq.fevent[ich][isample]>ymax)ymax=daq.fevent[ich][isample];
        if(daq.fevent[ich][isample]<ymin)ymin=daq.fevent[ich][isample];
        if(daq.gain[ich][isample]==0)
          daq.tg[ich]->SetPoint(nsample_G10++,dt*isample,daq.fevent[ich][isample]);
        else
          daq.tg_g1[ich]->SetPoint(nsample_G1++,dt*isample,daq.fevent[ich][isample]);
      }
      daq.tg[ich]->SetMaximum(10.*(int(ymax/10.)+1));
      daq.tg[ich]->SetMinimum(10.*(int(ymin/10.)));
      daq.tg_g1[ich]->SetMaximum(10.*(int(ymax/10.)+1));
      daq.tg_g1[ich]->SetMinimum(10.*(int(ymin/10.)));
      if(daq.zoom_draw==1)
      {
        daq.tg[ich]->SetMaximum(50.);
        daq.tg[ich]->SetMinimum(0.);
        daq.tg_g1[ich]->SetMaximum(50.);
        daq.tg_g1[ich]->SetMinimum(0.);
      }
    }
    if(daq.use_LMB)
    {
      Double_t dt_LMB=dt/8.;
      for(Int_t iADC=0; iADC<N_LMB_ADC; iADC++)
      {
        Int_t nsample_LMB=0;
        daq.tg_LMB[iADC]->Set(0);
        Double_t ymax=0., ymin=0.;
        Int_t BC0_min=4096;
        Int_t BC0_cor[N_CHANNEL]={0};
// Look if we had received a BC0 on all the chanels.
        for(Int_t ich=iADC*8; ich<iADC*8+8; ich++)
        {
          if(BC0_pos[ich]<BC0_min)BC0_min=BC0_pos[ich];
        }
// If yes, apply correction on timing to align samples
        printf("BC0 pos : %d, Cor : ",BC0_min);
        for(Int_t ich=iADC*8; ich<iADC*8+8 && BC0_min>0; ich++)
        {
          BC0_cor[ich]=BC0_pos[ich]-BC0_min;
          printf("%d,",BC0_cor[ich]);
        }
        printf("\n");
        for(Int_t ich=iADC*8; ich<iADC*8+8 && BC0_min>0; ich++)
        {
          for(Int_t is=0; is<daq.all_sample[ich]-BC0_cor[ich]; is++)
          {
            daq.event[ich][is] = daq.event[ich][is+BC0_cor[ich]];
            daq.fevent[ich][is]= daq.fevent[ich][is+BC0_cor[ich]];
          }
          daq.all_sample[ich]-=BC0_cor[ich];
        }
        for(int isample=0; isample<NSAMPLE_MAX*5; isample++)
        {
          for(Int_t ich=iADC*8; ich<iADC*8+8; ich++)
          {
            if(isample<daq.all_sample[ich])
            {
              if(isample>0 && isample<daq.all_sample[ich] && 
              daq.event[ich][isample-1]!=0 && daq.event[ich][isample]==0 && daq.event[ich][isample+1]!=0)
                printf("BC0 for channel %d, sample %d\n",ich,isample);
              if(daq.fevent[ich][isample]>ymax)ymax=daq.fevent[ich][isample];
              if(daq.fevent[ich][isample]<ymin)ymin=daq.fevent[ich][isample];
              daq.tg_LMB[iADC]->SetPoint(nsample_LMB,dt_LMB*nsample_LMB,daq.fevent[ich][isample]);
              nsample_LMB++;
            }
          }
        }
        daq.c2->cd(iADC+1);
        if(daq.tg_LMB[iADC]->GetN()>0)
        {
          daq.tg_LMB[iADC]->Draw("alp");
          gPad->SetGridx();
          gPad->SetGridy();
          daq.tg_LMB[iADC]->GetXaxis()->SetTitle("time [ns]");
          daq.tg_LMB[iADC]->GetXaxis()->SetTitleSize(0.05);
          daq.tg_LMB[iADC]->GetXaxis()->SetTitleFont(62);
          daq.tg_LMB[iADC]->GetXaxis()->SetLabelSize(0.05);
          daq.tg_LMB[iADC]->GetXaxis()->SetLabelFont(62);
          daq.tg_LMB[iADC]->GetYaxis()->SetTitle("amplitude [lsb]");
          daq.tg_LMB[iADC]->GetYaxis()->SetTitleSize(0.05);
          daq.tg_LMB[iADC]->GetYaxis()->SetTitleOffset(0.9);
          daq.tg_LMB[iADC]->GetYaxis()->SetTitleFont(62);
          daq.tg_LMB[iADC]->GetYaxis()->SetLabelSize(0.05);
          daq.tg_LMB[iADC]->GetYaxis()->SetLabelFont(62);
          if(daq.fd!=NULL)daq.tg_LMB[iADC]->Write();
        }
        daq.c2->Update();
      }
    }

    for(int ich=0; ich<ch_max; ich++)
    {
      if(!asic.channel_status[ich] || !asic.VFE_status[ich/5]) continue;
      daq.c1->cd(loc_ch[ich]);
      //daq.tg[ich]->SetMaximum(100.);
      //daq.tg[ich]->SetMinimum(0.);
      Int_t first_draw=1;
      if(daq.tg[ich]->GetN()>0)
      {
        daq.tg[ich]->Draw("alp");
        gPad->SetGridx();
        gPad->SetGridy();
        daq.tg[ich]->GetXaxis()->SetTitle("time [ns]");
        daq.tg[ich]->GetXaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetXaxis()->SetTitleFont(62);
        daq.tg[ich]->GetXaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetXaxis()->SetLabelFont(62);
        daq.tg[ich]->GetYaxis()->SetTitle("amplitude [lsb]");
        daq.tg[ich]->GetYaxis()->SetTitleSize(0.05);
        daq.tg[ich]->GetYaxis()->SetTitleOffset(0.9);
        daq.tg[ich]->GetYaxis()->SetTitleFont(62);
        daq.tg[ich]->GetYaxis()->SetLabelSize(0.05);
        daq.tg[ich]->GetYaxis()->SetLabelFont(62);
        if(daq.fd!=NULL)daq.tg[ich]->Write();
        first_draw=0;
      }
      if(daq.tg_g1[ich]->GetN()>0)
      {
        if(first_draw==0)
          daq.tg_g1[ich]->Draw("lp");
        else
          daq.tg_g1[ich]->Draw("alp");
        if(daq.fd!=NULL)daq.tg_g1[ich]->Write();
      }
      daq.c1->Update();
    }
  }

  trigger_type_old=daq.trigger_type;
  daq.cur_evt++;
  return;
}
