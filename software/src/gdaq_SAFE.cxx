#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include "gdaq_SAFE.h"

void myCSS(void)
{
  GtkCssProvider *provider;
  GdkDisplay *display;
  GdkScreen *screen;

  provider = gtk_css_provider_new ();
  display = gdk_display_get_default ();
  screen = gdk_display_get_default_screen (display);
  gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  const gchar *myCssFile = "mystyle.css";
  GError *error = 0;

  gtk_css_provider_load_from_file(provider, g_file_new_for_path(myCssFile), &error);
  g_object_unref (provider);
}
void end_of_run()
{
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : End of study\n",tv.tv_sec,tv.tv_usec);

  if(daq.fd!=NULL)
  {
    if(daq.tdata!=NULL)daq.tdata->Write();
    if(daq.c1!=NULL)daq.c1->Write();
    if(daq.c2!=NULL)daq.c2->Write();
    if(daq.c3!=NULL)daq.c3->Write();
    daq.fd->Close();
  }
  gtk_main_quit();
  exit(1);
}

void intHandler(int)
{
  printf("Ctrl-C detected !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Interupt detected\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}

void abortHandler(int)
{
  printf("Abort !\n");
  gettimeofday(&tv,NULL);
  printf("%ld.%6.6ld : Run aborted\n",tv.tv_sec,tv.tv_usec);
  end_of_run();
}

int main(int argc, char *argv [])
{
  signal(SIGINT, intHandler);
  signal(SIGABRT, abortHandler);

// Look if we have a special request :
  daq.autorun=0;
  for(int k=1; k<argc; k++)
  {
    if(strcmp(argv[k],"-auto")==0)
    {
      if(k==argc-1)
      {
        printf("Please give an option for autorun\n");
        exit(-1);
      }
      sscanf( argv[++k], "%d", &daq.autorun);
      if(daq.autorun<0 || daq.autorun>1)
      {
        printf("autorun option not allowed : %d\n",daq.autorun);
        exit(-1);
      }
      continue;
    }
    else if(strcmp(argv[k],"-help")==0)
    {
      printf("Options : \n");
      printf("  -auto n : run program in automatic way\n");
      printf("            n=1 calibrate FEMs\n");
      exit(0);
    }
  }

  gtk_data.main_window = NULL;
  GtkWidget *widget=NULL;


  daq.tdata=NULL;
  daq.c1=NULL;
  daq.c2=NULL;
  daq.c3=NULL;
  daq.fd=NULL;
  //daq.fd=new TFile("data/gdaq/gdaq_data_tmp.root","recreate");
  char  object_name[80];
  gchar *filename = NULL;
  GError *error = NULL;
  daq.comment[0]=0;

  Int_t loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);

  /* Initialisation de la librairie Gtk. */
  gtk_disable_setlocale ();
  gtk_init(&argc, &argv);
  myCSS();

  /* Ouverture du fichier Glade de la fenêtre principale */
  gtk_data.builder = gtk_builder_new();

  /* Création du chemin complet pour accéder au fichier test.glade. */
  /* g_build_filename(); construit le chemin complet en fonction du système */
  /* d'exploitation. ( / pour Linux et \ pour Windows) */
  //filename =  g_build_filename ("test.glade", NULL);
  filename =  g_build_filename ("xml/gdaq_SAFE_menu.glade", NULL);

  /* Chargement du fichier test.glade. */
  gtk_builder_add_from_file (gtk_data.builder, filename, &error);
  g_free (filename);
  if (error)
  {
    gint code = error->code;
    g_printerr("%s\n", error->message);
    g_error_free (error);
    return code;
  }

/* Affectation des signaux de l'interface aux différents CallBacks. */
  gtk_builder_connect_signals (gtk_data.builder, NULL);

// Retreive pointers to all windows and set some basic properties
  gtk_data.main_window    = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "0_SAFE_window");
  g_assert (gtk_data.main_window);
  gtk_window_get_size(gtk_data.main_window,&gtk_data.window_width[0],&gtk_data.window_height[0]);
  printf("Main window size : %dx%d\n",gtk_data.window_width[0],gtk_data.window_height[0]);

  gtk_data.channel_window = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "1_channel_window");
  g_assert (gtk_data.channel_window);
  gtk_window_set_transient_for(gtk_data.channel_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.channel_window,FALSE);
  gtk_window_set_keep_above(gtk_data.channel_window,FALSE);
  gtk_window_get_size(gtk_data.channel_window,&gtk_data.window_width[1],&gtk_data.window_height[1]);
  printf("Channel window size : %dx%d\n",gtk_data.window_width[1],gtk_data.window_height[1]);

  gtk_data.DAQ_window     = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "2_DAQ_window");
  g_assert (gtk_data.DAQ_window);
  gtk_window_set_transient_for(gtk_data.DAQ_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.DAQ_window,FALSE);
  gtk_window_set_keep_above(gtk_data.DAQ_window,FALSE);
  gtk_window_get_size(gtk_data.DAQ_window,&gtk_data.window_width[2],&gtk_data.window_height[3]);
  printf("DAQ window size : %dx%d\n",gtk_data.window_width[2],gtk_data.window_height[2]);

  gtk_data.trigger_window = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "3_trigger_window");
  g_assert (gtk_data.trigger_window);
  gtk_window_set_transient_for(gtk_data.trigger_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.trigger_window,FALSE);
  gtk_window_set_keep_above(gtk_data.trigger_window,FALSE);
  gtk_window_get_size(gtk_data.trigger_window,&gtk_data.window_width[3],&gtk_data.window_height[3]);
  printf("Trigger window size : %dx%d\n",gtk_data.window_width[3],gtk_data.window_height[3]);

  gtk_data.actions_window = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "4_actions_window");
  g_assert (gtk_data.actions_window);
  gtk_window_set_transient_for(gtk_data.actions_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.actions_window,FALSE);
  gtk_window_set_keep_above(gtk_data.actions_window,FALSE);
  gtk_window_get_size(gtk_data.actions_window,&gtk_data.window_width[4],&gtk_data.window_height[4]);
  printf("Actions window size : %dx%d\n",gtk_data.window_width[4],gtk_data.window_height[4]);

  gtk_data.results_window = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "5_calibration_results_window");
  g_assert (gtk_data.results_window);
  gtk_window_set_transient_for(gtk_data.results_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.results_window,FALSE);
  gtk_window_set_keep_above(gtk_data.results_window,FALSE);
  gtk_window_get_size(gtk_data.results_window,&gtk_data.window_width[5],&gtk_data.window_height[5]);
  printf("Results window size : %dx%d\n",gtk_data.window_width[5],gtk_data.window_height[5]);

  gtk_data.map_window = (GtkWindow*)gtk_builder_get_object (gtk_data.builder, "0_MEM_numbering_window");
  g_assert (gtk_data.map_window);
  gtk_window_set_transient_for(gtk_data.results_window,gtk_data.main_window);
  gtk_window_set_modal(gtk_data.results_window,FALSE);
  gtk_window_set_keep_above(gtk_data.results_window,FALSE);
  gtk_window_get_size(gtk_data.results_window,&gtk_data.window_width[6],&gtk_data.window_height[6]);
  printf("Map window size : %dx%d\n",gtk_data.window_width[6],gtk_data.window_height[6]);

// Display main window at its right position
  gtk_data.window_xpos0=55.;
  gtk_data.window_ypos0=0.;
  gtk_data.window_xoffset=10.;
  gtk_data.window_yoffset=50.;
  gtk_widget_show_all((GtkWidget*)gtk_data.main_window);
  gtk_window_move(gtk_data.main_window,gtk_data.window_xpos0,gtk_data.window_ypos0);

// and change color of channels according to their staus :
  for(Int_t ich=1; ich<=N_CHANNEL; ich++)
  {
    sprintf(object_name,"0_channel_%d",ich);
    widget = (GtkWidget*)gtk_builder_get_object (gtk_data.builder, object_name);
    //widget = (GtkWidget*)gtk_builder_get_object (gtk_data.builder, "0_channel_20");
    g_assert (widget);
    gtk_widget_set_name(widget,"green_button");
  }
  
  daq.daq_initialized=-1;
// Read default values and
// Disable channels if default values are such :
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    daq.delay_val[ich]=0;
    daq.bitslip_val[ich]=0;
    daq.byteslip_val[ich]=0;
    asic.DTU_clk_out[ich]=FALSE;
    asic.DTU_VCO_rail_mode[ich]=TRUE;
    asic.DTU_bias_ctrl_override[ich]=FALSE;
    asic.DTU_eLink[ich]=TRUE;
    asic.CATIA_Temp_val[ich]=-273.;
  }
  daq.n_active_channel=0;
  daq.eLink_active=0;
  daq.LV_ON=FALSE;
  FILE *fd_xml=fopen(".last_xml_file","r");
  char fname[132],fname2[110];
  if(fd_xml != NULL)
  {
    fscanf(fd_xml,"%s",daq.xml_filename);
    read_conf_from_xml_file(daq.xml_filename);
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }
  else
  {
    daq.debug_DAQ=FALSE;
    daq.debug_I2C=FALSE;
    daq.debug_GTK=FALSE;
    daq.debug_draw=FALSE;
    daq.zoom_draw=FALSE;
    daq.c1=NULL;
  }
  printf("Start DAQ with %d active channels: ",daq.n_active_channel);

  daq.trigger_type=-1;
  daq.self_trigger=0;
  daq.self_trigger_mode=0;
  daq.self_trigger_threshold=400;
  daq.self_trigger_mask=0;
  daq.gen_BC0=TRUE;
  daq.gen_WTE=FALSE;

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
  g_assert (widget);
  printf("trigger mode : %d\n",gtk_toggle_button_get_active((GtkToggleButton*)widget));
  gtk_toggle_button_set_active((GtkToggleButton*)widget,!gtk_toggle_button_get_active((GtkToggleButton*)widget));
  gtk_toggle_button_set_active((GtkToggleButton*)widget,!gtk_toggle_button_get_active((GtkToggleButton*)widget));

// Disable actions before DAQ initialization :
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_outputs");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_CATIA_outputs");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ADC_calibration");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_calibration");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_synchronize_links");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_synchronize_links");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_line_delays");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_line_delays");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU_I2C");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU_I2C");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU_test_unit");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU_test_unit");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_dump_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_dump_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_load_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_load_ADC_registers");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_optimize_pedestals");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_optimize_pedestals");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_PLL_scan");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_PLL_scan");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_LV");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_VFE");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_tap_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_bit_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_byte_slip");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_temp");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_temp");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_calibrate_FEMs");
  g_assert (widget);
  gtk_widget_set_sensitive(widget,FALSE);

  if(daq.autorun==0)      // Interactive running
    gtk_main();
  else if(daq.autorun==1) // Automatic running for FEMs calibration
  {
// Init DAQ
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_DAQ");
    g_assert (widget);
    gtk_button_clicked((GtkButton*)widget);
// Launch FEMs calibration
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_calibrate_FEMs");
    g_assert (widget);
    gtk_button_clicked((GtkButton*)widget);
  }
  return 0;
}
