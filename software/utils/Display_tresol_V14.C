#define n_G10 10
#define n_G11 19
#define n_G1  20
void Display_tresol_V14()
{
  double V_G10[n_G10]={50., 100.,200.,500., 1000.,1500.,2000.,2500.,3000.,3300.};
  //double Q_G10[n_G10]={16.5,32.6,65.6,164.1,328.3,492.4,656.3,818.2,979.6,1076.0}; // From FFT
  double Q_G10[n_G10]={15.8,32.8,65.8,165.2,328.1,493.1,656.4,818.7,979.9,1075.0}; // From fit
  double E_G10[n_G10]={  .6,  .6,  .6,   .6,   .6,   .6,   .6,   .6,   .6,    .6};
  double t_G10[n_G10]={565.,225.,115.,45.,  22.,  15.8, 11.8, 9.5,  8.0,  7.2};
  double V_G11[n_G11]={  50., 100., 250., 500., 750.,1000.,1250.,1500.,1750.,2000.,2250.,2500.,2750.,3000.,3250.,3500.,3750.,4000.,4200.};
  //double Q_G11[n_G11]={ 14.4, 26.6, 64.5,131.9,197.7,264.8,330.0,396.9,464.7,531.6,600.0,666.8,733.7,802.0,867.1,934.8,1000.2,1067.8,1121.0}; // From fit
  double Q_G11[n_G11]={ 13.2, 26.3, 65.7,131.7,197.7,263.8,329.9,398.3,464.6,532.4,600.2,667.9,734.6,801.7,868.3,935.0,1001.5,1068.4,1120.9}; // From fit
  double E_G11[n_G11]={   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6,   .6};
  double V_G1[n_G1] ={  500., 1000., 2000., 5000., 7500.,10000.,12500.,15000.,17500.,20000.,22500.,25000.,27500.,30000.,32500.,35000.,37500.,40000.,41000.,42500.};
  double Q_G1[n_G1] ={13.4,  26.7,  53.6,  132.5, 199.3, 265.3, 331.9, 397.9, 464.5, 531.2, 596.9, 664.0, 730.8, 797.4, 865.0, 931.5, 997.4, 1064.6,1092.2,1130.9};
  double E_G1[n_G1] ={0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3,   0.3};
  double t_G1[n_G1] ={210.3, 108.6, 54.2,  21.1,  13.3,  11.1,  8.4,   7.2,   6.4,   6.0,   5.1,   4.7,   4.6,   4.2,   4.0,   3.9,   3.9,   3.6,   3.5,   3.5};

  TF1 *f1=new TF1("f1","sqrt([0]*[0]/x/x+[1]*[1])",0.1,50000.);
  TCanvas *c1=new TCanvas();

  TGraphErrors *tgt_G1=new TGraphErrors();
  TGraphErrors *tgq_G1=new TGraphErrors();
  for(int i=0; i<n_G1; i++)
  {
    tgt_G1->SetPoint(i,Q_G1[i]*10.,t_G1[i]/sqrt(2.));
    tgt_G1->SetPointError(i,E_G1[i],0.5);
    tgq_G1->SetPoint(i,V_G1[i],Q_G1[i]);
    tgq_G1->SetPointError(i,0.1,E_G1[i]);
  }
  tgt_G1->SetMarkerStyle(20);
  tgt_G1->SetMarkerSize(1.0);
  tgt_G1->SetMarkerColor(kBlue);
  tgt_G1->SetLineColor(kBlue);
  tgt_G1->Draw("ap");
  f1->SetParameter(0,20000.);
  f1->SetParameter(1,1.);
  tgt_G1->Fit(f1,"","",1.,12000.);

  TGraphErrors *tgq_G11=new TGraphErrors();
  for(int i=0; i<n_G11; i++)
  {
    tgq_G11->SetPoint(i,V_G11[i],Q_G11[i]);
    tgq_G11->SetPointError(i,0.1,E_G11[i]);
  }

  TGraphErrors *tgt_G10=new TGraphErrors();
  TGraphErrors *tgq_G10=new TGraphErrors();
  for(int i=0; i<n_G10; i++)
  {
    tgt_G10->SetPoint(i,Q_G10[i],t_G10[i]/sqrt(2.));
    tgt_G10->SetPointError(i,E_G10[i],0.5);
    tgq_G10->SetPoint(i,V_G10[i],Q_G10[i]);
    tgq_G10->SetPointError(i,0.1,E_G10[i]);
  }
  tgt_G10->SetMarkerStyle(20);
  tgt_G10->SetMarkerSize(1.0);
  tgt_G10->SetMarkerColor(kRed);
  tgt_G10->SetLineColor(kRed);
  tgt_G10->Draw("ap");
  f1->SetParameter(0,5000.);
  f1->SetParameter(1,1.);
  tgt_G10->Fit(f1,"","",1.,1200.);

  tgt_G1->Draw("ap");
  tgt_G10->Draw("p");
  
  TCanvas *c2=new TCanvas();
  tgq_G11->Fit("pol1","","",10.,4500.);
  TF1 *f1_G11=tgq_G11->GetFunction("pol1");
  tgq_G10->Fit("pol1","","",10.,3400.);
  TF1 *f1_G10=tgq_G10->GetFunction("pol1");
  tgq_G1->Fit("pol1","","",10.,43000.);
  TF1 *f1_G1=tgq_G1->GetFunction("pol1");
  tgq_G11->SetMarkerStyle(20);
  tgq_G11->SetMarkerSize(1.0);
  tgq_G11->SetMarkerColor(kMagenta);
  tgq_G11->SetLineColor(kMagenta);
  tgq_G10->SetMarkerStyle(20);
  tgq_G10->SetMarkerSize(1.0);
  tgq_G10->SetMarkerColor(kRed);
  tgq_G10->SetLineColor(kRed);
  tgq_G1->SetMarkerStyle(20);
  tgq_G1->SetMarkerSize(1.0);
  tgq_G1->SetMarkerColor(kBlue);
  tgq_G1->SetLineColor(kBlue);
  tgq_G1->Draw("ap");
  tgq_G10->Draw("p");
  tgq_G11->Draw("p");

  TCanvas *c3=new TCanvas();
  TGraphErrors *tgr_G10=new TGraphErrors();
  for(int i=0; i<n_G10; i++)
  {
    Double_t res=Q_G10[i]-f1_G10->Eval(V_G10[i]);
    tgr_G10->SetPoint(i,Q_G10[i],res/1200.*100.);
    tgr_G10->SetPointError(i,E_G10[i],E_G10[i]/1200.*100.);
  }
  TGraphErrors *tgr_G11=new TGraphErrors();
  for(int i=0; i<n_G11; i++)
  {
    Double_t res=Q_G11[i]-f1_G11->Eval(V_G11[i]);
    tgr_G11->SetPoint(i,Q_G11[i],res/1200.*100.);
    tgr_G11->SetPointError(i,E_G11[i],E_G11[i]/1200.*100.);
  }
  TGraphErrors *tgr_G1=new TGraphErrors();
  for(int i=0; i<n_G1; i++)
  {
    Double_t res=Q_G1[i]-f1_G1->Eval(V_G1[i]);
    tgr_G1->SetPoint(i,Q_G1[i],res/1200.*100.);
    tgr_G1->SetPointError(i,E_G1[i],E_G1[i]/1200.*100.);
  }
  tgr_G10->SetMarkerStyle(20);
  tgr_G10->SetMarkerSize(1.0);
  tgr_G10->SetMarkerColor(kRed);
  tgr_G10->SetLineColor(kRed);
  tgr_G11->SetMarkerStyle(20);
  tgr_G11->SetMarkerSize(1.0);
  tgr_G11->SetMarkerColor(kMagenta);
  tgr_G11->SetLineColor(kMagenta);
  tgr_G1->SetMarkerStyle(20);
  tgr_G1->SetMarkerSize(1.0);
  tgr_G1->SetMarkerColor(kBlue);
  tgr_G1->SetLineColor(kBlue);
  tgr_G1->SetMaximum(0.4);
  tgr_G1->SetMinimum(-.4);
  tgr_G1->Draw("ap");
  tgr_G10->Draw("p");
  tgr_G11->Draw("p");
}
