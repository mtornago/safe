#include <TCanvas.h>
//#include <TLatex.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TF1.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TVirtualFFT.h>

#define NSAMPLE_MAX 28670
//#define NMSPS 160
//#define NMSPS 250

int cur_ch;

void ana_ped_DTU (char *fname, int debug=-1, Int_t NMSPS=160, Double_t noise_max=25, Int_t AC_coupling=0, Int_t nfrac=10, Int_t ped_cor=0, Int_t convert=1)
{
  int nch=6;
  Double_t R_TIA_10=5420.;
  Double_t R_TIA_1=520.;

  char hname[256], htitle[256];
  printf("Opening file %s for analysis\n",fname);
  TFile *infile=new TFile(fname);
  printf("Getting tree\n");
  TTree *tdata=(TTree*)infile->Get("data");
  TCanvas *tc_rms;
  gDirectory->GetObject("rms",tc_rms);
// Try to get frame size :
  char title[80];
  int nsample=0, nsample_sep=0, nsample_tot=0, nsample_fft=0;
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  sscanf(title,"ch0[%d]/S",&nsample_sep);
  sprintf(title,"%s",tdata->GetBranch("ch4")->GetTitle());
  sscanf(title,"ch4[%d]/S",&nsample_tot);
  printf("Setting branches for %d and %d samples\n",nsample_sep, nsample_tot);
  ULong64_t timestamp;
  int id;
  short int *sevent[nch];
  for(int ich=0; ich<4; ich++){sevent[ich]=(short int*) malloc(nsample_sep*sizeof(short int));}
  for(int ich=4; ich<6; ich++){sevent[ich]=(short int*) malloc(nsample_tot*sizeof(short int));}
  Double_t *event[nch];
  for(int ich=0; ich<4; ich++){event[ich]=(double*) malloc(nsample_sep*sizeof(double));}
  for(int ich=4; ich<6; ich++){event[ich]=(double*) malloc(nsample_tot*sizeof(double));}

  tdata->SetBranchAddress("id",&id);
  tdata->SetBranchAddress("timestamp",&timestamp);
  for(int ich=0; ich<nch; ich++)
  {
    printf("Channel %d\n",ich);
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    //printf("sample %d\n",is);
  }
  int nevt=(Int_t)tdata->GetEntries();

  Double_t pi=2.l*asin(1.l);
  Double_t dt=1.e-6/NMSPS;
  Double_t tmax=nsample_tot*dt;
  Double_t fmax=1/dt;
  Double_t df=1./tmax;
  Double_t dv=1.2/4096.;
  printf("dt %e, tmax %e, fmax %e, df %e Hz, dv %e mV\n",dt,tmax,fmax,df,dv*1000.);
  //dv=1.;
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);

  TGraph *tg_timestamp = new TGraph();
  tg_timestamp->SetTitle("gtimestamp");
  tg_timestamp->SetName("gtimestamp");
  TGraph *tg[nch];
  TH1D *hmean[nch], *hrms[nch], *hdped12, *hdped34, *hdlow12, *hdlow34, *hdhigh12, *hdhigh34, *hdgain12, *hdgain34, *henob[nch], *henob_FFT[nch];
  TProfile *pmod[nch], *pmod_folded[nch];;
  TH1D *hmod[nch],*hmod_loc[nch], *hmod_folded[nch];
  TH1D *hdensity[nch];
  for(int ich=0; ich<nch; ich++)
  {
    nsample=nsample_sep;
    dt=1.e-6/(NMSPS/2.);
    if(ich>=4)
    {
      nsample=nsample_tot;
      dt=1.e-6/NMSPS;
    }
    nsample_fft=nsample/nfrac;
    nsample_fft=(nsample_fft/4)*4;
    fmax=1./dt;
    if(ich==0)
      sprintf(htitle,"ADC_H, odd samples");
    else if(ich==1)
      sprintf(htitle,"ADC_H, even samples");
    else if(ich==2)
      sprintf(htitle,"ADC_L, odd samples");
    else if(ich==3)
      sprintf(htitle,"ADC_L, even samples");
    else if(ich==4)
      sprintf(htitle,"ADC_H, all samples");
    else if(ich==5)
      sprintf(htitle,"ADC_L, all samples");

    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    tg[ich]->SetMarkerColor(kRed);
    tg[ich]->SetLineColor(kRed);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,htitle,4096,0.,1.2);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,htitle,200,0.,2.e-3);
    sprintf(hname,"enob_ch%d",ich);
    henob[ich]=new TH1D(hname,htitle,240,6.,12.);
    sprintf(hname,"enob_FFT_ch%d",ich);
    henob_FFT[ich]=new TH1D(hname,htitle,240,6.,12.);
    sprintf(hname,"pmod_ch%d",ich);
    pmod[ich]=new TProfile(hname,htitle,nsample,0.,fmax);
    pmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"pmod_folded_ch%d",ich);
    printf("book pmod %d %d %e %e\n",ich,nsample_fft,fmax,df);
    pmod_folded[ich]=new TProfile(hname,htitle,nsample_fft,0.,fmax);
    pmod_folded[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_ch%d",ich);
    hmod[ich]=new TH1D(hname,htitle,nsample/2,0.,fmax/2.);
    hmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_loc_ch%d",ich);
    hmod_loc[ich]=new TH1D(hname,htitle,nsample,0.,fmax);
    hmod_loc[ich]->SetLineColor(kRed);
    sprintf(hname,"mod_folded_ch%d",ich);
    hmod_folded[ich]=new TH1D(hname,htitle,nsample_fft/2,0.,fmax/2.);
    hmod_folded[ich]->SetLineColor(kRed);
    hmod_folded[ich]->SetLineWidth(2.);
    hmod_folded[ich]->SetMinimum(0.);
    sprintf(hname,"bit_density_spectrum_ch%d",ich);
    sprintf(htitle,"bit_density_spectrum_ch%d",ich);
    hdensity[ich]=new TH1D(hname,htitle,4096,0.,4096.);
  }
  sprintf(hname,"Delta_ped_ch2_ch1");
  sprintf(htitle,"ADC_H, #Delta(ped), Even-Odd samples");
  hdped12=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_low_ch2_ch1");
  sprintf(htitle,"ADC_H, #Delta(low), Even-Odd samples");
  hdlow12=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_high_ch2_ch1");
  sprintf(htitle,"ADC_H, #Delta(high), Even-Odd samples");
  hdhigh12=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_gain_ch2_ch1");
  sprintf(htitle,"ADC_H, #Delta(gain), Even-Odd samples");
  hdgain12=new TH1D(hname,htitle,100,0.995,1.005);
  sprintf(hname,"Delta_ped_ch4_ch3");
  sprintf(htitle,"ADC_L, #Delta(ped), Even-Odd samples");
  hdped34=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_low_ch4_ch3");
  sprintf(htitle,"ADC_L, #Delta(low), Even-Odd samples");
  hdlow34=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_high_ch4_ch3");
  sprintf(htitle,"ADC_L, #Delta(high), Even-Odd samples");
  hdhigh34=new TH1D(hname,htitle,100,-5.,5.);
  sprintf(hname,"Delta_gain_ch4_ch3");
  sprintf(htitle,"ADC_L, #Delta(gain), Even-Odd samples");
  hdgain34=new TH1D(hname,htitle,100,0.995,1.005);

  Double_t delta_t=0., old_t=-1.;
  //nevt=2;
  Double_t qmax_max=0.;
  printf("Start analysis of %d events\n",nevt);

  nsample=nsample_tot;
  //Double_t *rex=new Double_t(nsample_tot);
  //Double_t *imx=new Double_t(nsample_tot);
  //Double_t *rey=new Double_t(nsample_tot);
  //Double_t *imy=new Double_t(nsample_tot);
  //Double_t *mod=new Double_t(nsample_tot);
  //Double_t *phase=new Double_t(nsample_tot);
  Double_t *rex=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *imx=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *rey=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *imy=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *mod=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *phase=(double *)malloc(nsample_tot*sizeof(double));

  TCanvas *cframe=new TCanvas("sample_frame","sample frame",1000,0,700,500);
  cframe->Update();
  sprintf(hname,"noise_spectrum");
  TCanvas *cmod=new TCanvas(hname,hname,1200,800);
  cmod->Divide(3,2);
  cmod->Update();
  sprintf(hname,"Even_Odd_diff");
  TCanvas *cdiff=new TCanvas(hname,hname,1200,800);
  cdiff->Divide(3,2);
  cdiff->Update();
  sprintf(hname,"Even_Odd_dgain");
  TCanvas *cdg=new TCanvas(hname,hname,1000,500);
  cdg->Divide(2,1);
  cdg->Update();
  TVirtualFFT *fft_f2 = TVirtualFFT::FFT(1, &nsample_tot, "C2CFORWARD ES K");
  TVirtualFFT *fft_f1 = TVirtualFFT::FFT(1, &nsample_sep, "C2CFORWARD ES K");
  nsample_fft=nsample_tot/nfrac;
  nsample_fft=(nsample_fft/4)*4;
  TVirtualFFT *fft_f3 = TVirtualFFT::FFT(1, &nsample_fft, "C2CFORWARD ES K");

  TF1 *fexpo=new TF1("fexpo","[0]+[1]*exp(-x*[2])",-1.e-3,2.e-3);
  TF1 *fpol1=new TF1("fpol1","[0]+[1]*x",-0.,2.e-3);
  fexpo->SetNpx(10000);
  Double_t rms[nch], hf_rms[nch], vhf_rms[nch];
  Double_t mean[3][nch];
  for(int ievt=0; ievt<nevt; ievt++)
  {
    Int_t evt_type=0;
    if((ievt%3)==2)evt_type=0; // Ped event
    if((ievt%3)==1)evt_type=1; // low edge event
    if((ievt%3)==0)evt_type=2; // high edge event
    if((ievt%100)==0) printf("analyse evt : %d, type %d\n",ievt, evt_type);
    tdata->GetEntry(ievt);
    if(debug>=0)printf("timestamp : %16.16llx\n",timestamp);
    Double_t t_event=6.25e-9*timestamp;
    if(old_t>0.) delta_t=t_event-old_t;
    old_t=t_event;
    tg_timestamp->SetPoint(ievt,t_event,delta_t);
    /*
    for(int is=0; is<nsample_sep; is+=2)
    {
      sevent[4][2*is+0]=sevent[1][is];
      sevent[4][2*is+1]=sevent[0][is];
      sevent[4][2*is+2]=sevent[1][is+1];
      sevent[4][2*is+3]=sevent[0][is+1];
      sevent[5][2*is+0]=sevent[3][is];
      sevent[5][2*is+1]=sevent[2][is];
      sevent[5][2*is+2]=sevent[3][is+1];
      sevent[5][2*is+3]=sevent[2][is+1];
    }
    */


    for( int ich=0; ich<nch; ich++)
    {
      nsample=nsample_sep;
      dt=1.e-6/(NMSPS/2.);
      if(ich>=4)
      {
        nsample=nsample_tot;
        dt=1.e-6/NMSPS;
      }
      fmax=1./dt;
      df=fmax/nsample;

      mean[evt_type][ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        Double_t pcor=0.;
        if(ich==4 && ped_cor==1 && (is%2)==1) pcor=mean[evt_type][1]-mean[evt_type][0];
        if(ich==5 && ped_cor==1 && (is%2)==1) pcor=mean[evt_type][3]-mean[evt_type][2];
        //if(is<10)printf("ich %d : pcor %e\n",ich,pcor);
        event[ich][is]=dv*sevent[ich][is]+pcor;
        mean[evt_type][ich]+=event[ich][is];
        hdensity[ich]->Fill(sevent[ich][is]);
      }
      mean[evt_type][ich]/=nsample;
      hmean[ich]->Fill(mean[evt_type][ich]);
      rms[ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        event[ich][is]-=mean[evt_type][ich];
        rex[is]=event[ich][is];
        imx[is]=0.;
        rms[ich]+=event[ich][is]*event[ich][is];
        Double_t t=dt*is;
        if(ich==1 || ich==3) t=dt*(0.5+is);
        tg[ich]->SetPoint(is,t,event[ich][is]);
      }

      rms[ich]=sqrt(rms[ich]/nsample);
      printf("Channel %d : mean=%.1f lsb, %.1f mV, rms_t=%e V, ",ich,mean[evt_type][ich]/dv,mean[evt_type][ich]*1000.,rms[ich]);
      //if(rms[ich]>5.e-3)continue;
      hrms[ich]->Fill(rms[ich]);
      henob[ich]->Fill(log(1.2/(rms[ich]*sqrt(12)))/log(2));
      //printf("rms=%e V, ",rms[ich]);
 
      //if(rms[ich]>1.8e-3)continue;
      if(ich<4)
      {
        fft_f1->SetPointsComplex(rex, imx);
        fft_f1->Transform();
        fft_f1->GetPointsComplex(rey, imy);
      }
      else
      {
        fft_f2->SetPointsComplex(rex, imx);
        fft_f2->Transform();
        fft_f2->GetPointsComplex(rey, imy);
      }
      rms[ich]=0.;
      hf_rms[ich]=0.;
      vhf_rms[ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        Double_t f=df*(0.5+is);
        rey[is]/=nsample;
        imy[is]/=nsample;
        mod[is]=rey[is]*rey[is]+imy[is]*imy[is]; 
        int ibad=0;
        //if(f>37e6 && f<40e6)ibad=1;
        //if(f>74e6 && f<86e6)ibad=1;
        //if(f>120e6 && f<123e6)ibad=1;
        if(ibad==0)
        if(is>0)rms[ich]+=mod[is];
        if(f>250000. && f<fmax-250000.) hf_rms[ich]+=mod[is];
        if(f>1500000. && f<fmax-1500000.) vhf_rms[ich]+=mod[is];
        mod[is]=sqrt(mod[is])/sqrt(df); // [V/sqrt(Hz)]
        phase[is]=atan2(imy[is],rey[is]);
        hmod_loc[ich]->SetBinContent(is+1,mod[is]);
        //if(rms[ich]<2.0e-3)pmod[ich]->Fill(df*is,mod[is]);
        if(evt_type==0)pmod[ich]->Fill(f,mod[is]*mod[is]); // We sum the power spectra
      }
      rms[ich]=sqrt(rms[ich]);
      hf_rms[ich]=sqrt(hf_rms[ich]);
      vhf_rms[ich]=sqrt(vhf_rms[ich]);
      printf("rms_f_hf_vhf %.3e %.3e %.3e, enob %.2f\n",rms[ich],hf_rms[ich], vhf_rms[ich],log(1.2/(rms[ich]*sqrt(12)))/log(2));
      //cmod->cd(ich+1);
      //hmod_loc[ich]->Draw();
      //hmod_loc[ich]->GetXaxis()->SetRangeUser(1.e4,fmax/2.);
      //hmod_loc[ich]->SetMinimum(0.);
      //hmod_loc[ich]->SetMaximum(300.);
      //gPad->SetGridx();
      //gPad->SetGridy();
      //gPad->SetLogx();
      //pmod[ich]->Draw("same");
      //if(ich==4)cmod->Update();
    }
    if(evt_type==0)
    {
      hdped12->Fill((mean[0][1]-mean[0][0])/dv);
      hdped34->Fill((mean[0][3]-mean[0][2])/dv);
    }
    if(evt_type==1)
    {
      hdlow12->Fill((mean[1][1]-mean[1][0])/dv);
      hdlow34->Fill((mean[1][3]-mean[1][2])/dv);
    }
    if(evt_type==2)
    {
      hdhigh12->Fill((mean[2][1]-mean[2][0])/dv);
      hdhigh34->Fill((mean[2][3]-mean[2][2])/dv);
      hdgain12->Fill((mean[2][1]-mean[1][1])/(mean[2][0]-mean[1][0]));
      printf("dgain12 : %e %e %e %e %e\n",mean[2][0], mean[1][0], mean[2][1], mean[1][1], (mean[2][1]-mean[1][1])/(mean[2][0]-mean[1][0]));
      hdgain34->Fill((mean[2][3]-mean[1][3])/(mean[2][2]-mean[1][2]));
      printf("dgain34 : %e %e %e %e %e\n",mean[2][2], mean[1][2], mean[2][3], mean[1][3], (mean[2][3]-mean[1][3])/(mean[2][2]-mean[1][2]));
    }

    if(debug>=0 || ievt==1)
    {
      cframe->cd();
      cframe->Update();
      tg[4]->DrawClone("alp");
      gPad->SetGridx();
      gPad->SetGridy();
      tg[4]->GetXaxis()->SetTitle("time [ns]");
      tg[4]->GetYaxis()->SetTitle("amplitude [mV]");
      cframe->Update();
    }

    nsample=nsample_fft;
    dt=1.e-6/NMSPS;
    fmax=1./dt;
    df=fmax/nsample;
    tmax=nsample*dt;
    for(Int_t ifrac=0; ifrac<nfrac; ifrac++)
    {
      for(Int_t is=0; is<nsample; is++)
      {
        Double_t t,y;
        rex[is]=event[4][ifrac*nsample+is];
        imx[is]=0.;
      }
      fft_f3->SetPointsComplex(rex,imx);
      fft_f3->Transform();
      fft_f3->GetPointsComplex(rey,imy);
      for(int is=0; is<nsample; is++)
      {
        Double_t f=df*(0.5+is);
        rey[is]/=nsample;
        imy[is]/=nsample;
        mod[is]=rey[is]*rey[is]+imy[is]*imy[is]; 
        mod[is]=sqrt(mod[is])/sqrt(df); // [V/sqrt(Hz)]
        pmod_folded[4]->Fill(f,mod[is]*mod[is]);
      }
    }
  }
  //TLatex *tex=new TLatex();
  printf("Final : \n");
  for( int ich=0; ich<nch; ich++)
  {
    printf("ch%d, rms (uV) = ",ich);
    nsample=nsample_sep;
    dt=1.e-6/(NMSPS/2.);
    if(ich>=4)
    {
      nsample=nsample_tot;
      dt=1.e-6/NMSPS;
    }
    fmax=1./dt;
    df=fmax/nsample;

    Double_t mod;
    rms[ich]=0.;
    hf_rms[ich]=0.;
    vhf_rms[ich]=0.;
    for(int is=0; is<=nsample/2; is++)
    {
      Double_t f=df*is;
      mod=pmod[ich]->GetBinContent(is+1);
      if(ich==0 || ich==1 ||ich==4)
      {
        if(convert==1)
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)/R_TIA_10*1.e12); // We fold positive and negative frequencies pA/sqrt(Hz)
        else
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)*1.e9); // We fold positive and negative frequencies nV/sqrt(Hz)
      }
      else
      {
        if(convert==1)
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)/R_TIA_1*1.e12); // We fold positive and negative frequencies pA/sqrt(Hz)
        else
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)*1.e9); // We fold positive and negative frequencies nV/sqrt(Hz)
      }
      if(is>0)rms[ich]+=mod*df*2.;
      if(f>250000.) hf_rms[ich]+=mod*df*2.;
      if(f>1500000.) vhf_rms[ich]+=mod*df*2.;
    }
    rms[ich]=sqrt(rms[ich]);
    hf_rms[ich]=sqrt(hf_rms[ich]);
    vhf_rms[ich]=sqrt(vhf_rms[ich]);
    printf("%.3e %.3e %.3e, ",rms[ich]*1.e6,hf_rms[ich]*1.e6,vhf_rms[ich]*1.e6);
    if(ich<2) cmod->cd(ich+1);
    else if(ich<4) cmod->cd(ich+2);
    else if(ich==4) cmod->cd(3);
    else if(ich==5) cmod->cd(6);
    cmod->Update();
    Double_t density=0.;
    if(ich==4)
    {
      hmod[ich]->Fit("pol0","Q","",2.e5,8.e6);
      TF1 *f1=hmod[ich]->GetFunction("pol0");
      density=f1->GetParameter(0);
    }
    hmod[ich]->Draw();
    //sprintf(title,"NDS ch %d",ich);
    //hmod[ich]->SetTitle(title);
    hmod[ich]->SetLineColor(kBlue);
    hmod[ich]->SetLineWidth(2.);
    hmod[ich]->SetTitleSize(0.05);
    hmod[ich]->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetRangeUser(2.e3,fmax/2.);
    hmod[ich]->GetXaxis()->SetTitle("frequency [Hz]");
    hmod[ich]->GetXaxis()->SetTitleSize(0.05);
    hmod[ich]->GetXaxis()->SetLabelSize(0.05);
    hmod[ich]->GetXaxis()->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetLabelFont(62);
    //hmod[ich]->GetYaxis()->SetTitle("noise density [nV/#sqrt{Hz}]");
    if(convert==1)
      hmod[ich]->GetYaxis()->SetTitle("noise density [pA/#sqrt{Hz}]");
    else
      hmod[ich]->GetYaxis()->SetTitle("noise density [nV/#sqrt{Hz}]");
    hmod[ich]->GetYaxis()->SetTitleSize(0.05);
    hmod[ich]->GetYaxis()->SetLabelSize(0.05);
    hmod[ich]->GetYaxis()->SetTitleOffset(1.1);
    hmod[ich]->GetYaxis()->SetTitleFont(62);
    hmod[ich]->GetYaxis()->SetLabelFont(62);
    hmod[ich]->SetMinimum(0.);
    hmod[ich]->SetMaximum(noise_max);
    sprintf(title,"Total noise = %.0f #muV, %.0f nA",rms[ich]*1.e6,rms[ich]/R_TIA_10*1.e9);
    if(ich==4)
    {
      printf("residual RMS : %.0f uV, %.0f nA, enob = %.2f bits, density=%.2f nV/sqrt(Hz)\n",
      rms[ich]*1.e6,rms[ich]/R_TIA_10*1.e9,log(1.2/(rms[ich]*sqrt(12)))/log(2),density);
    }
    else if(ich==0 || ich==1)
    {
      printf("residual RMS : %.0f uV, %.0f nA, enob = %.2f bits\n",rms[ich]*1.e6,rms[ich]/R_TIA_10*1.e9,log(1.2/(rms[ich]*sqrt(12)))/log(2));
    }
    else
    {
      if(convert==1)hmod[ich]->SetMaximum(noise_max*3.);
      printf("residual RMS : %.0f uV, %.0f nA, enob = %.2f bits\n",rms[ich]*1.e6,rms[ich]/R_TIA_1*1.e9,log(1.2/(rms[ich]*sqrt(12)))/log(2));
    }
    //tex->DrawLatexNDC(0.5,0.85,title);
    sprintf(title,"HF noise     = %.0f #muV",hf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.80,title);
    //sprintf(title,"VHF noise = %.0f #muV",vhf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.75,title);
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->SetLogx();
    gPad->Modified();
    cmod->Update();
  }
  printf("\n");

  cdiff->cd(1);
  hdped12->Fit("gaus","qL","");
  hdped12->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdped12->SetLineColor(kBlue);
  hdped12->SetLineWidth(2.);
  hdped12->SetTitleSize(0.05);
  hdped12->SetTitleFont(62);
  hdped12->GetXaxis()->SetTitle("difference [lsb]");
  hdped12->GetXaxis()->SetTitleSize(0.05);
  hdped12->GetXaxis()->SetLabelSize(0.05);
  hdped12->GetXaxis()->SetTitleFont(62);
  hdped12->GetXaxis()->SetLabelFont(62);
  hdped12->GetYaxis()->SetLabelSize(0.05);
  hdped12->GetYaxis()->SetLabelFont(62);
  cdiff->cd(2);
  hdlow12->Fit("gaus","qL","");
  hdlow12->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdlow12->SetLineColor(kBlue);
  hdlow12->SetLineWidth(2.);
  hdlow12->SetTitleSize(0.05);
  hdlow12->SetTitleFont(62);
  hdlow12->GetXaxis()->SetTitle("difference [lsb]");
  hdlow12->GetXaxis()->SetTitleSize(0.05);
  hdlow12->GetXaxis()->SetLabelSize(0.05);
  hdlow12->GetXaxis()->SetTitleFont(62);
  hdlow12->GetXaxis()->SetLabelFont(62);
  hdlow12->GetYaxis()->SetLabelSize(0.05);
  hdlow12->GetYaxis()->SetLabelFont(62);
  cdiff->cd(3);
  hdhigh12->Fit("gaus","qL","");
  hdhigh12->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdhigh12->SetLineColor(kBlue);
  hdhigh12->SetLineWidth(2.);
  hdhigh12->SetTitleSize(0.05);
  hdhigh12->SetTitleFont(62);
  hdhigh12->GetXaxis()->SetTitle("difference [lsb]");
  hdhigh12->GetXaxis()->SetTitleSize(0.05);
  hdhigh12->GetXaxis()->SetLabelSize(0.05);
  hdhigh12->GetXaxis()->SetTitleFont(62);
  hdhigh12->GetXaxis()->SetLabelFont(62);
  hdhigh12->GetYaxis()->SetLabelSize(0.05);
  hdhigh12->GetYaxis()->SetLabelFont(62);
  cdiff->cd(4);
  hdped34->Fit("gaus","qL","");
  hdped34->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdped34->SetLineColor(kBlue);
  hdped34->SetLineWidth(2.);
  hdped34->SetTitleSize(0.05);
  hdped34->SetTitleFont(62);
  hdped34->GetXaxis()->SetTitle("difference [lsb]");
  hdped34->GetXaxis()->SetTitleSize(0.05);
  hdped34->GetXaxis()->SetLabelSize(0.05);
  hdped34->GetXaxis()->SetTitleFont(62);
  hdped34->GetXaxis()->SetLabelFont(62);
  hdped34->GetYaxis()->SetLabelSize(0.05);
  hdped34->GetYaxis()->SetLabelFont(62);
  cdiff->cd(5);
  hdlow34->Fit("gaus","qL","");
  hdlow34->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdlow34->SetLineColor(kBlue);
  hdlow34->SetLineWidth(2.);
  hdlow34->SetTitleSize(0.05);
  hdlow34->SetTitleFont(62);
  hdlow34->GetXaxis()->SetTitle("difference [lsb]");
  hdlow34->GetXaxis()->SetTitleSize(0.05);
  hdlow34->GetXaxis()->SetLabelSize(0.05);
  hdlow34->GetXaxis()->SetTitleFont(62);
  hdlow34->GetXaxis()->SetLabelFont(62);
  hdlow34->GetYaxis()->SetLabelSize(0.05);
  cdiff->cd(6);
  hdhigh34->Fit("gaus","qL","");
  hdhigh34->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdhigh34->SetLineColor(kBlue);
  hdhigh34->SetLineWidth(2.);
  hdhigh34->SetTitleSize(0.05);
  hdhigh34->SetTitleFont(62);
  hdhigh34->GetXaxis()->SetTitle("difference [lsb]");
  hdhigh34->GetXaxis()->SetTitleSize(0.05);
  hdhigh34->GetXaxis()->SetLabelSize(0.05);
  hdhigh34->GetXaxis()->SetTitleFont(62);
  hdhigh34->GetXaxis()->SetLabelFont(62);
  hdhigh34->GetYaxis()->SetLabelSize(0.05);
  hdhigh34->GetYaxis()->SetLabelFont(62);
  cdiff->Update();

  cdg->cd(1);
  hdgain12->Fit("gaus","qL","");
  hdgain12->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdgain12->SetLineColor(kBlue);
  hdgain12->SetLineWidth(2.);
  hdgain12->SetTitleSize(0.05);
  hdgain12->SetTitleFont(62);
  hdgain12->GetXaxis()->SetRangeUser(0.997, 1.003);
  hdgain12->GetXaxis()->SetTitle("gain ratio");
  hdgain12->GetXaxis()->SetTitleSize(0.05);
  hdgain12->GetXaxis()->SetLabelSize(0.05);
  hdgain12->GetXaxis()->SetTitleFont(62);
  hdgain12->GetXaxis()->SetLabelFont(62);
  hdgain12->GetYaxis()->SetLabelSize(0.05);
  hdgain12->GetYaxis()->SetLabelFont(62);
  cdg->cd(2);
  hdgain34->Fit("gaus","qL","");
  hdgain34->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  hdgain34->SetLineColor(kBlue);
  hdgain34->SetLineWidth(2.);
  hdgain34->SetTitleSize(0.05);
  hdgain34->SetTitleFont(62);
  hdgain34->GetXaxis()->SetRangeUser(0.997, 1.003);
  hdgain34->GetXaxis()->SetTitle("gain ratio");
  hdgain34->GetXaxis()->SetTitleSize(0.05);
  hdgain34->GetXaxis()->SetLabelSize(0.05);
  hdgain34->GetXaxis()->SetTitleFont(62);
  hdgain34->GetXaxis()->SetLabelFont(62);
  hdgain34->GetYaxis()->SetLabelSize(0.05);
  hdgain34->GetYaxis()->SetLabelFont(62);
  cdg->Update();

  nsample_fft=nsample_tot/nfrac;
  nsample_fft=(nsample_fft/4)*4;
  for(int is=0; is<=nsample_fft/2; is++)
  {
    Double_t mod=pmod_folded[4]->GetBinContent(is+1);
    hmod_folded[4]->SetBinContent(is+1,sqrt(mod*2.)); // We fold positive and negative frequencies
    hmod_folded[4]->SetBinError(is+1,0.);
  }

  sprintf(hname,"%s",fname);
  char *pos=strstr(hname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",hname);
  TFile *tfout=new TFile(hname,"recreate");
  sprintf(pos,"_analized.txt");
  FILE *fout=fopen(hname,"w+");
  cframe->Write();
  tc_rms->Write();
  cmod->Write();
  for(int ich=0; ich<nch; ich++)
  {
    hmean[ich]->Write();
    hrms[ich]->Write();
    henob[ich]->Write();
    henob_FFT[ich]->Write();
    hmod[ich]->Write();
    pmod[ich]->Write();
    hmod_folded[ich]->Write();
    pmod_folded[ich]->Write();
    hdensity[ich]->Write();
    fprintf(fout,"%.3e %.3e ",rms[ich],hf_rms[ich]);
  }
  hdped12->SetLineWidth(2);
  hdped12->SetLineColor(kBlue);
  hdped12->Write();
  hdped34->SetLineWidth(2);
  hdped34->SetLineColor(kBlue);
  hdped34->Write();
  hdlow12->SetLineWidth(2);
  hdlow12->SetLineColor(kBlue);
  hdlow12->Write();
  hdlow34->SetLineWidth(2);
  hdlow34->SetLineColor(kBlue);
  hdlow34->Write();
  hdhigh12->SetLineWidth(2);
  hdhigh12->SetLineColor(kBlue);
  hdhigh12->Write();
  hdhigh34->SetLineWidth(2);
  hdhigh34->SetLineColor(kBlue);
  hdhigh34->Write();
  hdgain12->SetLineWidth(2);
  hdgain12->SetLineColor(kBlue);
  hdgain12->Write();
  hdgain34->SetLineWidth(2);
  hdgain34->SetLineColor(kBlue);
  hdgain34->Write();
  fprintf(fout,"\n");
  tfout->Close();
  fclose(fout);
}
