void APD_gain()
{
  double V1[6]={20., 50., 100.,200.,300.,332.};
  double a1[6]={13.0,16.4,20.2,54.0,384.,856.};

  double V2[7]={300.,332.,352.,361.,370.,380.,389.};
  double a2[7]={28.0,53.0,93.0,123.,186.,314.,794.};
  Double_t ref=a1[5];
  for(int i=0; i<6; i++)
  {
    a1[i]=a1[i]*a2[1]/ref;
  }

  TGraph *tg1=new TGraph(6,V1,a1);
  tg1->SetMarkerStyle(20);
  tg1->SetMarkerSize(1.);
  tg1->SetMarkerColor(kRed);
  tg1->SetLineColor(kRed);
  tg1->SetLineWidth(2.);
  TGraph *tg2=new TGraph(7,V2,a2);
  tg2->SetMarkerStyle(20);
  tg2->SetMarkerSize(1.);
  tg2->SetMarkerColor(kRed);
  tg2->SetLineColor(kRed);
  tg2->SetLineWidth(2.);
  tg2->SetMaximum(800.);
  tg2->SetMinimum(0.01);
  tg2->Draw("alp");
  tg2->GetXaxis()->SetLimits(0.,400.);
  gPad->SetGridx();
  gPad->SetGridy();
  //gPad->SetLogy();
  tg2->Draw("alp");
  tg1->Draw("lp");
}
