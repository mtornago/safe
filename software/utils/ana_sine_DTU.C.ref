#include <TCanvas.h>
//#include <TLatex.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TF1.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TVirtualFFT.h>

#define NSAMPLE_MAX 28670
//#define NMSPS 160
//#define NMSPS 250

double my_sine(double *x, double *par)
{
  double ped[4], amp[4],f,phase;
  static double pi=asin(1.l)*2.l;
  double t=x[0];
  int it=int(t/6.25e-9);
  int isub4=it%4;
  int isub2=it%2;
  ped[0]=par[0];
  ped[1]=par[1];
  ped[2]=par[2];
  ped[3]=par[3];
  amp[0]=par[4];
  amp[1]=par[5];
  amp[2]=par[6];
  amp[3]=par[7];
  f=par[8];
  phase=par[9]/180.*pi;
  double y=ped[isub4]+amp[isub4]*sin(2.*pi*f*t+phase);
  return y;
}

void ana_sine_DTU(TString fname, double f=500.e3, int correct=0, int type=2, int MC=0, double thermal_noise=0.0004, double apperture_jitter=1.e-12, Int_t ievt=0, Int_t shift_odd_samples=0,Int_t window_type=0)
{
  printf("Opening file %s for analysis\n",fname.Data());
  TFile *infile=new TFile(fname);
  printf("Getting tree\n");
  TTree *tdata=(TTree*)infile->Get("tdata");
// Try to get frame size :
  char title[80];
  int nsample=0, nsample_sep=0, nsample_tot=0, nsample_fft=0;
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  sscanf(title,"ch0[%d]/S",&nsample_sep);
  sprintf(title,"%s",tdata->GetBranch("ch4")->GetTitle());
  sscanf(title,"ch4[%d]/S",&nsample_tot);
  printf("Setting branches for %d and %d samples\n",nsample_sep, nsample_tot);
  ULong64_t timestamp;
  int id;
  Int_t nch=6;
  short int *sevent[nch];
  for(int ich=0; ich<4; ich++){sevent[ich]=(short int*) malloc(nsample_sep*sizeof(short int));}
  for(int ich=4; ich<6; ich++){sevent[ich]=(short int*) malloc(nsample_tot*sizeof(short int));}
  Double_t *event[nch];
  for(int ich=0; ich<4; ich++){event[ich]=(double*) malloc(nsample_sep*sizeof(double));}
  for(int ich=4; ich<6; ich++){event[ich]=(double*) malloc(nsample_tot*sizeof(double));}
  nsample_sep=26622*2;
  nsample_tot=26622*4;

  tdata->SetBranchAddress("id",&id);
  tdata->SetBranchAddress("timestamp",&timestamp);
  for(int ich=0; ich<nch; ich++)
  {
    printf("Channel %d\n",ich);
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    //printf("sample %d\n",is);
  }

  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);
  TRandom *tr=new TRandom();
  int n=0, val;
  double t=0.;
  double dt=6.25e-9;
  double dv=1.2/4096;
  TGraphErrors *tg_MC[2];
  TGraphErrors *tg[2];
  TGraphErrors *tge_orig[2];
  TGraphErrors *tge[2];
  TGraphErrors *tge1[2];
  TGraphErrors *tge2[2];
  TGraphErrors *tgo[2];
  TGraphErrors *tgo1[2];
  TGraphErrors *tgo2[2];
  TGraph *tg_res[2];
  TGraph *tgr_orig[2];
  char hname[80];
  sprintf(hname,"ADC_H_MC");
  tg_MC[0]=new TGraphErrors();
  tg_MC[0]->SetName(hname);
  tg_MC[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_MC");
  tg_MC[1]=new TGraphErrors();
  tg_MC[1]->SetName(hname);
  tg_MC[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_data");
  tg[0]=new TGraphErrors();
  tg[0]->SetName(hname);
  tg[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_data");
  tg[1]=new TGraphErrors();
  tg[1]->SetName(hname);
  tg[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_residuals");
  tg_res[0]=new TGraphErrors();
  tg_res[0]->SetName(hname);
  tg_res[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_residuals");
  tg_res[1]=new TGraphErrors();
  tg_res[1]->SetName(hname);
  tg_res[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_original_residuals");
  tgr_orig[0]=new TGraphErrors();
  tgr_orig[0]->SetName(hname);
  tgr_orig[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_original_residuals");
  tgr_orig[1]=new TGraphErrors();
  tgr_orig[1]->SetName(hname);
  tgr_orig[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_even_original");
  tge_orig[0]=new TGraphErrors();
  tge_orig[0]->SetName(hname);
  tge_orig[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_even_original");
  tge_orig[1]=new TGraphErrors();
  tge_orig[1]->SetName(hname);
  tge_orig[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_even");
  tge[0]=new TGraphErrors();
  tge[0]->SetName(hname);
  tge[0]->SetTitle(hname);
  sprintf(hname,"ADC_H_even1");
  tge1[0]=new TGraphErrors();
  tge1[0]->SetName(hname);
  tge1[0]->SetTitle(hname);
  sprintf(hname,"ADC_H_even2");
  tge2[0]=new TGraphErrors();
  tge2[0]->SetName(hname);
  tge2[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_even");
  tge[1]=new TGraphErrors();
  tge[1]->SetName(hname);
  tge[1]->SetTitle(hname);
  sprintf(hname,"ADC_L_even1");
  tge1[1]=new TGraphErrors();
  tge1[1]->SetName(hname);
  tge1[1]->SetTitle(hname);
  sprintf(hname,"ADC_L_even2");
  tge2[1]=new TGraphErrors();
  tge2[1]->SetName(hname);
  tge2[1]->SetTitle(hname);
  sprintf(hname,"ADC_H_odd");
  tgo[0]=new TGraphErrors();
  tgo[0]->SetName(hname);
  tgo[0]->SetTitle(hname);
  sprintf(hname,"ADC_H_odd1");
  tgo1[0]=new TGraphErrors();
  tgo1[0]->SetName(hname);
  tgo1[0]->SetTitle(hname);
  sprintf(hname,"ADC_H_odd2");
  tgo2[0]=new TGraphErrors();
  tgo2[0]->SetName(hname);
  tgo2[0]->SetTitle(hname);
  sprintf(hname,"ADC_L_odd");
  tgo[1]=new TGraphErrors();
  tgo[1]->SetName(hname);
  tgo[1]->SetTitle(hname);
  sprintf(hname,"ADC_L_odd1");
  tgo1[1]=new TGraphErrors();
  tgo1[1]->SetName(hname);
  tgo1[1]->SetTitle(hname);
  sprintf(hname,"ADC_L_odd2");
  tgo2[1]=new TGraphErrors();
  tgo2[1]->SetName(hname);
  tgo2[1]->SetTitle(hname);

  TH1D *tpd=new TH1D("DNL_vs_amplitude","DNL vs amplitude",4096,0.,4096.);
  TProfile *tpi=new TProfile("INL_vs_amplitude","INL vs amplitude",4096,0.,4096.,"");
  TProfile *tps=new TProfile("sresidual_vs_amplitude","sresidual vs amplitude",40,0.,1.2,"S");
  TProfile *tpv=new TProfile("residual_vs_dvdt","residual vs dvdt",250,0.,2.5e8,"S");
  TH1D *hs=new TH1D("error_vs_amplitude","Error vs amplitude",40,0.,1.2);
  TH1D *hv=new TH1D("error_vs_dvdt","Error vs dvdt",250,0,2.5e8);
  TH1D *hres=new TH1D("samples_residuals","Even samples residuals",1000,-20.,20.);

  double pi=2.0l*asin(1.0l);
  TF1 *f1_1=new TF1("my_f1",my_sine,0.,2.e-3,10);
  f1_1->SetNpx(10000);
  TF1 *f1,*f1_2;
  if(f>0.01)
    f1_2=new TF1("f1","[0]+[1]*sin(2.*[4]*[2]*x+[3]/180.*[4])",0.,2.e-3);
  else
    f1_2=new TF1("f1","[0]",0.,1.e-3);
  f1=f1_2;
  f1->SetNpx(10000);
  f1->SetParameter(0,0.6);
  f1->SetParameter(1,0.6);
  f1->SetParameter(2,f);
  f1->SetParameter(3,0.);
  f1->FixParameter(4,pi);
  TF1 *f2=new TF1("f1","[0]*2.*[3]*[1]*cos(2.*[3]*[1]*x+[2]/180.*[3])",0.,2.e-3);
  f2->SetNpx(10000);
  TF1 *f1n;
  f1n=new TF1("f1n","[0]+[1]*sin(2.*[4]*[2]*x+[3]/180.*[4])+[5]*sin(2*[4]*[6]*x+[7]/180.*[4])",0.,2.e-3);
  //TF1 *f1_cor=new TF1("f1_cor","3.01*sin(2*3.141592654*(x-2050)/3900)+0.025",-10.,4100.);
  TF1 *f1_cor=new TF1("f1_cor","0.",-10.,4100.);

  double tmax=0.;
  TCanvas *ctemp=new TCanvas();
  ctemp->cd();
  ctemp->Update();
  double max=0., min=1.2;
  int nevt=(Int_t)tdata->GetEntries();
//  for(ievt=0; ievt<nevt; ievt++)
//  {
    tdata->GetEntry(ievt);

    for(int ich=0; ich<4; ich++)
    {
      Int_t nsample_loc=nsample_sep;
      for(int is=0; is<nsample_loc; is++)
      {
        event[ich][is]=dv*sevent[ich][is];
      }
    }

    for(int ich=0; ich<4; ich++)
    {
      Double_t dt_loc=dt;
      Int_t nsample_loc=nsample_tot;
      if(ich<4)
      {
        dt_loc*=2.;
        nsample_loc=nsample_sep;
      }

      for(int is=0; is<nsample_loc; is++)
      {
        Double_t t=dt_loc*is;
        if(ich==1 || ich==3)
        {
          tg[ich/2]->SetPoint(2*is,dt*2.*is,event[ich][is]);
          tg[ich/2]->SetPointError(2*is,5.e-12,dv);
        }
        else
        {
          tg[ich/2]->SetPoint(2*is+1,dt*(2*is+1),event[ich][is]);
          tg[ich/2]->SetPointError(2*is+1,5.e-12,dv);
        }

        if((ich==0 || ich==2) && (is%2)==0)
        {
          tge_orig[ich/2]->SetPoint(is,t,event[ich][is]);
          tge_orig[ich/2]->SetPointError(is,5.e-12,1.5*dv);
          tge[ich/2]->SetPoint(is,t,event[ich][is]);
          tge[ich/2]->SetPointError(is,5.e-12,1.5*dv);
          tge1[ich/2]->SetPoint(is/2,t,event[ich][is]);
          tge1[ich/2]->SetPointError(is/2,5.e-12,1.5*dv);
        }
        else if((ich==1 || ich==3) && (is%2)==0)
        {
          tgo[ich/2]->SetPoint(is,t,event[ich][is]);
          tgo[ich/2]->SetPointError(is,5.e-12,dv);
          tgo1[ich/2]->SetPoint(is/2,t,event[ich][is]);
          tgo1[ich/2]->SetPointError(is/2,5.e-12,dv);
        }
        else if((ich==0 || ich==2) && (is%2)==1)
        {
          tge_orig[ich/2]->SetPoint(is,t,event[ich][is]);
          tge_orig[ich/2]->SetPointError(is,5.e-12,1.5*dv);
          tge[ich/2]->SetPoint(is,t,event[ich][is]);
          tge[ich/2]->SetPointError(is,5.e-12,1.5*dv);
          tge2[ich/2]->SetPoint(is/2,t,event[ich][is]);
          tge2[ich/2]->SetPointError(is/2,5.e-12,1.5*dv);
        }
        else if((ich==1 || ich==3) && (is%2)==1)
        {
          tgo[ich/2]->SetPoint(is,t,event[ich][is]);
          tgo[ich/2]->SetPointError(is,5.e-12,dv);
          tgo2[ich/2]->SetPoint(is/2,t,event[ich][is]);
          tgo2[ich/2]->SetPointError(is/2,5.e-12,dv);
        }

        if(ich==0 || ich==1)
        {
          if(event[ich][is]>max)max=event[ich][is];
          if(event[ich][is]<min)min=event[ich][is];
        }
        tmax=t;
      }
    }
    printf("Found min=%f, max=%f\n",min,max);
    tg[0]->SetMarkerStyle(20);
    tg[0]->SetMarkerSize(0.1);
    tg[0]->SetMarkerColor(kRed);
    tg[0]->SetLineColor(kRed);
    tge[0]->SetMarkerStyle(20);
    tge[0]->SetMarkerSize(0.2);
    tge[0]->SetMarkerColor(kRed);
    tge[0]->SetLineColor(kRed);
    tgo[0]->SetMarkerStyle(20);
    tgo[0]->SetMarkerSize(0.2);
    tgo[0]->SetMarkerColor(kBlue);
    tgo[0]->SetLineColor(kBlue);
//  }
  //TCanvas *ct0=new TCanvas();
  //tg[0]->Draw("alp");
  //TCanvas *ct1=new TCanvas();
  //tge[0]->Draw("alp");
  //TCanvas *ct2=new TCanvas();
  //tgo[0]->Draw("alp");
  //return;

// Analyse and clean odd samples :
  n=tg[0]->GetN();
  tmax=dt*n;
  int no=tgo[0]->GetN();
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tgo[0]->Fit(f1,"Q","",1./f,100./f);
  f1->ReleaseParameter(2);
  tgo[0]->Fit(f1,"Q","",1./f,100./f);
  double pedo=f1->GetParameter(0);
  double ampo;
  double phaseo=f1->GetParameter(3);
  if(phaseo> 180.)phaseo-=360.;
  if(phaseo<-180.)phaseo+=360.;
  f1->SetParameter(3,phaseo);
  f1->SetParLimits(3,phaseo-2.,phaseo+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tgo[0]->Fit(f1,"Q","",1./f,tmax);
  pedo=f1->GetParameter(0);
  if(f>0.01)
  {
    ampo=f1->GetParameter(1);
    phaseo=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampo=1.;
    f=0.;
    phaseo=0.;
  }
  printf("Odd samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f deg\n",pedo,ampo,f,phaseo);
  tgo1[0]->Fit(f1,"Q","",1./f,tmax);
  double pedo1=f1->GetParameter(0);
  double ampo1=f1->GetParameter(1);
  double fo1=f1->GetParameter(2);
  double phaseo1=f1->GetParameter(3);
  printf("Odd samples 1 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo1,ampo1,fo1,phaseo1);
  tgo2[0]->Fit(f1,"Q","",1./f,tmax);
  double pedo2=f1->GetParameter(0);
  double ampo2=f1->GetParameter(1);
  double fo2=f1->GetParameter(2);
  double phaseo2=f1->GetParameter(3);
  printf("Odd samples 3 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo2,ampo2,fo2,phaseo2);
  printf("Delta phase odd 3 vs 1 : %e s\n",(phaseo2-phaseo1)/180*pi/(2*pi*f));
  printf("Delta Vcm odd 3 vs 1 : %e V\n",pedo2-pedo1);
  printf("Delta gain odd 3 vs 1 : %e\n",ampo2/ampo1);
  if(correct>1)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      if((i%4)==1)
      {
        tgo1[0]->GetPoint(i/4,t,y);
        ey=tgo1[0]->GetErrorY(i/4);
        tgo[0]->SetPoint(i/2,t,y);
        tgo[0]->SetPointError(i/2,et,ey);
      }
      else if ((i%4)==3)
      {
        tgo2[0]->GetPoint(i/4,t,y);
        ey=tgo2[0]->GetErrorY(i/4);
        tgo[0]->SetPoint(i/2,t,(y-pedo2)*ampo1/ampo2+pedo1);
        tgo[0]->SetPointError(i/2,et,ey);
      }
    }
  }
  tgo[0]->Fit(f1,"Q","",1./f,tmax);
  pedo=f1->GetParameter(0);
  if(f>0.01)
  {
    ampo=f1->GetParameter(1);
    phaseo=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampo=1.;
    f=0.;
    phaseo=0.;
  }
  printf("Odd samples corrected : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo,ampo,f,phaseo);
// Compute residuals
  for(int i=0; i<no; i++)
  {
    double t,y,et,ey;
    tgo[0]->GetPoint(i,t,y);
    double val=f1->Eval(t);
    //double dy=y-val;
    double dy=y-val-f1_cor->Eval(y/dv)*dv;
    hres->Fill(dy/dv);
  }

// Analyse and clean even samples :
  int ne=tge[0]->GetN();
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tge[0]->Fit(f1,"Q","",0.,100./f);
  f1->ReleaseParameter(2);
  tge[0]->Fit(f1,"Q","",0.,100./f);
  double pede=f1->GetParameter(0);
  double ampe;
  double phasee=f1->GetParameter(3);
  if(phasee> 180.)phasee-=360.;
  if(phasee<-180.)phasee+=360.;
  f1->SetParameter(3,phasee);
  f1->SetParLimits(3,phasee-2.,phasee+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tge[0]->Fit(f1,"Q","",0.,tmax);
  pede=f1->GetParameter(0);
  if(f>0.01)
  {
    ampe=f1->GetParameter(1);
    phasee=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampe=1.;
    f=0.;
    phasee=0.;
  }
  printf("Even samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede,ampe,f,phasee);

  tge1[0]->Fit(f1,"Q","",0.,tmax);
  double pede1=f1->GetParameter(0);
  double ampe1=f1->GetParameter(1);
  double fe1=f1->GetParameter(2);
  double phasee1=f1->GetParameter(3);
  printf("Even samples 0 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede1,ampe1,fe1,phasee1);
  tge2[0]->Fit(f1,"Q","",0.,tmax);
  double pede2=f1->GetParameter(0);
  double ampe2=f1->GetParameter(1);
  double fe2=f1->GetParameter(2);
  double phasee2=f1->GetParameter(3);
  printf("Even samples 2 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede2,ampe2,fe2,phasee2);
  printf("Delta phase even 2 vs 0 : %e s\n",(phasee2-phasee1)/180*pi/(2*pi*f));
  printf("Delta Vcm even 2 vs 0 : %e V\n",pede2-pede1);
  printf("Delta gain even 2 vs 0 : %e\n",ampe2/ampe1);
  if(correct>1)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      if((i%4)==0)
      {
        tge1[0]->GetPoint(i/4,t,y);
        ey=tge1[0]->GetErrorY(i/4);
        tge[0]->SetPoint(i/2,t,y);
        tge[0]->SetPointError(i/2,et,ey);
      }
      else if ((i%4)==2)
      {
        tge2[0]->GetPoint(i/4,t,y);
        ey=tge2[0]->GetErrorY(i/4);
        tge[0]->SetPoint(i/2,t,(y-pede2)*ampe1/ampe2+pede1);
        tge[0]->SetPointError(i/2,et,ey);
      }
    }
  }
  tge[0]->Fit(f1,"","",0.,tmax);
  pede=f1->GetParameter(0);
  if(f>0.01)
  {
    ampe=f1->GetParameter(1);
    phasee=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampe=1.;
    f=0.;
    phasee=0.;
  }
  //tge[0]->Draw("alp");
  //return;
  printf("Even samples corrected : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede,ampe,f,phasee);
// Compute residuals
  hres->Reset();
  for(int i=0; i<ne; i++)
  {
    double t,y,et,ey;
    tge[0]->GetPoint(i,t,y);
    double val=f1->Eval(t);
    //double dy=y-val;
    double dy=y-val-f1_cor->Eval(y/dv)*dv;
    hres->Fill(dy/dv);
  }
  // o*t+p0=o*(t+p0/o)=o*(t+p0/(2pif)
  double dphase=(phaseo-phasee)/180.*pi/(2.*pi*f);
  printf("Delta phase : %e s\n",dphase);
  printf("Delta Vcm : %e V\n",pedo-pede);
  printf("Delta gain : %e\n",ampo/ampe);

  if(correct>0)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      tg[0]->GetPoint(i,t,y);
      ey=tg[0]->GetErrorY(i);
      if(i%2==1)
      {
        //tg[0]->SetPoint(i,t,(y-pedo)*ampe/ampo+pede);
        tg[0]->SetPoint(i,t,y-pedo+pede);
        tg[0]->SetPointError(i,et,ey);
      }
    }
  }
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tg[0]->Fit(f1,"Q","",0.,100./f);
  f1->ReleaseParameter(2);
  tg[0]->Fit(f1,"Q","",0.,100./f);
  double ped=f1->GetParameter(0);
  double amp;
  double phaset=f1->GetParameter(3);
  if(phaset> 180.)phaset-=360.;
  if(phaset<-180.)phaset+=360.;
  f1->SetParameter(3,phaset);
  f1->SetParLimits(3,phaset-2.,phaset+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tg[0]->Fit(f1,"Q","",0.,tmax);
  if(f>0.01)
  {
    amp=f1->GetParameter(1);
    f=f1->GetParameter(2);
    phaset=f1->GetParameter(3);
  }
  else
  {
    amp=1.;
    f=0.;
    phaset=0.;
  }
  printf("All samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",ped,amp,f,phaset);
  if(correct==-99)
  {
    f1n->FixParameter(0,f1->GetParameter(0));
    f1n->FixParameter(1,f1->GetParameter(1));
    f1n->FixParameter(2,f1->GetParameter(2));
    f1n->FixParameter(3,f1->GetParameter(3));
    f1n->FixParameter(4,f1->GetParameter(4));
    f1n->SetParameter(5,1.);
    f1n->SetParameter(6,40.e6);
    f1n->SetParameter(7,0.);
    tg[0]->Fit(f1n,"","",0.,tmax);
    //tg[0]->Draw("alp");
    //return;
  }
  f1_1->SetParameter(0,ped);
  f1_1->SetParameter(1,ped);
  f1_1->SetParameter(2,ped);
  f1_1->SetParameter(3,ped);
  f1_1->SetParameter(4,amp);
  f1_1->SetParameter(5,amp);
  f1_1->SetParameter(6,amp);
  f1_1->SetParameter(7,amp);
  f1_1->SetParameter(8,f);
  f1_1->SetParameter(9,phaset);
  //tg[0]->Fit(f1_1,"","",0.,tmax);
  //f1=f1_1;
  tg[0]->Fit(f1,"","",0.,tmax);

  TGraphErrors *tg_loc;
  int n_loc;

  if(type==0)
  {
    tg_loc=tge[0];
    n_loc=ne;
  }
  else if (type==1)
  {
    tg_loc=tgo[0];
    n_loc=no;
  }
  else
  {
    tg_loc=tg[0];
    n_loc=n;
  }
  ctemp->Update();
  TCanvas *cres_orig=new TCanvas();
  cres_orig->cd();
  cres_orig->Update();
  tgr_orig[0]->SetMarkerStyle(20);
  tgr_orig[0]->SetMarkerSize(0.2);
  tgr_orig[0]->SetMarkerColor(kRed);
  tgr_orig[0]->SetLineColor(kRed);
  tgr_orig[0]->Draw("alp");
  cres_orig->Update();
  TCanvas *c1_orig=new TCanvas();
  c1_orig->cd();
  c1_orig->Update();
  tg_loc->DrawClone("alp");
  //tgo->Draw("lp");
  c1_orig->Update();
  TCanvas *c1=new TCanvas();
  c1->cd();
  c1->Update();
  tg_loc->Draw("alp");
  c1->Update();

// Compute residuals
  hres->Reset();
  //tg_loc->Fit(f1,"Q","",0.,tmax);
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    //double dy=y-val;
    double dy=y-val-f1_cor->Eval(y/dv)*dv;
    hres->Fill(dy/dv);
    tg_res[0]->SetPoint(i,t,dy);
  }
  TCanvas *c2=new TCanvas();
  c2->cd();
  c2->Update();
  tg_res[0]->SetMarkerStyle(20);
  tg_res[0]->SetMarkerSize(0.2);
  tg_res[0]->SetMarkerColor(kRed);
  tg_res[0]->SetLineColor(kRed);
  tg_res[0]->Draw("alp");
  c2->Update();
  TCanvas *c3=new TCanvas();
  c3->cd();
  c3->Update();
  hres->SetLineColor(kRed);
  hres->DrawCopy();
  c3->Update();
  hres->Reset();

// Redo fit on requested samples (even, odd or both)
  tg_loc->Fit(f1,"Q","",0.,tmax);
  ped=f1_2->GetParameter(0);
  if(f>0.01)
  {
    amp=f1_2->GetParameter(1);
    f=f1_2->GetParameter(2);
    phaset=f1_2->GetParameter(3);
  }
  else
  {
    amp=1.;
    f=0.;
    phaset=0.;
  }
  printf("Selected fit, pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",ped,amp,f,phaset);

  c3->cd();
  hres->SetLineColor(kGreen);
  hres->DrawCopy("same");
  c3->Update();

  if(f>0.01)
  {
    f2->SetParameter(0,f1_2->GetParameter(1));
    f2->SetParameter(1,f1_2->GetParameter(2));
    f2->SetParameter(2,f1_2->GetParameter(3));
    f2->SetParameter(3,f1_2->GetParameter(4));
  }
  else
  {
    f2->SetParameter(0,0.);
    f2->SetParameter(1,0.);
    f2->SetParameter(2,0.);
    f2->SetParameter(3,pi);
  }

  hres->Reset();
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    //double dy=y-val;
    double dy=y-val-f1_cor->Eval(y/dv)*dv;
    hres->Fill(dy/dv);
  }
  double rms_cut=hres->GetRMS()*3.;
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    //double dy=y-val;
    double dy=y-val-f1_cor->Eval(y/dv)*dv;
// simulate noisy, jittery 12 bit ADC
    double t_MC=t+tr->Gaus(0.,apperture_jitter);
    double val_MC=f1->Eval(t_MC);
    val_MC+=tr->Gaus(0.,thermal_noise);
    val_MC=int((val_MC+dv/2.)/dv)*dv;
    tg_MC[0]->SetPoint(i,t,val_MC);
    if(MC==1) dy=val_MC-val;
    tg_res[0]->SetPoint(i,t,dy);
    double dvdt=fabs(f2->Eval(t));
    if(fabs(dy<3.*rms_cut))
    {
      tpi->Fill(y/dv,dy/dv);
      tps->Fill(y,dy);
      tpv->Fill(dvdt,dy);
    }
  }
  int nbin=tps->GetNbinsX();
  for(int i=0; i<nbin; i++)
  {
    double rms=tps->GetBinError(i+1);
    hs->SetBinContent(i+1,rms);
  }
  nbin=tpv->GetNbinsX();
  int nref=0;
  double dvref=tpv->GetBinError(nref+1);
  for(int i=0; i<nbin; i++)
  {
    double rms=tpv->GetBinError(i+1);
    hv->SetBinContent(i+1,rms);
  }

  c2->cd();
  tg_res[0]->SetMarkerColor(kBlue);
  tg_res[0]->SetLineColor(kBlue);
  tg_res[0]->Draw("lp");
  c2->Update();
  c3->cd();
  hres->SetLineColor(kBlue);
  hres->GetXaxis()->SetTitle("#Delta [ADC count]");
  hres->DrawCopy("same");
  printf("residual RMS : %f lsb, %f mV, enob = %.2f bits\n",hres->GetRMS(),hres->GetRMS()*dv*1000., log(1.2/(hres->GetRMS()*dv*sqrt(12)))/log(2));
  c3->Update();
  TCanvas *c4=new TCanvas("INL_DNL","INL_DNL", 800,1080);
  c4->Divide(1,2);
  c4->cd(1);
  tpi->SetLineColor(kBlue);
  tpi->SetMaximum(5.);
  tpi->SetMinimum(-5.);
  tpi->Draw("");
  gPad->SetGridx();
  gPad->SetGridy();
  tpi->GetXaxis()->SetTitle("amplitude [lsb]");
  tpi->GetXaxis()->SetTitleSize(0.05);
  tpi->GetXaxis()->SetTitleFont(62);
  tpi->GetXaxis()->SetLabelSize(0.05);
  tpi->GetXaxis()->SetLabelFont(62);
  tpi->GetYaxis()->SetTitle("INL [lsb]");
  tpi->GetYaxis()->SetTitleOffset(0.9);
  tpi->GetYaxis()->SetTitleSize(0.05);
  tpi->GetYaxis()->SetTitleFont(62);
  tpi->GetYaxis()->SetLabelSize(0.05);
  tpi->GetYaxis()->SetLabelFont(62);
  double y_old=tpi->GetBinContent(1);
  double ey=tpi->GetBinError(1);
  tpd->SetBinContent(1,y_old);
  tpd->SetBinError(1,ey);
  for(int i=1; i<4096; i++)
  {
    double y=tpi->GetBinContent(i+1);
    double ey=tpi->GetBinError(i+1);
    tpd->SetBinContent(i+1,y-y_old);
    tpd->SetBinError(i+1,ey);
    y_old=y;
  }
  c4->cd(2);
  tpd->SetLineColor(kBlue);
  tpd->SetMaximum(5.);
  tpd->SetMinimum(-5.);
  tpd->Draw("");
  gPad->SetGridx();
  gPad->SetGridy();
  tpd->GetXaxis()->SetTitle("amplitude [lsb]");
  tpd->GetXaxis()->SetTitleSize(0.05);
  tpd->GetXaxis()->SetTitleFont(62);
  tpd->GetXaxis()->SetLabelSize(0.05);
  tpd->GetXaxis()->SetLabelFont(62);
  tpd->GetYaxis()->SetTitle("DNL [lsb]");
  tpd->GetYaxis()->SetTitleOffset(0.9);
  tpd->GetYaxis()->SetTitleSize(0.05);
  tpd->GetYaxis()->SetTitleFont(62);
  tpd->GetYaxis()->SetLabelSize(0.05);
  tpd->GetYaxis()->SetLabelFont(62);

  c4->Update();
  TCanvas *c5=new TCanvas();
  hs->SetLineColor(kBlue);
  hs->Draw("");
  c5->Update();
  TCanvas *c6=new TCanvas();
  TF1 *f3=new TF1("f3","sqrt([0]*[0]+[1]*[1]*x*x)",0.,1.e9);
  f3->SetParameter(0,0.0004);
  f3->SetParameter(1,16.e-12);
  hv->SetLineColor(kBlue);
  hv->Fit(f3,"","");
  hv->SetMinimum(0.);
  hv->SetMaximum(0.0025);
  hv->GetXaxis()->SetRangeUser(0.,250.e6);
  gPad->SetGridx();
  gPad->SetGridy();
  hv->GetXaxis()->SetLabelSize(0.05);
  hv->GetXaxis()->SetLabelFont(62);
  hv->GetXaxis()->SetTitleSize(0.05);
  hv->GetXaxis()->SetTitleFont(62);
  hv->GetXaxis()->SetTitle("dV/dt [V/s]");
  hv->GetYaxis()->SetLabelSize(0.04);
  hv->GetYaxis()->SetLabelFont(62);
  hv->GetYaxis()->SetTitleSize(0.05);
  hv->GetYaxis()->SetTitleFont(62);
  hv->GetYaxis()->SetTitleOffset(1.00);
  hv->GetYaxis()->SetTitle("measurement error [V]");
  double th_noise=f3->GetParameter(0);
  printf("Thermal noise : %f, enob = %.2f bits\n",th_noise,log(1.2/(th_noise*sqrt(12)))/log(2));
  c6->Update();
  
  TF1 *fHann=new TF1("Hann","(x>0 && x<[0])?0.5-0.5*cos(2*3.141592654*x/[0]):0.",0.,1.e-3);
  TF1 *fHamming=new TF1("Hamming","(x>0 && x<[0])?0.54-0.46*cos(2*3.141592654*x/[0]):0.",0.,1.e-3);
  TF1 *fflat=new TF1("Flat","(x>0 && x<[0])?1.:0.",0.,1.e-3);
  double Hann_cor=1.63;
  double flat_cor=1.00;
  double Hamming_cor=1.59;
  fHann->SetParameter(0,tmax);
  fHamming->SetParameter(0,tmax);
  fflat->SetParameter(0,tmax);
  int size = n_loc;
  //TVirtualFFT *fft_f = TVirtualFFT::FFT(1, &size, "C2CF M K");
  TVirtualFFT *fft_f = TVirtualFFT::FFT(1, &size, "C2CF K  ");
  Int_t size2=16384;
  TVirtualFFT *fft_f2 = TVirtualFFT::FFT(1, &size2, "C2CF K  ");
  TCanvas *cmod=new TCanvas();
  TCanvas *cphase=new TCanvas();
  Double_t *mod=(double *)malloc(n_loc*sizeof(double));
  Double_t *phase=(double *)malloc(n_loc*sizeof(double));
  Double_t *rex=(double *)malloc(n_loc*sizeof(double));
  Double_t *imx=(double *)malloc(n_loc*sizeof(double));
  Double_t *rey=(double *)malloc(n_loc*sizeof(double));
  Double_t *imy=(double *)malloc(n_loc*sizeof(double));
  for(int i=0; i<n_loc; i++)
  {
    double t,y;
    tg_res[0]->GetPoint(i,t,y);
    if(window_type==0)
      rex[i]=y*fHann->Eval(t);
    else if(window_type==1)
      rex[i]=y*fHamming->Eval(t);
    else if(window_type==2)
      rex[i]=y*fflat->Eval(t);
    imx[i]=0.;
  }
  fft_f->SetPointsComplex(rex, imx);
  fft_f->Transform();
  fft_f->GetPointsComplex(rey, imy);

  double df=1./tmax;
  double fmax=n_loc*df;
  TH1D *hmod=new TH1D("noise_modulus","noise_modulus",n_loc,0.,fmax);
  TH1D *hphase=new TH1D("noise_phase","noise_phase",n_loc,0.,fmax);
  double rms_fft=0.;
  for(int i=0; i<n_loc; i++)
  {
    Double_t f=df*i;
    rey[i]/=n_loc;
    imy[i]/=n_loc;
    if(window_type==0)
      mod[i]=sqrt((rey[i]*rey[i]+imy[i]*imy[i])/df)*Hann_cor;
    else if(window_type==1)
      mod[i]=sqrt((rey[i]*rey[i]+imy[i]*imy[i])/df)*Hamming_cor;
    else if(window_type==2)
      mod[i]=sqrt((rey[i]*rey[i]+imy[i]*imy[i])/df)*flat_cor;
    phase[i]=atan2(imy[i],rey[i]);
    //if(i>0 && fabs(f-40.e6)>0.1e6)rms_fft+=mod[i]*mod[i]*df;
    if(i>0)rms_fft+=mod[i]*mod[i]*df;
    hmod->SetBinContent(i+1,mod[i]);
    hphase->SetBinContent(i+1,phase[i]);
  }
  rms_fft=sqrt(rms_fft);
  cmod->cd();
  hmod->Draw();
  hmod->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  hmod->GetXaxis()->SetLabelSize(0.05);
  hmod->GetXaxis()->SetLabelFont(62);
  hmod->GetXaxis()->SetTitleSize(0.05);
  hmod->GetXaxis()->SetTitleFont(62);
  hmod->GetXaxis()->SetTitle("frequency [Hz]");
  hmod->GetYaxis()->SetLabelSize(0.04);
  hmod->GetYaxis()->SetLabelFont(62);
  hmod->GetYaxis()->SetTitleSize(0.05);
  hmod->GetYaxis()->SetTitleFont(62);
  hmod->GetYaxis()->SetTitleOffset(1.00);
  hmod->GetYaxis()->SetTitle("noise [V/#sqrt{Hz}]");
  printf("RMS from noise FFT : %f, enob = %.2f bits\n",rms_fft,log(1.2/(rms_fft*sqrt(12)))/log(2));
  cmod->Update();
  cphase->cd();
  hphase->Draw();
  hphase->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  cphase->Update();

  TH1D *hmods=new TH1D("signal_modulus","signal_modulus",size,0.,fmax);
  //nevt=1;
  for(ievt=0; ievt<nevt; ievt++)
  {
    tdata->GetEntry(ievt);
    for(int ich=0; ich<4; ich++)
    {
      Int_t nsample_loc=nsample_sep;
      for(int is=0; is<nsample_loc; is++)
      {
        event[ich][is]=dv*sevent[ich][is];
      }
    }
    for(int ich=0; ich<2; ich++)
    {
      Int_t nsample_loc=nsample_sep;
      for(int is=0; is<nsample_loc; is++)
      {
        if(ich==1)
        {
          tg[0]->SetPoint(2*is,dt*2.*is,event[ich][is]);
          tg[0]->SetPointError(2*is,5.e-12,dv);
        }
        else
        {
          tg[0]->SetPoint(2*is+1,dt*(2*is+1),event[ich][is]);
          tg[0]->SetPointError(2*is+1,5.e-12,dv);
        }
      }
    }
    for(int i=0; i<size; i++)
    {
      double t,y;
      //tg_loc->GetPoint(i,t,y);
      tg[0]->GetPoint(i,t,y);
      double val=f1->Eval(t);
      //double dy=y-val;
      y-=f1_cor->Eval(y/dv)*dv;
      //if(MC==1) tg_MC[0]->GetPoint(i,t,y);
      if(window_type==0)
        rex[i]=y*fHann->Eval(t);
      else if(window_type==1)
        rex[i]=y*fHamming->Eval(t);
      else if(window_type==2)
        rex[i]=y*fflat->Eval(t);
      imx[i]=0.;
      rey[i]=0.;
      imx[i]=0.;
    }
    fft_f->SetPointsComplex(rex, imx);
    fft_f->Transform();
    fft_f->GetPointsComplex(rey, imy);
    for(int i=0; i<size; i++)
    {
      Double_t f=df*(0.5+i);
      rey[i]/=size;
      imy[i]/=size;
      if(window_type==0)
        mod[i]=sqrt(rey[i]*rey[i]+imy[i]*imy[i])*Hann_cor;
      else if(window_type==1)
        mod[i]=sqrt(rey[i]*rey[i]+imy[i]*imy[i])*Hamming_cor;
      else if(window_type==2)
        mod[i]=sqrt(rey[i]*rey[i]+imy[i]*imy[i])*flat_cor;
      hmods->Fill(f,mod[i]*mod[i]);
      //hmods->SetBinContent(i+1,mod[i]);
    }
  }

  double mod_max=-1000.;
  int imax=0;
  double rms_signal=0.;
  double rms_noise=0.;
  for(int i=0; i<size; i++)
  {
    mod[i]=sqrt(hmods->GetBinContent(i+1)/nevt);
    if(i>2 && mod[i]>mod_max && i<size/2)
    {
      mod_max=mod[i];
      imax=i;
    }
  }
  printf("mod_max : %d %e %e\n",imax, df*imax, mod_max);
  hmods->Reset();
  for(int i=0; i<=size/2; i++)
  {
    if(i>2 && fabs(i-imax)>=10)rms_noise+=2.*mod[i]*mod[i]*df;
    if(fabs(i-imax)<10)rms_signal+=2.*mod[i]*mod[i]*df;
    //printf("%d %d %e %e\n",i,imax,mod[i],sqrt(rms_fft));
    mod[i]=20.*log10(mod[i]/mod_max);
    hmods->SetBinContent(i+1,mod[i]);
  }
  rms_signal=10*log10(rms_signal);
  rms_noise =10*log10(rms_noise);
  //printf("RMS from FFT : signal =%e, noise =%e\n",rms_signal,log(1.2/(rms_fft*sqrt(12)))/log(2));
  printf("RMS from FFT : signal = %.2f dB, noise = %.2f dB, SNDR = %.2f dB, enob = %.2f\n",rms_signal,rms_noise, rms_signal-rms_noise, (rms_signal-rms_noise-1.76)/6.02);

  TCanvas *csig=new TCanvas();
  hmods->Draw();
  hmods->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  hmods->GetXaxis()->SetLabelSize(0.05);
  hmods->GetXaxis()->SetLabelFont(62);
  hmods->GetXaxis()->SetTitleSize(0.05);
  hmods->GetXaxis()->SetTitleFont(62);
  hmods->GetXaxis()->SetTitle("frequency [Hz]");
  hmods->GetYaxis()->SetLabelSize(0.04);
  hmods->GetYaxis()->SetLabelFont(62);
  hmods->GetYaxis()->SetTitleSize(0.05);
  hmods->GetYaxis()->SetTitleFont(62);
  hmods->GetYaxis()->SetTitleOffset(1.00);
  hmods->GetYaxis()->SetTitle("normalized modulus [dB]");
  csig->Update();


  char fnout[132];
  strcpy(fnout,fname.Data());
  char *pos=strstr(fnout,".root");
  sprintf(pos,"_analized.root");
  TFile *tfout=new TFile(fnout,"recreate");
  tge[0]->Write();
  tge_orig[0]->Write();
  tgo[0]->Write();
  tg_MC[0]->Write();
  tg[0]->Write();
  tg_res[0]->Write();
  tgr_orig[0]->Write();
  tpi->Write();
  tpd->Write();
  tpv->Write();
  hs->Write();
  hv->Write();
  hres->Write();
  hmod->Write();
  hmods->Write();
  tfout->Close();
}
