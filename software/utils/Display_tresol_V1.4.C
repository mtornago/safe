void Display_tresol()
{
  double N[5]={0.92,0.87,0.79,0.89,1.03};
  double q[7]={50.,100.,200.,300.,375.,400.,420.};
  double dt[4][7]={{ 42.,20.9,10.7, 6.5, 5.8, 5.7, 5.5}, // R340-27pF-LPF50
                   { 44.,21.0,10.0, 6.9, 5.5, 5.2, 5.1}, // R340-27pF-LPF35
                   { 45.,20.5,10.6, 6.9, 5.6, 5.5, 5.2}, // R340-47pF-LPF50
                   { 44.,20.5,10.8, 7.1, 5.7, 5.6, 5.4}};// R340-47pF-LPF35
  TGraph *tg[4];
  tg[0]=new TGraph(7,q,dt[0]);
  tg[1]=new TGraph(7,q,dt[1]);
  tg[2]=new TGraph(7,q,dt[2]);
  tg[3]=new TGraph(7,q,dt[3]);
  tg[0]->SetMarkerStyle(20);
  tg[0]->SetMarkerSize(1.0);
  tg[0]->SetMarkerColor(kRed);
  tg[0]->SetLineColor(kRed);
  tg[0]->SetMaximum(50.);
  tg[0]->SetMinimum(0.);
  tg[0]->Draw("alp");
  tg[0]->GetXaxis()->SetLimits(0.,500.);
  tg[0]->Draw("alp");
  tg[1]->SetMarkerStyle(20);
  tg[1]->SetMarkerSize(1.0);
  tg[1]->SetMarkerColor(kMagenta);
  tg[1]->SetLineColor(kMagenta);
  tg[1]->Draw("lp");
  tg[2]->SetMarkerStyle(20);
  tg[2]->SetMarkerSize(1.0);
  tg[2]->SetMarkerColor(kBlue);
  tg[2]->SetLineColor(kBlue);
  tg[2]->Draw("lp");
  tg[3]->SetMarkerStyle(20);
  tg[3]->SetMarkerSize(1.0);
  tg[3]->SetMarkerColor(kCyan);
  tg[3]->SetLineColor(kCyan);
  tg[3]->Draw("lp");
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  tl->AddEntry(tg[0],"R340-LPF50-27pF","lp");
  tl->AddEntry(tg[1],"R340-LPF35-27pF","lp");
  tl->AddEntry(tg[2],"R340-LPF50-47pF","lp");
  tl->AddEntry(tg[3],"R340-LPF35-47pF","lp");
  tl->Draw();
}
