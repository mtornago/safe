void tresol_VFE()
{
  TCanvas *c1=new TCanvas();
  TCanvas *c2=new TCanvas();
  TCanvas *c3=new TCanvas();
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  TLegend *tl2=new TLegend();
  tl2->SetFillStyle(0);
  Int_t n10=10;
  //Double_t q10[10]={85.,210.,420.,525.,630.,735.,840.,945.,1050.,1155.};
  //Double_t q10n[10]={85.,210.,420.,525.,630.,735.,840.,945.,1050.,1155.};
  Double_t q10[10]={ 85.,200.,405.,510.,610.,715.,815.,920.,1020.,1120.};
  Double_t q10n[10];
  Double_t q10e[10];
  //Int_t n1=13;
  Int_t n1=17;
  //Double_t q1[13] ={40.,50.,60.,70.,80.,90.,100.,110.,120.,130.,140.,150.,160.};
  Double_t q1[17] ={39.,48.,58.,68.,77.,87., 96.,106.,115.,125.,134.,144.,154.,163.,173.,183.,192.};
  Double_t q1n[17];
  Double_t q1e[17];
  // shape from each 150 uA pulse
  Double_t st10[10]={124.1,49.0,24.4,19.9,16.5,14.2,12.6,11.3,10.2,9.2};
  Double_t st10_80[10]={142.8,58.0,29.2,23.4,19.9,16.9,15.0,13.3,11.8,10.8};
  Double_t st1[17] ={73.4,58.6,49.0,42.0,37.3,33.1,29.4,26.9,24.7,22.5,21.4,20.1,18.7,17.4,16.4,15.7,14.9};
  Double_t noise_10=0.8; 
  Double_t noise_1=0.28;
  for(Int_t i=0; i<n10; i++)
  {
    st10[i]/=sqrt(2.);
    st10_80[i]/=sqrt(2.);
    q10n[i]=q10[i]/noise_10;
    q10e[i]=q10[i]/6.;
  }
  for(Int_t i=0; i<n1; i++)
  {
    st1[i]/=sqrt(2.);
    q1n[i]=q1[i]/noise_1;
    q1[i]=q1[i]*10.5;
    q1e[i]=q1[i]/6.;
  }
  TGraph *tg10_80=new TGraph(n10,q10,st10_80);
  TGraph *tg10=new TGraph(n10,q10,st10);
  TGraph *tg1=new TGraph(n1,q1,st1);
  TGraph *tg1e=new TGraph(n1,q1e,st1);
  TGraph *tg10n=new TGraph(n10,q10n,st10);
  TGraph *tg10e=new TGraph(n10,q10e,st10);
  TGraph *tg1n=new TGraph(n1,q1n,st1);
  tg10n->SetLineColor(kBlue);
  tg10n->SetLineWidth(2.);
  tg10n->SetMarkerColor(kBlue);
  tg10n->SetMarkerSize(1.5);
  tg10n->SetMarkerStyle(20);
  tg10n->Draw("alp");
  tg10n->GetXaxis()->SetLimits(0,1500.);
  tg10n->GetXaxis()->SetTitle("normalized amplitude Q/N");
  tg10n->GetYaxis()->SetTitle("resolution [ps]");
  tg10n->GetYaxis()->SetTitleFont(62);
  tg10n->GetYaxis()->SetTitleSize(0.05);
  tg10n->GetYaxis()->SetTitleOffset(1.);
  tg10n->GetYaxis()->SetLabelSize(0.05);
  tg10n->GetXaxis()->SetLabelFont(62);
  tg10n->GetXaxis()->SetLabelSize(0.05);
  tg10n->GetXaxis()->SetTitleSize(0.05);
  tg10n->SetMaximum(100.);
  tg10n->SetMinimum(5.);
  tg10n->SetTitle("Single channel timing resolution vs normalized amplitude");
  tg10e->SetLineColor(kBlue);
  tg10e->SetLineWidth(2.);
  tg10e->SetMarkerColor(kBlue);
  tg10e->SetMarkerSize(1.5);
  tg10e->SetMarkerStyle(20);
  tg10e->Draw("alp");
  tg10e->GetXaxis()->SetLimits(0,400.);
  tg10e->GetXaxis()->SetTitle("deposited energy [GeV]");
  tg10e->GetYaxis()->SetTitle("resolution [ps]");
  tg10e->GetYaxis()->SetTitleFont(62);
  tg10e->GetYaxis()->SetTitleSize(0.05);
  tg10e->GetYaxis()->SetTitleOffset(1.);
  tg10e->GetYaxis()->SetLabelSize(0.05);
  tg10e->GetXaxis()->SetLabelFont(62);
  tg10e->GetXaxis()->SetLabelSize(0.05);
  tg10e->GetXaxis()->SetTitleSize(0.05);
  tg10e->SetMaximum(100.);
  tg10e->SetMinimum(5.);
  tg10e->SetTitle("Single channel timing resolution vs energy");
  tg10->SetLineColor(kBlue);
  tg10->SetLineWidth(2.);
  tg10->SetMarkerColor(kBlue);
  tg10->SetMarkerSize(1.5);
  tg10->SetMarkerStyle(20);
  tg10->Draw("alp");
  tg10->GetXaxis()->SetLimits(0,2100.);
  tg10->GetXaxis()->SetTitle("amplitude [mV]");
  tg10->GetYaxis()->SetTitle("resolution [ps]");
  tg10->GetYaxis()->SetTitleFont(62);
  tg10->GetYaxis()->SetTitleSize(0.05);
  tg10->GetYaxis()->SetTitleOffset(1.);
  tg10->GetYaxis()->SetLabelSize(0.05);
  tg10->GetXaxis()->SetLabelFont(62);
  tg10->GetXaxis()->SetLabelSize(0.05);
  tg10->GetXaxis()->SetTitleSize(0.05);
  tg10->SetMaximum(100.);
  tg10->SetMinimum(5.);
  tg10->SetTitle("Single channel timing resolution vs amplitude");
  tg10_80->SetLineColor(kCyan);
  tg10_80->SetLineWidth(2.);
  tg10_80->SetMarkerColor(kCyan);
  tg10_80->SetMarkerSize(1.5);
  tg10_80->SetMarkerStyle(20);
  tg10_80->Draw("alp");
  tg10_80->GetXaxis()->SetLimits(0,2100.);
  tg10_80->GetXaxis()->SetTitle("amplitude [mV]");
  tg10_80->GetYaxis()->SetTitle("resolution [ps]");
  tg10_80->GetYaxis()->SetTitleFont(62);
  tg10_80->GetYaxis()->SetTitleSize(0.05);
  tg10_80->GetYaxis()->SetTitleOffset(1.);
  tg10_80->GetYaxis()->SetLabelSize(0.05);
  tg10_80->GetXaxis()->SetLabelFont(62);
  tg10_80->GetXaxis()->SetLabelSize(0.05);
  tg10_80->GetXaxis()->SetTitleSize(0.05);
  tg10_80->SetMaximum(150.);
  tg10_80->SetMinimum(5.);
  tg10_80->SetTitle("Single channel timing resolution vs amplitude");
  tg10->Draw("alp");
  tg10_80->Draw("alp");
  tg10n->Draw("alp");
  TF1 *f1=new TF1("f1","sqrt([0]*[0]/x/x+[1]*[1])",1.,5000.);
  f1->SetParameter(0.,10000.);
  f1->SetParameter(1.,2.);
  tg10n->Fit("f1","","");
  tg1e->SetLineColor(kGreen);
  tg1e->SetLineWidth(2.);
  tg1e->SetMarkerColor(kGreen);
  tg1e->SetMarkerSize(1.5);
  tg1e->SetMarkerStyle(20);
  tg1e->Draw("alp");
  tg1e->GetXaxis()->SetLimits(0,400.);
  tg1e->GetXaxis()->SetTitle("deposited energy [GeV]");
  tg1e->GetYaxis()->SetTitle("resolution [ps]");
  tg1e->GetYaxis()->SetTitleFont(62);
  tg1e->GetYaxis()->SetTitleSize(0.05);
  tg1e->GetYaxis()->SetTitleOffset(1.);
  tg1e->GetYaxis()->SetLabelSize(0.05);
  tg1e->GetXaxis()->SetLabelFont(62);
  tg1e->GetXaxis()->SetLabelSize(0.05);
  tg1e->GetXaxis()->SetTitleSize(0.05);
  tg1e->SetMaximum(100.);
  tg1e->SetMinimum(5.);
  tg1n->SetLineColor(kGreen);
  tg1n->SetLineWidth(2.);
  tg1n->SetMarkerColor(kGreen);
  tg1n->SetMarkerSize(1.5);
  tg1n->SetMarkerStyle(20);
  tg1n->Draw("alp");
  tg1n->GetXaxis()->SetLimits(0,1500.);
  tg1n->GetXaxis()->SetTitle("normalized amplitude Q/N");
  tg1n->GetYaxis()->SetTitle("resolution [ps]");
  tg1n->GetYaxis()->SetTitleFont(62);
  tg1n->GetYaxis()->SetTitleSize(0.05);
  tg1n->GetYaxis()->SetTitleOffset(1.);
  tg1n->GetYaxis()->SetLabelSize(0.05);
  tg1n->GetXaxis()->SetLabelFont(62);
  tg1n->GetXaxis()->SetLabelSize(0.05);
  tg1n->GetXaxis()->SetTitleSize(0.05);
  tg1n->SetMaximum(100.);
  tg1n->SetMinimum(5.);
  tg1n->Fit("f1","","");
  tg1n->SetTitle("Single channel timing resolution vs normalized amplitude");
  tg1->SetLineColor(kGreen);
  tg1->SetLineWidth(2.);
  tg1->SetMarkerColor(kGreen);
  tg1->SetMarkerSize(1.5);
  tg1->SetMarkerStyle(20);
  tg1->Draw("alp");
  tg1->GetXaxis()->SetLimits(0,1800.);
  tg1->GetXaxis()->SetTitle("normalized amplitude Q/N");
  tg1->GetYaxis()->SetTitle("resolution [ps]");
  tg1->GetYaxis()->SetTitleFont(62);
  tg1->GetYaxis()->SetTitleSize(0.05);
  tg1->GetYaxis()->SetTitleOffset(1.);
  tg1->GetYaxis()->SetLabelSize(0.05);
  tg1->GetXaxis()->SetLabelFont(62);
  tg1->GetXaxis()->SetLabelSize(0.05);
  tg1->GetXaxis()->SetTitleSize(0.05);
  tg1->SetMaximum(100.);
  tg1->SetMinimum(5.);
  tg1->SetTitle("Single channel timing resolution vs amplitude");
  c1->cd();
  tg10n->Draw("alp");
  tg1n->Draw("lp");
  tl->AddEntry(tg10n,"G10 data","lp");
  tl->AddEntry(tg1n,"G1 data","lp");
  tl->Draw();
  c2->cd();
  tg10e->Draw("alp");
  tg1e->Draw("lp");
  tl->Draw();
  c2->Update();
  c3->cd();
  tg10_80->Draw("alp");
  gPad->SetLogx();
  gPad->SetLogy();
  tg10->Draw("lp");
  tl2->AddEntry(tg10,"Sampling at 160 MHz","lp");
  tl2->AddEntry(tg10_80,"Sampling at 80 MHz","lp");
  tl2->Draw();
  c3->Update();
}
