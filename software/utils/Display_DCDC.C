void Display_DCDC()
{
  TString fname1="data/DCDC/C1FEAST_2.5V_2MSps_00000.txt.analyzed.root";
  TString fname2="data/DCDC/C1LHC4913_2.5V_2MSps_00000.txt.analyzed.root";
  TString fname3="data/DCDC/C1LDO_2.5V_2MSps_00000.txt.analyzed.root";
  TFile *fd1=new TFile(fname1);
  TFile *fd2=new TFile(fname2);
  TFile *fd3=new TFile(fname3);
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  TH1D *h1;
  TCanvas *c1=new TCanvas();
  fd1->cd();
  gDirectory->GetObject("noise_modulus",h1);
  h1->Draw();
  gPad->SetGridx();
  gPad->SetGridy();
  gPad->SetLogx();
  //h1->GetXaxis()->SetRangeUser(1.,12.5e6);
  h1->GetXaxis()->SetRangeUser(10.,1.e6);
  h1->SetLineColor(kRed);
  h1->SetMaximum(25.);
  h1->Draw();
  tl->AddEntry(h1,"FEAST noise density spectrum");
  fd2->cd();
  gDirectory->GetObject("noise_modulus",h1);
  h1->SetLineColor(kGreen);
  h1->Draw("same");
  tl->AddEntry(h1,"LHC4913 noise density spectrum");
  fd3->cd();
  gDirectory->GetObject("noise_modulus",h1);
  h1->SetLineColor(kBlue);
  h1->Draw("same");
  tl->AddEntry(h1,"ADP7158 noise density spectrum");
  tl->Draw();
}
