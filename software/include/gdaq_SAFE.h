#ifndef EXTERN
  #define EXTERN
  #define EXTERNC
#else
  #define EXTERNC extern "C"
#endif

#include "gtk/gtk.h"
#include "uhal/uhal.hpp"
#include <signal.h>
#include "limits.h"

#include "I2C_RW.h"
#include "LiTEDTU_def.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TStyle.h>
#include <TRandom.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TTree.h>
#include <TF1.h>
#include <TFile.h>
#include <TVirtualFFT.h>

#define I2C_SHIFT_DEV_NUMBER   3
#define N_VFE                  5
#define N_CHANNEL              25
#define N_LMB_ADC              3

// FW_VER
#define GENE_TRIGGER           (1<<0)
#define GENE_TP                (1<<1)
#define GENE_WTE               (1<<2)
#define LED_ON                 (1<<3)
#define TP_MODE                (1<<4)
#define GENE_BC0               (1<<5)
#define GENE_RESYNC            (1<<7)
#define GENE_AWG               (1<<8)
#define CRC_RESET              (1<<31)

// SAFE_CTRL
#define RESET                  (1<<0)
#define FIFO_MODE              (1<<1)
#define SELF_TRIGGER_LOOP      (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MODE      (1<<31)
#define BOARD_SN               (1<<28)

// TRIG_DELAY
#define HW_DAQ_DELAY           (1<<0)   // Laser with external trigger
#define SW_DAQ_DELAY           (1<<16)  // delay for laser with external trigger

// VFE_CTRL
#define DTU_CALIB_MODE         (1<<0)
#define DTU_TEST_MODE          (1<<1)
#define DTU_MEM_MODE           (1<<2)
#define I2C_BUSY               (1<<3)
#define I2C_ERROR              (1<<4)
#define VFE_LV                 (0x3F<<16)
#define I2C_LPGBT_MODE         (1<<27)
#define BULKY_I2C_DTU          (1<<28)
#define PWUP_RESETB            (1<<31)

// CALIB_CTRL
// TRIGGER_MASK
#define SELF_TRIGGER_MASK      (1<<0)

//CLK_SETTING
#define eLINK_ACTIVE           (1<<0)
#define RESYNC_PHASE           (1<<25)

// DELAY_CTRL1
#define START_IDELAY_SYNC      (1<<0)     // Force IDELAY tuning to get the eye center of each stream when idle patterns are ON (25 bit pattern)
#define DELAY_TAP_DIR          (1<<25)    // Increase (1) or decrease (0) tap delay in ISERDES
#define DELAY_RESET            (1<<26)    // Reset ISERDES delay
#define IO_RESET               (1<<27)    // Reset ISERDES IO

// DRP_XADC
#define XADC_DATA              (1<<0)
#define XADC_ADDR              (1<<16)
#define DRP_WRb                (1<<31)

// I2C_BUS
#define I2C_BUS                (1<<0)
// I2C_CTRL
#define I2C_DATA               (1<<0)
#define I2C_ADDR               (1<<16)
#define I2C_LONG               (1<<30)
#define I2C_RWB                (1<<31)

// RESYNC_IDLE
#define RESYNC_IDLE_PATTERN    (1<<0)

// CAP_CTRL
#define CAPTURE_START          (1<<0)
#define CAPTURE_STOP           (1<<1)
#define NSAMPLE_MAX            4096
#define MAX_PAYLOAD            1380
#define MAX_VFE                5
#define MAX_STEP               4096



#define NSAMPLE_PED            5
#define N_ADC_REG              76

using namespace uhal;

typedef struct
{
  GtkWindow *main_window;
  GtkWindow *channel_window;
  GtkWindow *DAQ_window;
  GtkWindow *trigger_window;
  GtkWindow *actions_window;
  GtkWindow *results_window;
  GtkWindow *map_window;
  GtkBuilder *builder;
  gpointer user_data;
  gint window_xpos0;
  gint window_ypos0;
  gint window_xoffset;
  gint window_yoffset;
  gint window_width[7];
  gint window_height[7];
} SGlobalData;
EXTERN SGlobalData gtk_data;

typedef struct
{
  Int_t CATIA_version[N_CHANNEL], CATIA_Vref_val[N_CHANNEL], CATIA_ped_G10[N_CHANNEL], CATIA_ped_G1[N_CHANNEL], CATIA_DAC1_val[N_CHANNEL], CATIA_DAC2_val[N_CHANNEL];
  Int_t CATIA_gain[N_CHANNEL], CATIA_LPF35[N_CHANNEL], CATIA_Rconv[N_CHANNEL], CATIA_temp_out[N_CHANNEL], CATIA_temp_X5[N_CHANNEL];
  Int_t CATIA_Vref_out[N_CHANNEL], CATIA_DAC_buf_off[N_CHANNEL], CATIA_XADC_channel[N_CHANNEL];
  Int_t CATIA_reg_def[N_CHANNEL][7], CATIA_reg_loc[N_CHANNEL][7];
  Int_t DTU_version[N_CHANNEL], DTU_BL_G10[N_CHANNEL], DTU_BL_G1[N_CHANNEL], DTU_PLL_conf[N_CHANNEL], DTU_PLL_conf1[N_CHANNEL], DTU_PLL_conf2[N_CHANNEL];
  Int_t DTU_CRC_errors[N_CHANNEL], DTU_driver_current[N_CHANNEL],DTU_PE_width[N_CHANNEL], DTU_PE_strength[N_CHANNEL];
  Double_t CATIA_Temp_offset[N_CHANNEL], CATIA_Temp_val[N_CHANNEL], CATIA_Vref_ref[N_CHANNEL];
  Double_t rms[2][N_CHANNEL], INL[2][N_CHANNEL],gain[2][N_CHANNEL], ADC_dv;
  bool DTU_dither[N_CHANNEL], DTU_nsample_calib_x4[N_CHANNEL], DTU_global_test[N_CHANNEL], DTU_override_Vc_bit[N_CHANNEL], DTU_force_PLL[N_CHANNEL];
  bool DTU_VCO_rail_mode[N_CHANNEL], DTU_bias_ctrl_override[N_CHANNEL];
  bool DTU_force_G1[N_CHANNEL], DTU_force_G10[N_CHANNEL], DTU_G1_window16[N_CHANNEL], DTU_clk_out[N_CHANNEL], DTU_eLink[N_CHANNEL];
  bool CATIA_DAC1_status[N_CHANNEL], CATIA_DAC2_status[N_CHANNEL], CATIA_SEU_corr[N_CHANNEL];
  bool CATIA_status[N_CHANNEL], DTU_status[N_CHANNEL], channel_status[N_CHANNEL], VFE_status[N_VFE];
  Int_t I2C_address[N_CHANNEL], I2C_bus[N_CHANNEL];
} AsicSetting_s;
EXTERN AsicSetting_s asic;

typedef struct
{
  Int_t SAFE_number, firmware_version, nsample, nevent, numrun, cur_evt;
  Int_t n_active_channel, eLink_active, channel_number[N_CHANNEL], daq_initialized, current_channel;
  Int_t VFE_pattern, I2C_shift_dev_number, I2C_lpGBT_mode, WTE_pos, ReSync_pos;
  Int_t delay_auto_tune, odd_sample_shift;
  Int_t delay_val[N_CHANNEL];    // Number of delay taps to get a stable R/O with ADCs (160 MHz)
  Int_t bitslip_val[N_CHANNEL];  // Number of bit to slip with iserdes lines to get ADC values well aligned
  Int_t byteslip_val[N_CHANNEL]; // Number of byte to slip to aligned ADC data on 40 MHz clock
  Int_t old_read_address, shift_oddH_samples, shift_oddL_samples;
  Int_t I2C_DTU_nreg, buggy_DTU, DTU_test_mode;
  Int_t trigger_type, self_trigger, self_trigger_mode, self_trigger_threshold, self_trigger_mask, self_trigger_loop;
  Int_t fifo_mode;         // Use memory in FIFO mode or not
  Int_t sw_DAQ_delay, hw_DAQ_delay, resync_idle_pattern, resync_phase, resync_data;
  Int_t TP_step, n_TP_step, TP_width, TP_delay, TP_level;
  Int_t autorun, error;
  Int_t SEU_test_mode, SEU_current_limit, SEU_pattern;
  Int_t XADC_Temp[N_VFE], XADC_leak[N_VFE];
  Long_t timestamp;
  Short_t *event[N_CHANNEL], *gain[N_CHANNEL];
  Double_t *fevent[N_CHANNEL], ped_G1[N_CHANNEL], ped_G10[N_CHANNEL];
  Double_t Tsensor_divider;
  Int_t all_sample[N_CHANNEL], ns_g1[N_CHANNEL];
  bool CATIA_scan_Vref, use_ref_ADC_calib, CATIA_Vcal_out, MEM_mode, LV_ON, led_ON, use_LMB;
  bool dump_data, corgain, wait_for_ever, daq_running, calibration_running;
  bool debug_DAQ, debug_I2C, debug_GTK, debug_draw, zoom_draw;
  bool gen_BC0, gen_WTE;
  ValVector< uint32_t > mem;
  TGraph *tg[N_CHANNEL], *tg_g1[N_CHANNEL], *tg_LMB[N_LMB_ADC];
  TH1D *hmean[N_CHANNEL], *hrms[N_CHANNEL], *hdensity[N_CHANNEL];
  TProfile *pshape[MAX_STEP][N_CHANNEL];
  TCanvas *c1, *c2, *c3;
  TTree *tdata;
  TFile *fd;
  char last_fname[132];
  FILE *fout_SEU;
  char xml_filename[PATH_MAX], comment[1024];
} DAQSetting_s;
EXTERN DAQSetting_s daq;

using namespace uhal;

EXTERN struct timeval tv;
EXTERN void read_conf_from_xml_file(char*);
EXTERN void write_conf_to_xml_file(char*);
EXTERN Int_t synchronize_links(uhal::HwInterface, Int_t);
EXTERN void tune_pedestals(uhal::HwInterface, Int_t);
EXTERN std::vector<uhal::HwInterface> devices;
EXTERN void init_gDAQ();
EXTERN void init_VFEs();
EXTERN void reset_DTU_sync();
EXTERN void send_ReSync_command(UInt_t command);
EXTERN void calib_ADC();
EXTERN void synchronize_links();
EXTERN void dump_ADC_registers();
EXTERN void load_ADC_registers();
EXTERN UInt_t update_CATIA_reg(Int_t, Int_t);
EXTERN UInt_t update_LiTEDTU_reg(Int_t, Int_t, Int_t);
EXTERN void get_single_event();
EXTERN void take_run(void);
EXTERN void get_event(Int_t, Int_t);
EXTERN void get_single_event(Int_t);
EXTERN void optimize_pedestals(void);
EXTERN void end_of_run(void);
EXTERN void update_VFE_control(Int_t);
EXTERN void update_trigger(void);
EXTERN void update_DAQ_settings(void);
EXTERN void update_channel(void);
EXTERN void update_active_channel(void);
EXTERN void get_temp(void);
EXTERN void get_CRC_errors(int);
EXTERN void PLL_scan(void);
EXTERN void calibrate_FEMs(void);
EXTERN void ana_ped_Monacal(void);
EXTERN void linearity_Monacal(void);
EXTERN void ana_las_Monacal(void);
EXTERN void open_results();
EXTERN void update_results();

EXTERNC G_MODULE_EXPORT void delete_event(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void callback_about(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void my_exit(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void select_VFE(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void open_channel(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_channel(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void open_DAQ(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_DAQ(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void open_trigger(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_trigger(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void open_actions(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_actions(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_results(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void close_map(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void button_clicked(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void switch_toggled(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void Entry_modified(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void Entry_changed(GtkWidget*, gpointer);
EXTERNC G_MODULE_EXPORT void read_config(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void save_config(GtkMenuItem*, gpointer);
EXTERNC G_MODULE_EXPORT void save_config_as(GtkMenuItem*, gpointer);
