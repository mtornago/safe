// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Jun  5 15:27:57 2023
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 36 (Thirty Six)
// Command     : write_verilog -force -mode funcsim
//               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_9,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module gig_ethernet_pcs_pma_0
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_9 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_tx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_rx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    reset_sync5,
    powerdown,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input [0:0]reset_sync5;
  input powerdown;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int),
        .reset_sync5_0(reset_sync5));
  gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int));
  gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(reset_sync5));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(reset_sync5));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(reset_sync5));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(reset_sync5));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(reset_sync5));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(reset_sync5));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(reset_sync5));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(reset_sync5));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(reset_sync5));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(reset_sync5));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(reset_sync5));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(reset_sync5));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(reset_sync5));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(reset_sync5));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(reset_sync5));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(reset_sync5));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(reset_sync5));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(reset_sync5));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(reset_sync5));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(reset_sync5));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(reset_sync5));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(reset_sync5));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(reset_sync5));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(reset_sync5));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(reset_sync5));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(reset_sync5));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(reset_sync5));
  gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(SR));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(SR));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(SR));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(SR));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(SR));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(SR));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(SR));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(SR));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(SR));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(SR));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(SR));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(SR));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(SR));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(SR));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(SR));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(SR));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(SR));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(SR));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(SR));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(SR));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(SR));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(SR));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(SR));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(SR));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(SR));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(SR));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(SR));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(SR));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(SR));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(SR));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(SR));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(SR));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(SR));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
kYrcO/E+Jhm4R/4R3+CukKYR9M2FIvcsEHYDIEQ941LV/qe3nw66ouV0tjU2K77WxMp0KzE3bUaN
EkHZUhS54Zbapq0AAlHGThTWWu9TToic0Fogfo0uxbTRj/YKvsYbGHXn+38UtVT4gl+Z+q34s2Mx
S+RksJLLbqa/UjuB2IA=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
k7VYfhbczr+tglBVnP2dNpzQUg4faERuh35S6DlbOXKmaLBzNWJuLZKd3/iHJso+4/ki/NZUVDCo
PIbVzwxMtfGyW1fMXDvveUi46OnejPwVxk5t1kIbtSbcZCd++dNgqg5UzMEgptRWzheZuzX0GigU
yFrxhwF/EKgqip1pp6C9cstz8ElT8YbfLOW5ZqJRuK3p8wRTUD9tZ+3ZT4AUQNnb5LwhJYd18bKy
gCZ5WG9Mj+aMW9valUSRFjEY4oFOYnca2u9dC1uGlv48Br0t9pUhfrmTbufRCalBxAR594dFK/W+
13kLKPWgZzIiZRLopKxSb3kx8JrEbJXF16BnhQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TxEtvLMShWARGvALMwAihIuShrdtPpwirMDR7BzuLz8WzVhoqvJSM5/nLMHFGqovxD5hXGIA2TAw
UB0YVlq6K3gG1/oM4RpzHTN3yz8Lt5YW3A+UfuxJr1V9UVkS6LmvF75rPoruMKpllkRnQaQkrdOH
79erJYgSSdvNFj79HX4=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Jd4QdSkhWhpPJfQcqGINGTBbyQi4fwpgiNWDB3Wd2IjKeric0AmdHU7UViuSzCLh03DSaNG2q/XP
qatCMMw9/14uzhpUJU/1zUWxXlbRxdCkB/LSsYsRRmVRjaX8PHa9/COyOOXOwziBKCZ4EH/zCO32
LML+m8CiAQ/Hl3o7OkbgzReeGFKo2yT0AlTR1mlGeI1ujqvvwRe1Fai0g+TwEJcmsDU1/5bkvxQ8
aV49pZh6N2SUhTCJ+wLBZlcMIljfD3Bu8Sp/4tL/+j+yW2zEEf4Sl33jw0Cb08EifW3RF8BmuSm6
hUeX9HuDvEf347dVCR8t8qRzeC+0nGD4/fB1NQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nE6k/lSQEQ4OmPB4XqBcP/LpC07K/JJ0IvLqk0FbQzQZjzqT5yDvPsiRjELAcBvPJRahwOqlfyes
JDXxH4G+XSbtKQtE02yLheyEjNesZ0dv/v3vL+wA09O8khSrVyP5ijRndW00Cf5Bf2IpNiaJRcds
F1ushZZu9jXeBItrh4znBf9fOoXggbdnBLyNjuw7bRfvTeY2Xhe1Z7RpJLgPWMz3yKmlUVxO5Zyf
mjNu1+82dGuZ9x/eImCHDzcLcpca/TdMV0iJAkZHrvuhhu0GfQ7zgBbvuyb+I/r0q0vuL52PeEET
HDmGQS2oxiFTbcwiGY3t/ioXPJYkEEqNFUIzSA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EYYoCPbR+OMFlmBfBNcQ1RKQKD88wkYgxA5pkdacb5EuwAeven6zC8gsLrmbmaf1Y+GE+exjL/E8
csfwUz3cQq4551Y/pgVQB6wc+K/5qus2SV7wqxTpqsWY/Yu+bULiGuBSdS51qWlfxDNujKEBhRPN
GKWkQK8KP7xMHh1W8rO4WL7cLP0qnZ7xSovnz379iAYpAJOGf/f5GjM87wrRCh+60BUmNbENwN6h
Un/7huetrD2tvDcD67Ox5Dkto+nybbrNNH3ry0zh96Cq8sxNBI7cJ/iRp5kCBgqxCxELTa7hlTHW
RWkLjA2W/Y2HjatDbYo5U0A7bO8ORiG66IX0Kg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
q9bGXHBOyTLb3eTSnDNZfQbfjyoc3yN7NB+1C2N+mReGSJxWRtlWWn5HWbhvjoAJehclGC7OtjK2
ZSTJ0A3pHY3St3rul3liQXKD5kCQ9+vFLUhyKlQc08mhaOXPkXVrLBkSbJoneeg+zcwJuKQzPvv8
Se016G+DYsP9PPIjvWbgYSkDDPBmrvDI1+5mRe5HwZFGFGhAQNqFMnPAskAW1MwhObzaIpkQKTZT
7A6i2BjYT3UzWyOCYK2zgjiB9ZFwChUw4Bwh+H8Xf2j3ysF46VVr3Y/hfiRxPSHR8Jb8iMEkCJjf
nRAfkr8Y2ZxDL10aUR1VFpL5aHsLiRKnNRdZXw==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
nsakC0nZIZNi1X6ujQodgmUw2UIdYzuFQ4iAZwA9YfvRrxXUL7ynKQCgPpNVzwJk5S+CJlgNjRvH
avhNsBU4C+cBB3dvqouQ4tOLrtjvGCn/tgPDevuIaG5LBxGdZZ/MOgVEltPHWIYycz6nfuA5/Axp
6IIz71mUhQT3OW6kWYR5cK3zVKmHXkQGZxfNAWG/Pw5DHuc9xxTQpswaIv4ECw8olrxqfoRkzz/n
gmc1riU255Qanc8CpzTXkB0TXLYD8b3W4k0EIAYhAlKk5HVAVS9D3DfcWg27dKxRMm5dVH7ddpvn
9W7az/Gv4/jAcQ/A2wvn+5RGmVdmY2XJTvnb42j3M+6+R6PXkHvxDCRRgj7df9TYddZWyOeT0KQd
DnIaIlkFA345xytHveeTmDy6qVwsD6GrlsYJS9tCsR6FloMwjoQcZKSxBqfWh+rvQ8/8NxsGVy4v
3tFI5PwOhr5e4Nw4hm2q3u3mpmtv9+BzXIuf1HXxWr2eSaeu22WHlCsg

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
WuUgcS5b6yfqTuzjufwmIVC5kWm6y/3mx22Aii+Dgdcnv/uLoI9/njjHdhb7hUlsD3Xs1keDNIwN
3pNTWeUxyZTJzKR7udvlJMLBMym3o/ECBMv+uN4BToB/hl2qqhLvFAO/r5AFOlliZqDwiGcbQvyz
YxE2I3qA+lBeP2iX2/4t2ns07deHzxcGsGDpvkWpwNcM3RmD3m5puzv13u/mWj0iTjzSuDu+lCO3
EIjElwRdbJl/F7N/czlKYgmKd6feg7/nbSKTQgrJk+bEOJwzrhlLGQvovZgtfM2nxWwlvulcT7sS
n2ZxTDzZIZJeakYPGSP3PRWLzaOntLk4/JYNoQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HAfLWwf5IE4nVH0RKu6Ckfcag4YISAB7GxmA74RLd0WtgVtvSg/hiI6xjdDBajL3WlsS8r0EeRuE
7k3XV6Iw18PLWYY7xEqYXN+4UCUMJuuhFnCKbupuHsoPe92DFCS1iQmSCu4KA4if6La2soKs0Eai
lizBuddfJbplTj7Z459Jc2VAD/slvgcakh9coxr57R1xf3xL+SqtbztnNWXTWebaVsMi9o1R8+q2
Bw6o2bthJTK5AjuaNFC1mXchmICuCVK92/JyceC3nXwexvYK1qRmiOyoTPwPOS9/j/gup9+/1Be6
vYxlYOcskfzyxWLNti298ohd6UCc2uC5C4Rl3w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DzCZLHkutR8dxKMJJC1uS/LdG9PoCtj5GsOR4GKxJSZTHbAW3Lwb4zUisDiKbo8nzvAc+Pc3aKIh
FZY+iEihN/UyNBp/ZVBx4xfw4KiNs0WcNidwHxnj/AmT0YahVcv3MBdpFE4TvDgOFqEqCr2KvrS5
K14RY6HsADqifYcgChtDVh4X+2Nen/oSD8dZS1qLOsyQr7ETEhogVmc4Gi3TE4/HYjm8lV5GRuJM
x1+0GPRONu+RFuc2B6sidWODYyJus0b7HVqnBAA8gMcV6twjAADrnyIqZwnPoiUCKAMzsDKVKhW3
GrlmNwP5uDSVq/4QrLJ59GIzFy3EXCfFTYr7nA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153712)
`pragma protect data_block
EZApQIw2cAyO5ISMZNLQd1f8Yo0WvJb/EKCzJ2qYSsOyqpLXAhTccspCpNXXrXGI3tSna0XFUgDD
iwJi9ixUtx+tqylLWhESVJjZXz5n+TVFvWWKS3fqemyhEOsm6T8aDruWneUI82pUvZULxBIxQ7sC
7KPhGYA9Vubc1BqMegu1Q6nd3dwOsm6ibrmM6ohreO8tRzVnBzURTek0KxYBM+8Ebf1gvqZm/2sS
P1j6wQJfo/mguiuCD8v2KofarreX5Zv5wD3ck7VEyyW8pPjkDTfh57UpK1WfMoUC/OPKztDUY48I
SNNeH9FHS0fiIKKYWhUguTK30KzeK3amjX8wmmE+5S8H77sVkGaeNrvAyvlmTPHZG9v9g8kJg0iQ
ujyv9gb3MRO0p5ps4HIRSerUMM5L6Z3EF+iMLQzggd47Te8NpIaqHl43BelZrk23gb33KvaXMspp
h0JtIqA0ksCxw/RxF99RMXwV0c0IoqY760G3U0vJKZpxzYRpgarVSC3kNPxLUOAxmnP9n3PQW4S2
JvxI+vYHIWB2pR3vbld+f/drHfDFbhLHLEivKskMCyoM0PQ/Qd75sZNXvoZVGevZbgofWtS6Kcgn
mZpvKFSuSyQAKW9rn55ITD7dn2g+jcJQL3O2L1fChLch/JG4MDozfoE9TWDGtMHbv10Dc8LTCnzl
Z2EBg8Nq4MK1VqD5E7JO3y8qIoh78RouGOl5nDV1+v6obfRoINh3p2RoRL8iF5n9OfRFg1OKxkAj
s58Y5Di3Tluv2ZJ7AGEa2mCl3Z9ugtQNfIygmVVEci/JZK6oX9iwWIq0Q+K2zMcb0W3lIrw1KBPG
4ttMjG4bZCTd65Q+KO7wBrPX95BXJUUU5B+oJYS3LwRAlZ10AEkVXtf0P2gaxfFw7EIISBtRarTD
VqQokmQo4TR9gl7/5bL0FLXmROcrVDOnOy4zx4uXhr2ID964qXofiRDSxggtpGR5tF/Il0Q7aspf
nZiEEuZ9IRJI+cAGXRHACXakzjKDSoZf9kZH+Jx8GhH4OlP/UPjtklA2UUi1VH7MHAcbI0qzMtlF
d5H7WUwhKhaVOGii+sW15g2cduOAEHo2U3/2wSbiRBkcrHIpbvwJg4mvBCIHBjFiEHbHsyD3tHfA
/oJ/DZEJSn/IOE0lpMQJq6Kb6/+1UsZ2n4+Xb04mrzFaLhUmr0RVRqwN53cOamhevVL43Bqvr2O0
2/fw0Ulgc6flCBskAjPKp2qK09Pc6NNCGn0VhAy9nLOe/P+jKs43JQcwrir3kQZGI1p59qmPkIwx
NVImJIn317nm7iWLfHGxh8vmPDx4oj9MaFx18perAtYzBadvD5b72G4BtFl/EudOFaULjh7DaeTw
JAXaFAST2FyddcVbHTHnLUjPAh1uDsBGnqLn9GgMpUSfWF4NTNzdCNBKjPoNfycqWNqUgtsPbnug
PPRu2fG3wwOHSYeYiuqt7HoAsqIfY/cLyB0bYuz1RwUar6tRSAxgWUoz8DkX9+Le1yJaR/PVAeql
ElA4/+DHwp70V2qREtGqH02u+YPfooqo4e9qKdMg9hIa5np6Fz/krjfMkuistxUycqIPRG1JwsVP
utLgyAlu6qKhaDsMNjLK3ajsBGxSFcW74wCzR4Rw2wADVags7Hot14fbi6kbzcJy9mD39R87iY8g
fdJmHcIjYIxRzaQv9To7IsF7nEzTtR54MVNicwawieo+Od1P/3yiIOsYlIMq/ArBvCcCs6Ir7Mwg
VUwFhPbUGb9tfSFUKaC8tmmR35fvpXm13bgpCHzIee/RdKd1LeFlQ9+Cxfn9GrteHuG5oHVoA3Al
a7twwfUPMIFC/z34QusNyIogRCXzPIFUoqf4O6gv31fcATquOD63dfoS90wFT5OxNl4JiCAWFypr
A/KNJOc/oid5mGH8yJBQw6BqWERMzylenrBKwKwPp/VlPugOT1xTYISXlzpR0jRLHnyexD6MJobL
gZf74MHfoa+I/XA40Xa5hRf1hINkPT/9oTjRhaVVXt9XUpdQO3nhMsu5oGcHZhEhUKhg9xJX8ls3
7d5hsGINuBdUxnePM6Xm5y0KyGMd7knxH79tXY7ccQAb6seOA/v8TGjm/ahaFhIlFqXPVhUzVbvJ
FlFh5J9XBNGT4MMfi86hDnalP4VYnG5lOCbyO1wlW6jEGFMF5/RhdS+mW0biBEg0k3aIyUPxqWCi
6KJThh4USQE8I3DRo3BqDFINaFpOf7YNJZU4Z7fbTfS/YJNKuK+tN1CFpcxJYwGcXwW6iah6QL1m
JK3klnieYWzKZWMgl6k8uId5e+KKIKIfuyBosW04M9v5YDyPuGNHvMWpG+BBSiGw/QL/fdnQ/0/3
9m0V5pSaFlC55WOoGIjhpYa75t9RskLiGYj52XCaxkt1v6FJOD9d12KFJesV6x20uxnCnN/tqxIt
7T4Sa0DIMrp2au7fsZqBnYqyWbvfmi+qPdAny1v3yZTtHY0w//6tsK3h7NXGkZ8T98R89oUebZOP
N6kI57gHgW+KYA3Xwm/hhjQKTI6NLIo9iZffWlmCW2cnwUM2LSOfyZssL8jj1IySjCeJSeH+Bt1I
NfadxDJx2ov5eOpT/I/LDo94v4goaM/YaHgHBpGNLaiNsdjo6m96lD7DrGlkBdeOqUvqirf5e+bm
oDht5UBVE7fvVj7SGeraR3JplYD+7cbuSNi/ha8nk/2GgjmO9Hw1hn4RmzeSaa+8zipOWHO8pTp4
mGCkfewpnuC4e1exG7TcRek5Ir8NPxgfUH+TqpX+g8/tUJaL33DBMQXCsvArctUfSAnmH0Qw8zvv
u+AUwBaLZqxE659JSKejlaTDInIZn/jbET3/hOd7TMXHqzOnc/uyIBvcLlVKpr22HIyp+GfxW023
oS7R02ebDG84bCRrTjvDgo7F9n8Uef2ZqrSXtrYGlU4Dzz5dF5Ge5jHXCYLJ9Y3cs1gAFPE7U4TV
MaxDlpBzfRl5eXi12HH9otG+nxmEyOVulackC8/05pKITyDDMZC+VPjJRLL3gLMorVCkDjxrBMjT
ujT9CM+47/wDDtCrf3q9G+9frTSK452SaCGIgosFFvmah10qZgbTwcLAp6vZwLEtq3lNbwAwCEqY
8BG941qnwHu6uHuL2ikCoYDIhNMcZu0OKVBmpP/8QQFLA75m1i9gda+u47iYcXecuR8al2969xER
cGgzxcLhf4NbTlt90j4J6JoSpyq8t1qKUvWOXqf2WVh1G+MaF/Tq6DsiaNU1Owr9RVfCGKIYvbXH
hm8dvtaXTrBfZquOzahtQZDQAgVXJD+yw0L1gOMGN0PaKB0VIQdz/2UzndAXfoxkShpIDHSrLgOt
UapFG/3L9IoZjR9JftlHLARkXKtfOCRmFJOLVDfKfPRrdJkNUS5hzu9Avxc36uR3XSaCQ8fldbV3
30jQK+P9kljd41rmyZ+eP2RwMoYxhKrEFlniFzuvANlLojHlD3jDiE7qcjbRa+s5A2YlzDdftdoU
QIWV9s+GQmjXzX6cooM/XnWxDhyiBTtGhMa114IHkxsPQuqHZG+G3Yf4GhpE4Icu8v3G+Z6SaCQv
2NGdmyIAFG0IVvbJ6TBSnLgii9qhFlbSlXLDk5oW/40ZRzhLQ+6Ew+C+zTIqEkOS7YEkFf8iUpkI
wcBhnTrZIgSZ15WTVAdVKiiypk/4Gv/XypNGePSofyHjT9kuYXDEOcJy8SxQWNLnEynoqEQQ0vWr
v903dIPcZsYf4tJsc0YqMBQoNQTC4vjpYjJrC0GpYum/DcLH1SoPR9QaNDKlwyK/v+1iUQrFlpbW
u/4MVSMZBAdKLNN9GpaczGCa7n/YdX3os3EAROaZDICJRoHX3O1I77Iq4yBbdgwxOnHVJYwmErYD
VE/5suX0iWVOo7FHBBK2jqwiE+lvMpLUvEeXnW/RkeJTPgJGqxb9qBXsRhAkJVZYttWIWMeQTDF/
DUVoespysKgd7s8gRiUfZGNXZCAtleahHsw0eOaikYGBtcke8MV4w0To0eAPjRjolURMOhApwlVy
aYT81xJDSbICpLdufMUNN/ImHQQ6GWC4l7Y4llHtxbPiCROjn7Jt5mcB0A2q/X3NxjtF/sZuKyxg
vY260e0FxzI9JRBiCaG+Cuzqg20E5WqN8LZ3OCiS64D5JilPD/0ymptAcVYCTIrMzsQYBwt7+Sod
GQ8uph/HodISuDcgTDxI4JzeebqRLO7CDLgxHGrJUHv8pfQaziE38ZfOb2tgbRoakstgvVLGoO3z
A2M7mJzUWuxDxTbT9BIjbymbdFaIGlH6HCz9QTDrKglxF6nKGCxio2EgCqXVEomb3tnHfs9TVcli
nuD0p3VuxH3a7EvEooP2KLHie0wWvYIl264WwwIV82wcZnkBm/7XUVA3q0GgjVTphEf3WDY3HO2Y
XQ6bDDfBUcS0zNlmupJYA2IeS9US9XBbC6Sn88zee1a2YTy2mcyimUT5ShN5pz1KMGf7M/SlfdZh
HT5AdcLfTtbZhKe55taTQzJDf3W9V+OIa4Br9PkLlATvwYijCkrO5mbY8Acs/nzHsoG6zFgO83Fz
dga9kDJqX73Uai98N46OCp6QT+kYbXLqAxFUEYRGTRVqYj7HWmB2MNDhHq//jPwKUNQ3TrelNwR6
lumvA4gwezc1Wo2rxqOzV79TG7qgnKVROVSneP8TZ3e5FwVG9ZCS5Eq4FaRWa8PjcaEJOqs4HbSw
b9ixe6ZiNmFr9ntv+9L6++hHwtiq8rWATzfqZhQ/+dUMN/fSaczd32w39Lo2y0b0LfPh+ItEEvoQ
ac5o4XVo5ya+ELBXmL/RmkAJe6H/KL0NSiZwXIE5GuMpTt4n/0ket2qvMRT6zclkdkLnIOgle5Yv
1PZNLa1bP5cNhLB9OKPcTaVvXmrDFBErjIcaJb+76BGn39K/GFxe1sCWiswRsPn4PbIwOqESQJSo
XmfqM2EvEyFZwkzxYpL8ebNI6u3b8HdFyYmvvmaJFbuFfrsOl73oqh/2ATz1bnjlHjnfXy5InKfx
mbm57qlGhXc40rvUzu/JUj9zH5R218a49Hn2JIJt3Sb7/5MXfD29CF3eE8LGUk5o3h59ziF9mYB1
KPNP+tKUoSxENBqpWFJMWkqJJIPUgtXYVNqt0jvJVEGbt6EC5ox1nF530NzJXF0i2nXpgY3NOnOh
+QCijTeL4JMfUQna14Y8BqTYSn7x7TkAjj0rhNA++/X2VFdIMb8AXQw8KW8xnvkqsIsci2mFvX24
Bpg3iITfc06OgyQ50kKB3b8yYVNhjMlWdijqzNSxFxkws9Y+B7H1J1eyZKhe2hlzYdxHZOt2iPia
lkRRmUmnvZmtfkKuZnXZ9v0FS2fQQynrppY83iXZiKFUwK/vTINFlU1X0wjHgkIlqMRwUfSSNGqA
gAhaqeB4Cancn0z8IEOkLzcHT0wrhq6oXhJ1X7qgkgRexaVR7A2KxaTdWZ1pAVMDp2ylggJDTyHd
C6KGjAEieiD18NWNGgOb4PQnoNZTOvjNUd0uL6STBKvnV8lXHoLOXl2TdhTACtosdGz4yri6Of72
B3/PLg15XYF7NQ+UqWKBtxv7ICSwOpWeWfbtqTzU0lV+DLCflzyMxTjKuC74X6xConqh6E2n6+Za
eMlFCN9FvkAjuW4i598+FWp7maNT8jaT8gkte21ks1NUXKBTcFR3HXrKrGiXUZaC1iwm3fcsuAe7
A018BhEjS66IMWbIVsZ2L2OC3cvuoLYJ8PPXsGUfQsTXMSzCoPKy74CV+uwBCpWbxs6nT3j5z1mr
h2bvtMESUFUZPAoGNazYObZSd8DZtxMzZJHNs9UhL7J5oXiXW9EPDSNvM0msNl57Y0PM4B7Zuz+W
AEhzPboaK2W9CKxVK8WqPH1pjj7VuQoe/iFSZoXcAWSVDAcJVpJMmYbJ7F3kzyC0LRoH7IUevI4i
vwqcs/valTc3teQuqp9OwLASvFe7MPvN3JMV4dppALknhnkM/8H3SnPP6KBsVsDPsp8qxALeW9n2
FAGsIEVH6S3/JK80asouTfUmfJbIjY+QiCfRP2ihXUl7MK9PqvMcV0RgGW6sxv9EVgoTzHMToSsn
A3y6mQNY24hmrsBHdBnqOr9RCKVQRCYAfI7BjnfPN9LYA6+Jc0C7Usv9fc2jAXEwObVwDZN+DWqz
oZI/tJwRcEUJPo+Uw6oY3AqfUQhCCOojdP7ar8uNyL4GiWubUy22pbpf74U4jCdWGnaXpLioBnSB
RPUtQV866m18X6lOwiae5tNzvhJbsXvLDdBKTrMejnGYiUBFVdK7Brp+X5bc/50D5URdDQonDBUB
Kocb9qFPdUSj7y0/6RDBTEJzove9l/+W5oNFfBy+23Q81uT48kMIDLUhr9KQ8J8DalnamY2hfRws
v48kG9OZyuVbeM5gH5mbIyL54hlv+teNKSp8t9SimnoRM4H/J19JICEXnZGSOVscZenxV1yZ+sC1
3/3Gcl3X9tJfghhoz1BqzHdvmVuc0sAi7lcJvtcWVmh0UJpgUpUOE9dVDmp0PRs34bzlZM+bl90/
wEWn3+yiZ2P0EavrvG13vxN0XUWDOmLXuk2Kam7Uia2eqTcDzZQzpSgaeNBpXCt+jv38o7NJHdpb
Tb6txJT/3nTiCEZEesYjUWZfPyiX0ZN0Z53RV5TaFwV/R44ly348dTdaymnw68LJxTgh+iKQvqSj
iAXrFIFXOIaN6ogKzFrghROaRXvOok+iokuLEALGeV6CLe4SaKA+qLl6uSr3ORyYOR5e2MidSqtM
REZgViKmAqxs8Lynevj08gqDnO2Taka2Fk04IQyJ22XKfHTMGXk+KAf/lzVVC/ECph+xRGdPTVW2
OXQ23YdYsT3I/VDLhWPppHh8nsZJypDEJYuNtwY8BLSQBepbGIKafPBmtb+EB5VpMf4dcQd/vZSY
db+gHvLWkaZD+tvW+q9us0gwoO93Clg3RMAeC4Egj/Uq7YaBvniqU3wlHj93q0+aVq69DLM9heaC
i/t/0zmBi6p/eLGZ3eIQKWVfKHgFckt2N3/02ITGs8obPQsVCiW8fxOGH/TsTkGrBOgjriz3QWw8
WUevEfWj68ShmjMxNQxN3CFna5/n39QOYUnwo6AsXZwcfETQR+DcII9ggazLMj4sfxfbbLHRTdG5
seSDS0Ni9SBcUp8lB9YUxG6t41QOctXRvtmZfLV0XLLElyg8WTRjQ5ZdpzCeNTJxyIacXqNhYcgq
wD6sDhcFAE6LM3AxfEGozEljbUMklxmD0LXtmt8V7m5b/jYf2srTmSl/HCCusaHLJYRJP+c3RTtK
26mIEJM1ZqaZmkvQ4EKqNnCwmIvF8g37oKZGqTLmNz8SX4YwJPbfR2qfCnbPPgoURU4o1lxJcwVT
M0Fvq0aA2Ye8G50KmCK5VOQUGYuh6yrttRsp2uIDye71/u71Igt9b03tXeeMyUWxGIm/76E/T+iW
8+eGjPh42Wsp1qW2NVXKn1zQcxPHanoSMNQjQEaB4FLe/c7IJhvCsMm3jq7UTVfr+GnKxoxl3eOg
9JzdiMBJn2G4PnHs1TVd66u9/uXTwu3ofYByjr8hqkgI6YM2b1PDcFBRYNxO3jxKBslofay2ERBL
UkflbOLhSAeStxsH+Zm0QDKKHmSPGYPajOBdBuXmu6gc8BULzbXWn3IJN+FSqDUd9UGVf+SbFUkw
tJMFeqWg4SBkx0EGtfuwgDCcMJwc7/E8ZP2oVekQLqRAu2szNCRA/UWrOJpYBG5BL2s92fBLw/PI
PghudivqGUqAMpJfK1wY6ueCAdjSNcHgsqalsNcgfmyQl9xPXKjicBjTuo/HhfDkSwQLxDrt/Svn
hqu9g1eWLV/Vgv3tBwVkofJKldDAHcJiZyxm2J46BE/8Vp0lRjXzOScSEDo1w9QQtSeRH6YRJlG9
XH8G55IYA22oIG+pSs822nzC1OKtVvwYInqYD1fONDoiWY/4ABhyi+/DpO5ZJtKSKS65vc+vFQYh
/n3jwA5gUJ9UjHN+0MWYbS17psCkX1t8lwX8p9O59VeCYby0lDZNguqJZpskyPFpPR0bXqgpkflB
EVnbLO3xGstqx6XZSq9Iv9Vb/aUnKrvimwJcSogTO6cGKgKajQqwkALGpaWvVhHlxmTBySKX8s7j
OXvwDm/6dkd9ZWcw98mQqUK9U90hWuXXtSnfIB6zc2r8iB+MDevi6d9AtHpcThJJEpQnxdHPEpBT
rDirxx4FWZ65ZkwlFMTMRs3PZBrusXDW7kfDynwPnc8ss4h31+5oz+ifSEKNRg66nvxwhaZ/KDf9
TxT5eOrkyIFsxaBO6AjCJDSC84Fd5CLsOOUNKHQqBK/6ZQBx4NeZ0KgghVQ/RCXwEU+GAhzU2KUR
j4xyok+e6KAjpfL0ptjt2XSA0WNb4jVeOxCyB3NjioJQRxUHFi8n955eZG5+kETZzXxJoNdObUkS
6lgo14UxWseYnzh2az5W3oVrc+uM4zWlXX8ZlkX4cQBjAPbuKzMbspiZGbofvMQ6kdEL/WLJtkoJ
f8LriaCtEu04P+hWyxx3I6Va+gnKqc7TmCKzx9TlvLeKo15NQyse6+thJk4LtofrGEtvuhobj7uk
NpdnLsP9BfU2KKAsvPT9UxVeJqg3sGdJSOGua0X6pVqEtHG271kxSQB9/kJokEWREJqga+JE2QvB
AlZAjB93vfHrRy/pGnmRs5nc8/FZI5z71M40ZmWl1pmtcyg9SNzIJVbxsP+lyEAJq9qrnDql5gzy
FbnaGfev1PupatexWk82aH96tfsV+xZpGhiCcGr0iSaWfqwCgbBJ2UfokW3ypu6Z11CBUa2jNL5P
+8JdZYpVSoSQOFP9OfTxzZbMbxB2B4owgmM9et8GfJf6kYTX7R7uss15fHu0jgpIAf/ds86eL4WA
XISTY96/ZLehPJJgz538z3XrQCsj4EVAQstvtXzzZHK9YvQyXoHLOlSmc9qXiX9j6+DS8XC+4m5e
66eNk/wL7K5DjbO/lN+td9rQSEsJYYN3BKDCwQi1A4xqaDgYqNCH7e0zBW5vRtGeFG2iviNtg2CK
NGHy0+8YCx9pNBHdgbmSPTDMjxEVCxyHRxOpvGFsA984qUbBHtm5efT321YI5GnWlj8DgQHyoLDu
X4ll+4Lq/K9oijv9pmn0D9OaVHe+YA44TQd1UL2mMCEIuWewr/jJHe945nzuuBgj+Z+wpvFsae0N
9dKkzpILKrvPXUfK/sGRt0X+/bYYVM86wt5TO8Z55KphJxSoXQ43Os74vW0A38po7AhoN52V4+Cx
WiCTdtEsRkEAZtEeljSgpzlq0oQ9kQT+D8EtKkhhR6GPmW39z3YGKHQX829Qzu8cZ4GnzFUVLk4n
BG8T9qNf8rccVBpFY+3gUexuIezYbnk9Vs/DSUkY/NRh634VxFw1lo8Ynodm2SWu02NEMhaiFJvz
iNxaFcFv2zj31cdA/NU9ctxMJxpUfVU37BSxkdsCVhL/GQSHoEdFhDAymwuqwlsgRHR+NsZDLiAN
qKSMlsr6p8MQDNcS1+GnJOZihDEYZ+Ua8UKkSJjV6db1lfkHIHWqRovDKIRQt/r/EJ7capwOgZvQ
S3KhLhN0mrDu5uAyYx0fMWdpP4NnmcLXkyLtvHrAlqrsqUkjM3vM2wOJqSonvbANcsf1hEo4s7/Q
OwqKuPL6nfuHtW7MCi4hfq+ZscYCohY9FTacYnuWcPNxYwk40S13I3fFXx0kBs60CHvTl2N5Dy89
TMXchQyFWgeNWphqhD097Wp29YIU75orYtGswSMblCKUHKTvIVMDHye87e18ylrj4xDI0ggXrs6Q
1wK5luhudJUh2K2uLsZBqePN+X4zN+z11o2+qGU4HqaqiQtlR6FI7GZwUAry+Vr4JOowyEEKkG5O
TM8NAr++kY0RCJEesicpt65e/2gCeioXNpqFvMReHXcGwYqmEg+L9HYaf68TIola9NGwzfSCofLn
boqNVlqVxE8PFw8BfS9o/fCmprSL/4+YGiU9Ct+7s/tZLNJgIoAGsWOHFei0N+jISXSKFvqzANZm
k/ZudtPz82kOtHtIye0CG8k+sxDFSKJ0kM7p0uGqPo+SowTxcIN8tBK2U3WHXdSiVnrK6rpalLwF
Z/n5zF6sm49c4rWvcie8tfJXrKTbZ8HYGcr9I1CkQBet2EUvfRkhQdGzx2Dq+CYwmacvmw4Q92+j
GegcXnqvB8Mb/0imgC+fergh17iFmSFxmF+BYt905pD6x+g5h7S8RfYrE8QVOkLB0pSpORqzDC5d
QGVUn3K9LYQ3fkBdp3b3WFp7s3ZnqiFaw+Y+GzicW6Ilhd1Kk2rYhah0r6edaSiAq/lZZP7Yk2Mv
uREuWZSn0z8XT9GAJaetYCJIOviwrtdn6IGJQRI/rXl3S/+azMGaVnIZ3jhO97hhZrmOvFnmjZsY
Zq2Rw5rMI5ea8jSlSaWQDD5pePMXFAlWIXah9jdh15HtesrjyTnIiDeDJS8ZM8HccwVtdkU2VJla
fA8J3M2H5k5oeB41hPcK7aSpfl2tHt8OLlnYxiiSUBVcNDWLfpJpUgv7V99InWpBFVdbHWLcrxB2
yofTnRmDuvQe+S4N15UPLi4qCZlsKOYfRPu3fJdCxp7bubj+hOZg+ZaY49vFO1u1xx+QDCTXa28k
vcLZd6TcWjrMTMk8ri+WVLalLagRd7KwVafp7sghU+AAQ0q7nSL76ioUUvmy3it/rJ86v9AR/adJ
dMvHhaFOhgpTEn1eDcZX8GbJnQ051oqFzncRVDcH9yqyYEcuZxgxFYU4YmCJg+R4YR6c9KNGA7B+
m1znmZ8GpDj86WNpQu7s3/dSqTL0DF28eLOMaNmkE2n4eBFn/mGhVeDJZEbnzk0NzphJD0tVeCy0
Ux3/hzvuRjEmCFHFGLqyvus2pShkZ5hGQ3sKjPV1f98WxlCoA2w9q8h4y5808xWBprIFUGYf8p1Q
XWdjqWGZL1MePgjBn1bRfzd+09giwgPNrHyAB7z9tAYG9Fan7tQefJiDnjp2cpE1FaJSAPuXHghG
R7M6N4yUOeEIJ+wn86QzHD2+pLOuTQIYUqYFaHix89avmfI4A67MtH4fShdMtEMjOJPVCd/y3dLI
h43dRf6YBXwZQa/gaK68v4EKFXoto1hGYQSnejvnQBwL4HOjD0DaJ+z6BI3OawEK0wbG7xRSq6QQ
EBInhTujVTFY7H2YJDVN+KGjDPyYuQslD7oa90AcaT2yY8pz0jmVVeu6mXXVGnChVDz3TggnxDWP
s4/pGF/dZh91PJtj0MaFaxCeDOPH3QnnD/t6HqLh8iMGjD6vgLQ+xz1iQoLaWcKBZCeCyN7mJHXB
YvJQeSKxY/o8WOTeKCA4Bl62JfOkLylOHzXgJiiMgIOmnAzAKIyC/LjR0O/3Aq2TYPMdZo1TZHzx
kzSU5jYmo0+xPucGASl81u5RavAqrSURiAmv4zydnjIJJW1D9cl7P+PHZnCTOr86EXHUK3/gW80O
KVxsDelCQ5A5XOtOdAdp4swF2ScemmKOaOY9GG11y6b4v6fEftojlOxJOeCMW1jm/rDvaAidzABv
EjNYNYhz+Mf0DusevZaOwtR8f8njjjzdNf27qgpwDmmVy/PTuYux7XZgi6h3Lrv/4HQQNMan9nRF
i//liDe+HrGicbdZTVdCqQ/UdsO2wDLQOsznioSgyZMUo2naRfqUqXBE1Svxcyo/eTKzhwlwhRLV
LtdD6ClJf9/xHB7rgyCBb7Ro458U+T/YgBLVSFoo5OXrt9+Y/MgyOih1grQ0yZ8o3SL5iG29QFxF
wbPq6gDs8MzoR7UgmeGUrEPOsNFlkkIl0F1dyCToU2+KrJoPyv3o19ktb1MBE7SZRNHiq6zk1QHZ
N86zz+fPWsPvCvEkquxkpuyu1ZcquCyIjTjuWKR4roR86mz87qsrZSRjdj3IGDaiZPLllc1DOhHV
gaKJEdNRLPPYODU7xd5FjnlUuT/h8Dzi7ey6+KrDNhD6e4UM9M6/QMEQRXFNVfvbwFOaq2amFLRp
ZvtgA6zUtPsqVfxO7tLz16B0czPPipNEqVpwo3H45hh2ClvsnC/tBMPa2CeFr3FMtZ473cegVnbD
uGCYQzwHGf1s+vowM/JcJqfWKmgBcSN3BUIcyXovz+eL1YHbgza0538bBo+iQ7ndyREPyGRvrH5n
na0yn/x1PCW+poXlMfVGTBCdnc1L6+luBwAdgUHjSQfXEOMJlGBb1KHh1YNa00v0YXapL8UIX7X/
jtny9wm25sa4TzwgMHOlntG3Ebv7RTMHIAFhg8RDa2+ZV4myp178XanBjendos7s6Jx3z9+1LUrT
NpROQ45PqRuT5Oi4K2lWbjoC6ESU/CJ0plbEARe/kjlamzWsLNCNDP8bGcpSzC+57CZhA4szCTE5
59L9oaxUwE5sREPsFlzLyCj3KEN5niomPtqAFpEk3ARP0eCbz8RZEL9+d2FgeW+pHQRkEXdqsE/3
x2xxPgECOUDTjvkgsUdGXRyOT87V6uB0uXIAu3ZgjrRD12zWdtGZRLKBAlMau1VB40uSfu0S5CK3
KoqRYiP3dBw/vhp0OB0cmdip0KV/Gzke7kbUfuNBZT/+Db5EVSp7/iCUQ/49FZ5fGU37PEPnRpHl
sMOUBCq6cwGJMbuBDXFcNXNRetz6tnCr3KX4rFM7V2f9xutA3+lvKLkRjwtRNAa4X0SUmRVYqf0U
dYqwiXrd2Z2pNyTWO0teH3DvAh2yHiIua39lHuj76TROVdypuiWZ5Oy5PP5JkphLU1EqACYdbpzR
1upaQ4P7DFhUZAJqD7vsQFIhRtIxV2JEBqkFS3BVxtB8UtAi3GEvFPr4o+ttOhWyOYhDHoycF8Wi
pnThxj8rwSmWn20OrB9HI2teR7RD9tgwdQPoDamymCoCuCi80MtJtlz2EFmtKkPBiPGSjH3fzaMA
rmdSk0EP6ffWlJYEJLPIXuhTzIjnBJLOBAjkafYx8TU2J2X7dL5Vvx9LkEqst6A2WoW/6a9HqN8V
uth2Mfk3Aoo+R9Pj5SYm8OudnsOFtwFIG0LflzJEl5myQzyhQeN3YF5D3oiTYPTCvjKTgpOBTvR1
LAHkzzDAb20ync4pZEK1lCZraENNt1ikMncOcySJebXKAfgT6uKUvmR5RtyUHpWeHiEsqP9XkDgX
SIVMwoIhQissEfkUvSoerEo/FGpaVx6jjMIQBRhKr4PgV3fDj8JGsPI0qGs38cTN7eOHzO8oVwSW
wxOSNHC2qBzyr6cbWwHVLnmL8FkfH0G4Q+SXrL2o04ntwDOYDhaZKdh48PPMh80/C1Hbtpr9Rf1E
duP9sZBBk94rAUeiS/+JnhMM1HFJ3h35hDB0bmEMwlO+WaodwWV204R3TTOjduNaXcCrvXewQhvB
aDJby9SQVf3TTJLphT9dHuAvNqquxH8AhP4JFrE1/y0/rko16ml1bYLdZrDveGDxUGlVJGT1Gt/8
KgDEdLn4TDFHFiyUa+CHs1oMK3g3FusESnT4ps7H7kc0n+gdsO5/rGhuOFmBtrtWRAAu70O/nPXR
wKgeMIpU2VUVJ/PKR45j/+USWGCy/9XqH17/jQPUua9I2Gg+ENRVAL8NTZ6w73scCqw9zF89rzd5
H2WPrA1AR/9j4Bf+fn+4vm72X9gaakavgIDHy/PiCABV/dVYtghb/G6nqGP/280sP7xzYw4eINi9
YpzW9iq8OGCxS+usSYTP0pAD2bEcKY9uhesO3b66pYcQ+A6TR2JtLTVIHTeYsI/UT8QiclZnMkvO
dsR0DCKVOlmV9HeErInrjuSKfr8swNaoNtiONXz+ZQ69MGFl/yXjA8f9U1kVTPowTYbmham9Y8lL
3CYKE+Sjtm019C3u5YyVYn0NltiM/8jHQ9v6yfiBdDq138cbchmKqhNMAN+FIWhzjd/1Z4YYeXGM
JD2+WoRnibJLgu1YYjRJ0gesp2Lle7DbelebvDPWaKm00ySkKvawd41fO+D157kIACW5KuCUEyJe
AnNq8ESCZExmYR8rfbITLW0cWf9QnDsm90Nh9t0y/T7gvBznMxo4DPGFmJvBvwm4ppOyHAfjvLaX
Es2UlRNlCnis2uUAHnQ3HoUw9H+Se4V4P63R8j/CCAQSGdW9TpTYk6ayIpGC9M8HQSwGT4YBOwK2
IO9gK83krj9JywCYirnEdMARQZKdHyy9eQRbHFDvbos85Kq8W+Vj+lDLO58J+5EqD3YL+fZuxV1o
EHkf0p46ZjR8PQyzgP5V9Z+7vgbN23rYugg17IZ+RhkDLGNIre2+VTtdnVhsDd2nit0e8ATWLcJK
Vru9cSoKRt68jCwu+gklRUEP0r9nXT/IO5ZdGmBOUUSuIQTGksQ747x074t6VaUsLYMpYn8Hvuxb
MOpz7hNEHg0Byk6OLEj4CKM3EYxybifDvcRSZJZl2mEv8QLK7SRfKvnX+4XMKHKhvvlldg3qa4iM
6u2oyDOzy28ZxF4rLtFsDmSJlEWJ+p8668WCVbEOXYVUTTdt575wyN0qRFaFG8uBg8MZjal8nJtA
5784yctzWK1iWS0AoAV8UcdqML3vdbxaufVpI5sP7NBTk+7mDx2jfJsB7PMSPW/AGzvJLqLbHpll
UNBSrJ3CShbq8+NN4KiCNzMb7qBZRwBm7lGNiJU5/WDZFa5pJ80K3yf2Jga3Iwlz7SCFwfZr5ytk
Kf3xUZ1GRYiXFYnSf6NL/y2Erd7E5fyB2gMXwD1qVHdrAYdgvMJg/+ClugetqWUS3keJb+/LsgQg
nbXd0KTMkgYPTph5khYO9HlfmrBUPyICVFk29LoznH+u/dUCt5aWF/s0EF9wabWMIJUNcgb5XFdU
Q041RHqsUZZ7WuJrlwgF6MMwWcV++Uy4uTvl4BiaTmauna07gP+uMTB5Ir4i3J5pWfupvvkFdxQ8
vCy+zzo9HgNSCSami7zqrvX9B+1IovVI9rTNzO+ic36lA1+Chi3dO7e8R+KAvjpkYfx7RQuC+/ur
vmqay4Lsg64zDzlgYVK7ExmITkJfZ8k7U7cwlRT6ri6Qbfhwl1pYEC3iHbH89pLJdtyU+aPUo3BB
02G5VOg2j/ji/T6TZvDc7dUwIHE2xSL2tuuIcsmMtmgl2Sq5orkxZqvHbsLHCVlRRfOQWn3akRHw
vAC2uEGbIKdJzaCkrFfofdPeEgucmFiFkUdH2/GNN1P0EzEhw1al78CSkmgeP7v/09YnW4sUlj0u
D1kRVqFsubVBQp+icFfBo50fVS/mCz251Ee1IPaPvx9HBusePxliARtXYeVaHhkwP2SZVaBfH8N7
7ge+V63P1RU2EP26i3FWsp+dJ1+7PcUAze53+0FIjb+823UCeRM9hYG1WrEvJWUu6JPxs45gE3Hd
nxaLclNGwZiWuGFoJQDgkE7uEQOkMrXm3ELDdKxVGju9Qxjw2jTDm5fDSak3TJm8tVwtOLJAD/tf
GO1dqTzO75KHArICc6z/MPr0T9ntQfaUgQnQnO3jg98ODO4AN4kGjXhcYHd4cPtVZdes95o14An8
uxhudGbhTGzE66rBKSLca9Jx3MSHjYymlyWA11yVqaLm9Fca8jnYP+qK0jTXmcps0ljFtfEjKMvY
cqzWuGzhEyTh7VOh5bpElPg7Ux1aYw7OgeAdijVKN/iS09wcGtrbpsFRrMJvw6i/qxGv+A1jVZg5
UHS7nRSbdP/A0UMJ97egudGk8uLi6+MBBvHnRy3JOV38PwOcszgHNMqwjUO6SSbtfUAFs6YOVO7m
KR4Yrz9+QlTfOrFNL2wJHy7SLcScWauCaCc6KxdRe5T9tsXwb1HxlbkeANjp77QSKeDLuRmFrzcz
jNS5yZ77whn3vHi22wx+IjSvHMWLzY3/0LHa5Gajtk7hoADPhy08BrjDhIBTD5cYFVp2XWbkjtGr
XtPMHHT7kH6/FGE03ph9RbniL00mMn0ND2trIq114mzGjpL7N3SzGUq58FlROlq1KdmlIEBOWkoY
VL4SSY25FQayzXLQMUsQIL6Z3ZaIj0fAO7zV1K/5r9C65Lgb8igGBeTcthMZdAoFwr2efdrZjfVU
A4Dta9Ae+bQkptAPD4QkAO9pkHF4wnu0LdMqGTqK9Fm4y7f7bvnYeXSNJzmmNsoUEFjgGz2PSw4U
QKdPo8dypjgggRD0PfQJBOhiOoQzhHyqnoYeW7X+xADjPCZbOASBKP/Vda+90tCwuOVkxviG8HJT
UviPhT02sLNkEqPwE0dRgjGiJOWHGpDgAuMrflqFPIs6NwtoPcAteW1F+WDxpzD9am4SFh8qw1Jj
94i1kgBR6guISICqgfDOnWIlcbqpkES8xUnJs0W8jdwKmD4R1+RiMx0o2dNtTtkUiO3SHZjYb7vb
233TCTV1E87EG+Qts16jXRwWCB6shWDQ00HT7nLfLupWLH945yW/p+nk14gwlrYtX2JOkjdyGbxR
tpwCs/EC2SUtORGK+JNrNDVPe54Eldt6CzV+x0Oe9AIGzdadscqB1TScHSXGYCUsavnD9DZgveOl
ZnXJn3YjJ/ldcuaELw2QbcMzhuGtAk2Hzgk2zIU4yYHqOGKVsyVpfdOlZn5vmUjjlBU47cvM+Doy
UjHk08UiEeBs+xZPm1p2qOx6G3EvtXt17i8seoTK+Ol/07S1RuhWnmPEPaeWkkfy0LwFIHprno7e
xXEO4WFCS0iJxxF0sjP2UWdvlj/9QvdIK9iZQ3/TbSf3j+GcvIgai/M8Qo+tQ4bWxGESj1lLP/zt
Z2LLVZQWwBB0wAM4+df4PKXsWIeF60X4VQjnZhs/0tXEP0cieYJGuCpXdxqD/KnDd2rKamUfUY19
f5hHPdOqQPWi1Vc82ofmubQX00AwKWjGhtoA5XP9ink1mdcOvNpC72d6aPmmiWgulgeSBrdrHxD6
VkJuOr5iq3/SYa9QPll3qHSjF17J/tRYkPkp9Bf/gmlM8ZzfB0JpclWWO2dmmhKKCd3trL518ly7
8KcwMYWb72j0OXe0P73+uG5dzD+QJYEFq38o+ZCSya5DlggMx9UlLoYFJ/Q6n2Kk7Va7hNwWdfPn
w1QRCX7ryeIyedmiQZNER4VbNBrexk0ZO7Om7qnA2wzwSLsuD9aA+OCZDQBy77vUWDNZT6AcEPNH
dB4RrtXslZLfq/gji1I0DHqOwaUVqWO+qU31WM5mdse6/MIPDV0sqOI25YfNe+qScpTqPK1r0rKy
o0YyVjL3VNKlbGQqMYqjwBi/niqjLJJvnD+FPgYkYYfcEsEcCgyQ1b/kvUe4L+VDq8XHD25jvYw6
Bt6j6vIpOq1VvLDE8RwVxkedG33NLtf84eRJkhHxhZMRo2QrqgBymSaV1Hwm/KCQlBQzwz4+RuCp
RQoJuDKW238cE4FoheCZv7ytcT6u8FkMWzr1Wtz/pVk0QGK9PuqVfUt2ybFoxOpn8JUAtOpYzhbV
DH5ERtnpJbFb30ToGVZXKgPYoOSGCBY9VFIqx0UJP36f62PRxzNYrBpXyR6V6x6DYtZCwzSmCjDB
H97zyjs41hNvYT9/8PgVA+5/vM97qab2XxQZpamgJ1yoDtWMOACTVxyCXYq7D47tWFMyzR+BPpxB
czsrlYv4Me7zgQbeKs8qv06+lpXVGnV/o7XuIA82h/Vd056EK/Cggx1/C9+FsKLEEVA4XkpvUbCq
tlX13GrRDCouNMZkJXco3XI+MzlR5huhEtPwtapLWbzSqJt6bg2XkG860LPeC9czo7my0x0LLRbs
OIOD+wHyre5p5zarvH0aq/Puodj+KC/eTYXR8KnYKzwAIklyHj/r56eYTe6xj/7DOtGausBGG3jT
sQGFKCqpeH0WRKFHKnmgy1Ov96pHIXfCiit3nLWI1s7lJR9IEnjKLnzs5FPhIJVL3iCRkUko0UPl
Xn6jua6OvuInfWjQ/qK6wZTbvFtsEEPl+VSWsGwAc1H2rhhaBOuIJhTa6R5YYljvUuY/LH2FL+FB
wZ5eDH3F5uvfwQzwJy3AHiw00PZojQoeQLTs/eElDqD3JVxGGov7nPQhjKfeYBQuaDh5k5NPK1ED
G7Dc1Gwsd9q+vWQYNfcKei8sTI2O7PnxWJ8gFRFau0Mu4cnC7dgDhXfBBHvHoHD5+ZF2piA7U4hG
+rTkbokPi5LuBG04sa0Y11E2hKsclboQoOE5IFK4hZz9eRGpzijP0/6DpXdq5fXZZwFzhxhDDGxs
tz2m7cbIparUMEldtvZA+ZuFhjTEmq+pvfvI8wdTWJHMTBVmy5asBN0NKPKv5hXMJei4lZoANA2v
EkfUQt7LrFrclm6M8auSHtB+0ovRejEvSMxuQJoKx3yXlUZe7SIDllxbIn4ayA28LCjmwXSAXfln
eB3KLo2EqwPUQLLW01CkiXvQJjHe0e2/VZPCbBHJws2i1Y+baTot1TvPI2UrxQirR1uY7CI3QA+v
WmqfC4CPb2UT1geiYdgrSyE3V1ViKyIlXPiPOiGcXk5Q7yYgaWrGkiOhiI8wHjiFIdE+Qt6dqqqr
PsVt3OarTarUeEgo+rjfrkS3QOdbdJCDts8De24quaciVLly/Fjaeoos1iJimOjyz2uJ7Ethd+DP
q140Fe6LMnMdDDSOT2ViDKxZ9M5xrMn2aElyuxp5uk+TT/wAcb55Tq5sB0CsJRU9OU+fnuwLlD5p
yxDNlkI4xDYir7BvTH7C8VkWE2x0aCjcKWmeS0VehXiWUAVwZpPSNIV+0baJTumJfRsD0dUJFWeq
0z/iFgzsTXkyIYr4R8hAME1A7Rmd7SwM8lBv+HnkarHSi7bFzUHYFyG/NY1+qpL/ftVvVLpGqenl
dSJVziSEuQNqJQYxkQJ1Hi69WTeVXsCuV+/rEwbauQg1gOxCNo+y6Jqao+Qbf7Tc4z2LZp+/3bB/
DXl8DkAowmCjxjp+i7pAwKCSx4oYW2FkP05iJMc8lqMvfagTifWtPSjNeaLFkF3wVtIWDAyMlko4
ukpJyF6cBSor6Hrylxv9VIy3f3zrhQajcDfIlror5Rl1DqaPZCiHusve7Rb9XhidlKEHieYX6Ah2
N/YXxseRus0QrwpZF7E1aBkwbrGAUz6rMjSA6KqYsSBLP7PiJSXCr+wPZPGn+7/RDB24+MAlLVZg
Om/T1gskNefll1ihic/cLngyAcKKSYby7RjVbsVcqxRnz4sXlvqa5VaQfafvBs9iTZUMTtpyFnBs
1whNE5qJG21o+UCuufWIkKWk6BfSQuXiFWqimrfMX2uDNNpi4b82ADb9id1nC7ZD/+OM+E5WgAcR
wDZ5hd5rsRkYX+b4OnjUucW0U0WKjVg6toeaTk5GuaOJOQyq0JqW537gVPBXRTATvjGNL0NYQGmf
zk4QOos2C/bJEYE6j6ssq/mQx1alkAXKhhnRyziCkw3lvrRVB/YCCon8MlyccD8p6ym5IH/IlAT2
+CdUZa36Fdo1vigCNa5vvYHhAuAJvYGMEh9LMF+VX+2OiY/0kLoJdmj4k4rcRixV8fI8rs0+vvdO
i+sZcOGoIwHYLIBa0KJSNNnbnY7/sE9mp3IpUbYXnAn7UM53b7dFXkwgHcWiGO/RHraOGIQhHAJO
4scqB1rhgZSOUxPZAWZuGvptkvNDbTmkxlziWJx4StxePvjnZOmy8/kFT3ldme48YrByCoU6OuL5
bwPHjBr/zgtMToNVMe7uJcZyGn7iAVBpH/Drs/SSF/Fs/MpZbkf+cf464NdpRIT5xVESH/wDvd9n
8mvovUWs+BfOFS6vnKSMdL+O3PZ+JcHWBQL8KpfaPHZe8N1DO3ZYv+EMdoEyAX+8R4DeQEqR/06e
euSjYzPdvuG+jOUyWN+aevqZqsstnFLmV1arl7RZhYxrD6/LObG9tjfno4CjSX1POmGJJA6NBohC
OM9RTqGcVYJpbX518QahmI0ihHvWcubp27CRct2TkS3YWuvcDt8NUcAjSgBvn62qrajwQ6kz9LcS
J2nLNSseRj6mLg7rK4W4lkrqSdiceXg0BiHavCYRulhGMQHRUerhLefv2ceWMCDMaJtNSKaRtKUE
FHKb8DLFiJyIYkF+oqSJrKzPXr50j3wjbSjLvlLYOpHnrR5ViL0yh+otDA4b0lLa+FFy2U8L3diP
MF6RWA+iW4kQUJaPG0tkVhSSDX4Rc6VYR2CJCLjnjulMy2pCxXkHhPNPqlX23XsxhoxZqvh3dPvL
Rt7/x/+MMtzydXpW+2wv/TDys7MlOdV5iaOHbMFlW5/igw7B7H04G/ICet+2VVXCxa2DsarYd95T
xFtavrHSn3ZT1pLTntLEU9UtuTHY4OLK/jFP3MSlIvkkvyZjIeuX/x1U16d4xYBL1o9woch3+4RS
pOGIT0NEa+G63JKt7rO0QrWapJBKg5I/K9B/tnSVnQRTqEo41vmmbHOf/DO9Zd6nkDs3n0Sm8E+7
Dm0v9dHxdVmuDUizXcMY94UZKxLksZugYnBWhV6wG9X5MRWTBobbSF6xACcig/zuN4lAZJebxL1A
0JhrDk7QV8gHPyy2NwtYn2lC5oOwUV484KeXDNts3fte7YkvhPZK0zIDSrrcOKIyKKiJZcky+St3
6qBWNYeu8YhHhoU94NG1itxadrNd+ooWvPfDdlLgiUSSYGmS+OJshrkLYiKXOi433ESbWr2Kiw57
izoKmktRS3A7KlFfBB2VtDP3fQERKTpO0ySyMBLIi3dDAijFj88rt3ZGuzfvKR8Qfi6l/rB768pZ
HtdH3t3iTSy0X4xfHDXvlgnRTJ3huiLi8hy+cWtKY9jJ4CjCyf1fpnui6/88hny7LoyFoLlMp2d+
6/Gdaa689TAKEBat3nLOGMInRStXO8Gq6uyrKe2z29Zpt//1qgj/akNbmHT5FhQxts2QQnzJBjcb
wAUQgKJliWc3W3cHpji7nprfvoTSCQDxQlBubVNt2zYS0L2t7/3XHlSLF/b+EVVokvsdK0oNqRfk
FscjdqdLpceMCZLQBZnw0AZPKiQwRqcvu9I6zg5yr78muN1hesQEyNRP2O6xVOfZH7WdjZjI185i
iMISf+JFR/0ZWEPNFOLIuHJC2yZzqp1PiWqdlrTPGxbZQbPbsmdpjo+1aYEEmGhbf+++csJys5K+
qHQAus/TdOtb6j6g5ZHwWuLQL+wHAA5v4ubUC6EsfC+rNaP45R8Wa65kPbgyfi5/eSxb8E14dUdt
C0XIOB5KZL58Ed7j21aGvZiGf0yYfsldDoZBq6n3UhEXuyk6iAsULIN8zTzgIAbIekCVXEDdKzit
Q0hh7yGwyXEyNGQ9KJLtrDNQM7qAOQ/AZUjAGoC5qVIuip+I3HJFJPsrooCWqV5DgWxloFV/5CvC
JkiEPmfMldAv1i19jzg6pcKsRC4nSJkmMyQKGhUM0z1R/Q+dkkFtCvijSzSCpYn6+omkNNPIeFl1
3MGlqnpI7Rh5cg6OikkRA0dgugcY4E47jI06zaVHV+gjLC9DplqHkbV4kc/WwzrHDj9sVSzUGF2E
VLQcWUJI5D3HGeFbq0Qhk0/XkFZ1mwQpgF4+WNeLwCksrxjrghUXx+c8iDrv9mVm2k0X/AJdY2mX
D6QCIy4d/S7FD6CTi0sZprG8kP0YQDoBIm7hihXX+/PsRXWQkTWvpFsbjtNvPdrYCtkGslIdUmYl
e2aZ7SsZSDsMgvpPDIWGm15o62naLUp4pVEZ+67ZiaEJ3oXhDVPLi/Cg+zyDKA+EJjg2mt/12grV
9UQ6ou9ECkm5gkLZWj3ZNxn34c1u1DZ/5i6X9dAKQphbdmLoAYny6Ew8ybOsvRuM/o0dVcOtfWi+
5koh8/nnI56DdnLDds2mIkj1BWMaQ5TlUmTscWn4p8Mm3Ru9FLZCGJabykUsqNbfNM/tzVnN+60b
AmG41/Ro1cXPxUoQFodpIZqphPiaCrbsAzOnQ+0LY8C/gNkGAoa4a/sSJc4o+D7turq9Xj3GiDYl
zHYV/KVRqYjkF1uiNGTicT9v2yOW0T6g4sQSlxuRZiJiMBDASrq2xEc69uaZExzi6ZR9Qjpfa6M7
FdtTH2nl00dIEyhT13JbKr1wa/M73t1FjefHpTjMp3W/+af8d+HRrRNBZ+Oh7/+93FeT+iXZuKX1
Nh6XC+p6IcL2bbhT9Dxd4WomB5wmxcGpS7VKqUUBSX658lrUeX6akauBFdZ4qhSNIpu+DaRInc+x
rklpNP110JzMytw7094Qhgz0sY1hO/US3wHZIaseUTjPQufJi0HU9bSrgAdnfiHlxJRh9Io9jvrD
yjUQUvWqXaFwltHyvH9KqhPb5n9n27W26oNehQQdE4LbT62HlzjO9GH0vDuNfjVo8b9zcxNdhMyV
oLALNvNySVWKBiMWc6tyz4sqANW9uYvEBEKKfaJoT7vcEZF6Sc9221Gfo3Pj9CdrmbBOxLPC4kEz
JBPTwEcHsRprOWmWmm//CqnqF2+RTnPSi2/QA+SsA6AiMj2Aux2GTpqNHJBcRC0o5Zj9Ch6znCoh
7bD2TaN5ZQMzMvY/OHjKGT50fGd2d23gvHkbNTZ2Z8cXn3VamrurovTTDxycszxUcGEIpdVwCCQC
8ua/HXf735kinsxg4sl4jdHaBTagheOSphYULT/JqkFgs5Hmvkm/KVZbqCniKbIwOQ6HQoIGbepz
Z5/QFhciArdJVnsQlvqPwc0lWede+85jY12EsibRUfUAO5BX31h6PWFUKCliarrf3EqbUg9V7ACd
uWOsouuUwfbrfTa5JkT5lKthGbujZ7quxyamy6Xt9Hn5Ke9MXIX3oQuK4hQtfiymtap5qSkXUl1t
kmkY2PUPL+Ud0rvLWRcPzcnMemqsR9bHVE+docKOrRnypvxF2mdfW987plccYI8Gte48T9F4+9Bb
GbheFa1yNMLf/1oLFQ6QYzZ2E4iQRdXkS2Z1moxO3UH2TlMXEf7mdDov5e7j9NaUxNpiR0JjN6Yr
sLAz20TXqDgXP8Mz5n3VnPBLJB5bnCoLaEbRPmbHqMlBNzj3vrGd2IhVX7wiKsyDo9dAXMJinQ7X
8FZfSlL4dC1nNB9RqKwBbxpYlykajNrvTcJO4etWqK4GeGFmx7V9VjuSOdVu6emnxlSzb8PfKIy2
JupcUU+JSeXSv2GQ2ftwxZcNsaHq/KT1+VNkqZAHgG6iaWLQJp0teDfDf5cArNUtl8y850cAS6Jt
Y5iMTDvLMYutCJeyNPYDo+BwF1qYDNM45+g5LZZIj1BeVcg+WNaJT277IDHJDm8qwE+yctZTkDjz
VRF/0E+mW6B5Kr7/5HMAbD+NBM+sRpah2wlKQ/ihrfSpn3CnXkyUox/ss989Xe7fZeNyJoPecsBf
uxLTNwG6nApcbE9ozhrqTG0iGEPd+C62rXKLJouILg08oN3OCMx+Wge93xS0jZEolReM1kLnzNnG
suRF5c4/3fU7RmSh1+TIMFvlLJFP8wwra1pzpLfp25oNYD7crd+1bYcgZzLpr5nA6B3PTMS9MpLQ
Cqyx319Hhvaf0j1pz3iQcCp2Lce4e0Enf+xSM3frU46UOaJzPNflB3NvwBTICqRKaNOAfLacrv/K
iqR7RIhFb/GslUNuv6SSDI49F4Un+6xM6GWrMe0SUFGdOAAM4RYWm6hEmPaTqMoewD0c5NJ9shEJ
G9hFb0HXsh3qUdT+RpkHwsda0e0Plsk/PBtCpU1UrB7ODadKSqM8SSoJRPyiiLYE8T9HboAO3v1R
y8WyX0MrYZGSSjxdRcsKZs4pM3aTlGx/EzFn874t0C/ib4RlQ3hxkjpO4Si1kbnIFGcq8zCa2dhU
MWzVCZUJ0r5uOiW+OBFWPLB/XAJsv8WdA4682ZH0HEHruT00UWaRn0OJ05t1E580bFqCP7HiNJie
qzHImEz1Sus8YkOEE8hYBLKiFnZzMb3PWPBf6Vk8TtdEP2FIGiBdkEGHG3yAmKAf058nA9wksh+c
qJWz9eIx4ulhWT8F/DJiTofYMlLvBV5DeTP8nBS+EqPmpQziIIjrXtCqRdzGRZPPjpbQ7wnS/OpJ
kLmGlBTV9SFKL5IEyMGuZhzm1GyQZjmTCKo3OF7gu8zFxxdWuGCUq43P/3k9uOZcICwDbk//F+mX
90iq5si+Ditoz2OU471G4Yev1lptOmyLQXBRjzTHEfH/nDXj37btVReIjwf6nyO87AD2M/Ue1T0N
ZAl48djCS7eHskWpw39QAcvFAYxay5kXjpxqsKHOaU4jVW/4/xdAr6BO8lBSzsJjhwcSHeP9To+z
aF1SjK9TzAh3pkS0N6eyUaAWNcagxTlVnbVKfggMG+Eh8n9DYSv93a/sNddunWH9ge7Fkj+u9mqc
sUDBL5ooTJhK/wwRfvVxrbanTk8jivdxYDWWmshfeSjBaI5yZKXcKUFUGFD3hSN7EnTQ+bnuhmC8
FnV8YQef9oQq8ALNfr5HY1RfYeld4VrZeW6ZYV69uAaJ7qPnS3PE2guJD1j0yIbyjQqA9YpwR4TU
PDThlT2jxPHGpJDXKI5bxrzUb+c9A8V7uvTM4sBzG3KqrgDqd+YzxuzamoXxFC955cseRk4PBvN4
9sEjqOU9wrnzO3tZZd+o9ZH54yfiMLb4Ps32LnyFX6A/IkaYYfn6TfxE0d2DpJgKrtEs2DZ33Hzj
eKaUI7thjhw98aOzfJJRRIo0nxLIzCwqmpmrSgvwVvsQASozfeB+dm8QhfDtTDx1TzzZvRkrSfz/
+agkCfy+gChycF+JfhvHuOng+gTBwovkx1ZKT3wRae1cN6hPV4TBF4jQ2fxcHisdIgcelU5ARWMp
ZAzc9kTC/j7JGGdm69PM3tHdA3Gm1CM/OUc9Rk2BtA0/92QmlW5EzwVB7q9OYjZufa/K/vMeGXAL
1wCvcZJi/xaOVhOz4PY2Ee2cRcjKn26DACXX7Z2PYhzEZurQKUQ7PurU9WO4D0QZxUNlHh0Cx27P
EtBBCDersfaRfJQw81o9osdxY+nr1hyctbhJyz5/nJXOZYcnd6x7QJLMneLAHWfuR8MDGNBSsev1
26FijY/la+jG8tdr8EADCgI24u6ANRa+EfilCxnNN8D3WpJFrO1XsD+FBppm2ab62exUJq9V1iLT
uitx3ztJq7pzj/Cs067bfV5+ZnHFibyWHKo6BMsBbDyumKGtRsIPXJH+JleTVkKA3tAldlY0pb8H
aWB9bWmEe2LESmuEs4yCu0maZn5I8/9N65yYFky8HAvfEtlj2FcoiEBj7RA17a9cZKFvv9lcmkmT
70wZikO63I5rvWWqrZumaWKtBi7zRpFwxnP/LVm5AOqa6rvhqJ6SXA4G1qsS5AsTeqbCjBYwapdD
nvn5SEvfYE1ynqXah22L6n+3jU5qOLi6W9B5O0bHi3qa7X+iDIpEbVvnOEKX1UL/SyeZUkuWI/bK
kYKYjrlZ/h7CzmDXBa5OZ/TA3LRw/V+5qmuVY5Wlm4NY8LnExS/kNfcJ4QdI9Pyp4mb7s8SDZBM0
62/TIyXShN+sWDd4OWmuyqCdIp6VnwgWNWc0xUkZe0W7AGBfoikMHQgVUp4U7eF0VvC9IZcgom8L
nh+oWF/c+ebgc+2+6C9r/NvwjhF10nQVrZHxpAfGwNcEF1+3L6kkpwdUF1xGtgOk9mE/cov54QB1
uGbBEYLohIRnwq5uHMsmQ8BtJBFg/ti83J/cjzfT+sOGrVHqLcO8STPDSj/++QU79030mZAhgpvP
pMs8sCeMY+FLeLbrlKMZLLz0V9+t9ZJTA/Bq9zrSVlaoNmYMnczYbxRF1QgALoYFetBqGK/vvSoI
BEEHHBwrdR1lqqwF7Kcp6YJizbyJNCQlTJjZZjrrpKn+2jxxpwbCTw0IdMHFXjmnN4O786vzGAgS
AAMW/j8yHXU5PfmD1hB9OB7sJx3dLUo5VvG+tUF68xy/j0fBMLkwYEXGLDSsdbA5u2qY+4SX+dUg
ZJDBSdoIjqjnfCCVClAu9kKzL99b1G8+S42xY5U9fmL0rpi+JYFjO+2aAhwci6ftUK3wKeyhS13O
QlDBqhR9AQ4f/jNdj7AVrCz9vpa9NBBmE/teiSqsdzxI30s01NeukFoJwKqwubNL+56C6hu/dCmM
mfP7x33lfIkxuNqLKxrcCKjr7yKtdf/jviCuEA/eyw2OGslezPzonMsu7fVZIlx3D2Z7t5PgkU66
73XY1rCUlkQFYuQf51IRGgmq9PbPtFsuhvomC4y8qnupi6thXEq3aIWSZwnNDrOIB3SMHAOyltZQ
XyQXbCYgL6DRNXJLM52GNyTNEn0cO0b8aEEyoSnLLVn3/BgphV4NIxm9F4wpjMSy7E0nm1PG019H
PmCdM1x2hEa1g3RGRZAPMg/HT+Z6QqXJdKl5qqU6p7+0kWIBJClPD0RIrnCREyINpPr5ZF7sMkaK
JBTkEDg1DUjdJIDbFYIipVktUPGl4TyWZPBKuR9XFMxdj7itJ2HzwT0vaaKc2uTalAzQ9s1nAjXk
hm2OD3ND3hTbu1KcyGsjb2vC2k9efmRd8eNpitGmmN5EFpyC5s/R8EbMM5hmNj5U/GnJMPF2CyOb
oLjrFEJyjz030uyiBnTk8I+0wQTOBmJJI9rVnxvzTTkanmTHe9JMivegPjCJpK9rLMCfgvySR8eu
w4wWbGOrZhLlMJZPRsBMwm36D07i4c4NNtQqYH+RAZIUqM7nutYBd1aUlitY3uaghrabMrUJFmpw
lBCZqelkeGce69Midzgkx7oKQb3gBxkSClz/grEyREJmXPYRUUflHm8WOqaYMkV8p42I6sovI5b5
IVkk+eBnwvJQf5QKObNbIwGAfG1bDbPUm8aTf3B/7WW8G+86usIQTfvZZE5hmlZKdVKaxlWiVCe0
By/Qrkgur9EVV34RsioOQys9y3N9qAFNi0pVKuSSGcgy/cscpM6cOrKyKyXl/AyzKRlRWAdxLhJQ
9ofoukByxOsM5sOH1oufLZ7FIeWphCZADSee/T+sKmgUxN/XqXq0kaQsn+JzNpOx437b3tEoA3WL
FeT5LByA18B9e31ojUo9KNlVAyuMiGCgclmitWZsYhLpFoigK1hKtDxrbqV+ojm885ZoW1sJFuPf
Fj+PaDPkJS3GdKqzcLEgwG8RSDTuOwGAhZ1QyJX6PcYot6Biosu6Yydoy0mbol3zS25CXp4YXO3i
LpGZ9c6982edrLytEpCaLOoGg4KN8f7kwmobVTuEhbYQ/7EgdtvXSZMG4NJOC122PCoxtfA1F7a3
pzmbfjr6GpCpNq0XuD44mBFlgpYBJ7/tjESfOgDHzh2OdpI6UFLfEInj0NMZB+E8staEScCoJn68
a1GgAs7YbALZIpnstQkF+Kg+F09rGSgQXXlIeiwMJ9ZQ5qIAapq6rMyLMR+WDFXZM9sNVoFYTh2Q
9fjP7Q2qL7fQkDnhmM/2mT4LEemF4diZTuvhblZimRLRawh6ucdfhsKFWnQZ1Nx/SpDObW3AL5Uh
LgiSaLKqfSK3Rx7yuZ+EI1YgRHqC8kJxz66ErcRc9XmiMbTZmF1Mo0jUJTc39SPf+LwFK/K0zgcU
NWFvM1j37VJ1JTCE1LdOba1Kc0BN7XAQpzERffyX2/+vTTgxwbRbQMRDMi/rOIorV+kH1Sprnrs4
nNTCDqj2BINw8BQ6FVpPDNOXj1ePhoR885OK54y4Rm8uMih9SZ8UzWtLDirI565LGF0gjl9XiNI4
UWpUpDiB/9vliiOxZYAT/JCMI0fdo7dzmLuVJeSAnIelsgi7GlOQcWcb58UKKtXPGKuLXRF14XJW
0eyUQBs3wGnB1h1qrWg7mKqfIM8pIW8oGaSwZGbkNWqbAmbwz+BTVfS5fnWLnZo4ulIPp1Q+mbJv
VLnd5YzcJRuOQ8VaicPgyl5rTTs7O+8Ba6EFwC8GfDznblF0WElazKB8T3d5p1DsIY57gQj+NHd7
ueRQ4IZS3sSFB8p9z8awzKEk0+S0N5ImgvDb6j4t68EdENVKLhHjbzMqvGwYaW4FQxMs/hrJItkX
MKzMVIafy5dEMdWA//Z+EJFhBP3Mpxpui//sTsUJjuz8HpotQEIhrIWeiIg/8x5vkzjAm8UsjBFN
QvofVtzkYJQbRwmjV653emzqX0FOuPzIZeBRmNZD9+wS8Lp3FJTIboxiP76HAFkBXxeuWXgC+FHZ
vTZUCxDtFYAkmU2hCpzWQa8emqzcDyjZuBjmIJecB/qPNrpOZS8SF7819tkebAqXTtTFsmphB1P9
uFQzMSLw9kiSNdwgA02pzLiYCgM87a9WR4nfAQZxWzFmtzpNHmPIrMMmuUJsiuj6tVtaag9TmzpE
dtOU3s9vITkHBPGm9rALY8kwctZ5jiOGkxB8e/VyPZvhXOO/MHA8FlR0gYcoYX9GZn66fWEIYGc/
mKStdrV8SSDqLEC+DhmyPQIYlYhserf/PeQTa9tnIJWWmQvzporvnNo4HeBOhCVW5LmIztBnG/o9
Y1OjYc3pJzSVaeyumTxzPvRF7hmQ7t8uGI53tI8LJm00HQaC8XQ588wOjak46m1UDQircOxBYd0K
NELiEbfJgUisYyu/SpsCmPnMCSwQtMQrTJD7U19juNACa/w7zU5i7ZxqsRRFEZPXNJyL90brrBpU
odET2vnBJkZotrVckGb1Kuibit8pwx02waKv96G1ZvsFFWoYj+YlV8hSYEZU1niN8HZBsLTu3UYz
/ZfWI/jsGWV6QT12dDK0drAFSoNLZqqj3OPGj+f+SBp3NLLR0TlAQribpvUMihKLYltdVJ5AjdOK
f5ouZ1cMHgywN/yDSvI15JVCuwSXVlH4J9k1VUe3Kg9c5TPjBSNSxTwYnGTZr3T0H8+JXzfaZI/I
mcXpuMVkTLke2bzdrE1tUJz/kxNyxcFWAAT8vTScOQqseINIEwayJrmUhTmTkP0to2uKqyNHd8+8
bEndzjkSRTPibjdJg0Jjii4DCPAoVcWEHA/tMETI+RhvBbhlr21g2/3sFIkd/4qrO7YQIe0VeXg3
JThunvavsOOa7PzDQHOgIhWUFBrNXSXQkT4fMkRfeYJxdEhhm5HjRWTRcWsYuO5Cn49xG7cC+B/K
bAin8ARDyvFbMk+LjasR+p5606qaH2p3M1+yiHwiguaXqeMlqXMZaZbLEEtbi6NB3d1f+ORwX/ic
WZw+Ld1+CgCR0qLt7m55zyG/LxCIHw2l4s4TTHOEMvdhOItzWXJrGhWVor23HE6dOgB5p/DahP7V
pga9uBya9z5O5TyaGLF8wDr+/jPLwYbdA1+Prb0/Bi1byKRtkoq0+ce71RKrI3uWSjjGCoj+QBle
TvYHK0E8XKOIZpYLus6nzdBDUUFTLN83BBiPMwhvuyIhak8p2LSoYANMRys8xN3RRUO/aeNvSsig
LWiChz+yQ5Cw0BpMTXoVzU8jrnUdLQOIZm5vrrNoQQ4xx5Xsl7T3Homz+xl5dT8HeeHRF4WbxhZL
99na+2jLsGzusQhkjZzgM62RXVkVUATZoPt4+j/O+WIHZ+ct3lCvWY3m6sBCmdHGHZete+qDqt7b
ThsTgJ6XNgPyJbjx5t+NsKxhML07qoPumPEdc9YVH/Wf6vCG+ZxZLTLWe7Ry34Tt4e3diZxH1lWB
mbE1cPJbTqpRL6QQY6YPV0VubtEdLNE1E1LoKMyV/kFWfFh81bEZVX7MSeUgW0TBscoOn6lu8SPE
8EzPrDmWDeAfpqxnL8+CPSZ07coBPyvLIK1U0qeXMrrVuxr/fL7EMxlh7ct0pdyEA+yxAicj4AD3
9WG2PQ0/iXwNanv4iMWM6iswyslFo5jmOVPkhOmVl2LfODFoOFH7610PYt+/1/FnK41rTKAqXPDz
aTwLj6yExwjOrMFEAjrX6FfEUTElpniwDk6LiaaUCAYmETTR0PM5h6Z5Nsh90TfFUJU61YJO/ez/
j9llxLrpIxCh5GIqRUDU7WDTgFTgN0kI+PAAxikalqOlALI7h89vhI9UyXmngStNWxPRlNvuUd59
jhbZYcOj5J+n9JK0zPovEJ713xQ702J/Ka2mV5QR9Y/OfdAr+IqAXgFq93EWc/q446+TLa2aZwgv
wlib+X5ygIo1s4tK9xAcdJG5Y9iVvJ6zPlH4L9F3yV6HM3lqXUpP3pmZmlzdzD/A9qWhGNp9Xvdz
IeOPdJ780dcQeaB/LO91ugVceXJtHtzNQgRCkrIqiD3f2ABcXfZcVy6QyEBhCDPLZ+oAVW3fe86c
ase0sHz3xrfvgrKvUETRyFgBrY9StfkiaVAM/PZ7fAjrt//l6ToHRSXTDZafW+p956h7QP2zD0Jk
vANL6+cS71E23mUIoAOkemGcKqql52ga0bufq2/zLFW7ECWf8kGUy1wWx6r3J1gUYOHI6IASsPdA
Hsn6Y9No8GcqrZwmCVBIHN8BAtQnqPmgk1Zls13cSRbAKoDwAQUXpDEw3G2L8pZ6ahOvfX3d6Hxi
1QZ7YhZRbz+TZJ436ZIoWb4QtGI+gB5QMtyWYHj6PBL2mDJzcbJokM74NBqo/JAJIwuMJ4ZjNybr
E4tqMKJScHihBscnqxGl/ZVsfjYwG7F6kvq0F5T4ekudDdvZg9JmMR8UbwkZvZ0Xa/iUmheOgDQb
SHEz0xFsLJxTZzTu5cfKyiJTtxu8S0s6eoxZ3rxmEZqiu+wtIbMjAum+b5UWPZbxIbShk9oyxg3C
Pt8ezsKUX+mlq6/KYLejbehRAkAlzHLJaGXN+cyuUDaH0i4XRiqfvYsWwakkbyPAeub7UH9rRN/z
11MsrmPicrKHsMrWWK98LM5G7w9Bv9yBLOYeoQsTlSflTZ1rF/NT/Fs3qgQo1yWFLhTnfnZzn0js
gj78hI8quz6WK3qzLHSFKQSPms+KataHPJn40UksYPhIF+T3/wf2lUm8UfhODVMW7dDLY5ApcrqQ
nZgPy0WBvTwo22FKr9bCkjGAzFJUDFGRe1KUNzy+9RUfEADDzp8LYwwrmZpDfekxPTgsAn8i4Tjb
VI4i/1QkwVCEjXpRC9/Ls41N+/76voTXJyE3835oseTfRTooN3ZGjmcBW4EMkgGImDI/3sOacAkx
H0RbhDjCL9QyWjIUEuqc35UYszj6HwQpmFxQ1cOpIRqkuPOS02xhvSxliQYoHSPu3vb1V0kJAq+m
oqYKxmWjnrQ5Z/CFJe5Ku5hCedU+p/FzqQZeSkQEqnZm3C5dWZhBXAUj9P0K9qg1avAVOA+KiBBl
UIaJQAFMj1Se52djMUYgvCd+dKMYVDjrrLj+xYUO7hXkomVeQpEwJLTvKaWP2WptH1Q3N7Hay1Ay
CUl7ho+s8eOANaXsOhfTIqLrP83DAv4oa+GhAYtj+Ks/dsupnrVZNPL/T7zQpMQb71O0DBu6+UwQ
LR/ZVxImVeommouw/I77YhZzRUvNPQTzEDvWyyEYciwYtJ+hgcwTKf+27HCs+By4vuezdUcrO5oR
WWDXSKqMJzrMMdlHtaHqXCBAGlwy07E4hDsJLfZh1PSLsVfGaONZssjqllKXeZWD3DDGSuuc7RF+
ytCnl7WRftrW0C6kid1oGuFcR7N0vZxqMbUAfVsWIZPlYy9zGH5OcqrTBbRTp/eorbPfl900oOhD
5U5mcDypf69BLa033CaWuHXkMTVaYnZuYTgqX+l7UvwhuEUb1m98PucfA/HUgdymKtjGZx3OKg0o
qnDtQAQW+oqSH2k8a7r5iDR/eLZ9RssU7/x9Qm0OylvSzaKySwD2uAOuIqINzksqpkLxk0K21oPV
PNqMccXvvZzJbn0d5APOqeYQmD7t3aWbWCBU1qkTEtbU7/h1GH0xXKF3KbAodcbk6uKIqbZXEMck
8cbstD8Zhh0TAd7+tzRnBdA35TkuYdv6mZKZuHUeZFswZFBHhC/ivvxLcVwo7Ox5bk5AeHAUs6rX
sSy97eZAtyDKunO7ad9JLfEuDWwssDYq/H6nFFlsHvfuIqIbg38N1/48PgpiyrTCvqEcOb+PJPeg
sg1fQjfOCeAzIIwgXzmS33QMh8cBe4ST/MtzR++6Sn5KYD/XWZPHdjbKoqoyRabovWPx7mWfS+1m
eOfMZa42iwSRksKPc2+f3f58OhTT94dG4rpVsDH6v2aEn0HK3CUGQSqsFBgJ2EQqJZ5DjjqxQ6X8
Fl1BBQWGgx+w+u1F4TT4Sc186Jzcp6AjxFI/bXgySg4oWJnXZiIf9GlJZkzrNcDsZTBMjJXNfv60
qiIGus2Zxlej6X+tQatsf8Y+FWXcrCxWDZ7ZykioDad0N7vACdVbuoA8b2n+mfG2ogP2Hfj26+h8
eT6TAEUFjWBMRixlsS7Xnb/N5hzefhkZcoDBFibSgKn35yV5s+z6tFexitisa+g0M1cmm1eEoUlb
QuAMyr41lkJbYKAQDqxMImSqWMOZz/hdcVpT7EgG60Gye3ufkJs8rsD1o5tKpLJjcY8QrU8iJ1jS
EP5G3rDRcUeGbLF566bJ07CXYvIJuJKjOrxtT6YZKN8TJWOL1LlxyeT0XfiB43pCRRsl9OW7Sm6f
GrfWut07KGjU8Jgc9nwFbd5nKQOPh8UpwOxRN3OPCiRSVDtwhiSZUy0udUU2VbTa7QLYQtTe9dft
46PaAxyAwpIjTe+DCDdBRRcpUsse1X/4Ayd3xpwaO7uKgoZ0Ij2FRD88j8o2gdp4dkoul0ZDiX1h
cnotXze8iSbTC08HndlvTdURWDz/30XkZwlpoiru+JOyf8jrw+PxDIvgqzwlO0/aWNT05uUD+6PO
F1NJ2/L8n0speWTqbzp9MMm0QnY+XMOg+iwLMk+WOoiQY5gVibFomm3IBhqBwOfsbYbrh2QaWolg
B4n5wFR+xbNrWj8vtLMQzow1PTe3obFQa2SnQE8J5/JjAm556UStm351fT/IdTyC0EMCLWzsg/Xg
pY5hAQdyBN/B4eAd208/PzEKYjA4CXqvTVfvcG7QsmXYPLrWhmOD3amyKnC20flmOoVzlOV7RqWa
XAalv7f9edMmsUzoY/I4NQUMPpJsoGhxnqgICw7pXiW/NzQIvNKuMPOFd4Nn73ANEZxYbtaeD5Uy
v2oAS0tCfbgU/wcOCc6TRJMdkcYsy4HwoAVMye2ytffAIUmIYsiWExvRKz4JvlC/8cOiT18elfI2
E+qcSCgjT+QgBi+SOHQ44mlhgfOAKpK6m/Ar3weOLx7TVnps8JuTvTdQp+3pmrbZDNWVCVjemkxy
Tt8nu8b7T3mHQCPqLV5mIyF7E/Uh9eJW/AMCb1Xqq5UIc175CIqDxFiqigIhZ+gkmf8MSHCdGHPS
o9tewT1uMcr8w36p5vbnqA/b4Zi1+HCF1u9hywXEZBSeo4OTlEX7n1Ic/6Kz42fqAem3dPiiEq2h
DdR6XdfcPM58Bu5NDi/iDNQPD9AiQuzwym9vhhtKZpJXXceuR6MkD1A8M1Y2dXCHVF5RymIyFLgx
OFUqgZx1hEQf4+QutIlg1fmqRwK7/MOtF3/Mhs9r60mB65L1o0Sh6ko/dkQK+usn2biZK1NPrHQ7
3YnOXQ9cfeIq7tblUF6XZVvY6HanY9ZAFrq2JlM4MEdr7YloCvomI/oL3IvhYgN4wuxypqkS/Soa
mMbiVMe7aTL8jAa8QJAntwgZa8bB5/B4GAus8PhM0XFAYAKtkh6YrUcWnVb9teCoPfsHbLxHz+uo
mVuk5udeusf+43bbUzNhCwHj+iA196+MwpPxHth8AJrmVNlFbxE/wSpJspDAu7Uarce1so2O71lt
jcbI8esz5WYnl1xKKMQyn9JlJVXy6OEBK581Kdy6ct3yw86lLK6H/S2a938hURs4QPllduC7/XC8
Y9qNsYeba4csS6OhEuyx2PHUc0BAMK8RR3NIKODdqRfa80ZWOy2ORgXOTdrUPe+iMRRRij8CGEgz
1ZeMcevb6eeJ7iOFUbQQ7mXt1jDig7fxVoTj05IaOT0mPW4+0CLVNOj/cWmZ3rkFBIPIbupeZnMi
LCCEU60uOYzBvDkJNd7rN6zO5biP0LfOGN3NUvlrgECGEKS/d96Ys/nb+7+OUTYHOSd7b1v8HbNh
SMWUPIOatfI2xYtw/FN0ReZr3QvfgCKArcBzIPMDg+bfw/6wSquBLz3vnUSbEWcpfi0YSWs83cXU
UJ81I3YFO5x/3WMqzu1LA8HCN9Ax/IVqsP2huobluUiZwAaEbne6VNrvsYTAviHU3guBQfGv3Jfr
1AG3Iazn4EkA4iH9xKpQRPdHjCLh593oT+a7IUQeeVpCnt3LSEAmbDMt2hKRl6+pvijg53AHXBdJ
lkgITRJpves7C8COErVnNsZTNF5y0jJJi/Sl4KZgBhjiefA+sURVbjFx9Wzb9z6v8qONaxAEVztk
MZaX6eNVt1HJBA80oN5rSb5tKyVJCPR7hs4O+fIDCnYm2CA91aAEChgslTZTior4rvohYfmagvDu
tb/qMqtly+dJgZA6Vn8kL4pNPspaRX7MeJ5GAEUGTFcNaqFnjYs23wtupNa9RGoRNpHmjfGDNA8F
bVB1LdU8MaV7pgNO/PbkHsebRPw5pf5MOHQ0mr1LomW9E4aVArRlfM+XM0hiu9tP7ZoBERDBIJw0
7quZBPJhijJH+az4ve3V+LDCeMUkTcHaFMIjM56R5G5oz9wCGVe5tQYGxUfT7eBRnkeiK/Vw23rY
hzy3/PkCxKu9Gh0m6vBz7mgHvoa9whuANToyiS4DDSmZMfV3RcBt0398eiOotvoR6mLJ4RBvHITe
q9+RjjEOqzJ+jJhp8I1b8Rz9j92+XY6aSuO8s8LMmMIjbNwfEr+p+yOYSFIOjZBArSUKR3RYqQx3
j6awLru8eErWVQQflbBBdHdcWIFRa/KQQvJUVgZUNr1jnr7ROFJc8rcBEx/mjz0AFWmVMwG6a8iR
d+Xy+RPnpm411MLLC1YTJ3iXADGhCz/kd2iMJk9/z5XoTKjPkQu842/6ZfFV92ootDxd3YKTQ9x/
xJvIvChIcoB4pDeNVX5WarU7416p1HLY3t0bXxnCXU5pQqWHJ74W+1VsspRpZ3JReKPD9Do13VSI
EMXlI4R5mUTvg7Ow76VHwQ2o0ALZ/yqrINZtND07mNhgwfnzXDJ+7uwxueZjRdOUJrN/X5XiUpkl
rDwYALvvWLWS3YeF0oQdY4KFf6sJU402IWkSqevaEh+gGWWtx59+xmw0ur0PGrNTnxLCyHR3+N8i
TN/Kp0uOMrtlFm5/LEHzwVekq+vdtl40YWBPLZmAM/xlgzql7GFuHe3qmLvXMxr1Lvmww5fdc6iF
hP0iUoNnGt7dUxbjbFio6kiZHjRLcqVFdoCB1mttwrdG+cWyHXaEpTFszTJ9p3gCYlNVkecJPjil
WZTl7wzmQDctYpTpVNW7KOS6AC8e5qau1yWGHVxofXCSkMtHDXM/gIpXmo7XlX1Y/0gbn54JMiwa
/74bLcGrMmTARkbUCsOL/Nz0OQV8E4eLxwSTv0/5cS6id0XglKjmOfaf3jmoafD6CwIfrX1bm4o6
STS+ChDXMbyJS8VaDI5rRvCpVG7nOp+nnx7oB/Y/vUEDkuVlYJe3chJ+R/hWsttEMtHnKazS9rNA
8ttPYIE8SepfHbzmM2MMQd5lxBbADdRDnYy0ZEKzIGnDE8GSFSo+rx4V5T1IuYbcqSWWk5EDmVJG
LkOWk9sEPfEUGkPRWRR5H90wMTNG2brdEEXp1tderwzwgC2xliTyjxIpPW0VkU6vb6ecbbc79dpp
1r5RXNlNhbD6OuZHbCpfQW1T2VeFEFVfK9W5Q4yDKpFqyjDhXrxbblk3gb8r8tmiDtot/a1VQhSL
vhp9sP1WAGVMC/bzFn7eQNbcJX5lnbMHQmRdvthg23rK5s8wOlpOn5FUeoXAIByMUAEJa33Ej3VH
dCVct/5GD2ukjzpy4dzzwqOnvi2dRqMyNmltrOVuDoXMFTvyalRSoSwlO32tVCxzvDLzSYnLq5jO
VYysbPiWhcxOvbi6DF+awt0f041UEIbxfRXyT/o57nI56juKjH6cTJgIiI32a56PiNsxTQKBWYw8
XvrAreZr7RtEHPFTi3fLy9NiZjcwKZuHtjT1O7nJZCcQsJ6aMjmjREm2YG8zgm8OOhrG78VCWaQI
Lk9nQ612b6vbomfSzToajdlN1OA9xE3GlCA7hDjFufXAkif5jKrESeKkAAEikpGfqZtwir1sJodJ
iAvzROWsj/onUpvyV6RbA/eXGD9CP4QzPRsbzqvUEa1/QdmT4wQS16Iw7QkA9av9yynX8rgcbhOh
OoyomgFC+7f+QBfAoM9wpnL+I7VYFSRzxOH0LX8ZhTxQG8MoZz711KcVsc1zCrRuJqis5OIYRCrg
Z9dSmpOkXaqy+8hruS+32B/q/ZJoTX1mI1r8V/3LoL9b+P//oe3uYdbfLUcTSQFpTla+x3yWAbHD
E8CsC+et8ihxJcaPusLdhzGXlPZcfgSoO3AbGEfC3HSzQQ7yVCUHb9lEkgjCuBenHx2GgbgdKZQL
w68Nzr9O0JRLl85NspaOKBzmYRz1W/39n47tKQiS0jtYvl+ey7vLKSXH2cc4SjGBQIQyMeC8Cnie
Zvcbge9siZVeOpPWcJAgc6AD1eMAx6h5qOm7y7BB9kk/daoFPPyE4BunrAmE5D119IWCcuhqNWi9
8mMsGhaCU48VVtkDKuaSE9f9WVwy+j6Hqqtif+PPz9O059wQiU8kFTMs/3Y6hH3zGpWkOsHTRNJK
fXKW1RkICiQKFNZOJhFOFuPY+/UKhceXoI78vL7xPmc0UkBCpqtaRE885M5Ub9JuJ751ytbHAq1P
ichbAjhvvSJbAAHzY8uxuhsHw7I2Q654Wpq/Ov2fD7Fk1F8yxFAm/M5ORa/DHrM5iAptkC1yNypz
6dQhCXGF3ud2k5V3lYibPmO/siSRDOy1qmgd70jsKVA7yyxpji3Cg0PR87ljLRWJOM53f/8Sn+hZ
sgi4qtREK0JttFeDCpsL7++09vtaJKlxP7UTDUKGH8jgxG2QQIzDfiqjy2GbDgkVcqg2gCgNB0sE
Rk/g74nR+BQN+IAvfsG0fZS+gwh6+CyAkmGUvQV+RkKQVYTb92iZntVbhg/HIlxYzRcQ9OXUH7Ux
QpqQN19QNQszIAcpk5fRp2kGYSmjmVZyaxbx6sObEIk8QCX2L5P7K6yWlhvOhctmsL3QvB1GsQvJ
RPqfzrC67E8KmNl+n6aeBXuMQDf5JXtPQpaEGVLW/Ssx744m7Qpmvzd0DKf1culJMft2/SKSxiua
P1J//hgU/y9gY3vky1pTde6nov3Yt3eYwgNqHAUpM847x3mMRsxFjHhm6VkH8rI4YcuJ7cVaANww
CuXtgJ8UtRQaNegB74EVGt/sTnbrpas6YPd7Enx+Ert+CMcKGoP74826+sfwYNv3XfxoaYMzpUhu
fi68ULG0MYCcYFNYAKLdi9SKfICIjp7fxuc+mXMkalLN3EpJVTCD+o/7pupxwJsfLPyMSFMfYHq2
tT2QUKSrB/7C8Z5tVHc+k79yLb8/ixvaQ3Uyv4iJvVjejgDZi3xtxJJc+8EDet1imKeeeNkmc1gh
S4TPXyzSh5+fwC07gbYcCTqf0Q7vwBhkrlYwpbLXNnfytL0EVjVuOZkpdtlpH6SRNK4Ha5yw1uT2
XA6PBiekWzAxVo83O+54A7NMxQCrbBaCZBIN286cSVlNCawy9OOx91A/s7ysTeHgTszcQAStV52J
AC/1LnO9AORD5thgzz2FovqVsqI6p/jTH0clVA8LJV+eER0ZViRBzTZOfwlp680hp80xIs+7Bjuu
qiMbWPhjd6oNRcqr25RCR/zSlp4kZArc4c+J7CvGnhlz32TjspDWMqxL74yTapYsEfc7yFFm11DH
yL787pNzJzmI3+NbEjrB1tTJuL+4HAfEWshjrRBIY/CyTWvOtiFp6Vr0D12AiFUZ2Cx1TREoIpIJ
hF8SEQIlE8onQ3vLbYWc1ScWyazY7Nv4Ys4wZY/HiDPwkD7QMukIz4c8LKAgBxQkJmEYgMJqvgRm
az/5HiOHbYB0xQej8W68mTnWa0Ay3RlooxDbWAR+N5C/Vrjt25cSVsQTe85mPBSLqiTf3jm2y+q8
CaZFc5h79PUGsO944PgBv34smD9OOU0SdJAWeYbt9U+c8z9WLHgaSfwEqBUQgj+hJehVNQ/Zdp2j
/NAm5u16fdV6ImyA+lRNuMNfrsgKnsWZpr2TBPP/Esqmu6Hn0LJwsaLOHTSAvN+R6Z+6xAQH/Bcm
Jc3gjCeuF/7SBIPbT8XiKHYDXAxcqUXdpS0XXLzkXCBI375Kn0ymrbQ6GjkHeC3cLNUf6MLU2BTS
ieUvxNHacOfTBaTG79K1tPJrxlUeSdjoqqo92yqXbNvyj/r1oQQMRwpkQy/lhPOAlkFApAzW/jxl
odXFAwxkmfw9WLPK7BIWXHBvMhW4nEjpcF748C9aMWgiK+6oCc6quqO187Pf3zy8giv1cBVzaKMv
5lqhUDorJxkLAsgGAYIStp8FiuLSQ0/1VTME56srtGPDYXB7i664AUsmJ77bLf9hltdCwsq+9I+a
xBkrdsrJnjLWO1UWBWDUCvCIr0XvXI4rpMufAM3CaQzdr9ZQmHcJ5apzuZ1pPOwzXEJZ+7n6Dz6b
Al+D96TRvnBeEfrdbC/60GBHw1OnenbYWKR2a+F43zSFHVomyOfHjy17Z9KyX3SFgx5R5blBWoHr
BIXdaLSEud76cemcCoofdBJjPGRkZUkLGCmEOegRrYMDenE6nvILBgTcXOGY8NNlNKejFRu/wqEm
lb+fHHF98rKJ6Nz+3mHLECky05WVLyi87JGHPUldn1ZUMFsfzs28jMj0CTzD/Wk0peg6mKdoK2Vh
0qcOE9l0O9SYffis8QGvl5UCM58HgdZUHWEIZqsjA3XZFz4o5fg0kyftyrGgodibdczDnQu1fRnu
54VwWltkPvZDDKTW6gZgTWDcRlFGNCemHkcdASTn6CH/X6P6ew/sUu7cTgK9o6Ei+fmOvuQHrOOi
10K+7MWdrbTK1wI55E8WtkVDUzO9dvCmv0YKqoCUpDuKbJ1RCqYiGjikkaez873UW1pQHx0jFAJF
/V/EpC0VCaOC+9CTFDYH/4tfJ6OG4JVWJKjIB83N69R6K5hXjUg8eV1VBduBrJ1wLG8bSgJfhsOY
/VYATQhiqUjwY0E+4kC4IHM1OBmdv5tXWNG8i7REcAo++4TuipG9hN+NcGmQjAHGll3sxYU+GsgU
bECulfnSBPx5vd2TvP+ibZSfpfqblQtsmq2PdrYWjaNYCQ//vZ2bcevjPAm5ZeIqCw3KtPP5wBug
t2Ct31D71AoxgppvNh7yecq5lmQdptP9HHPQ0avSpJdJ3vl7Ajcg6BMAeikD5MA/lDlQsZpMFLrv
zbFi03Wl0jBrjqwjSG0LFwGaBGawwfDHFOPXI/gApQjhe/23nHOPayXUsmWaZEtatOi/cQYw49Li
rIyi0Q/vgHFeRrFleOyWZj/7EHooIXY3EtQ/lfsTLe6Ul8aTI3YPLC5iS8qN6dxbEQr1kZgfT2+Q
dACTTIPCZGXa2b9yLUBhZ/PbbZ08H4n+bw8D6J9/ZGqr5+ZdCZ7DKh9uxp75tBrFrP8HtXEYGti/
PjC6lm5eIT3T0n9CkvRWmx08xfEsx/gdZNj+BB3U4SafTUCLIQ2O3RMBaF6apXzIyFtBI9/mvA4W
BPK/1uCz3zXyJnCkIYQbb4m9BkCP20mMNnBVxSAFFGS4y2zE85ZHlDlNYddBlJWLafv2s5bFRRi1
EVz2yUnxcmzRDg3QvnB2rJtJpDp0jVgsnDVPI5iYHHOL7CjOG0vpdpI52esyc81t70uGO+iaMUmt
1cNGJrtDEd5SyKtm6a5cKeHjmhgaSTLwEUMJboCackw89N2SSFMvGd7pZrFbX8GOaoLOC9ggV35+
GjavlbuoV7jC5U+EiRIBnu4rZ2f9str1TeikvTnIHpbKj+aQLpBxVhSIoxJpbu7TTFCzOjVHbDKT
7uQ9oHBHyDX8uAeaW9TfauJESI+PXmgP0M2B/wLu+LQJVU0ACAA5SOvfvjaNeimTLWmkrhHoRjsN
gSv0A/klWaFm0bCqP3uZqSeft/30YPK/BvrfTa9etK4pbrwJrf2Xh/cUPi7PLn1sqhyYom6joruy
4Yx4hNrKXXVuPb2geTfeHo+irP2toMO12wKptCA/lOsQ4XFkFkTgBPh+HNR2yzrlI+O57hs49xC5
lOolCI/FPAhW7ljyfjzREB26nhe0jO8nN4LQ7S1hlMHIVWs9uWlNZXO3wZDdGJHmpCYEMLinMTYK
gL2GeGoV5I/b+6mUCpATQqCm0ga09m1WoblomxoHnYfMSyrzAogwzjtqyDrqB6x0egHI1d+tTm0F
FF1Kq/pvkLCx5e0mVny62dG7QJCGG/t8eMR7L6H+06M5SsAGRTaNlUkTAUbSKwlk1ZgZJe62SHyT
9yPCB7SYskBctMJeSDle+cddKz2IcXQzXXapBv3lPRO0mCcq4uTyUF9fjz1pHNAau+1jbqyKutBa
+mW1ngiSUgGcB7eEuLLcxwW3lPgnMm1CUdqVCJ0zj33HI1o8G2aV2tec4yP8Pw9Ab7FYkW+H2Nxr
wvLVcfWaGJLAzJ6lrvFbrZ/J0zqT2JSJbJ91rE40Tn7OD+QDZFc1Uc8JgKV9I2Ooj+7A7tyWSxjv
rvS0U92nYi2LDNDkAHZ2bmfcO+doujXa2Rq1luSw2rEv2VXOJuu610IhNEn+memxVm8UveRJ6itv
oGQ00LHOJEtEyPgdWwzjqKLjpmRGwQEEloQHvShYFCchEF57x+QRIY2JqE/dLL9hHh/ICw9JlsKw
v1LoXSYOgQmn60bd4Cf+KBSUGok5fkl5d4z1LC1AMjKbOMfdP2/wGGAgZ6GKND+l0mi1fFvTeGgl
EDUsrsMwj3mpbHl9hz4HSca5vVt3aAnYT2aCHM4aUoBwSPKSzQc5Bbpxd6OMywyOYWR2N6FPT3x5
ea6+1D4Cx3Qy8CZGpNcUXZXYUmed8hCfF8MyfGBYzkensQyEoqCy2zri8btHKGrhHXRSri12P3NU
5BW25QfwE6ZbEBj+iucAOn5sSPjCF1hxePgwtQ4B2MFoYAdYkCp/8AySgJYPW90YmSP99XQmwAcq
re6JeOA0BUUAR3gFgsGcYRvF3JX2wJfNuU8yhs/gye1UyjGWhInlEdsMoSL3T83hfnVq/kvwZLYs
D9it+VmyXdSOhWk9yVdk/RtajmLglc339PffoKZOzUcG366hP6892fC56RJvMN03WbM/smVlcHNq
PT1yRFLOwFA3D6tavRxMXFCvHRj9Z635KJJtx7o96gYAaIPf7vzmzzlT69LH5GFZiWnOJUqakgDi
UEzmxLqyvylbtOTr1H9J5e3jAq5c4jd7gpZaY8CHPPg/K5aWM3xDrJupTv4DOfqDrPbaDgo2uZpS
hbhim5pZkw+LnBS2bIIQFz9CYtdd4aYYbVIxEAOqrCLFpIQbZ78zIt21SVbSfb5f2bTCJ8+LiDAr
hSUim9mKNpRbQM2k8MEM//dJxHxrgzU2qbk3U4wn0kHn0Z/SGG8jF/2Nkliy6MOxlgBO9O+2f/sl
/0avHYgC2FkPnSa+HWrHKEHoSjOYqqtoqgQ/bwLPVaNlzFrvX7GcBuQ/5N4CbBr8FichmGuF3E7w
mo4DoAfvvU8+a/nGwrZ1AOtJH6r46GAvIP0uo2W9A3zPhfnPe6ResbTfUidaZML3WbkF2Q+f01g6
b6SRL1u4f2Pbfa/3LrTzeqnHHNLuAcDnnnY2Lyi8MERRUgMLS7ZRrXw6IFScJ18V2XkaPc3OkuLY
ZpZvZP8esuOykCXGmndD1tRK45cEBxpi8BjC2Rw78jKWmySQZhWGUyiacOyOEzFaQVmr60suk7Xi
vmjYyJSJwy1Y0sukg/OuSRX3zT1zW1ugj6EUE0txdXe454A+l/yflt5axS09bFrMfL54qiaLNETv
y6SgABpMVi/umagrh8h1kEVDTxmlca+uymQr+v24lZzU6emrDwtUm42hVMFl19iQoX85j0CIJ6p7
tw86JtIYyJnzh7hzqN0ZpDlwIdyHtgj3HSE5pMBq/ZXuKdoXyJBPHYfo6oITGwoWTJfDKgjYlc9Y
vQg4FCnd8FJsZ2DfWxrEygig7OT9dxYlhHi38juzQR/yEAkyquoFjCaI5Fk6mnWOUpaRZGzJ15lZ
6eU2ymBcM6p8jhhmpgLC0zfOv+wlQmMZKSkmh0QJhWX+gWX4h6ITRutBCJ5AnJp/KA5CDF6vRTid
VQtp3mWtz/OW++lwztaIIQ/2JCPQvstdmoM6SxCtLQT2jG3yfeqLiuRFqueq+7hKSfzfuMDtpvC3
1T/LpKW5pDT9mqFQhIzzSSOldaqcdHKaXy1Hn3TufbMjzIWVnPiVDHIpFOsJPW9bjeAtlvjhe7Ql
xJE2M++4vuAw8TL57ROVUrEtgDJf+ExKE3PLBmRRMhcZslZjVY1iKmosTX7P+8OPt8Ov2dSs9QrH
PKAipvO8dzxEErK8EGsP8p7WxKHR5fWfgetjh5aZrdGMdomU9Tv9bDgN9yu42Ws5HBv9KBnqOOOZ
OI0eHVQgOd8nsD2ASfyCR9pLyDoit4NsjtdVI+DavDq0czz7kgRsQI3KhYiml/fk8QWXTGNvTK+W
ewoYG/sfeDO9VGDPzb/mRHh2Rikr3xS+d9WLByoLiVB3mnlL0rEYvqqpV4K/JWmc3BmUsodLEkt5
snb0FmgsqcNYDwj62ts7PC0wb5tu4Tb2eHwtn9ikZFh9zLLXdfeOPzhnVqSFMLeBBIbrIHkYordF
QDrO6qUHJH2YxAFp3O64dmwQcfj1tRpVrmO4F6e9pe7Gm4fLNoGWiphhOiCMCspao0ZtDLdheWQx
RIGihzEBVbkc2xNYahXzll7ehpNsYgD1DKUANFN8bGenUS5t/oGoenhBzQWeKZzKk1hao9ef/CeA
nYJyg6YaiBX2pzmvcX4VbiX3xUyP3M9Ff2bA3EjMP+iCtUYPARzHb2kU7CBxZbYdi2hMlXDYxMd3
wGx8dOCD9PqDyT68bZTw15eWPllmP4D1RYiowYpwD+vGo/m4n9Vc4VQvVmlltNxbHks+lbxQI6YJ
+jFZp0i2d3EfHSqV5YUiEYsHvqjkhaWIIaA33nYrWQ7PZPBbgUoUkDfSWK/mx+1+9NwsfdweUTAD
mZ6g3NNZuZcIctNXmB6PM1u5T7U9zYP9MDyw9o90xRwNjxsFCmkR8jbqKornWVjXImz9ZqxzP2PS
C4CPXKQmrCHGsfAooRZ7n0Hkswr78OQ+3tkzWl8JWtplb52ZgAJlmIM5gvw6Irt73CX8iDDmyUNh
8uigbHaeEo/2Y3QPMu1hnxi8VTSn7lPAkqHVe2L2K6xsFnf5UuQZCngExLB7lzRFG2w/+eLRgrnm
XzNlMNjvYIpf/bAIzclsxsvvseasywsQkMrNvqI1xpJL7zUJBllROhA4Bz4wJhsje+lVHCtXFKOq
S+EgMd5sHFPcevwBRumpoQU60mEbv5ySlfyajqapWOmkgOWGXdZC/sqJpaXptzXo7oiqaaJyMxci
L+YNi11OirY9kMigF7YSsk6ndR20AvI9s2Jf6K3tyka28Xe4YKQo9wAmGRXEnFdTtKUl/pPE5rin
YSmjpDhyrziFL3C/Rt7Kd+rJO+L2EOq7rNKD+42X6lUBBkQMNADDIoE7U52gnWysVrg3c9v6Vf9h
T9hBqhfTv0cHZ8H0UDrCgG/x7O03Ne8B7zrDgO/Jly6y8aq+kA5ctObLwKfYuF9tTFJ39Y3hRzCo
vHflW3iqr0Tn2D0k48C+XX7ZugbgW0XppfIFv8eX/cvXdJaJWlIVgaKrmbYHxzjeczWFqVAwOqJs
Nepf4A1tctlrrkksjZDApLonvF1YbX17r2mvaykWx+b4aQsJT2YA4x3JhaWfFuNfO5kp7d5nD35M
rSlDokXMcSNZxlERsFbDS7Hc23WZvsusEKlk6hJs7gyPT+7/KMYyGOHXNArU9UaRaMfJP6/OgJza
MJMiD0JsuYjU9Op2oVqP010mXhi9Uu8Uiiag2lsdFO4TqPbuqAxF5oW/KMNCcx77XzECYL8+XJ/k
zcRQ/Xtxiapm9mdD9KvGzr3PYE+Bv1PLEI/QblYmqIct7DHAyZ/BVIHvj64hfiASohhRb4MHInqb
h2N23pb7/kN0812UFRl0C14NjrmTmPySAjzm+xCjsmxsy77hmG1FcQrvPxFEWBjqJb9x3sgIhAJd
Sn4EI8o+YCH5bT4RY79wvoA1DxLgMxNJfq3+kTeyulv3RUOC8P9qmQMT5j39yuEaAaFhZKlWeyNT
STvlitTkDZxGcrcKjhkPQ5Os7TRpXFFWoJrJVLPlc8sC0Ex39JsMA9874K1JR7uGb/UA4FPH+jki
mXNLJKHZ8XDprfrw+XDEp5zcKOQwolhdNx3fMR/npKwa6NTUv7FlXTcf1QTeTF5XJriLUqfC69N1
7W1olKCa21c6KoPMBisRyprVEF9lKV8REnNEVkr65wbLqhxV7ru6wOxn63h0JVGLXeTWEWyUEr4b
RqeCM6bmLIJLG+V3meMgHHB6Flddq+GzzUM0JqHs5+75kJTWS7dJTMQ+FFFKpjcwVjGCHTD/NXQ6
6auhv9aLNTG0PzdpVL9Fbi+kOXogDGaU9DSGD11vGbAsX9XrlowqHz8oJ/BRmcfwLRrpMFzpm/yV
cjVOwWM4v928ahjKFcbfBEr3OHK8FJ+6xzLRSRTFmoUTJys5HWxlXAgZUdhgsdJcCKXSkhPGa2KI
Yk144iy78OGXKHTCOZ45Z/38UNjsYiyeD289i2rZNRkSxHZagkwcg5DC3kxmrORP7iOrJIBFbHXU
6zIibXI0n9jzQJ/X3G3W+k0PBMX8qDSIC11pMwZKwWcp92005ryszpYvsPmnPna4V1wXO1TkJONw
/NaA6PjUSnopvMEd3j5+lYN+A5wQnXqoxolanGtNlLW5XXZ3nrhA0YrxchbP07ckLjN+31T7kLd0
wAdRcJ4AgEWqfrde+TTwS8pdA+h+gTpXJ5bhAkUMZ+qU96RaASHNySMupPxYaa9fpMls5uTjaEpk
aGwXzdqCHNnOA/H23eXSjM1d3yFkd6Knj3msvzTmpS4xQFVoAr0DlE8WaU+jYL7fxK5bijmF2h8r
m3mnSrFsabIAk4IODLt73TEXvvyv+rlZTtQqHE532atVvoVhi+crKYdB3Nst97FAGSIF4l1Y735v
Zx75tU7qq/vVaq3srWABCqJyM4aVJe9kJeAm1y6WBbZYW6lutKxtpqbqifpz77hOs/Z0/3k8VCzF
rwcEezu8SGkM6X3qFdqCeIXL2ANO6ZkuQZOQHFmqoRfBd62zQ+pzleJLLCgf15AeR4mywTaAY6Pp
kJQhZxCdYwz3LxLqLQOSH0lmSlcprYqMuqGWQA6oyincnetXrOGIc4C1mArNmvwhirKHMizyPjdi
5NbWNdyB8gz6II2Sv3fDcH3+xJP+9DEeHqfIjISlcfXlYM4uaO74mTv2FJisVIQIa4qMmuJ2xooq
HzyFrju2cCNWgNx/H2NDj8ugOY33s5sw6o3HMF4DKi8ClgvDf3EO5NaOJIMy51/S5LHS/GRbhMJP
hP/++2jJYaV+dgRD+CtrruwcJ9ETzoRDsq9MhI4snt8uDty6InKEOSI7zRK7FsePebkQ/j8FfK0H
c2LFZlXh8y2Ej+tI9vQGztr+Hh8uhbf1fiH74YqvZwBMRti4EiD4wAjJIv9oVc19o7pnfnG4jH/W
ib05ijyyKmmRCer4ILWYqxRjffaiMUjOjG3/9VmEoUuwQOuRVZcErEYqlKfx5rfC2tWzGBFj30wa
k9AgIrJAClXRnQDtqhd5n+n3dUJ2DJaZhtkjVhJjjY57XU0PqzP8CXjADVuhmoQGyQVabvIA7GcR
DlP23ecGIfu9LJy2CoX74i8uL+UyaB7xcADgfMeJb9ANN21kmJhDDWeNW0ccjjb6keIYf2JaPr4l
0ThynbX/ZLC+xuonjM/uTRlOUD/KID45JFKCmiGy+HYbi61gtOfq/pF9sfUBdfGgu3gm/0FiK3HG
aPSNBXFUjhI7rwSHTY0F6t+5Dg1o+nD9hYSnRfL57kKd07PygFKymPK9u4mHcVkU5JE+BchxnNNG
yIEKykoOZsbsmn5ZPAW9ViaXseVxcDwyo+E3IGUvic8AMY/yOmIEV6RHYaR/3XEDYHSvLwSUtMxC
XjZKCjN6Kgr03BV26aXU6rPGNfMqBSWn4eawZ20I9lz7fFne8xM8xL6pIw/VMJDDekHIO4zwXnKi
vIu2Zyc41GEW6X04Qou7ZwOqSkP9WejxsFjHSWKT1JkJmO7+rYHBKaSIu3jzYZnX1n2CvIgIM/ZS
3eRTaAaIwqNvEtdnANL8mrRsEqRQfB262OkbM5t+Cs2Zt2syS57wODEt23ueZbU5D3p5yMTFWtqR
Cd4F1S5t4pFlnQzImDwOZHF8yFFNyXygXm4O44TICZhAsfGyNkZQiZRL0I2ejkSGIwYXs5hQCggY
t72mxptiCzhaub3l+MqrFOEqWQndzNdH+AIdzPJheiDtD9J0YQWNBivwy4nleIH92yBoMP5gO/CD
10SC6JNBwkFcUesKonFFVg/vn0D6o9D4Z5oULBzjbh6JLQtMDrn23LvJjWTYNI0MGskH1wYwD8Qc
kb/SgfCX/tdldRci6z+mlp/1OjZuF0pf0KnjxcijzuJgVStbpLPBUDnu9yMQ4beXPCjsrCghhReD
lJ3hFlYiF4HgKp7//ciF3p2V61G7RJqnVPDywadeokb2536woAESERFXsS3n40BgEy/P3W3idJeA
9h103pZeyW3mYnZdqsPDtKtbPVJKNqasjyUUFz8RUdqTkaexNvUXEpj7kuzUKla4+sN9FGlyBTXy
AOl0+JTnP6/8kWOabRIDwqIdwnrWoCjyp3gRgKfbRpA/ovuhJ28yFi0IRy1LH77fylrCcrubwcXi
miWcre5QYT50Bm1XnJp1EQeJ5bJKxKBJZ4kqwlyRpiW0E7FpKKrcs0r1MeAoqh36LcUK9Mv+4XTg
xaX4C+aPaFvTu35f9SDZi6HQ13lAFuTs5lwHAPP+KvMxCBws+EryiZ4VvWyzUJjV3jcIhL3uNfYM
dNZocL7YTR9mMtgKgkXXEiX9m0fpuzKBOWkSNS9DIJ0jDWubW6z7T1OIg7m54h3uBbPtfLE+OkS9
QBhEXTkHG25mqFyJ+HNtD7NYonIcE6TVQ9FWJ2euuyBd0EtGupt8BQWS/qTuG3XQMbkGCBWJxLku
NM26NkBDSi99OKGrO0JBmAdbDPubzrfrLfjBpr8OqDGhf2YmejKoaWGEzvBbbT5ynuwkaSH+Qs+r
p4X/pM5ccBRdj96KhQOAJG8inzSYnHtRCmwLchB0soaKGGz11taDDbtyKLY+mYl73xBFXVfokAvN
I0h8AbO0RnHVSpUeEB3UyYqGWUlROPYJRzAR8gs9IZLDqzSuXjokZVfdtsGgjg1EiEcnKaU+VfoY
Xv2DJtGpVUt7M0HK/8BGkR6RomcX5l4wV/JRLzVZolE+es0I+/NcvwUi8nhHsgKVhl1pxthaEEEe
H4WuxQyTatDAYGCgpwVAcDUhtOgzlsiGX/9BOp5N04xDzH3xzEdxxF6x9Jx8AtP0K3P6GhZQAiJk
mpF7lvhiOun/csCw0TJ4zL+1pRN0RYWdDCAB27gi6np9qpM5iv9sCKXArnKVfTsW0vCv6HuBXhHJ
Y/R8MhzE1jpt+HUMIRFB0Y026od/IVsrdq+y/BJpdAFuIYZguhyA/fGZvSkBWyJBVT2f7EKgfqRL
inbU4wgzWaW6N3GlOV/1oTrvdcRx7yffTSkgMSkboGL1gnPhseLuFzBpJEGjRMxI2yL17SlQldWE
80KXtCOUyv83lUHFo/8wDC+F9Iqd/RwUt+4aZmFAqoYNPsLJ4CYCCykTaj50FtWo8en5hoCpKjch
cSavUPadRvkuCGGgmIYoBZvVwtTCzOh2hJgVk8dibZLldY/dRi8dH2lcZ6tYwwm6TbQPRT5RH6U0
/T+3VLbcPAK+PHTrZLX39meDVp2P7jRmmUEMUuRXbfsQ4l2QzkvmRpUV4joxEZvAJCJkfoeQSXph
WNrwybyTH4n5NfIqjkucwz6ZJXMqJqizyZlLjiqhIHuowV40Vm19kwtzdtPXkBtHJW7MoYTVl11v
Pqdlj2l+Cwx+UFR/zTIH52SGURLOyn74ZF1UwY2IxXfHU4ZjVunu5BtHE1WQX6e06wO9PL2pkmd1
nmaDKXZ1PWTLbsEgGqxnGZjGDY0JDfRD6PhubktMu8YgprdoX5INVEm1+6ENAA3bNnA4M+vxWQL+
9Hk7MT9Dp+nHckhL8KCSFNUwVStL/rZvIjQIjjEm1Dwi1hytZYfLOibflCKfzdfp2XpoIZOznaSV
Srb6JYjMzIS38T83itNBVqh8dTsY18+rd1OPukfFpYajN3k+nS9oKqw39lnYoHuQ+qXH1R9Xd6BQ
7d2qVWTp/0XpWdxxjrLnpBdDSu8yaRYkYYOkBisNHUheacfcnFJh+kCmau5jcRdtb6dcF5PGvFwg
9CwHBfaYH8Olfc5MZVPAHzBeSK+4S3Um/tUVPK+YB2ckthq5mfQmZ//dLs1cIZH1E/V0V+YmYb09
mr7ItHZsqvlCF1K8RelzdkK7RWIfkJvOg76o3rnvBRQzwjD9I8/f3rR+Wo5DD5B5vp27F9OoyT2c
RWeJrGNklSbzLiE9Q83WTQJFhq6ZvnGJXF3uPLc+b3MICysY30eKjpbHk2GFkbt2gPvx0UrasAVe
nbyyc1AvGn6i0LyVYAdQeRxIZX6j01uLab28GaJioASXzOz2OEe5QvzBxh7NEFjg6ACJnJF7/8I1
DuqkXTXkZFiC4483ZKUZp+DEygDTqpQd49npMeYRWufugwC5atFpEfPcM1qWt5o5wH+Qf5O7/tGm
cxuCOeXmEjzkC1AA4YGd337apaRJnABqREZR2tTkVt1mbdlcPc641Z63NlQXqI6z0M1EbXc2wxX+
pU7vYh4OXFc9zCuSIu77p+hXbI5kJU0pWUqGuDch9ygUNWJPcLxUVxBYeNdzlgCtUN4ELBeUvxlV
YM+EWp+ihG0CLjLVdQcbocRbFpLy9TLkmFNo1TRiREzWRTcMF7zEQJXHS9bKmd7m+jRpCN2+g1Jj
U+l/GJmS6gSAeCPmS0+AuL75XqoPj28a5B87WgBHsGtHOVyqkS35LdMroDC1SSRQsY2AIBkb1h4/
UXh/CI/dyn1tHPFZQyDKAnbMbjkyGU/vN/vy5Pk2BN215c+EoWJH0PlrpJnaXbwL2cRIz1JSqTB/
8bDsDM4vPUXPDNoSu37ni3xT7EtnK5T+m7PbaW/GUo4GIbi+8gF+IjR38yTJ2gDHoDM3Kl/Rsfu3
YVZnZYCpSptxFq2Q9YyI3Mq1pVn6BH8cQKQo4UBirFocdefRrPZ/ci4FHEYvXB12qC4Eml0yRVD8
G7kCtW5Kl5oOKt0nQHUXJ2wOllHruElltZ9DjuuctLXBJIOz56+S81NsvcK8yNiPU+w5O51fZZAD
hBYEVlFRPFzdtdCx6YmVC0XgZRTq5BSK9RbXpWQRVEal9A10Z8WNKSutziWtfWJDHn7Z5UGFLMV1
h/1Op0dK01oHXJhpUs1hyXTIG4V2HuDiHl/Lg0Py1G4osUvqnqfSHmdZUXOU5i0moVdS8/ohjI8e
fuh8mbnYYeoThJB7qDKVbsEKaZLd49kkkf716ztvXfREz6UQGyoak6QRkTam2zRnWvAh70foDw3Q
Di+iJTS4A/yURWXivZJtMkeW8Fbfrb36sbgcdKzWeNdCl2xwTuySKqPsiOYvSeEbt47dG8PFIouD
Nee6RDjkGE/DtZA2drzjMLAdtMz+xgRhKUwL1gp0lDH2mcE8F2DEUvdQRpanLvjXKhdBmS1IeX9S
stWHSmgy6hNW3QEnQsGeMKTHJbLc/K9OQEJceTxsdadVNH6/nolXwAIae4671Tc8e6ZkZlrwhY9i
CETMa+62Vgn2v3M8YIUET3tFrprjTEhz9MnA76UeneLNqskU+mtt+V5zKJNUsi4VHWN0hhw7yyus
afYaQqntoJUjgA1zrNzqCXm7kn4CMhAcPpa1wuFaOzbTQzi0MEuW+qhRIK07bhDe6Qp9FDCo98XA
p5g6FRGPIa0ImYMg2YovQXL1Dj4l2YfEgjr+5cTqQc2lYdClYVTWfZZzYu/M45kM6PSEeVCPhp5M
aROEB4R0jMIXwInd8jLlHCpLziBgaz5wXiJn4A6OrgH1bsBMYCjrcfSWjkN4LYYt09Oc9zP47+bz
jy7idIzTQWMnfyUv+uiH+jPqnv4HFjQwTzeUdIxZOm4C/t3rmXrEJyai1NSdSRw81o+9j7BL83TP
2DrubhJwqZjDTeD/gCH+u0Hlgasq1cAXVm+zQIixuaVeZ0WZBtnIyaDKA5auK4yKMBBDpGJIkMOS
E0flB3S9iY7iD5p/3mBiOjRTIMlyL1aURrbAzYg/2eiSotphOofJ7E7g4fBnLJhC7CBm63AZtYbN
qOk4nadw/ic9Q4z6+M8M7hF1C/tGK8lDCNoq1+pqsHge/ThRiw8z6lU1RfNcqkjpzqJ0OV8/WbVm
MD9M3vsF3JRwBnz+WSdyzUkMMQDd201QQwkRzUND47qd1PUOVMg3oO1pJ6Qq38wMe7mFlreo2q4x
yls2CvL5pKNwJKQNkJxb5oCdF8E1os/AtbiyENT/BXH3dyqgmkGxSJGhmS/ZaUs2bBGh9xhPH2+r
vD7booI44ONRFDflT1w1VzevI3IJJIFLg+rVfuRPMJBtJr6e1dZCxQzLJ4nCMP2SFXRSFeNH2n1P
RVaqW7/W4ixzW6xTCn7nBhocfHtsxQRscCJcYcIaeHMlhVOTNXv4/XcoHf7Dt+HEyoIZMI0Njv07
XR2YSu6XxDlCXM5NWx5sf9Qud5T8+sK2NbJVRiSd1FwlBkG8ynBVkJzEFNWH8hxoMDTUHIU1meBv
mXCzjkPYBf6Y5EMevI1xEH+FlWzMDfxszZfix45E65ZeoV+uUS3wE8f9DG7GRuIvIYSa9TRpriil
iG6PmuFcGH7wyhClcTKnn3QUPst7gX8Ft1UnnYAVSwdnC32XlwXRRpSce5a8c+77bFTQqycAQeIf
6+J8eFoQDKE82HKmfieOwYEH0RQwHiQHe/MBMRTXWr9DHcmCpvULyjVUmX/1+FrGZvoQvQBJAhdq
pguuM7xESq8ZWuXIKeRhxLIXfxm4sf7L+COvqE5cUAmxktCekcw7ACQcIJc2mJ9UxUrTI6TzgTVH
2RJTPkb+ZXbUjrbko+9nCT8keZS/eyWHaFJtfTduH4ul7NrtaPbgoZSGzwaO+cjRrSS0rAy/f+I+
3umKzDaLCIHi82rEhq4ZPgkpbdgRmYeqHalv5C32q+tOnpLI2IgxUfJC+wvb6G7GAJzyGt5LU5o3
TxsUBRnqr3e8aM1IivNK1uKzea4TNWDJByVTUWZI53ekceGiRSDoSJsB0aZVl8663OEnc+/emjE0
eZvvW/T/n7vdhFO+qSW26HEa0XpbcMnbAR0B9G0DsUYVYf1sS+f+mVuvaX0h/+/QbDFW0A9SZIUm
Z1Qk7BUSvQCEP5Q9qNkaP1nnCEvOuI5VCq+kz+SWHoKqZH8ehw1KBT2CBJ1upmRKvsMTIKLNxCNN
zzLduH4GGvcnCPBviePmEtFbCqoRk8tyBXTv2/j9ncmjYpuO1z6006wzwII8Azin2WsFuNoWepc0
GLBs1oadbbfPs4YFkWC1kZKO+PSm69EdMU+L7uCfO2DsLdyyH3TK0sUSC09jlDbK1OOwgL4n21nm
3LszjiKGwcS8yCyavOGpIgKEo9CJJe+BRS5WgyHjDPPu80T5XQShk6yYHj22mGYMh13hizA3Fp0V
u0jS08RXH4yD+kk2+qBhMiwFYkYCrSxDDfGuoOal6gCmnCS/Bgnf50km0MpPolB18TF631a2iOAx
+VVDRib1e8F0lp9QEbDMBfId+e62YM2PzCzzz0uep8QGA1Y/Zau1FEjxFNDIUvhGO/q3nOw1tt4i
NV0oyM/OvJ8/41NBwGtmNeGIA4tV9wlr+U8TmvHSTfUQY5ak/R5/NEF3kosgGS9uCH70VaU6MUTH
H1el+DDlSMSnFW85DH9iOM1QML8a2u+zCpZT6ppOu6wmVfslnVMy2W0y7x89VgyOo0fBJ9mS+zNI
OEp2QC2ZRsd+sz0vbwH36NXM3Ie/YG3V2XSHuWEaHkBrXR5LWs+1Kwxxym/ujRwtjUEAsg7K+yOq
z3GQ2eSVp5N6fZKC6TARUSOd9SyeLPsNE7Gr3rQ0zE5albNL35hhdzlYg8Ygiyf8jp2+fmZz7yPV
Q2OQIxQVTGMJDJ58KyqSJbC4+cTkh9ljeiYoOYWDXPlm4QZBb3GY/Rr8KHTwHIM00PlGwiuPM8vH
0XNEmiEryrC7kmDSDok4YRKHjLlBwckIgfTjulUcM4ja5hKqYhOpHzIiDFc7oUcFMpq0CXc4YeJO
4+GJ90CKSkhwJAzLOcFHNIalBiIx3GRZyV0Nyw2N/1WzTISizM1UArMOKxgR+7eB/LiWnhrITPlg
sJ5ItAb4+L0i7tnrFq5V8PasXXvEIOGt9RVg3Gmu5SidbKEhgPx/nwlOEUSSFKUhN3+sFEWyrlYC
rwfZc0J55ehzvcDunBSaPNj5ET1HsQvKZjzu/X1AS5uEOtnFNSurtDWHrWRCcQa8zjuzPOO4dZhe
6E+I87sfQZ4UnzAPJqRxCbLvsdb/gxNKklcfemkc/EeyoqaYUTvLKI+0CkEx9L1ZWXHMnE0KtRWo
+1HGWMa6K8kC+NqE3/5YYiU6AoJdKyxIYJm2IQePR93xsJaPaA5QXM32CdAIA8Z6ZOUar0+P8xFe
qpz871BJwekRXcNdtM74CNkbJM81onCik/fQtLbz/NKmXHRQiDOw06iqXZWqyxub8rCRB6ePn5Oi
pyEm38ojA+v0SwDki2XVIGaxHJP8NG9fdbWh1Mli9YPMbNUB3QUYqOs8U2XD9Q80RxWdLmVQU/Yi
JNavGAfSkGNSmnw8HOxpPEFMz8ASo3xl1CXcIAMw/I7h+Dc4ODYBQh//lMbeCcKQIO6rf3AZvl7G
1kpx73NeqvNXsq7u3BS+bqAfnH6OzGugHNimQiIWYS22O9Aht9IAgopSuU/1TcWBsljanRkE7LGR
Bj9tQZg38DDSwyKNsEmWbuo06YwXpyxNM/ZrUSh3vPqgI2GdCDXxtkUwjVXppv02PI2BoXn3OeYT
/VTpPrw7tB+zljVWUoeLYZYUZzLBCrnmH1tvn/YlxAGw1aMC1N/0fhaQ9jQ1+e6V87GloqIon41I
Ow/m0Vff++ZQT3leRlUq1uwQpL8aBQ2CBFPz3EgNVWOxPwmakWexGDqFZXEKUCCEOnVo+vjbK2y1
cO7uiRoihED/WoTHQD9AayWDIvX/BMYFYHChe4UAtjFDc7kwg6YXbJk3LO5egU34bSgYEwjtQKr7
MEjuF+XWCxPz3VmIhs/Ic+6jRN4yxu6nDcusvjFc/bAh/hXrHPyhO9THrpQDKsrxA9n+yzRpscPd
0zbmV80Zw4UGBUW8OxyATuvIKUu6Jf3L2ZVNZV96Un4/6izlLBv8z9PmZF34t93qy0kfSEgp7lot
JrzruIdG7yndRC0BQOBzMkE+TsXd4xLLA52yzuE4hFZxhburEe6oaQ3NZ5WAHVspiNnHMxgL7n5P
zYI++upZ9cdCiGbgRKAkPoQqvaj7ChHqlJsC9KZeCxSJjAdVxBaammcm5pMdohxo/mFSkLU5+JXw
IW2oQTgoNoB5q5XO+i9J0N3X0tzxdqqPQe5IO7XoD4kwkrNLev4Tj0Mew7ej7v5MzuB71u1RV7dw
B9HA5IdwDKQOCYqQVDF+XAKP0heq8u2r6FnaRXSuRo9rbyLgK3/A+8eGi2N0/cleJWJTSs3gKo9Y
e+Wm/lAbL7k2ETfIP6a9Y9QAw8RiA8B5fwwmd45zMDkLY7jOc0Q6ikxHAnO5dQfBgJ0yH91pdeUr
HaE6GGnlcppNiSMN07hN7YzQpP/hyX9fDPesTSLD7qKLtuuHTiBhmEuXJGzxVhot6stSwPINR3m4
Y3v0awQUNhLJE/DfK8561GO09UdeXDA4449b9P3t0GNjmDl8UWK4aL48AbU8NDoNHFBPNLqptoAM
E3TVveyaSPou9sZmIyxdn57LF3ZWA2mqP01clmWvDMt1OwXmqqx3qYm+/ltIEXWyYlGbgtvZjs/V
1DhQFpt12HCRg0lMsNt0HAUJg3QFNTfuHYcpMFBQk0hah1XbvUXw1lDlIlEEx1qAoUGi8djQYVaj
lvckPF3MEMnBZYdWBz4LAr1N+ptTtXS5vjcTvWigTwpUF/qqqUWuwxWyvv636lFJeviHRXLrdh7D
Ua2BLyqAqFHcgXDs409tPPkwCYIpwMzv7KPOuTh929kaQTiQbcchH0cyIDHRA64Ft3Wv4o+xLqxP
oXn3SyHi7EIF90wBr99iS+0jFrKiNutOXUFcMPxd1f6RhEwzHQDGH1108a1jlmd9b3/Lxv2nyBlE
563iUqtwWYQNOWlMahe+9tdieIs4b/yiXucSak5jiiag3uk8h/WZiM2cBto1m5Uba2ZBBc+oRzIK
t1TAR0UTDDKGehqaU/U9TapezoLHovt68tnnn5c1/AAjsMt5J33/0cP/PDyIv9EL3A8RKIoo84ce
FUSZZpye022mgOoV4Vp8mJ9ubikVZ2VpwwyOxZYHch25iCxWCWzuXfH3tgENufCMhQ767AbH2D+o
8cR/FcjpNo2lx3Z3rwreNIb0rK12/3Ab/ysj1dZtCe6RZqbOe4zDKm5ezdBZTojLBv/aSk83t9Y0
fUGwwutaH5V+OB5gyQmkB68gvbQtq5JynbeHxjivJegGe6Mip/VKwWbDmKtyOwk1ryDhDAeckoYw
7ct9kUTo1lsE2GK8j8xTF9j7KNffoxqUrN8CAIgcXiiSXERCuZPhZAPwwWVQ7EPUwVNXmBQnHM8j
6Z/pIxqA/wBUn1OABRlTlgnYAl553GkT6wZHFXmIwK2oxAE7RU6YAemKVMEhl3BLAvDTk8wAvtHz
Cw6s6yDL3V0jBybBEpelFdSoaLkG1pnqqUDAHVPT+QBn+2Xm0rFf5X5tzl4bPuk3SGULxaxni2y8
fsLByEte2cPoSiiV4mFwbKefRerppsZ1AR44ngOKUj+GreuOPIcog2VasUXx7aXFb6LFzm93fUGq
/T4/lk8L3fP+ggWYjlGARx1wSPY74ZJaCo5QI2NguBcLnxOTGF0W0JrgwtlWW/XrDott2NtXHu85
oVm0edqVOfWCQ6kPrUHodyimVXTECflPEVHBH3kq1YVCtYWMSTznErvhhCNCju8GX8NjtB89Doa/
R+cEAHM5f920rUyxFHOFXl/c0UT2yjV81pyaNP1aZmMQEU6w49UDg2DgJG7mp//EK7em6G1DXm83
coVg9TuspzUz6uhadSDjuF1OYtFq2/fgYV5/GL7aFMFserISeTfLwxzHJY8/F2oJiSpWnnKmOhwO
hjZLF+5TkGTvyqquwGxpL+hCDyfMJF4dFxfbcVsSAkFmHQLa2SRL2wNKWDIwkXJrLcQLcXNJOQ0s
aPaDyR2RZTZX5jX6pD19kvhsMACe+zeXeMog64Pbfk3cQezP2nyJ5rlkwfGC2Klx6HKC4Kc/xSS1
+ezVETf8N+6LxvMCu3AgETK4yMznhe3zQNCJvVTpzMmZ5eyCwADguLUwDeqAK6ZrLjvClZ4zOYtn
CQxNDnj9Aw8MHn8SsrcvvE+i9UhfrDUylU8Ig4zT6wqXS+5xbrI6Tp49zmDLqfFTJv53544YbBqo
uica3aHQMEAlTglxQNJW/h/1hCWAKMI1eP0vdoDdM9mGAt0lQKDMxK6ExFnATTB+XYYncGHs79dm
JO4gijSUXln9d1HbgxcFt+qlzOvmr6TFMjUisEWPKgnP5qFbv7tkkSExhrjwC7uIlVvBE7ETBKr/
zs8fDBMwOYhfUXaZ2s2Qp6UeByO03EUIbZSj4DkMAvFtG6hYlQETXiRvoj/+8Yus6Fqpjv4C5TVf
ixPoyaaVH6qVTtrN9J4yFl2CsfccSFZV2MNXorr8Aew4dGwT56uhTzKAVET5vIHq64YkNMsz1L/A
yxpKzUWjdqoLM4h9yMhHT9cy7XKuhNFinhMDOUQzsm5npJvfaKybO68Sd6xHFnc25GO1E0chX9ac
wkMtKumhBcV607LNmwt6hhYoNqRxhWNpNCbQYv71NpKbDYXFd1pcs07AmrZh8CWy6rOtAlGAWLJS
rxrYlJiQLNv8H/EcEbKxvSv2vhH3oJ2qMFInNLfB90FJV2gAarObweOmZ/jgHTW439uCQYD9/3mQ
rKcbXAkcNmIN9YXGIbNKzVa8Hq3znLYL8NHfoEJQKeDnfcniQqDbdKFNAZcz64YzXjpj9p33GFuq
VpNHRsuPgyEiuv+26qOuhy2vGFbLn4tb8B2GkVHBMPrQ/+raSCri44YOqU/ETA0p4mKdOqkmYt/e
r/vguvTB72+TsLxc9wa2go5ebI2f759yEVcKn4r34xgULhKpI8qAVn44Iy2ThkB0ML0f0ebPcQOD
hNrXhf8f/t7BgkoXE4xaX8tA5sNArAEYE0Rwv3ABx2D0OqoL4xtoolUhLRuub0ev7svlKchSpdJb
awul2mR+bQ2CVxaVfhIdcY09R3JY13hzpJIV2BcJidWG4rJefnGfY3PBCJp70b3Mt+t9iVSO4J/e
yrLBg/1o8y7vja5OPL3IfbLO14giWPr5NSGNFfy9e+V/K9/OnbdZ8YzYUDn9I9UmyKV3z/tSR1EL
RxfD2E/krWMW1z2w0o1rwr86Yu74ZKay/4GkcLHSNbq+x9haGlv0oONGS9ebT1L/8/+cA3SfTsea
rRBOvTeZVXmhh4jZmqnpXQEl+NL+cwmY7SwCPHJPTmrz4bfmUblDWqFjhGtOtMRRk+8DeoZD98Qe
SAnuEibcLe6Q9/dyB9H1o/8/020cp1U4fPRKPieTcLRQQj+tTf500pVPwz/Ma+HYtSGp4ImuJYbi
l+0unPbMd1r0YEx3XaBlcRTvMM2gBOsOWCZuxpAg4HrApWuFxljo2YKkWdbZDrhYFbQNsTXoF+ko
ngqsfT/DzTJaiFVMJ4f61vPxPf0ecfTnYpXX8BZD6G3FzHdcUnwRhM8IeC2FaeldywkOdLohRSjM
P95SfxkD5f5YQPm2yIrKouqDo3wB9ItwZITFbTa8CnEv0V4Slrh9sTZBr9wZWtUkFOel8TvrATny
OYo3USJhaJzhTLBFJpH5eehtn6dp6FNPpeFyPAP3jb7G6S1DT8yubUsFnufVLCq3Ayi+jw4i5duq
pWakM+JlLIs1JEjEzOtV7jsoJwuxb2z6SIKq2nvf4R75Cz9wntcd8mk1fs/z2egY8UBRZpjSITcr
8u6NHitlWBWGbmQEMNDzvHzdRd3qMXwqduQ7z/nUuz7Uj3bmWT6yW5xwXRzBjLOPoo9NGt86D30j
FPdHir66XgU7lEmjkGTjLqm6bteolVrHWF6mqSbeUsrIlUcbAEwxN3oUCg3+6aYUDzQe/hu3pUHE
XMabDIYtv7RvtKaLzbThw1ZSWhCJx0o99dnywD6NtbvTm2WUmSn2GWNQ2KKy0dWG3C/aaP2iNppH
PznEahKtEKAucneIERtilFSpFiKxKAMTWNnTYh4lOYgGqdowuM8/aDavnryMPso1+zKewxIn3f79
Pdx4Oek/YpavkBLoYkSTyzy0IgXCM3OFVOkoWOSqDYDlpaXXc22soNuLDXHRKDjfuwYHfYQRHC4j
ksMhq41+BJWZchrvc7Vxg7JBJvpBp5h88VO7hpP33fKuyTfrVl7Jx7RRoLJ8hRbvsBC09t3dlA4w
7wx63a6nSy1wW4AIz/z/YzLb76t8wFh7wgVtNEUqF0jVFT2y0zbFF/2RvColS6W5sX1GvrJK77qT
mV/gpO3UCn6x4WpjcZBpAICns22/O2sab6pSGKwYxg23Y9j2wiKbHHr30oufwanAB6qYqvlZZ6WI
re6EDHQsEFr+gTbJkiPQxb2gZflFdaAEnqO/By00lnRqLej2lFh1clP33QeT4M2kK1QQdlM0xKon
SFYVlI3GvM9M3nSK8JNi1oV0o8rOn0loTPeKxMERvIYDFsx7ZmmK/SNYMc7gZ0sQjrasjxjHUbKO
TO79xYQEbpqRcJi3fwJETePNNxjbhyV9cbP4mMCQfAf+BA39mDH0n6OUPDHG2oSCP2/mLo8/Nj9b
7bvBtuYwLErNG1otiAc0zS2cYOr5MoI5fO4Oz1+RHPH2xmI3kH0IXdzk30Jjw4ZvbIv01X1ynO8i
kD7OhWa5rmnmdecJakx4l23PHxZsiuSJ6HJ8T5VK8Fn3pM/vwfTP+k+rzIMC1fPJGzphi/TqoiHy
fwwW+TPa57Qg1pRTNu77qi8n3WxJG7TKKDhCU01iNb/WUzmf1gWTy5towBCUmhg8/aCuslxN1Mkx
EfFJTjtirSnkywr8IuhU6aXSpmSkldf3DSl9uI4BAVJZt4VRXe6rW6RAgnhLGYSPOf0Br8a05aOH
v0iZgL3ZRgSmcCvO7/zVi6IQZuTz5pkiut8BHtDqPrr5btoDvFqodECb6hHSsYzo4bXhiXFxw/Bz
a8/ptwjxJPNWlY8Ud6LvS26/2KtCA/1Wa8WrW2SiKxPQua1aZukPFVG+1Hh9uHUbHqR3hUPAgSHC
UjhXXFo+t9xz4LYfUUqPb3S3O63Yky6p4lp2RH/PcWPX99Tg4QmqhPeKYSoXp6UVhojme0NLmUkH
SYt/la/JNfDz5xBKioZodK3Qj6vhjMoztZjMXyagJp/p2szHNr/BlUu4mQu+CKDBWNn8Dg7UF6xc
HaWGzAH3aOAGb4XroFkue+Y2xl9wm1iulzDMMArYTk+96MEgzAucvBZ/JTljgUNRzOccqVmF+UlX
zgUnWimjgRlgBa+Ir6RJMVvd3w2K6kmzPC7ojQWSVAHUPWcwfUAADbRtd7ry2hWv+QbXFcCuV0Rr
EVKzXDoFgJdRkBLGNEpquCXq+ECNsr191w+Mf+HlH670uOk3Fs6SJhwm68nE9+iJGGg8PifaKf/H
Pwd//hBSzgQQk6jHGswCGMci1uoZZaP16M1ZL08cu0PNytv5y/F+x1GmlDHOqRjk2XXZA7KRhFo0
m24It3UWTgOr9WC/MVJbzbG9ihABHZkZAWd6aZDKdNUySqZV0TsGO3SzGCANOcNXMzRW0T4myqCU
akYoX4bs1go73QY47zWEspNZ321Fdl/XjVx1RvCVMc+jd2rqELuiVikFsg2hjtyDItqEF04DSNxa
g0tTOZtRM5L1j5Io2uoFLcY+axtQ0u7o3/sxZ1YWTYIjeDZPsH4UuSBjqZo1jhuR71T1/0oUqRFG
qlwknStK/5hAghFTluHE1VK+h/kIh2oee+HwzzgvitvKGgPGtIc0/cVvWj8gjdryKKoQ1zrgt4VO
3qnU/CcnVrW0sZJXUUMKC87Z8WVmx8wfMGw2rYY0S5AumZHSobidJ5JRagrWwJycuvkfVoHc0l/x
woOCvYWGz+qslB6I44ZUn9Stih2SiVD/wN7y9oF5LrysV0e1ZxJpWt6b241tzZNjcMFaiSUHswMk
IVuXBplR0pQGl4ZGpSe8p8PRq/jeFPTn5jFRQg3js1vZSPhicwu745FfJWks4CADW5ntIPNQxDhR
WXR2+MuUTBOJJUHZSCWEtzmGA/lw5AEytcSRSHEZhJZvFJ1toI1wHZHprUqY5ThYOzZpBuCEmsfV
8Llk97N/jAh6j6GCn43bOdUzWu6nYf1GvrrBgSd8ViAhYe1nRpeGPtl2xusNLwX3rlT/ICtjXhaX
W6laWvPfyupvUFEzU+EfOw3C+quz0qGstWqy8TUMioPMrfstk+xOwSZ92n+EvZqKY/xf5gtzcJnR
iGFX6557smNz9+VCUxhQRVeMaRC33b8CVRGo/u+GSesSEnrMnE6W86QpTuwnep7SqUoCRmL4bP+b
0Wza3I/NhR7nsmLzty1uVK1pBMMr1k5ydPMKBlAvyBxV2fUpXNk7zcQpXhuxuaOAuFWLc00xdTSQ
8WY3BtTY9IXTGfa0no3viPSps4udVh3Pw3YowMHW4KqMatlQQfk8NpitJE2cmIuNJpan/4pZ2HvZ
WhWwUJEIlnaZ1TbBu7Ch8scItL88VffWHHvgeJ8pjD2nV/26vhNX9Mq86L1VC6Zcezq3UDh3TS8F
i/ssU+s7JbIB+7IqJN+s1AKzktTpeTeA2D5ksFxSIQZAzdrfYL9+/9rE0I+g1z0CCY/eqcnMN3GG
4WCR5N4SW/tdos2PmgKbZNGFLqFw/mjSCTm0OLWDd7ST75b7/MCjPu5U8V8Ma56WZeP/SoPHGmte
A6BGPoD8Cs9nFuexceGG76kDVf8g1jnl+4vwHcSxlM/aCvtqWq3K8Xm8RI95UwbC/sGOxBu/ewKT
QUOahtKOKof5bbA04h6pafuyhjQgAhkVl+Uc6ve6U+zZ+qfP7CCYTpG+SSPG3oVArORwbLnDCpV2
gm9QvbTjHmj46VlHIPYEKAEHuRkJxfx80aStMNO00eTaTSumEvQ0fyVLQqm/IhzzKKLEc+kJQMB8
sAKp7xJlf3P1nF1xxcMKsP4oeJe9xjT+o/KImm+bzN6/6oudEgt6aC8ZUGvZM28RoeOfpfUBi2Kg
fBi0hHmCUd1ljb8H2ZqtBB+YzQQd0px6bzy0o3r8i26JqRGafFAHO04+GgZeI3882vhdNjsuNdPN
/ZLqChJorjf/DTeDLefatHyKbd6E+supvMOwkYp0jVIXJA9mSWlpgaTB8zY3QYWvtd0YvsQ3eFlk
SQWzdnNP06mvm3yi1yONpaP5vXxsnW6zomPYUiGCWtDk7ZooQANCsp0Crf/W4X4K5j08jjEuCD4I
HHFZppdO90JGCG41Kx0etGtXGmOMIiAdxKSGnnAVJ70XOKT+Qths+Ne9vjCXO2xBghnBDG6keE2Z
rF6KZrJyQ7RJCoTki801+/ZlW4P+jcBoNyrRNfDnCM+S0WzLsEiDb21rdY5CNq/bYEBnLazF2715
tz4Ovdf1OotFT64GURtUh2GcxXgh30znUgacOeuPOpP+zePBNv62Ydk3TawFwg+YXTCfFeNyByK6
D3Ji2p7yDWd5uf2bFtlwJab+zC82bqLCxqAwMxWhKDshOu1tcWBEuaKOEuKdt2qiZvPHhqBw4zFy
N//ZZghw4AH2k8tmhsSgNpQZH/PfZ92tx7WmKeiTVw+IAbNlwWUB89CEPhPdhs8ICyYQzkWwiM9Q
VLCqD7U3GrBDg4N48UV5QNLHmJKJO0eZ9NHIP5YyjpIKJMeTjE2IMRka6Nl+9LimgqTZUzNG4QmI
64RYQ60DC4rWyRdCCVVY8le5SMUcg1BOUJKIGbQ7OIfjHgkgA+9K0YYVAJGIujg0Bjm+qXyHtBKH
eZeTih5EZL1moLIghuSqfZ/OgzZC4NFr/uTmcUhyNQC1SfeJVXsllWbvBHr/L2fLn2u0V8id7Q6k
cikYXeR4YlI9H/IwLJ3QPpLWYK0S+pCFyPnSmrjqZlmfQZ8wXyRvqhcyLU/Av7ZkFk8aO0zNGGL7
YhFb7JqFamJrcN3fqzjz68xhXChNl7fwvztdwjQZ0bDZuuwbT/gWVJTSTCB0XjRkWhM0tz54OjOt
pU0tYeZxH/BuGcxbVyKboDFpp4XE5Tz23z67vTwckWsMoXsJmWAMrXswfypLvArTHrvyPmzIXVkU
m4GOw2U18kba6A6PnLUQzljZsou7MLrb/PxM2ElE2a7cLJnPDWCPzVwMYsOWQ0rfjCIfL3LtjnMw
nJTjfe0IWJIm5MubAnEbiasArSZmny2eXSm5aKLUdKZ5XprijEz2aLId3jWI2k9eeZxMXU8kaLyr
U0RWMxI3QrhRkp4LV75wwXd4Vh6/Jtk3SvKVK0R9YPbo5IMQLsKSWGk+XWrzn/pP0jlSSr1TBM/2
c0y8F4dFT2D3ki4XkSczI8Y76psTsb0XwWDE+5RmnI9mggK+HPrF6alhEuaZpQnHdzKpldX6JmW+
i+rrzgsOvsZitYNLMVHhkrtencGIyeGd8zIk0vDXpM4WZIQajVkSXXkX1kvccxq/pwUiPLXXe9ZI
NHLKcFJWR0eRZIVlMU8+9j+3lWrzAdiIrO6iAi3y07K+AzEX0GH51pDGu7V3vJZBTJlDVHoASUkM
ncBvy19HDqkfmHZyb3jv8NtboAC04AVRJ42cHVNuz4WWDqhWPpwf9Cs59kHhD+crNsXdvVpk2T8Z
qZk9gKl5p2OK1iUMDp7yHnXwovLr4pxo0cmfj5LgvBCsttnDKzlijJDHuKA5WHTEvXnOaTzZ9cjo
z7xA/UkXcOk9IJWPd/NeUBa7QYVRm+7He3LOn6P8Fo6/XowrsGRGRPXqjuA18D444y56blp7PQN2
l3cwx4buGlGBCjXYJ1lNszOX+xxkvGP49PkIaYaeTn29q6D61aKMMNrkJh+hIAnQ/jmu/vlNH2tP
nDsCwAbZ6G84xjAoxO/R4/ZEeGiivfz5xaaTAm0izGzQ1ve7P/a0Q+9UIDvcwZFxxj/v1qnbH1PI
nqjBNqcCd7wYS7bY122J+/+54AY6n2ijk8S3RoVdy9coTJsBaq+sMEUdfLWcpkxbjB2G6xma2Ndg
GB4wT51C0LOnHrYa5f2SiBEWzBvpmPXdwC7cbkslAA0DNbgex8+nlB/oJvL4uRsBu6hCIixhdJDr
LmZGGWEu3hVRfwruxRnlPeSVTuz0tj7GscIAkY5uCCjXBmNzUa61t5eTZzUspIzQste065EDVfFz
PcDJVhYLjuPIv4SUKk5/AXxpz2awA57XbwjfMVE6XsTfvjZ0F96NqJ4++Hh+nfQ5TFWREVjt/Hw0
Mrqmri4/tx5ba63pCftDmYZKjJEpGBHjxm7gXsmuDJofnf33EBdz9wGDC/kgx8xNt3q2WAe/EiO2
G+y6otlCF+LZPKcQ8COGRbWyL8wW6ka1xXKkUGjTkEBTRFvquGfO6ev1krj/Hwo2+dn79qE5R0dl
ZginK0BuOChmB1+cU2TS8fP3ZVQQvCTGGKbPM7qLa7oZf0t4XOk5D1YqcZjrs7G1QMa0i/ObOxgr
0Icwv/Q7BS37oG5dwQtceiOGq2lzsC6/NsRv2Sf35OKWhyQnXmnYapUYPhyJd9W7+eCHpE2hDTLS
9qw5Xk4EDPKjqHB8Zpp96ST/x1urDLnUxJWA9sLqneDeOS5cepJSJoZbQIdkXbetX8wFqvPDUqGR
QRFjVdriyP8D5WzZEavksUWXMWwCyFxZktaihGZM3iytAuGpynTqIuLhR1QFA4Pl8CxO5WywrFw+
nphCxm2/pZxOHgmZUkvWLwi5mBe5LFZz/nFE+oCQ1qhBGW68Fc7mkr8SD1GFGTMY+mXliV31Zax5
Ply8qxCWxu7QC+lbKuQja265EBfaMeZa3AAW6OkWtTe1fJRq8DSSFo22qs0brQGkImQNWcTDIcVy
K6/VCXD/12DpOAhJF5otgdaSJFVYBd9jDdBXHBHCPCIOiPizb1Nvb+Dz1FMv3N4NW9ka+S12mUci
L5O17/gGm5hd0xjjqwN7lFrhsH6ZeSQaqaYxAm/BezF6UGGrN1tmtoKUWqlOohbc8UXn3ilMwjOu
iNByR+eeG3JJnSXiN/A4n5wAhx1DeRyZyodOVxqKrblKffeS7yJdtLFCfDhFzxx5qGG9WFuxbMRX
JvOhuA0vEaprAnjhWPxHhRBtj1IWQdWQIrAQ3X4HplbfiS9H8jxu17dy5LRPSuksH/BiNqEqsx/X
6+iSCECg/eHtecgZA/6dcRu0hlF8QIVzMOfl0nSBLsVPGsDLX7JHcAGwLUWeCDNvsuRYN1T9hyg7
D5a5/z3wSyFfrOOdtEzQqGZpGw6biTi4/o8nVucXdCduKS98QxLHNsJ2W5gxi+C1eR24KRs48F34
cfRhlYsvYQ3u/4ZuhPwi9PUnrhNOZKJlcOpuW8EyMvLsqZQwEnJ1Ni0edgQCWYhN3zT3+sI/D3/K
Wk2eEBamdgFwCHxTubkPZywKksDZP717GJ+JEFcOto1XJ0yhUDM/ZtAVkm+oaq3zqfD6HDQzlKiz
nblh5r62dxSJYH7PU7tDhYVI0D8Qts+HN6GxBFB0F/liiHkqHwl6DDpBo/d0we4AW8UIaEHdpHF2
p2yu5sDrAoHdYahInf0ISvamoM7ZzVOG47X+GQ2QHdUuYg+2ZoBrMnqc6CuVCjS/1ZC6Dhp5fGOq
4Ftgr+3vNzwCmnbKvlqrSZPQPwlm+KkR40oBHt7QE+9V8is7+2LRm3ZrHmm1omKya3m+qacsPEBO
tF0qhS37lqyKWibD2FT2k2A+1Ps/fm330g2HDCpZtPaRUEBQ/rEFCMXqLXuDrX8tLwrzgVHkCQ0y
uI83yp438l83NuAzml+be9BwIoi2zuj/9s+oUZjwIWdIcMqrZtedbhQ0ZOvB5YuuaeC/oYQd5gpv
e28fg49aHTa++L04dy4MJNJDQv30FEwIxX22gYdauIOTlOSQcBsBndCldypmvUDXpiAFH8EMnIJr
QmgmEuJc6IErQyMrValMeMTbMfm/ZyfTprJOaq6Y/AJPRcaOlqHhpiwtDo1uQMyBvxt8ilXEfQ8y
j6HRhzTqbJ4MIOly6Nh7PfeCpS6OsOf5qVuS90bjLVqC6DJYK9sxtZ5L33TMfOjTe8ccrLGJylQY
6IMHXvMEC33rb3XRKxj9j9hIQp67QmOXayHxxO1Q1fECi11hcVsigWq1LSEVm/+ZimhosnsuB3Ek
2FGQwaDOKpWBH7EFp+8Q2YELR/BmrW4ZBGk57kWDlaSyHgBRPSA8yX0zG9TEf/dBwHKwsVeo0/BE
0ONlzTV1L1b0zeMTicDpS3hPleUiiplXGyZo/2BvufapRecT0zxw+MEGAqqn4bIsepuVKo8j1m3j
JSByCIbbDBBHX0ZLMDUx60n1wlGQqBEPiWZ6t0zg6KUD8TTeOxudoZ1Z5hNImShpjSiHRoxjQDXt
ruxBEgDsn2pTr1wiG4opxhEQWNi9oxd4S+tyvkccwWequT/7FtFZGMUAkFqQljhTZVHjWVCi3nUI
0XGZoH3ewzLj4DNWqBMWbhcIekM9+VdqCIHYyO6wLmE3OgvkAOvSHJ9GYWEZHLSF73Shl6qbU9Il
OrXK8QE5UmhudL9kkd0OnjqNHGPtzBndTreewBZgDuI0FIFh4aHG9BmM/ORXl5e6wRuqf3c6PSET
EHdqV6/LWoScfIp8e5z3MCmrv/IlIGwNZMcGyZZZ7PcZq73lx3s/X10gvgZZRljeGX/Il/tdz1dz
R3ynBa0e/5RREAlgBCTxUiy8GwoptjPttJth9mXlHO8At1mUx5MX6oEa16QBLhne0z8QAqvHABTT
RFedWmwnRYCqjpxuOfDURrn63m8ULb9CJ8SRPNyFM2j9iHcKuF+519I4YmbCX1YZTOyz/F8Oby2G
ioj+HFZnb7pRxiDmftHXijztlQQMXki/Hcw3RV0nH1IhLEwWWRX78CCVNcDNdsjlsOhCs9b3Y9bz
6eyi8MeiBa7RN5F6GIM4fCzMsvggNJTFlRXuQ3h3k1YfDyy7GvPY+LXxJAxrNZRC1rZOl7SLsC3I
qQ8AqA3ZxcY9isGvF9A8/u205Zfjyzns7CatgRK+lXP3Fc8hPO2I06NVa9kYkjvI6l4Dch/vi/aF
p+rGKwA+s8nSb7ZqRrQ/ukcAnUG6R/xYF5w77CiAgukUr+B+XvvvU8gOTr/XI6lIadhPj3X/FICG
n5+aNBr6D6WCuMW5PeZRP8DT8eAEzy5ow0lBz5HJhWq7l2qxlS2vPPSk+iBmjFVtNxRq0zh8WWme
nbB6m9/MYkoscvPEAP6/uegrHIvgrIlsPF5s6mnSEfT6U11Es4GW+aSEx2ut03pUZrJDxJkCxTFZ
GrXSevhbpmVAhfACw5mhny1DI+SUP189SAs22OVUDgzbpjErl8+bmKlVUWj+1O08+Z0cMSovUD/c
caO4YofHhh7fHQU+f2asb1k7Xx/JefeIwYGbfoKeKB3V3XYqtZn2jY0x2xzyMtfq/Qii0+jfICMf
ul63D59B4vWqQc26UUgy9h4Pj+2qzybGlVvRalW7AmYXT3oTbAZedzUPJOaKGdB2cv5Enzlv6+Fi
04ItqVCu3nJj9As3NMz0OtEwXHb0iMd6yaPi5/fAFBV0fplFR5kt1AQjeu1ahJ4XTOBU8EsOV4Rt
XzZdu2DpnH3oHFYYox1zLZMsUIn21UE1aqMyeh7qqV0zd2Q9oY6ZVMEsV5SKCLGRUKGhQTrHp3OA
ihboteKanQ9e990c/SF3l2krOgHkE7JyeQhpLCjviBmhMfQVArKgVZW/QEHWVxrE0ShVA/+Y6OUG
vPH0eKAm8OHhoo6ImgqUR9ppL7CEQzCZ/eDkU9XnvHsodWgrWWGRZp5S+b2Z5AFXxd/iIXySNbgm
840QUZPEVahqjEy/zAdgk5j1YKBDoOApQ64UQEhE08OvjlFUV6ri7bnDc3f7cMo1i3GAtVr+BvRl
qocTkmKY+IsUMZuhOPV+xv8BI9WNk+vozS8cdAJZF0DzIwrN35D7ijhB0jaxij3+blA2gQMrhfHh
FdxJLY2oLlulznduPn0MKgK1/mMPJAdoU0B///IJKhcmxWLoD6V0ONOo0D9ERmxB1hi78bMmmMI9
EBYN+Bxw6JTApFC7F6cP36euqGsjbtTkhtrRhSQwzIqVTYHn3gELFuln1cPZTkBrpcXO1qQmiwDu
PELUSMA9UIpqbDKWJvaiguAfVIxmWOanSd34ORx8h+g5jWuFnQJe1Tt3tBYYDndZz3tjxKynRTBz
xrWSNCP67OGq4//bRFI6DQuJxXP9EVk187hn4uE6TjSzslAT1ApAYjNYi4V7SIq5LKXVlvwY8hLa
bFEQJ+0LHd1ZUV+spC2lP5aHx4TSaj/4P00pX9R/EwWjPaSWT1BQ9aAifYmwNXH63XvypQit9+VA
UOXLzI/9xeOcwE3IQh3bm3/CIjbW0e/rDm2iV20WZvHAOvHckqBQZinfGiVOdMBqc3SeoEJytwfi
kLTLQkWgTlJGl7jFno++tcY6flHYFTWaO8rJ4RGnHJUoADVSJkvdd9DC0SI6IP/XbuxTRQ/pfcTS
aao3uMsesxS0FUZI3Y1rsIbl40K55E3itFw2BIjFLOkGgFhDIwg+GTkbm89pkchhotei6om+7V5u
AMq1Axvll+TFeTVPHwi/YPm2WC2g6iZhAajok871LzKV5EgaIupan/gNi4U1TaCTFYcusVWsLIwL
y9JwiHB63CRKXfLE1Td4Q4SIZxyJMSXn3+Zj56VJWM8kTlZyElzAx7tnFwT1Qan5JMuazWuQuRcw
1jOevI1sbximgNIq8Czf5wdZ/Tt0RKU3kubi8HVQqYvg87jeeGYJ2IH9bQSRVk4MZdk2fAnPWRyx
tLvcDkt07CHBqVzqSVq9EtENLxxN4yjl1zudoDkTGZM+rDth/+dd24AMlQgvGmAofIV/4chg1N5i
mkrq0Z6kXyZcsQiE/teoCb2eF4JLHRa97QpcSbWYL62FOyiRQQN7G+e680MTRtdvPGdx5NNtnZqS
kYaOJxUx2GRsVNEls/fSZj1ZRJkczJoOpaKe9PfDJbprcvibhjs7+eipUmjDOXRkJxsJ0IaHM0fK
mliehceZBBxQrne7jgqixtKfembYQqTXWAkuVeTOW0/m6zvGDWqOQXdRUMcEMcShDzkW+kDVSBuF
PPJ90WI3kYUkiPlGaaTmR7ZZqoxqdslX8/mq6uzXtiFPhHdkSb1qnCsU2J60Tmy2j/kuDj/LGB//
UTlmp04HE3Y0VcRQh+HXbRhSPj8GRjPccGf9Gml7YqpRaCO3zjFzL8qGpOJuqxZmQPzcbqgcQLMR
xIv5vCv1Hhx1/a3q+4ts+6R6SV3dzW1+hznFbDTL4iCM6xno2M7XoM0wNCVhgravtMYNBuOvod17
jXG7SI7njLZWArkqZ7iH9ZcLTVv7vstkantiAWpmPnmQoVItai5o9zkwZsnG1ccNIAGb0TsXGagM
gnJKDgbXsjKianayHxMhI6Z5pXQljjRTjiK9DfrT5UKOyMbO1sj3TeEBgKRE4giuKtUGY/sRRE7i
32L535Z12pp/j9DjYG6Opv45Khwmaw2/NcWa5C9E9AaYwdsTDMA33K/N5SFKCNgT4OSYHeRNdxel
iZE8lagBVkEuvzY/tun7ji14n1Hega5VlzC00txlRrTuuNTDkss/+wu1H39hUCCs0z81WstBAnCQ
42wI6TPTI7XDospfNX8/ZTHZ3apg2MeRD9Bo5UvQ4hKuqT3o3KgcDlbEOt/N6A/98yBHO+3Jy6PF
R9AloWyWdWwUjdq4m3K1a9agJTblwqYfE8V+6frnUXuLEf9rScqiPdEE68PT99UP1IIbeKUr8XyK
PRM++5EL2z6ODbRrFKN9vHI300a6Lp/DcbFixenejw78CRFtyrKWkU/Ul0S2ST5Y2DVYVQoUKbyJ
P02to/3lrtOWxD3xn/OfEqGDKzx+vN7Yzze4T3QPI98du7rxt+ZrEmYnpPnI1Hr3+J+U7idSpwWC
JiTN3x+rBptQpZ4ZvySHfSheDJo8FmWwr8CB/kKhXh6ONiSeYKgiirF0lIEDu0UpDGpEQvg6EBj7
xz1PC6rokxIqhlSvHNFPzYjhHqWtM/1mkwyQghPVDqSfclDZrj8wvkSlgwVljm2DImgbbrWWIjlK
dq1Qm4h7IukS0GJmZikMIREOpRdPh4F6w7FgoZV8UGh7C+yuDdOjnpHt05SpqbioPgmclnLrrz7l
wn8FdCroJ8r+Yav9Z6E1XQ5xtRTiY5c9TUPtiMJgIcX9TNppSs+MZjbDNg9eC+f+4ek9Pph5I+5n
t/8gCubvfs9P0ROxP8Xh/5czstrvSMcfpTktQtrWeRI4VvwJzannCjceC/zj67yTgHQ7OXE1V47I
uRM9zm5Dw4CRCYQvdReoUi65O9pgxB8aZTJ9sza6h32tca0yNGetm13rkeRzEISyWB/YOHjh4vEA
K4pA0Uwq4O+WsjghH88K3B15qC7Wo9KfoZrpW0yaomou9yvsXBHcP5aLcZmiCc9g6NhDe7jckrea
ICt161fx1w66JU6UnW9trSAJqOOrhDFWpM/JaCuAcWtKkr5MpoByGYsR8inIMCmr0bTt/Y1Rsz0c
ewAsPAXTGQTmLZLqMBSiAhpJQ/A6WDfvxNJmvjGs3hPFKqoQX983IK+/IwcHkbHfDBezxiVOtKTY
6KmwHSggom1n+nQP2yNCjSy1vwnPNG9QHQOCMNQ9nt7Kcb0yEvNc7FJY9JrUDVVQgrU/roexnASn
J9h5zLC+SI0UwiUhOQh6sLZAQwkt4hNDuik78UrIaw8rZNGj8PIDvBRUxDfdSVMXmmSEVxmM2kuT
kCXJtxa2E906kgIkBDtT9zPF2sf9S9lmQTeyG+Viu3uiGIsL1DmV200dnnmp9o88hKcPNET0svrZ
0rvU7FkbjQxXKbxbVOMDQi23Nzpoh7h32Akzs+pmA70kx9WGq++9qEtvJxykti+tpixBon82Gm/R
DHibOE78cP5n60cGQJ1PRGTvtk1J6THVVkzvGcMIiotwT271tiZztW3vJNJH6AOjkCervQ6+XujX
Tv+G0O/Lo9ibOeGTjJyt6AgJ00N1Jsu7h69NVDOj4gRo67jAa5VqH5+j2uHiTA4UXsn7d0U7Vm5r
KkVL0e4AnQ3HS9wS0B2aGGO05b+5y+jHB4MP2HO9MY28Tv9hLJGs0YGvceh4lBoQLY1Z7h7JaxUN
hfdOtThxiBCIxgvbg/pnDGnJKoTyOBdqzRL3Ucjq8X8q3VEGYznLFtUIH817+PEDFv8X5JuiiiHl
12b726Dr7692JL4RadJ55eTd1HyhqAmnI2r1/ffpkdTuWu/MgRRzSnfbvmRx5ZXbXiR4jcjeq2Ee
y2e8Wnuf1fa4mzB/JdGKGsyH5DEQeFsfaUQ1E/M7bQ3mxTyJTk6qHQ+4JbjllTN+Lpu8FE6O1PmC
77BgwxCJGddl+OHxfHrucumkgNTxo9xHvQ22GB1qfvQldbD+aGZlYcrgQtID0wV1nAmp7Z6o9N9H
yUeP2MHVDKbsHdCxw1JEq8AHa9PKv/7UytTPzdqTsaDwEkAbU54b683aKWDKm/7AQTjc39g5ACLQ
8Xt7WlkE/CXsFP3JJSj6M0EYr3uBFFlkJftzMVRcZ7XXHz0cKPHPFMwgDqkFowo7fG6AX17GeTD0
yWGCLmYuQ0bo+QPhRPP4GGHPFR8TCyRX/KlN9MqeRd7KxUMcIrb4uurjkQAHoUBcKdWNKJFmtZuu
1O+z0a9t9Srzo0wSNlhg/gos8ERMqicaJoNWuQrrol68oM6+TA04pAvCv3G1LyLuTY/4Xom5F5n/
hyVcMYJgAHiScwCJlaLzHJ4yLagZPwCskJzmipLx9SYmbDjHfTI00xX18G1WXQoGCFY6wmi93lgc
ygDTWwlLZ4gIMsC1hEUTNHeZ5dKcSxfdXwtDLHGiWCaxN3OC2eUeaIzJcRuAX3rKVS2EV94L/Gnv
mixMh7P3SbqBvpwpJy2/SlU3f/Iq8NerF0KJBsjE52rOxagQcSZ5moySLxvc03pLCNtXrOcucL9D
oPfjR/tdUFvFdtLI+qmkbHWHcvQyrC6oYk/dzgvtzTAVDBqtmmP5icaO8nA5hXSsp8NFm2pr0L8t
htyxUMm/h3YoX9c5qCZ0HKJM/sGbtTwqyf5ovMVk5K/PH8WIgv4xcYhbXKJ3oL3+YPLzQBPOumlq
ZIj+RCE7Lh85JKxGfif9wG+z6yKYpZ+b/Kqm3YDz5uSuNicKiXydxrTuTwDsuzMUNNGgbVpJYTwN
D+/A3n1C+7Er2SAyNuyUruM/25vlsTZNL+xO6Mzonm8wo6k/T4R1oLCVMnKIVZRz7qSddAANEW0f
Pn/ZiOP+3GJ3/dDTSdKdhgTNgOR3NkmW0yFgJDP2sgCBrPnbbEwscuECq70N9lDboUS337XE6r+Y
BL460TSwxvh3GQ1eH2Y7+ByL93w7URsTzESAZ1eSBGKub59L5xeXLFjKIXiHDIrHG1LJn/c0M5sZ
piwvI64Zikv1cO9ySG5GHoFO3/l/c1GzZEMY7sTK86DRczyqcXSJtfbMOccLwHufr2ZLJrJY0gmZ
nd80168EjF5LuheFQ4pnXOpWC3ODoM3KJMwV8EhCHZ7J6bLn79eo+8s8OfcFIEmSanLizVxYK2dF
hkLOCIkTyC88t3RmE1Z2VHfN5S0PIuYxDBNI9b/NuJNe8d0Zpqa6soDusBpnob5oxQhu+8psS5pi
xZPKfnNUOmVkryuh8ajXO+sX0hdEmRb7+nZ8serFFB/PYvv8bbTHcq3aXmdz9i1nZp0V7JqhstyM
NdI6iSkhUnTiMT+xLWo/kbz1X3gh8qUhWy9XX4Y8dA45dwaUoiDwVRbxdZPXC0g+joo7OXPUKyDN
HCEp200zk4bED9GmfreVB4v4p1SoCVUVEriC6BZA767WVViRE9kGNycLf7D2TaxLn8F6/z1/2YPZ
iO7YGYatzS6X1sdq13CEhurPxTNk1F3TEMxAAuF3t7Ngqqv260K8vqNPcdz5gPSmzk/pMLoYjU04
29FeUBNuuYVUUE1SFOgOnuC2ISghlCB0RQywSUJ0htk0CnwfzGLhELFKzLlun8GIbda2DARYa5Dm
vRxVGvNDHMS/cycNP0sfv90L0ls+FKPJUg0IbNHAf2GIbHW7Q9RTWZtUjBjS6/a2xFn6+BhRKv5T
qwjOJmLZXoqqF7NLOkxLZyHnjjN0kWWg/SiIvEOO7rXBDtGkWxxiwqFyQpwhkTWElfI+JV0rBacy
WPXLxxfRmSmqQ14LwPpsiZSse+k+8q7Pd8NkiClK5M4+1HKDZNDDOY7heVAc8zj9NdGfGHe8itgU
N13AQb98yPhB6EE159W1mKT3XeHLEO2+r/+SNUNPZsf+kjAtjSjSqdCDi3f4wPSFLmHKiYAk3bxW
1zhE4Fs14G3++/8Rw8BU4DZecms3YQ/DFj6dAQZSn8FatXOtL3BwhQHKidXqGGsX6tLcFKcorF4u
3RsoY4x6I0I7PSk3Y38EaX6Ki419w6dtHWFSsDosPbireWKFn26lqLqzus/5Cc9bsgyXBw1U+RkF
GTu2e7MOzw45IatF18EsITGjCmnJxeR41WaAvd9VVnmTHanQ09OCIrYm0mbPLeCCvoGB1qzvc8Y5
yYFtWpe+5y6VphlJN7G5pZoUbWI8kQVit6Sn8/rpm6AZgKb+VXz1qEjxgaWSbbFZ3Or+UbHaMl2j
XuJJ6+uMXMj0YeVNaxdUTQIIRPWaGdcT+Pf4bZB21msdhwomexNttJzNqX8cquo0z4M1BdqZ54L5
G5jfs+sIWilgboGOH/wT32yEqyC8IwL/4R/4tcdER+VI/ZUEQLq6SLSpL3zhB2kc3AUPX+sOot7f
KFPj5IkyVvclGhQGLQA2ES6QIRHd0m7p0vEAb1qVkS8dXpJ2WKfsOfrfKlcqvPAcHScDi42+F6zk
j98g50oZzW23YYTUKB/wBpFfDRtSbU07bFjbrlM2rC0IpuqeGn1OqbtKQyvqwmzhNN/MsiyiL3TP
QY9095I5GYWnGQCuzYYvEX3/CNE9Q9JslHqbvZ8LCH+kucV+kFiwUZKZieqiwrSbTBRwAwTE/Ufg
5vuyiC3icI4e7Es5NO7vEUFsOgbmlNdiYd5wsJbSed4MBw5qkaIO7RSK2a2v7NtCTYHoaOfm4b5h
M2wMGKJaxuouxsBu0jcn5dPkAJsUlfx8G6hnB4O2xOHUGvqWuKHhBDpMvkeVQhi5xwtmqh/VCzzv
MvK8ohCth2mSytsTKO7ksuS8jVn96eAL6wnxP4wQydd0EaLTllq4A/aLurRfOcEIxpEpfA//iYh8
SkOFSxfDm1cpmJYL94KtKt+X1HnOGmbygrzkEHsLgC4Q4TnsmuvZbGi0Lw+9wlZtc5NaE3KyHh6K
vphVXxFp/3bjiUqrR96S9owZbSa8KcUmltLWqUjEu8ZYvuxrSFZzdnuQsp/mdbQrxU68AMUA0RQq
YAmWoNA8yohbDbdN/bMpa0QlrriU56tIuxmNNQTAUdzPzQjrAVALseBxFUlj7GZdjl6+DhncGtXZ
o0Hl9qgD/D1hejQqK9TVxgCBP/6M2M6eHUlEHDQLFes8mF4/6TMzCKRlSlv4vXDhspgL9zTCK1C5
uRmdqB6pMu63gq5K6bj+Ywj3obIXCX20ipeZAEZnXPKnvm2vO6lO/Oe0HfP61T+Mw7b0ByniRfZp
7AdB8DVtcTvaGUMRMbEYVBtWTWWj99JubRvSF3kvM2TG3OMTEla7S93eVTW+iPrY6uIK9Vp3XUxp
3ENnsWxMnMXMy0XCIckR+xHMl+cFXxnzZpVCWpweTOGiFFKMG4euoe7XehfZSUfuEBCL3EJlUXWl
mMkeK+n7NJ0pm29qbrsvJGrw3Ww0lrPiY9lyxrc2hli/9tPG5PyHUoU6gXGezxf7IByzrjcowThu
TPQrjppfOV8bP7KDON6lNElqCvGhZedqWZIqUudAHmU0lgnRt2jzZ+m2BcK8zwR6acOoOEOmhSwl
8Wtv9110e77yChn7/+LxLQofBJnCgC1ExFANLEsl2x4FmoVUrfkoKanbuaYw+rBmicIQzJLWAQGe
AsJR6Wu+PGelmV6vQlOXiN+7uW3DSyR9zgD4pY8T4aU3KrZnoPdazpXp8zxKS1tr+CydOXO5ATBk
Z/T7dbz3UsuTrRRU3mNZTfL3BcgGailwBcLCDGui/vWDbpGCX02I8W3ZCBZnKyqmN4wJFyll43Ee
OVuAJFCHzbYHv9x9dto1XqNprL3V2nfITE0KHzTaoQLk8ZxGtAXayvvOrvZ1f0l+EB+N/yqZbxHl
DVwhuLreWUKEXp2Sz0BXh3Lr46buV8mM00s6XEQHqGe/it9azggL4xqQ8Uasz9IBvZb4lHJipaJ2
sgWgxxKkh3XOPCRGndw8oW5YQB88bX5Ze0auFvnvLmgWxpVYDiZz2buDyql1LrgMf09VXxadcu/p
ztdo1BSJn7CshBR1K1x0wzSgNUJDZlkmb1/H/aJg8v8h+TDOCGCAmjmPmq3mmEDq4YPbzCSgX/Nh
bQ3PVIejQbkp+yFWGQkHSZJl/wrMyKZtzkgCiqP5FAftRpJ2hKPWiqNiePkfNWgktNaPAzfW+JZe
ZawUM6LSdfjirgFJ31MbX7CenQmLXrI67CYS8dGjtsV238iZQ6YesZ0H8XHgneq/HIJA2YglX1vR
MU7kSMGWz9LzGpO5tLUx/tBCGnlkQqFgJCjoEKYw9WFZp2wCo++UFsLKKO+CWAju6+UjXYT90RNF
DGal1msSaUtiuMQ5Hi1O8DKr4WTKfsVWD2zyYL19MMOvvbgKESoMG7/FCGY1NfIsy6LxqCerB+du
DsFKeusqMQJ6gSpLwVvPPvKqkwVvxtXdHaqrz7Kz4XfIB8Z8LbpajL9W7KsWizlTV9dK9SS7MtJM
/8NY15f3qpEQVnRQVUsq7H5LKHMBdXuUiBzvUFk/Kw8Q+xYgDzZYg0onU/9kNbrFYyUexwF6FwVJ
bW3dQjwBAfzLYV/LeEmM8eRMUr4hA9/rmLJ86MoDeSIdLGkd2TxWyxhMwc4QEXFQJ6jFLsKnlUN+
AywkcNP4Q9+RSayqDsBZ8a3lHwWrDuh2spuugEnZvVVcbbRSF92UBxUAp4RTn+Q5G4b+EQt5cJJk
9pSs+be+u6dbuVKc8DppA1rrK5xNKKtcx3I9+nToSVkuWHeVut+euuAPilIiVfR7GFqucSsqO8Cc
x5gShczM991KxdIsnWZPZu3CrHMmlg51AvD2Xbxyn1Lppkx4fZJZ99osDFFOxiZpw4JRumjHATpQ
nooLIJIdHpzpa0taaFyVF4/AK2eoBf6ZvFS6T4EJSjiEACTaWT2C0wqH3L1ZVKUFyQOawjQ3asiM
cIKFA9fuyooRxyzjjmUWgZkzJIJOdizUezSdgI7LTMq8ptV4ZOLfSKAVqNASI2HEr2jKSLxbYvjE
MeUwMWOs6lq/0eGzKf2m2sKRqFqcRhe3fWktgwvZUKrPSR0uUDsk+cR9wCjgMmLQpsVsFUD5XiE9
E/7jjBfVObW6Fk5VoFrq27BUfokAVO8QwI91WMa8i/lpCgYpux2DzYX7STRK0eiug0UVNxunvHz9
sa+zEjoekzyK5hRmGjMMxLlBnx/6AtshrcugkJafbiYK1h19aVZuea8ihImiwu9RzEAvhdbyeopV
M/STl35yhRk3VwvRFZpAPEiw56OBAHWVc1R3YEnpC0v/tylECLoEF2Nt+thvVwtu8kQCYSBwMWpT
QOKIezO4yIF92xNZElDnHB7ClAGeQzaimuFlOA09lazNilgPPVNsz2tzJ2Zol8druBMEPzJ2JfLN
S0AlO2g9Vf/k9gIaF6Eb9fX5PU+HkaQ6cPCw0MlUqT6UeEBgVMCHS83geZ4VTkOYGekP/GxkaH1p
tjK1gVVtiYSJS5oK4weT1NYVn/2BsxJikQXu/Mc012vSfVpBO9bHc6C23e/Iac2jwdVwc6mRRBx6
cNzZxi5ve9Ma3cprT3F6ZezG6JLQVQ+ZSa4Y66SPeugi35T2vHDBqcM3UqfWNtB/rJR/yHQxyb3e
VUaxQPbt1Z8c7Mc1owmmt1BrCjMffNFfB0yFQUWTbIPkfuV9oXIjS1IPlPlVa3Vp5HPtV5tjpIYl
Q7u69gZQ2stoP/0k0r9uAGWa5+kt8TtKwDVAMhyOKTUC4SBim8ZzHi9oA73vA2MVO8UdWkVVlJt/
VPMXAJdR4Gob1GIvIkFPA3jenJ9PfaLEUbgF4X7QteQgTPtR43H5z+Rtf1RahEu8KL2U0EqYsy5S
0Le2xBosbJei/Ly6lsiMRRBrWFb0CtDj5XKUWmKpAgVuGMcgTdpuKT5rBjJXgbqJXH+vpmYXsuw6
vgXv4eXYhPUpk0H+ZkZgJ1vIkFNkrksD3IkV4EsJEAhZOIdlhwMYHMztKlSzjvM9m7wyY+y+JH1G
+vjbMeqvS7BSW82yOx5EYqmZixQPd4lfA9pCqXyqnuo2D6YTnwz4Ix6DP96Pv+7Ra5TTwG4RhdHh
doTAQR/062nPaCAT+QqduCr+u3VGNWXcreZuR8cbyTo6AOpLRltkU+FkNnL/97zJGM+h8ddkiMER
IfMVU8o5+sKDzRtc6cgZrVyLz8VLIC5GtbL/M7WgVhX9b3q7TciOfFa0zFZvpGLMcYbMb+vsQZii
ddo68hICQvne8kITMSUy+b6poD1DGZPhUXxIpVfsWd6M0bO8R9AkV8nrX41UXHeduCVEm45TYYF0
QUS4JDpx8V+UZQXuiXCHHkWkbMT2MSs3KCMrAuLsO5b3+q+gBjjSWx/ZAWUbcJ6nE4rU21p53bgI
ARad9Yl/LQq8f/L8aJqZ5CC0gFXuYyL4pHxMSoCVM0EEt1P/WmH3ZizHzA3dF7Wn9ALOTo9uKJu1
buOZD3ItyvkVNGtJwht+9h6FcsawDAY9tsDYgkJnk4d38XtZRiicIIS2VR54Uj8NpOC9vb6xGojB
rRRPSr8GZGwVC3DCF7YSpyf53w9FjZvkXfG/WxFoz4thex1bfVy2QyW62jjN85dloBMSeCjrRrJh
ABgfyXWx3mZNmR2xezXXh7aVFhDGzimeHhZajooBafwRKso5WDdh5eq4ydCQDYCxutY5uH1WVspb
2qvX89HAIFxBEn/XGtWAvlcWrHPA4gwIJvM67UnzG2InK0sYl8I64s/E0cPqnXwTsJRrTEFSTJCP
W+OPczlyoFx6cV0BUojOTr3gnGRxDjP+LO6AW7UAQbbKhjfVqqXAU7sYeHD2TBv04D5poDJ74FqT
ObyVZbgNIg698pj7E54eIHKzSuTBODnjjvOId6e6qTfHBpgaeCCmO0xL+1GrY8wLVBKi5Dco9tmk
Wlcuuy39YkG0hBGmN/MIIYLkGhtrg8dpI/xbOMDvOER2p6lKb54VNXa0Lao2LFWq/cEwsutvmsoE
kj35WkgLgpqteQNQ1baCKvLdnpotNPXpKwAnkFJNPkQfGBNfvj/39gqY1JGHO3bIcm5IY4IXPJmQ
D7VYOOgzlWZXY4tP5u4hM76l7pEbPRZlL8AGWnxNrikENWANCvlOEmjBxHb1EYh/t3zDylXN4oqz
Qxx6NYkT04BqS1jDbWBg+MPSNb1nSrU5XAcTp+1CEDjJAk9iBA4WgiUu2JeBz5Xbod1XYw3Cbg+T
a0fa9fn6LBQCRmWeS9S8JWDnLNS9q8mlR2NgUPG2wfe69ZaUIcDCmK8BcJvi8VxpPUXXxTFrvjth
Vdj5AlLUY7HF2YAZ1aF61SP2PbdlSHVufEtbc6kWQ7YeaEysXufbEzfgW25gJtI92T6KTvYaal4W
UjBus8Clh0Xze3EdBhIH56PqW1QII+1QFCPSAUgsuDCuWdmuzbILA+Yo2kpNAyMoVft5G5xLVScs
ldx7+itQCSKPqWu5yKTvvPSDhUTU2uOCTBnVWjrzW3vFkMRrsLU/1yz06GzTF1poHRmB1ClJTmKx
M2c49uWa9sa8s3JvLP5VNo3MSNGJgMZeU8EV4JKzNSsRhb2Cc26fTZO3Mwkz1pmf9oskQfWvKsQt
as6iJS6nM+SI/hCKhhbW5RYamMaWqAVmfLsZ4lHjMH4OAJCnk7bfwVAvugCXWd7FfptAr+TWKYzP
iKwlS0Ogf8/kaYLQlkSauECUbpgNgJzbQo1ztsWCzJUgJm5/CMtoq7DvNoLRuTWGdfQ78Blk8RPc
UaEc8i/AhR/qP/aZAAIOfhFITf022mJlLekntkLXIj+uqbJ5Eawl0JRuBDZClieLCJcIdgTrm9f8
uSQkzs6/OUk93LFwrmU84DK07kVBvvtGf5GcZsF9sV4M0C473Oyr5Ye+yM7RRP2K0+9fUn/j2MMT
ZBPrfDe2xXIZJ/o2BOoARO2ClmOuWyurwYMtubpF3EA+FF2bL18KwAN7N5JFWV/nc+l6w3Li3w3L
anaqFdhdvtH+evaIx6a1/G8UuaNUeVoKtm5mP4Qynqx3nL1Gnn7HeNVSJxNaVGW3jwSfGV7ip2ND
6XQi0soTfSDs5wVfIYQyTLK07CntAVzl8/MkJ/9zhCBtN6tkEZuKVgZjUhHfn62aG1FBdg18nIm5
UK7AYxyW4/NLPRYJEw5oHyCZdSdAASXRuEP21QZlNBIs1uySLJ90j9yztBnvQpMlWgKj5BiAdLai
oacD6KUuGcb+BpOByrDvv2TJ2cDW11Aj6b24kNkcZC68wNhAOWFqLR09NkDX5ZXdlJSz6cd3rjn8
IYcjtip8vPaGr6Tk0fuOw5kDJFBbBXILxPdvC6G6ak4luVit1gPPpxwoT1gorKh0Hg8fNJ6M+w1i
ebfNNCqlgHPN+GzFQoa+DmOpZCla89uF2evfXTbgzdlmhLO+m6dIjcJGLLrKy8MpEMcwF2ubJRil
nKG4f0NMvrSKUvuIVfWsRzScKKKXq4Px/jSVvLoyOOkzjwBF/qQ3XGrlSfOKE6ybmzJaNp403OAo
S36VVE9KvTx7O3346ldX2/z3GIh6xIIMLJc5u2B0ABxvOdMosVdVqgyw9qc8yWW+6wwBLdKjKErC
HbWkej4+qvvVSfYD+y3Rpz59JwSiZx8jE/Tjfzk+e/ZleECVWyTM30wOk0Qjp6mqfuOaPlaw3IGf
ENkaqCowJtp3WQEQHg4Q/s9ID791NdHyPik74FkOwu1vzTcsC3twy+chFzLuw4zZbpbiEwCsr4qE
rklHTCcxvn1gD37XsEuj4GCINkIc5cHVB60Vo/Pb8wKF9A1MxsVThGH/E2A2ss7PUiI8dTHEmmgw
vl4rD16OR+nAXImyvJMWc66jQm0+NDSEKk+Z5FGWRTJLjb5RdIbAwrDrpCmbmvvyb3wFvvJ5y8R/
DyyBA/AOFvCO38X1aXRWSWBG+zRwnTDf9x3XkWVQjd+ijQ7NB/BVvbY79bm9kK9l+RJeLr59cgIP
HTAjkGRQm7PhnnkS0UVTxlAu6dXyyzN1x/q8/D0I9pvpFVeY5j2/F6sUmKM+S3RBT4UagiLK2SzQ
jgl7UxGdDBhRHGwD01xE6BGzYF11QxeooTuJCDMAhI2Lqmh3icTHht/yoLM2d37SfF8Ou3z/Gbtm
CYmoPzqT0mcmFSprSfP7ii1cB2r47VMkDZXkF1i8AuMD9Mu+gSp4gVO9cI8d1fSzHTAFGfim30/q
hGxOjkL2e11/D2YX/AnfmxWXvgAzYVKhpH+NkF2jSClKvjmkLzSyq42c4yqw0G01PKeUY5TIb+LL
7cSOZPk0nxM4jWXZk7uZ1uzpmmJ2h8tyk0EDJ1aKGEHZCJuV5NMxC5fBGtGPQFpgAGnw33KRX4IP
48dpn7BdmJj2sfNMntAl1uSVB3egfPpzonPlirJsi/z2gWxJmG5I5gZ5VOgi6HnosBwB1Su1Bj+s
6gEYWfKCCkuN8PTtwZxrUXbjCButHHjECKYr3V5gQzGCYskCaTsNdy25No9P5rsH5jKmro9aMwC8
XX7z8N9yoJY8zkbrGXsHXLFckwEL3slkJHQnqz3jsMMtUa6Vd0S0fKkaR748QFzsVBQiF/FS5yYx
OsTbnb7MpypCDcFUHUA9QCjwWcGdUQ3vuD6p/jAkK87U5OrKp5gMwmprJXak7IAsbTiM0uRm5sV7
hF+P6Sf6t0cx+S1H6ClRNUb8PR3XoYIAwP2BTnACFpiAV3vt88Jdl90oOCQCxCxYkfnhvtaV+c6W
EzAXX4sUMA7Ze+ZM9qlmOZJKMrQF1Ao/DfZ7aLIDzzxdpm3hnyBaaFeTT5Fxd1kwrP9q1QTJc0HB
MSNDrgxtJslJ4jdYseq1wu/Jxh39hfodF6HrJXmr6dVyaAFZlrGbHQ/QOWoHkgLIfRhHo+mu+lbT
90YVsakzibsSdqdfed0WkIhw1Ik0Lk8OcqrM5uklUX8Ai4w4Rp6ktiwIWlMMrRh7yqXKktdDUbVB
qsDoGaa1ql9mQofiSzu6Lhh1QfVA0TctqGSCt9NQi0/202wpLaXzC4sTCXY1Ealmu6h4IEqPxNdP
u1VFQuBzq5uxdQcekh1+I9i2/979gn/450+xC8q5Hd9sf1Ac8EnmUTGpMqxZurYTnpA1wbDW4d8F
KMSewgK5Mipqy8Or+cpXONqkwf6RbrouRwq/0JY7+wt2UN21ACbdiPLtHcrhCliSrS7qQQG95f/R
t49CehtwM9Hus1bcAfP4d/q05XXr7XI3d8Nk3DVpyY6Pg0rdAtxlkkyi39b6H7e4omV+0cSNPv7L
Ka9IgBFClOT4Jyje5zBTpibJR3jWs102Eaa9j1tYJmOzItlZlYmwQXdB0GunjlCzaBoqT6mrLbLS
2nrSnK9noAjIEtCwjnqmhShLOLbXvL0mqwyWYXLm0bAyxLZ/+hTyhzDLBoKbtQ/dIeXlkjsdtqPU
ueptnjuQRPGSCJkrPvXJlv8JLa1hkQNv61dAemiV569lsNAVKn4c1RKMKCSRYcX/cca2Ws/hoI6o
kkW+F6s3wzdSZC9LBiFJpsBMu+8wgY2RMtrCWQbzLpuvkrNUGeHduv3o1sEL9fD4L1YET3MeeJ0m
KhOQJeufqROsB2oaZgS14zCxHOIt2r9NYu3hnBJGDGXIyVcwl71gKU2c4zUkWJUTqvGkpJmvhRs9
cItDKXMIbqb8BzXPnduEGMYkQL3VEQnHqz269Zb3au8yyhTqFgLXzzvsQRnjsLCTJTG0+Q8B2XR3
y0ix+rsE3RUFeoavnJOQRSskZDIUO25tBo+7P/JMoh+/ekzEPvUWTl42/K3xUFI/nsCbwSKOK97b
Ay/pahoX79Mxo8N3d1VQHGAAwysXLUrJAnyu/1j2bV0A1xk9RSCWzww/SVdi5wk35nvU8o9JZBgF
Bf23UV6dgTXATYhqpetipAazz0HO/lZvQk38J0RR4NgPE7zGvw3JnG3l2BngOH3LnkKBR3Wi6DI3
UGfh57f7TPhFRg7l4xlkCvospsqtqsCK3oXBvMEt4IlP7DvYPtyghXUGr2suQINq1Aq1oxkBgsP/
CkllqJDbsj3WqrlEFN7Wnkje3cGluvbzHE1fWl9PSQDP1MC7RJjvkN7nUCQohH17NZowS7fztCP9
TQO9tRS2Z9Xvc9cA+MoHjIJltVQHftjKP8wemF4QRrXlraJf1mEl1SM+1K2a1lsYFht2qMmACer6
cAVyRHfGt9h3f3FFcXS2unmuosR+cBMteEWqO+FrNeM4XelQ62MjGz2kHnfFkTODGrPMg96wGbax
9lESCiFV0/LMoSE0pHl3oBXM6gKSNcahL/He4Xe/Kfv7B6B7fKRBGdKLOUXS+MB1KolF8/+1hQqO
Stte/lgWT6skwgxUkrBjvK0vlelfE02Rpn5l2HhpyF0nGwMfZnAMsCd5vHp/+26CJTWMK8hXU4GJ
hZacyuxZwy82CCzHY1HdWs4w1w29KgML3Q0t95cNWHQX+dzlPnfALhkriKIJf/7syECZ7L7yanml
vxCvkeybsDkwLw+BhwxDuviicTKFtU9Xg7b71n+h5mbmuqZCzzOssEB9RmS58UxNOFGkhwTZh5NN
1qhe4c6nW+fiHILOcZnREgynB481esD1efEYxGxfKDWnMwXSvcXtcTA8t6K1G6XlontGlfyzXZjz
/eyba4bFRm2fcHNyKGq9G2JpowLsI9yghggeyvaivguoLtwG0jHk3acUFXDzt0TA2Aakw02qmGix
bP5zcx644/kU1/pjVpBw0j2JxRNe6wC1KdtN+3mbGLX2DuAHyTj2wGM2YIjlBzZ0chcD+CrcjDxC
VpZqbbXgYD1MEhLcsun80JqsI9cY/4BUOJSG04rWSyyZUlROZK1B6BkIFecIAwvvznEKuyAoNYYb
EfxMhVsdWIRqIOwRn3GQftZ5LX4AMNUBONQKvdbNziwl6RImfTopOUaVr4VPdlFFxZLoe4s7QCPQ
rcKBhQdp9F3hDZl6f6G5Oo3QmtPliWRZDDgu29IRcAPDLSwJsqwbi+KDL7u6N5DnOwhdHCo105+Z
8tEx9MaxySMXtbPverUxGx7i/KLLOw17SRJY/kcZAWj2hsvjyzovsosoFw9W8L14sIGwaqun1f1H
+fQSpGncLrLzJMVpRc2+7Vy/ck1/0I7V+zrE3N4G4byAlJmvc7wrFasTfIXegyq2E8ENAcL2CU1o
BG7HYvTkuSYrNo9B6KuOM968C1WH8ceXF3uqBHigXlKk4EJFGUcR4tLreTtBoUrYMPa8kvV/ImMq
fe1WBR9Y4P1qUuEaDFfmCBpRAZ+qnc+Wi1MS6efnT8z+9i7+lolGZ6HpcuHxRCz14OT1O27zsR5H
4tV1eSRPVvpJuYPr2OsbO0UQKu5cXbZbodZk0yT/nL5zG1ZaCJ5DUixP7JyjJsF2B2yoTzjPnrGc
cq52SFBYiNp5grOhmdKW2IDMeJKhXwD32HezDGE6f987KI/3B/gXtSLN//C7vTPOIOKCXOsNl3y3
07SMmlN7ogKzQ9oTXc0IOY7EY094VRugg8fvP9YTwvUagBVbTS9yb+7mfJtmMKN+KTfy5PJTXVL5
hifi2Nt2icg7vYgh55vOssMzHOtXaMulMquBwGjZK3TU5Nkuo/5f8IV1MiidAJnfYJ21lHl3IXl3
q9e105PjaxzXz10KwvBnwjgLoZWNRZ4DcLhZp7DQqjZfXtTfbHCD30BhIxRqd5QUN4FWAVw1wPKS
ZEtLgN5HEC1bdiXTjtxX4/JXcLb/6mvw6+Rs706ha5800eL08WjMFWOaR7nJBzqz4VkQcaFxtlWN
n211E5EPKTtZuk8JpqOqY012/bZSVo+jbcwbmQa43678dPnwh7MGbq6wzkLbEJ1zGA0wzglwlJBN
C33EgJ/PG18sic1zXl5GCkSc+TZNnB5wDU5VurVbPkAHu25B+xw6fm6vc7y28Lxk7iVD4LnYSOLC
kraXc+6BEweN+N27OtUt3PB4Je8JLclyOhlKi0/S+eOXXXHenKbzIjri749XlRduCd3stsfINUoG
PtUSjlaOW4p1IaNrCMZKiWApRHGfzP4tvR+5AS8dTMUeSvcQS4wIF95bn2EyT0tWCoPMDr3oEON0
TUig83Ou3vTxQVencTSkf/fr1vDRbR/BQmrTEjf8BVGrkg8z5pDlJ0dddHb6P1KPOgFMbfv448+a
5olVV8IbTq+dkDjRApe3oM4/Gc75O//pZIn0qZByE57tpi9fs58XvC8J1bCaFbYpwbUxLLNr6FqC
dqqNO0B7z96ByDIYEjYYqqzjCJ02Y4+0iS1h+rANw53vUIAeFMpXhIyM4UL+D1t2TxRjQSLaBfWM
IkcYAJGAn4Ow1TrYup+9HKonl5XQ1WQJ4dUx9yWqE74NkdUZsBM/wX1JSYehxNnr2a9AxSq6j9TZ
mC2HHd/clU2GuFEuiEO3C5v1trHeezdlGyUiaMlM99p7Pdk7Fw/vcVrTDKMh+XuuGWZl7Npk+LRH
yd4sjUWmUJXs1qXBVQduOkmKaiW+n6PPcQCVTkYxq+0CP9E+VkgPVwsWO7oA4GhdNkR7rahNbx+q
GiWj+oZMupdhlVxNtQfZBZn9wFG56b7M+zWaWG/MIpM/5zb/9aDqhgEM9bO1ScUeTSFI05jqUXFW
d05RcxoyMVfyyqZ/bPkT48/mRH/0h6kVSNRlmAP3XCsajPdhJgyeBxSJ6kXzoCZLh7uQy+04paBD
2LQztuLWBO/Lif26wJ+BBSKxuO9IAJ7eC8X62Dp7yPGT4I0tEbD8/ZBXgo3tvuZISVo8U3iJDq2e
Atiii10An+hk6wB6AEZ30/miA9fCNobdOC+/eJ6RN8XHfxTGA8fUWSJ9AkMnkDylsB1rzmJ3E/Rr
2dGyB3RcyxydxYBPd2L+cDGfdKQcUm6Gn7uLgjYDjWyeARPsIoKxQcwIQcKbmtaxkCp0meaA19f0
u6065tgngopz1OxyiK7a8hr6WahvbOR0YbDZe6MOqXEC2eSpfff/jix4Prquk+1xFoS6V2+XIEM8
mP1F4NntHszS9Y+bQKPOYb/D1eTFpfdaSPcT1ofjUVM2KzIjoc2M++/KodpdJO0FQPay+FtQCflW
3IpsrH3OujJMva2ntJjxHyDWIiJfpiy4jaELmF/JwBAkYv9/Xu+V0Zv1EkNPTFKDw4pBFWBkT4fX
jBDP3s4SnAXsK/s17NRjeHviz4AmkDYY3lEsNzO4+MJpHn+vwMTOqL53eZyXAMqT0Lg/HpRJLtA0
fLeNQ5ad+lgVoPZF/nzf+7CfhRSl6VHvkx3d6DYPLAhIGE90MjtWePOtzMooIOGB+sCjyqbFfX8W
dqtMG1v9no0lM9A+GzemCYy2WDizsDJLtVv+M3s1n5Yz9pOzwyG/xfeqqVoUPyeUzBNaF/r9CSMg
6tIkQs+R8wE0Sy7qqnb4qZZOVZNtRHkfkU89dC7kM+d3uIanR1ZOiO+gfZx2lI0TVhACXBETxhy4
8qTBfm5NIYMHr51sRQGrspf0os4h6Yw3J0UsUy77g8r8dRlIPO5qHc6Jeovw9ZUUQgz75+D7JX1w
AGAdrIUhbLjM7r83CqdWiLOkDPYBWyYH2EXTtUYdz/L9nyt7ubfiiWOSC4wKCGcIGOMjmdiZ/HtS
S+xCgUl/QMc7vC0gNgRsuCfSCm7puCJPhfNJWoopRvt/dX8UXlilJHZ8iNEvl8jdn5LMHs2IR8RI
d2ySpGKwszuNTRnv2uBJQXhut1vGgpABtEIPgVl9GaEVy+raKAdkgUT6C7y0PndG0sdu+N5oDhhF
uM3x0eyhNZzK2aARy802acsvfWfdcYihIXkAdaBil7Cse/I7+GllUjA0u5LTiy3CZXRaYEG1qBJb
mWdAJEk4MZsvwWJawzfQr72340eG8UlFNY5kwtIG/qQmnLJrvGgyWDh4Csy6ozobJC+ijtnY8/ex
512ADo95dCreRdS3i3CSvBWcCJQp5opuBbIvULI1DT7SevkwgDqj7aNC/Vvt4f0XJQKfR2zyB+kF
T+Naki95IzAnqoL7CYpDYdem/Yta5jORJWYzA6Nsyo/+zYP180HDJ+YpNDhACFslOuttx99uw3im
GaSvqen+Is5E7nNIAacuLZzq0kJ+SLA5rGKbEMWptloavDtynFw2/xXoGXV//1Uf7m+PfVTN8TA+
Ro0mPV6ci3RKmtvWmCwd5njegr5fiW1FDC5mmnPtcsXivFVrX9/E1sslnJqbn5Sjs9sdm001dGvX
3ctp5tf8wSvNzslhc8Q/kmKRmwEEQsOb1ti38R/cqQaxLJ6uQNXL4NFj3Fi/3a78YUCtpjjUmJJP
/9XZgIQVepUN8edSIGWPI6nty1Yvsd/9PW06AP2/CqyBKpu3j+1w/YIKBIrK/IGxNKM/z6ghdVvt
TztwTlcQr/rYoayVOuYVRSJCO0NXEVVL2bwgT/m8Zj28M8PcfT+DQxGdxQr3YVnCTwC625w6Juhl
0tqS8pqI5uUH/smC8SqN01NyaY2JeFJMtCOiFqRFUXHpqCzCzvA8bfWMIyFK3VwgRdNp1TLe0xGQ
KWmT5wnOuyqwDYR6xu0hcR4QBX+3yyIpmJ/GOyKUO43stzFNEPhdTcrHsmecqFYfvrJEM5HMYiU9
2WA475gPvvx2FnrGwFRAXaBBFa4wMR/wAopw7ijTZurIca9QgOX1o34VskGJSPdKgaoXdnfZh4cf
w/2HWXcmDt0txyOEba13Q1FYd5cPHNZM+YPW0aN7uBN5rT54KTxsfZka7Ff4eJ2FagGGUoTfSzws
5kmAALMmKn4Q9L7SDOmE1ZEPGGcGzBe+Y44qta9krmc5GKoVFHJE7JsZ5+AME4wl5u3VUqi1J7J9
E9GsJiK+vABI/unOjZS5qbDKWcbURJKjWo5zeWx2kx+ikuqr/F1mfRlbqReKcotuyqRoJAL5YSyu
UpHnGa6faiETgaywXexnHeE8yiJ6zVsHpC8mRWtN9sr1yUeN39FeELSRGjd4vApF1Z8gz+a2TXFk
VhI1piXUBhTdy0/AJvBS8BdgpbZ24odXMPptW4rDQoWyY+FgZK1HwrIY6ItKjAOAS89oTuGb+FHn
MgUrH77hpjOohzRcnJjDDoiodZ+jA1U2xUOsd/eL41T7y9b2iDLrlY7chipWzIHQX2GJuXvzyhKp
iTTKpDzY6M+PlxH5h9ERkFYPeFx8Koo/NuAr+oyBwQRJnZ79TLapaK9B2R3kIezd3BlyEKuBV+Os
IhAdiKhPHU86kICkgMHZ5uuwTsC4q9/qmEOkKf1PjUN8AUTLZv/7Crv9VK63Gu1bAQiAPiA58Ayl
fuSjtt9zFcemo+QaFtxR85+IzW9mkcaw3m64AsAqLfvMXnb/lt8z1uyI3oN4lkYSrnWQTxSqGD5h
ICRZhmQvjaoHLIRifAomejAtoBHTOq8WtWJ/2yhC0EHN5X1qPhSfiOu53ANcjkZ0nZMf5krNVxIj
IE64hMFPcxKVe2H93OKyV9LV7crHzMjE1OM6mpW63rumemhdfD6CSlGms8uhwtNHvdnqbxInalqy
ojOgkS6v24VzOjAYTBK8jwEsWp80hXrmIfKvBs6Qhbq+ziTEX3ZIovWBZgPPHRbiILGcbkBO+zcw
rkx4VNB4th8CfOfuEwZG+uFXa/OCDqNUAn9JDwRk1SJ8kdAhTrpH2ll9jyUollUqcA3pleNRM2Wh
7Bz7ZXM6ODt14nIIOZCyqO5bICPLrNazSVd5SMdHztytfb+el3i7FAV+4EpyETwMCk3T2zFIRSTI
AkPUsE4HeTdp6vnlgvL2e7L+7Aaa3yBesaHEOKHbo0bSu6yiYTs9/IOUZ06NN6VgsZa2WjEb/Ujk
kehHlB9gJOe1dHg7f7j3RdkJXIajD/D/WcWMVzHW+YIUT9BfY7RtMDdU9Na43MaaUr64dZqh2evO
eIudodvGtlxwat5AaLS0nM3LfcOpYr37lNMd9Z/Q3h5XorjQoDIHdyHt91+T43p2h7fyiyxfNTCk
1oM+r7IPV/uRFnwN7GZXuJicMepl7HzLkFP9Fa+wjY/hRdboz/4f3iCjkdI87jvz42bC8aXCui+1
fJEI9FhQrPdv6F9izYtTWcLjndOSh0XZ4EiJCQYzMLMwCajhPJDHcMEzgdpSHnqZMTV7ECAfdHws
kY0U/FZS2B2z0tDFSqrN0DvmOLKoy8QvbU/mSYpWI1LnHNmY+5BbAUHTd9zUVpgvmoOCOQsSPSIT
WCoW+dxHiI97lOx5bRe4zKb6F/tPiMWkm5+Vgb1w3kIBKSD7WKDpU5lqix+Vz229m/iEUP+oasBW
LohY2W7EDm3/0982Pw6GpUIAGp0MeLM6o6BeGWAH42Xcy1E0NlnDcBd9RJz0ZHHCbjl+z1JVtNtz
Pd3Miqu/+WIoC+/SR/2+TLQg7gaEQk3xIJzIZstvrhCs2mOONh3f5jsavPb4CRDgXnlkzVgKbyDU
gt6lo6ox4OLq3+1ufi1TIk8my+CyaUXF2SJoH0uys8Ns0LvngSa6aKh7eLruZiPikgia/dcESyu7
N3qZKND31bw/cwp77n93f7o33MGHZdMpFgwCAJXPsn5+pHODJQQDs0t5/YsfoJ53ifsrO4D0TJxg
8B7XPi7y7I8UeMoc5FTRCp6e5FkYLXOWXQ6ZmEzjuTgG2DgfksJ5ys+fjtpCd3WXj4uHY8nqi21W
29/CswkBZ/op63BcWjx4NRiJjSzJo7DQxmnzvCmi2FdHHR7s5GSFqFbLfseydc/eKC1IJ0+o/Y0a
gKROQbrn2ZCBKKCegRy5YNARBT9NBubv+kG6iR/JAsUdMPC4HMa1cdZHxKWZIPEI1AazCGJvkmfl
xQMv2ZbupXIV1t9UYSzcylIy97JaVAUw/vczwFxTeXo9p97ANBRgnpkzGAxyo6KCrndXPJ81FvwM
zYJ67vKsITKXQ/NsJrT5rODV5m8ymsl1z0m+HLQii22IVHDBfP+e2yNC4IsSorfw6knmyNPi9k9B
oo495/lvflNALHh7dnHuDYHd68ldXsrmL7NBixGJVuzCTGcLUgFMI8Ozk4Ghvx5R+EwVoebXKFEN
yuSKgzYsfhses2bHaQ/sO4iW/RqDr1nXadFDk4Y2jul4cLYuRbRuTaGTL4o1CB8XICaXA+f5sE6r
1VDe81oj7RUOT/0uaJWdtCCVjKQI8MAySITZyOkZJxZuYT/S4IEzS2uES0koDoT2bza8LjN63Azx
ZFvLVF1CXCbCEojTpJwldi73x9djEqJZbp3Na9P6PIB7QZMj1A3FHiaKN051azAqmJqRG8qka5hF
oJgXaE5B76LYktMt7iao/OXZsVdc+IZw2ywYXZlot5Cx+E0IxwTFZudl3964if3PVAhQU5vXjwh2
Krr/CwFwSNY39rLnOemIEuYD7C5QkD916BXeElC/Hx8riHtaQFuJg6HWnf4D2lRXd1bSSNd1n1T6
kXgWN1rRQ2zsKAO3zoCvoORsDJ6hx0Gwl3drH+DicPnndEBMfStFzxlycVNLeJ8sjkN5hfuKzZE5
7hs+Y/a02vpkl/DeWmpxLKn3u4EYusYem8Sz6napjXKjnveUhh5X/X5JBYxoP9I3tWIM+cbbIZJi
0xrCpAXN5D0c193aNtj5zF1WF5IDK8aLtn4yhcoZQkOyJhnkMgF68on3XxS40X8/Nykojh0DgmFC
lESVGZqGfxxazFT2CVCyzDCjmfJcQdKCt9YpQ5LPsMIwbpE6FyqZeat1rmOXk2nl9prZRKLyYdwi
cNyKyXy1mpDF1QctorEPyRmhwuE5G7R6XTyCYHzT9lGcjiuzlQhUJoxWHRG3zjHYyQJnSo6fby0f
WC3aXA4y+nY6Js/uc3wiqKU4XFwGvh442cYBNI5YMc/UrMKu4j1A/HQVuExBPRBwy9SuQjtk8d4S
Cz2gnSYSGcQ88diJsUc0KmcI/QsUOYQqDLg4jUQUyopK1P10Hiq6a/zVeCwXGrZ2zHvJok2kosEc
ymN2lITIot1Rwkp9eZdpkqiCJ8Cc4HBa48c/zJxz8H+H/iK4ImkhZIwjzGYNFsNmz957AQA2isZm
Q25QZTWE9iiFk3nwoyOWV8EQYP/nZfm56qAei/Q4I54mBmNZEQPKzitLwHxmUexfK8mZpFZ0aESF
Msv6MSYdsxpHzBOqNoW5Wow0r+g37ii4oOdVBk/C3tItpVJWMMJ+2+vPK3/TVvhFM+vQjqYXxunw
aQ43ZxU/syt/OyMytR73NUI0ApAeDy83wa7bYktwAnxtNJz9KPhDmKHQj7SyOfdPb1sepVM0RdOA
VNb17ErJMO2o2TgMg/Vj9NtQ4if0FSNI/zRkM77EePhN1iVHeh4pqslpC429innTOyKJ4X+qaBvG
fpHveVabK11bcOsop5OUr3Ara+gt37aUcgKSEKkq+xdd2j0u8tAnLBuKKxbeHoug+YY1u8MBurvy
NqVmUzvk8thb8FqI+Q3thhMnl7m37HV4n0Dy9HCFYM1P4iR1yMCUoUO4DZgh6kTMo2UARzIey6gc
2Ra5ZRY6Hp63cwCGwD9JRlI4+/bBOHofCQ+iZ+/hErxr7AeZD2nXOnT96/P2P1RBz+CG/KyfzT69
g0e9kqXCKupvRxGLj08hlDlwNV4SWyaKVYQ8bnc0fvcBm4dpC05Lb7LLkEo/3tVof2nbpUqTrjc6
LuxqiM8kPC/kKuH7RCIg+G1ZQ3ZqT1eRzErx9uOkEnrDDAR/ZXe7KvEeGnFZpZtilyBPW76jsLzY
8ygurNCNxFWHstuy9wxqga5GmUD+reCnd36p6aCW1ZFK5lk9V72S7xEnqCVzt5hGdmqliilbx9l7
Cb1xTi6JKbQlEj1+b7iFQVh37fClm08KQmvIZBJkTBebjMNgNvZdiVM4JdYHz6J8G8NzYKEbdkBD
VB58FTyBTT1ACt9Gvdl/4XnsSl7rvBTk7AiYCWA0cXei66z4D3FdB8qMBmYRN6E5BAyjq7S42y0A
fzPx2+CphfVSGgQL6jdn9mMjSbF4MvUkcT0uLGbJTRDP71La8PUvU3WtBhHV4GHJ7jjr7khf28F2
0qj8Kja4C2208MMTvbgcc5C5PCJtmG5x1wFI+pXVyDprci+80zpe/iQu3rSXP131EYFPlMEtjUUw
8M/pomFr1Xjzkh1Y8dq6Vc2DgPXmQj5miE2/GPNx1r+IcMKDlv60udLoSjwfhm8LFkjKvwcfXRK+
bsLTYZN1khBZjRKx0R5bsdTmVPmLIMoEswZ4egl06pg6SOw6Z+0A/akG2yBILE5h23b0SWur3Ze+
cFYgWNp1HcjdjkLmrjX8M/ODJ5m2N6+3D3HMPAw7yCtHiNb+7RAp2ooosLc9xJj3IbR+iDbsDobV
XfHaPVmzQffVM8WffLKUaubaahoP8jm9wVqpcTjmywbUypa57pFGJDY06mRTVDmr37EjywL7+prR
Pn939PLpRkMpeXpr7Kh8zODhyWS3+MItRfefLLpAkwOmL/L5wIUUwp0+S8iyDFB8wjj7EMG6Z2pw
8wbzccOy9yOOQv3wHqYx3xOgAeoxbGm9NgS2HJlf2mt4XYwTlENLEOYmlpPy37NDkEqpy8puoPKu
Be0h/pk11uzsKPm4XTyuZ/beaMr90hY4BwI/Xzb+ku1r/QH9T5RUiPv4UWm7SvyCsi4pt3BkUI8u
IvsRw+Dbm1v1K6T3xEyBu+JR6hss1NZ93UYnPFNsHEK8RCodDBY+FVXB1COO1H8i/kabjVj8r30F
BaeZx5o4VOmpQtlpzC3spa/ecGfnlvRNDgrri6B2lR+FpR9Elwbrr3cNeBlTzeFiq/03tFSqv0l9
hIYi8F+266fGeeokztKTQ3YxXP0JNlay5+cnhRycHQVeCjRRS6xUjTC82ERC8t3QwYWhc1+P3Kj5
3TJoMeVrY7D6oyFRUf0VFLZmasKQ81hnN+tzQbSu8lVNLwv1dr4w1KN07B9g1TExS2maH6hFn0HR
Uv7GYBqHSv2ccr5MsZudpWKheQMutv+1q6QiVbWROkAzr7YM0HhSDv6zyZbIeHctzLBftVKz79BK
PftL+H0IGKEhCZ/JpO9PGQ/7YXVm9m+76XO3dj5I0ZUWCtgdKwskMLHo11hPuD0crFIWVW5UCQv+
fHzQcfXLjfG1rCtXEqtMdEhpU1DBHS/TbLEK8eIwWns3sXzmC5Fmp1vIbKbxG9IohKgBNzD6GdtI
/XUzdGGgM/rOz6Uju5d9Z7SJGIEaJClA0ilYV/38Tgwhals4n2O0CrH36kZVj4qywxFskF7zadX5
93MHAHCsXghR3gXQ4j3UfT9S7KdnqL9ApxILVe4Yw8yu1ItZLC/Fy7IcsHQPiF4MWYSP80qIPmrb
zK13spk5FN6o/gTt0g9MOleRGB0DqaJ5Zijrf5B6UUNRljabDkfT7R8GRxcmL9gqbNAJhqtZTizq
lJNWLhaM0nn158o0VHERv0fwStJw/kcqATCG2/91lgGJoqWhXbHYCDc9jUU8DEczAOHD3o9WdvFI
EByuUcv2vZgkTz145XNYknab415hGO11+FckbBdHeZy47XmyZHL70yKIaOQE+r4jfBqUBDlzxOIt
/1uMSijkYLqf9e49E3NgN5lNK8UwqWvv1vIexYcMVcmFUdLu0maV0g2c2nS4WN39xq3AgpTB+o0U
vXljtLN1ZnihJ8WS9lsHM8OK6fLtq3J2yWfaV8y8+eAHuYdkZZcwneeOxs43I9B2XdgEiRBJGB+z
yc1++1LJbcoZ4SNcDoZGH0SGMZC4NkkH3ft0LbfNjmhmwIQTWAffrnicZ0v/nd027ZgXoVIpMxDL
4LIujqPdNpAlNLoGQz1mo4NxPDvTwHYiylcmy8Q6zvLxc6mudGscsu1lmPZ9p7IYTjsrzO/J6NGo
Wnx+voq6GD75lbCYfKgMH3TH0lr29FcC+cpRD3pF+iP4eHLthi39NhSsgyCXZX1+xsmcrEMwx80t
9gOQpJIlyKDKk5Qv3uV8bftAu1br9CIWi4A3tN3cjW7TMYFrMiTHADKqqAIO7QSyB3a6tuv+9Bws
agRud1Hvq6/Hwf0NJAVzEGARLgpdiLEkff/jetDZIbG1vsDRgfk7Gutf+6CcwQhVXZrhjwv+J3qE
+Q7vTxv6a1qkH5oaRlerG0ILsVTcDrOJ3TzMKPioFCmDIwQ8CCZaUCvvyUlXFPHuTqpZSL8ou//F
o6H8opdp3fOv+Rjrv/csx2jN14w6Bhhr/owmQ54EYQPDD/8YAj/8wd7Tng7ROAID9dvnFvuIwBOB
6Sx9DBb0b04BIOLDq1lGwX7jVJwoAHbeXO1No1NIO+YI7uIXqU37qehT3XQwLVc5bXzIK6xopkDi
uni0KMNAG1TVvoUqqYjCgjg0QLNvZEJ3nQV40yRmGUPz96gz+X2kqPcPxfe8y8jeiBI2WiDcDhb4
kmAMnevUwCRU9AvRuPRapMYG6+MgITmLOsMjRSGFjtJM0acJgJvj4Q/nAVKpvuU5dHFxS31+64lf
3AFqdrM4IZye5XENxKNCCowZda4QImhqSGZO+gdJTazaO+Vdi8tkJ4aDG47LojrsDBFES7JTaLZv
Ft76NYw2kzMwEafi4Byyat03UQTVQ0LzQ2dYVtp9W3TjvioDcpo/GGTEJaOpbBBl/rf+sYMvQkgN
VKC8fc2hq8joCJVOU5Bq2MQx0PTryB2hi/duYR4kkU4u/wnjpjNdZRmudkljtQbrbCQ1gmG3FXg+
se9KpqlbM2aB+fEgi6412CvaugePxbirdMie4pM+T3IxWnfq0dqTg857YfZgpW5Dqgxwpt4Tzt3u
le9p18UV8AVt+dqCxVl5G/oFlHwILnli4fGr9dClF0sy4GS31h/9rjy+zKG1IQqb3OlRBst5goiA
PZSyECZO4/mUCAhy+9N6SoFQsrQF3i4zrJt7ce0PYrHSFzwat+fbX0XIQGA4t0uY2fNkbkyOAil+
D4fa3+Idh8FT8sKKak96ABD/CWIEiLzOg2hM6bM91c9jfzfDSx4On/XlF7NWaMxncsFU9N+nrXv7
7CG0FoK3EFzxnXCxIcEnzpHzQfPNdCwZvt0cuIUvKFTF4nGEB5HAZ7oz3f/HqWMorM6Oc3LuV23T
ZVOmPeLf9EMZDzGhYBJoaAdbIVsfU39Bv8U0m8qTpAMrcpemOF6KQ8C/xbGYuoVbr1zxNrmK61yW
LaBEcOkITpHWg9+g87viUiZrnEx1JRKE/dluCbc/yOOWkHbgeYOckj0ul91iP3CBMTeOlnJESm0f
0zOEtDwISgRwrZDmWmCwxzGODsqF4kMu/x8DtReUvpBm0PKFT3TPtM6zRtcWQoLGC4o67cQ3FGiI
h0gOc9Glxb6ckZ6NZUuLGCeWC+QasrENIpJcZnnAOZdnM+5LY/dC0LAbxgS+RdanZX3C8rBOVR4Q
+tTOX/XFpkVK3JJMXqXBgGEbZJVwe4emSBdEo7VxVUtwbMFfOCz1Eu16y3jx1s0AlmChfv1cMAgo
xuMot25LPEtRz2tX4Q7u7uS04Se0Mxm7OxBkZ4wsFmgNFrY2KD0NFg8xfzNINtYNMWG1CA7yaXPC
hGbovr+dIadug50dP2kdkeZvhgIG/DE646MAFEyjVef7jqs13+EFRZdKc9xBQTbQ0yrgMFxaevgW
Qsic+uW3ZKxWgggkOcJakdofhoB3lE933//Zg5rF1wWPkaV143b94EzpfhH0PT0WpiHB5ARTHnAj
BX/Hd0Q0GbvPJP0Zi0MvnfFT4sNaFtjRvKP/UQqvjbDqOO5GzZsfnTNvyTfFFwVf/BC68eb0BIuA
UeHM+3NWHDTKh/4vOJohhiXZpv30p7hYElT6O7Hsa4AszDBCtQDnpclil5siHJ4HRM1707nB9baF
DzyRB1Ry47mnHj3kTfOdvXRiJUSE9TS+98PhvdsZKUuNEioNuENLYdx1RwlMZjIlKruxqcawXCME
ymbhgQ7Y9kECQYC4BTiUDd8X76gus+d5mjnxJuAv07EICOtYqxAOU/cnrOeQucSvDadhU3qwXI/r
AkAxN4iyNZamqm1kISi4GPUvsNQFDHLInqXsIafAaqWiH7xteyRBo3YBC0tn00+jEd1qKBk26YVM
g+VuEddbA/SW5BmNjw0ilLhgNhMfpHQ2HDxcqWKyEN69VK6eDWN6mc+LRfimJnE0DcQfSs/5F0f0
4IhDYEIfM54seZmol1U+tLOPFOjbKINJ7kBOxr1WkzwjfnJG4dMarBZuhVa7pbLPVfMm5/0aAtrk
Hm0T77ZJIlbasrK7ggBn/WhPcXnkv52R8eqzZP0OcQO5QOdEpO+ZEe/VL4Eua4ZnczBdAPCSDZHV
CeHlC191PtO3bc5OXaHHx09luv0OQ3E0Z0+Gr9JZKou6EiJqMthR95ftsx9UkU2LW2dO7F+S9WaM
Oq1mMKwQdPZfHGPhMYmriI/xa75AyZRcvtUYCvqnQx1zCyFQO3AaMghmgSddUmqw8ilvwy4cNGf1
pjF9Y0nsOXDGLY3iT2bmug4LYoWBi+Wfng+q26q68DEiw92FozSgfdR3D7M8Gn6Qwe7ebWpLajrb
HjCtFTqGl0miX+uMerKx+pfySZ2hx76BwzHnzoKNsPub5KeqK3f7Qc3V7ToTpCdHpLTpoljNAGHo
qlV4qhTjIHvpU20gzmIX8fDrbqLYguo9YrVvZxktmj2mPcUYN1A9cYuhEm0I9TvtGj5tx2Y325wM
+n8pN5Mhs41ecQfJQyxwTIoYQwWtqWwf8YhBNLkg8+HsBqDvs5ahAepXyP4OW4Aw7X09CyGATfLz
Eu/dQBzIi+FjKiJKJrOaOUezLqQ6f3G2GGbUmTVFgjCey0U+uD1ppedBagLFK0CxSI2vAtrl23t2
P5Dswqh1baP3eMtLpYrdHmL8d9xjpnbLqazFlgloWh36EL+2JwC3GmpoH7vZgHpM6ZSneJAlze9X
o+xo4ZgEQH58gQZKV2TCBI0WJPhhkXWum86srJyxDNoMXpQn5TUgStlO0XvwbjxNObtqfDbdzM7l
qM943mpO3dc5K2vYIcl3zCl5MBBmMqWio+T7jJkjmskT3ezWK1yomgpIXL3UkCSYZTUf7kZ3gl3r
ziAMOjxqS1OApQIwy7nKR6GF3ytMI3pxJNS7e8qzGzk0vKKA7aieBKgvPAL7u3BTWKlB/co/7hlv
yBuvzB7+xvAxz7lipUmcUKhkUTCuOBCIwwRdzkhMst7VAJycNDUF7bYLhfRB/AKvmBCf7BVXlFhE
2VBL8EEyX5xcLHbr3TcbTL/EHXbI+ShhFnzYSy1QQyax9a1Y+mXQ7/pK6X1kNsAY2w094BuLXj+P
5lINb37MzMXOSMWXdTcpFfjrGm6ucz3SuLbmNw+Qq5jPc0TxWPzlcLdmGZGCD64HQhUUc48PJLBQ
SvNnf24W1IS/9cUWYX6oiLIO9MtFhhuDwZiuw3Fd/OIHQp5q8rHnD8J+8BeYAT88ISKgG/fbmz2c
zFagTBNXs8hWuwIpE33BnlSFJr6mC851EHQ5kbvBkhkGOO5Jff4urPc5EseFMUSCZQcmYhgvbEIq
GB5AYoXgfRjECKvmMIcWZ6b5GHBmJJSV/FJohG0VV3l55t/mSoFgECnlMkc7wMsHUWVF0Sjnbta6
U3TPXTIrzPUzvlOQHlXckXT3VkmB8133dPJOhWWy+IF74gP4FaTc/K7WuG9eaJp/qnSIzZlcPSeO
Sv0O/nytnvhAm2027tWuUTtZvHEOhzedb/WK10Kb73nbxa+P21AQxTtgf9vDySnK65VZHm3EglWa
TjpmUr60TTBuQ+SUQHxt75hlQPpn4R3yZtg4GjBIzFiVc0T23/WP6ZgTv0RY9eslw7B50Wk3rind
Z+C9RTavLuSDCl2jMQ4sUBqoy65m3aauPnTSe7VvNJAfcgK6AUoTgpiVjktxFGxxsqab0yohON6D
dsVzuy9inLEff5xZL4gUCr2VhqEx89hKgVx/xqzgMoM+dgDqeFEevVIuA421RPb99T9MN+tgJFUM
QSgqfuLN5CIV/LiRq5OvKOOBckh2w1a0/AVQZufLjZfkmaUuotk5zRiSXs5EYRGVgUoe299GkQOK
BBeycJay3PFz1g2YcqoctOsiY/emQ/dPnXG9sd0W7+Hi00CQD0thrlL/zdux/bv/S7IuV0nrYwqB
WGGwnRMgxghxGrwILPoGfBN7NDrwYO2T6jFHyKA3N/n4ErBobTq2b/Hp6exfHXANVNkmy2IUvhel
x9zUCGnZQm/+YMIDQlcUVTaaRFuRbfmpGF7YMSjBUi2AcCQLKn4DMatS2ViA6K8uyzaUakUtuFc1
fslBR2z4Gan2FiV4WvTbNUGKFL/bxbVQlBpTQZy/ce3F+hOvNSr4kQ/A1F3KL7ef9wjd/thYTkPt
Cfin/yUO0oJ4Up5pDeyVMEZF3YMiZRp6TjmfKQOjtPLyyLuhnhQzGYyGVNlzQvtMKeRug8vTjrcQ
Ti9kZcU5vnYFfHTOSopImeOkH8Wu+4Y+EEZ/CHT314HITT+l9mQydmMGS1AhWEE4E4brBRj4rxou
1SLCMWDRi5bnXwd/V65+sGUMWnpg5E/S+8wG5blEN7zzUQ7KYnWw/WHVySn1f9RXZYTipkbOHzQI
lGU/z5UiEp6CpJWclCZOuqAW0WNNuJRIIzUa0jsADe0rAyu8LMXLO65hUe54mOcRRVBGzhQqfeQS
6/sqqrAG3c7SYf+nX3Xid+pCuCT0rmxgmHxaoiL8QFW/FmGT22ZXyD85QrkH0igimRtnMGh1+ABv
Qj3M8Ok/vD4ob1NcClK50h+CQJoxnRrsLPHF51C7O5vabXL43SE8t6e6E8crQGmjYtFZ0TtP5ShO
uhOikytpjUuow6o2upzVHBYjm1tzh9I1aQo+yaku8obCA1TWMe2CgWhXdeAGs1lT4iwy4e8rJ/s6
yJP4JItaAMmfN1rihLS8adRvzjwA+Ni/zpRfB5p/SQNlLhIBIv841x05tyjQZRFTzHty9N1kLySc
4sDOwexQ4IIa9B3dpRo6hHXWBPVvTwNiUQ+aDB4zsfElKbGSYvA7R80QZWq5yrQ4cpiu0BqVRBde
P0Y8+1ShLKR5xqzzIDU3sT06IV5WcShlMLGsjm7p24vcl594ZbFlXry+D5Cb8vILFrKlhJezXKd5
6ZBm2jms9ygZHCrJMIe7GtTbuuG6I1N/HvBnveqiE5L0gZtYsKwU/LIrh1F/UUyMF+FXEVhJ9Ty2
kgcJwtk+nLWC4Z/CVhD58sJTzU6XGCJmul5SgJyyecFksqIl+1xmb+dXKNlbQZHCBTwM5gQOrt8u
TCo5u2HNob92uJ2xMNeQVrj4MYrrZRFcOsPArceEBEfPM61deSAG+NlbKE7nGjcH/j2denmEGz3b
ZR7FToG8jNtt+cMn5rTqscRi1NbuwDb/zNdNTpMpt+flmlrnNFtIMKJwgsHfQpC09K5NZSWN0Dib
cR44b1Wr4VubF4T4fNbTHSTBiypJvF0OmNOsdzXhPj0tYwrfOFsajNA0k1oKNh/1QPWdx7HTDVEf
2ACtX+HA3P8PBbTdAZh2vgKTbGsZzvKrysgZmwUdaTmlg4bGKODol5eJnUo9IabW1BGw2V5mjVec
JFIe7BAssaWO4XENA2VDuqo9IUmAacs3nAj8+5a9WEJa7+FpdPIMKdsVQP6ynyCjK/SuGxrhO0h/
pGrtQBsjOPvIOJwrGj/SEogeAN/PgTA2BYVYr15dfV8yExAlLLhhKqYemsBvHh5uodlFf4f84RU5
e09eJtTDvQHXeAMYOEwd5Zo/M+TdLonHzvp8yStr12u+r2SkZJ71Lx7VM5ZpDmZ5nnEFV0RuNxK1
BELIW+e1A56M4t7L0ihP/9lAuE9ejdrISSwZlsvqskfabfYD8F2jKOaeUccQqwjEF/SxzOSs2w06
UV/HI0yTm9EaZvx8CJRn54pjpOujaAN7unBEOX9a6YZRtkzAHSOx+D3NoX1ykcfzwu3I7ESGpvhR
Ylf66fllhJ7miyFJEKMRHCdSeZYtaZb8q0I/mDrGmiBNVBIiIS/YtaelETt3MJ4B3hJNzj5XQ0VA
wb7qFoQAfYRoM/7Pb62EWkitpEHj2OsITMkDWsrMIsNfv19nVLfkuXcFyWAwFF0UU2426hPywLUo
sHGd18y9cb+BHweNnEzWsql9I2md3/aGaQAVP+2qoZ7QccWp6YjS04pYvwuXOsxCVkWCvDT65fly
eRxKaYjqhK9agWy/TNjjz1ubYJdZOKEwoOKRJ0RbDASmk1buEW8Fo2UG2syN6JU90bcDGgBB+Hz9
boeV0E+0Ar6b8dmlvEbFm0/eCggz9nQ+fl+NDfWaMHESrXnbPHdw3kLVlAvP0zLriw0lTNq1C6vk
IJcNq1vRMNbOjxUI0m5uyIoDIgjNMzSMikfRtBMwOEgxUgBgDyVwsbhbnC8CoqQ2IfWy+puEEZ77
0iC8M0Els5XhLlSXIa8PPvm+uUdeQ8Ypp8Ic/aPmkbJ0sFQJ+9B15hpA3v2xS/Pm3HIBEAGOMLNh
2/O8fe8oO71s6lH7DmZ4sPGC57i7mEVf5qybN7+BlDqSa9uuA9Dwbib4rdNx6Np+e27TVkwX1IOb
tdA9zJunzraHV6gcKijuzTBNGkxszf5gElQqlEIuKZixDs1NdaGAGEk9zNozEozD+PcHieyUSPZR
1dSSug60OgVrmqx6kYxTIIfbibkpv8h+CBJUIHEDsDyMdDn44F2KNu16vdRbEQcGylPZialSyGxq
gS3ay6QRxmSppFJwzHFCDajlmkqSH7H+2SAflHGrPSk4TVcsWudGZC377QVfSQCL1CO5tcp/fy2b
IRig4/Um9Mv4tr6lvUhuLzC0genvE2P0qvM32zw71P9ryEF8jaz/j8PhWFkC/RhiqKMjrlC1a6Ry
3Z5uzJasNqvfyDH2iz7vDF3mlsqfOBlZVfXowjw71P5W0X5bsNtZEXJjwrSionbeGOYZIaqvVuyH
4AKalQCZ+CWxKcQI2nvTyDeCND2gj//jk2nco0/wGje1az+RTJX76pJFYq1BsFDdYgcepzN2et8K
kmZvqpANLRvCs+CBc9cZEyjdLmJHX48OyRtwrti3Ydc4aG+xS44HfxbP5rLjzYEBa/DtL50QjMO5
eCJneiUxp5pIiHnjVYQMAr1DEIN8ZFL+VMJfV9bEMresj2fcPgWo1d+jYgfhAqx0vV/lu6glBJrw
NhvgPGHsM/4dJLdBTq92QdpEsbUXFX2PwCmbmWxmvnviShKTEXIOSO6jpTWCBZGUYw21rldJbQkG
t20G0YpHW3SaSCvt+PPOkjS9wjwygnmqF02mdtZ4PORfYSldda0AyPugL5kQtb+J/YyyuSApxziz
2ZMqKPz9MYdRzlTX/oGxdqrMbMZ+FmQtWK+Qa1FjW+6oceSJ4Ce+4lT2QHzW43EX4Zlt7Ci+65zH
+og5mmQ7bXHFpYaszXD9ZhOxKX3Q5LzyMG5IqVv2PIwj8nqVVFWGUed+cH5+MTyPCWJKBzXuNvz6
9/0Xko8K6mKiu4TmQksT+DSQkgh2bln9xTj7T5gO+CF+isJqMXfYMTcv7sfvuQ9uMuf6TvqFDbJp
OjBzq7RlT9r/eTjthOLSJ7I2CTewQIr7PcbVw6lsGRhehhdwK/mDDufJGQLhPqzxk4SCAAUD90oH
ClFXC7U2DPcUfsHNAD0IOw9TP9D+6Dp9/xvVAwcUwHWiZn8FjL5thsbk4n88ahLeszhY5bfwJfWD
D7+8PvKZI5sOC22iViwV1qR2iX9urekEVEEr77MCKodIGfDc/oGEVPONnx+ELk5CuwyH+g7MCjJ8
RjS/VgLUbn+99qi47LhV081yDoIVSzrjokKLhmigdG6pUq/C6qW1itRTcQIzepwKMw6NyowzATZQ
PLcXdPh8qeBiKK+/hBeyOcWsJUp/l0AZkWqXPp0JK21rackt66XUwlOJup/xH61TYxBE66+C9nJW
LZ9oGjGNzYBLVw3N9iDiDTorReyAfpo/hdoXDfRJ3UMQwCD1wO2BjwaspqLUHDI7rDBYVBNy8wqI
cohtinDyl1aQOq1W0IORpj3nLMckgpVX+QcuZiGkamew6AWpbyfXS55g6HhtdRM+fgEv2SS0whMB
/ZnOjOXtG8QHgrKS5ElzQG28XjjRxwG0g5+uVfjXmmYjFmgfDJlqbm0Hu1+qz/g2tyYrPc02REkg
8j+PkaV90YZfXI5f9Cs4RzSWTIP/GHPShxLb0qbkQQINRP3YvvIy831wCoBI+8DDWcRAq19FTyjx
admCfv93yKtzhtxpq+hgQDpJ6uWFMeqnVknO+9dB4yPb06jskPkFZHAuEzotypebGI7nbuIVDEX2
deatmojA7ohZ75wGy2xxvK+3ws5/l098UxXWR9xZUorFYyn++QxxfGj6Ll3mMEF+kKC6iUoF1QLH
WJRLzxh3IdQtOuLESprUCiGkXJ3mhvfb0FDThtTeiNWXRklIxBkSFApBQUQQ3GxwXTfa+btBeURG
ydp7O86qm7Mqy3DFRV+se31khpMprE5q9uT9DUY4aNvrsX4RpkBPjPBol/xU/CP61ZhBiE6ValmL
gCPL/0Wzvc7EBxeO7h89O165ewixxWYxbDx8gcsUrtimx6SyYOSkto1RFU46k6+A9k49fmQRWy7H
iIsgGa1rR4IvcByN+WHhw2csKEGgyDVmTyCZx86ujhJtYwKAgClWsbWXtvtWxV8rE1mlWZ/uKUkO
nX0eGEN5wzQKYgFdrF2FgVWixHPr0I7ncPNNayQjUkjaHgFpHLcBtwh6c1UgU6iZMxcYIDiR8a2y
5w6z3uILKKc5h4y8cyQ9ju8G47zed7aGz3Vg5lKkvf7Gn1+CoXX0NqLq3PaE52swL1nxVvydtitE
kyJnFsY+hXUt6AdNJyJIrgNEu9q4/lgf9BFfW/djPzjE43qna2Pgzn+EmZ7weQ37H0mbOvt6s7Xb
rd0v8HV733CzihHQ3PeQ3yoII7vewjAIcK+9TeuauG9mdfTrElXzK6ZUx02H+Pq5BLI3cGJC8M0O
G027SQrVsBM3Mo4Z1hxNqje+SvggDMTzj9+/31Q3QLOMrzeZpQtxImV8IRsArqaY2MqSJtnjh7MC
6iwwfDi9Fc8ykfmCaZYqJRf206CI0uw0/URMp9/qKV4LFnk+C8cwGpMw15ZAC9Ze0ujm6/Q0f+Li
FIcrCN38JaIrFpkEmPKflQIucJ9A8jBkzW/+0FgmACJx1oqfLRjyw6kEQOTxpi42FZx8PxlbZvJK
hTSHyMOJ6NHRx7KXYXDbuq1CkB+le3JdtuFgXSxEW1OziDHW1bOmBFR3ySeVGlnwjejKSQ4qq+Fl
xiZAH2TD7rZ+Olfn94yvrzwk8WmJaQIz7DKzzgxEw3vhUDlxlCT4uyyWqshlb2SQ77HJmMu8URvB
k9jQsRjztV1rduP9k742dAFCQ6tZIRheOaJrINR537HKPsepkShDfpzuYovBNFQ5QdGkPqVvUJzZ
2S8/SfOfM1Ftf2gDK7X2tIqB+d2rlbKDvLu9KnDbNdy1NAPBbXv90g9eVeLHbd1Jdpd6BndRkLJJ
LmJabZShdiZgHmVUhIFGIRAIPbhZigRLcNcMnox/CkqAzqPHgdk9Pn3q9ek/onzdV06zUtu4G8fT
jUWf9ZHcHyZGCvcxLaO2re2f1LT4D6yjPXzFIgsIB86kkRZ2pfGCjMh7fxCD06lMfcqHZd2A9c9s
5Jf24j43EEwV+BrHFY8iUjuRPsnFdymSi8qaJjTISXnVkLFqS0qjknJSkXk6L3qfTbyqZyb0ytfw
4gLAYK6DAwri/mDS8IuTIsO/alE9aw1UrGpTfEOw/ho6wAOTlpjAbegLNiLmWdhqpaxkr/kQi4U+
N5dt2oBx2coggFAVwVnFvcdrQELsU6wdY5hYLMvIFV7NELozcaxSbIJS1QO7b33BtoIZRX87i0n3
O1/74QzUF1oVeN4JmUZl+d1Fb5TxDnABrgsFROGL1M9QcU0i2bhrEwki7psJPeSTOtSnaIsd5fdn
IAGzZQ0fnW8G1XTaJpu1I2kPkPZh08Dn2znXG6Xmjl5Q+4a9SZBzRWuLfkmA4lODAmWYwcwZw+ui
Cu49oqYUhBouWzYW5Hl0ShscWgoKj4cqEDqyuIwSvZbxLYWRm9MUYvcz5LS3xWCOACGnOZ2MF3eF
qXemHq3hVe1niJ2A6pBCsFTXTwu36uTw16h6T2CAHOEG3PDmQinAI9880xZVsYamydhq7kdCDk8r
Jv/pKsW0hQ0a2VUl0hv9MW1fmtc9dXE8H3TDHqcjZdYRJDEHLxFlDGuJXJGbTLh5wAEbSgqkhVO6
ExVETF7BnLwUNN2bHp/w7Jm1CMUM/aUarDXtA10xybSSTVV4BQO/1zhBYNTC9enN6jGsSAFLIbv7
Oc5Nxh6L9WTe3E5/3JH1ffKyGwtNB3cU7WTiMfWTCL2l5RPmJ6NBcxX003fFGanV8LImEdlr+C/t
+B/OcK2cycXYemVKL2UHYuh+YUkojEWcqdk6jzEk622zsn6k0kC5V0dsubfZgs951W/PrAgktwlR
5XEZgi9pxWBvNFS/M1g4jkokDvdXghyqDlhJw2itO0eLVZwr+6GE+KCD/I3zTExp+Wi1ORKzN/pD
6DVPYDDQ3VSAM46I2UAg7wSh5fffu5fe67KYlWy/QiuFPu0VxNwO9VA1PhSTcirxrdwBopPm7rmz
kD/qL/iGObohutGfVh1ik239CqgYLX/bmekF/Dxz3nTLzpYe0h9Ta9u1XuP/tPPcmOQHvz9Gv9EP
dR546oHt7Qgr4zL/VyRn0J+5ppyZz8+2WL4iEp2EKkM048Hn++xTD5UGTAzTtxpPr0QOdeetC3ur
V4vNhCes90Eoa/RKXwg1RbTAWm08c8rOdj6HGzi+WAdoSqunS2Sb1zsNoGP/aIdPVPujnM1wcCbD
qKFrmEgj8hRoHDmZBnWInCq0mwUEWjMfhdAwDzXAPXpmzmXtSLYPEoNItCHH38m1Xr3HDEZFxTVc
YLk5e4CGiirw3BmWApMPjGju0sXg5yftFNt+NtdAtYbpJbRdqaezPxY4FqFV4nhfyFv4TV1fToPD
WAtL6AJibqbp3SQQV640x1iJbwDtWbbtNdIBtjfNyw2EgjEZCHUsIZ5R7h3Xj8SsEe6hmzOL3ySy
RkGWrmeh+UjdhjRvhImCV7rmBGqSQYIcHgAvySxkSUJtU0lVCZPBljDCaC4LkOlxC8UWTcEzOPQ1
vRx0BtSdj1ySv+3ZIQPZKErgLdPlDj7MxJCXOCtD7LQZb5Cj+A/vEu9b0ZESBObAN4u/m7B+k13k
3dB1i4Wl1+51XsbfLvOZwBcfFwrfslX4ZtQd2CTZ0CQS2vYW32OzOCEArzKGn3b36hLOumsfOTRE
XSeZhf6nIOs8t8v0XHaL/WhzYymCaulVLzgpgyBw9qyEwgAYeZuSa154wqAbEqPGnXP5HgNBDMD6
iJOTJ3jyAtJxgJNOhPmPEEyP7y/c5Prh1JA4ctU7EGTKMmfTpU0eASd7IiHbnqr9FrcCFBuUt2iz
lIN0067NwUwz7exHw5HHRYWlsreY0/quMtkIwUD++jhFdsM4fB4d1Pf+YC02ftuwIaf4k88fQcTT
6plVXszZfYM3zKVprhJcrxj/3Srwuj0SgXZjyZ8bm01CG07UMHrE8+d6TAJFO+lEC1ZxXFxvZ2Rt
kbLtTKvgJ/R/8U6ehV8ZiV/sO7u3mRHzWNok4V4xosXVYKrkS9H9jpeBurfyMSVfeHb7C2nXaP26
TvW01owGdgPayHJBdySPjVDPN8HLwY8UOY2QL6OR8eM8sRCcdMCPpO1JobLtWB+2xJY/3m3PdCn+
sumvJcjVnZn4qddk4zhRSva6uvREw394zRJfVJR90ThQz2A8q8eTvs8V9WN7CHSg81QbtQmyqSQC
ZSiiGANeGqSiPPr+0Po/0Gz5CgPd8C8Anu0D7yTBKKG5dlFG/4smE4Kphhzg3Qyf8CqTl/qJW0r+
qtkE+TfsYmPvUqCgv6/Z0SXcjVHRK08QvdAYoBF1vijaH1hFUXxBwNLZ8WkK6Ogru1NdwhjBkkNf
jfg5nBY0/kflOCWlwOJQj4oPO349u3jYBrw3JYrO1MBhn8IrVqr1xF+NWZZE2/coj/AYgxTUrLXn
RCurroC9RuhrJWURAV799I6v7VEac1a0lGtEAuvoGFiLiRc9xXnAyWC9EhM6yyrXn9/PfBz7PEWK
cxY7fl7BIOZG5fJNKEn3pj+wltXcVpBmqOPsRNz2noGSQaMs7ug/omMhfrQC9DG1RJHDzrEeW2wH
MoHxjPQ0zsI5KvpC+frPapSmCIhqJttvuJUJtKcKHju30eDBQPGPwf8njTmiyh9XyIjpPSApfFev
dUaRYwF6JAlxkptxTCsRbB9wuZlnI5svC+DJrjh0H2FohnJW34lkl91WhsXHw7mcK51CnA/napMb
i4SPLGpfR0UAZkDd9DW2ZZjbDqigBBmlw0z/qtVhR+IH64snax7+x7kHnFOZhYNTCXvM0Lb5yewH
waVovYrHG8Qd1yYaNZiLOB9IR9Uic2uGGkvFGzuYNQbKX+s595vzjl3hDrWnkGtr4WWsQCNUghQD
UQXBhM6p5ohz94175eAXDC3X4iUeNS/t3A3fSx31cHLFGWFiWWPSpEypAUwy5IdgcBaWjqBreovn
dtvAym2hs4bL+5nf+YSfg0Wrj0xHWVIbn2WwQYXAXubUt1HGgOHms/Xa9r/J+EKoMhw/VOkZf+kN
54xfWIA2HPVfn9WwiYP7syX9CZMFr3ZU9dOmnQLVb2WlN2T7Uid347BNY5iP6qzpEvMeSh9DHRfV
CM3bU7MRncLH9hPbKNdkurKtypb5fqLXw0ekn6PG+GTFT+ASXmQiEXyT3FpB1jXVfL4hyAnd551q
tFdazxIXOP4HSEVXFkLOg4z+MQdDfgRXefrtTecFiAcpbQF4bxpiX4Jwg4bIZNYVteGUjpmf7bem
RFsBWxatBZPhfXqMrl9k5MeJ5qxBqfFESvSfAGWwD1h/RIIh89m5x2qUQvkFH8DT6Ia+4l4OGC4l
N9Pxmnw7SXkiIebXMhdseNtC1eoOGVkc09Cs7n8fzMpGXTir1q9MFSeJl1kvWJex+0tAlmB329vf
lYvI9heHfzblS+DUppCUf8I50B+rM1Gx5/q6CL/B/NAqzNx/Lgy+u3vjE0Tpn0ZnLfdCnAFmtsSz
DBWD0CgheSlyfnB45hg7LKfFusDpy7eQmNagBKhUSbdFS2QvByh5U/It+bdYumb5IJz11Iz22yPH
/e8+54GGEtrZy/hlFsoXtmaCLaBijlYGYIsJuX0sVF0ONTwiBt7AD6aTH+/UopHYYm50ac6gR06t
ej1G8cu6ZqN3U8m7XKtgO8pRLbq2alhT9ZO9zG13h8dRhCXyRQX00F9TZZclzpgIJSORP+PBKCph
wmsd2szmGg0Btq9EIaI21i0K+Lu+h3xMR7+cxa45BQIJ/bkBDCL/5SCuz8ZdwktXCu6FsIyzvtkq
52vvHum4ZFVk2zBS1M6gNPHjMa46mXR/22xukW6EvSHqm4UAL6og+aIVtLU68ltsJPwXPKeDfveV
ZyTcK7zK72koFv3vOFmquJ7c+jd5w4kVZxw7JQpGxyJRJfA4bYlvtgOteYzdVL4T9kVf8aSirO8w
92b8tQ+ZZEXvVpUhdHVviEpQf1gfz3XDNNcMnnsChtvZY7D217t1ZzNn2htyC91j4UmW34Oui0ar
+WQJd7rhJbsiLx/onm7kj0O5FS1ju35qbTvWwiRJEcv7AE7a7tW+q8FOjqxJgrEqxqD/P7Jt8aWC
t1/VkV6/nMn6PmUcpOhNOkrZmYEvaxsoHL1hhho2PdD5yAs62AQF+0BrCx2QwEn655GsEK884Wpm
Ko7bv12XEM5opBCy463fXr6bEk4CJ0CU7wazpU5GVl0CvaBELUUGgPLQi6RVfC42zqwtkQRwFNb2
2YCHBUIkOUPtNGSRYpXbBihmJgajnjGgoE/1dshxuCqEHpf/O1yrUuDoXtaIjiVMZGIy34zfA2RB
CqRHz0Jsw9Q6mJM3gk4L40hY3d959RgbTUcjWTHPimu0g0XqbaV6dxtbPsILLhFJGXActyvZvUA2
ov0WUBHcDKt+7L++zyIBtXzX3vtEI1j7ZkkjuTNyst/bH8SGjH+M68WC6G0fdcPCOVc7rN4F01jY
aUcT00tYucJ1VVDtIkFT3+3ePpSQ5ikovqa38qpJCLGxhFjEvQjtlxzqcjFWTM097WHYd3NalEQn
kjUDxawqj8NNAhPKDxlkDkADI+HOgNAGLkzh/bWTGBm1smEh0cW656Y2DWR/D8ywQtRgqRMnV7Xi
V4OUas8AbCibUgmCokTdz/MHP0hg6hSBVrTYKvslKzAp1wUyRtFubKP7M1pHHr4sclqrJd7ynZWd
5iRIKcvIEgkxyrxQ7ce1HwImhh+xxGCIj9m6boGsYviPznt4FgmzeUby9ZMJp7uTv9Zrq8CWnZz1
4rS2tIrMHYVL8qWQR6b12eiRpiSNejmZ/G+ktRXSduJ7+VsGWjIrVI3K2kriWZe7vgXiH88ckrAF
ZhCX2dGQjbusrn3s+UlP0zkjaywpguN3+rSlSdoOz2jyzEMqYurJovjgCB8wKgN20Q6lIPJ917lz
7lHkvn6u0yE9WG6lPfJOAN8+PpuRpIJsSvtdPS0CGXtgAajp5YpV1/X633LnCs/rZyZw6ZnZ7QpR
ZRaTK1BOm4tC/yJ3XVpjoMQ3u530N1U8GHFPsUFnewLF4yifMdmwsAzQcEP8K5+Dj/+KEVdEgciX
eKkTGhR3KF+gmmz1KiqVWBQcyDgpTaKvmHgePQV652lCijYBKX3EjD0GhUKVdWDhEbGWGD3KHvEv
1uEr5arEGkqOVpJJSCZzrbeKFrSPRvWbERTqWosQlN0T9nQ7N+j/8sFQrMdb3AS0tHh+Oug1Wsj5
vcgQY/0ltZVj1f0f98wcX/EyncyJq/aAzDFM7c1mB3n231PInuFTWR/U6RqNpMt33vEC0LqGKgLu
NgBEFB3/RggCJIG1ftnVIMig2u2u0KM+DTI6C2i6slPy49X4Z8wsoWqZJMNl0EMoOlo8I9azaKUm
+CZn9rj8+uTvU2BSB7IZL3pOBISMwTRjVE1SaJ8nb1hwIfiMQe692c/DpTR2K50rPJ4NLhPx/vFG
i/FgNg8UUFlpTJ1CL9O/v/FM5ybD/+ObsHNh48/KeuQRvHM2rLwRu173Gaxdt7GxNQmT8+CUxVn0
xrnBluZi4rTbWZ/uKtvj8jwmg+t+fhhkU8VKyOx3B6Xf6KU2wT3NXSyr1lcQQHsFY/ZO6npFCygw
YFN+qx+Hq8m78P93nnTKWaAwyDHRFF2D7Wvs7TtO+TyTtO+07r15RSbxDam8ovaMrnpkNEKJBdu6
cPMsZ+FD/LrKIkMU6UkAHoMg99ZUtTLQ0Ww69Hgh6MIOmEcWanw10wcwuja9Z9NonAYtG9aTHGhD
B5yvQvxFCUH1qGWIjw2suvt1eMnAHDPlUkvGvwionG+Yd3NlEBE6XcfCOr0fOD9oiQ7wpSfyRWTB
9EOpeUyVivKSI5s2wMx7JxiXCOAjInulwTKkNmlTH6xV1JhxB0l9F21dT2bwQPLb1y1GAPTEU+5r
l5rddFMs2oc1o0DsorCFk5poeY/crdnf4EAy0he8QMV37IQZ9bg5KdLt4TfPwAZsWNvS+BA7Zwwo
o70uR6SFkXbpvdP+Kyf/p8mA6Ubzj0ma+w747MHpV3lCEXRMstuzai5Npb2fL/tbn7ZtwjOtYjJl
/hwibEXn4czVl/cka4fMa510ijPQZOG89WlmCe2Kps0EWRMrKoNyE/5tT/1lCP/CmW3VYreJ+dCx
pdXVuKcN9ZcJaQeqcJuoogyzejdlgGlQ1WI2kyLfHkJVwmW1fXOwxtbx0WgUL48rZRBTdFuW4SKq
s946MGFv+BhCLbWG84QxXsMQBiBtSjNrAP2g6lUcwUNm6At5UdMvWhUsdwxcy9DknT5mHnQKG09F
zzjttglfU6xd+kl+LjFNvvr2sn0j67qLG8GgztpIKCVRkukRvHLT2bM9n0Au8O0/j2JqR+weZat2
ojUFO97CHdFVEVJ2gtclsMkVVi0wy/59SJEDEBgfHaJQ4u08fD/jj0gusVbhDW4RiQbFxe8ULBEP
u66KuhV/XPWfq/9063zBsXIRfBhHR8bi2mLCd0fpeb/0BUQZuIcFKDU+3DB7kpf898mT1AP9jsnj
raN15PrTZHti/OOwv3KLtp4G9qWjq2xmJGR43f3xNdJ1V7FcCqp9hJf9jagiVm2h3oA+vJft6MSP
HHzm3Og119/N4SK8darP0Yk5NLdaloHTIXMoVlnuNglN6Y2ocLsubMovOIcjUBhWcDp9eVp5DRa7
88nCidCZN1/1Y26cO8zePpfSV06+GpH+yAcMMqRhENmMkD5fLQEXKetmgSlDivU6m6yUvXdDmOX9
IBzrB5An221qxAdEQDStRz8YZOJIVLQkG8mHAMkDUktTMy1LCcNHy15e9Mwc8Ai/95Dvte+BEBHc
O0BV4gGJXR+bLI2NtvU751tFJWdx7X4raJ1aRq/UYcvA64rznGSlxbYCdmzRBthOO+RF8vdxXXMe
8b0ZfLnzGOJIomPbU9fpnQClXSIq94OZAxVV/tGm1F/3wHKZUrGOegyp6RGKXfNrzXf1Xy6e7VQw
j5iqTvYH8dycX3MIB4hUy/E8933naLfJCr7HhaahzD1GFrjPP6h/qB328kinbqmU/DGwlnmfoZa1
ACTWfbAqy6JERTCTci8Oc+1ojbbYb9RvKnJ+HkfUgo50YJo4OjSvcMwxntD5/l6TEbf7cZ3xvVNQ
ySRa4yv6PslIesg4bzf/mfuwZAgAoLQrj/sx/s3t2ovQrK/HwZtZ6OaXFoKuFfop8Vx+tLZJLERM
BclclfIRDRtit/TlB5AMCRCxdLsCAVsws9TGfjPn/Y31aIu5/o8UeEsIOLsnZs4N923xMpC8naod
ayFlSwsiWyw8C1njBi6aBWK31c1uz41C26tfS61nifmch4yfZva4SEYpunSsG42GqJET72cN7NN5
VsVG8nG9lS0LQb0XE7dUssp5rQu/G8zga4nTTDAiXtWaOVom3coFCYJ8X7imTOFTKVJQdIYrQ5Mg
wjXY4XDF3ZSB6DXjSLo3+3aENT7KburjLpFFfPMhvq+g6hlD1sE/ONcow4cxQxAz5WCnv7iK/0mo
BnHDosC3HZ3pukhiR51bcyaSKuk+K2drJv0a14bMB7pl1Oo9nFVWNFq4o56WDE7gur+x6AQkZBjw
5DkxPjHLMhTUgR/C784ReF5piDvEn6/ycZCYYgq08fXhEOLH+ssH1ZskLuBLLHGWhKbdSbzqbWnz
Rx4p2pm1jOzEnF61YbCiq451hd6DD4Zb01DvvlyVJCEi4UsCK3oZd+izwRfxrSqFwNsoq5FhQ7qz
LQVNwY7XlWhbQbB4MutmnCikjfLF1d053mdg1Rb3Az24wS15BEUTu7fpDW3wxmLEoriqxbPi7/PA
NmcK0cBc5etXqzOB4s9UYtOSsctN78r0bqy4BRcKNZmLSKuQ51CFk21VVAHRH07y4p4dyZ/GRw35
fwqrnzWnRgf3Nku1Ues49sxOc3N4NSISyJm2sdSK22KWfjBZJNECx2lA8bGz6/8dgD55gWRsl3wm
3BoWsriZmw2Fs5YIE8CMZh3tC2PNURnpiwD0d0pJSFt3VqzmPg4vuJ1m0qlA9CVwO717VmXeajgO
5QG//WehjTEc4y0uGL2lQ40oZUuFTeVUfcxMxzY+7oeHKKt13mQ4Tm9nnlGHP58/e1QlKocecWA5
CNBoUy5LfVOgzO4gDRpHweTWpuHPhEKg1fy7h+CQDMrT1oWUtut2097pKk2FWdJ+JtC7ZZ5UcIVq
eulGp/14hG9Tqv7SfL+siCq+ejgAtM6/v0fcY6YAFCRPVZlGdLn+jnbwREECNDmg9rRjvfWxqXlQ
1/cao7Io6U6Qxt7+YnD/0sscf2wH/wsSeeGTwBsAYKgrCbN4mJ+Qenp4O9eeR/vWDTmc68Jg0jyr
iQZO/7b42iCEu9uXXISL50GSY41dpA1fdINoL/4K6TenL2E+hiY7PG+iWCE9eeb0p3/xcyctS5mG
zSoDOl6Upo9/tYjPYO4sGuxnhs0jxw777x1+TEYqeK+nQTGXqDetgXrc6oUaqUNLnK3rqbjXl30V
nGkw8/VhouyyZFehM+eiXeBWM7yv4XIsWv2ceQ18JWEku8Mtuqd5k6Wvdj49r7n7GZCpFVGJxBSt
WG3/Hv7Ud3K4nNiG2pPohEXl2R8ilUbUojAWu8z48jwdUWumlUAYlQ8JItpOmUGie5V48Io1ASRy
sxUSOAZEsras+VSpmaYa0V5Gx+QC3cOJEMEWj9/bhr1f99FquFbOGfhGmshjI8aILO1uqUTXNTV2
WeMPoF1GyCTfd8/pc+RZbJRxx9ZdWlQ7TnNvZIbJn0jrQKHXZXtR8z+rbMgM+GlMDnX3GtoYePK6
Z9CXhI3c4o7qb9wIHXq53rxhXjiCplJ/LC3bK1BgImKqTUyD1IZJrMjrd16cG2+o1r7BD+gzcxn1
qRbENxXBL7J2TSap7BmaNYbAbQzB0ovzIN95FaxHMS93Uj38qqKeFV2yHpyWj49d6XxdIuK9Cu/J
GeZt99280RcxIi+uMAPyxq62cl42t2Uxyie30FpAZxGe5c3YJOHtHF3GHlBfTPKC68vJ2MMfHymc
oYBhR1V+vjcSjRKZR+Mwo7xo2kdJIaAtIPTLR18ZOU1EspTQa/d/rrwxV74OVuh1rdW/4QCjujxW
Tk20nA1JQCYzzrID88vXbkx92C5c33Fibz8zphjYSXF6A1VXVJ9q657O3eyY+cfIu4fHsEuN5nOW
vN4xsPR3BaZzmgcNgvoO3LFUs0Nc0lDDiunhoJSki9mf+SalUuW2TRYM/y/cGGRU5aj+jJlfwS5S
kALo1uWPO0GyYZ1nIO0GxhbBaqpEf0t9gsjTnhrgzwhR1Vw34G6RW71E9qpy7CYh4AcrbdjGQ89r
i1wNd7KQtmpMMKrZKHydAIHhFIumTliq3w29/pamUgxICrII87oFOO8BA9O3PSca7lBJ1fm/I3S6
FX05nSLNGIqfC3VZvGoK0hUNKKT0gcm0MLhumeSmxemnuBbm2Q6giYIUPgU1DM6J5h/fWilTtoD7
6sacrreoPZqb88uFHVrmrW1vx/BbGU/t2hoYGJnP7Z3dBt0naMpOahrZL8MzUdMBjc4FWSknNAwZ
0fHG9oVNAE8jaSEjrdKmCZTjGSMwN4ieGxFnqTxtv7WFtjRNMQrA0e3uWrGoBKfN0PmbjobLrTdq
jXR2JTF4yNbPBt4hf68Ypf95e3XgkF7+zWBIdRUJVjEAnMB/tkqt2AzXk7ZPY17hb5llw33z5/3W
x8QSmkE7OYDGM7BV0ic/cAEfzLdsHL6KIOr/KkHWHpp8HR+utz6myJutkhsGjvvMlDRuoUf7A6bM
QJ6xlwUU+h+AIAAoYPvr0KfNWYFDNznPIXRCwDDZl7Ey7h3VLnUx3ow0kEBPaZ0fNx355E0aJQ/9
t3DoXjD0uASGrsuK1uu/L+RN1nbc9ArhNsn01nGhRQJGHwewQLFJxsPnqRV5bkVW+KtMD7o9wq5v
1v7B3qCrc7OmNtx5rwye2elRkyldQVwL8B8tRdIqcd4p0LH7/GAC1dC6NG5j2gqjFdi3OU3pPMJf
/gTMz/TEN0UW1zMM3hu1nh9kJ+BE0olMincO9GbLu1mDuiRXTyWxXbhozyM7YvN6SzYhgdA/ynUQ
PoGsnMPsd39/WtEVzzF0UZ8H8CtFtxT8YddhnzBP00e4+J6l5qMhTFMZ05Vwl6jnIZgwRRguKZmW
NN9JUc60W+0G4MC6HZJMZg5fsDo5riKMAwBbmCA8XQDjeTi7aZ7sT7qrSHVpyuKy8JGO4MWHbJQK
R+Uz/aQ6sCsOALuprHTcB0TMuv359TXlVf1EzINO5KCKlxGqJq/t8gIor/JNcemfGhlnlFTQygnE
LZKDX7xmu/KGTV8/G9DzycTpWrN/dtQuxrPn2BXTeRh4cBYyix7rpFmQ+0jMuAoG8s+K1Q2nhY8I
2V0rTcjPxWC1I8vCcHSy8d3OuPeESmwPWkhKabF07HTy0mDLg9CGk6F54x0U4wxfnF7LGGVFPIXV
J8q7eb3otwsM6zG9ppp3g5NbpGfeeY6nfJJ0Vw+pgK6/iB2Nrgl5a0q8YASSlLfURyd/q5pzeJHC
OO5J20LT1xX2Jt0ifm05aS5pIHLessBNEFyN2JTDvSIIcofDin/SQOjSbw8ismJiJYOc2Jks86RN
AHNWrJ/eXfM7mm/f6/E0IdKNigIAtNb/WecXL13BTBXwctST2bXZltMDxD1Mq8JbZ+6Z1Eq6U+Ks
W2bM+S6QlAHC4389rpxYpi8tqVED2vnoyTSqDKl6mcnFao2LWj8hj3TR/jsjQBJ1eyHrU/cWODjM
gld4qDgfxmmFUFpqU1/mRMvR3QUg1ABV0PR92TCMBtipAVJYbJjtUKI7uBPQxfUmIblubsXntC1+
9kQ7FyXTmjzKN9oCe5wKLaw5LfKG03yqCo74cxa9O5rTjqgM8i40v91PrN2/kjvNAu3EB+M6+mTO
VV6428sgSxOhhJ3W0Vs+XUJnTgvapLye0nHVxpdqABFEYa8cTZuWiJQj70b6xQpCLMVnhEvDbB3D
aPvIflgpVh4/3LlWxdBENOAVClpZLnrGgHjneBfKG2eK9sp28zRpI2L/lUu2Ikvg9SDjwUo1FUw8
YmV4YrGYCPxv7F5TkvS7dqO3M1ZJvynX3CCEJYZb8Jc0OjiM/NreGBL4613/jTrV7bcq28wYDqYm
lee5PHZOAHwIG3KQ8FlKYt6X+P9Rw5O3CAoz+1IWhhYx7+Do43ha9Gw7hMI95INpcTWtH+37aJfc
/vK33vsBJqoEyD8ToSmws9umvNUJV7KNCczarW0ET0R6yKpDXEf9fbwGma3carnMZaPdcChT4XPU
Nl65xosGw7JBnsF29bgx/psNBe/MohmqSl5BySlNvZCrlLouMnLCpdPzIf4Q9F9QK7HeUUclmdtV
O/Yb2MaqpPtAru4EJIYzVYDksa+CR/AWuKI6bWUXu5rE/8mlJ/AnpwEy0MIFI8jDEgUi7I6zverv
QWPVGEXExd0CJHNVw7W/dKf2r2X+7LXH0WATDAUXu3d//TiOq1RMJ3xWVReQeL9eWYFyVykoifiB
ISxOLpbdFM7+Cfca8fHu7llmXXSrXfujVBsKuUkORXGB0aRf2LEptRWhYwB2WBhFSP6oWJuPpXOY
VCPJRoJ+sQ64kQaPa06OzshKh0K47bIBNjt4CSFl2ftI5DqanncnRvhPI5XJ9XrYMGUUlhRJnPX1
d/NjZ7/vVcbLggrncbmM22KDt6tHVx9l4gOP2/m74NMGHCj5A6AhDDKbf21vkUKP8auVOgnctooR
gQHpHwl4OFOSB/A9XvigqWIYUn7c6fsB8oo7R/uA94vkKAAWURHgftoG/R7bHpE2L50eLu2hprOw
lMpn7d7nXNVY+hrQNut1747JqkV4pLwUtEJwLEC6tmc+wlDpD91gxLeLHN2nCg7dGIwinjai8X+m
6HX51Yj5kaYAEIGSATJ+offZphqWqcYXmsBb3cODxmBGCl0TMUZxovZqZrUKW2mrvB01w0AVwo0W
kHC2UyV62JfBPDA8ugNqcIt0Rz4aFKCsGyKJtBu5/I5wK+tmiit19VBzoO5k3PGzmJ3nB0gSrkFs
U32rmlIqcCpONp5YSKVZbP7zTKl2y0Cwra52x/JmRMv5CUZKf9Cg+9qYyUIXSWhjVRdLh3kDld7x
rz3Lxltj9FVNC/DNldSBYrovaaz07+uxnU4N/bYr6jIt6PWdRpfEFKFwE1DQ19wx8ba5pF9NvwWp
IM+/iFNGAZV8jYr7MHBqXGFi/1/OU4KVYEkFZ0p/Ga3RqZyHdI87Jt0DtcgIghtfId595E1bPlmU
++fGF78fJqlgY72Lnxuylb27iFOZhL/iYQaUG4LzBuPhutyazD5o2wTiSCO5AOjR7ofyBpgnTs2f
0BlrGFXGbF5sJG157E7zMAawy6W2K+G6BNAmn1iGERCzgHNa+wjYEqbwzUkOM41b+HxuqrOViZtq
lZqnk/rjdDE8Qe5Z2R3JsulcPLSoK/Hj+n9+Wu4UXEfpyiKoosFCcn3JYmqFbes4eG8CaAnlXxxU
Est3MqtKRTNICKQQykkicKUHvTzm7X5+PI7d9n1r066z1n6VxkE1/UFQwQKtPP+zatL16OyeSwTD
ol1cVLB970VTSiQHSSCAHikhJ9QCcQmYdHz5Akf3SelIR89T/OsJ7wzcUpkomPzcVNlCOWHNnIUE
bjXnY6z3GE0Fcvp/S55t6IYbITMotr0HKzgfi7cAxyAXuuMLIVyvrYq98kVywFL2UfOS1dIibniz
jaPbWezr5aV2nfAIX/DM32Z3XR3gO5Oii1IX9Znb0M3RhU8b+ZVmPfSU8kiisS5Iyb9PAwcRLd/s
3l4DqieJWvCGiU4JVFLJrzsj79/XOpZiehRe8Gvh9yT9JaY0S4CVHhCQuVcaw3Ii54UYZTubnHjC
mo/ZfrV2BPpsEKKljLtZIK1Cw5bBGyLUi7ypGYCQRgwxLiEs7RA9eqEfOPMuAPH44Hcz3BOShT6F
GdAo+x3tb+bFOXkFgvpmGr8sguQSgdA/A/1bh9baH7N5Gj969D7ymUlmtYgN18SXAKg7XOsJwBze
LHUjYwvFMla3dhPzS8HAThKIhwubtBc8COQOuGELzrwSWihariHU/z+JAJQFM4ebtzZ+Gf7U1cjt
iJduC4GA/PVLqzTgEMMRcaIQTVXOQpGFH+sLZD73lq1Q//vZMftUcqqjoE1ImLOcCVE9OvzCMK/H
0tc1zVgorISssXVL6UyZJbRdrjUfk5v7Pu4PdLyBNYrFqFTQuaOnbJjZdShCwGF4gHV73ujIvx8w
Jv1YY4z/M1g43TERZf6J5AcuD+4GA4tKXgztsDci4XNXc/Hzo4/WS03XRHYpy/fW5bpqqFQZpDZW
1r0wgtanNrOKzQ4+xadJYuDHGrClPk1cRQMzaVS7Vk9fTlPErzzk7Ck92loejsBtKDkip9vIZ49q
27P0Ep0+vIs3ID6RmJomLbUdr+OLaicdcJQSdud/xIiZt1a/drZXh46JgSAc34fSSqeJrmr5Q4K0
2pv4hottYc2ToUMkepX6511ccLyONcHCXjQyKCiuu+NKwetRhTFZpUS6CSReTInMs/B1g1zC6atA
jrz3Itm/m0UgChYuJ7CJ+LaITAuYsHHHp6CsiPrOe31AN1sTBnevglgrriLWRgJ4OOd3Wn/Qr9Ex
GTO6tdjEvVDgGXZ27zF7JGuXPdh2SNvXXtbhPnMWmS+lA9Lrxl2Zm1aC8GjznMM+Ti/mPpNEUzoE
s6tRJ7mJwyp1wKtiVXrsOTkLH6eSav0wOO3aTfgjhFjAfLotO/t7cBQhUoqfvXFJrQ+8dwbXB7N4
rCzfnpwFbL8FaXYaR+3H3PJuZzIm6sOuPUwI5wEpmX2sCHJ7jEz94PDvS4VZvg732SYLvFjV2LNM
rQOKSkV/kPRz+WsmB7DFK1ivyRpUI2Hc4uPNVXpMKk6zD/VCxGNS75yIcSXmkG0VNRnlu+wAaOBL
ZHNZltj7UUCj6VNJRgc3j6ndSbD40mFbvD9Mu5pNzStwcMBzFhKQzNHmtE4W/fBH1jJTjUwK0aSw
Dad/D7PXdDDedPloVn7bAKFRgmC6S/FBcbCVUEW+x49wVGxoaNlS7xji/oAL2SYDO5EV58PDo64b
LkvJQ4XrlAG552z2fLATrUWKUPhHlKV9pwhmVf/bUq7CUQVC3J3zQzo7WlftdWg/McLzPF331R8F
j9U8AlYZUGGy/0QIvRGbALt3PbXiO1yXLhmRyjj6BRa54xECmS2BXANMYESse57okYHd3PqzBOeB
Q5d9EfX5NdtngbZSG+jgbjRetjiZwFxheH4sgCY87aXv+OiYI+WxaJFEPdnuTFHosWInCDRkO4Si
JzfnNTuB4osbRahw5Ktv0RTod/++NNSWuK2w9MpqyaOUHpUD/evSMfz79iph+GveCAVRdAB/LNIJ
Xi8utFotseWeJaOKxxTrLndr1YDnCZ5bBY8GUzzTK4F7SpnAQAlBEsg1WsoN7TL6Foo8OQDBi7ID
9a+mkr4HWeb+s1AyJR2hqd8tpn5oE+c+kJd4HePPxEK7+lF9igF7+W5C6I+joyrIzGv38y4vLXrC
fQXXKvBYyqqM3XuyL8g0JZ6fbRk1MilvZfHAqHFuZdBr65qny0ixmj2jzKkCy72cdqRHsuTOa44p
RjzMyT3xkk6akovpPBU/iCQz8RBRoh7bL2YISc/htUHWiR159+FXJEgk/Rll3LGSZJnq8S9a6eW7
Uk5M2YvEIgockZ1QHoEMlxIRMrF7QoDDMTmSc2mVDmNMpqcs+Hd7B3bJAe6hrdME8b5IbDJmNU2c
VO+w//BZs0p20sTG2KseJsojr8iNCZijfwwJwd+lsNAoyebsl9lZUDhxY98ADQPYbgpuDURchbhn
U+td1AX7d53naAtnLz1PN6JuxA/DMCnl6ykJCDc1tJ+6kJ47L9Mh+iz9G2c7FBQgELWfQ4Kfo6WU
fRU4oA42874Pg19U1x8QY2rufZxvl1NYjb0maLOox4Kh/3RNmgeor1mb0yYSpZ8S9G7EgrqwUeZl
p6BGp6ic+ufLRio+mFJVHPqRChn7fzEGZFgiTBbHigJ9WfelJ8eL//SI9CNzRF5FWiuu99gfSY55
xFbjhY3WLcEwqJHCZiDzOi9k5EVWrLtFb0u6KYEiR7LySKQBd8iqHRO6cVuIsSC+HipYQfO8nrfp
mO77NdPC2+xH60FEKYK1v+czLjR1nfdrdwiw/vPn8Md3oe4OizSbZP8DpDuqa2qYeBzzL7hUOyQY
Iq4iTdYpSvrSu3JgaHRIjZjHFRNPP0xrRhleyZ2iyGFpuqxnHpm9AFzVUWSCYZiMLZyV1NjQQpGc
O6rU8HESUV8WGoz/kC1OGmSN9jSAS3XIuQuNpTfFx/1tCENHYzoLOwqYVvUKMal1na9LIRL8AA55
/IpBNgjw0ATtigEt0cwyH6C14dHlWuoA/T7UKtDRGQNnED87c1Dr1EaUj18z6VNU56hz32aN64bc
g3Z3wusFwMYzLUujNVjJPxn0Focdtokozc6OC7wpric4INAiO/sDO1uQybG4EhwgFPf/Styo4jqi
uhfePn80YOTD3uDWUfkP1ivgK6Q/Qij2U1rJ7LQnzGO2QJgSLEzqV0Rx5wyahC7Zsvp5WZPJPkh5
29qZuetYDh3Xubi0+puyL3IVaBJ0X/Lxm3yUm2x/RjnuF+txXZxOq7lM6SNA4nz0WY9vPEBDIZgO
6iAObAubvyqkLhVE1b7LZV4CaS3hB9RfWO7EQbltlMN7tf0ohQE4nj4U2dJow6Y2Td3crvsTyz/q
yvcbU6c0LJa9BrRDDi9z4GO43BAzPmAoI/uxVi6y/U19jedyr2cwV+brZ5PsPQspFG1H1DU5ojQu
Ss/FB+0y1ha9gxZeCKq68g16gXOhzF27gt4RejB8McMhZJBTe3J7iG9Zuxu71GXmMSO4ylNWU01z
MLtMiBeI6XRnVEIqIiajI2OeyY2V4zLduMZYs4arOpB3V+Q2Sppx18V6InPNKe28G3J2D2RtRKt+
HgeGSVawFpnAawFBt+lJU0J7rPMOwCFDg54oQ/HKI92YJa9kd/REHXAzEDG17Sp7xSBEyNuHrqZs
UAJDwLLot6IYheB3YEWc27jIKl+mtKHQMJcY7FponCDQiaShvRZmaa6pB3iwh4nEnFHLw/WE6GuX
hbKrKqSWxqlFPisSB5nU9SkSVVbdO0y10mYdrdtnr+/SwBTEZHAj06JoK7E6sxb3+Irc888h6SjF
GHRTRNirfmG3rioGSy+AKXrEDby9YI2+1vf292uhGeO0iJAOwr5TA6wZfIyDaiph34YVFExdXn4b
UXNYkkMnyOaR3yHetwnlm9pqsG41k8Vd4vDhwkc2vEIiV++UJIX6ssRQg+032CNRnzbskVkvyWJo
sgZ5E51eRBniVBqpm4c8nk6YAexMHsREEkgRlTb2BdqhYiBlRoTG7/fcyDpHfk9c6OsQtG0fznqg
jIkPBYCgIbOKXrZrCgqjkMC2KlKQZL9f/XTZzdbNKIUX1pgMCxHOCoOYz2sQU/0vVOBnFgmWkoHR
Di+rxiIpxNU//A2E2JnbI9xdgqTlxyNqwfmhDEWaQEVAQkTsq5OOdXP7DeHBhRNE3vziUSLY8OdL
UP/qCM5G7+tp1/oA68gXHziD28wAzSOFp5j9s0w+tPRQKqu8fuFm1Rnijo5HPq2O7dkzvPsLqkpF
eC5RceMiUvTh8ysQZEGCOcFKmzvexEGuBv/RT6kdu6U2Nc4mHo1Uo3IMH6i0m1YyVugj2/LQGr6r
/l5Abm6wAIsLeU2bED+RMupa28O6RLEbFVMmbNzc87gs2/acRedD/e2eMhuFgodR5ojJshD+Wt4n
URqgi9J9ELFTw/j24/htiXvp0hZba501OMuPME/02Z4oxM4ZEvpUU3JiRD+SCvQAcgoxcQX4vX92
+WbjmohoLNJ5Rxh614hObRpYrSV6VBnMr7iHhg2K6XBJcU3nUEj3jDuYAvz170+NHs9keZcnrY14
4ZifCoq4fV+VusmHO9+cNJF+2R1iUi+VYtNWwuE+z+ONXPizuYvbsyH3R9QObkfvYd+7jiDEMq9f
cG2vDFgCEUH73LsFFXdE8iFyRZ19wZe+cMHf1XOCv96SicuauZHjN8623FmUZisFyyOosVxUPm7c
HgY5BbusxR0b3rNwRz0KVNKKFDfPzQemuBcspDGhxxaE7HngUZLWrBvKuiTxVjkuauA0n9wgWHfK
I/V5YWwx/uMvDUDG/528sDc3YjPneY1+i5dFITSW8UAfzMN2/Jom7nw3kZUxHAfmyv788CxQ0nIF
X3o57/3SAmguRw8GbM7EKt4TMo4Le3TRUb8gsCJLAwAf/3tek0C8R/Eu/4Dyh/9OHQqet4IitzAV
18Dbqec7iSavKb8Du8NHxkVvq+PXO7QpMFqRh/FrBlii5hdRjnSMmF1SBHuzWnBMuSvQGAtmYFMP
ymi9HQPKfu7W8TZTzURExZFFLhtTWn9f6IMq6i6R9Bw2hlwSYnnbU+ScDRmi+E9CzUXGzGa/Doza
5OrKN4+ZgPp5Wp/4em4ry53zCIm7WOZX3l5zrMGV/viUBuqPiMPWkkf9QjYHAzqcJD/WUOlTtzAQ
KT2MXD1Tis41GamjD2W9S23FFLiauyaV936x/HcHXWylLnhurtS3XJ2KRlrh8TNfH3QOpq7uO6a1
Gva4cvFR9fkDzurXq9KGPDdgMOSzIArKZ3nwvaU7JPQvuUqdUw9gxWy+WrCK4ffOQM6FSKamNTOO
MWM69Z8W0y70LEM7Q/avBqYV4lYf3NmShCuSO36zd0MLuh5gsS5pN16oTx98JCOCPpwR8otkSl6o
1wwFKcT0K2uVt97ZpuqgNDQVibrYUIjLI1m+YuHxemm3GMR0/5SbKTVxwlv1vC1KbNfYZE89zx0g
Tw+fHaZYnEi7G/QH7IZ5vikhdDKWg5X9uuXSQN8V0UcSneh/AvqnLBeybV0Jk8HIR+Rk2QsGbufT
MqLOHHKn+RLxyFOXZYzLwzfykSagKHuKbBboskIRRSI4h3w44AjlN5UheQPPlqOA7ntFSBtqtP9s
YVkiTIy6oaFwzaa5w1StMVL4B7HN1JUwUUd0ihsVbLTHUZY4jDT1BZWoOGI7ObHjOUthbGrAxVPG
w9ESAlaCcVCmf9lfOUJCkm+Cr0tOgx5zdEaEig/N4rHyB9+aSVDsjDUscJ3/PL2YS/2lFISb+ky+
9+n6/vZRI/9mp093+GGOekQtUEqxsArJnheYt5Sf+G5HV6GXhvz1HjH03MVVXoazfQ9Jm9YnHz3z
kM6lq6ekR+sgIYpzhw4vm8R7RXCDj4QOJDjpRV59Ffd4RLEwZM1e7G1HDIofZLS69wKD+i9LY63c
x4gCpPYRsfLKPyEYg37UCvLA02nw56/7Drjr2h2HshlV8xliuGepfkMFI1FrxcPt2LmMmN07qKRF
/w38/aP4cudMJJUhpFkXJceZMQJDN9ZMBSlPI4l4V3pe1WzdQwdXhV5xjAcj+IqcOkh33TQeLXHQ
tKAmnX/Dj4rKKO2RHhada+B26ra9ZY7yFu5znMRvUATQzciFNkYkSMKNWr8Urr9IvVMmJKxBDOpI
rFS2CPdx4ux9A1224yj5CJ3RSzmtgUONAMCU5QvW2+CjG/vJeXatEEr+bEH9uhUq+hGDuqxn/E6a
QLF1WgMSuc3SfZ32/b3kiCQL+isP6U+E7k8RVJbVSmN32jkbTvUx0PuamB72lEQHOQJYjue5+Osc
wvGJhftuQkTlF71zHl1ZmLVexBhAjW/0p7aYEjeA537b7o8/p1mF1APTRM24jHXY+q64xScyaHqP
1wSFaPdZR2eESPKNUUFdzOMzzErfINubyZj/g51gkz9012I7wG9vosrVPGkjqT8NG3uXG9z/C765
L1lCSYZ6xMU8VQHMQVu6RJfvTFHIdBRudKN6inb7lvLUVvTOcCo9jhKTcA9Ht4xO+1pyCleQI6N8
DIL/+d5f7BxtWQK7zOQRrvqi19ZGDwJcrjZZ2U9/OwXoPe54VbrwC7fFsdme0VaXlfjCyihYgfw7
mnR5bE3hGEJ1EVNbSHn5x4m3Rd/aw5iKXC4/8n+c2Zb9rCO9/KCOxAV98abv97t2SWy7w/gMeeID
H1YjLh4veqytcKUXJhh4evQyJKsHZL2l0fKAO02hc6GCngnWVlaIUXD7LJfhm58dcpLaaTXPTx2r
xPr8XToAYXihIkKIEJd3x4P5GFycMRkukPuSbMpQjakTIDt7RuauWg3A0HYmG+XNs87M1H99aCHY
OD1OKtKnDSoKgl9pDk+ZgW/RZLs+8P0pknF+jJ0XyC+zrtefoL00Bq3A/qPrAuXdYm3f/y8la2ST
xcsFGN8FB+CGBP5RHEBAPpV4g9/N/cwItHByq/21gp+Pq0shT4+uFOOPaIaPltOoLPVjjzZk476E
kofi7O/uVgNLlqdRJIocTPTlNdkf4gUyUwhYpK87PaTNv05L1s5MbZ9hiyIp/NjmNZ/nrZylKKl3
Ku90NP7U0uX9i3SDOoG3EkbTMKpl9biHf2z6hwm1uLQ3B6dNY9EE1BNlKDHLjcYJ6lcMxg+h9K0s
7CaIIesTJtr+orMkLsrPFemIQIszCXx3DjhfkslznYUG+bSeoku3CbLl5aYk+I/X5tmoq6yXGALr
xP1JpmHeUP6W0pr+geR61VORSjxRn96pRRudBn4+PMhpwrbMwKz5a52FvZtsflHx2gQfLzXPuGIt
FKvfNcT0fbX2wzyrOHNTuLiWYF75E4Xmvp7PWJjKJ9tTnanv0sJaRieM4Esn4hOav77o79rA8XUb
0mOWSygjF8XaW7AD7EMm7QVXytk+3sHdnoZboeAYg78ZyetVpPHW7YCdQE760njL31TBx7S4/aY2
poMhGWRc8Z3WmhgiFN+n8N+fAkZClg/ac5tJJATOza/5y/j2eZrdks7YE1q31PHZ38z17cuke2sC
UkwMevQHrcsMcJ7LzaBuTC12Jc4VUjv3oNHz/Xkvs7d/QGsWCMYZSp7IvioEArDSbUaJH3xaicUf
zOt7zuE/QbyGb+qRBekSoxPpSCEnGGc6WmUx1TjRhn7luyY+vI7MJZgz79Tw+Un/ENTLIHE0LUpk
Tdx1r1sBCp/9Opg7fURI5FeTAn0rgB7X9U84n4jCUNXz5I2IVc+QZ8JUUPL9cOYYQ2vlC+/8PCGe
i9nW8M9hPlm4tXSuvlG27NptgkDPVzyM6zaAPfOCDSvpVCIVhnRDwarFo0TLC8nlt153ir6L1Quv
n/VMOCTW2TwEMOIAe7ult5KEKqqpMvNZW1lKauTUAaeDGHcKPEU0FuxdwArnld9wTr7ufqMTVm/3
igspFbutinC4H3QPvDMoZnf00XWCiLOZRj3o6eU2Sp8NVWBB5rNORnUdl0aUD9qRq+tK7WIdkYPP
i+e66M2AJYHAPLSZiki4k8MiFoP15KABhUxQG+73TuRh4LpGb3skzqT0ds9D0bSuHS0E3rKY1ehV
pt9I/afTQuQ45ZfsusF6bxVzlY1uPFiwRi8XtXlt1a87ZUifOjv4WSZAnyGwSDECO7dxhJRYo9+/
3bTD2cuhbqgBWbVX+7HKMQA8WRybEqjL06OHUejYa0jGwnOg1oDlfAG7zpeQG2v1ZmQSTLvtexSt
QqBxQToohsQ1JLTGLaP3xYmvQq24l4o9c9ITryy3u08+ALuFPwvUbTC4/XrYy86fSm/vn0RU4yIf
WtqEXpGj+u+oEXAcn2FxmlDsCUO4TOEz4cq6bemQnXMYKK6bxKJz5JOqZKt6BSKO5jV4+WNd0z0h
FPQox/+lnm6bqDQC/1JatrPcyKhcIxEAgbnPq97OC2X8LsVaIerLF57CLZInxx7i2CnTCFDDWI9O
FHQSJU83I6VrkR+T5vEtqh3oWqQK7HRXZMZBWa6OFwUil/lQ3etb5qScNdn9rSyRq2kBfnbeWUiQ
6noPf0S+EpeUpcumcjDRNkW/tFt6FmNKUX2gSfnd+P9OW0BR/4Njr94A6iBnbjOoTzwQoOWEAJyF
J7+d8fw7tcdu1Lk7jFl5ksOkPVqIuxJoT9nTr/l95Bfn/9Kkm2mFkfI1kdvXO2Yu7ubhZlAhreLs
c8UwiZ/7KABsuN7EVscQL8+8BufWfEWoPPd9yYC/+rgB7ZXMQThjuuNWnlMTeqdjEtayv17grMls
cICqL245ZEi346SgihDvVk5CAi1BBQLRsp9fENN4njWT3oXQee2ffT1Z6BUV1Xxqyl/Pt9pWY91v
Q00ejbp62f8NDD4Q18lS7xOGI7LoTSW48C2SEOxywLqZz9B28PPvx/gpEavf2Z1a14RwoBkTejxC
kIegjOeq8pqoPbgc9FJfkKglN0bR/pIylihUB5qpf+qtjRJT4zIXY6eQ0uWqLAx7tT3FeA0kgpZL
NADND+5NX8Wa0Q7R14tCMfzJZLd1wTiofrn735bIHKIXVvL33ojWsE6VtUuMFWSaLiYh2tH3Nxza
d3X/qXw3q8c+CBBPCc578g7Zw+Cs6yEFvjWH+mt9lhhAan2uqvpakv9rsovttTzjeHaq4Wyu+MzU
7V9iRh48hgNqcSCmNmHafpKEZYqf48ESXInlMfYZibtmtVOHljE8j65JDJp5RXYv70UowumKB0QI
/brKIY0shd9K+/2ckLR3MCWA3Z36NUeWC9QT2YHYmbrNmF714VPPom4DbWUbfI26iRw/uY5kZx2N
cUC2UPVVCrOfsHX4P5/w/KjHtHEpHs1DVkkYvcRLPrJbH636kDq+QxffVQj0l9HrLwKbfNXCAblR
ZCX5ZolwjESya66l0IxwRqQlJn1HzAJ0LEN7S8boBu5TAo5/QYF/fC3gsU2nlvgxW/sfONVj6/c+
0qTucvVn8KQxQT4CwzYOgJioNHG1OJb/rlIKMsd5OLfXnkI0W7jRn2GKdm/WiaS3QA+au420C74+
+4qdreDRmD5tWR7k96emniEbtwag8ol9ozzRRafLbbUWj5US2Utqu3b48JiINLcT7kYpWqhQM9Ce
+r7JuP06Ek5GwAOOig4Q+pOGJAXuwGLBPcK1UV0Mgd2j+BLYb0gUUSWCefrNyV14qzh6C7sBuAcl
8HDySOcXDDLU6Tcx6rlIHlGPHJ1Fc8FL1ZGzCJwJRA70DrlB33947sGm2BPmMjk5apAy7s89IVZY
ElIlShLyd7/gMNcpw60jcllI3wHes51V7N/8Fn4Q2XL6bU4lLaJzkeZvlxuEYz0JV5+fI8MXD4fe
a+FxIfoWbCLlpSzzCq/3k+W//rde1uGVjXdTwbOew9dx9giBXxdM8HUyXBuw3bJhnZRfF0hsrLBq
O2hKs8hkWebvaotjx1EQNfFO7sx7xfaO0NkUv0TdroF3lXom69RC5b8IKeHvEs9yC/kCiavY/XPd
ul2bxUMJNQvnOhzgFlaVDV62ij+2HmOwNbdbdbhqlJJTz/C40jkiEqLvxkyC3QcMTyjV6dg530tx
KLFLLY/zagIZgnjkaGW+i/eR8YE3ChXyFstnkKSlI3p0ibUYxuc9a3rZjXle8x1hdypxHsHMCmLj
RbZ5P6NOyYnjt0hnVpPeSzmmil+/jS03dD4nqLs3b7lUL8ab9oYJ/k1ja/KV8s4cHCyG2BhGBXyJ
BShQjEqTVig+svddGYSW6GaUdyn6p4iEeUYLDRTmtEEXu58As3oGJQozTwpDkwwGyxazlEffcPCG
A5LhdM+tKyn9y8cGcyO8YoHkUCjNk1zn5c2dPXgP5vVrS32TqEBJMPTCznDJBJQy5qC928CW9Ux4
FNEWcwtPf6AQv00WtgM/OwQHyNDCjjsgsDu3Net13aCCfofr2o4OerguAMp4xzvAC2B5FDRfYgt8
PGPhVwGbsUb3GvMzwj/d2DvD+UwDoB9mY79lkF5Cc8tEj+GQREo/SjhMkFTiF306ppmivoCxHhwA
3LeannzMzXvJsHPBgxZFgvwuHmpP52a+89mR+L30IVh91OAgHir5V0EfxfbRiXjnN9DMbixcM6qT
ixk8jflVBrkhkEo0OTcGXgUf1qNilj2IQuiYUCDvXfkEP7+gqZ2A6KYLKS1G/gy/7KOEG5DSzdg3
OPJNGU9RDa/Q7KjM8EDTZDUZlKYD9dfbEPbM0XSzU0xHGpdT6xjaAw7qIPhEXAu+Ti0hUC9AOhG2
cmPQSsBEc/MA6ygJI7znFOefpE0Qxa0vguSvLNuMsFrYD4Ec3XYuvRJv/IYfPfCutCFdWmzlUIAd
b862lAFtP1+ZTw9qIsrl6brXwaYLDIp+XsB6AjvjgRmGOhXA8OueLmYIkIuSgm7XKJmmj40xhGVo
+JiA5tl5myve2GUhSqCrgdvKDRCpyMZWXt4FPjer7fBr85RQXZn4NL1k5fMhYLqevHVyOQ6+hl2h
F6y4vbEClz/dWoEzIOGGmKWmqVCsCeRdY78j6QsgFZFMIJ0yUCgGPzAZvJFdmwwZDjeEMt2/m4DJ
G41mOtP1WUO9EjyPdE5x1YX1jw+rE7pQXzNabF8RmgnXuyNzBAlE/99abBWc9j2t+rrJLFtFtmjS
bHkbRhknvS8/r0PcxAV/a/OLKoC4gl8435if/uLb8YxOWMNK11uDCqt9nfduQpfjxCCxDgaO8ZiL
vW5L2AmeNwZ8K0bCzWhL+cG1AbkotPOqJubNwKewCYt1iipew/A8+m5GoZMudRNvFJC2rHc3vQ/y
QfXJtrWSsfbaD9k0t7zCUNxdfzeX/+T5zqUIy72aIcBZyjGYS3kerdHWO+RH9x5DAlpCSKvh5ogQ
XHF44FKTw/B13eIl4TknYt/qjl2iqMKy5RejE+y0aXM5L0c2gh4WoETLmiw0N5n2JVOf8ElBzl1r
muHzmQh13m7biBNcYJPD8GbMdG3jVQCc50O8Msv1lGeXIOwooS9mFCevRVN6OwtHctq0Egp70K8E
8xrTi09Ou6SehDnk0JwDnvAKybHoKAV6Qw+A8p+JBRC9/9b7qHbzYBFCcmAFtmmlePYl89yUCOVk
uP0wfV9EEnwrB85uKAKdbWssT/ZxNMpVAUsgbhdodwuS2L5Kc1eQu0CTGZNSs4arF97yT8n3Mngo
QPx08TtMpiEED2MGq3um7oJgLmI3JaFAobbFX105gSCKDU6ChJEwCHSqfzSvip3oJMxhnM39cnl/
T+8U4+n2I+H1ivmvfFlprD7UkPyJBFH5aYMfVPMm7hb9Y0mvzA6+iHNNTz4CFQyerG2DL91qY9VY
ALqu0OJQnhxHgIPnVn59HNYUvwZ24RTtu1Otg5DWGAbwJc+LRxAQKtcy4bKSuPHOcPttFSAPGbUQ
h/O6ZASvji1KUVKU/3YIXTmZHnF+Y1J8x+V1EOT6ZufBUXwJC7/fOSZEFPSiKMQimtcTnuJfw6KO
aB0LkLvo6h0GtS4Osy+4aYI/fTuhxTpmcvqh6BsvMT4HytPRbLole+YkxBMQqM2tnhTVAOLqHKth
UCZk4oHdpUBG2G52aOwKfsuo+alWS6/52NogreMpolNB+fnb08qd1yNnu7wwIyG55PiJLhjz5JgB
Zo2mgCnX1sE3dSUVIzrbQvslNoOzC2Q48w6MJNHaq2qEpyUSW7duaGY/nMKYC/dxWMJ7bG8Ajh5O
Hd2lubUH6jTPhbxJKn/Ve3fgIwoGNhP5wzjZcLJGRJx3cyjdSINqKP5+OmUxrRtYY1ZPeyvkjZdd
tQEN5XoG2+nJBe2cg9+QekAWTpGcldOtATShOlM5hEIgZfvPhMSljVqraWZqsWALRfzbvwqLvigH
eKKhbbvt8Lv/z91zlrAv61nsDwbiJJ9cP1CK8oRQTVzNABsK1i3ICc+V7B+ec3JPhOIuTs7/mTud
S9o2+g/9J7X6LJS55PqxLJz3hSkHYEEffO3jH9sUloxam4CXpLmP2eA8O6NG6yVvVaduDxCShdiy
2+7t6sI9Ghn50H6RpyjUG6m/aFJZT+U9jogvz/Wga/L54Fp1dn8m4nXQYHf6F4yrqZTrzyG3D1jD
ft//hoY81wzWM0jv/ed+FnyCEmEHcAeqPZBigjLBd26TDyd+p2erjewIWCfRYg3ViEcF4nZutjoM
KkLLFYvWliBo6ydvLFDeCmJp3LCF/yPXVyjzNUhejtoAB1bn7esa/YG5DrfQxE5wGtykwFVovNbK
GdvkhsmwzfeCxk1qKXlZUgjWSHD6GTgz3zs0UnXPEllpGeeIHeanOO9Q3pEWlHHUD5qK7bfbLPJM
ZTMLKGzVm7hro4ByqQVRAAUWXJhuBDM+VRLRLZ/DscQkkDpjzvr1Mu53bDBGA/Zt+PH0bbrH5tU6
XL6DuD+SwQsP2doOIf2ljCd+s3S8AkaVkbBeVeq5VfdVnLm1V6qbFvIHvaXZ7bawYdPnUXNublwy
dzSRb9MQGh7I+BuKwxbznbOUguWHpoHten+XqlZ5326iLzsJ3YeKUmtc1wbaLgF15TcPwQ7TRQv/
FWDS0OxoY/6w22HYYMGHd0tI80ubcqpdszouS9PY4DXjzyvppYUyXZ1wnqK2biYv/ZgCzGPACVR1
g059J8LFK1VgPgaChNAqBeNqqXzy7TAb0ZWjeB+F6nVN1gslR1tcABXfBzdlftZ3XaXSo7leMRCl
PQX/y7iUkqKvG1HN3uRt1Bid1E54wnWZBeSX05wrZCu5L2qvdBnTMFiuTSBfHceSTIB511gGWfew
f/aQpBe9ikjh1uXT41AKnvWGfNYAMZbfqGRGHRw3HYx/C8ruJogA9ERbkpkA1dHS+Ig86eujGp3l
g2HFRHfS4MkI5heD3uRsMaZKk/uqaCASgF7hC2WwbtdSe28vKov0sECtNFIN5NPbP66CcAeJD8en
y9iYG4LuOMLmt2X76s5NyoynOh8B/zJhN2ZOBenZwbw8uJedlQxB0V9jLgiV48lxxpzTJHpuz2jV
25oQY1w6qULRL7JpWaPYn4+obKhCg2qwQNScUsFoXEG7afmsGiqYMPPuoxDxpbNoEpMH+MxSdi+8
IlZhS4k7UjFSHqR2Y7E2sfYafgkOuyI5SQ24k1BkrKNvfHGlqDb4GvyaTuazTw7ZI6uL48J7+ZZk
+tRn2tR/PM0giZQBi50W6GwBzyHMhL1bY6HiyUkzf6fjbinAMxw6JaTlM/WhSNYl+JvdBzmnzrqN
1g+OUdAIhtbOFUHdnbsvumJyOGjOrurVRCkkU/OgZ+8oN7ULNy1EteXPUGnvQqGk05iHn6wGK2ki
FbWICZOarhWKnp11NA8wt9Zo+uQ+OxeOapEmHzV9c4ACxEjRv/LV2Np2eKy7/+b00il4kRO9XKEJ
iZ8oBvAn3DZ3WjtIm31ugKIbnvajbrw1hwq+mtyIo6Eoc7flhmN10pOQ6YQl0bfyxOCuuc0SfecA
mvFFe75/VvgHTFrBJMwLn8ya6mE2T8RbWYk9RFGmbz/KyKck1nn4mJ2UBnDzQl3v5OUVNnobGDEq
lu+zIG7acvDlikqgLJLcUGw8q/pqgbWa4Wag/tsqiMOiDvZaJWShi4Tq1HJEn5OBzTpBD3Ve3xIE
Or2iEUrh6VjMv3hlMq1BxKKeqV4LPImH6apjP62FOyc0WiA5oYqj78D8F02qHgFEsrbpQbEie+0a
kOjulKjkAuxubAxmpLZRVlizVKAJ269M1m5URl5H9HMTs6WosVhiW9u0wKW6+a4HptoARuEpkU8t
oruTEf9rFhtE0QB1AHwl+EsW7wd58UmZO4THPycAlziAkiFe35rMJobblai+DF2tRlBIopdyt986
VDXWigMndK7U9RFUyBgEbz7pjImuVN7vU9xtZvQqNIbd3BblpeNew9lU8wxsu3Y+yHZ4HIs6Arx8
VL8TvwGV/pjI72uppmTX5MDW787jHRykPGbVIJHrBSch+PpFOZdjz12OTIMNtE/lQdv88/Y/HqjR
biDZSUJZwqfXnPjF7lTDYqGPoTBYOWggjNZll5LqR4xc5CV4W7CsUDZHHM1zxan9rQKnml7puLoH
qnQQXyR7MTHj9pKTB3XdWS6Ky3zdf2JP+BghEz3BfrEOVIX/88E/bolmmGVcFY6t8+YAicxjyJCw
UlC7hOQHFiYC2Yn8q7ZkzhkZ3hGDQ511J+ed6QpPIdd7ia3LZ0cR2r4rlQY61edesbaGKLEHWdhV
bnBiajhJTy28Evt40MnE4icz68YBXHjMOApTBQlOip582QSNvIGpGomRXK+J0D3naqQB6qjo2ko9
Q6MtUBfevaZzvoxY0WY6UU6b+5+C24K9S+/t2q81CfPYcMle8wf8vCy9pu8ugQUErahyrZh69OCC
gYAO7VrKNH6crQpqjQN3i5of28lO4OiR2A+5/hZethnuj4zoaVadyMUM7nGVr9VECN0ruPUAOAy/
UWTtSsYTVp7+pR21tWSe3mg7hsiehErBPNmC5oGSZkawnHj9Sj8T8FfRDso8tX/QrQGPQshyFgTR
5lgUDiIqwLcg7ehN2vvqzN1rW/TsfoSFB4KhEYPW9OLHD3S5bmikNpPw/m3xpgYktbOOnhFTXCYO
gkVwW3Bi31EdQjVZ5taW1SYa6oSGIlOfJry37j712+pUMmyJfpjhqjp9eAQOSa8q3hGbta5/gZ1D
vhCbEIT8abmLhTVk9L+HJrKfEYvThsoPBJ3R/vPQJOhm0HNv/14fipHMMvsFIPSgvosmvK4Bs0/M
/V7PEKZ7EiGiBopjo4QfhRhEFwsqHm1pMG+GoXk/jas8TPCWIOxsvO1LCamr0mncXO1gqfO081AB
3q6v9x9G64UBHjORQ6mDO4RJRC1pDwL5Nv9Z48WoEjEh2Z0F0cedpGvZhYXv55RnJmOe79Xbjrcr
e5CPJlt8L3QctVQsvTyeECcg4+WBErfEY8JlBsJX9oBhNsTHiPoVZOiyQwTsRT2s7GdADZbGT8/j
bfG1DzIKMcggAPivTDgnmZE+kwSax44Eq31dpGOLVipyIIuGm4gk/4rbgcI6p0fjW0ei9CSbNbAt
PHPQcdh86wn6l6+SJSCVED69Xr9YuE5LN3WUVa1nHyQCX3gtsajJXp1h9TIuEc8qy5xf4pyHr0nk
2nAcT4JJZge58ps5nQ4VVzU1VIjVAfndZu4S+za7864gs+Ny8bpExCc2n+ipPiaUM9DZPPozpEDK
ADUCXsxPMxjqOf9b+q/pekfMBoCEOeR7Ruv6WY3HpBw4q3V8qBWmSZNnd5lzQ2YgCl41QdLTQzMS
5FiTy4TkACCMS8iBCbdLfjrWjI2uXrnzAwQynyH0esUkyA6vUsxwYwLTiTLiCiwGcOFcO4MZhexV
b4KcH3NUYMPHXzqKvjD7QlKxx/GFQpidOLqesbyFst22406xBKXDn3PXmeklxVSJ/TXwed9XZ1Wx
oXTZWa0oPmHrEH+ckT8GrCdbarjJPrZtHOIVklijjLDHFc2msxERJ6ZVNitt7NbbhRxa2pR+qkoU
egiVAnptqBwqO8xjzUDykQKK2BhgNnm+AxdvGrjzY4hLYma0rPvQZzXIYFn9f/RgNq8LHQ+MTJ+L
u8WOjVwv5UnonRwESKK+RwcBa+v5//jdo/GsJJ1QR97Fs2MM/y8DkQHj++sE5bApdlbonjBRR2xk
gInp6Nvh7g4Etv4LexcbTZwkQ2D8aedO2M7Paye/n9w3TnnE8YmLiKuVJ+jvKsTXnC5vFrKJdmf8
wgCh1/qR4YvJGu9G1276LFRejL7NcFg4pATRF3MbQkBc1AjkGzhQZdwe/g9uC2SQqitU12j+8nGP
tCOjo2hWOd5AoTp21gPkWDK2MTfqhEqYiPhCQw+wVI86OG465WmZBw2Q59ucj+xiMPfv+r70TOvl
CnO0c+lIGI2hslfcWBLso6xC8eD3NMJrqV3bi9KcVBZCeGJpbb2oBtcV/qdNVNEzBqXYfrQtxpkv
1cWKvH/K0z+FiVWEdRlB6zrlVldZyApKIXuQgy39Qpy4RZO4alQ1b7+tiZFCNVVnzAN1r810I0ha
6Dg/makrhdgguAA+m03+YVC00LAfac2WBmMz7eg4DsoVAsJSfxJUkc6ANV17lXCDoQvsKB6qhsGN
xwbiuWul16/Sl/Y6jT9YSQKzVgT3HAdrTEGPExUJbnPGh9nM7a79e9Ygxz76zhpypBHwKJ1w+0dX
9gF493p6pqJE5l3NTh8Mu8i4+Wr9xCWQfoJAkOUCys374BZG85DAJUmAuPUDNJwTdplQVzelv3i2
DDlRBK4xs2sz+fPXLbHXfNP6RSEoOUBs/Ev4I2qYl2kNe6DenFW4rC7y0FD8DRQz7raUaISSs/A3
O6fawyGMGyG27BOMm4uGwc02J9JzgsWO2WPRnT/wROEl3flHBLBQsus75szPysSS6vU+sXa8z/jO
aPBl9DCmmoqpRGLH+D1jRpxfPfpmzyQ+bmP/vbK8ihkvkw0E190mcJZk1OQicSsc7272C5g/ivdK
4heV0ilveJfPJZCzniGUINCfByVkWlUfKUKAehu5QA8AQWtTjZSDaTtYJ1gL9B/cw8Oi3gyUbrHP
nrc8gSu4J+iwDYosVwOARmmBw70nkH103pTHTSQ9zCkCiUxsH3IBKsUu8HuS5ikR2/XTMRemh8gq
JyrgO8xvw2O7jErLtHRcURm64V/nRjHZkyEqyAdiO83nptZrSrkWzfHZdiCJp/H5Y8am1zv9cWag
PRvsvne+GciBEP4sPtmzUQZjM0tmv2bpo0dcreYCjcG/NSpzVuOqEFpsYdxVyZ/3Be7kAcL2jdyx
lGasdV5S81o04pkBz4pkm8/cZQIiIUXZOxTdAf4WIgzpP9HMgcuMhsCuWzWyupyEiE8x4EjrJUVQ
iLoxzW4fh/hp8IJLwm++XkPw5JiI90/WkTuKhemTDWrs06kqVwSEMjT/I+TtzXeB8Ov63Who8tFk
4fa3L8WlSBE0KtorsCTtMZ+I8Xywdvx1B7N/C2qBsNIp2IvGGd680tMD94hMg/RkI/vU3Qh7mNtk
+V/uVChl0EKc/FcDab8FyO4/JnZ1s3mB03amM3XK7P+2tUGJqYM50NtLzFp+sPhPHA8Z27fFwwkT
ElsH1rJQPMq/v6TIv17R1SnNa4iWQe+/6p2GlnXalhfw3TentRp9JGsF3CKlNSGjkQIse6QT2o2q
eH8dpfhIlr88IQ6vC7At5xDhgEwhVFMIXpImu8XBp+IlTs7XHLryqcp4RfHhTtwazWq4jUG9ZBqP
HlSB2C+1Yzlz+O6entjZ/sv81KjKdsM+x4L6oTEiElt+XGYX+HXXarzOqciAzq0bxUSz/4eF8ogK
5VshZFiwlB1xal/28npa4XFcSyQp95IzL/6e5z3f/NkqJDGK1dHt/z9oTsci0iogDb4paVcf9cVG
a5E0rCBeyTbq/qg+JwJrc9ncG/F16O3Ikfopt04+LwVQyHoCx45F9FSMihNtS+LJeJnTgLtR7zez
9cylyS24POVONLuqA72wVM3HGEibospNMmfpRuWHKpxUo6GEQPXrKLrdKpm8bfEJD8595fp+C9BV
9/p6o2Qeq9eufhX9YaKjUZOWHTQ9vQRfmxRlQPmYZRMs8+hmB6Cp18k2/QM0bG0kp9vkgojxmX/w
WORQJBKxJVnx7KHVUBrbXuWYPBFdVfuk/4B8w2XWA6SW/UlYn2SuyfyTEvs9zR5yBdUtp/5QoKNh
V7YDytILmvAmNy9raIxHGsR7MTB8CuNF0NKcCgTXg9docX0W9y74zf8uev0ibfUo+lEZJ51HqqBt
pcvXkzHwel9rpBmAHm0ffK/sPSyYhnDSYziYUiJgXu/KYIWTcFuOh/dJAMDjFHoRwXITBZ2KddON
mPKobI/r8BavTFyeIupqQjAhQk+62ojQ5C0rndtJj7t5v4tV2bKidbWfyeky3lKVxIuBVimoTTL7
3dMMmopc7hEla2e45RhgCD+li9QSWCe234dHYm/sAG3Tu2CSAYgq/H+gX20hHTYL93rLs+ToRfc+
80ODC8Kt/Ny4vAOp7h35NvWw4Wg1+hj0QiQozYLts+ACfGLqgfHR3+C359qObiQCzbXqbjNw6jJ7
4yR1O1R1nXKBUX5FvF4hNn02JuVJn3t20yMq2cUBLzRXpNLbx/qBtbK1vek1hMAicncFu6CUJcpD
oE2oHM9czGbXMHSr7MhAcDK57mulqcG73wuB0b2T4P5hHoeQVjvlW7KXNSycpAotzdu/U+hXtyr9
SZOyzetja8/KnZOigGls/A1ZCDqzRrSWS2QfVdWrihSy2y7VZRrLqp/noX6cRK77kFePw+dhMTYD
cYVl6Cl4wXxszhFzY3WKEgiIZ0q0f8feIlOMJkpd+KhL7gERWHm7RNmW253Q1zp4YYyD2d4NVhYb
7/p8sNp86vw9Lu5cT589RT0n2gVU9orkUy5SLYHoEuzzroSFxPqLQddu3rjHobyBeCtN71X4HsX5
WVVw8FMM2kitG4MtZbQSc/kiTUGEu0n1vs1zEg/Ro84BTNexNDbKmfgolR0/EsxEVDbG8Ayrv4Mt
rXCVzVZIdUHg9q9JJ0zcGnM0+2Wvvif7gAgIPyCra5VmuMgjzhErQHMQ0RirIkV9dcErfTpywD/Q
ljWLhpJTOAUre5RkzoVXefpy3TRywSci/tqEpEIReiEEQ1v4BO7BOYOWF29TnWRBSj7d93Tj9YA5
KZkoJ3IgMGh/hrHdulKygPtzdiIBWYwNiw0XJKTABAFaJG0xKc7kVY1JR7V70tfptuRRRpzPnAo0
XlND3ltN8fN1Gt7Glwp0z0NxGq4FS6m7RZ4cRTGeJ1mo6FCfoax1H5RJToSlHr/Ji2BcoRlTglA7
RDqdkxaBMfwJvWGN1Np+wj1Fwq8ZUoDFx8/qRCgzy/BZh674ds67zdFXITQkTb0CTmKhPb2lsQCh
d0xDZtKaMzXc6qAOa3ZQgVwuL5fiVNgbW2AbGMUQQI1g+k4kMLj+2R/iOuofJqyCk+4Q/w98pakH
yn5K47QsGxhbE+NfMMkg9Plnc6dYBky18ct7t+IQLXpuaYvEpw5rMDwNKNBqJUYxxxFFTL86TSdt
LivgIem/7ebttTkGnH6JboPw1jkRezI8L9ggP17/fTAi0ScGzFFGALIGjofA1fDuMHjb70mXwtV8
RutHvo61TOEPDLd99/ZgiQlQWvJj1lKN9TRcdI6xMUF6MjpPmNjFH3c7BnjSgu3GTOhXiqeOP975
SjlIcKh18fspnRGAPjTV56x7lQbqiUvaMKFP15Iz6nTGIENyxqgJhOFQB4cLDeP7dGVtO0b9/EWh
bFDmesR/Y4ZGanI+5HiNqKhQ7MfOdMKCHPzMtu2r+UI6bq9a6tUkm0Qg84l25+UX5boDr7YSXSpe
Kag78xFXs7JZJZHN9N9aX4gvbCGQhUZ+mV1+e9ucTDMRnNIKt+rz2oHnhRt8FSHS06QyXY3vFNxA
7RHbciCM6gZn7XKjoDazJPsJQYT5rDR/Kxiair9YH81lajF9Ahmw+aoD1hvsue6uraznJUZ27v5w
5wMn4NRSXoABfPuFJkLhsPZeLUSSrD5yn5IxipIhiNPbm6uZR60ZjgTfRYs53GuB+quzSLnqYt0t
xz34srpK4Zm1gN15xiFQAx5C2eU26YDb/RVAybnLy1RRxmR6qwHc5oydeOeJqu2wfw9FZZxIfL6f
c1qkW7RPtD9v1ogZTudQMDJ3QS9x/2+PTB3dJssAVW2dvR0O/38XAdbVdpp41sTH0YPzwsftBqer
GKBrTpJUmPXz2Hi9m1zbl4zNlirRzz+lMZkFYOMW9Zy0vUgeo792ijLgCrTfpPsDroPiL3UsYShv
k35K5+vTESRyLQhnlUUEImkWmCruCtZgGm0JTu8mn7KyXf/y4aCldWt78XGUbvfOml4XfXTMaG9R
bGYvueERWXkkD3Sa1+Lg3Xw3w1nCUjrEXoXvuaVYybVmaFXIPcRFRc2PoGBkeehRKvPJwYmXKA/o
De6kpGXW33fttw9+PHkfiHF+PEBfBp5xeYiqCa8Yk3nAzQXTKrsTKNIsDRvvBfmkBNdrYQQ4K2RO
KpfsJH5LkFnxnmXbeXkEvfNvc3wh2MiTVRQO91MJu+kSZMWNVa+GkCV25XhOaedq+YpRhrcZPZv4
wR+ebt+JgM47G0rr7vSiEKie+yXWlltBiO9F8fqABg3SrjbpwhRn5BCBjcTyHQYTkTg5RaN0+T3u
o0Q3Z8mJvLhhdyEZFjdLnNfVWGNJ7XJcapV4KsF1T1SBuvuU8We5X9hG6wWRDJtPGqVLkLYpdEX4
TB4bJ2zOhUneHlwwdR8xmmdAH920FeggXOHy0NWKbI15TjXiDDfDK2kh4q2N9xi96nAT1lNCCQQi
rhYzRzDvcWByuFoTpe8plJE2/jbmHRt8l18jNKK+p2p6Pq08niwHS6xGwTetw5Z8DqutbD/6hlMr
Xbmcd+xEdTOtuvX3HQS3trq3Hzi4HdHRufmevFEiTRx9piW8Mf33TcJv3krNgn2NoM2VoQqet+8C
z+JGbOHlyJWd3YgUxLpuz/UgmhsohjmRnvN3ditRFkmc9L9jbTpKNJCPpYR/j1vNhUcgRQJY+hDW
XaWXCUZGbbpPpZTDN09CmC1YPsNxfaRcPFubFwXUbxMBhwYDSkI2wkGKbGTalrH6UNNDrstZk1pZ
KJFcGW7buqsarreb2jzegr5PTfBsLU+wt0pcCyPtLDyuHxVxIJt3c69OHA4Joy9FSrXLPTH/z6PF
mmet4DX7eVYih3ysh7H6alNcpBNRIRC3MwvMrA116hJ6Zp195XNvpjWvQBSede27DgS4OdLoEEoi
xcFIrcLYiSW69S2q0UrAWWTINZV6rYOokKYzB3+JFXCEmgv+3cFoHWUCM4cDPkQm5pCSW49KCiCF
C5PC5ywUcue3tFAftcoYZ2yU/EFxkohyzkRoW/fo6PE+hMp2YKTK9+BP8EzRiPwo3rv0RGWpD7/d
2PDhQXZmTi/gm49OqHUDgzlZ8jLnXxiB74ju9bbRBVi2jKmq/6fZ3OZKo11vk2dLdms9l27QBxoj
A3oZ75euLUeG9QnfPyNX0YUNbDTTjdkhsCoLSSpGibc0JaRngFzOir+TZTYmDetrTm2V6/5enpPE
yHtyFreCrNOIytSVpRuRfRf05U9D1HnlqG/6y60p2fN2I04qgCkixyXCamcDM5840L+nmavyYzQn
bpUwa9z3oZrslW7PDSji3Ax5OrUsGfdjJN2ttPG8nqX3j91+NNxskN5vrDWflizccRrDNf90qhir
2xjW7Uk6GzahQcTyozHh+bnDJIJTM2jrPN0Ny6Nus8eVFcX9nsV2LNA0QtYsWCYzVix1gB0q75oh
Rf98KTfMnTvfpit/pSJw3UyZ7Twko6Pe3xo5cfRVS2lsBaYGS4G766UxjhfFc/nDV2Bow2Gagecv
dPaC/a6KswK/aHmBQ0joqCSnY6osb4A/xSXuWrQ5hfRRzEL7h1fcjAt+9CqUVnxXO89RpBocVkNM
5/kWSXwW4NQglcTtaavso5oue4JhYsyco5OISsb4j0hFZFYJ045qc6g+zmQWMZRoN7BTWrww52b5
+YKBUoRDmrUiMoNgk1e6vm0BThPYJ7nQhbP7zvlgZh5zTt/cFZ6pLo3YUCV8dObowMj4hfTENzQ2
A2goi+1rlQqUMPLF+4T0K7DewUIx/OY1HUFPrTPyP7OAwg20bL+uoJ/VDxZakdgCd2K5uO6eiFnO
XM6FFo8QicARKf86f5eMPzleQjrIKhC3KYAnbykKKEKIbjV8bBn57eay6P5bW3xttPPbrtfGxpvi
fD9eI8s0ehKGrmZHp2eiCYBUlnyBHVLqhzAJJhkoSQHGX2h6AlpzHGJV6LOOTju/nrUAl4iG9Z4t
2seZwYWIyLqA0zMRxGDchAcRYJ/TAEu9IBaTCQrT+UvAenxAMISBuE4rsq4OY7UTQIW1z9RqHLSb
IBPBIt3dr92XkGkqMq9U9T4YslSAJhvjv9n9rHrZTiBjI1bW6H6HE+KXvNegw/ipVFkjtTOWOsy6
d5YCiTHn9I18rudm6ELP1A7Boimk/LTffLnOWLJZnql1LF0hrZwJl8+NtTgjBZRdvuTZLya/6tWR
1ShrJNTqA9gIjmi5VRv60tSIKPI6L3soJYYXqJM8vxeAY/isd51+6fAYHFrVvmr/5jS1cV5iI76I
Ieer5Ad0+ZirWUAlnxzkgdOoJ/0FU1eUoFkjb5TJhf+FUolEps/csn09Gsd89j4TQineylsimqS7
PuM5GVfgjpKc/quM4DkakUEuGWNl0yMouN9NSF9wc53u7a8Hs7fqhTJ/IlWUTMAFNhQUnMOr2XGP
9qFxo6u6/lDP69lbH7GDRkyg3uIUa8c68vXbmkseN/iXutFcYnMMdSeyPkxOq8W12UOy77/K+S4Q
8oI5DSs9uRI58QkhDmfHmc7b4nziethw117uwyc4ec7zKFEuZ2MbueBVLMbJ52fuXmzrkQWl0/vS
wOZg6DpG+My5GWyP72cXJSgRNb4bcp4u0RG9g9BRAuvzhGktMgrzmq4+QSu1M4sLGnE6E4VwnFm7
qAwFM8KVS19Z9W0/Da70pU++nkS/OiwgLqHDJq3G4JtGUU5/bcB05duBQrF3BpI6sbFL8SeIwzjJ
RAen8kH5TLBdqnC0WAfBIYRzEI8cKiktf49RAJAZ0KKh9Dwp8kpGQyvvZu3KYoimQ0as2lDj7xYz
pcWKUi08nHgKll1rybavK2SR9cbjaZw8F2HqfaEKEPQEBhvK5u4A6i3gt8Ipi/ANd5pkSbcoMktP
IwaBaTRWUYB9KEsjpgLYPzKOteU8IAP3G9LngJkbO/MY7yQxW3YI8REfEBvNQtk6Gk47+tZe1dM5
zKWWTQKlfGUZPTiVyontMbV5/QjRo7R41u2NQPvYNxQqTk33gSSEdkNHokTeE83LE41oTSope9Uw
Vjx998vFEUDmdJu4ovDYDJjdzIEEWhVTp0Vj747qenfgxs3CZ/w70kwwrPZWabNRyfzGxJ5wubqS
VIUFQJJXG0tOTL2DlJkphzzLm9Ltd0619y5ASKHp+70A6T1aLY6x2RXnamEEy5z3/aqZRI4G/5nm
g+mifNdHReWKunTzkj2GkkGx31gR3Y4IoD9QnW+4+j9rOAPqOyvIAUDJPgYANH5vEncNcaZ6A8GA
qgWIXNSfk5MyKPOTKiSx9WHE6EIO6r2i/MrElXA892O/EvBPXdOFO0Xkq2qvB0FfaZVKG9A1e78z
a39fs49XHa4dQ1s6KBVXiN8PwRP5mPJdJJ8gzaCLNwdfDFvy4b3qEK01YP0+s0ejVcOXB8CnLqg/
TOrpKGi/uc7/oZozxDI+s9K06Vwhm37Nh50qavKNrP/sU2OtvFkYseYOigr1jgY7PvatnlXCEL5h
lkNv63AdGAV9plj4KinPXdFFAuuOm7Ffn4Kvif4Ksr0aQayajpd5AaUs8Ir7mowqhpIVb6PO9a40
RP4AX3wZ/meD/Ifz3l2EyYR0wCo8wuPIDhjCfWTXrdT825jY+ZjwryVp7MbRVHHhHM7FutI2NP/S
TvBALtGfhPHikMKVPOiT+aQuTCnn9/I6BZEZQHbpbComeOIj1dx16px+buBukurvsp+K+DnpSmxx
kvQA2JO5VfNVxxH1ASTkl8+GH9Gfnm+wjTHWvmSTMUz97iTttlLInTJqBgpZGct8//QaU0zRnl7M
k4hrWkc7D29CcOMTljfReLlKbsKhxoIUgK6Ks097fzCZrjhGh7lyRI9h4ttJWZWeJLnBShUMg6A6
gD+tzFm8AGiSO8DlWCHufnuZrjmJl9yjjGQ52t2hkirLFeHMVDV6TUMNGhjk99AcrIWBeiA4hsxK
UhfFwe/PVHRwQL+TythyZ9y+86k4XzbC/cftShqudKzxNwZTVqibtbBJ2fzwV24V/wr7PkKYNq+7
CtjBr7aqlfajW01/jXKzolBCJHB6pQ3UZHVn/jIFo9G0PmtTFwnFZDqw72PffhK6/4qCLqsy1kLO
CfvvORYHDVfZ9NvTyj9V2YNJMMaT2v0becw2eLHAC5z3bdYGrVHZeYyiJ2Q3FQJvIlUiqlSKgYuh
jXPRSGdZlNRURQshwjHalV5viY9lNaSmamp3n6jieVQmJ9MKX+Ylov6m3GeHVrhEhtmdhnjTH9Gs
dopi88yBy3Wwl8/X7QkLljdYImUBYM/nh2IoENJTjz0CGrL16Latcc5+pJ9phSnnOdR6KwTCIibq
F8xIf4PJ/XRDNaagaK94XE113jM1FZXFcGJ2RNduQ5Qgyk4riBQJlMOTnOdxiv1RV1juD0VSYdNe
1x6b6pIRRJlBPcTTsW+0g04+t0CgrkR8dlW7tn7gX8s6c/A2xZgog/wgPrFlU0+F7efLmIBD/FJp
y90orG9Ys9GZt+/qzC+wQYYrXLiCSfUAN2iNaJcPz6IMEdGkocGc46GcJJWbonnHQLXQT4TuXPHa
SFifUnLm5zH7TMw25EqKkwINgBYmKFV7yBitZYlotXwqsazIAm7ag0uJALi/v7njbH+9yj51HrTF
jgs0RiS3LUszXUKc7vQnTIxGudmUYodXATuN93asjNhnwwPfvfZKQRknmvWp7yWtg9jN90NKBLAH
7ERqi0xYpsysPbtMniP3EVEqQnRga5qG+qw4oVmjIY8NOvXILxYPPxfMsh4rtI8lLlRj2fUXhGvf
cSjVOwPx+Nz+ejZKwAItvqvjzweMbjCWuKJkufxLGoGNVYwPj/FCvQ0eQZdTn/8rVfYON9D9+tUo
IibuEvIoH8K5hRipZFM51NQs7/evq/WFVTy7YbXKNJBwncyFYRhjGk/RcPnWiWT+SAhuLGeYpH+z
kQigpHVPhb5jT62djGCq5Hapw8rum+NDi/U+w0I5u4ISEYfsbbOCYK/ifaEBjWKHEfr3tNTaidAI
+u4MnHHv/gB13gYIIhamfJ0+E5Y1b5d4DlSCl0druIVw2BVMQdL6TQxDoMIEuxCGuX4ZrsZj2I5z
iZEDA7itRbGCZVYVW+PzV8jC8fMSELEdiiFfOLgv+41PSgK4vT1kXkClIrhdT2veIQHy7XGc5CXB
hlhJ3BApFJ7D0T3/ZWCdtsIGRb0N2+4tzynIirr1PKPg0ruoxlc4h9QQ6hpL09V02EjA6P9itflq
XdnNT4dYAMH+JtqQ673QDcPwBg25Cowpt3xy1qeD1KyINzCKb9wlEJrhvRXiQjacpld5rVBOPBPU
g+04RAoxC0USiPMlo4BcSX7nHVrBCy9nhAarADqVK0uRK1PmztZvliTMjSJyS5X54D3ND92Coh3I
zBKSgKFHRxAoXW16LMJZkixnQ/P+QiBiUmHCmivydAER1fdyomdPty/RiC0ZxPSW7J/yML+E5L96
URyjbXG95K5QgQx61ben6S9g832aoG50Eh8ilj2id90W/Az7HDFMzbsfv4fh2G0raYcnWdvPUhsT
gQcD6DBhDbf8RTIFz13dd3ZYm3/wN2Ea0y5fE76wERKH59xMOh8rGMl2YX12boTpkCP41vocqpNZ
VEK2WwwE7GglhBe7nIq5o4vmif6LXd+ymq/WoX796SnSYCHxxY5BL9gZYBWE65rAgT2iDvUmobya
K/91tTMyF9EUEhfeQGogwvEDwX+Oo4kxZLB/NctFJZfQ1scfeQnt4hz58RddURYNdOzg++r8FJfl
BxaIYTxrwxGTDppMbbpYX2nTDK20W0VpE8sWLpowmoBW6kCM9Bn8IGh5ZEWeuL5Csq7cM08Ti/Qz
wOBd8AWD7Pd4iEBxGhgHIZMxkJjnKPlVz+1i4wyC9pqCrbLI+GAFdIstB1I3x9FYjrnbK+MfgZ9f
3eKxavjsLVGNy36MM5Dk7MyjLSvlYDSkklITNEVnKEPmgCZcQRx0lCX5cDmjJFrhEws/GM0sN6Dr
vUeDlgPCdNNfDiNymCivUkbsPQz9cI9n4ZT4X13DIZCq5Tbf64zhcHnFj5Hgy+QXT3I1jHoUIN5f
I8f1i2jH/nl49ttfRmFgeQGcCicW1VxjV0aYfM7+zsbdcG+mkeRfU+fx66p1ihnesmLssJY2mnen
z4DE3ltG+7E21kiILs7Kqi1PlLZ/b/5Bi0dVQeyrIhlaexCzw6AOjXw1PoIvYEXozxWmaMiAhnLb
Ovg20WCVSdbmWanNGg0LZznw05Q8z2QgTne2JB+7h15mpo4s2Mzh46jYrNMdE15xLA11dLwClloq
GLwEZoRHCCUBmtzgeQhmEYX6XMIMeAPp27RkL31AW0xI8QNCcEfv53Fch8/+CIs7/jHVqYB8EDzU
2njPAYcE1lz0TptlTjb9Hv1Zq6wV66zDfS03dMEtIbQ5Au86Us1ByjdW5+9nLBltb/VRNjGpUA7m
YozGIY9HFLOIuQ7rHL+0615zsYcibAB8fvvCTVHo3rnOE5DsnWIPepu/fIV8YXn7wI3VgwJ4lVI8
UQR4/HuN157AEcwEYtUTRZAW3SvJloMHHKqtVqeRpnLu4gpLXkUqLry7CnkdwBdtcnFj57PNBYzt
ydjBSJHmZZpL3KXC5Pl/pe3aHq5f1bFU2NTVLAEg6q+0oehZklGUDtznrbqJKuY56P/1BeOf3ojh
/NyXZgX1aYUiyok59gSM9iGUtfAVQpc10r887mm1vesC5TPSrxPcToOZH4ealCBX+rHjVkV8GTC2
UV60allDms8Xo3M+9ngyzoqQ4jZvyc4EpgRuZOYZGccGP2OzqOBQaxZDxxM6ya1Or6cVXo5Z98ed
vtVbo84I4RKKE7c4XZzCn4tIZUWTyxjL0L+XEAh91Tl3wwHe4PcmwH8pcJL8575hTq/vFALHcv0v
sMG4U1uy+XTCMrP1PHPcldUZDmwOKvYs7WrLVwmhpNcgBKnY1W457X4GJk7zvQC8Kyk9so7ub0ts
+8V21MyB4LS/b4ajs583P9mDqd5p4ImnjmKAnC3332zRguA7eHw4QOz2WcjkGUXhb7n7PkyBnFff
RFZZCb+HWrz0DcJc4f84iTqtLUri6cnFNXG2hi2kelqx3O2TjDwqr2Fpis7XZDbPQYbl4Rti27JW
26qEp1ojt4wJgLOWOpEatWS8VNGtauHtQHCzfterMyu7J0KvPIxsdEf4D9Vy9lfktjUYEKvu12cj
jFMTNPO2b2ofO6QNPTAQVEirBilo4ITji5VC4gVptdLAK6+P0+/6QBL0Nupap3mdIgo3oz3QEKog
5MSyejzReHlDKU4sru47UiX7eQIw+39N5hTeysJSjqgs+x+M8kFmY7q+656bB+8+pX+t96JsFLDy
2xK+RcS7tLtOF9/WSxlT3tgenw46iSWdtpzn1WUtRTegw1wlHhSCTWFqDG6KOx9ckpVlRtwlI096
D4oED93RCuyaOb0G+HXT5OjRNBFXkfrM1F7GtXPEQPUfiq/exq3ONX5mPqwMbajka9b242ownU/b
X0v/qdDDuakVrqqr7TB7szBMFqMbTTCJ7PlghjLPjjXwsPhJCLusHwo9rB90OthcJ8iS/3TtfOBG
U+CHR3DjRCLnKv6ek2KPSuQUq/mQHb3i0frNJT0Zqwa+2oI3SRfi6EZvPCiZ2EllnFMa9BvDze81
+O/3ruMDZfUWZOzg0z/SfrGKRC6NtZ5Kx47EnR3ZO+wt3DyfVXdYt+YtbKBp75qdwqab7IEX8GcY
XHeGr317qkVEgu3s3irC3y8ZzPmGKx74zKSh7/ClrHcRJ9LB8zjdXNxklaewcUP51kGtnDFY4LPq
QOA7fnZewXrU3u8O8VbxHmTAwHXCDqdshhGmiTqiFdpZ5Z2SwK0cnp+h0P+57PeV0DyV9kw+84cd
k7R021ResBIQB7yE0iSsFvqY0xmpYPV8BrUXyswuQBLu3q6v+hB1At4xWH17T+LebmRB69S/96MO
FK0LL42nCUPbfSgaQ629S6usXTj5hOV516/QDXiFC0vWerP2WH60w55F2EEWH8lJqv++G/Opt+6X
CUS4lRmjng+AGEX7bEv74IPZgYDtvqmgFh0yV2GEj4nCXQkil9aGQYTX4M/V86S0sMRGovV2IW8/
IBhF2LbvdljKc9Fzh7yk/NIidf7MwYLJUBGs2rP6qGHk6tebXqmKPA7KKF/KOCYVK0G/PBKu7t7v
Klj2d78Jtc6ZqxZBmZnfX2JnwaHWeXz74EfvRAYZvZJhlKhWWATw5n9wviqqIIJGjydQy7fFGJ0c
c1BGqJ5H4Ne4Xy6aIrqLYj/IOx6FOIN29efN5lWuxq0b9mNJ+fJd7uIBX1mNLT2PPN4WOroaW83Y
Ao9aRMyfRHm5gB+X3som91p08NR4/1O0euNAc17X9/aHi77w5b22DzVa1GVtoiY+ba1unHth3AK0
46ZFgnSBEC7rnrNAmgBlMvbK1MFA0nb/0ABrQ6icEa7FY4u4F3CGfD6ZKm4xhd+F46YbXMQv2R25
PuOxgVp3GzEpOAREez7b5La4VKIEHFaYWYcklRcB0emm8flWsaiLapL1kKf3K8VQnT4oILnrsSqk
4OhX4d6vldyRcHEyq2lSipO33+ZaHDYnmKng+UIGgNmADf4CMOzuybht3xInZ9/9+hZqifFQ8HDf
lZjZKlcMMvLa+QoUK6N4W2EnhY4ehyMl+gAN1SnhqGvknBassMmYMYzs6xUEpHLDG4phk6Y9/xme
F+YBEX0WymkDj8pn0lGPTpD634U+yMRrjcVgphwt3eqxv5krmvFiWYUynsWCwsW4YUDAU1VqIU+s
U+2AKoPZKp/vaKJR2zLbtS8V7DsnG7yQK+RstPUNCvX7kMgyZaCKaEguzjaAc6FuGfGzUJqIdC6s
7sPVdRDrKg4cCYMEpkgRkXdDOuBUtyeREIYVX8u1De+3u0dEznbC7HogL2mTQfH7Oc0iDsUu3tXY
z3PAHQwE8bNoa37U4vO7rjLQFHRo6oRqXA8Haww+LvLj/jjNskoCilHrTkdQ2a14wI7yP4XL6/Kw
sZECOvrsFRCnp9JfMkaA9ZSn5lXPW/Or8FLvSORJu9/E71K5Z9l5Fs/AJzumERpilUllJW38RYGW
b5VzxayBDWTmLju4JRVyyl68mNVF3hWc7L56lR+9cZus3bBW0i0X6Bp8xYHeo5WfhcNU6XbWstrk
9CvWHQlJ0jTdpcnAQFobhhCmVRKIKw//mSi1YdozyGA7a7GgJl0rah8Wz0la760klEImUGBc8pSQ
1f8aNFCAql1K4ySWBYd6J0rKRhrz5Uphtbcnq8nemQFbdbXx3c0RsCIcJBVknRORWHsWdiW2X1CN
f1AM4gg3+5RPolUUBHNB9jURW8QKXFy80k9oLeNajhXqpXe9H9cQ6oh8/3xCcCDyktZ62rKhZjwv
k0PsHvl/7AmOh7TnDOPUQRDOcqGZu3mMBrdVqk1tWIKNAnl9T09YC54uaDCRF3fmTWXFqPfEVtsB
MdGRwzHapREBwCjKFLc/K8PpEPacblrNBiP+a63WYATAhP2Z2SyCPg/SvIsGEM1GAUvfkJgwCmTp
GLd4MratTE/PNsxXDW/9eN0WGAFMdv6DiTXxYHeGy7ltJBG/9x3ibedaCwVZk+6koSn2dyBbGtbc
KNFp0DchFv8XnR78caw4N6c4xoBDqAaH8rxfOplFsUU6XKGIsmmbVb2QxWyD8+C5Kcqld7kSO701
2tew2nDmOf08dUkWY7a+JKjq7/8pMrTMuhJNJxgreG1tZRRRZaWnBiwHRow6uiZRPKEmUyZgC/nk
y+FNqOAZT1V3k3RMYYkSh6UjP6uGLOfmidg5eu9K5aUKs6Zk+1/K2/TZwugQ4u9tTkmn2KEcXUPa
K4a+0fIcLrcThzAFw5WVObcBnmToqohqQAu2DHVg5XTbzCJ+ZTaT3oUVrjzoaMyQhVER7PIml/r6
Y9QRXSeXu4de1fcucevTm81FafEdwUcCGDkJ6ZFLlR1EH0mpiUE8UC4W8W+0+DyDYsZy2saLCvGl
UmhfTO/BybLnjr7sgBU8BJ5RXjhYrPk18+mUiASc2J25jV15qax5OksNAAWUrCLsUM52nrn5D2iu
7Lt3Sx9G/dXqf0Oo9v2igKPInvSmJDfr1KRrH7fRZQxEGI7iN6IasSeaYZBTgE/Jl0lMTkpQGpdb
E6vKP0K2VLxV2MqHWvE40vZS4h6O8TK28ITTJJixbz4BKZ0wU409bfZ8yS5oZ9srdELyT1KRcPt/
VAxnsBP5DUFJx04YyX8tEH7tbAUlRcPt1S5PXIJGwdYssPWuLxhREnlr/Qt5aHFoQ/pUzMeja0p/
SF5gSgFu7eq49OxmLckXNnDPx3i/hP57jlSuW+cc/qwmhQBRW58cVrYsH5VVMfOFb9H8IwzvIVIE
haouc9eA5Of8kxWbvsj27bnT4qBsHB1ti8SGTstZqwp8cgd9XV5Ywjmziaszm6VSSZrNvllXCTZu
PtiVWhcy/HoKsEbe8nHwYbd2MVfZp0V3eCYDyxXQKJ2Wj8zasZ4Fxubm/y5ZDoJgQbZb7vPSylQM
nZa7N3u/9XjfsLYmIhHJIn7bEWyk5V8RXCOnVxNtfoV6gfjN+ZZPb47yv5xUV4MrZX5guuGe2vz4
Z6ZBPzrLTZdXzocluqtUv/wai/Tq7Id//v0reoLoS4tu4clX38eja+rsax3B9bmVDrpoKLNmstFv
FnwhzNJbOprhrFZICg/FRcMbvMaTDQi4e021G2Odp6uAZroPakeX0vcMZtTrF7/GvhLC1uFIl5O5
v//S283BTmX8EazRNS55B0fA+e7c47KKo7nswBjbPKKb1oTKwdx1uQVgu3xKCu0HEUW05DZyaZHP
6hZaGxkE+M4VRPVfwIP7c4hz2qBqYVNddr9bOYLkRi6U5jJ/zvUwjWa8obNatcEdxh8+HWqxS7lR
8L111rSc87IrNap0BGFKKbc4vN5ENdagnIzXY2FiNPN6MF9F9L8K8D/86qY+jWTqQ3dKRztWYrxW
vXoaP2QXRqH3egYYYmZLAhTn8nkffBjsmOURIk9XkaL8Map5eHeAE5RKQ8woFmg3fzWbfD2DrrrE
hpfYGvAhnNYzVUUguoMK6pEtkfStzeGnus2NdM7+PstkKaEOvHWsFyvQzNl7qmqb93R8fE+cJFs5
ViMOotWjTToCdSFwvD53S3NfLPD2bUtERm0WhZHSHPMomizDmld+GoVQbNvwBcBIDJfCGYYlHK81
Fdsun1jDYZChHdk92KyHbcpUolAEyoRgdv0Yk3oov87FLZJH/o2jII0ol2NfJW4hXWQtIi3SdKqx
dGjvPL6Du1GOfPgWWtgH4bGt/lXcTkBRTJ8KSjzI/dj9MwwKjFsghDcPDFdtL5G5NSIEQqi9NvRw
9GN64V56mvCDpLug6Mzi4YF9gv+qmgIvF6YmqVmZQdJ+EzDG54abFcJH4/LiMw+rw0m29NDGNrzx
fDgIyuHKb9D0MKHXW2VxhbLQ7P6Nc06MD6r6zZtVJGgP65wdBxuxiyKizLrq2VaG7lxCN5FFyxtk
LXDBndffMYGe4NmO4GEGl12dunJ+surkCBOrODS1bLicuUxuKkYLEMzriKY2TJ4szseXafh1AWAn
UzjIhsdVcz9+MH9/7MaTVEIoLmrSHQQp0Pk001aSaMYo3Fu9i3CLRFEzgx0J18MJGhXit8IxFWuy
atcl1d5Oex0mo0IkqdboW4RVstChWW5FSWsPC8FBP9xE3ggKAnqabKF/y1ZH1wRITyjcfcPyjXr8
qLr+cvUxb8ghKhFZsiLrd3CH1KWNiFlthEEM7RDXMh01eXoKmfKk3EXnhB+zr0ks8sxKvq1mFx7c
3MVD1fzQ7Avk9VUjB0ho4t+HS+TORgoyZfqYRhD0ehewvi1HqGsfjSr5WjxSxHRVbdVefGlcRWEI
b9lUdPNFyEqjegyHRsNFrZEqa/ZOA/52WCMxAPRdc3e9UJW80Cif4YmIUjQHDCWDcbKCa994UDLw
JgK2ZQTUdfBnoEsKal8Hz2IH+DikSOEMAwdCqwSPAd9U3AibroK5pLPMdU7oADc1qEvmXkNgIAvd
/uRD11HqA2VNpIoPM+Nbf7frcw/H3QiM775Ng1pN5BN6QATgHX5n4odUWfo7GgHmwIZOXEXGC4je
CvIyVVhxViJ1N9UDXEpna3x0tnS8Juta5RAL5cfYwnbps6tsVmNP5RxntRQVCR7Mnt7/DTTp1dMt
bOGOD0xgTpbXaBrucfXqBhTzUL6NGjtgqDSyrmHXKysxZnzURB2IG62wPuetxdtAJyvqBp9MdNxv
ZKU9BKBwVOu+8gct2CQgjgKj7f6/Ko3mCvO7RrEHs9KlsgJONwz/pqJVfSZ6g7/oKf0RBKv58M6y
Ke0iZ7UNL3qDJ2TGjhKkz2uDAQT+aFU4P6pso5hj7wpf/2KCdeTr0GkIjOM2pvACAGPcRkLfhbw/
FDicJrUpd8E4WMA4q7qLobB37FxsDV9AoAijpf3WFdLoT4CK1kbShJGMUwWHheiJrpSfiawNZe3o
AegkovHbhVcd2wQKo2dDXW4Tcb2VVBIni/Gbkjx7ramUyIzYtWdWYQ5enq16l5K2p2st0zwq5eZr
+KUVzXqkhWcK9yIUmVJHQfqopcKK01wtOUnr4CSvA5uCNi4mNcoUiG5ngYy0Ovf6MqHQWNpM+/Pe
CdYZaGAidS0AKxzA3ZS6t6Z9svYuGRIYDYA1+T5vQie+rxBaYPb0erLFDm3mhRWk4RiD8pVWYGzX
Ay4pUjqsgSLtDuOlzfdMewfBFqGH48GHgWgpfa4lIgiYdcyQRqel8GieQri6gUVhm+tHOcqe0vkR
cVjDEfZcbTVXPpyC5VF7qrpAtplCpskdBV8jkl7ceGK8+KZPBlcYn82rJyzw9+gwMhsK2uh3iVPU
9HLhXq55ZqLa6W6PRufavjurry7l3c0AUyoilXRvhwfgxVIQ/yfuaoc9pfkn7RJ4bA46yoFLeLlQ
FL+5hn+MGxoNa6tmu00z4dfGhREUk5DQH+a8CII1N1keDRsK3oXhcRxLwSUF+XAH1Ng+e5GtBhrJ
Ghu8ErudYlPsseDLG88CqZ+OWPsMWUbR1IkoScW//3F2/8PuxvFIYzbVh/wRtPpXlwAR7ghZJ3La
eU0Z17Lfp7ZvniaUEhH2ZSF4ecn1HTCHX8VvT+1IiPlAZR1Tk+n1nTRCVCRFRYIinE543B//SBfK
pLvUqPL2eMM3TTodlu2TRsvC3oLtuV+Bsw9uZPPU2GKUQ0V2pm4uWNmay3U1HSuaUi0jQTM/PVKw
MUM6ZhFIfZc5Xj9ug3Wfq2Q0stkhQoOpgEz8gEnlrbTPDqZ//XJ4OjBkOU4SaJ2wvL03kwBZR27O
Kalg1CRGrhZ38yXvM/eoe+WzIhw+0VGG8xZ+Uyjv7BKhho/HpLv+HehBC/V4ypPyxzJnH9ZcGqMy
pI1SkVrw5GJBP8Qbzh3rs8/RsiTGaMJB65hV/Db6w0ehlk2go6x7duldwWTfO2dywF3Vi4wMP0be
cYKmB0FTT/ZbU5LFixv6S66ZAmfHYzIXO11jC8KSGUTozgfJIHzM40AJniQBuAdCemgqe0mbvgul
Lm3U3HLFL2hWMSFeH59C9zDZFCJ5I2eb9qVQ0oQvqj64CeWQYKmD8jy0i0UHglf4vDfdH0s7+4o+
ohEP7QD66Spgj7QBynGUAWTh6DmYR/Pmo8w5O+pEu7IUwCNESgG2aATPAab6+xQMA8YlaZjYnbjC
y+0T8i7aaFPftzCrLPQ/5ZfOKNlj9F0HpWMUS+/Ot/8u9U2oCiPrFDjFeCwRro3gBnjZ4vgG6fLY
nWrGwQ/R1uLBbXNGisGSRsgn/rSAB3O0nTkHuaMF3+DvaJmB2pE7u8NxMpA8YeTMbc0tduMD5Znf
tmmKwrkEmk076320SmAeQ1VJ3WRiFhvfoilc8IGLQJqeis6mcGIjEgMMf7U3Gba3asFdtJIRP23O
a51SPHhrRJpkGREMkmyHeQZU8u+VvWJ+4y9njzklMXWa3GN39QJ8if0/QafHXIhMMu4BjQhYe5HW
KAuoS7rYzqBdpF9DvZoCCRNu/0Lo5Rj/2yxBJdwOKNuka3is21+6P67wBls+7czLWFXSkMo3gAWS
oTROH8sNh4vlz9nT/rKCQwRsR6kHNetqkQ4T5fZWLQ9VeZG2edcR7Fnefk59G1FW5miwlwa8rRiR
EpLIVqd8p4TRa4Rm1B/PHbagYk2sV3j6ZROCNVVzFVnDhtz0zv5foBoflrYLyGsSN8fT62grsGkW
oijW37DGiHFWdVlj7npXvrRk80a/l+L9+2bJnUs745Qq9TWXKnnwbyHUDxDR04Q46hMLroM8xW16
3i9AewSXlmrElNtMWJtASLDIcxZjLZpEpzd5vz/g3Bu/yW1Zhe24TCgJH0RA0yVA2kbPHGTd8GMa
L2Abf/7smAK9lS06+vq0QpOBtvzFFY6TUUjM/xGinHHRkL0LKqCEnLY/O7p/TTOXY+QjIJaBck/1
15MZUDiNSAZ+vlj22hnkzEOyPMCCP4np9PTSN5gg2TYhyIDmYdIsve6SZiFC6h309VtYW2pWz2QW
TqXNDXY3Qqokz/OVWhYmxp2ADWhNNDhkpDrSNJzPkWi+/LGyte0LGCxqq9rirad+Rk6Fi2L8ZIk0
6jBqQj5ztcbdM1kvOL2U4BzcrH6vDnBcGCqryDzhGjoaZZ3IpYN9VEL9hgqg4HjKhiEqd5FsQO4b
QfEvTVFmJXkZmq3ptNfiobtIAGaFBAgFc07zpshwcL1vqRwkgLAN3SIT1omEdmzs6ID/W2COMiNM
hz9AoRUAScrJdHXYcdywF5se6Uo1QKCcRY5ZG4NjQPryco5fmoQgEOzDAPlTx3y1p5RAXWeKDUM6
Lv1gktugeQNhknHh4Pzp4MnH1azL1p1L3s59LRSy/NIWz/DX9YpvTFjF+DmE0mcZBUnZEAFJAeY7
8NHHoB/Ydfed/ti3VmSoIUaNVLEbnj6Eg9AksE5FPxiUfLB83yZxMCs+3MK24kuBkSSQsabZ3xCL
Ze6PyRJ0Va6ewxIN5veUpI3geXyM2/h93QwxlvsjVdQHsnGMTZvbvQYO5riQrgfQzEAxovGvRuPO
dJPvUTBuHkL7f9+qIVHXKgXthYDyaNNTTgIOJfW4tDLk9Oa8C6JF/dmiBVdwiL7l5w6ZEqI4dpho
pz6k3e2oau6vp2Wod/2p2WKMs0tONV6Mkfz3oC/1n49m8LLgYaclsKOwkleZuMWWP/i6aw73i2PD
oQgUvwOWEF8KOy31f4loTLHFuwh+1bRnVA5MRrMP419YQ8eOSmXF5sQRsjZKKn+12Vgl2C10ZZ/G
psH0OCkn7E6XpqxoRz5+VkQPT5Z70CO3iF7dbl/nghRIzn0qFSNUZMI91WyDfuhBxJGaDd/RST7Q
1fymWe01ffbenhea/M/F93GkOrA/pSUd5Kj+LsXZ5ugEQoLjN7KsWXD2qTcZnor46l261ZoE5jKg
JfiMf3Y+kZf1HJb+tFLz8zRkkJxa3uU7TxVFjNvsCLkQEYkBgL/e8Sis+nCO5COW25XuawTEv8QA
5R81iBLHCrazKneTOXOqOpeM6o87ty9/WH4iMJOV5Eve6vk1UvUvKmNnn/gB6mzMlKWIG0IqeqeH
R8zFxJ5nWRMV09tlFTlteYdCyPt41/vKl6H0rvb1xX9KgdnCmePR5uMu1FzlI2P7CAn2O3GExRIE
4kMLXtd862icZKhfnnBq/rouZS/Hqge3b0Ymy+F/30C5upHqwpvpBv3gMZbw88QnNFW5a9EMAmCY
cYn8efpOM0LoWyvSQYVR+7XVF/ks3CZayXftEZuFdO7iXyBuHs6vZJxlkuMqBiGgwCyZCcpExH6n
8ZFdHIkBMVXQHdTE6bDaILjwqx5MHFZj9Vbc2pUCXOj+qxzcO7fQ6EW6Xx1GFMUiyCKJ14bfQKHn
hmz6D+MAh2+NoCwj0IZTdcGi2MkTgVoBAjCllGrD9IjXItQCIUew/9oVSOXZbwi1QtotBUt5DFTX
F1XEsv2QX5GHrDbdhABWoAU/mjakEu3nKTzizmnMqKa5jX6qEuksrc4Xj8kp4VEg6zqd70yLUpkj
EVplEE/KT1pObEunn/zpgpylEsfw6RaA8awN/Bnat/9HPDlLJzd6i/HddlzwUnAIXfclj7sqqFlc
BXIa1rR6UtqTJbSfw9a68NOa6/B/TvDR0TdWZJYLbDYKNSSaZqmqKEi91pobMDgQoH2P80bNLO3D
Zjx7Msjk6kN4cf74clZOeq0M3z5sDxmdaJQ76ZMSpah3hwtqOTJQLckB6I9TVaWKSWXntOJrNwpd
6JSS5SHlscVJCzf2bRzshTh2RIYnxbcfRQ4KPY0IzAAHnSzhpuNvqbJQR4hI+5ytkupxP2ecHvHv
Z1m1PKA0PuLPaIie89c4i6TlXyvcraaVUYv8mBAWxZMR1ISsEh20VLG4lNWwoLpTTZD3SozcWkZO
bKczFqsFa7iKAV9D5VQJ2Vus+fyQAQl370QR1mW8zW8uMLUGauGbHh4RtnawKRMov/QmlqyoN0kK
TWV3wUkUom9qV2FjRWl7x73Hrldnf0smICjQqHzXSBIoiuGrs/iXx7BT+eMB7XwRdUKsvQd/uDv/
13leNfPH400ShrCY/sOTCrfp+o8/QmK8IPiB4eJodn9aoJwXxR+Z7wyOTHnL1MjggC0UyWTrjuAc
Lp/WuvSEbjptQb5rb2tTRs/zYG+tdI5YQoGTMtNH+MOgmbXMDacJdCfzyf1P0M0QKv8IIU/IV2ya
U2UGOtaaiO2Rpd/heFC9g9NRk9b99ZUuFe3zq5z+J+Pwsy/BdiVLjk67YMMobcNVZxIYJZA5oqBp
TRJeYwojiZJBdU01DpraZurjENxQewDjcJ1IFTKkAOPwrZ62bli4T20z7W2jiOo6LkykmoA7guoR
28SnvBoQJj42v7du5umOolsQLCFVFZ/j9oVG34nQPXuMHfhqRAVwAhUiRBilMsDwqpyur+AyzQCE
ufhicsWyLXsd9jI/22rU06leqypWK9AQ64uGA5GphkrKyF1xjfnXr/vmS0r0z9Hql4HZA94lXkW5
iYlA6gKso0wtreK5TpmolSYm3ey4QHsTK/8BV1u9oKgZAhUUp6HENxuAmZcVajMSPx5VrbWKJBEJ
J014JgeZBWVwMHMBqyQ/axGYFXnpqJIgqyUNRQYYyyDoclYpljNMd3FEFqLooaL3ozLlDSG+Pm82
5mg5sv/m8jczxSNGvgjlHMAsdc8ksylhXsD2AIBArGM6CJ/15U2oe9xsAK4RYNCfLMOUzSeE6Zkr
zBjivMifnjy3/lqaUh8ZbjlTwuGz6+6N7niz5L/FBlapZvncG9fkmGgMOpCs1MVPuS6rFG7+aY/+
UuCD+xsFiVy3dRX2uKtATKtQMhwfjWUoDUI5E7DPqFpNFwNFqCTjQV94OZj7bLdQJwXdUvbFu6Jt
4UqCpA8le6G+OajGm9vuWTXNLsvbnhJsH5wp3lZencs04rUocUulEs2Kwwo1m30LI+593KYibTPM
YuUlXRZWbLX3dvAo3nld4mjXm611xdC3c6doQZR+ZYKx4bDYg07P7LNBFwJ+6rniCr0hJ1thVEBP
n5XGzznsQ7QMoyYAImDdEJzjQIJuDZobqLTk7CCKe9DKD2sGRLtyYsc6cOhaNMaF1PHObziregYi
5L/qZQHuMCmAYRuWBgEJ1Hvu3U9Nw9iwuPqFy2Gk3fSiPLdEWgJuBQb+xTitPUNCEoNw4tfIyhTF
nGknUbwJMYsAYCGmwJPE5Vs96mDD+v0TZliXa4FX0mgRMXA0/jlAMwTMhph9COHTqs+QCtGE1hxl
o8BE98c7wdXjtV9aKR0fH2nxtU6fKa6JjZCZwvbL9Uehi/1L4FYn5BVDZHDgyWh6YeYM1GpwcV3Y
JF228phDCJM27XQQP/ELSli+UnN1WouIOXm8tOY/tHzB5c4x4L4aO1QP4y3VPuZvnvUzx6lv1xlk
6lTcQ9t0WLkSVYckIkuz0pRkMMGMuXZ/HhXLNxpJ2JwFrw5VOvSwdu4srDzVN7XfvFTTNrxj7Y3V
iMpABB/9MDrQlIS0FXGID8DpbW87XdNH4C/tFBtkcUyJpO4810Y9lsBNuZGJIfhi0lF6CU3Ue5G9
bN5TLe1y/Xc7rph0W5b7CllLaCYvwJPoNU3SOWF2NxwmDiTGcPpQzG+8CXtneQ6aekDlKfY37O3u
4c6MqjmShvqQtDI6u4P0tPg4lysnA1Ko9QA5qPRhGYtJwjjc9UIEx3pDvUIATE+NpXGbELrg/ag/
UDec/k9n17F1AJyKUXxVx2ERjVBSWo8I+UmOUbOI0FOpSM8JedwL+PMW3J9/tT2epjI2ijH1w/kW
HnylCuvxy1PDWWCKXpkHFhIrTu/axp6WU6krQhyN0lAVQpI2F3YZDQ0RScc/QX185iIJCR/CHvHg
RK/WSJ/0FEXR2X7zKBOPhG81nMkaNYhqiVSHDCve1JjP+1zyQjkA/zG5SRY3Vl6iLm3u1RfoKfBX
aQMBiz4rlSLzgB0sPlj1Ylu2C10zAfRBIewE5Ay361amoPByHkBWHOBvzHqUOSGW5QBpqzJhOy2q
F2j4ZjZFSO7W15r/ucGg1DL4Pb9sfCYtUdlUnhCY1epUGn6SR7C105W39a737NhAhp/BuGg5VD3j
EIJ3omXc1P7+ZCZs/6JP7VaFFeM+Caz06Ynty2L/ZpHGDdB0MzdcOW7PgvW+yFp3lYolxXmcQ3aK
OoI3LGaSlg4i1yHpt41O/rFiidawaNtfdYMdwvaX5i7prV15xzVh9Y/lqVm14SpCjFYQXg2J0XoT
bgBRyyHGAnOyII4R+tA//JFhhF4tmN0ULCeQGjKU/LodZ5aKIQqVqkCYXpxsvaZTfWXyUVVRMUr2
mbjjSaukzjniS6Mf73htFlj7SkrlD5SIf4cTicA/seKY2Rmnp2JtQNXTrufHiK+jD6NqgoRl7M9m
SZ0YKMmUDIam/jZ9h0asm+KWulOxGNoHBbnmKUxyoYpELVxl0TnsHAxgeJrzD/DbzVAN3PlsuJ/O
xePoDWCYU1rO1FmOQlU6Q7DPQx5VrYvtRkik1AQ1oodR8+mG7hFS92N7sEIh+HbRckME63Qgk2bG
SjL1n0uAYIR3ZYxPmP6eBRehW2WcmjVdJ9h87YXTq4O2OJgAEohBCvf7SGy39uLQNadOSfXo9FoA
lqA1XFBVrs4v8v6eG/wONA/tSZWzB5u8rnDx13RlpWy3mB+Ltb7vsEJWZKndmBRT0+KdQob8hnck
eE70FuMvQbV9LtIz8E1ziYUpj0BaN8DLLzRKE3Hg6NTb7+//aZVFM7ZjyYqri3pLuuowXyd/qvTS
REDcGSgSRw+WnojSW9hY65fdVOZqYBvhR/IECSeevmqf1sYqhz8klh8PQBqMYpqLiezQico6Myj5
6AmPTV82Nk6HmXroitlHPgijn8gm1XEApcNK791rz5XmqwIxg2OIYC52HPgBI51//7N0q3EedwMC
fOxq2maRnmhmGdumwafhtmGbj2xUBobOoi3TvD0HWMJ3TOUA4RLL31klgWMOcNy4KDLP9BGjgZpU
2C2M/bfwo5s9m+EpI3z4BaEYDkOdyXjvwFdM7DZDqr/J6AIqENwKxbRD6x9NbXGKu98qHbJ99NnX
tcSYosQAhUHWRO20xEbvSIi9iXZUazmNqYXHuUyfBq+6Ovtm9xjboQaTwASaJMPqtBNcMlHdmhap
mEiaK7W9U+9pXf05VqUJvxL8a8Kr/SCFLuhgv/3mrahAWBmcTJvSspXSYYW8NUQT2hXD9GaIoa2F
hTMLKpR+kmXDjsqLvrzSBYIDezRo45fWPgmLqNZglx4vfiJ5D4xVYWfiFy0L1LHEj/XqkSKf1il3
sLZYcUnnV5/rZq5/m81GwFscKooG9SrMAAFF0UrvOdcV38NoF4nS1V2bRX76icceVZSq9uWohZUS
LshHqEPc8D23hv2pHE1gbVlTEQk8ESnr/1aDcsfEWRwnerTOHEc6SH41sXiktpX5YAhQ0wkxD2ZR
83nKvMi+vCv4WEs/0uR4ie96qeXMv6jGijWTEJTNu2VVKmRXEddW2dG/MX12JHiGKovS1iM9+/GU
Gp9NmtmxgggJC7QVl1JN7tVWLnyvILP0HmxsTx4nwIWDlLuDkgn30c6e4/F8tOtSGuBNxkCxRj6J
Xw6fayN8bYI5vLPXp8/RE2qOKYlISdC7mW2iXliOmb+kaHzKHcE5daZY1TGquyCjlRbZgqhDjsy5
PZ4dwVh1/ttAJ+3RdlsqFyHk0c/5554KqxdusjPMZ7DuzZwd1YAuHBdh9cJZNof76lZU7UHDIEFg
cJ1QfMPPN/zH/uBcVi7WybA1Zzw9KQtB2ONc3NpId4ZQzqs0fKuptGgg0a5UUmLcoS56/zzRJhRs
1dzQ170MPeigEt5HEDzGHjuqgye2ujQHEmUWKAVv+OPrLOPDhPPHIHKIOGM4OnM2lkMu1vkQKA8I
VHSY5go7VjS99lqwycvPC3JYC7Og7onTQWViYlCw//gj8MAxk1qJVLeWaVgTUTn+z2PdsZpaULH1
HDAr4+5GH9uEMa1hIjmKIO52zAFWq0yh+y2IIHaaOk2Li5Phfn0JDyMTsYE/kGCfZWXYFA9KdL/Q
vdpF7N70WP57zqh2vxw9+I0etw5qcgXxz6D7+Ew2fVsWdR+EpSMOXA7lqo6OrR6JoJZt64wNBqzS
Ol7ow3NuLNQTYAC4T8MnJ5+7UItm6ngd+vR0onuR4GGxgMmlqQTmN+R7tiAseLH2D9UfCY0WbSqm
pnd326QEy6sXMkWhEoSBMQ5pDyj3shJ0k3CjvOAKrx4G4fSnyz0ESQGhgNqrNzW8qRLUNggxE7od
1KqzqNuYq9GMakUGcPLHnk113iuJUIoUVoyRp3ZSiWBUtOhIkrGIQBamLBGoxWdQuBu+9GnieS7N
SNs0RiL6d76+Q3g7SfQIX0gIjgSu9rfJvu5ybb9/ibYDkoWp+bMeuMk8sMWU42Pv5umcvWknADFD
kL9PnrVWl+heTHhbGy96Tc5nNdpHrop7jvCfDNF3GPqS5ok/w4usjN7fR3XV60FvhuWXssjPye/s
77y2WApJwdQxv2TGZP2tM1MDOwjWcTkSdYPhojrtLaRI1Z2aE+rc3F6aU5OUUkGbZN+/xMkkf9oy
z6TWoS8afXr5PtO2Ha4zCV8WVHKzaDHBnECTkAW/G+/phfJWx0B9KPLY63lzn27Zx4L4y1j2Po9h
I+arnVb3dwIc2OPT7PL1Sz+9pszyZm/pbexT2KGe1aJMha83iAcyG7fCU97zKnPOaKueBwSRyiq1
AxafFPsAGX2HGktCTXqhgGVSSu2uGbZWHAJkkCGAgJnYRRdoCbDy1lmFuHKeeI6EG7hyBkpYMnMj
6coR4YPas2r7k6TaK2QiJaPgOof40eFsHpQkFwBN24hpkTLp/mNo/LJEB4cyHNxGibQ3878Bnar4
rQtVte2pZrzsuG3vmyPc27AIGhmRFXaBmPGugCPqWH72Ht0CZ1SV5iBQBySTU5v3TKhKuy3xM9iU
CKU6LLlrsnISjHyu2RKO6/8rLR5H2kBkehTHTIvUDTiy/IN08mIGtsmwhJFq1cubGuSVu5LHuYzr
km0PGkkTmaB9YkNerYmuBfDHRVnZNJn1Rd1BAn647YpwTMtq5o0Nv05eQiHf3Y5dDFGiuDK6JSEA
e/gNVXIDwEW5tUSerqm728QBPXkrCBRq8VDXVRoq8Ae3JchH3cF/dVQcRcerh46KmXs++Uzygm9H
Cv7q9Hww2kKuYBwq1qWS7b7eYZRXvfcgWPachEOXRgHEXK6Nr/x4GupNNKl/8EuNtk/bzb38WV9X
oiF46JJScgk39f3aHi/0G8B/PR8rQudSUbyLYxjkbmCWZK1Dk7irM/kEDDK14TjVMgZ7Bf7TAX9n
o83Aqc3AW+6ZDbqE07XKVS+4MCScdrUVikBHHBRzTrTL5p74EBtlaPLG5W82xOj4YgThPxtmFGnh
6SPvJHB8SCu9zeAVjii9FzmPUNDjrxr3mQTNhV4JsCvMYDXN12wKSMu5pBXsWbw3KXzoiqE0kN9f
llTPhsHjD8OJGCUUYvw6U97gcgpRtxUc85Rhy80984JSUyw8cKl8Y5S9LV9D95IcmH411cXyRmdN
bZTyTu285GCIeLEXM9bhz+puayK288u/vnbhVkWMkSPefH9xK/PXVM2A50E7VjOaMdyGHk42JEtl
Eap6I3cm6cPepmNigmPs6JSLbmUa+zF5iXruCpSpiHaAhLDwjNAEJt31HlLODz/2xaJ5YXrlyZvK
ONYWc96tJ3VpiPU9zvG49vzGPqzU0Ai/69saHHL5q83BRSTjjvBu4zjMXMVZbggYKZRKO9hfbUhq
p0LiToY+mTw8P67c1/UcHEd+gHeQnd9OBtY1OwH/3O7i8BGQI8/G8boMaTtu7RrktzSD6s/RVkp1
wPscjX+uHSeJJtzsPvvOIn6/4QnaGE2jlyvkfwgQhlBNP/Mh+h+wIAVMVW7aAcpVlSLrmtVtuZPz
QHZBSQXuSWVKLl/IA6oAOF2my7wgG4sJJOpLJDckrJUhsQrqVKjGMnHgF15hMdhPC1GOC5gugoUp
q9gzdz2frQRDfJX49JeSe8t+aX7wdutkgNDhOnYaPKkqlpRjLxdcITNvcUFHRqLckVl6T6rJGHFa
wmKHZSCstbH32SP3qCYAEDLdCF1utbgS6q1XyiTnR7q2K5UJRcbyjeohCRyuXIt/VLUhEh8fvTix
iXeaReNyIHchwjYFTkXQi42cFn5EjImwt7iXfh/sjJkMniIijD2696yCqYe8moLiVTcZ0vOHpVB5
kAfihLkf9UnM/FoU0IblCvLaZUhe/NdlVZRJ76Ei8ubKCVp7aZdGamz3Hf9P+MpRUqpI7SuIgpSW
QDpr2PBxz2vS8qmTFg9UeCs4GzU6Hy/c7l+7WSw7UOB+KHDbb+95mUzf+zK/HV0oo2O9xgR5t5ll
3kfK5e+CCSIpueom7+g9uLNY8AFkhbra5ZF8J77lapPI9hswJvew0l9NnEPp4t7CpdqPSrG3tkKp
kc4DljuAr/CWUiG/x5PBQ8uQYAZXh/vlo8sqi/EmMwQ55emqHUe4cQ2xqWzS6BT3J9/uNNq4Pjgp
Nfal/dVoPkY65fbPpzj006oyc9lhUuMDotcGbtEWanPmw8xyjGaTLEiInXoRWFTBJs5AZESYeyKd
xRlkkRPhS7eQJFl+Gbd8ZsK2Z3RsG+kFTWDrthkpRK/ICkjl9gZQCG/LzQyowj7N7DLGownxXAGl
W+dgn4BXOxbaxOW3/CxfJYWfcPZengFkLGigf+TGDXApYHYeBRtiMfDe8UF4uV/SphL9+lLGa+T9
Ee3D96zHUfJ/MREczfTYutA7biD8Sow38CuMSfbhRTrCFQ2SFoSwKsLXXN73KQNAEM9ZVmk7AFyz
hE+lxXIAcYGgB7kxYx9HI7u9CJCYEqTu5QisalRQNR3LZ2YrWwNq2IO9JTl8izfUNUZp0drkTwCf
L3AAL05IXS/6Q5jrHk0Z5JbMHbiBjYzCmlK17fhfFoikBNdGaK5jSlrTpDqyGNPhW/MPb+8ZLEmB
iJz/uztUSiVbZjw/jN5V0r+UeUc31aY4zk3b5vFIHzrEvhDU+3B6v7YAjn+4H4/Ln21PR91w5TR+
RPmEHbhzLTOOP4igE5p/khxeeJpCSJh6wZBdXaVTaFtn8YZc5hhHAaRO1dUydTglCehk2E62Ci3T
3zAzPSLa9gKmpN/gZLrIxXdkHloyOtle/KA7KkQ2ozVkjMHcqYeTyXNCLc7lRA7001uj2CJ0RK8f
78ULaUBgbLrkXwhRAw4yceVnCDOSo4x4a3aKjOu/IoRvCcwvPQ3oKj0+FF5MASmbWhwDD54vk933
qVwPiiqnFADDRmJZgyYzyXonGEAlWkjBZduI5CosiUnXOxig8vzzLCCIVDEArp57AtnpRWZIS34Y
4RPXYAMtbEQe0PolNwAWeOAdB3opxP8lCc9+TSTljp/67MOX+Y8rOKoebyMfyilfhDoyP3zCYOrZ
ZGZrvnuxZzXloJFMvDyyCWCmYdfSu8C1FZV6iOwx82CgBAqqoar0Y7Cg74tII3ol3mVIupwT0jqd
NZ68MXi/xnMUd5RVoxU2Sh0OTx8bBIt4gSIVKiQyriEG8aPqlndckq0ukBG1WKZvHu++Se11wT6k
mV2drXff/XbttKaU5qLIcxNsTC38BXXrMKh4b8ILbrykc8483JZ302V7EwCLKQw5M9tnh05BsuNe
SfZO43pLuZkSKMq3BbGXuC4UD2qIJ8G4MJ5W5hD9YMgvUtPG/Qd0HbgJhmd8LuWilQoeA1n4/rAd
R6FBv2Ml0dP9hDg1d6OnbMy+utkDgA2gz7dGt/AspUttyysNjmxsevcFB1rfuGYzZ2ddz06eclpj
z2OLDt7rB1MHSEsS+Ba2ugtagAg35n9WjBdQgnhCzD/5aEGBIIuClB9BsMwSFB/w3fcNM3B0nYPN
bVVUvQ4YuNnutzAa8GqIxN3CW8KemIb4fgzLfiac/bVfpGlfSnnezxPrWrYlD4Vb3hJ4OW3tOB9i
R6/OZdjR+6q1aI2Zhdv7ZTsFgKS/YqUcyFbPMvvAocjfkhvekguGELapg75+r/GwQYgaoV8rvgXe
7CgOzWIvPXDUvp/+yqpPB24SIc/2aoADY2KQbRSOeIPIg1SAZZkfZO2H96vl3io5y5Sroc2LX6se
RXl1RaA1hOwvfQmTcsGylzdzbMfst7dIfYsmA4bf53kvixjlcXk50oHkUVXjtJEeHlkrHF1JwiBc
W2LidkAaFe1wW9he58RRC6skOhWg+pyG5RfdF2V6VeZzPGKKu4Xul7losse6AfG9fG7qhU/UThoH
Gmjv8Nt6byBbV/rTFiZf4wW/0C7qAHEHarsD+nBlPiW0V5iDOFjIKNgLOMuD51/BfY7vWgW3QIAa
KtHuFKS0Uc1aBZuCYnwC4ZMItlwffn2rkzA/mpQ2bdK50/Ls9uTjiMr+O3a3No18nNS45CT0kmDH
TA6xaj64OSlCMRYpI3nnRMIyzLQMhALQfB2H8CJ8/IIS5H5l5OPw0r7F3J1Rmhle+CyaBDlu77Yb
SdRFH1UI03AJ1AR89nbL7qepZ3v5KS7gTQxeDeAvPMVN/gl68HKch+DV4KVBNK0jPR7PjorH6uIm
e/+YucfLZ6KoNEne/mXy0VxXIDYpKMbwp7Z1D173StUdAC9ZqvsS7IKoZNtrk4RKm5dqRRNpIakr
+SGF8rqP7lCXT0C1MGxTCcmipSmTrcIi53TIU+aPFoib23WUgaIMmkyfRtN+53GWjyv8gxqBBXQG
QjYNQcOMbfTVlf2jjYUyhyq5kG4tF4b9LwjtU7YWNhICebdfJor7cfUS4ieTY9jq8d43es/UztCH
Nkw3XvK0SVi5jP3v5hhl5kmVGW3KroEB2M6zaXiibnUx1W7pa7wy8xRQdJ6IHuh/stLOkeG1BfuM
1F3Hrs51PjSsMK6qT9awFFrOjpY2BnkjpmlW/QltxMPW8NwngtH4Wlh7PXgfmRGncmjeXfwGWc/v
FZ1p3vRc+gX/GexRpm8p0M1PFxQLPDrS3cn7+GRUXEXmGAoMRu+mfhH2NZdO6oifLVOqXU22aTQ0
ifQj1giPFXfa+S19rrZTDe3PVtnm20tcwiJkk8E6JjJHUwbO25cFperryObAYfB7FYWtW4GAjqZx
K7sXJ3WUPCpGhKvmDg+XYmgLcJ0VUAg4hoBt5c0sbxvrPTR52lTZ3KYtyXttks8/fXY79xwu47LM
hPU//YtCcbah0eU0pLI8guz6EAvIH6lqj/hI3h2p+RfQ8iu1fCyKKmjpDPeghMPgdPE0ZctpsuFu
7Bifx3KVMLaKt6auZSIeLhjRnBSSw59YfVdaK/i8k/K+esxf92T9nY6OrPptpmy6mdjBNocN+ylS
gsIhMRc060ZHmQBT+GwlxYeceA6qtWsYIiMkzuUyrnyi7GoHOdMAWwwvIjooTVfekQX/Jlf8vuwe
JdlfiIdIaNoAHfAetHP2lupkv3yFvTXn11GLQxpm9AZyknf4YtgUGJAjNoFLjw3JmBwE1ARq4GSU
7rsU9Dk4puZ9IZrEzbzWp2A4HTWg0KVxnuf71Mz01MQ/qGX/u+l7hCB+F1YSHJwSf6bDo6uB76jg
zoxxayKpKi6KSl3RB3H6c3J00vMNJtDSnGgtO/fw8qhjp6nk436zbk0IQQoKqDO3YMN8BN0XlQUz
gXxkvXXG+rVwhJ8iqQg+OzaQv+3a0HPKj9Qxo2zCNMNwCue7BiO6dHWhrGD64djX/c8YB1E6BbFM
LvjDPFfRdmOageTevSDkDsfZR0Q01gGtCU3AnHrvYgsWxiexJSclitfdWvqjfsv6ZpHXNfs224y9
22C/MToMefW7kVqr8So3zy9v1lo20hPJx9+aeuIx0j+CHa3ZvgP4hqmdSjaJEIwWtPtbtPCCC1Zz
D+QFdvzMV7AlOzYsK7aHG/eUhQL8Qhja9j0JVp3iAuKlVnTsJveXF4d3K8IV3p/nVaSSVgXkUThP
x4Hkb4JE3qXprJRu5HhAl76ktnLDn9C2U++0P0/y+p6e2ZiUFySry1nVlHglM9Lx46N/XzxmRUWS
MMo4S/ke3iW0YxeCbE51B2gCyHjOLAQ57yEJ+ZmigxqqUNCfbm9pFxKUHwii/kkPB6ho5rBljUws
JUFjXbXfGbZgAVTPB2D7wAAfECPlmMUIY5x5NGZVh9ZPlshb1y+pB5+jza1kgCcoJdxsxUDGC9xO
pN9PICuHG2Z5/PXOrNKcoueOmGoyGjID6NqqL6C6wEbbCCcQlihBBHAEnLjsl8M4DEQkBcmdsMbH
R/MEVfcn0oki5BylKNm9tuR1MyUvcaMgY2Blw6DVuDM6b+MTQDKQ8ZnBT2HYkZ44t7ayl4V9bzfP
/Sr2XaZj7dpcXMfAIuVHMxogd8B4r8Jkv7SLOEppLDxFMEOjDqOve5rC2Kb1VL95kZh+noclcHUB
H33J6o74aeMuCJ7vWGThJLoOZ3IIgA8uAqatqc6eB5wr9wxOk+N0WGwkF7kh40kuY1KuLAhoGUuG
Osl64ZeDyLFN705+uZyza2rmF5JbeW0d+syT9+FNwubvVj6jpcN9aTt2mxBWgYo00ETU85tyLeJy
tMKKfHxU+CT7sIqsMWsY7DTV8f+L/Rq3zPLil5hsOAP+ZXRD6C8p7tEvmFIjwq9FpItsgNCtQwuJ
zolUnVWhNGEzYK2l8rjOH16RMeAa4XVQxIXokopN8ZPnPFjEm32Y0wqj7eU1Xeul+WnAGPP1WZAK
CAzgST/CEsXArzj21Xw7QnIzVp9kOu9P7jUi+d49LLqcEyq3UK8b6DBc/fFuaksNArMPcIcAi10G
cRd0EMz2dE7f7UUUirZOqDLlAr0dBROA3nEoSNZEWkwEi4tZSDOc2sck05704oeYaKewY+cKl9d+
gKeNxPaPFcHHxpnJZXIY/OFFsQhnDauas1aqD9+0j0mLhx8kdanWaNGimpAtNQOcZxwZ8tpf8xO0
TG9G5aGg90fEVL6u0tZMHP2FuAAT1qOX2KDfB5b8FD6bZWmMrKGAEyZl4A2jrctstwN6KAOu5AQn
hVznAI88jlVJ1nkIMmP0E4+HH0Zmde8q9ngClgmuJCxaGqWu1r89VwOeOhW03yl67mVbCTqbWSCX
H3HV5C1rH3a2L+AL9f597F6pqv06SeVEXQEgSQ277oDDVUkGWtSmqfH9brn1VyxkyjTvAJrNfqJh
DxGF8yrww+ZVlZ3QVkwxedkrk9gpQBWzGeJ4N2gCg9FWX78bPJCqiAdteyHnGktdd2Pm4yCIxGdn
avVzyZYYyZzl3qAjHfnCLODSoHYa7XssTvNRR1jPWsydscf4kQCrwWgl8ji/9EUNxHdusRlUKYNI
iw0m6cBujXq+QvI6+3Ba+adDFpKTjJeu/fNPCMtx4hJX16e0d2bDQ5bqd1jkeWnS7+ZV5PUVRmMq
kq8sWeKsgSrLSoKzivpSWNUOGw0qDPWs2Aclu6bSG+RGLhWepKkXq4xGxjeMybEfwroWgWxIZ3cs
fuDB5/3u68PgmqbdAz+4zrERJnt/iscaSG62BBwjSjqmE4vKgA6ezZ9ncTVfl2dNwXDWCSyl3QBH
MKMTdekkuozUB/jTbzIYTynHJOyJvwSnp1yNXE1Zu7QHHgN2vO+HMNbRLO2c4lxehCDBctUCBui7
K5+4AYaVVtFWZhWGho6uW5Mv/3f6NxnjmZMHAtbNPbpZgNM1FBQ7aEvXsubg45GRQKDqPZ3z1xJe
tirAE2pcUCfftXC3D7hh6UjX3pdgS8v1ceKlww019nVHssTXULAh1gzAUM/yM6XzhijdLiKPEjeR
OR5G4uKYny5PHC04DgP23avM3MvrwLZPUSsZhthwlGVaqBxzmitjnWISLkIVjep5xtaMa3iAnFw3
XRqxOVLdtVrTRLAZxNoIDbOO9AOPFVJX+Z+BNioAdx6GXrdcaSfE7TvNh5q2GU8A33JlPsft6ZCn
eDnQDk4pv3dsHrVDObhhcURwQtcU/uRAr0g2/BgJ0MNrxr8vYAi0xSIm57IHzNsr9qpeYJlSh5x5
0Xqq5VZdAGaTG2jIkgYvMnCLOie65RDO9tkWogGnlacu4iBOrLW5khhNF6S/abAXHmGQZYnm7xfE
vJWzhJHRYKj+BUvuviBeUXZkWTqDiPosnWzlSPQwH7361IsBk01sQq/CeXoCO1rsAwxHr+OE2vVU
OEF/G8QRSLNnKS3tfsBXd3GERurOV+T6eJCLC7M7Xtje5paL633XgHlSr9HhoMuNob9vifz7Stgv
ytkWBqR99OdVNjMoqnx0LhgMlnsqJ9oTdZjl3qPQUE4HLBo+vf0MNCyQb+vThSJWrGrAsJcziq8k
iIgcTgpa4XxrYw70Nxi/LKSKsjTckJ5Fs9f96aIOjmf93vSilL/hXTWMOd3qa41xJ6Xkbf0IEG2V
m6WwuSo0WpLSEI+J2TkQO0A5GIO7xPtZ2lNGT/GtIVVNgae+39Sh+nsJ4AwlY4uSGa/i/ez9kiT/
GMif8/txUF2S6W1skh39rQyKE0Uce/tuQrc33FHAKbjR+fiBnSzsghT1S9EcBAzcnxGuCYm9QYAD
Apb5J0NvP2EMGgz6QKETpPDFB4Tadm4LoRZOssOAH2ULfoQfltjZySU+ld/jXY3ck738hqQJ3aTH
4x01JJjKqkYrkp7OvwD8jChnfL9gVe2y0M3poS8SqmIGQcBJO3dt6m4zh6eTx2sUtfmMNzukdulE
y3TFVa3Z67HGq/ZR3rh0V43LDduYOhhzPWZqcXicb+F7vnDOZ6lizJWZzi3tTB6eO/ZcDnrBIQMK
T/xix65jjsNUxUaNOFKc/mE8tXOSi8HCPj3dZIkIzeLe5TvorGXXHMK0WXpFPXfN6NV2FbhZP12h
keyKNUeiPpETL5d6VMpwtxKtWkmLYvgwfC6Oqdeyv33BhHaFlR+TYVs9/zzqLpZGNColq4zc+Q0g
Dzlf64NOaINASsFp1Bgb3zQZi2xz+PD5WYh/gDXSaz6tMLB5S9gDN2PMhmOaSwoxznxpFtGt6ANg
dYP7BpS9Fpnfo+gdOhgZylYbqJqk8zTHmz9bM4iWQp65AYoDPx34HAQFNowsZaj0/4zSZ5I3gCqU
6pHMOlajrd6A/GjmSFBl0Ev+fTywP3Mm19gI0rAuP+PYNCYp9fvGSwSrBS5ujZ4fzViH8jMBptef
8LxJ0Ftn4DEb4XhDixnLU+PpYUNO/t/QdXsvRYLuWBRyPXsO0fYRxn6eK3cl8ZNrjowdaztMGKxb
3JChbJmP51PqAy3wRBPi1AzLLmcHtsc320T0pA+2GxOZ5A0oOrb4wgsaHzzmPkIAPCLaCEGfz3Bx
eTm2MIQo4ilBb3h8coPK5uc9SHIkTax6i0snDJYCWuw/hzVfe+t8ZKBvwTvT2qfWfM1AZiPJ7iW9
QqKHUuvxhBZvYe1/eAxpMaPUZcQHca3fSx8IcB6zFQpb6Iuu2/LzkAnTFcc03aVVGRy0nA18Snq5
o66MqM6r+ssTzevQaJCrz6C8haV/Acun6uhWQc1y44I6QlMAzgnXGsUM+c9vTPnvHDc2o53WMp1I
cPf9OPAaHQJFOT8oxOmCGnC6Dxb9uZ3M2KGP9mUUBHKdVSGXFFLwVlg7sWw3baq0MhcEF/CHJPwe
q/fzvj27opgMtZUHXrGZz1P0DKiER535ymZO55b8UGPI8tSG1Wbdr7zf4lnu8c2O1hO4I3ylKZol
YJfNqTE6EpTt0wmRSfNsnm2BcNBR9b94/IrDCsPjruCaxZmpyrKV9lsv2isUMCjTK2rTc34XTsi3
6JR0MNxDoRzo97p0ki1FezEfrg/BQepSrWXhlkRUGccKf/qEg/qi8YSa8nfio02P1UbRJ5n6UxZx
wMM8w4jQLeQhQc38si26IDMLSByzXxJfczqfS9fF7LMrA0DaTE0oPEz03Js8RZORb8rZtkxxhqXb
FDsjnJfX6nnGRGp2D0kWlg9vIYj3kkwxwdnMZoQ/AXHWQsxR5UsU/Ds1C8nldwJ1wds/q29cbrH6
ePn4/dsvwvB9kRhhUFtMBZWgiS0xKI+gT2bdyMQM3KOKB/IMsORP+4WQgG8MdFP7SWeD85p4KAdx
XYnZ9Xkfkw5eUYsm9CkZMuprMe51BQIFsmxUTtZP2A5mckRKzEVl3Mjqd7a4LsZquMnCf8ZkRYS4
mgbOa1FGFOWwxvKReLEalyQiwclfr+/yHmrNlzTImwkLFOAmQnLDLwhcve/bRv262PTLxDCEAT1q
fIWUTCHWW3KpJELZtIpvq1IWsfqJ7hAAaoXoqta6YvTbldXbBVmKuL/otO7qoVSIJYEYonjQKrVy
apPUM1BAKvE9apqk8ifLFwo8nLDQNQ1Wf3mA7M6jRBLzbVyd0A+pu+eW6JGApcBxoa0xofrSYMOm
FtmBL+qPnBKS0CcvTz5tZBvJHnn5LzkEjtkeoC3a1MZ1VMtCFRk8MUZkzaOhe1eY/vlP4/hhoavs
O6yRhho9YYhWwEVAD4qq/OVK8zhEaKrpgTNPBTQmOq7CKonZytqUdfdvDUrJSd5dAKbJWibe4Rma
Rpx5ySwUU9bhSSpJd5w3jWp1QQ6+Ft3Q+TNLquPfPDI/WtOKiCiaDoX0py59bGAd7+Vn01dVbVWn
VVGdOZtW6TqROnfrzxuT1uYGjVUj8tS5qruuBGwUDRUBmT3CDP760gZYEmIpF2xlcIG4/jpAX6Yt
kUZPPP0QqH/YCAXc0rZlrdmUfCY1DU53NAz/NbRV6gBADyH0YV1izGYuVIuWmjZoUpKQPlIeoaxr
gPhuh6ANeuYFMOWo7W5U0KgzZteHA8Sawnym0lS3pqdtY6Tp9FH9kd04HVgmZNARklzN401zGZVe
iJKphxhwDVE+wUPQNLduAH2HrbfWsNaPm2QwvKfoklf0vKnT9HlRz8YJpo4aIzvBnYhJGPVHZD8a
cnEEMscuOI4eVEmEnAeyXukhveoSsEoVrp/2RK3Wn810ui6mrqxDjW5cSL5GR6EnH+7oqPnTQ0ND
shquvwSKscy0BSmBU7tdr66/g/yIHLv7KE79UfmYqbwxyrbA2VC2w0s9Rb4Ols3c2kiXs+NOSArH
/z4/MwBs9HxD5hc2yQ1xyoNlXHZo+Kie6vr83r4nP90NThZ3Ksf+lf6bxuSwQaK+05X9n31ktbzR
MdDq+XFBlaO9YRTDxDzO9r2ux2ZKgruLpyWondPrSM6vsXa23vbpiyD90viDQ+/S7FACK3QQomFV
G2MKV7E2xmt7tlRoORC7CbKfmroWVBO9Iy9oUWjH/+lXnKkDAVnA0J9sHEQ5wG09B5LQUd3Asbyx
L2VzhbNJ4YgZJvU8IiSa1ApOBqIcWGgQ02/DFxQttyMpIYQSTxReknc5YjngKu44BjpcjQsnv4tK
hHQ9g62n4KTOwlL+oGTMCBY5C1mhCbA8/XVWQBcSifgou4qRo3kNg2ryPFxuDviRb1RNPXAfrZPu
XnoL3v232PzPb+P3YE2+w9yu1g1pgYnhYndh7nnkRZcJ9gU6eV+tZXYtmIbUJSemg9UavILVXzc1
kqn0P6G/rdMdYWQMNeG+aJs00k0/fl5fHvPk21Uz6FLblKV6Xv3ozt1NVbUg08HhPMvLJZ2vyu3M
lqwXHoUAJcjmDi/uDse7PdDSXZIkRjM4J7amNzNRxF5yaavpjNr41q49QvpsoOYndcYPSlOuOtHg
cM1AvN8JTGU3PxVBM0cWDn++7KeTLpFppEnjgwLAKVHXKZti5TJbmoB7Ufre0JZy2Ym2RyXCr4I+
TEwstzg9eWo0tAEd6qlnjQBGreBNIPdzImWMVUW9Fdj4Z6uF/XNZksnNXsCIl+39lQJdlhuWxpBR
nCEWMoGzt4ojQQe1xBqfEWX/j93DptISAKZyFpnt6ujUDIx99uyqJQ9jRHWhMGciYhiZDCaGbzzF
ff2eEEj/BqAWNRopK5uywUBZ5nmlP0+VaMFMI4+xDKWooKZJOZKkTVoDmBgOEhaqyYfq0Vvnnj1k
db92CHBvaXbZUC8EV2UXAjiiJ2meZ0cWcS+cq0677CkigC3wje0cufGwckft9arSuoHyBzdJXM5O
DPcMPWwNm9LhipoN/45mTxEaFK3gPK+FDU28FFxlYJb7+Sf1mfwd1ow9B6CgRN2BMOe099kN88Em
9zCTC5BgAK2m/yZW7nr+fDr81X7Na8Iuez6GIQ14VfpQpgOIRpxjdDszEpzNRyJrfGrRzjyAPfXv
OsLkZ9dX4tEjaB7ac7JnEs3PdX2+yACM95yj10fYqQ5nWpsrO/qK0Y/9IwrCtUCKVFK8XtfHmZvL
XpPBIWZeikHpRZwB2k/FKFit52FEbKUSNYTaY0Ww/2dzXNDx0m1ou6rGZTnzOM8N621ucu1FE6dO
6uFsocDZz/Jg6LWdfy74HcTglF/Gf45wLADYxZDQrw6TJ9eRWi8eawuGgByzZh4wsPuiy7BGii3e
4nXr9e5eevWIESvKn+5d4FrZW1BLAZAswmNhzCkGTloo3d9+SKhz1gBXoRtbpA8lclO/M8V4aBMi
lOjQ/OJjPx9lDsE87Q41YbELSJqK2d5Tmo8pv+2ABKYXN7s4/WS5xDf6mkA5JjGhC4HVCR5zn7nt
VabjYU0DDuDNgFohgH+iittAkmsEQaUEvd94avWCJj950QE1BWzD3TIidNY+rGoiq+GEj2fmYcqi
tiQfmBGUD6ME80tHh1D9vY/Cp6Pc0RXX+lAfZrqnJ6CrOme/4INjEo62nZhciS56mD5xmQ4OSunl
nMkPLv99ttHcqhQtN4QqFUSeqNaIG/u8oZABxnAoWPZ6/yqZ6luNOiTspLOiDbreblFOzt/G7MmX
k3i0jdmYxxlKgHMISLHpqXlgE1Gv++luahp/CcxLFszI5rDRDIir5rU9Ckub/wDuThD7aG7uqD39
TOm6uRqtaRivdZefwH+9gjGt24oXLRdaAunbRX51+8BEKt/v44T1sA3sROgqU6cHUk6l3Hqf/xNu
yPr+dag/a6xU1VDDovI3RkolAXDUuFkw82T34TYubRXhPO8H3DlYiOFT5hsuvbHruncUfA3ZXv4h
uwvT4qCJbNEX730FY8laffs+ElMUCtSuckq+Syvve6GdZR4cThGq99wF731+xnJtQG2fHqtmxq30
Kbu2XPmHED/3xINqoFwbiJFNT/KX4OHf0jSfr7Cm0+6D7JKrhmIX9YPGUX8WAY8UcfHP6xkHSeK8
W5lt01Mfehs/OngE0HTqClLwWmy4f3ZeA8qqXvO4wRm0kp+iV44SllYIwGgC4HJnAPIRnTyx6xvT
uN0Zi4UtdW1utcSol/uZEx7i2AIWSLZvjaTWuqWg/7B/fPbxtfQsJKJdmrkopLWG/vv5wWa4eNR5
0PXushG3KHEvB5P1gbQuWUpa35GF3qdALzwTLCb6EkHqMp1HuDVIvzRM8XZGYpyu8F0TslujyZP+
/QBC8FRVFHudqL8NRYFE23GC8giwJHQOG2lXBJWvfek0U5v1nDaLSH7Bk8Ubb/kXjLFst+d4kC4V
QYcotpcLTph81xNXuwbTPM95xvckFB0ZFZEvyy/z325Gb1g+2847+vqBxI6ywzmJ/YtSTiCzYfDN
d9N0JVRW8dwycqUq2LgD9c3a+Ym4YVHE1IxXgm17EqVU/TniQn+VxSS/DaYuzg1F8NYdXpQqfKX6
48gp39bofaCf0xNuLMd8rq7RLcKoWEVCLWpKmwx89RVeEREJcUR8ANzQbsH0Z6Vxpsgpy0TEytso
nnQ2F+zsLNQFTIcZRIuEvhpQFR94FWnxz5qUMgVUFHIY8TMPjfDW+ypOQoG9daHpTp4l3RcX6nAp
z4wn7O613M8JsJvIA7eTVnVVytiLc3ZSAYk7Fe/K1KYrWE+PlRZq9VkSi5K/Yom/zOcKmkVe/lMQ
W6zZ05FdObT6TNle1VAUGoXrfJVk9yTkgUFHnaRrfvN4CwiZ/zo5x2hmRKCAkhpwQnc286C4ah8Q
fwImfreT3QDWXrcHnUdhFGHBWxbWeIiLiOstshSKGUSBes7oAURMf9r1RT3sPIn/eQUTK46jeHfv
G+NsNZ+GTYMr19RDLdElX7O5dpi46DjvLoY/j+svmlweAlem9He5/6f+FNbcYdwx2rG6gYfpHloJ
WbkBI3jU0/CqtGJcK9QIFwH2IcYWzflhYzNnavY6kvwf+Z4nvaq7y9/BmqD4+jlIYxK/Wqf+1Uku
laZ44D+az9uldABx6VK6QkxwgT9rsn/uD+muDlye5aerV3YsR/6aeB/GBP5oTsXUj9eblRUo9N2a
skiHIQB48w1tI1uVn+7YnpJ1UXRf/Q1HQ8eeEJxRyiuMags5/NqanKSqEU46tac2Z3rAVk1ENjPP
DicgONA9PoObQzxTxRhhzILnzKVvQXGxAeh/6PVQmGbfsKjYEJxbHFkNSXEI7Yymyk8vmrzQefnk
HJ86vNLkx1UZteuseS/DR04i4Ov8sxHPkjDD2aq25Snl301MIhO7V4vB3tQxb/5pDoor7nLYUFgu
xgDcxVOnUhn85rJuO4AKNWvoOQ+ivRQIvPuHbGqT9EfGLUAZgyQDerIGiErwCyfnDQqMmYpf0a3x
VsMW28Qi3n9Qn2NOHUsPVv0shhok1KkdF/x6aFOJIFJtFo1Lm9Bt9ZwHNAvRtkBms95PSpTAsPa9
AfUmgKJwWYoYbF1x+ZenXrQJt7i9XOKNmZSfIttmeTAVUazKFEJPMdVYNYpS3oC9D340Qa/GHycb
h+aRjpPGBH/83upsZl91t1WAVQT7xdB89J1DzPi0XKgPM9x6J3UtyCOm7JStzGDC+ei62A79627i
ABw4P0zbuV6rr6B634dyt5JvyCLEm3LaOm8NlYp/FJqICWjHBRRgCQwZjTyCw3R7Z90anfQj6v6G
8qGT4IeKR7EZnSugCu6NIa8mSBgA11+Tfw2LplDSHoSlzAyspbKecmMPmqDydH9yNsqeFsAmEHE5
LeRZw+OzReQJY2XPqMD0gSSpnN3WUGneGxL2EIhGuZ51kA+tVsVrxaP7+1fG8UhwzFoNkfyPrOxS
tBAs5K8OukL8RrOAN73fMsNTfGs8+xxaoqzIVjJkVlQyzH1Bq2t3GXFMj7TwFXoKzzfV/WKY6UPJ
r+HUhLYpCoDXmsHzWvWPdAk3qT929V4aU5qQ4al2r5PRb+HsU1sFxUsKDcSOjzWJwAZhpwbjiOWW
nUf2oett+CPbd9crc9azA/PJpNuukzHYYJDNY4Hn/yTxcUXdtsEqEjxEGOCxPJXPuVtyIxu1FYbJ
bNLJXha41bxLKesveZn2tdV9UUx0Ju+nWEy2xF9EJW2SpjohSyW7NHmMTCg5I5nIU4NWRjcTMfIp
yn95bHZPhYfOBvZGLlqjkgf99S1M4HqvTUD7ZaDl52z1hTelubm2EpmwkRdPP34VhJ1l+iOK1tjQ
16uZh3uAAaiCroaEdxAHaTarQISQg2z6DTUU7Xe2LHOA9tLcuoUUQZ+5LkV+HD69vOazi/leaMu3
UxDMbG+hhQZGESpFTrO0bKoaXaHHBWAzz5Vra/qDb+/8sI3/LwTdCnQVwO+68gnPnAth1VbU9Hnx
mT0mN1d7NFulbun/849xXkkd6OHPQaxAjBCK3TWtZSW8Ko/wR3RTKsBUbG2jyYQVzDB5a77KjOkC
5zqvx5dUK2aWVbREDxdqCwCgJqH5+nal36ytppLFXxUB5D3N8lIJJBvvyG4UlvVsGQmIu1W/1Qrg
WfJJRpiQ2yuDtb2DyH9MjvjoxMQ3ZfOs/4+BwuShnYW0j3zgCih7S97KSfTk4/RkElq4gJ2vEYb9
NMBeYjx9a6NrlCcrELc4ay90OOczpJeeB26WgrnycudOcdQ0R+ou2QZ+7c7rk0ADQPkJ455AVe6n
EEvMTGtWZCdgh471XurKbXI7P61jdtqkOPAN8gPmMrwJpGd/BsHHZ56pddyTjuEFRd/vRvivx2Yh
OlPy/XDGREt7tijSTKhgZZ48nXtkBmCNMPDIoPd+JaKKAxxJm1TQrna7sZTMPpZQOVZ4FvCicWAH
+Y1Q1u432H+uRxSUMsWlrJfuzP0eNBuGXNH2ZM01NGyRLWpxf7VH3xtfnrD9eco1GYgLeIUgARL/
AJMFdpmlS36lvXRslHDGQ7D4B+fzSGYzwkFbE9ylF77T2KniDv1zVpZ+3Un2UtPxC5smqaBqOGx/
nQimXOSC//nHaC5+7FHLu4p5oT+77AIa6N8cwgU2XTztQDMoO6mHMuHUNBQBfXo0mBpy/QyWn+1+
z8VDKdCKJpe6yFIx4ROe505jeOrlacOEJkpqlxal5ewltFzXO3M2LFbZgeh/snCf2E7s+/ndVh8Q
gEGqkgmBLKeCoDwej7wSqTuWkWJzGZv6yrFznJxZIU/iNM7DYUU2ptmqAaAHKDnUbx+jojVm8Q1B
mAYUzXWDQSkhIirb9cYAAzWedRY7hGKWcK2dOLwdH06L9iHqX/zGB5OYtD/lHj1yEy0t8RK/JhDN
BrqPg/+sk8WcT1R/9hwN/wtc7nW/y2mA6zP8pCbDMhydxlmOTtbjnVcrVnaRRV+bFR+jSFeRhTKV
EIKazy7dx/MnvZB5po0aTy3nq4tT55T1eqlJWURf2MmRQiRz+hGRuvT5dPm1+H0gWJhUNB+fBCE7
bhNILMrhGaKCM/uI3K4KZZa2kwGvMT6zqbCWcxzDteuSGE6vJG4qPXWhnoU8fkBZQpbfkGlFz8m2
FlsU8FGx/Gx7tP3JQGzdSOXrEH5RP1SyzcHxtBqUC9IS/IOHOSSn3Pc2SZgOr/47Dzq11Za5Oi5H
GeGetMckILiARJZhvbXKvrn2OBhV2Mb97x63RMtJEA6sJXkYVTOoU+unrzlJ/s0uS5MUfQoNigdd
7aWs0h1BMPneN3nMrWTb94Ubs/q5/TKpBtN33r9xIOvnssxmzpS74PvYElQzNeuMFt+rhpVrREZm
LUaAUjSg83sIyY8cBNWsZmsA0AqRgsNSxNP7ul9PW9GOKm1l1nWo/AV7k8DB352dWP4pL5VbuAqs
qPojtnxoqN2gwmW0xoNk7I4ogYIIlYA8+j+DjLJzcitW1b3J19iN3vJU1xvAUpBKKy2/jn3gJGDN
wkkS6xCt+RdWqDwVzMC/6fTdiQip5/i0ZngCLz94FAC7STrxlg7vdfAvODNBhPhoxTT20tthiLec
aIbb7DhzREiR1OEtx+roiLyxoPkGWAlqHjYmgGdD5tQXdbwGrJ3AcvrHYjYSiDC3+Ga7Ei5pMTyJ
5BW0lVq5rVFMBksdiSp44QUF8RVJgVhkfrLmpv8D40AtDoeFlEprke3jG+O+wuyEtBexx4Humd43
ERlop3omZAecESDOJXII8ktXqPH+A/3SXw6ohMa17ytosr0OlA32BlgaWLgEPfhy9Nu4986asUTm
lVY/ZmzYEfjD0qT6AtyMlTErSUHmEgbzWsh2JNeT6r0aHlILLVPJeYe+BCLj6GU4aVBsFusibM1n
cVszzfYxn6p6WIll2pLJXGuMl2Ukd7ybkGkkNut1qX8gG1GR0I2O4IOT0hkdJgUTN7LprFokLEKG
Zh9ffwGroJjGLjKvrXgxgGS8IbnyzPX5v/xqHlk0T0rQX/rinLbpwgAVjH+ljGl5YzaSA3Po5fKe
ETLB75wneC1tCAlKhc+JyGrmq6AV/z1X8nxh5cgDisXFpKj8dxlJMAQFK4kF4Iyi4WDkvhMaTHQc
0zfi8nGcIVDfZoGAIwHsZZywRMH4XdYczrpD+H3bMoPJiJ7Hk3AsxmbbHpMFLKjK8OQYIn/MeOW5
gc9PgLi9R/d4F1pGoHb2XM83QTvyN/IOjxAZOV57JRLGD956DoRaNPaE03WXlzdE+8P95dXvRH8v
QFfvP9C4iWvJt515TcA43iLN5ljFof1TclPpb4NIP9VLQ/6S/M3gAGMmQjrWBFhU0nPKnSy1iPLV
qFxURQw0HhAHptt4a4dejD05tr+NYpHSEOA0+qzCM2otVNKr8Sc9Wf2PUYOYjXht+opDtwhiV2c9
z3wvdT5IyEJxsZFXP1ir8CMEp6jgCnE2FDzCalPt19kbAFS8jPRF/iJZDKbAwyXqFlnX8Ue/wFNB
zBECzSE8bKSE+dLZwq7eANDj1AUvuqConBNWjlKFtyCnU28xZjnfgfvomMlDhhAyioIpNwinXP+z
CM23UIuCZ5YFOoOq4QNF5CLsaoskWrNkqHUPpDYWGbCwqyC2j8cSwxQ/koFbKegEsSKeZt9vGweC
McsjWfmzC7VFYq1vvhMkDqaPPmj9PEnfKroNJR5Q8YncLz8/JS4uHYIfACW0CH52e6+GmpmYh6KZ
c4VHtYuzt8nIy6g23p+HYHKLQ1FQXOyUrxi0BZ0EShmQTSBwpiRFawr/MaAja+hUHd/vF145sfkO
GyI+innvab9aHPRl8cvj4ArLHuEvJnG5Q2EjUELhjiIAR1AKvPACGDk/PDzwxMnu648QdfvrRpGY
1wxBg/JAPCvOhItDl5s+tcxb1i5wIo8VaK6BijylEll9tkL9sT9UWmZSkNkQgVPS8T+UqRNuu0Nk
o36Jts3nzJ5GxhU7JwnJEHbFC1E+bvYVuIIlbafbmfjK5lIyu47ToQeF4FMubp2uh6U1cM7IPbaT
oGwXnz3m3qNhBzd6cZLSvi3gmHZYFN4VqAII1OFT//f3Ql2a4fyrSFi6ih5UzH3Qwf15I6FYsngL
6N/7Sta3GxLxMVeJwzRUmUCepKdebg5g0x46ipjHfQ2pyFoWTQuLuYtYcImKgRJ97K9Dj9GVdpes
xH1GBfNlZqowZtLX6IfwmxUQPQaaopf7BDz9DATrfzpyYcO9jQCrENXarFkYZzCFT1In0EQLU/ay
w/u4UEmZGUMSWgBonkYVnlvjPzVCjvpkynVsIS1/8Y7htWJAG9JA6w/RP61orrcNq7v6TTZy66NB
FzR13HCmzw5qgQ/q4yvVg/dyrRdJl9sMt0jay1lsohEGPwDgVFxrJUjmdMzGUcCQpmXk5PjMgUz1
swdNVHA8r21f7gDp0aWnDhYVj5BQUfhBCadn88k9J3xjybsmoE2MfCKQ+BTU9112DV6cPCMH1ESN
TjaVmmgZ22XIt8skZeDg7p0HlgQxxfgNAJKVoPU55tY2UJy5BpbNKVsfY4e/ajBCJCxxYALN7ch3
t2H4o04m8n/4TKTyCJgRDTJA54pe3Q5fPrLcJ3DeuCZ9qUnW6Ez3mJBzpnXYFKJBOuDxqRfU1A4B
PKnKAa8wDzeROn00rBVtXpVwYddeJannobUiRzrL2Eei0LW55p9ggxvShkShtsq179yWGvBy/yzy
U3QeZBhmBCik9bbRkKxUp4SyRLu2FV40ty0hMUdwVUA55p4kZG66JEsAhYnNM7t496K2iwhrDj37
NJItuGMaQAD0rt7FAe2QixQMx1irXoc55Qi+wVsLgnIENC+1oRo0lTvfcNsJJ0CaQ8vAW0dLMj2U
cBomMcHgLTT0hUsXulPfXZIkIeiDsyxg4535Saus6yGd7C8eqktB9r22pXiDvqYMC0MZnN5fnACt
B3qSoblhFngFJZ4r2IS11kA1gwopXN+G270G1sW/fc/jhsq5mDJ2vaCPwZtFRGVflqhAeztNe7dB
iC7hcpqYmSYd9/CSblA7jhV+9KFLGfnhYgzTrICmf+Ure0NxfPMhIrVvEVpqJOYZRybV/32Umgb3
w+ULMHUvfeUl9l8Vyf4m0KIFGr/z0j4fepDQTUJ9CwOj87EhqRmvzPfy0a1dUHgf6/jVXeahlZoT
RTbZiXgciKAueWbeq0njn9DZraocDn6JdNQ9jN4tVQCx5td8WUjwUNraobCnAcYJqW9uytgJwEVv
9f2JKVB/1feUJ7qrJ3JSNEOfa4bPaELZ/w3EkUEpeKGFCrnSoLRmUrTrCuAqI8g7C5zmYrZFTOzb
5GBg6EVN9uyH/1Ch75pDdJo7ebiy/u/jdSiF/0oqoeepG6kyy2U79dUAGKWmT0T5aOchaIWsRB7J
uliCh8FWsfXVqCfpMz2dNH1cJ5pX3v0o40YhHQV55Jnf1lDONTSYnFhs1tZvreZk+RqRAcEiBGkh
Tqh8mPEqiQjtGkBrDyPJkwzWrOszMYwouSGzWjIgitgy02d5pUCM+4IqMxmpZsHJTypgzpfzYxrZ
aHuicWwAOF7zet/93sGsYO4yhOWScg2WiTq+BvtPFbhH8lTK70b6QcfavNcxZE7VoGFoyyhoE2PR
/49ri0NBV+m11JSVBFqjjQ5HS+wB2DD+FtrCK7QzDeQj7ZEt/JIg3j3O3HewVO7bPreKji6CNZjs
ShdjkxqgKQGIc3eeK69/OcBq8J9ZWejjVsfeEHVUEPzCLS/VC1ecPy65s1KynSzMuA/TEyeBEzkc
Ec6FbYweLTRfM54gY/uy5ZVrvHfJAiOG1BcYEzqOMFDdJDpvJwBP4cBNttE9oRmR+kl9V9WrIJQP
DtpaHl0e4gf+P54C4xKNedgXmaFoUIdrNwpdTKS0cv2JVwiAxcKKHnfuI4ThQI3g+86vVvNy7Fef
70lM08Ibgmrv3FYyT+RC9hCG4uj4l6/gPZt+04fc4OZYZFVQWjtsleYJR+9mMzp+B3JPUiPOI3rB
+LGYLUF5dxjpdPM9D6BBz3IDN7Np156AVGnoiI5ecKNNbH740V5/6Aw13Sa6OHV0QYSFYRXWpnc7
/r8evXJCya++wjUqgPQGBmNm9MCmw2ISozUi01owDPmXte5TyW6g2sJro8ELgzrvkrCnDO9VPhxM
dcFirBB6En3Inpf8xtm9DmnqgTq4Je0IIJDxu59X84UtO3w3Wso5I0BEcu/EMI46wrJ+AsS0o/HT
tyLl29pvSQ3E0Bt/Hbe78aBkxFZ9HE0RQN8TIRcAN68OqfXFrjNwLEAvJRcWpOsNqMq5Z/6ImLhi
IPIvUPQJamMfh4AKpWqRf0XiQaIw8ySQ3LesXz+nsX0y23J3SO2P4Y5tgmR2GC0dq1Yvb1zegjQn
/3oTUOES9YCFN0Zubge2I0TF/DbU1KzOcvwpYjq8uvQRpPWrXOXDW7RBMkeEBvFMfxPOgjD+YpQz
R72LMb7kbxbj7LqbnWYmqrnxY1+nmKjaASGSYsUQj1cLrl+8tWclmb6CWu+E20dxenU4F8/PIF8q
ywMS2eAzbX2QDgCpAU3iRNlivINZrTZTyTmZ4Y6pxFVKNr51wObrWfniB+A7EfMNxOHAwRdjCEJ9
6qyUPqBqry/zwJRZZYe0MhRTV7lfc80d69wluHx7j/oiX+b/BYmP676+ctwwuh2UqtowD1aUP6KF
AQTes7rMvutWlyyIK4+/FK1v+TsmEwSxZtibJdULmRv6hAgAIpAxLHQtBJcC1JsHfEfo1JbM2dOR
uAUMSbrTmnscCL5OZSYBU1OaWVEhDIK2sGv00Y/+aNHh3anbGF61NqRWLfc4ycCA8UcmVFW7mkFE
oplPh+zWDS+IFcRVOSTPYrEaOg9p8Gm8rs7GT+r7Zb9x9wOTVK+zbw9Sfueq+tHp1f+cxPHlOVK9
beb2VMPUya5a4OQMmcLlPf61LrZEdLz9zV8BpzmK1Uf6AHQsSBHVWFJgT2iGn9viTCxObaoqAVYs
TRz53Pxd6Qj2DsPSgdCRNuo24q87FsOM8yJ2GEC84Pbbcpa3Tuq/Z0v3pgIyxRRc+7SfbE2y5KSz
DB2N/Zx9pa20k0va0CPJ7OZoWXaOhorb+OSJJhu1Oq31tbX49nrL3VX5s0mEtE55qpq1d+yTaEsO
tKJfG8Mvues3VYoQo4R212GGdJIfl2l6Z/jgzNp/hV8Qi4XaU0dA/7r0Mgxu4pJL5edfa8zsf+JK
zpG6uRnux192a+DUx2SO7ZcloF1aLo0mKtOaOfSgAgZGeEWdTvsNBgic3O5ffaGxLlp+0E3V1Vsj
SyWOa9wrN05rX96u6HWkskCOegP2we+MdV09BZr0T9ZiqDnFWR3sJcve7/M4Wp5uHpYhXwpUZzKQ
FT3vjseGk7jJ4lMXFfstANM/KDCwAahZIQSlW7gl9hpLU2ytNJMOIlPuqXPOJFeVWw5HnerlFaOs
owGgZqZIpUCmA26Os2ov2f4IUKtNHJTeD4TaFY+gHB4zgvgFWOg0VMFr11Jkpf3kbFX+a0uvDyrP
1HMfmYTdA6nSnGgXUjHr4ZIZvCjdxzA3ZEt9M7R/owCCrMcX4NwMyQunrThJvr1lzllfPVRiIm++
3HE1ucSVsbO5wZLv0wZjG+PS6AuaT7PQlKnBmoYdFImvGROXalvKLOYLG2su7c1S9n82qtTlIWI1
D+AkkGCMFYGqnJLaS8tGjrPRMB2LT3D9vd8gD9oU2urpaesIaazOEVIGIt7+nDX5WHTkSWy6nl/9
cnH55iSavUOXUShw/zNcMj68yB8bvYSmxf51Lz6Mi1a/iOPL5uTCUDlB851mksvc6jpmRRPCzNYz
RXshA2dyJdG9s0t6S9uXnZfG5mvpQjh8XLEp4i/rhQaz1KXhpMKj+sUiV7CguNLIPNmn4HqThjt7
UEVUIymYZ/aH78z35Ns/Zx0ea3C5SMXYd/86FSCX7GnUQk/BdUw5fqsN03+mSefMd5E/qJQMt9vH
TR4ubHMZYOY/Mc8Bcs6/TdmberzwGGaoSACov/hbRl3YxQKA7U57g8Vpp6QaO9w0TmwJ2ZzfV9T0
zQWVnXNb6RbNvNE7N1dFpI6QjwbKBb8BTXs8jlxB3Z6AaBIhhKnuvSS8IKaqXWInOZr2smkCsJtO
rDyC5nvewrWnmZoJ8Y/LH/YvMAH3hXg36BCEXsnQ2eLspgTlQASDUJFFIGtbXepIqgIEcITGrlM4
XQI3jSAaEI6dDEOhDD/tOZtdcu15Wfk2teUmcNk6EjB6pkJckxFglF71C9YgOo+1WWqCsS92GQxV
DgpV8hD+zjN1a5rHYmg7mOsPjmymzrw6YLLvPntPPUdJsLk9rIDrt3c4Dk0nllsm1oIpjaOwH2ca
tFtOlbTt4stGpibEhE04fqZyLFW+1oaRxCs9Ycmi8pohHEdpwppqBxdS603zLcl22Y4eXSvCYiKj
hQy+gmfK/UqrdEDvGF/PEo6T8lGmL2HBhZcFppBy2t/T72cQKJtkqeFgzCXt9p7IOVfjcyx9j9mN
5yIvKZLvogYK7vZqCt0K7eF32ivlypiAXACnFB2Fd3cR3bbdOmbK1S2uPLARNmC8ToIRAPITJd5n
AbckOIS5WXdbwoY5Azt/aUPAsaJTUE959CyucSSYlbaR2apRZK9qzJSYr8ss+pNcTWpqBHR9XlYk
EBubRMIUDnU6SFZZI43U4pd5hHDzMPnW8KsJ7ktH0EnzpknsqXXBGRius2wrs/+4asOYUUmAYSSJ
FOemeN87qvYguUjTFh3PRZAd/cq3iSLx4kuMVspUbCGIGUxqllm9FD8vFKAFJWed3CPkBsB9VbgD
jyLQbcBcGRxaRAuLDcpvbylO/CZ3eBUafRblHaUPTpxZCMKYXiZBgPr6OTeYF7fcJjmWOUWmbI9e
k1aqJQHeEcr8jBupKi+CFfxPh0k84fDLhucWsmsaHiPjbCCP1qqBwrt4eVgzpP1h3QfLsHSlOaCd
OVqvkHTDT/aNUmxC7kzmmd28e4zY0BdxWlQyYZLPToDyLRjq+X9l0avugHvguEVkH0F9JsHlA9j3
8fxU9vO2ou2w0P7Gk6xvTyHIbF8Qanewufp0TanT0ofj5fGhnTKRosQwwRBXfj7zvgoVHxbNHehM
brYEN3oElFTLxNYmIuZad9+JrfEooHfDEhYka79szbpcUAO058a2RXo+4rzbCBRu/JoReygYtM7k
V1uxN/mpBb8HUS++TXDAUz6g7luRBtnlpU4SrD60LfJguJA5fPHzccXwp9gkvx2h1viWNiI3mHgw
GXD73gnBBUbIossHbJh11ZJn1yZTEmJIFhl8DFwaM/0KzJHtXI3DjPsWVAr5eg2ORulVDB5tiKUV
fgEiuwS1O0eTOTdqWH/DdKnQYbk0GB06CpFfQBMlY69nd5gFn8D20rGfHKlhfU2AZhL6nOcIDV9M
IjpUYcTXfmtO2nbJvvg7zgVjkqnxSkCPAj1cNhsqbAEIPiUuJTYX8DWDXtwIWQz2vgCQsZhlTXhf
+a+qTjbLp7PTZYSdQKAug3EB8FBajnvwMnKzMbUMVTx8oX2JBgKA8U4+SeeukM6gqMt1omz2s7FN
Y1sMf/RPh3Fo5a1icbsuOyXJoFa9qkhb6OX3kaAVeyR9wZKDoqoodTntL7BOywY5n80wfbCA4Gfe
Zfxdm0ZqwNvTMxQE8hkgs5joj4w0Ega7vS+u7qwYUqXdvYg2mlCgXfBjGpp6bFYUmjTLu08Khjtp
ZjcO7S6kan5/+cfa7EFWaI2V6waXrS99E0pQ806+ygAxVIVLiCWmRFIAuiJh9F7ZnD1xnNcmp20s
u8f0caih+1TcX37vB679fiyzMzfRQ/K0+crPIP9n2e0/nsu0Jx27rlu1quFg+/wNKWj23l3jQQKV
Jwsuj2qhRTD1gYaO8+PMnzBY9QuGpg0czioy6vMmLsFUUmtnDrvhbEQ3H0nbYQKvhvLlfCAqKqNs
B+oKvruEWmAgNxapXstfOMzC0yIMQOXx7MGMuc4Y0ksvD01a0TfOixUA5Gy2itEPdN7bybNOVj8X
Zbmw5APImubZKd6fIMVS9YprGdYQ5T672GnRx/h574gYNOjAoNCfiPNLppZn54MD4VWvq2BIYadg
b4AuZh+ONr2bBs7qhfnKiAWYLYLDOWCJ5asGbxwsc7DU4VEm3qRb2GAdABeRrPfR9FXduh4ZnOzi
Pt3pTHLzAOcA2IOIlGCxHrsvJrfSIqlXuzFysTaAERwAw/mCIthxdpvRzBB3OSfkyKJydA3KVFSA
Wj/w6LGVphIXhXg7cTXll1Dh34DPgmBIdXB/rsJzu4WqMRIdOvFUQ4r8xwr5+xSj2sSpzZ+VToR/
TmLU6djWKsjTRbmjjhK5A4nE9nPjzgwTUItYdTy2Ijy7Ch9pXDihK6jydooRNdolXtqqi+36EE6g
zUDo7YNcPAZ3B0M4QZuNqPAaK1WjtYscjUJ8KF7RBEwtrXmeQfojZ56yazeGNbR75k6b4EKMqZS5
zVbrrW2WzzXZQvJlLBXsASnPgVTQVI6EceIs5m6IYAJ8S9AOehGDSvghKCVJG7P092timfRb8h4N
/hsyHiWsfUphmRiT3G1OHIvyFH2tAL3prNyNoHAARtUPBNJZL/gPYaKfpwLHf2U6K2sqGuWLKmdb
QqOGjCoJ/ko+dhrQsowmr/sD0Ce12wZzrYwlozdMsQKgBmJhkjgtaRfPJErrE6uuDc4P3VbwviT8
1F0lJrzLkVHA5d+AHk962sUmS2Tn76ZyCKuUtZcnoFgtM8/IoHvHbdJ4wLZl8GluJ3XVVNGSv9Vc
wAMRSf84Rz6DSzFNCsvwt4PJExLwUZYq2PPzCnjy7gkz6/kDAm/MatStjW/rQKVNz5MDUJPgjD6A
mg982iAu0OGNTtLdEqOWMWFjX/Ot0daxHFVrPzLbO9TGBj+yK4b2+BqQWuNWff5kGKfj1y5DQBa0
/i1+lmPpnk2zy27DAARe5zYTw+pHmkzcMNxlsIh5kszcPKMDa8QagWtyklCi9+gIm2v1SXi7KYaN
dgYalBKfgRU88PHwp7qoODCli1GQGCkHZ5W4SHjvUueqgkx4wfnhhAyf5TBKra1me99WcALhNshq
adGGg9+996NOg5KeagqV1LE5dJo+AH0L8AMEizTdfOC5I9P6xr1E5qZwRNsdnOwQ7gDxsvbPMnqe
FzL12wy4SwUlT3V5c5M+74TRBSm1bfIDQw4MTagY8/8lUQBqnHLcEQtyMKx5jra33/ZSJKvswL83
qJjUupbVsvTce6WJD4FmCrPjuc2GTJHkti2DWcjpA8FjHDsFBjbxfW6GkeXREhnsRpdAfo0+9Y6t
bovmGMmbDyN4I/VovYS8q5CWGSKV7iKjxeC0MxGwtM6wSxqTMpiF7D6ZGN7vLOEVAsMm+mQxIxdn
FL8upovkbZfpzMV1aPtJkmVflOegAQlbvQoskDUJ9Zyc7BNY3FAhnDCZsN/uKVIhhpvIlUbWI4Yg
bv67vlbhApDc+USpQ5TlYx7e7EKYUFdaZXN3eKdp0AfzbhJqflzdfo6uLV2EXzNso5DGwWKj3eAN
Reuo4MQUogHmWvVvxcKHBaXMT0LVybhiYuRlNT/1h0bUy/c1RBOwzIrq3PCfSbw68kIAkqpFHTwF
jjq2HCS+jkob2cwOT7Hu0ciTryezQF3tTFBJpni6Tg5hpFL9sxgLORrewT9RfrbxV6Y8hyJb3lD7
w7We08W+Xt40wG/SiLxLDYzRBRTyRHMS4Klaofb2XRhxX99rzTrhEN2b3csKs/zG7CZGzg3AaPpi
LF761CI7nZA6CNSgvLYYkGr6V2V//d64hYJ7cKoWaKZGndnBRbluh+C+Fv449TVZieIdbpMxbOhF
XXpkKgt/vUduWe8ioUsyt8/qpcwFBvVPwzXHk7v4/4rXtLb9qfOaBH4HXwEDsdMXoVYKl9fSBjJs
sjzI1h5H5ifSVUxDRIaEchKPsVk5q0dKkxJX64WIKDG07g8k30mOg2EiryfkYZbICRtNoeePeDdh
RT7N6IrzeYUJ0bUGlW+nAF6X9v1ku5xjs0BDDv5UN3nGmlK9MBUCOkNLXWxB7bk3dhSoOOEAyxcw
5A7BpVVvPRm1YPhYNyo9Om5RA7yARJQRLDu/1BlnaHrn9TRoKOTIgHDZzhFZHwmG1SFyRRw7IpId
dVrAdMwXfO5Xag6lU8nZcG2Bfk4G99iZ8yKHYLkim96gMnYo5yprzBCOIDM95FP5Qm4+gp+e5diP
FfpB3PSmlJgD+8WiUBEZfc0IY0hrXTMH6MQt00kI8piByvb3Vf87CY7driv8erhwObdFHzF8ohk5
019G5GD3meAGixGRD1EAekuBwyDxWlg5CI2TX+uOraF8Zby4UXDUCOmnqwBkUXFNRL5JSASht1jc
ZZwSjaW4sD3CM0ZrrOwaMDXPOhSf1nyT3YSQ7fuwC/vT14CTPPYpbLjUgQVH6lVPR9Ue+cHvuq3n
s6WWZ6XHaYBHD1vhpHAjQz/PKl3ERwp2kjd4QLPIwhmfMmY1aNi5SiwOD/JZgKXRXgDJLQwIAVh8
fmD5LCMWRdYPbvDLSLLQ0xntBuekTeq1Htt/XKRExuf+Ftw4pTHfVtOUX5jZ812AO9p25yo2NixD
+ERrB6eayE79Hn5qw9L/lBxCrbdJn3R6McRgtHEL8yOSkm2WerlUJnpTgg0DktDXDshfvZQr5Fly
CthDz8umd5KxLR1hUHW/GiSEG4WURiN9zKpMThfmaLjHrtODJXvZtIxD3Yy0k7FQuBIDAAJEjjHa
qviCOsKLh9N2dWEIO26rtT4UfysFlWNSmvNXpXHhyBcP0W2nMEaYXoxZEfKnI2J77ySkZK7dMypO
RYHk+rWJwealtsn1tRognkdC8CCjU+GCoAaclagr049PXk6X0RzDgl0sCBNMMVFOHsVUXTrDGrkM
CIUcTE2fewLk6hlg/os9+g8/kAi8TYuUFMnxOdYBBUbVpvBLZRsmamS1irk/efNMTuoe5eqY7fe6
kODLhznLzBBfD3dRn6hJv8fPI9NvSNXglDhE+jPuzse9a57BszO/2rXw4TIYAHWzRohlGIDDQLgA
ZIUbzoQe0Otcvua7oHY4kC9tTqSRl5QOGnRlBd1dpOyBCHrCTKEJcSFiX0m40iZk7Si/45832GNs
hfuWbq12Dv46JFDBxIhAnfM3qzrb1vfIgAHeruI0vFAq2ypoiIa4mIuhXiGlo6Rch3R6UlF72e+a
olPT8/OZGYj7OPyAc7BPSbcrURqj1Ws8KfXuWomrT+qgZgAtQKh7m0xd1UNWqfk3vioJPhFuFuat
Lf4Z/Z9Y7PYLIqiaD7lhXd7PoaQbq0y/J6D/ESQC1tu3WkrYpTIqsJDs5hxkeATZeCwtYlhyh+Oi
xq/e6Ij8tJUEOv1gUPdPTjjPgn5wtUml+iVWZ5PTfmhAkvxXP3IbfoeQZdB/KQfXmQjzbpOFTve+
Bgk2MehR0L2X91SEWaLYCepMAydZ4a3rXhrGF7qb+drzNJJGpjrTz0aGtS9In0z4TAtkixMEPdDb
+KgoUWCYzF6zFl0MLjrLitiLJt79P9oiQpoN6E1ZybcUNcWnIkZcX8n3AMl5hNpPtydjSje2qWfw
bwbO7Z0Ik0AROcfyyS3bHcdJaTybNrRoCOXLM7x6uvahz7CnPoTLmhTBRnDLI/m35aVFEKyKIRDH
3oq1MDGplm40jWVapRMZTdhuKdAvvBC+6nF5QmX7KrEqu6p6RNbylmYFNRsqWWnh1HKWCOS5QtUw
ZnOFxY6OHoXzQF46gV1q2naTvBp8A1+nmL3dj00lwrlWlxR1svq79bUs8/wKuoxNUgiwA3QY9dJg
RK8mFCsNE8nsTWEUS0Zkk8ChDYBiHcq+LV79rQKNPaWHesNAm6uB76RPqR0BUzN2wdLF4+2Occz0
P8f+o29atOvoWop5fkhyw+2r2LeN9is9TFOo6POtrgIAMR0h7SzbntIu/6HJzZk6bZmOHHavQ+n5
FItdH7/gawdEnhTatWmYnSUhCYbvv1rSETt+8UATLXdKey98Fdt6KaAgcvbPPL7KN5Ne7W4zRS/k
n9Jynvod7mwcRvUw7OEIj3KVZ+hw4PmY7J/J9oQzyzTyL+PeySaQkZd20PyWDuYmXhIn7knlqEfJ
FUYe6QHRkG7ZFIiIe+pdS6/Asx4MZlD7TFiEB28PjsTElT7kqqiqwpNasciI2P6zgc0gsordreNf
tD4Oex1oDbQcqjPwfmVj2URevqe9/JM1vNVokfKcDocKhFzdp3wk91AcbPI/0SL28kiiniJY+QXO
/i0m5GuNY6rIZVgNGaP43hRKhREWh2XCmsNw9/ZtmRhtOIgNjRqlWHDsBI6r388hGbbmAjJrPilF
jzz76xS8NghbNtTF/fjfDM086qnS0fKix5T/N8ASA3QjakJQhKEXmS0A2I3OKC/2FGahUdZBR4fK
uAeJTawDls0FHjf6ZJWt5qNROhR+tLiGm7htOtL1RnVqCci7LQdElIuLp0yTFppLsQ05XQXJ5eMH
NgZdzNPN27AuPq29XC5UyzE59S5cwSI556j5FXyCEGYwb9HGFy29RHRSQdk0/D/A4V1gsQrHe/84
NyOXpPuOHiLeve5RjIiYqRVi0uKl1wtouc9tbceIz4QR/ag1BickJTnYfFnCEjQ9B2Jp6Bvi+eIS
udEu6A4F6FSonsXam6pOANy8u1dVrYtTTfg2cQW4oUsZf9xS3JYajQ/MT3xslFsOuub7+TelxDTe
getdSauRoOuQO+pFoL+z/i5R49kvEab11XR2wnMhZ1bL1aE0vuvnaaQxA697wRXuxK8L5AGpVCMl
c47Bbtic1zgajdPWOFfBx5p9bz9qliAsm9uHOzt4zRgwfxkv0tzaNcO7t+xYkmBgdRuy5ynfwgTB
x/S41XwAA+vZ4iucLDttSJ011LgDAEGVvSqytgZeXbBv6OAqfl7IPN8EmJt9KLluwy4aVazFtzU0
g8zlztPfUABlqdIhw9jlLKhP2FXqvmgvb0JWUn7MBN4d/fizakD5eKYaoI1rqBYUmFOkP0Ift4ak
+o+aXpmNZ+IGZMumtpznVTZX3tCwMgTUBu+II2PQ3caHeV7dN3LMmBNInnMe8Cz78v/AuoTF8vlH
ATzmz2TQXIY60aXXq0/czYiu16ihzzFz5ch3yvRHRHhm/KewVkUXQ4044Y6e7vfVCYapC3V3rUNh
VzHHkR/FpOYKTRdwS95STXRceCRkm+Xa4Ykx7SQEv1Zug4nxHFO90RG7SSmruTW3C2E+IW3Rgg+e
vTn1cSKiWeOuZek/d6vd5MxINcgGOQ9MDWzbU0TCcq/0pjex+Sk2q4OyHiXvFhYz3LvCHihO6wHm
+QCFayT0eYL85QbBNCxZ1YV3U/9X9NJjySKDZyjGCF51y8qqt6d/fuRay5OWMVc6pFDGv9zsDqKp
526fzNSOHgM8qLrMqa+n1W/mMZwi4lmF8x1X8kSmKN21bB+yuFVYoz4iWjgnZXh6AdU2q9JIqeqH
iQUJoaO/yltUcn5crYapMrRr4Y15Hlhmw3cULPpE9lbVZeynEO3BCf5FALtlU4wtXJRmEzDkw8cF
WxMXIHFGeymuVols+waCf8P+ltF13HS2iCz3+skNyO718yCTPl8eYH558PlmILYXDUmP1nPpIxvP
KAOIjxEnv4E9Ua89TcIYrm3M8V1d7f3EPjaxWSBXp5a9A/gUx/eUmDUK+/9d27BT0a6G8cFrG9/k
nEdAJDrb6LP1E1txRCV2KIMfXGaF25jls/EofFv1HAD+Gt3gHV3dUDs9HQAof1RxDtFuH2vxSNdd
PR/clln9lqW3+d/daIZ1WD73wSy3jd+mG2XrR998EIeYTSqJqv4FDM95HsSzJdi6XpK887Cx/iwr
iGEmBCyB8luGUlRZ9spuiUnxBLYIew/704pN5WXcEuOWPUmcw2/nVrNAznFhqy3mtc0GvTqJaLhh
oi9kfDw6s6g+vTJGxYzmRFNIpb8M8QSVsoTyK7wSFcLq03mE6D+MUUymJ1epeuHu1Jxv2zXEdz4r
grOnmDGsfi2haOoBCb7raWX+CqaLaUdRQOLR5i0JnoT/Zz3m1K4KkJ0KkdW3VaqxIuWKMuuXwyxh
HvO+aXsCzKkk+BONYdry1eAXnEwfqQ1UpQUNSmsepj+mX0+lopAsPACkmsJ7LM8L8XNorEmsMQkk
AASUdobFSsIz9CcYxenQ3llSK/YsknYtFJD/19zwGXH3sR1W2R/kUCIjyBezJXfX0FkWItK4Op5u
y/SSEaq/i9qFhQpflltPEaqZKcwvXDcjcw+PpA1G8cj2cLWR3Ep4oX4TUuvRDLa5RiT32r8aCvy4
ZigH/pFHiNAMnzWZD3RvBSF7A4I/4//rrz7FsJ6FJPpmZMFk3htdFa/4auAj22FPHhWcWWFCj+9g
OLsknT3stMZ7bbKZy0bikqrz22HS/yqCkvITZLn0DSpPF3QxTLV7nRAU/HrNUPc3yPceuu/pX2Hr
S3Vla8df6CoKsX2EWtiri6J0N/BkCK3NrxAES2yuTZMW74kssya8AKfy2g6aX0gZKyXRXxG1AYer
AwI7Y9VMFcZj64CsXJOkcRklzMbQO2Xd5X/9H/KNxlE6m2aDZJeXIQgtI79Ks7QejT8iwQRMyAqk
25gS6AOvkjoQWtB7DfjcHuSlJac0+4BArIxo1t7BHvCQoiJGlJjSBUCnhlShNp9O+S1oWBRDv7YA
IMRklkrr1Ii+5Oxu0I0XvN5iZ1JPbOEOShIFPdPXcfxMk/JH84frgyKc9UJ7F8antCUeZ6GDip37
UYHggNvVyUjFNSZjLTPH1J74j3OyXobJpE5w8BigEUsf4+1peV6i4cdjgi7B6DkXatNfglFhkCyl
Pkog06Xyf5ycJ5a+IOdtXuxVUbMzLdOl9oIJbtmkfCL8pBkjWHL2DeCgOoq/QFBmKS6q3ldVDLtU
fzFDilBn7l47yI6W/h32bD7tLtDdZz6LpWWimIPz2b+u9nEGFqGm5NijuwKM/4cGHw48UcsWvzdX
Kblt8J+n3g7w0CMyeOc82N3tEjXSS8pAmm/Qi+59UmSsmpS5nnqimMUh/Z9Bn8we6V7ZOCLr53fm
oI/idtzWit6O6T1jIt2wdxABDh9x17xS6Buqxs/uysUTfk2jCkwLNlBWh6/5JfKbm2pXNZAsWkB6
G/aDqDDvRzBptv3BcFFU2goPWQRe+0/2RAPqkX7ce4fCnyrwv5fWKX+G+4nmSrUeI4Sln86hw9Lb
DQKfSxpIRxnnFYj4kYDGHPt+6kRccwAexHp5SCCE3lv2i3mcrdCAybk86eiT2hHrv8Euae957jux
ULOK8Ye2ZWq7PuXnKDY8hNQDz7sHTEMfWdVQ4u4uf4mScmL+NVBOhlCiVkoyjtqpBewvA66qPDf1
ND2yjD1RPTMc1mAmfm8fRIZWxtHqACfxenLnpBUDTmfkoOD4pjZycMGPxU0A44DHzWFMHWctdNZv
R/5Uns1u6X/LE+GHx1eEZFsMLsU5O795h7TukQAJw/iEUDZjRfPFTPG9m4bQrnJ1sNEfRV06qGo6
9h6pJPorXdn6gGNFHC3zW0Uh7PUCDNbDdUCZZ78LTsanbq3uYN1p4JarDzjPx0uJlxhJOotkCPl/
QRyOX2KUa20HBPABLteRSfACRw28RhVg9rWmhnKBf2BGnGrrvF3SPw6mGu334lOWAGDnYrhu3qGI
B/dLaniFnnEH8ee+I70g6jKUYD9TKID2V2szs9mzMMCP9r3D1S0QCi3YR15g5YDabyPpbXrFPcMC
HBzN0UrScJePxjXVy73j3GL0BkRciL4EjwbwYBOOInLBKiUO7TDSupkyzepSr9+8bMeG3jfR30PJ
CRBCLwyroe0R0d7FIHZJTEYNMS0T6IUk90A11ffoK+BPRRGSm6t3N7DBX5/SQy+ge7578LjL5Ye/
RDu97QlS4TqpaFgag8Gh5H6eELMUnurpp+PwhdtUCIzn2ivHZRpnLtrHb0RG0d1Z3ZU6ufMACCxG
QTB7VcFrak4LQ9FrzBIrtgYIQAiWeTCondJHGCnnThPiobwop65CZjxASHD/4b08Ksv/iTylJIsF
L2B29tuRrq55Twajjv/ybFaIcJP7bBQbZgM1ZHICvZ1zbnnizUjxSZ6K32EdHHt6m+p2OeVgGmXP
s/2TosGWqYMK8AatzA85hBG3yDhRc1vlAaZAVxpfNKGnBAykHTXUmSu6qa4qKZbi7AKTXrfawoEa
MudqZ7XxvgtYa4CvSioj9qm+Yx7EXqb8qtw8jgugc11kQzPupmZO7s1VPXhnJN+lQ2HQr4xPGWJz
r9+YluFwQOJPE8DLIxhbG96YwMuZEywDr0WbR+rnSyWaT/99qNOLNeEV+mDhLjs7LZiMXon+6vbR
5ypFl3EK7ZCY/vPUEiC2eOFMLgSzCdEcZuF+JywcQ3qqUunZjMzKH5q3cERC/NiOe1bTbxPtqe1D
dbiynWNBPigBXXogo8juzv5XRl0O00dlcdKqsZKY5h1BRQhjgUDTlZqwefDOyFnIH4fn6W4ZHE1R
RZUj4ryuFjbV+ae8TjSq7gJ+ZCWi3eBKlgoPu5FT8Yj4A+3PHRz4LRFHQozX2rgG4rSv/HXD/iqL
WdmHcaBnfYhhKBzVF1HpZzW5U62z2ipE9TLsFQ3LXI89q99aRHJT2o+xTVNhLDTSUUvLAsg/eNng
EJePjFyrgmOXXNVkp28hk1C1227fk5Geb6jUxIdjR4ULiXRUB8XAFPdP1SdU8YEvuAntzVoCnr7h
KkYbZbcwTWbJqyAm0otgjBW4LL/Cnc8CMregp1q0MITOF8TkIp/JHOQ8wBv5untLdmkm7fvNUtz8
z4I7eh1/SVtwm9guAiuUT9plwcpm+N+wiRMU59m4b7YkrvtuKRDIeZ4WfY+hAvo7F/RIXLGYL4rI
I/oZEljmrfrlhGgouwMjebyhMd8lV+DA5fky1R0EPMSHkB4Y1PCSSy2EpjZk+OLl6f+5QF+u+RzT
8d3eflMuTCNkLtqPU0P2SGuOPZXOPZ/t3J9Lv65oPFJDngdlJtD1VS8HKIvA6Y7/OoqGxiAiEH+a
g4vDWRGSdZIMHNpkgq6heIbGMLFZfIjyfxk72QQPTh//gRtGlZ26azm2sA96dkH4jrkdko9hp8Qn
fAaY70vjTRsHOWcVx7Vf+tLh0TASXE3FydYnP0n9ygghNFhXJESmom0PpvZAbVMq+AB5DFokTOwq
TBVxlP1Bb9JSh60LrAdpOvJTfBPCLG+/THpdcCvLBWBfu9hxrkmDTUqEBXaC0YT0At59SXTPfHMH
8WN9yDXTB9j69n1zgF0SgTiqoYOqv1l2VvI9KZ0QqXrBABnH3ZRtIEtm8AGwXcb2PoG+bUpQcG+I
c9JnY0lZ5IRPOS42ckr2dX1Reyz8S4c4v5FMJb3rTf/xPxudo/6IMVKmwemk2tTj7tQNcwPE7Mpi
4PtLymYtDqU8/rW+Ae/C0aaTEWDO62Yz3njrAYN0UYW+qWlr0iXwRX0ylAUn/eQNx5rnz4GBGK/X
WWeIv4NAexpSKvgYXVN2brnsmCDCZWIiJ42PsMM/prTs1EtFnTYZ0biGLEPi1u6fh41IGxinWGa4
I50h77ZrcBV4gOk9Frz3yoC3QN3ApIAjZtDW7FhAp02LV6jif43Q9H9GYSghBQfccNPrHuFxZQ7z
YVl1fL4ekmi3AeJznW0kDJeWreJnmvu5qgHo2hVw2hdCS3VtMjuehI7nqiNNsFynxz0UQ6N16TQr
NAMHMyW5UvQVr2YMWvQ+Fc1utk+L1aRuAI/erc5whKVD1I8MCTKLPabSdjLymP1j9WR5ZyEkCqrx
zyWwhlCG/w/GX/AEFe1J64/Ko12LthnnW4cVimKjOrJErTRMd5gyhuDBYhXIDCMLDDpCyFAYb3DK
xnOTugVv9D/PMBrACLq5pY8Qg5na4FSu1W/AI5UU0iWKjDYMcFF4xnI2bBCh2LyzjaqGZtBqTVhA
w8M3+r1PMDgROJ+GtlrRQ+XA4POsGRqbDu/kYlatTs+i4dF/6oLlN+MGlRG5+An3dj8hSZQii6/q
ZCmTQDh3Tln6p4ZPvYDZe3MMLsyrtZOup4ERzOSLy5c00Nwbdf5+5VPpAZgV7wz2qJKST5fcgFta
zwAUlwYsvMbv10tHlNTzSz83InO/ytOhYFhxjCyJPjVfFGuoaneaa7gUx3D1uXK4Zy67zoZb1iqI
Lzp9mnIx+QtOCxsKX2iszFtS30g6aveNK8C1FHkC5UCoveQKbjjXjaon6Vs7kv+8W+sVaVWVbZrf
dUtFEIds2mhaPl8+zIDl4LozNanAuQsROl/JuAR1EUvWvz2KUISngP0uF0U0y0vZ5r3OhP4yF1sD
e76X7UQq3uqGS9Fq62J69BFU8fz2Jqkgtx+sG5FGM1OZNmWtKuTIrlZz6z51HO6fSD0s1POV0Oq5
eiF9zgdEjMs1C+kVtp/sQfSgOPBDM+NSsfnGV4wRs+as4j6iDXdSQtjUqWacEoKKF5OAsqxmwt0U
M28TIh9F7Pu2Brk299B36+/l8FWeEgjzYjIb9UK49v3uGHhI2zMnaIKzJlj4WuAemX5JEFz74QwK
L/+R7pP4N7RGJX7adzAD8rPjeV+ZQWppRgaFYxAvfp8CFLoX+2tnwCnlQxR5ydzyq9nmrDtvkh4i
tofb29x00fYO74R11cZfoKGNwRsCj5x477wqxqZakEDUMOuM3yO/+41Vr3mFAKCJGqFYGiNNIl6F
QoUtBb3sYj3o3sj7dbUlZ77S0NaO/XHW2Ss/6q8lpkIZDImQh/Ei4lT7nTWG2ZVb60YTcCjX9wFb
PsrsQK4Cw0dinJdYaeyJ+roTMpyARfk6nEBtrieMVgczP8kTEi9AbI0XqtsAHIhUbo+0ngn07Zjx
WYafNpcd6c+qr09inEXEYUFzWnPc4k9yphcjZBfQg0wqR8xsuhwgS45NvkE1s8gDEuw7PlIhxWVk
MGB9MHaQsoihHxQmVEsCGMrfPvrUWTpzKSujud3GqXZVogYv9QgYq3CIIUato+msIiqUABpg540v
BLqZRiO2KUa7EVdtDMdRh4dTpgtUlBqmKbQyG7kdfm3ws+20qWHGVzeLsLa2CZNVjpCHuZklWYWw
ciK+yoSfevT3G445gdjRX3mKSG8A5Uuax3H5eZ2V9aBLaiQaZ3GF82m4NcMHDcRUWHV8wQJSNQ9c
+XE/fgjlL4uKlRDRmiXDsceZtlk2Nzwvxo5Wxz+LsLVjG4N4w87xVoS0ZzzVYhfCrnuwYHdLWSne
O/SPIwtxkAEw3Wz/JIUuKjGF1dJ7ndPspyLuVvq9rCyt1H0QotoZp97N6oESN+4TOFaUXOb+jbq5
0QQ2xh9cmZKRgIWqv7SS/64G7it6FtlrKbNaywIa1O7j3vLDwEFPC1K42JRvujkURqxrWiQprf9d
K7dGH6+dguyJlvQideU/kccCwWVBAiqQY2wTJldTKaRagTFAIz73GYrFYz+8CZXGisE7VJT7om1H
QFXj7oS1GCyfZsCJDEVi39NbxYVEJY2YV5pyFFdRutQZDvKQt5ZuJkzyftszJZgUuHlAObjfrjDI
P7mD6/Rc/WLKIDzuzEZuLFsoEvsusvCd2RBa3XqW/fctY8OzLhQiVBjL23G7DBQwu0oRHbbgiDjZ
56rVTbrspis7QnZVzwyYbr4bRGgUt9eoGSlzLQgnT7SZejIEkB0j0Q4QFOz+ZyOtdU4h0AwxTBtf
7yXYlXTW4z2m5W30KLu8uCyLZVKZ05SNjedkSM+S4ESbgkgRtpdbtAjZmlS4FI51AAZ4Q/zL4W2f
bu/j6IplLxtAqGdJXW63LCu3dKSz+DZu/FmFVkgrRZGVq0JWKtxKakyxIM3SFMZNoojERFFLM1D2
ddCxH2flfjyBEnxtwzGaXG12EeWL4x7sZgw1KO1/XwN2ZXcxO2WEksty710vZJFpfYQhKGFmsM0N
DjonRWPxW9FiZ28t0frQM7+XbGnSVBimou3JCwvQbZr+Ba4faF47qRogSyi79fS3Re0ct0Sqr9Ot
5fWONagQSl3sPyh8Azo/oTsAA1V2Bddrl2xKAp/jHfGfSG5EnLp0yBvpHHr+mbRGLXRMy4udJ062
XafFwbs0fklJ5fzJ+0vGZo2J7paEzfZZR7Xucnp271+IAeIwAQbf0PkELUZtEcEVfGM4aV7XAhlw
csgYFXe6KI81DKAYJ/SOy5LAOi6WFia7jUtNocr8flw/vVxBJS4TDNcIN08Vv7jDoP3NRRbiN9Vn
5WgD0qjVTc+TsbQYyYbfej6/C03tL8cyLM2eVAFAY2PpN2iK2b7ncL80OTSkm08ByDOEkZsMWtLy
naTqaHhvK2tIcW9LkCwH3Kv0Qp5lvrVo61MoNuS6OFlD6TxfB3XZ4IW0XCCsp64tCI1Ts5eFWbA7
e8MEWvaraMXlZNKiyP5tLv8k/rk3mqo32U/24Yn1KIdD3GHrm2qvectaFetn+4wRLnPC8ypb519P
eFYY6qSjPpdELIjEqMzb+k7sN6BBlw1XYhDPiuQd/9MsovQQCa2Jar3DD22Q4Z87BIXlL1BayPxe
oBwzn+uHMry357W4zl05nmsYEuKo02Y3rbi0YzacCDFxICFo1iXK5yOvRHaHGWyGRWO+NRw9INQU
0pTZFmSb1PrS5aB5Ig7zAj7j5ieBRn8h+w0vEGnCweumh1AxEn0KXdz+fgEcQF3ReFawAj5ilvon
eSlXv3FWo3ycJzAAReQdjEDi9+CITIIqfBeSc81nMBn080b3Eugoihn5W55gx1n0qd4TFFktZ/x2
Ciddl5H2de6bcZZWR2Z/NX4E3MdMoO7odKlmoTG1Gmwkp5zR6TyTOX8OLFp0xyd6LiWyJ9HZZ/Cn
GRjrxKeAOP/1PaIsumk7T4WKuY0dGGo8dwVjDf5jdC960TQBmHDv6zlNV5kB8zOL65BABIoBu/KJ
o9DGW2Z5rfuRwDd+7ASXd33CODQMD9XvEg8Ad2jclz/3GAjRH8xRNa+00M2M2PttLB19OLxJrYRl
MvpPCBNGqDcE2CnGSvVeoIpLhICtkCL2dGrNVK9e2LooK/KruSSCdFellfNmqO2JGPhchokWdcUH
shNLWwLCchQqmkPfr3vEYJPbxOuu/m4DaxWCAAB8vb0W1uVAkJwZxLc80vC+HsK9QTmkT/huZDrX
MSu38HKMKO8Y5kcweI39G91Xf4xiL+hvrpqrKDmGnxq7qjt182EAXHsgt15eJbaH/GMDp8btzHXL
+Y0xvm6Dk6Qm9Kt+y4ZQMCbn5HDcBz7BQVgwiE0UitZRR9qohVvBtghwr7fJ1x5lyjiyvxqX9G6L
OEz6GSFxNz3sVxoS2U39/oYxfdCWcHd0grRPlRX3K/e47ceS7+A++DKu+CY0OCNBlFXz2ajfbDxY
dR2T0GtukEB40U/Im+8ZJmuTCeRjr8ulGzh69XPdSry0TS7aSnS6a8gV06nfcLtxh4TfKvCI6XHz
jhJfQmCOP+a70SKL0xyGpOcZxumKReYgLmew9x/M/BSpWxzx0XMiaxrFAxqTv4PohrfowkZHkVUn
zCgHWhAIf2ZXmvj4cVXBDrSyZDpmG/wkXyvuroqAk5dfsD92Ma/tsxRSrxxzCX4NGmcdGw6h6nk8
V2pX0VCxUCVt//DK6swszzYSsY6lMWwaby5/l/y8vXnBrwS5paLM2io7Hr2YcjoFupvqnYeHOkVg
kYo/hmYeni95h1dLSu1DgHv0+SeFp/VPxyRiIXtFg1j1CsfApNvKKpvgF8sMcGbJtHVTVmwRT26Y
IRpkDsq/uJ4Id2mVqe8BPkKTL3LEJn78ot9GmTB86n/rNTdmNUWs4nrpkwAJO/gL25pCFLPUXUfD
WyOMgsxO7GxH8pvTa0kk0QvUAyoUSEukzXcmIeoy7w5AqGpf/nNnknSBaYhMmEykfMqWJI+QBUq0
UIYqBathdrS9sSGc6BFHkjeZ3+eAqzhlTQF38Ei19E7t4+0F/wXPiixL915Q5SelHkU3pZ+Tb62P
Y/Ly+UBKaSnY9yXlbiIzhBia3a0YwuLvdIY/2oVLtGzeo0ViXMn29nw8WkioL1neVXVlx1xxC33M
aoB/CNryowH590MehfxTkfjCHx/MDgAaFyuA8z7vCrH/3XPeoju0aEaT9j66ednTFDX665k+7rEe
6rmARegnplneZelfFDxAKvwyTxxFoyqSfEihGC1LbufHh/Eh0ggtVTf7KThgxOLJWgDNXxc44+Yn
UtbYikz9J5fFmsk7ia1pjirGZbBEIPSAe+G3qsAK9dQRCho3ByUiRsobLeifzHAedtpHFDDxlMFp
iecSZk8mvrtcJ2On0m43yI+Z5Aj9E3BD8lO9if0+XDOkzUjsS+gAn6vjsG7YxjwBg7p6lcS3tuFU
TSe+SPEUehOnE7vaaS0PiJLTRJHrv95UB7aYjx2ZRqueWPXSq0w8MSuB8Ho7E8np76xgdSSl3jZU
58daCgPJdccjy4MQ40o79ux88U6Ef7hsq2RdPaPAQkGJFGx7KDUaV5gqK6ZObbaaL3IyrIWFzuP8
2u0pZMmqKV1YSHhEBSpYjU+2made/TVRL2tJewf1Z15fUwCvldcD3UapjuBAeYNIbbqZgBoQ6BXq
6OtJNGVL9GJSMZzTxMagYMdeOPbGd+QvDEneDvuZEkcrgqbUh1Rx0hedpoLUr1wbD4xsIyvTTo+m
k8DVzqA33oCy1y51V/Xq2dYFPO4FQFNbxTi4U+NLY9soGZBe3oOFf42yAC5TBqn3hUG057WFjpmC
SAZDnnRc0rB5AdDRv/a33S9vq7Yt4qWbroaiLU17zyF3gHKlyibMe4vy/YqKIhWl2hIlEhmC2hBq
R1ghDCtOCVbavGkfTMNTLowgWvKVLprNtdXUPjvE3j6w6kGTu6gFzURtK5idkVge9/W6p94g9cHE
qV7n2KvyLUd7r7F2SOeVJ/+wDJfBoAjctmmC7R+h9uPjsz7FEcFoDmrfzxXeL0QssdSKOpr65BKe
aTS/DpLafzI9EGxS7rfQE5QCQTztL/1Miu8qQW+Lut1GDLdg8EemJVywm4Rxoxo4jC2ymW71FIw/
2RSWhlFHD69mf/ccohh5PAfZMIjPHyPucwKKxBD1jafKxkXObhu4tHlSGvPbMGPlnIo9Pxa7ydOz
w9gRNUi7yGvUMNg3wpzCPJ4NRkZuKYri4hTTsiApWyJabyCwPyf76it7sxPW1v8X2aIYuRGrPHPT
dSIEyTViY0zsbi9ANLIhnM0xtBkNAtWhsU5oH+5W5Sw3DSAnCLokQ7r6zvfTBzFas/xIF/Z3NiJn
OXpZEeyjfpHquEeQq9j3o8Jc7o1oprYUuHV9v2HCyJbTZ1OYheSDBLtxTx6jKpBrtjBuYJUdohdt
W4iYB1FHikkHiz8NxqbWYSOQu2cMMur57ZKROeyO07/quZpAOVyf8Io5l9B8c57QWgyYcHxarUWO
GZCqJaNUVqnoGMbeFIkQwNd/UcQv7ljbpyJ7ndNY8gt+1xjoCO/DDFNjC58EsDbpkeLD3cHvb9m8
uTHNlMzpAfKk6ae3Bd2W7ZXo5gvJ1QQPgKeeHdjmhgTe7KmZ6zjqeh2zfBiC/NcObKgLcZXE43Ou
1pgvT1ZiZg1ofe9DQIA2vJ70JnsPoSfQe8y6Crp6p1z8lwZdAd8ymbGW+243AL8VoXK5dG38qeTE
q9tq0nBFU+jcS6PRNA3h49hH72ZwL07Q6vQoTYujpGnTHFIAnKjWaTutmmoEW+JUhDaMzisKEeIa
AviEtXBHYTQt4lfxRqL9Sl5jHSQAdPi51Fpdd7HNMd0yLYSlKOkJuVWs4Kh1QWOwt5O4mnnuGlBI
38XdE76+wWVShjqgsVlY3msuv9b4p3h6W6VTkGJRi12WxfWqb4hMNToN5cIQ5z6fqK97i1bW4syZ
X8prx0cUsUcQJ+hJ6DXO3vgzrwYcpFBfbIWzsA4xkyB2Wq78VDcRtb00Js4qBfKeIEvmFKSGren8
wG2htumMlo6C9ADf8UguEjPPL5JSbxhlj4fSAXVVSXC4CSTVOmrQu6l/2SZyIM/B3kFw23Q375bR
/+od9+Gm3s6ZN/i55y1sgaBwkUlLMOSMIUh1kWFgjOtWV0xyn6Rz/23wIjDdZiiNQHhzSAFoveZ3
PU0rk8r7JOK8kJMAtZNRsPmy3BNaL8pM5g64kIM1mM5jDPY1eSpDv4EJTFZL/QUdQ3A4MXHIEt4N
WPNY19dgcxM5nCQSjV1uALclgIHX3bacLV49QHYV8eVaurcRqMaSg9+WWvfLWgtyRx90Nzkaal+1
BJt9Y852gW1B9sDM546GSOwbgevra87eLVTl4dxjEnRswyAL+fGhtLFbyoMY6jmp0roU5sTbJQrc
+K9yjZHE0wDhsMsXiTwPdJ4L+HErUo/y7NQ9NtjoPJ0ZYXrMU+ZdZ0tVpF8AZxyqfLngHIfUialF
9JlnqynvhVlVKDK81Fn6YLHZPmdEIrzRYUe4yhNYts51WuSU2X9jNFzhdw1yqJdd9Llr/Fl4M5zV
YoiVsbXLI1f3idIA//HrwnQ3l+eXnJcuVsjDg0dMVRtFFNfGNAs7bt6pRH48ndr/zCaFOhPyHTsQ
iXKwPahygWGVopuqTAdYL2xWe1Qih05zTbyNUolkdt8/46dwet7XiTmI+JR9o9LsXR5XGL+F+pOS
e1Yhurg6KSdXQwLOvSVV664N6RG8LWPzEQ7FpOvQB36nwFUG6yRzt5XPinNEpo40juWqRnXlor7G
DtrwZnNhRQdPCHzAzuEDfvEUJQVcfRrWuIbkJJFgihX5mP+VqK1lUHw+xyETGZOWYYInMed4pzqc
thr5x2ZkHjafavJ0MjgNegScRbxescpSEDidoxt+Pi6HffkksPdckpvJ8O4YLP0/4PAbp+TeARB2
52heh/2f6AdPPZCpRdiJzVU+TMKqIfly3IauVaOBX7cT6qfep9woeTEVF0EiwJ6ApzB+xcVreXQ0
MeTpz8PJVmd4BSB7+dEZIHRXiGp5yS7/pcwSXDXXDfocSIM0oL5qtr9ggXMHqN2TpSuLeFrqcfO9
1g2c+tJ2HjG6HuQqxpvbHPaQIf9UYoLqDaQ6M4jD/Z4QvwIGPQGk0NOiMcp/WJeVHuTXo7iwI8rB
WiZwjquCnzatbmP9djnWj6iMmfDuMKEGM6fWoGhx7QL/jPzCo5WzHrrsLSNtCnyXdyQ+67HSWJif
DIOiuTlmOiR+hd5mjExqMD/7fovNU2NMshMFW5XSYjW6FeZXMt1q8kQka9Eqj1NkQXALh5WCQYvz
Z77EEdqyp4n25zygVGfwPHBnb8Lvly/+YAxC850cAgxieO5gHrz4dCCY4IalD4+q/ZFedVgz3duK
fCI815v/r866Y6BXQHH0+IsaQw3T6yu7SGmjVssJFSW7XG7BHqeiUWbk+U++YoWQVU1EORgPqsFG
EUJTy3Xgawdi1EQss07SO7Cul920ASxs+Fh6/uW+YEPuexyAXtCds/Uc/X7i7nsmVuTVOxv8LKV1
CxIChESxljA9xwp0npVxbxJEB1yKLReoQKxFYsGEFeIM9CTMsTgM4g7RXEBC1u9vT7SyiQFzxquw
Vj/MAPnPPQAEAfTZ06KYF7UFbe5XPf6KCWkPYn7ny/MAQnUIrFd0m9JD2jq7xW0jhJ5r9Ecx6SGE
6odOdW8ymBOMFDaSuWjNO/5HgXVzqEA6OdYozKHYW3fo1zvemNXm4ox2nJsuCDqjwQlrYCzScHn7
mQcjpRNBXc+YodqgX60DwsR3UfOz4C4dJcmVOoophlA9M2zy6u0NmgSaX2MHhU2HPo+RRYOrme4I
dY6WSSJEswZjyVpU7miKq+MFA//TKynKJ8kvKi2zC8FVRw4ai45CwohhgxaE3lI3H6g3Pda3ot1z
Y7k1shkhvc1t0lyMnmvTmscDzqzgAYlIhk9O1S/EM+SdI7L5g/cmCLALDvPuISZc0je+d0M6b9+K
v+MPX9NP1ZxStc9tGU4JOGPwqiDVNAAiQrBKjDOREB2u3WRckTjetpSIA36rIOiLcmK7R1bl3gCo
RkTvRGZF4FSS0RK/d3b+nD0CtwzmbBQfUWLsjUVlL5OusN+K+DG69p573L+2APHrNO5z7Mp6V+EV
eiqMzAxqY2OhorQhPagP2vGvTNmci+AsXwMKYxvo4/wXSKDS7WOpNMjatZwtm1ls5ZCv9hZiOQpf
v04VMtsXARFfj6Vu1+ux5x1DIJSgLTllAHwDlJ7OX/pwwBK9FKco5AfyB/XxwzxXkoHg10pCO9tY
6wCMVqI9km92azLy1EBDLb+hEHbitR5yQZDAa5WRcLqA/oxR6eUO+Xug3K7hD4xIHtfWFuBy5j8R
2dFOwoS1NOAmag21gJPDKKmMQ+MhqJJruw6Gc8hsNNUhIuoFpcShLMAxhd6kfux18vLMt7cre1Pe
XNI6Ndv0ldZgqlYP94hOAINYo+u1iyGjp9ck4CNuYe/uowi7iWXskYlSkJrpN3DlFeDNTOI67tMf
VEhtzv+flLnB+nYRU54CmGUomH1IAGTStNWDALW+9Mv81mJHk5mNEwbfm4M7+ch1C2pDgS4L2Vl9
BpgRhMP6z6fAUNd46KP0Lhxy1mQrqgH5g8x02D6bxusu+V3gHofaEoLtcDSXvqIuM5TB5YJUpOxW
J03l+vD8AhgfZMv98rQFdaOhv/jQoo4cKBQHvzeduKVQFmdREZ0AjiDd3wJqTpObFHPC3COBY62G
TbzzZZ8Ax+K5FJEXtUISS08HaYs3Sht3dy+8e5niRVtPErZVNWHWZNSdPMqYAk9+hIyJmOAe2eum
oiISXwY+iR8CWDQQG4Uy9nGyGstQpfxmcDrd6x1prmZzenAvtojSEW/tJxAsXjPVEiStZ5zyBRzP
CXvd0v3Uj5+LkvADr9ba2i5poz3Dz2DN99UhVH2D1SlnBW7/iHlBjGts9Ji8oaCaGuJnUhzLq+HT
hQVrjWVtF2Yuf4VGIM+fg+P5AysmQirBeBpJHoJxIhQOESOXpFtPwO33i3ul9M1eWtG54Gsg3dZX
v8uHOQcpzuKOvJXlw0Sb3yk+ucgAZs1Qfsql7SSl/17APj1gAYekgr5Z4/HnL4VJPBflVjmQRPfD
YSIxD77rQKT+w8FDd+m3GCVUG2SvoUvIZ1b8z/fTEmIsp5sYm+H+28Fbuo1Nflv0vov64SGHnlQ6
Yw1RtzGFr49ZqFxgoBaP9XiJbmL4RfB3smJwCz8y+6QLuyt/4cs9YmeaGl0xc7mN9fksV6Wq8T+/
5mRSO/4kwuEMGnXPQB6ohHyKLAx6L14Oe/UL+Fvb7v3YMci9YUCfivixNIgZqsfzmxrha3uwPx3g
ZFFCarWMUWMcpuZevlLzPFOrjqhZ9kSsbmo8ISQuecVoQWvbpf9A7a7Nn/+z4+BVK46OmpuCvG4o
Rf7ilw8EhmzYeB/4p8FHKaz64WAY1eQSnMgfBsLZDirwmCyUAnyv1FVfEyYnGYLsbc5jK6I7HzwC
3TgeIZnBtASyx/XnZWwiP/XsxHtIDzTVsEektMJvvtyUCXy5dL2s5xVNgTI1Ubkll9Bi/LRsanQD
lRt+RBS60Tl7s2qQ8nHKW6/M+U2w5WX7psTq/8dzb1ghlHqzEd6blaBzbA23QLo/x92LHpW/Ghgj
1OcE2kLqAefp2Bs9ykBJ4y6lnPvKVDB3HfXxcFoPtLwSJBw0Vy5b07VsEm0CFa0PJsT9g+3EK27E
qkyYXsBthiCeFtz7RCHimBebc/PkUuXltLBWn5C+hu9n4auYv+DVqeb+8ine5zpA/CdBGpRv7RaX
U/SxGQ9rHs75CKki2bYwTD4AHiN3HyVveoO9l3ai7g4X+iPu5ijRMwlJPldJgk8UGqhc7VqLUpRd
AyiBoGICkbHvppL88Qnu+m/sHz9IkKS7Xvc5S3pDKpnjjeq76wOadxYrT7zSL/m+lkPipe/wYDK/
+h4TaCCLDEzEGiSl+Bb4rO9cJp8m2W/cJedkE22Z1n/1dErtXraqrXz6YiEzloty5nky+dN89Cmc
cl1GtTRz8TJBvqJp7dbYK5Nf44guCSXDKzTHlNSozm3VMkC5NQH89MwuMebfpUnI8pDqKIzkGiHL
xu5N4RywB6RTtSCTAKL30sts3Cr4GTK+OBvjv6ojrKy1YE4PO67XFM5DUqoKYG0GOuYgtmSAArYb
gizlFHZ6s6imyOm8wf+1P0Z7RJpm/Zl06hisVlm5pyY64CvpSeB+lw8l9Szh/Y0JYNj6zoUdoUul
4fFlfuban2LLmJWyHrweT4eZHwT+J9dYiGMDJDFkET9fJwHRP9xsOxHpuLwvlCJegIj6fSoN7yo5
A8vd94woy2PDwWX8ZNITECF3ULy/0HB9E/micaOupIEfcD+j9nn8xPkqDEbBgErM+Je7ByD+WOAL
EMO5Gz5aVvb62QGcO9ovkeBhVWAoSRoS//PTg8/f798n7UTKvf9FfbINPsg329P+7tLL62t3KDaq
VecFhB1J5IqB3FnyJFqCTrAKltPT/lToDnGaJZVkgY3es9y+2n1W4hNIQ84xHTHyqvp4bBYIuigY
3X4aQKeNoanfIdF0oaih8SHTFdR5Sph12z6fJdoVINYUSnNAorEiov9omGDqkqoZ3XCOvCHyGB6n
eCXxwzxtBrwqA23QFz8mVhiWI5TqvGEau5YDr8pH6/3jaLEY2zMOPDGdzcjN2pMXjJVpMg8ZWQiL
NoPgQdqR/wZQ8kgglMK7SecBiVIpmozjtlABbFpMF/IrqLDFaiBi6HyyPZl/q71QQyDjfQisys5d
Zs6U7QD91clGGGII5c0yJAqQMpxRsOmZIkiEHyyN+gERF0utAEyy4xCPMIx3MEGJ+CI0ZcZtjo0L
syQ0cHWxy9qWBohvqtd7CgVelUmV8465Ex8UyU0ZtBO7si2rHAxkyV7tjSbDOfFcLOWDqQIKgHnS
oHEAlDe3FUfb9BCOLcVzzYqnKT4zhk11t7OauwXOTFOJwop1IQEXHoc/miUrvp8y4PkGg/IM36e3
Dc2obRfxO1TdsiGIPLGaSs5vaoCq+4ohXieRdDfXRkX7a2moh3dlLeAL210FV+jUgQ3y/GZzmXNR
Xn0vvmOEzK7MdMI1DA0dQGUmFAqXfd4RtRUfxOCgRhT1/if/iIUJZLNvw6DJZ9fOpGiqRI51PQ0N
C0FeEgB93QlBMrYzgf9AY8x0WNSwxVvuMyborfYoWHpxJe5oRjrtfegikRRYck3RiMt5UhH0/blk
bACaih5VZyF28o3zETizWX1282gA0cvNv5Rt9Hg0uOkqp5WPvNBmIEmz735a3y1l2gCP1Bz5lAeL
pXihNXElAzf9UstlT/3gfJC8CncmnQ/Tbkq9JLbgx8n+quv2DZoH8K3xprB1mXIF83A/jc2YK6Nw
33NDZ08NvN6WaGJNV5iXRhWAVRX0jDNLZymLhw/d17/M2toejTfZM04ESk6EWxrl9plEigF0qwKh
VxJnAAAGWwV6Ph1uQVEHsPG8mx0lu+7qGDsUm36ZpqorE6jJRvMo5TA9QwQF36gOtaX0iQeBjnd9
16hD8wrvlvl/u1htNuulSS8RNJjSdbXECQHi/vybDeNG8zD0kZUC/zNUek7Pc5RO3jaHz5iHRTcQ
7uz53pA81t2LnHunu2xaxz5CriovbMZdPjqIfwaSY/e4S8tI73BZ5xmM3t+/MW+YKPeU5+JMzr6I
V/BC/GgjSGpo4UVyUADGntRo3HJ13Yc64aT83KT8k45lQf6WEvX2t4fLEs0GIaUgG/V8PfSkYWbS
ND7qlOXovKxmJFUkAEOgxKKF3LsocuuBTaWW1H8cGvqlW+jzkI1TBvrbc3o20zla/VJ5KbmsJVw4
HmVS34X8OOM0fYj4offZ0upua9tJbHi1XigzrNbZuB9U+uHlFOXbkiCcSAoBriHlN+3JJ7WR0f5R
bp0byIA2OS5Mog+hODm6AsuJUdrRjopkVPUyNClPSK3VAHRHox+EoBR3/utW9vJoJpQgx4iA3ijp
xzxeZJi6VH9R4V6wVrmzM3GU0CeLByzSYyI513ameZBUu0MrNOtnZqt2R1XKeCofMr/Y3zpKnDeD
zIqbfsO9351MKlYaiyvkD79L05DiQNH3upddu/kesnwai/WADfWzUPz5AVD4V1q0i8eakEsdaDk7
cMxDDnQ3Ca0Hs7nwAK0xlM4d9Iaw060RcBtO4p1tLVszCa4RnXEexAzGspcKbaLndVnGk7r0HLpI
1Amv52LpdAyEcaVvkNrkheVSTvbT3enZ/ZkabnH/9dz4uvLpoKzd6AM/pwqSV8AdET71V+H43b50
1ZutdwS/1uYHwBR5bE4dRE8nCO/dtzitbcLeqOQkKiZ6MvfdGPkFVFkpgKz7MuiCMu7GUkjJlNIB
37mX3BejaGzXo/TbSZAIRIIaB4bvAYBLgtp9iTw1hz9uZ4WJSJ2nWmviz550ilTMr5F/LkRTjmqr
+XsSoVJ/IS9pcYqVBoPe2T+eBkPppGdZlhvf5Uj6eCZrFLOkbRfhXeFLbJJt0tihEwMRW6geGV2j
tmSx4Geal/KCDPak1N9pz/lJKDenCYv2Txayp62xCS+zNPlmrw+aW/4ruCJqUr/aOcnNnF2aNob6
2vPL8rlq6YUZs0TcPs0IWiyXDa+lnhVknm7dAUW5RpiesStwfwHabrxnx1CCdyGmYYrrvi2aCqH0
JXmSD1IySavtAKT5eWoIewr1rA80v75C+UA0StcQBZkWvo8/aLmPxRc7R/NgXCOpE9lU4+iie7uC
fB+x3zTgrqfWkXvOn/YiAnmgt3Lrb4rKlTVfeu1ef3AI+q62Bg6WU1ZeOkQjItwOmIs+irC3z6gb
WTC5b1NjxxcOjOTJOl+P40xZSWGPmfX3CZ5rWlj3p9qPqcA9DZqHq3QMoPbsGRQ6CTCGTra+zqHC
+p9HkVxlifaPJTibIG99bdsCbJBkzE3Ewbhwykm2gaNslqnguSOMmLT3KRQLWDCFQ2Qf9mrTNgwl
iJWAn27j1fptMqK76r/arUhxjdxefvF7q0B9QmS9AtxNsNIlphczJCmuj2y6TmNm+Swsu8AXCDPl
kAbXWDYahcXsxv/YIh/m0ZvVg3hNbE6ZJ6klAsKNcN99vYT+Vg51ad6ejm4Fmh84lPN6gZL4TQbM
SY3kRFcPK+zZc6+KAktF8mVoyh0fu6gwNSZGPv2YkFZfFUysdtR0xJnDiCR9Azc1zIQ8qzmbQQLa
aYP1LSEwE9w691+1oZ36cHakLKmJVT9sOy5ZzMNQ2EubfgKpd19cRqP/SlL04HM5HqelJBtdL/YV
oQT2/M0QczuVALyeCjKKwdbP3YJ7YdY/D5HCNvP+IK3Zl6kJDArPHLpuUWNtNhZ2CVHvSzbthny+
OOy23tsfU8UrgmUhp3/H3iatLV6YupfEoekuXuu48nfulhjhgrplpsTcwnrivpmQGTlkeHw2gC3Q
1TVM1Znwh4JKV+izDHiF1jLRym1lcG7Nhmh+XVbHddl/uBr+xRDFh8nbyrirkAfP8pFe5Cx2rEqx
8iN6taFWWwlI4lKVA5m+LbK50F1TWVm0LLH9ErbrsCoLNsfNnQ98TAeRr9R4sgXL2NKEBT+cHOJT
dDPtcIYDacer+3cwmJef1wniiXL+kIWvXeINYs4p+asjKDzhV7OFCA0EUT3z1tT97qjMGEJqOj24
CKU/fLCxscvXC7guTYau85FykI3pVpuCX2gnb438bgitBvmV57sx1GCaBXsE4NmC+28UQO4SGd29
RaLVq3iJZ6Ozo0D8bvHv3IxdvnHSoE6Wq3Yok2MpU0zluKuiSoUOpzOjC/1wbMWGlajXQrUws1gd
GZZGJQCcV4lHp/zwbN7ChA++JUhTWdHRwsCxk6mp0cHGwCTudYrxJawY38kFDzCnX8/JFVKVE/jC
B46JqIjxPUt0Az8me4DoPi3T0wwDi+8klmDvMMiytT8rqDYKOLc4lunszK8Md9FGpugS34WJ71Vt
Z8XMTCYHamibwlVZQYGBZpzBbL9PtoPL/n/kRpZJUjnCW/7f8f/QDsShWNfYXygY1u6gZVvUwW/4
ntVEtRPYEMBcGBQv/SaveHmwULdAemxjpzvJe8hPz9w9Vy09PQFDTzVFHB+BIBvQ2exX0JFMubbo
T2f2q95GhRIBHIgYdhPqDXDG/L4/eyp/SqzWQU2q1KrSwqZij0fbezFhOROunqkz5uvzSzfKJqu7
yFWr4C36KVu2zuH33h/OLkmleW/3OyLBcQnZn2amFME8CdNiOPN6itiZ+CDPl9a1c4neKnZqyWlq
2To4TjYZ7pxWlt0WAbM3BvQ2Zewohtg0PFg0I1U6T6GhUa/Ip1O0AUCMxrv6xH8OCbCZj+iim6Dq
b4u+YIE2fnX8I1fjAJSFei2+AisvlNo59HbwfkA6zWm8gDiE9BoAzd4Ps+gNAW3YSYPcYBPCf1Xa
945I5X1yc3FVfbbL8DKW6Pjtx9Z8tRB6lYDW+xIhVsCq36mDh1h2pv6FM7xQnHkWrGC9j4Rvg0js
gxP08d0LThb/aTdzeInwYa3JFnytKq1wrE3K7zwTgz/j0M4QNUA7+e27D1RDxtxQfWjYf7cGXZGo
6e/MDehdKMbprrqonU6miM2KDAiVjMWlIqqR5p5Ad6oFslStvTa3FzFMLYBIJ6q8LE9kBTVo90fw
BiWTs6CY/LDhBiGeP5iIj6cTRbEmxXt1tu3j2pjxdShR+ZIMfWLPUxVrOkQ7uEeZJ3d6DQUyPAI4
lz2qZp9+6c7cm0fG/i1ecHWNhj4nA1RcyWfcJeI/SAS+jRrY802+RYrc1BlLwjNV3QPwP0BqOhp9
ddkfk3owPxxTchl7UEUQyoLOGH/PW9Qz6UOKIhgpk7kaQ+J2ZyiYaBvzYyNYSSQtpt3wcGwSCsqy
Mbd9dH9M9qG/qMLpN4f9hpphVaulVtq1thwrrBiv8SmmAhNw7BVvLUpRXGpO2OkH0wNDem2xpg9X
hLiDq5w8eCk0lxiBXMnLQXCgcmFOJ86qnqUdIuY6JnJHATaCwqxwLSxqvDNGPhSZQUPXFjDP0K3H
awgBCvL5k1/h+Q3qZBbbYd1YzQWBsc/8O6cjq41IjRNLVQo6vNHBZ2svHcD/bWB8NA1NqoJVsow9
24JS+fKfB7Kl6yxl0fRcxLv/K+8iGblZm44th9UqeizS9IJoyMIgAMJ2ZnVT73FWoKxrp/mHkx66
4bn+bZL7ekR8u+VBYMKMPiylXwn6csYYenFu9fAom1Y4SHfekGZAkBCRmWUFyEY6b3HflO/nZYAY
Crj7FJL9dqLmM+yuKwurZTTUUCPoLlqLBF2+9Z+3/3sb9Sg7vPW7dVTjErcPLo+hZzvx84Kz5CH4
OmyyG2kt0gpuxK87jOybxrGn2+VOhGr1Ku1vOoG0EyYoZwugMUlIZA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
