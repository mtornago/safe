-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Jun  5 15:27:57 2023
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 36 (Thirty Six)
-- Command     : write_vhdl -force -mode funcsim
--               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
kYrcO/E+Jhm4R/4R3+CukKYR9M2FIvcsEHYDIEQ941LV/qe3nw66ouV0tjU2K77WxMp0KzE3bUaN
EkHZUhS54Zbapq0AAlHGThTWWu9TToic0Fogfo0uxbTRj/YKvsYbGHXn+38UtVT4gl+Z+q34s2Mx
S+RksJLLbqa/UjuB2IA=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
k7VYfhbczr+tglBVnP2dNpzQUg4faERuh35S6DlbOXKmaLBzNWJuLZKd3/iHJso+4/ki/NZUVDCo
PIbVzwxMtfGyW1fMXDvveUi46OnejPwVxk5t1kIbtSbcZCd++dNgqg5UzMEgptRWzheZuzX0GigU
yFrxhwF/EKgqip1pp6C9cstz8ElT8YbfLOW5ZqJRuK3p8wRTUD9tZ+3ZT4AUQNnb5LwhJYd18bKy
gCZ5WG9Mj+aMW9valUSRFjEY4oFOYnca2u9dC1uGlv48Br0t9pUhfrmTbufRCalBxAR594dFK/W+
13kLKPWgZzIiZRLopKxSb3kx8JrEbJXF16BnhQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TxEtvLMShWARGvALMwAihIuShrdtPpwirMDR7BzuLz8WzVhoqvJSM5/nLMHFGqovxD5hXGIA2TAw
UB0YVlq6K3gG1/oM4RpzHTN3yz8Lt5YW3A+UfuxJr1V9UVkS6LmvF75rPoruMKpllkRnQaQkrdOH
79erJYgSSdvNFj79HX4=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Jd4QdSkhWhpPJfQcqGINGTBbyQi4fwpgiNWDB3Wd2IjKeric0AmdHU7UViuSzCLh03DSaNG2q/XP
qatCMMw9/14uzhpUJU/1zUWxXlbRxdCkB/LSsYsRRmVRjaX8PHa9/COyOOXOwziBKCZ4EH/zCO32
LML+m8CiAQ/Hl3o7OkbgzReeGFKo2yT0AlTR1mlGeI1ujqvvwRe1Fai0g+TwEJcmsDU1/5bkvxQ8
aV49pZh6N2SUhTCJ+wLBZlcMIljfD3Bu8Sp/4tL/+j+yW2zEEf4Sl33jw0Cb08EifW3RF8BmuSm6
hUeX9HuDvEf347dVCR8t8qRzeC+0nGD4/fB1NQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nE6k/lSQEQ4OmPB4XqBcP/LpC07K/JJ0IvLqk0FbQzQZjzqT5yDvPsiRjELAcBvPJRahwOqlfyes
JDXxH4G+XSbtKQtE02yLheyEjNesZ0dv/v3vL+wA09O8khSrVyP5ijRndW00Cf5Bf2IpNiaJRcds
F1ushZZu9jXeBItrh4znBf9fOoXggbdnBLyNjuw7bRfvTeY2Xhe1Z7RpJLgPWMz3yKmlUVxO5Zyf
mjNu1+82dGuZ9x/eImCHDzcLcpca/TdMV0iJAkZHrvuhhu0GfQ7zgBbvuyb+I/r0q0vuL52PeEET
HDmGQS2oxiFTbcwiGY3t/ioXPJYkEEqNFUIzSA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EYYoCPbR+OMFlmBfBNcQ1RKQKD88wkYgxA5pkdacb5EuwAeven6zC8gsLrmbmaf1Y+GE+exjL/E8
csfwUz3cQq4551Y/pgVQB6wc+K/5qus2SV7wqxTpqsWY/Yu+bULiGuBSdS51qWlfxDNujKEBhRPN
GKWkQK8KP7xMHh1W8rO4WL7cLP0qnZ7xSovnz379iAYpAJOGf/f5GjM87wrRCh+60BUmNbENwN6h
Un/7huetrD2tvDcD67Ox5Dkto+nybbrNNH3ry0zh96Cq8sxNBI7cJ/iRp5kCBgqxCxELTa7hlTHW
RWkLjA2W/Y2HjatDbYo5U0A7bO8ORiG66IX0Kg==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
q9bGXHBOyTLb3eTSnDNZfQbfjyoc3yN7NB+1C2N+mReGSJxWRtlWWn5HWbhvjoAJehclGC7OtjK2
ZSTJ0A3pHY3St3rul3liQXKD5kCQ9+vFLUhyKlQc08mhaOXPkXVrLBkSbJoneeg+zcwJuKQzPvv8
Se016G+DYsP9PPIjvWbgYSkDDPBmrvDI1+5mRe5HwZFGFGhAQNqFMnPAskAW1MwhObzaIpkQKTZT
7A6i2BjYT3UzWyOCYK2zgjiB9ZFwChUw4Bwh+H8Xf2j3ysF46VVr3Y/hfiRxPSHR8Jb8iMEkCJjf
nRAfkr8Y2ZxDL10aUR1VFpL5aHsLiRKnNRdZXw==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
nsakC0nZIZNi1X6ujQodgmUw2UIdYzuFQ4iAZwA9YfvRrxXUL7ynKQCgPpNVzwJk5S+CJlgNjRvH
avhNsBU4C+cBB3dvqouQ4tOLrtjvGCn/tgPDevuIaG5LBxGdZZ/MOgVEltPHWIYycz6nfuA5/Axp
6IIz71mUhQT3OW6kWYR5cK3zVKmHXkQGZxfNAWG/Pw5DHuc9xxTQpswaIv4ECw8olrxqfoRkzz/n
gmc1riU255Qanc8CpzTXkB0TXLYD8b3W4k0EIAYhAlKk5HVAVS9D3DfcWg27dKxRMm5dVH7ddpvn
9W7az/Gv4/jAcQ/A2wvn+5RGmVdmY2XJTvnb42j3M+6+R6PXkHvxDCRRgj7df9TYddZWyOeT0KQd
DnIaIlkFA345xytHveeTmDy6qVwsD6GrlsYJS9tCsR6FloMwjoQcZKSxBqfWh+rvQ8/8NxsGVy4v
3tFI5PwOhr5e4Nw4hm2q3u3mpmtv9+BzXIuf1HXxWr2eSaeu22WHlCsg

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WuUgcS5b6yfqTuzjufwmIVC5kWm6y/3mx22Aii+Dgdcnv/uLoI9/njjHdhb7hUlsD3Xs1keDNIwN
3pNTWeUxyZTJzKR7udvlJMLBMym3o/ECBMv+uN4BToB/hl2qqhLvFAO/r5AFOlliZqDwiGcbQvyz
YxE2I3qA+lBeP2iX2/4t2ns07deHzxcGsGDpvkWpwNcM3RmD3m5puzv13u/mWj0iTjzSuDu+lCO3
EIjElwRdbJl/F7N/czlKYgmKd6feg7/nbSKTQgrJk+bEOJwzrhlLGQvovZgtfM2nxWwlvulcT7sS
n2ZxTDzZIZJeakYPGSP3PRWLzaOntLk4/JYNoQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HAfLWwf5IE4nVH0RKu6Ckfcag4YISAB7GxmA74RLd0WtgVtvSg/hiI6xjdDBajL3WlsS8r0EeRuE
7k3XV6Iw18PLWYY7xEqYXN+4UCUMJuuhFnCKbupuHsoPe92DFCS1iQmSCu4KA4if6La2soKs0Eai
lizBuddfJbplTj7Z459Jc2VAD/slvgcakh9coxr57R1xf3xL+SqtbztnNWXTWebaVsMi9o1R8+q2
Bw6o2bthJTK5AjuaNFC1mXchmICuCVK92/JyceC3nXwexvYK1qRmiOyoTPwPOS9/j/gup9+/1Be6
vYxlYOcskfzyxWLNti298ohd6UCc2uC5C4Rl3w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DzCZLHkutR8dxKMJJC1uS/LdG9PoCtj5GsOR4GKxJSZTHbAW3Lwb4zUisDiKbo8nzvAc+Pc3aKIh
FZY+iEihN/UyNBp/ZVBx4xfw4KiNs0WcNidwHxnj/AmT0YahVcv3MBdpFE4TvDgOFqEqCr2KvrS5
K14RY6HsADqifYcgChtDVh4X+2Nen/oSD8dZS1qLOsyQr7ETEhogVmc4Gi3TE4/HYjm8lV5GRuJM
x1+0GPRONu+RFuc2B6sidWODYyJus0b7HVqnBAA8gMcV6twjAADrnyIqZwnPoiUCKAMzsDKVKhW3
GrlmNwP5uDSVq/4QrLJ59GIzFy3EXCfFTYr7nA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119328)
`protect data_block
8sg7oDP5gJWo2DiyBMNozfAOE7PmiElqkSX9wsVnS1PWXL3C7Cm7OKjZo2qjSa694nWLiOWor/Iz
yJPjFqqr+SEsAw3hoCYZpYUJeo26SRApJ/AWkAoAIVvQ1uQRlCLy1VLNMz1d+Ip4fUY1MTIYT6yz
/TD8rnrM0GvIZUYuol06m5ur7SI7ybkMhoyjw/nZg3nVPMCeYabCsPPdEniMZ4S91blis/Pn6Z8t
/XC36OIef2Y8l9AUK4QcFo18G749GiSz4fK24HMePZEaLNwz4uyw/L05lwG7k/chY53hI3GJ5qRJ
7DqiB0VzEZyGmwr/gjD221tpJeMLQlcU4Y981uvP50TMm1iKAmxa71oGiQClCSzEiyf6nWwadR0D
7UvEcqrLWZg61UoStBjn5h1CqCILBBRrl4GPI0iBTTR7EIasPDzVUoF8NdjcklGCmu0VKlL+O3As
GOkObA79DiBuKiCLbO3vtwL1716vTukrnX5EPMIpqruvRaDNnhUmQj/fsxR0Y6tvnJJLPQguYArY
sjDIDL25jxa/QW2cBxGvDTzh7h9vaiGS4Z6qCOHHgbEj3u+CiEgB+Iix38PaslGeKPX6mmWP/dCZ
/3DDUytivNh0mbSvmYZuHhtLAujMkGcrGKn/+nKhw21KIoQeP/iaIS8hNclQTd6AzoOssXc/OTtQ
hs5YgyRtvLMMZs+h1ZRxRjSTdsGbOjyaGn8TZTQxQGze2J4mNGu/NBaQPBH2YW0sf2w99aQJaWIo
EdTRC6J2B6pC0DOQx2tNoEO3wnLIjDFVmLzpqQyWqHoptFzt6uupwHFc2DwZlXk8eonDa52NPkiZ
QDOnu1zvGm9jqvk4IoX8rHiUkDAKNXRASqL69crAIPB+F7p3w5ACZC63Y1ep9NR6FD1nTlrqJUqf
zyyP+mH6tjBjpw+zeaHQzSvfQXlBkQJdO0UaSfwU1Zo9Moz6eszn28wBTgJv4NZv7fYI6+WzJdMu
/GpUX5mHnTYe/uU6cJKGPgaig5cjd040SLWO/jvnBwzGhuWpUAHnATpgp6x83mjmw5dTQiLNzjLM
/Kex4XKq2iC3xYVII8OAMA2LpeTxETHD4B+vMTx+MpfROsU+8dmrJHjmDZXR4uDKpDcrTg/9ay+8
xz2jXCt5FdP41v5iPiIS+vHu5UBl1/E95NxxxyD0wUS78krs87caMUcg8368SoWPLZAwuEs4iKTn
ePFDZqWfEl0NRgKMzuFxZ1A8r8whaEVQJVCH3Nu/Yt58gRX5DsPROrt2CjkViaT6PYRYqvXaO8yr
YtQKocXiUqF8HX8bbflLlVmdSFpeRy2B7HJdbvgsZf0N3jGYT4d/NXGSgegWq0jg1Et4fhC9CsXm
XrdpmLb9QsvbvNO+uck4r8vjckthHCTZTc08tLDU5QbQXBUHgQ+JmpnHl5E+NPJZJI/JMKzXzB9i
ms0nbMojeTgwzhtfe4tw50Hl00GbWhssSTMzxHDr4wJfgd9yRxPBpf8gCZniza7nvXDDn8W5XySl
HTlhfdIA6ArJEvfXfvQhVv3R8Xrq56t/8l5fuTH6oB0JYYRPgMr8Yl+gmR79rCNSTA+B1qRQ6rDe
gFKI2e8smU6fONfQXrydAiGj+JX9lwmj4lVCo6LL41EaHyz6DeGmf1JX8lWUCOhLH9j7RVXTlJ44
Xa6Px4O0XRh6uQ8v6/JaKxVEivYBc0JfibMIhIgjfIfxaydel3jXaPqTIj+FyRWoGpxTGfTaxpU2
TzhHqNlf+VCPxb1iNNNn59mOLzLebgMii9LkYmUauxmjgq/rmUENaQmiV9MHepZXDjs7RBgQEm8n
PXD9dO+N6KYDym0Mzttli6xoWPxbMs7VpdkEjxygePgKP9myiJWwxlI4C2vgZr5sLWlHcFJQY/UC
fZH80bt+s30DaEWLo8b4tNQ3SqllXPl1uVpQ68XPdL5OE30arZNcSJvTQaJhpbUmM0YmCo4cTG++
3MRUloX46tyDizTO+0rgTtksL93mpGbDvnDKMjzwlln6GYld7pFJvp3k+IBRIEUCf6macGsxyn0Q
jeax6EMEcw5QwFgtJpJ/acHZSyxwdkp0W7sbzGqDGNQthPW3QJZ5wA8ER6iO2tejwE8Ia+qzukgf
n3OzpYvCq9wJyTL31l4GAFjDERtqEn8E/z2NCbE6huhDQF9PruS/WD6kOqxvAdmXBPO7Sa+0KQNj
lgF94VFxvB4ulrGHjjyXhdU1hZcKACM8kg2xx02FgeLXz4bkMBaGo9H1BbPKFE0Doq/pHFuWoA/y
1wtkIIbX15UzslMB0xmgWbrM5bPpr9nfsWi5s+6RFG4vEpNGet9biZfnACTYGrsCH18hrS25XPHF
sLDW/iFNIZg8B52VOYsd/gI2PkYHzMTCDcZ+Jc4dWVj2sr2eNG5TRFkODarqANyHpU7Opi6ospmW
n3CtgBR4BrnUXvvqnNHQ2uyRTFmMfVDy+gAJWE1y3kKnEEtwsVsEL17Zf/XvoxM03keXafFKNFWz
OTJeMZ2rdGaJPkm/+Ge5NJxMiyWbURbC4O+jlBdstbei8RT7QxvwDBNxkB7VW0m9QDmtEeaeGlKy
kQ8r/JhOy5t9tVa/MNNp0fjqZl87bASlS0UdZiH8ZbooB0sI4EW/S63PDqEn0iqnpVZkEHIbWOSN
xDnNrt528pn+4VKLidRnZ/aIMdvtB2ugTUvdxgbgZuynPrldU1yVXSV/K3ajJygbuCOpS6gfUvrq
4jBsZTdoNAgDGzZM5cb0rhCJyKSMgBm0FGK35PAbBkK2nLuhTHLC1Gv0GT+XAWs9y9rPVsW0Uyb7
Ozg0zmL/3nbKTKd1i3cdiEHV9Asco3JYE5AR9jUg/+tOT+XAKYQcPaDx0Lxtc2veGAb+Aq6B/mFk
d/sBAaQZQwZfR4RzU2V7SecmYrWEqUFXfp2dv9wCL1CoUIu02+dAGPGVsOgBlAm/pgIBmO67Rd03
StbsyNOeNkgqqV060oaV4japp1OjHyd9m+y//RzBaWqylpfbZrW2B+j+ewjo3LieK0PUh2wEYKXl
j09xOXgytfJcwn8Xye5LZO/aFm4EVM2iLAzJ/TeGbJrLr4Ke5Xx3fCmISNCxBJEwgThQTIbI8wa4
VUGtYwA3Y+MwXgps2D1ie5Yfo4LbRFHJ6kcOFZKA1xPoP0UKbo1mWOxEiMwgPuQN4E0XqRrN0Dm4
IWAtbvxJRACjqpVONnKRwV0Up9gIrjazkZMiEZWv/qUxqvyhVp/CIuqPX/UXOAVCO6Uy9NyMguDr
zdKkE5HG/llmo4TurhKsFoeTL+wxlhTRDtqYi0TYTRUaxWiX+lYnSqNOCk1uKOknPY9OFVgmT3Oz
kSzBlXa+JkdVNrAMeosn+mCLSpU0bXPMNgFkbYwy4+HHdKou+8l6aehYikKv9Mzfzpt7Cv1/Vs+x
uml3qM/3y4ViubrwZRvmqL2/qQaKWJarjYHq2SguolAFSPDQVrS4JcdwFzTatRJbN35ifP12PY5S
X7O3vOAHEXgFmA1cOPiXhl21G6Z3+4CZswPqcCBOKCjGOINYHIFymqs6bjp8DtatsioGdrN0CVBt
rNxOnHGGvOmadhYC9h9U/HEDvHiFXGjByUXZDA78R2NlBfHJpL9uxz13UUV7imlh1TbWSLgfCR44
dwsBNqbezliz1xImAdYrdWOwnkk1lrblQF/hMk4MqY+0QzivtRmACucZxVYgC1uqbHchtxy9IZEK
RnghuJBBWJR91ggb/QFQCxY8e/WIhq6ct2tc6Zp3mfjSbY/1ZSbnoP9C6p1a+wHHUZYv4qrfy6ji
7f2LbLS7cG0U9iQDphT0WsPcAgxrXYr3EM4dfmUM+NN3CSvVDUPv0OPLGsx5uFniCeoq4n74FwJY
mEnY+aPJZTysPLT3dYAu9vpsxjICvarjlpWJlvI1dQpQ5xs3ROccTp9owZ7mgQWz9Phwt1RDpPFP
ryhpMDFyrbAMQFpo632K8fhXnE3Og6GxzNEyMaXpc0bkwuAlzq8ahN13y8qU+DfwyicNwlP572Rn
ZC7kgjPsbn1MF3kMq3nXWk6ksflAk9iQnKZkizJ8T5ddQeXJbvxOGmosvxDy1dGlcuhcTS3cymsb
uuV/WwxqaGv/upr3Bj4g4qvdG5PLCc+f9brQsPKDLxTangtDtWpe6kL/8Wuxe+R2wn52EiEx9pqE
P9ntZiq15bhLPGKHT6gPzgeyThmzMnvizaiZhj3esXOJ80vBHEYgAU7Y61g3VJmz/ugRd10tIUfP
dp4x7eiAuo9rTT5PUDq5zeIuEW8MOwxOPDAaVivEeZLOTU1gch+ZKzKpxIrl00ztK+ZjRADjFSUN
vOuvrcdVdDdFNBz/7MJTtMHG3eLqJFry8jbhekOmBKuXAOgZcwzqMT+MYtQgFjohcxw+6mjNHM1p
EFgv8nL2gocUkt00KrEabdRa97l4GjrNti6Y8Q5zHuBJUGZuAoiHbKhyg1iUoeqmnvZUnZG2iOOL
jOXm+yHcv2zI+TgW4MsqHizux5eyTHi3d/nJxq5xIP31GPOOeVBYBP+uvs8FIa1rG01J4gkXl6bG
82IsAS0csPVfoUhOTbwpMd8hLTv4Rvcq0jHIYFy2OMbuf4fK2y/TlY0KeNy3U++qOCHKnxzwBRTh
+f9+/r9xzRQJgL55yZMvTHMOr3mva8eUf8B2qJcL9HJKw7FuM3ABtogEmS+qx4tnJBMzh6vv63Gb
tIk1V0tQ2wkCNUNeEFR0OpW4xoNi1Vxr5JTNalQF1ZWkqopxtkf7QhVxMIl3iAM0ccSqTqelIQso
KCIvAJeXXJZFuSjn8rW1R4fdSiAl2psnqzY9xIEmijd0xd1+N0nu55qfZ525DedC65Ke8sPdBiXf
/CzewMwByPThrPzF/ldn4pY7UEa4L5oCSQ60ASNZeWRseODKAxOyUGXf0tyAI7RFJCHW7FKbgQl2
ir+WNiXGHCKmMu24IL7pHg5sQJkeeOpff3wxnSWTETGAXGi349KR6n4tCeD3Rouk8nljoC+QucKs
dH3bhBJ+EnEXFlMa8rif9+E+F4T+4VdX3pFxDZBLk5zgm0fBV++LHv/bQpVqEKZCdtMeZSxFq08M
Wor85l7Q4pLpO2CcsDpcw2visjbawnbbWt2QmLYHKVzJvgQTq2AHxDsWnfBFExIXa+8SLp9S2LLY
Ut6W58H8xMs1ojModL+xdc/cyXygUPVjf55XMNDgzsl33UMWbLqZGSSEkHwrOMW+33E8oCDJMDCJ
Tj2wwxkd/WJWURKVAM8MTM+FzRCizp6Xrhm8yMU4U4I4bNDEkNTpRY3mHqFdBKxsnqdlAShecKe/
K9GOYPl1Yk9OfIHgt8gyLdkOJy71HmVGBOQ1Xxewz5xb7INrIooJ8gQNYfwmky4JRnD9CSFn3tfG
4j66Dq7Oo2kuUIazi5PjY56n0bsR7vRn37Yat5PBR9gaGBpci/BiAHYjALSNNwL5niVkjHrIyySE
u58qAguPsxHlUPnywMVeA3+Bmd27hOuhsUlhOB6FhcjsDNFl4EswFi+8k1cBpmgCSnlFmOy8eeJm
GEnVAbOTVBnkkIEwhNNUvO00vNwxYvlT3M/yTxRD4F/DarPzvEgkKsETR91B5TWeuUuPJUUu921b
/WmI1Ds49+MprKGiUK3KxcAPiZLUP03sBuAMdoxlEGmSiGt3pom/TsLeguOI70RQSIQ3rahZKWNb
3Heq4TKu67BdXyYZwYyG81+844Fah0luvts9q68I5iSw2zCsGbPe7NIQmw44aLhaGDDNPAAA4G7M
mRZ5wb2gioJXzBnF/78XNDbk1RVEM5TX5kpVPDudsxI+RwMSGnFbkxrmpLpU0J4JPfX2PC/88AKB
Jhare7G9Y9dnV53s0jyzXNupCkyomER3D2GcDrjf4p8pis39yVWjfhhth7CFERNa82ZFAWq91rKO
D2yt7Fx6Dw2nvSvW5U+wOwB52ZVj/L2a/B4Egc9QLbCmV11Eecu6g4ZG9TsWsE15ZKKYNaeQE44p
A/WBeAhAP/LVW1vzQqp1HJvfyaaVyT4lMnE3rVGGipBs/3vxuUC41F07Grete6D50lyGXxVlic0B
HBQj2Wi4nFRX+7J0ZMsl+PYymHBVh7EAOQ5IlYMIn0ojTdrntV2egMaNO650oclq3TG/2ipgH2cU
wKEowuMYlSFJ+EVfG2Wf+elZ1vosw0XS/+QVsNwreT9IOW67TJSovpwwP5URSa9kA4A7hO7rM1EI
dScTs3vIf3gDW+6JXPfL3PUi/6SM3B6nnQuv3+sEj3KXjZZPdnUdlBugrNk7iImWctBvEr5PVPtf
6wTgwr+xLHj6mFjUQywmz/FwWWqF2Z+tOJP2wwu493o56/gGRwNVCFQuqaCxEvcqEJizs3YjM2fy
/0lgEmap4vPo/8m5+/H29D0JUvkn8c1yxiP/KQKpg1Hjqq1viIZArTmI2b4wzRMr+SrGCj3UCSrI
O3i7Pq4fHhdzFo/rVcbXObZ4Dr2XIMS/5moaaN3cYyb14ijq9pd7x9CA35Ucec4BAihq56yd0y0n
VTQs4/wEZys2fneuv8flhsRTfJTIscCqU1Z7WCwkV7oNb2juWd7Z1Y4LW/Z7xQJg0i4fevIsy/9w
JbdZKhhjnkisZkHS4FqrWd0/iIE0LpRK+0LAcfZeXBFygDbKz4rRLp/aBwqh5fbjRX+E8zg7Mll8
6d7/RGTrf1PRRWZvd1SJG5nK9wfXWSGZ5YMdq92YhO45fgFpNK4yHqSKgFGFYF32J6fpgmCU7rA5
zZ3t0UyNbv51LsVx5KpPPppEnIkfu9pd9AvNoN2T5wdy9UT0vr6O0oe2HNtsQRtkk+vrAiVYal7j
it7jMp0TW0+sJTSo6IYOZw85aPft5Hi6p1sEzSO7VPS+UZ0CEUbNlqvAIZTHfl07CzfIvoPIuEIo
+hOxecSJywM4F4zdCPFgxhhbfQVuFbA/2mltO3atbUA6wBwRMhthqRktfgxueGpmVd28MaRH4e6a
9ZPIe3Cb0TqVFhjj0oqj6kU+Fr/tEOd9CCX+JFIDMBGAcp7KXTvf1APbUYP5tx6nCRnoNuPBvXeb
RzD6V1Qem3Lsfk9wc4CKEaPZVZJXaDd/zpbM+k7VgQUMRyFRNCHkmxGla47j5gtTqZOJ9DIeMo3o
uZrAE+KVYHf2aBSCatPh7qamQhTyIfnrcbbiee6S5BsjBhk1SXzqE9GacZeMimxf1A8bFeAsriKG
WB6BsGBO8qnF6akuBFMsKCzFvqA6tkx56YhFpFcyPpt0b6GccXazZCtj5M/J/4WejxvuD6my8b1I
giW5qTGgSDpfGEvBHa7g5BvD3S6rkqRkBuSTlhSosIM3/1QyMtARFOZXlK5h5sAGi0lIxrQMJHKV
XOn6knd1NLNHB7IYYxo57PO5B7CkcZqAw01z20M+hC34Af3396adh6aDmDIKXwZfP0daGNN+qGe9
Xw3M/v4yayFhD2CDXZUEQ7jtwccbRoBOVYCLwTRIp43aVwGgFcF4IcIr00q9Xrz1K7XlPOVpblxY
L7Hx97K1olF9eUEI6xD2Chv7/xChUxdXH+v6ShInsVsOxn/qHE1l3invgDPelbYNJ3+TY2ziVzAg
qcutA+bux5Lqg66Kit7nnfWh9dnwV25rrGkzER8mMbO4chKVv2yathvHdY++LTJtMdsJwdeNf3A0
EJQVhqPtJqhjtrcHcGwYK3iOXRFTXTzNNrHuG1LVU/VI+NFYfQwmoJr6NGZrRQCB6utCcGJd46e3
JHC6sQy2pMUwen2aA1b0TUDNT/y+wHUVJyoKyTggK4TNBclqHe3jAg6V2A854gTvRtQkArgD2fG3
lFLX3TrijUTbmzypDt+eydeVAvhx3VLLNj6AHZcTZfp95BuRe3tGTx74eP9FBHybX6o7IPpz0ypR
NH638hqzzlWPKCXhh8ktWkcrl160u2BMdHvv4Qxas/jKOtAnsaaDgWDHG59jJZYj2lRQTW45wyRU
RlYxIhY9wCkjdPNaaI246azG65i5X5fnaJyvgWbnh0Vve7zE53yD+Z5zQCwY9sHmrnLZ722o9WB5
uJodPzPceDFGDxOcak7a9/TGmHvXzzeOBbgmCF/yOclerFIw5IVD13HU9fanOOqLxtcZGHlOu4jw
teJQOBmWdrPvzbb4Q/OLGKZMKA28OMl5YjPEnqZrqxkcSRGHBVU2KqEfe2WbqtdU5YREBvm96uoJ
4HDp5ddeN4woZPTYoCtAiWFlYBfyJMkwyTcvAzksNUq7q37+DRovROcwwUYzy+piU+n9yYAH/LAs
7WfQ319dAQLRxrEKWqURa50zOZCm+iUOGm4WQFh8mKCnlKaYzscuRJfv17IclgjSJOqLVtBcNmSz
CKY01KnD3dkRx67lcDk+lbcJCk1Cr5FbBCB3iUeMSZ7dNfKLuB1gJycIDTa6RhYlm28H2hnzsbYP
SxavqVMWNcAyreHo7cmrDyLlWMdasHQDNYVDVuIMeOfj5aRJqUlaUZIOPPg70mgkjTirqJCG0xt2
+uvCkZaxMd++rmmpNAz8iOYCb8F5TMT54o692davq3c+FRzxfGFaJSpOjtnJDr/LnMJ7yAWVTy8Q
KSnPnmU4yZAIwtqbpsjmyEG0s9JtvGiOtg2QAVLzV7stdcsf4Rom2jMYnlPTpL0U4H+c6EaPYy+f
MvQr6Hk/EMHABNyCfK3aXNOoVkvXbhd91NBE1cBuGKLZ4rx+BRuutGwUwyyMxTLy7S0GV+6RVLse
mKWMtpSkp/SaP4R0Hl3yQ1NcNCN1OcudzNl1BNsrOkOghBf2NyxpiqXUQKkjziN2NRBmx+2Ai6JZ
EsgrsDjOzM+u3USyWwAFDOLGSxCP6oMcc3InwlsQepMVdmP3V6TRs07B3WeXnNct45aFDwFnE3gN
/LeuYyx/TH0m/QVpqwrlTHPNBN3gfC24tGc3/Raqp3xtVAemtmRgKPFTaTRVvSCFaW9boQY90K17
yka1ZH023w8KwKkRfW0fjoxMqFCY+O+PLnr2nhL84QUnBPLNtWnriPqEMiFvtQiNnfV332xExUvU
jmsZeYpM91gg4KTPB4WK2Wa6EJqzLVfuqUckCv1oUFQ2Ci/boDOv0FcvI83C/Yvz9poaD0w5Ee2x
/grTFBvLcn0JgCITz9mpsOKwpwoCuh18X8+8ViOSP1HiT6B3v8/4d+wJ6zL2pb1f7NgqOd1jjFIs
QWRT+RsszE1yNVb9mfmDHmuQR6eq6Aa7YEdUM0gJf4GnlPIElyJ34yMwHlNyBGh7tZonaQZQiRyf
R32tvUmokex6dZMhSmlauuVWUK3uVeXcpIqZ8Ta7MAM31oYINPdqPYJL7mzkqJI1b1WaemFR3iOt
XkDS4gxxq2HyB8l1EQT8ca0rYumdIYnwDU9R920XdOH+J+inUFeOohcgy7mKKZCXQEr7l7iVhiNn
DssN7e1sZXiQP1N3VDPBuewi0LvdilVBaMCaWlUhCwXfqVgBV2j84BtT+HBCGMj8kO7pzV2R7i7p
44QGtYEXLZ0CGEUJE+0Hcn/++SLdNTbJ02yi8aas1UAYnrW+7/fnGY7HdkRdQ0Rl/RA5YDvkDlFU
T3CUHB/y48vP4jNcOvsXkdgRs4V51tiT6WqVJoqMyrXCBQ+THZV4sqC2SkNyJNjTpPRGVDiNQzh+
0M0UnDtNhMMEXhXVR4kh+7ZI0z30BV6gCJBcdGn+aPg68+t3OXxqNxxmdESlojCEwv6u/2QZrSIi
lwKXlO4Yca3zyakNrxb47qxTV5OZ+oK7JGx1iuh3HB6kSczlMzB1K0vKnPJwvn2AI9NTH07tDYXj
y801eQL4yR3nbbBvN71zyPqVu99/WA2RQCTpuDkHr6YTNPxLyGwFPP3pC+gKYbCWZleq9g5ryVUJ
+2D0aaxUHX2HKOlI0Jx9JvaUyNjANNjs2/g1AoyI3i3liss/EmKPPRlL0ahJp81BSNMWAJv1KwhY
fQa7DLIpgmtIokN7Nr2c1vjVDPCR5DgKsXVKLgxifMi2n+3cYX4R3PYR0OphtF2/oE5XMPRk5dhb
GrzUxXBuVAzMwi5Z7p6DMshf5qPnS/GiTLc2daH+VrOp/aMWluleqOagVibV5TUNfO100nzR0GYC
sFPGsK6zwQIKjsn2qzJcoAD68eekCApk4yeN3PxoMcbMvx4BRtklt6QxxQ7BcqUEKiQjhHalO5+R
/qtMEGWWfRmOIhbL0XcPbeMppejYptAPPdWnrirqJU+PUZ2wiipMa0l9aqpq8xBedllDafQJadQB
04htsJvdVbZWG20BBzEy7g/rlhP0lfV6V+83rdgp9OlNwrtRiKhHo/bvFGPfJJLfCR9336i5dABY
3t91KdgrWcplX3T5PhV5Cdj+khJqPTFgxZSZdZsLwFBR0fqymwHKET9e1ioCYWKoc1n61CN/2vuk
C5uNfWSloLNNpoKYQ2jAU3TFiDM1WIcwjwP036DAF8gb42WMEYiYLhl9EBhlgkqulz5CBQ/vd/vj
+k1tn8Dooxj0ABWbPtWRj6QJ4c7NlYVuMltRU6eMU5uGUQgFXSCzbTKXN8SLw4+7y772TckbvBH3
bjTVUV7s8Xm145RAw8rcKwapRWaDbYCkL2XCoWTQpxAdd97vpC308o/s2iHl6Irx+NrtZtbARHJe
f3SdUuPK3Zv/AltVvvfj0+VWraQPmBEQpgvPRX0MbplBYmStgAmCdqBXfsCF/AL2iDwE2PQzSPEG
nbdo/CZ5+5IsdInCZ/OkugVOudm52R/vlXoWlxz5ZyuDpgObpNTmiuzV6Bhc7dI9MKqxAURfW8pc
wsMQGdecjvFyVF0Q8ni0nsA8lRLljlOhru9dPHWhUT2m3Ndr0U3FiMlkI91JT1H4tf5eturw+gsd
6CdbWA3Usd7WB/b8keFGBiWvQNWVC+7OTu3xMsND9g4lA/0s4SPXixtKp3v3AsdS6SySbw23WIIU
X1uSJzade0ky6NPc/FNvo98KzqNylv66H3BI9trqPMWNHUHzrT7nQTiJiD9xyCukWt3U5r3NFlvM
s3T2/8Q39L+uiPGjZFr9ARlZCY7ClM42OExD/vZr2nhlCRkz37n6RUhg6owblfWSzxATY9xgzDRD
+txOTTC+vGTNkUmn9LVbrafBIBv0QByE5l11qXIDSNn07uOdTobjLo0apoL1ZWVL0iyxSI3IegbT
BnU6nbqI4+UrXpYGxCYmkIAvP7PB/IffJKSdw+uk77JR/D7K/YWXadPQ48FM81zdyp2oB5W3Ddwc
gN7twhSuZ3W7x7yNYpCJlQvWXfMRx/l8+11LLvN1fUv5SbpoZhoBfpY9KghM37AcIh4R/mifIahI
pHtAh7eBoDIPkH5sK3pSQrP6wLBDY5/3gqS9nEiDlFGxeOuBbXGXQjyTK0s9VlHnwXNPNY4+v85h
wVY0OpRlLGGcNkCr2gnuQL8B/B+G/Ci6Hya2pvpackS6j5oZd7gm/OQxgnFjEQ5VGkPORiic70q9
FGRzTjxC+3tWKQpEQOWnPeAP8yUoFwn8hPVxwqGfxAa2XFO1dgKTb5/p1I8yFnHspQxrGFLMb/84
mTKdf0Gl9b284SeFj8HXhq7Tc3a2gBH4ah4JCHUrDHL4kOpEtTnzvtzCVpf5K8nSQpBf43u6bmNT
EZrnSlMO4ebbnCuBNN13gW2jXLSiVdtnYUViVYocW3YZQ5PvSgSnd8utiDqgeip3Jawpk9rbAKrS
7ycKs77CUIo1LIxQZh8YpmIRoiqLi5DTEWmJPFLdaFBUsNNxCMg4JXqZfWBoE8ldS4qCAvzSVgbZ
sH1049ZhG8Q7DXmefCp/rPnGDE4lGN2A5M5XUhcO4jQAHBK7rUdCVN+8KkIj71SDtX63pgjtscJ2
itkiGmKri5rQKSXUZRWEZXk/3HW1LaebxUzxIdRwZhS9bioKMEVaKL7Bx3t0c69f6eSJTacViaKy
9aGeiypt/Y0DUGEjW9aEgzryLEO03HSEWMEaJcyS3K9yvMpzTBQfDLf5aNHh+I+Cwo5Ic0YcqV8p
F3+Nc2VpE8SeCJTc+ByHoGyeLtbz8dWoMbRAbPnTogA1oQfwbA9yT6huOJYlaFwq7+O2zayqQ47k
9lU5Hlw6XsD1nItYfjUEljm1YqV+2nQDxnC3kGm317z+rsyoj2pAIl85Y9Glg77YdhOfsAAxPIWg
epX7E/w9al4M+e9n9I67vziM0p+Jb/Ofex1iShfbuEMQ4dja8O2VONEBDghsP3SmJF3j5rKUGmKw
88Nnbef/ZpZ+mgz725F8DmYorbJ/n/SJz+ZGenSJh9nOmQxK8mDFbFkp0ZBdaqwiFbHfVKvnFnDv
M76dQMFix5qhq1PCaGVDV1t//ppHNTe9qnmVARLQTcbROZrhKBUIuLTQlH01hiJuJzqM6L/LyVlE
Os76ACXIC/CmSYE4ch6TWKcQiC8tNMBfm6hMMOMF31m6PaoHNGfhBsIryQnB3AjTkXFCwuguk9KO
K1W3TEGc6kuu0uQEDiMos2O1OpBXbLd/Z96t3ubyeJqIbC8oofKfZ2Nqf+iSP0E3c9CEEWVRKiFi
Ij0Vn+Lf2wlYQdF+1RBC2mfZzjc1bmIRKnKbj7Hh/jlvASS9hbiG5GD4k+F0U2AFBzBaF5PaeBr6
6SA35xIF1oMUs/8bZ6Obhc9AZttYVOadV3pOpDxzt/wIV295AsBVKeCbxqmvQkJS3jgv2CXSXhO3
xlSj17fyXR0Xz8xWnj/TKxFZoya5Cl/EGmZReTWA+rMuFVU5HiLn9iL9f9ZanPavIqL8Bg/HnAGi
SrS47Iva1R09HDeMRj9N+0iZ1LUextQpj0oAvHroig/FeHKxKTrGTOBs+yboObbOU1Wjwy/y/nu8
T9I7I3DVOIsn/66SMMV6o14arLSylbw9ainH+n4BJ6CnoZQ4GEKlYfk9Ul5DJo+VJ1bgKJ858hMe
5iJFLeOhVL0tqHlPZzimJwsP0XtTING4UwCjqK5sQ+4e2YuJKylMT2BK0GGlVOb8tf7sam/g76Al
jIBcADUJSnTGNd5BSzFnlmO2O66++PANdLfYRifEd8xLm3BBig9c5qu/tVTjlX2NBtWyui+bondA
fiZLPVfj4PUUAtcCaLcCv+wnWCFlbHvzWCJn6U+rqadoXaLoq+neGukNbMjYfjLRNLHyBbRmJpxl
yUeUC/4sqTEhr8LZ0GlhuScXIwYux3khV9WVWkWAI+XISIOoBxXsjEzrMv18yK6iF1lRJsH782St
qdUhO23YKcpk2ompST/4S/uQMPm6CT2NYKSPo96ZvansVOcPW6vl9SKMy/Gipni+a9BuGTuXFVKR
gpFIV5AjHP6RBts8h+E/7tyU6Sa5esMSnEUgKWqF4mkYf+Rg79LbOh9kLhgHvtMX18JHJzYURUj5
/kJF2uIEfWZXpVqkTJwquKX+5LCeACR+rmntgzd1N/R8oweIWLhswGrRWNj1mSSlAjLfwzModP33
vD6EfdqMnrUOv4ZunXnv2D1cPMzeRvSDP0stoHAddL91HpsIgRZqDDEpdFBvDu0d9G/T0HAO898h
rX6h/V7hklggL04P0O6ypKxHVoY71wpahu8XmFsQhLEY9ZdIrtpNeEV5Q0YXMDQS/p6VOr/JCxiy
JWDNQWDo6+pYWmJ52XcIwyKNC0lCZAux2K6UkQ41UgvURVEJTL4UFx2GAesZexYU32bQsdlAd3gO
9SlHzxP7bjdQlNqrc62aHtU6bvW7sYsrKlD0+BlbFq2HpG1uKkW8Fwe1ZzKGXc4JtOVkmEBk8FE7
U5mSuapmC/gLTyqt8cYUcaGepzBY0isa01Vht9s2PpRW+yqmnuTPTgAnzlT+V06b8bARCQWipzaq
VZRmCkc/mgVY2prkasEQwEhz2NRReFaF0yVR6G1rc3cSohjque+ewouSd7ubA3CEaxJcpgi3zi99
tSlPzkJ0LiZKbvBCPsyf0InIDkmazJ6cEyELbvahEVI+yILCJYrl14daQARP9IzG7qv3ObxIBJmV
ZiDvs/WXjbPKhdqemR1AC1wT0XdenU44dgI4nb2F4fJ9pPMlhZBv3xNlUmdRmnJE/uQR/OJ7OCL3
9bQ0T750hZqUvnJdZ9Xv2X4F1KZlya//B3AZBfYM6nPm4YXzoUng70Od5hDdcJZoi88n24Ke25cN
o5OLSnP+rHA7YWacu9BS29azfbdDTcPN3fi+1mXk3JC1TRGjcWf07qjknajtSQbR/DqkkRdHgtSC
rDT1ZTZ5DVJ9iGeC/jQNWTYhqc5I3tiHmuPrP0VMclzXQ0NJhiFtuLYtXzZRvcGW9d/YYJRfTUgB
8vazkP3TrCSJQPBoBs419QF31+VpYs9F7p4x+9F/ihD1MkQXk2oBTNplK9Nv8cGv4oqUl8Jbdpos
i3GjiG+oQ/Htc1s/p4aUIBCWoysVH+YtEwTUtoODd6dKWS0FD37iC5fq5VQfDj2VKhyxWokk8MOc
8YDZBHd2XD1oJ1kimEyXU4ZtOGx6bmcS9PnDbl2Y7RBgk9mQftFZ5q6P908zOFs3NjECnhpN+NLE
e+tBt+XXZadTFKybd8BisuKsi94M+F3+FMSFs8Iw2Uwjy3pofn8AfcGO23mKlcPGT0jKj/N2L222
WsSLVGiSW4TGeAE8gC6QKzNb62CJ8gCC1drS40COtZTKeOPP6S7nhFPShoY8PAEE6o7D377DGnKK
nI2y24nTZ4onR9G7FFHgYQdWjN6kC1nyGtRPvRaXTjWUe1cRp/dSDKFs9l3kQwtP1v3SBR+R3igQ
NAFfhJXvU9p9ZSy0LmNy8/44Ky5G4/T+5UjLpwB1jYXkVHcSRcB3GlsRa6H5JmIoSbGWCL+azKfj
07pT6s0XkqYg74U+ET0u0gJo+M351Iih1uWE0dkLvBsC77lYpqJ/aR4RDlPcTqClFNLwhDpXvv3c
kRT+F1NcytS9AQ3oR0L3UAR4Wg0OIOfQd119S5UPhCFavC715q4i6U4D9Vxv1SWEj58CThxMBjKE
JQLbUR0FFna0x05pDXrPs+l9JnrmjRFvT5HY4OoRAunRwxTMWYnCXNq4ESrWwnHZ63I5titwtSdZ
EgmeQbpc/vAHr+Ep667Nxru+Z7p8pmdKEC9hQrQNFOXfQ2yB63z6S5D3WwoZD5CQoJLNXGc7MTKO
tr5Zy9RnbQSzVwp3zcqAtKHRLDEcnjgEP0/yOWIsc1vJW1XznsXYsie2OhE6nZkf4OuWLDLJk4ap
euUjrh0DB+EKQAqADqzro+xn/nBcGJ5/Fjr+b8d1pIIoBkk2RKnlkkOSMCRg9d4syy/JeJrbhjVM
mWv0rJM9fKcDaHxs8+4JRVr69WZvpFyJ7rNOsUFOw1lDxCXkV6v00ODTrVBK9/msTHcgPGgtG2Qe
aDEIAeqRHNUcl8ceOxqjgTCwvAsecDBZyZXaYkrwzYqmS9V1wkkqbY9QwDGB0XXd4JUkvlQUTb0v
lyP2o5vdMheWJ9tiJMN6Vsk7FTXI5y1CtA3+3uk6fdbHvYbCEZqtVp/trmY7wrKM9q1KWVXSJuT8
rPwqeWuD/3/4RYGTWE3K/VPs1HOiABvfZvFnR9gR8DA3yIaqICoZAbXokLjQUAu0WcOoam/lim03
9Egl4E6HEVB/kPIS3RPm8mF55PDSDaN73vZAEYOr20qcBDFO5lpmTtkBiPvVCo57aYyw9FLguwJc
spIJ7ANL5KALyr1zK5PCvUOkempRksMRwPB2DQFRf9fbXoEWQ3GiXk+WcrrnbrGtO9P8nAhvktFi
hDjtx7oqyBN4T9fzytMsooq9f7xXYgC1so/kvDYAa1mmwbwlc/mlaoIbN670B3xwc3NgKvU48Upl
idXnDINqcXgRus330PpmbrVkncSAytsN8ptL7g4Sd3Upb1TilnkE80nXu90jjj+K5Twj3Jg0JDYK
0qtutE99JOxCDNvE2DQPgUrWORcUmRq4xNCeRYDcbaG+A/9VndzexNOtr8NjPofOiD9wot2TcerT
ITR/Lmh86ijXJViz7U6GMUWnSfCQVoBoPe3eYndW/J816x0BfNlWNs2+3DhNqh3ZFa7MjqYiILK+
P37GOww6L0A6IErh69qF+Sfedr+e73KrWzcqHrNlDcmNGSGGd+651fUJ4nivCs3FeylBysgSSlZk
AIm29qHG/JhlZNVz1bbvIx8UbJ5zkIPnXJaZPkJ7zw0c0SZt0hU49S7u9uas5eAyqb0+C25C/b1v
1qz5gsbkmXmHVwwvueKSdGh3/tSVVPzXIE1krd1KKOXCZUZtVlGx0xe/hJZarxZ3+g89ENs2DUn4
XwFT3LTm7IerRW9kIQgEpzqA/u+wOQhpn15qAyVBQdBRNogpLJYER1Qdk68MMN3QNu/1VrRLFMyE
H1P41wXXBrZh6TViO/Df6sDQ2v5L2X3QXVIjCZtzJXWDeDjIQuFTddQMtn1gQlLRPFSkKkpTq98Z
Wd2JWbe7v1rElRwerNvOofAro6KEZPAW8OE9fHcj0L28vomBriW1LNwWVvK5++VC51IRlrnP8yBG
oV/800cVmWvrMeahSt8oKk19XEoMvnZqAuo2k4Bsld5Ntve/ceOU+IDfpPuzb5CiEOxBlWfMjadC
2jLvPDFN2YRx7SdHPt1zp41xcdsIHUd9slLb5CEJKi1T8gid4VZjAWrJTaFRWVkyl8f2it+XuUhK
tZt328b8uNt19gjaltrx6xC9bntdiR5dMZI1W0KX4jBdfvYxIzbpXq/IKRPB4/Ja7zsJWDC2vLxL
8TQNZ/+/HL5ZgE1ykUMcKIkJjeSvPnb0+0ucVCaavUrGLCcyoQ0njI76IZQi6Yv03zFqo4aF4VMc
nS6kdt1SKV3Yg4sph1YqUKTKvPvOs5QWupCBdlpyMiCeJ25+f+EBPkHFjeDLUfuIZk309j31Vyfd
VsstYH2vgQhdY0Mqbg4RuD9LMjzhzXx0iSyYfN05rDH9gip5DGOhkEBjYh6Hc1B6Rnx7sy5eybZn
ShtHhnIYwtvcMWectca7LqBwjrjap+FXOl4+2saTuFq+rT4nJXZgwDBeg6+Qo6KnsVzQoD0tBKFe
QmEpMFA8SXAGZiQ4kxifvVtBwQ7GEvud5aE0IP39HRf6RY2C0xKgtukPmVIdHgZ5ONK/KkparwC0
VifEDwxPKWLM5kILhV6y/Qg3noPWa38YXzB3bxO/EmOVz9d+6T3eL12BXZeJ1ACUtPz30II+laqL
E3KbsA233tELDrR4Z14NIs4zXx3qr7+o/I5Xy4pVmViWUFAI0pTvwUvXXpe1JYkzAM055k1QHE+T
CtDxs8IAzWDea9UOBcV0XDrl8o+nfKM+9L01TO5aLwHJC3aCiN3oZ2yx7s4fJt/kO/epiY3HBjg2
jOHJcxZ/lrzExy2UQyLuga+BP8cBczjrEb9RrBMLW1RhAwffRgVbaKL4O76anms1+xwnDSQ/q49A
ipuZhc/8TxpUBR433IE4EZ23MSBXvN6IrXd9Qbbxogqzcmx1UXEsdonm98gvkf4DbkJIm8+flauA
EsESRgcWixYfHoMUrdU0dTKQF1iLZxD0wabL6+b9u7tM6YFUbeH+e2C0Wzh9JffIxX5zhTppF8pe
BZkzRtMfTcTzDwFKexc/Nzb5CdlSlqLEx5gJCcoMfiqh4mAG9yWFlCTVb35sy+mCJCxiHe+iOhUM
BpZ88ECE/TwlNMJliVj/GRE8rg0f6NC/7oAOOk509OhCSlq884qFLq0HAe7PkBD4nY0pF3sHgLZ1
fzuN78QSKI43+VVgy+AjJGaA3CkuLA3VWN/BQ/86HHJ04qD/ww4HE9ceksintB9m2aF/JgWYj5PQ
ekii9oVWXPwh6fguVYVsuFstExFhFYscjzAJf7paEg/8nPZYsQTV2oo+xRW3rEs9F5tKqTkm/beo
AEoIPLBDHMdfv5PdcY3JvFpGRO+XNEWtdcB0x/1fTjVSG9rsgAn72Rq4y7FpPspYEHjwuLVFeRXe
RirELYxNgGAZLXJYye9WzH84rPqwGIjQaEe6qCjCmNJRuknS8S4/drjr5IzGFudeCK9zrCzVqed4
NYx3Op7kuVfof4B/7WWWj5SBAvRKKyBjXif94/Q5YKzW1Ne4MGHDS/lDFg7rZJz/Cw4px9K9BIRW
y3wyHHCnkac02uzcK1iGrBjDXalIErzEzEQoGkB2M+x6XVctHROrHUAM85q3k7Urf/tTsNu7PvJ2
+HrnmrkNNtDu33V+cAUUuym1JYYINDP8sppamlKIrXpnJqVy+aeiLkN4DtiNOMXj87qFRpaimKNX
MGb2RtAiyILjHvzhyeN3/QfKGCLOVNULE6Gnuyonphpku+B223x0iGzK5IY5yMYLqX/FuEG5gAU+
fsbhN37LX3nd0g2l5ogg4WX0ayGaHy85xFMUwEp+c2RSI8uDwHYM67RQyjUrsw5uIHcuqs8ylax/
QJHiX04ZVOQ4plwtyycpi4UeSFkn/ODy7NeKx1wg7jvrLiRlXBlKkePADsBOSrbMEVpX3WguPdac
MiBV3saZrEdnGjR+kMoRFBmOMIwCx7yC9FSjqMD8kREjb8cZo4+h3nEv4VBaW8L8HJMWDJCVu87k
C4qJwlkb5pR8Tb2D51WOCyJxfYqmRqXUghZuSYsRskn/XocvSDlDrxKxPn04UoYMxOYVJbQb1k/h
EtXrz337YKYdl63KG1oa/i6OCgcdszpF8gEzoz1ex7YbcO09uGsRKIwuuEgbM2vMRv8Bm6Wdd94R
ZIhrDiWePZSCtowAKQzCRPHiWFT6dT7GCCZyEw2xcKna+XNudr2IAgSNWgJUnFlQPd/kWU/nURAm
esT58AaOt0XLPwEMnNiylNYiqwa15v2CgUilLXmaTSUUeyxXCYrsz95r142zaaDL6uUXmmB8Jd5z
0+HitHh0+PTHg78slgKYqoQfmPm+nC94mVunBz4ea/vUdbu7DziIVZDHYODguRkt3nSXr1f9O3XH
KgB7pyH4hRPWdfiW6RuycnhtORRv3CbuDlNTkksAr70cxtaFGGpnP/Hh34LGVNZM/t7T9P44q2MD
e8zma4QuSjDCl3H4kLJBe/KDQI4QNtFxe9KC//L4JG+IqHZfvix37IaE5I7izJQ6jWLoblLdk5FE
iZHTOOeLqsUv+0TqK7Le8f8rkrblvSHAfvTKh01GPsETeM5OWf3P+n5d0Se4hWF2jnhjCLspyl3B
G18Ra4PbA7klKG1NN8VNCVVj2cCWRGCJsvhi3BVzWnfgR9nOl0gX46Ojc+8/2RXepg+hw/KvgKZX
qQQOGzP+mzCp9arwtN4oC+ca61B5QRFlSYdUeP+oEurBMo8wSs8uVPyh6PGOO8rf4xS3NJxkMVe8
fEaGpLGiULWpsamsf9fJWJ2tEt6c0MTIaho+RdWyINJGEMyyils5sTbiJcOh5o++BqwAoIhElgHt
i//XsyfpmLp3zF9GvnhSnd6o6o1oW1Zy+6p+UXpKT+ZTgIBC4FN3A/sDFXoTu134amI6Vs5RHqP2
qmobBVWCi32MyjLQ5z8zscjJMMMrV9mP9kxizIgLDhP3L2VdlTVbHk2AF8zH84svYZ4hotdr2eO5
rggl5pdMFaHFn4n43o4yyfVxcZR0TvxQgc/6D3Gd3lvAcwVBrfOuWJhYzaRkwdBHSuR+asxurOLE
u2Ir0xX+dvkptaH/l2KV197gfM/pQCenCmYR0yGAmgenLj38gZmRgusvQg9JudNTX1kI1J+MC18g
RwLO/3+YWehrcJzxHi3qHxmQwPFFn6sp7tCDOgWM4FV+ibaDb6bQnvbtuFrn01UmCFEBz5iqCHfm
I+m36krkWwToqDK3hzveI1ZOpmoxd6eMKSx2FUYOtb/rtR8iNrg4/Tqz3UG7R5ORKBZnhnXwLVO4
1MVRtyQNMXpN3AFi8YRAwC6DonRzuIzR8yz0PpA7TYMoxXTh8bZQq2m4zSM54PeT3FA3utD8paz/
7GkQkHyPyZHkrIDeGdTMEOcBZBVPRVKeMqIIuT8B+2vrgaYmhDFYBctPthID5MqgSDolPS1zLbB0
/g+HUGK9p0N8k6L6jze6tOvQ0uC+JIUn4qSPuWXYs/bb0F4S25iiI6IdIVlZVmgmSZSxT6pW6FmM
Mbix6NCZoQ0OkwP0sV5tQku9TgO8qysFCMcJ/LVxX0na+IvsjZ5D4VvFyt+o0PY0OSTyg9X7Ql0m
QAmlVLNr1UBziGtASn9IxNA4+ydXCgVp5Up3A77aXRhvqtliNYMsZ/Z2T1A9AXuHWHTYA4YGspa6
w7NgYvCnH4CemLDcBPUg6FCTGUD1aVFs40Xk99yGgTYW4fUcfVOWOGFtAXbeMiB9lGoKImENFWcr
cyD91j/JPXyEssqwyZbq2iMHnmAO1dhD8hdXTWbrukZFqWR+6PW8akdOm21SlJrzCA3IMCAluDTb
1p1Qq5CrhJcGad6NZPHxIt8r6WvirZKYAVeIYsmAJ7G6V/ehEzk1MWUPZFfMkzCC77TU+oARgafa
e6+HoZ012YfHckkVYY3C7REXsnab40j9/MiNN1V2/8En8RkpArDSGA2PWaZ5V2sMW7JxYnNSeVkB
jJZ54VjGOLdgn/uj72bYynEzSkl295tl0Z4l0gQnHY1XOfBmMXTgmMxfZEfWnzglfpfHjuB3Er4r
7jXZnsOUATw5RG0GmhONRLf66sA8DuHTpGO3UH+heTCGVyMTS1W4V+FxSoc04DZZH6YUWVfwadmT
xNPmGEl5Ezxs1qqg/TMEPY0rIqOqU1cG7KCWK8YMyMk+a5Kzvgx5651GOqQ592BU1d17eQqkd03q
VRTisq7zkJ9avbn38gEsvtH17xWnlu+6Im2e9kuuT1FjgVKSO7+p+5m+0BJJL4afD9ZH8zdyP+nW
N7UJDmHSasjqFj9GykW3HeLgvsud/sUeh1ZXDjixcBMkqO0vkqqpsVSApvUj0llmiFZQBUX8g/0u
WJnpyRmeCC+Khc5kRxL5Bnk6Ue4/dSCyFoX3zrc0VekXAsV9uLH5fa/oBASKH+ij3gomImkMnWV9
2o1bboqDW9viKYV/jIu/ILybW2VwcIi8O6s5ClhDQ2kNVY19ityxDTRoGEpXd4AXA01dKiryMF9M
e7JwZn9oZzj1pETa+1dnkrJJ17swSlb/wFx5cCn3mZnq6O4zZlNGQd17yoedyQ7Drqb8RAkYFc+e
Mn3Of77hKH3q9JeFjZClFu45pZppt1ckrV13jqObGjjmyanVSaF9ffqFxww4prHIinfsF5ZV6AlT
lrWQuLe5SroUI48aYHa+5ej7k0G76rhKPxhJfUnKTDzp8aTnd+pyAglr8uakisQn+me9uLPvZPcK
g+dRMttOuogqo0Zz8AL4BW372YapB24xOgvGjziKwuIvWpGaCNo7OKjLUHvfKKR3ah6+6oga55Fa
YC0R4LknVmypXk5XsrwB4vMnGQyldT0jTGumepzIUpVmfqO1MPZF7H2XEeEc7Ou6a+2w6mrigL9+
H0RePLwaY6KmLHs9hsICMcFIz32KprhH5bkPuWQcbzMkVLr/rBCG2qrAgI5+qSrjpoyju4MQTIVU
l83wuWFUPPWUJfP+09PZroHpwEuX14GMfT5cr3PhGl6fKC4xreyJiixfasVNPGaZZDdB4UxJx0ZG
Lu+dci3pIwNCTm5yz4DEKHGb0D14mP1CO4SxwneJ0zQEAwkfTKeb2yGUyJFGB3hSZXE2HxPFqJ5l
8J14/wtqx5V2TOA8veD7dWUkI7K5Mm596HlFbIgOdDWOuZdPsL6n0QYDqYYV3f9ZPqv1KQILiuXy
R7S1lTY4TaNa0d3JxZ7lfBe/dAB3VdJETFvES6qMjDRUDP6e7AMEBQy/eesAqd0sARvH6uCXXfAc
gbWtAWV+lxa+p806Ezvjm/eSpe27rp8sLzbjCX66FwUZiJQ+tYzeejjK84KbysZfX4WRJaWrdN2L
XXbkZvr/ZH2Pm2DNxuf1ZezbqwM3SWDnG1NtJWLzUcPQQV17gGc2yp7XJ00eX+Zx6eoBBsumaTsi
KHB6FhOcCkmSKv6SZbCj3GhTSTwTqUFhB1hE5NBvY3bMjyKiy7s9/6WBs5iHl6FH4lCiAxrmEaoP
3wnu5bPPGgZhOihyekoW9QnYeMhYQ5AgfJ4/Z8taAAw0m8qRJesLnyLgWB8dialZp0KG3qliLYhb
CfrxaJZG0N8VhGcILUUSo6oR22Gl0vXsh5YDdmMdVJvEcJ7EFtaS5Exjea/JM642MuXT0Wj8iUsV
4tf6lQOIqUDTY/N/RmVsVJJyY0tWphQPn1J1zh0YZzEXtAgAIIxm/eP8DR7oepLNpySzQRq5pMkn
VTgT0BQ40Z3Ur+xE7VA84boJAoJtWY0v3dKnbWHiJ9oRQb1xHHUkf9gx1FSBmTtSOh2cf5s/SxGH
E8wIQXy6IhX2JXnddhX9R4McrkhR+hpQxaiQO6cHpTYuMoUGqwyW8TuTYZIFxtHGq2F8UnAz1aDV
o3VE74Dbj0D4sUAaql2dGnzzmYhR9apq68RKd95ljyOOqwQO455PRXQikfcWxObXZnqVPX/jKL4u
6mM0jcTuw8nQBzjUAUDDg25riA61v65a0weVxToJBdlPoTE3J5J3N7U9ps98qm/v7dkMFA9cY53i
pb28CG6J38QQ/YYKwsdvt3LoQ8nEw7wiMri1BX+pxF/GuK/fpuQVd29Yi9ofrIKpULgZvRgoTOyc
GuPcgq14zRutlZzOSRk9GOhexua576djcQ9FNCF3FWNFT8wy4WnX22G0BgLHbYTyN8KI/4v0LFYu
OxHIifEWBu6gyZjY8TyAcY7zEKSS15tN157f25wi4Ks3TzZSBhFxX3GoRPW5Ur2n/PTjZ+frqNMW
AW54DdHTovZBVmpJJEtCkd385ryzqxtBB6fn4SJ7ARwwe7ozA9/opRLU9Y9DRe5JF0/3CpT7/BOo
tGFHLmEst23EdgsQbSzi1QPUzyxkV244QsTGG/OVpFcjRuCL0lgOUd8ewgvV4eB/xMt+20YEWy0S
OYGNpgwRI0QOwHcaPl4H+FDxxkdUCuEeArEbyYzD+t4khNMc56PCQ8veTvyxR8Nfk1SMowhwhr20
zCKNaxzGYdsZ62v6yEZqxQHJrX4uCDYfGgyeoXr7xeoC7LOwbuKIBW9eB3QkFlOAaemfCusx72zO
UxkyzFnrLjuf5OTP6yas5ZImJaVVXjQpu68JBC8qJIy/jbQsFsgk71KKHYvtbLEyrY/NqQYkaEyi
kqlK/Y2kMKq8HveGES6Ng1ztgS15v8UqgSZEdvbamTP5GCVTDc1vvCSswwssfpcYezgt/GEqfFt2
mseH3M0/ooPCS2irUt4ovR/373B65VZQpmdpMAvF7vIaoerH48ulHSPkd/Ku5VFEr6fHQHaBElOk
OGHbjYti8bLZnzaHJ2OItQsyQl2KPnb8b/JyX2CDkqsnjQVMRl7glOqkwCWSqmEWEaYFPuYfJVJm
tkbvpHyIMXu4F5W4Nz1NmXhsklkftYCwwfkqrQyu/iVLDUIBt53pZjCoDhpuu/7eQuHWf8Lfmz1S
gN0NDR4KSllqZHfMD2XHCqo2k4vucLlVAv6GyPg6qVVjyPTxUq5D/9ZQEZfX678An7T73uo8sSQG
N5aj+OBBBkhMmPqWLVQyE0MBiV8aX7l9no2mu6Yd0sNYspbofUAgm09gPk73ALr7+EnqTEpkVdSK
MGPgXUPNTGU3prrECaYeLWGOhDd6X6b3FYWizgvg61B8/CCHSYX9Kck+KGWudRkxVhtY9jOcgwT9
96nVy7m/UBlqhNYn8mwd2ubMbNjMCRhvohSlYBDQXprakrE4vlI1q7uWQf5rdflDksw6aSjB3o65
rbwO3nhKcQXPDre38HKRHCxrgDnqnKwx8VDVBkIVm3y9gOIqhO0mB7zwC1aVdIJCcNjWC+TIEead
yrzktJAQ2CPpIdj+UHlVYOtvH/2CqoDF8CpqsJ6N20zA4OLwIQ8W8rFdoghUIyCmKMbEsiQbCaGo
1CCS9jdbyw3Bn9xajOl+xxIzh4InoKqElgOpgTn8xGVgj2hKeqXStRIC7fAC24KRTdGOj1n2ZHyf
swGQzAaZXpyzA9XBq+q03XO0d0TLi/HlwG/qQYBTksYqa9OjjYVqFJHEARmBOUxRMp6Ow0s83jjc
F3xS1B1L7se6SNTFMYDaX9dTOJtI8ia7x1QX2+2564g8O6WMN9kHjmwYsJPxwgDnz6sBcXrX2yP1
NlrxaEZUl5/wb/1GfVq49eUHkzAhSadvMeT65efK2+EymO9kkR+SFJfkD3TAr3AHYpcySaBcr0l+
OmDqZfaYkc7v51CYXontagWVbmvij5nEQiT0rcIAZ/QwcIQwDn/tmm16tl6OvVbNraBN1PVzIJ3U
y2H/4TQKRuHHgukbFH+JWThkAXajoyLXrxaxcRSZ+tAx4TfpRtUB43IdQseVOG11oSsIaWXiYhbx
Q5yXLO9m9ZJyKaGU8U7B+TjGZLZmoQm6rOZX0rGIxwaHTBpF3BDIwCPMiFGG2/gIKfEZ9xMxCb/e
KxQliSSocUFy8kMBwTNWG3CUMjTTSCPPwYV8ITCsiaBYRDDITUVskkydhiJAzNKpYoh5u7pvxvCI
lZ5KgjWYBdwBTj8G4GmLNEVRclhkDezk+NNfAVzX0CwZSJPCe2XbluW/2uii19XFIrOchWZR4zZ1
wt2buA4l7juKgQj6VY552mjG8HJZ6vPVrHel2lvs9MCEPtlRb6HerQ/bv00KFZ60l930O2SlBYLC
ZLXiXn7uPfsrPoWopiLUTPZtoiNFbSGYqd7PnY/OrmWRgTOGkIpkHwrVNJKNSV9q8GBrneccHDyt
V9RT+wgq5W4DHd7xUGeFZs/lhD7B0HuP4Qlf1NtzkyLTzZ9FddK+m0v8niPLJ2xtJU8+vcxpE+xO
ql3RwjPVw4gBLa4B7NYXbbQaHRIPzNsdz3ptOLqQQKS6Z488NfXxweEJTHcnQNwScdzufUlgk8NV
hdAym9e0h8G7yVB/f4DECAr/VCfYeMHQn9q/ewUyC00Iww93lU3xarCmQmK8NVZJRi0aCX9Ofjcj
tYoqx36pyKiVGv1DK25uLdHIcp1cvvxmjAOP/PAlk3bEZ+VSQ5pcIpOucCK5WdkBauUeJHnu5HzK
fDrFb4NZZuJCk2TCuIqygsB+08qb4U2z5rd/Pqd7KZ6UkypuVOW5eHh4jW45SxkrSoZTYnGht8Ok
4sSLXWuMTMtNKdrgWwSyi59CUkwxufzvfLgDii40R2CrOosKQWWRDyBdqThIl4iO7YjzM8GM7RbM
0cZNXazY3+JXajMl4K9c4KSVZZRbGI+Q+YGgyUJvB0BNCSooejWq2UEZ1ajXdZX0nUkN3Xa3Tq8H
0nVzOu+PJYIbndTIlFyFKqntwYY4DlIdp2p/EcakoXlFDWAARV8Ep61lVgKH7YP0c9BQ/OHpPFpp
r02EgFEUgWc75fplzqfdQef/ORJ2k9fAWYhrpgn4p6rset5VgzkvSiGPjL56snrgB98RmiJ0QK9B
yYnSLUpRSB3KKQd5JBw+kgpDhFOI4Eucew1eCORpkX0aEzXOjNWV5vGRn4IX/HOalJ11YoBdY73T
aAYzypFi8JPylV84SO7zxdrsmFaMJ627OSp//OXnkGAC2rLGdgZqBT5pJPm8LZWBA+ZJ8vNwGKV/
UIqAf6fnhkiheCZa35Nu14JbUuqF58MyqBuDsPA+kAfcJuRv4nt0PXpi5MouSIcv5C4iLtD3IadO
B8pLkE2+yRM8qBCcDzZQg4d5kGWE4mK1vh7FXXnt6+b9s5d1z5wsY67jQfbaEAkYUGUQmcVQOMn8
OFUK7Pq4qXi1vHDXCiCp3X92BzSijaaOWq1FrrLG4OLOhY/+fX+S2zZf64uIEBiyPC8YQVeV+81l
IWyE9meWGD6swiel/vliD1dqiPWwFTVvTQJvUthZuTuUpWZbGBsd4TO19EZZvmFbZMFs8kLVu4aW
I+BOzBisYpTPx3r/zxZZBaASwFE+CNE7wiwxH8X8iXq6u7hOVTfqa7/+qKgqu3k5DTmilnWydbFh
igM9RuCJLIhL/36t0kN9YRPDQTM+49W4+igdZ3ih8XJX10DMIhGmx+zzcq6X3Tkv5hgeOWjGO2a3
u1VtOUjbjPQVKK6av9klrEwJynXsMSFZn1S6/SE+xZa3/Ng62UoVJqnpcELW9y62i2UNCSkGcvmk
5SKEsGReSxgk11hxunMn1TWlLVtx9E3eVTJvI/i4EnoKAFquLvdRx2tPYHOAyRNGHKgm1QVA1TcF
PlN0LkJCjeXD4hARgC+IYyvu2az+ZuQLuBExhsTjLnzVNN9sQz5Dldod8BVs/rMd4fNjM3NMf9VN
0PQ4Tk8DvsykH/0A7dmoAd/k0cnY9h+TyvfBHXZPWABMRMc9IluFkwQnv+Qu2CrRr91tJDI5H4x1
MrtRt5GeAwULukToKcl2TxoZyyMBX8C720o8gU8BovotG6Ryf8InywqZv4oTydT3u0ozi3MjFzb4
GvtsrzAavbcEDYIvvCF5WXi6EvdrxCsBOIdE9UYGFiZfum4woMPQzO8WU7OKpd7nDQMU26o176BY
bvY11AqUClCu1aX4Z7lCwIoe6oETWDfHjfD/Tru456oW/IjZwMfdGq3DtquYwMdX2UbQGEFykk0h
/C1hYvVukjfK88wATuJ06eTza+pbi6LCnreoEKoA0E3qDy32YeWNkHyUtmtXaKQyLuigFsAZQ2nU
3BIMSsAJm+QmgG+LyoCibYL+bF6g3iaf17g+VfBIhG3bl4QYsfufHi+M0mDhKs+8L3F4SF7mpU4A
zgep6Zdk5QyBm6+smn4wTNhH5oOtibaqWhgi3ER36L2n3axnPJjWAu6z8TwtRQPXWswYQyZVk4jo
d3J/aCjzQ63s8VTAtqO77EKtnu2yAKlBfwKfMtGKeF3RfcUd9u4m1TKhYCVnH9A8o3BsGJPCKuSC
D9UT2qU6O9A76XMO886EknGhb4ZzOtWLMjmgrDeSvme/HyR0Kjg8kGmjDjJUit7CoqtDg1M/pvND
xNHlHXSrM0OYiexs9Bpmf8H9kdYuU20DrGNA99csc1r/lAmeAoovL9jcDSzNw/lNad3qFoL8nLGH
Hjpc/FqCLoKO35kgWa6o+qI+ndAo5PW547H2F1A/Kh7F7UIIHuWKY+krSSzSDL3ZHDHyFowUNIuN
8y3p8IeXN7i2wYgGnO+NFj40SjmLF64DF60UK8Lmt+Xvbv95y6piH5PY9WQqvSJ45KOi9cdD3T0H
1DPinlKg1AOjJ8SQzemKcRsT5gk20a/UlneW7dMh7sKMVW1xISyxGL4PH57l5RCC7Ws7zJRR2FP0
my/KV2Ixh75Qqsuy0/UABOAHsbZKpVZDD9WETfEG1oPS+HfV9eG9aBsB1q/CJ44mTv4GDiLKczbw
lrDf43Cb925jOXwly7x5YF6trBO13O0G2y5f9CpWsYmMRAJDFkIR3y5FyRIvr2EoT9WQ0B9bOBnw
NnlCJYYD+N2Phe7qYHgTW9vk4n5eL7d189CFa8EYRc6sbKkeLWZ+sUdGRk3zfy2B8gPF101dWH0E
XV7PeeBX92m2Ww00o2qpQRDmw7vkSFCvVTklFGEUL/yTqSv9pjhQhP+XGGD0eIX8HHVK1/iAT2sz
EDeZDQx0ew4Y/u0gb1niZkDJ+D7ho1PrMaw+IGSDL3EGtwnMr9esf+uea67H8r0bmJB+1TD/I6NH
hsQ0gyQE/eopsbjS+X6E+gcEbG8B9eElEQO0690qUJdGDf4XQoZxrwojl+3PeF3Jrqmo6ojldDmv
3Ku7ptTcYLs2imI2tTcpeoxXkAkhAOgFS78e7YN/umNfUW7UUd8xSnsmjkR/mSqCe4uhS5DpQTYG
Pq98d/og0QUid0LRFj1bOdsq7dxFUL6DHUKTGDxBNKKhsSretmFIcM9wqamVOuj74sSQKNm8nnSg
C1fuOPC6D64Jlt8M5l8k16rYJBPfcexrtC5ezH/SJNjI8y6ZYBqhmo4jwsMwGYGM7zmqg+37e09y
9Vz6EVXc2uPE0lNaLgYttOWMglRwkPr1041k8JE0kQ+DPWF0XiekyQhkdz85r4X5H+kOxEdQaV4Q
OX3X/JEnSgFW/shUUXI07IRVom5UZFWgfB3I2SFwKLMe5rZZBCsEt0h/t4biDCWJzrnr34b9k8Ba
CwcOXjvfHtSbcXkc4XvdyRQ8HDMzTS1ATS5OmxwB75cZFQecJ7ZNKqPsPhVdl2yGjNz2O3y6CMr9
8fgo3+qDweJC1VDPQI0bWEMzuGFhoF8wBZ4KuzjjxFMgaQJD1UnsRf8hkDmexB30hWd/AL/xilmX
NU601SsryoMnirNUDB9F+1RTsQz8BxZeCqtwKcSoCryHO1FaM2cC6MMvygU7MnAtBBbZTGWNyq+5
oiPc2Hu9S5WVDTIVH5/qcKHI97G1ZqfsrzE8HPpFtQ/sVJi30/9cgzhSPD3QaGfbhthaPtpA2tHO
jKBM4PF9SDtUZpOQHARi+k1vCiX+XxZvAh7vulTSAtAURv+xjAbfUZwA0f4MDgTlmlvqKxqXH2TW
1Pjl3VKxsJfcluVcVC6BL1CM2DI2Q3eKv9dfZLGJnDPZUtBbo5wpjR4Pj14trJPCcH920G9pm5Ew
C7+L8l2oNcCkSU2SuiDzo8Dag72xfStvpqKlF/RKJBphcHz323IGHiWXAJo/eLadcrMN5WHd6UYS
kQcHeBclLfKeR/ewLe5trfO7F8XwzPmBJxqq5C2inkzpHVVM0GVXGAvHK6nml3jLA7mZai058fgp
Bt4YbsILYes+ljPuiRP0rS5sMsduSWSug3d38UkUKvm0X+iiCzH+pAHgunUY6AQDcoACpT2/ruuA
QPQCTlNywjIrOJMhLkAcoYwCU3uZ8xIUZLiSdcBLqMrHcdcSXjHuChEHP1qRxB/i04Zv3iinwRAv
ggRYiN5RxfDSLl7sNL/zunzSL9Y50RASttqlJjw7y+lC6DDMpaACDNN/WSSG5i4ESTJiYrOI/GtK
GoRKNqUAILNb9UJLx50MdSbseTP04ZNX8O/RPCC5BDtvyY/gzTRCNFPe3ej6YbwFDeQzhpdHXMdL
oV+ZHpXcrbzVd9d0WaduBJ07oT5lG66/r00jv7TuYWPyX9NHTVivS73OTaud5m0RcJXmdC91x3CG
lt5KufG/6aEo4XEeQWMmS+GBerQ16IT0P2x+ZGAXVghYvS/3RtncDexs+aaR0HrpWahhPg3Bvi6b
cOTizLWieJzUGl3mcZR0WeTz09szqfq8TG5LLM5ulHoFOJUP3HwNosYW2/Qi3LX5z1DxNHKfVKDk
/xzbJq7Cm47m5mNECWVnoPkr6sDualK+LAQT/Mkpt9RelWMMZyoIjdVK1F7NDtrOIFqUDh0T/jEX
VX/mUXN6KM2aASPCJdRrueC0RB0jkwrQQzILWSZlESUYkW7Zx3VgH6O0yko9ncNCDBhXlFBKNNP6
kGKkwg+tenLeELkLoVmPajoU+SBlomZi3haeOIHTyXhCTIDKJK7vET+X4Pg3OxS19BmKypKiHS7B
QowNbsy06c45yM/IIm8sTB7ah594naMzo9I9kI07Bm7jLj1yBnUlbNVfZs6Od2VtDirDqGciatpl
7S94tuwcgGbq9lvrfpgjvmO0bLfpmM9OXlb6fjdxt13lPlRKq85Cm6mTwawBSIIe5UwX+TAX1HOi
dO+bB3QMjNLVzujRKQ2unWt2/cEFNQ08ROWE+RdmDpAwj0ih/e+SxTNNLyL4RnGfVfnrx2EQqSZO
iYRiZ8ikVcSZHHz9VHrzZTNf7w4P6Wv5BoJIhhn423fLZN7LbQTPkdw4GBnOpVZArZ7SucFUuwg/
K7ISf5eCZ72ODQ1uhLEtqCCoxVsbjISzm2d8b0iPjsBHI0IVG50d3tkmhxHsmUHyJNh9ISLQ9gLd
KILBdeqbmKSpqmoyuwrfmp0Bopwp2B8Ok99nJ6YgjQ0GBMU+NmlgWrfrHGCihcexgLOjXNlVA+eP
+MpMkmX44T/7iRoPsO5AQ1Wr7q0iWYT2R/A5hkUsleAFLkEeUHlPqVTdmwGWBeLHNroBHiSTuNsf
ztMHfsV7blJwuA0RPdPLvpM1nlwlOe3TJynSyNjN4xQ/a9wiHjX7ydSKpjaOw/1NjxzP7doy5ytw
5Fem48slLdD1dABj3Z2K2YHeax7xHEJ3kbLJ6KuANx2YNVNOe7fsa4QVTSqTBhJOXUxve22fwdsO
JfftPk3cRlcCn1z8dwq+WRzdCf58nE2ldXvx1WY53BRUXUnhiTOC7t1DD4MfLX1XEiVwc7jdbY2F
ykuasF/4hJmFGBUh6C5/JJYXbxmPv7cWM8fGgKjd/EfgPhexA7Q0W5aItnC4T96C0b0SN9EfZdmw
llSjDTWfEjEbnwXJJHQGQMBNjCJyQgB9Z+tIBdOqwGtHyvLDbCnVq8mz4mN22z19sNa50hF3AL77
yjYcCUL60zZs003ZHhPmJMx6iyAp2RqKwh7uDkKeKim4kfTcb1YwJRGxPkNUc5IcNHOptoFgrqtk
SoXPjk/VChB3K4RCzVBzW0ZFZIalNiEEsvVqKISGkEG0oq2F1aJMf6Lh2TPOhuINiRmpY5SsZrv/
xzIK6xxHRTUzpOgzg0WT/GppiLHe4NeNrgomdU7wrxL5OBHFelHmg9kqtFn3vpDUSHMKr1uBVmKo
2gs79LhjAlccE+mQtVMSHODN7kMtdLXQ5vmp6QZynnohLILMmzXaTZngvgGIWfZk1YpsWciCOF9B
YowibL6ShhHmL4QQklet1nRPjl/cXzt6ywSvcbrIbgST93wAQszyTI4/OGGz75uApZPhCP8vvPKX
PKpxyzxzjbkrwo9n4XT28tmuqDx8NV0CC7t2gnsXynfHo1Nrgcui3s4TGsxbE2+I0UxI2IqJQbh/
ztO/LeCQzsrhsCbTC0DFTwgMAnNnTWERKRkskY4u4VQoFCEAQrvyRjRgBSyt/cOF56qHM6tgZ1V/
JjKDmkGt1vLD86Zoa6nO0MXcURAF8OmC81dCqJvS3T74VLhEsLsfabk0OGuxgL43SCxr7rny2/OL
9y7b9dXVJoLiCLEOFittcdwB0rqstch4uVK4KN4/kc1ANlE0xai6LgmHJWY0ScIU3ihJO2vN7jCh
TaHI+vDaLnOgP1dyG0DmI25deqbWzfU49+MRWVAqw48QnbtcRRx/47z7HksemCtE64mpQcqzxrES
N+UQrJX6aWh5Xyl1rTl5rfkK8hGL2btnzVq9vPxJndqF60uHEydy9R6su1Ps1S0vt09495rolfXx
KKV4yqBTr6+alJKS350vYNBTI+9zR0UgOpon2bAxXsvDcHS5GoeYnEGjaYH+az1zepJ3XWg8DhD9
Sm7wsyJANKzWatFEN5lAoDA68ONhZ3IJ7uvk6UnqFuVFesEJuQvH6L5N7ZIcv5z31qnQVRWNjAdp
l+uKnISJHTz+oYo5D+3Ggi2d2d5CtP34bxlS5VnMkd+VNGo3JTX4hjZDVEigT3sXaDj+ib6r9hgc
MnxOK1ToT46A/xtRqQZmg0rJCsfsXCeAEdbO6aSmCUNDCjMCVXl98botxq8BuOellwwAoWJ3A+bB
MbA+XjdZNy/Tb2yiZMB9FuOd9WOYRNBL1tx+xhwmibofReAhM7QHjMKt++s89QvDqxZJVPqImtn8
DkUC9uh9QTQNPcT642V7bTNJ/d1B7VmN61LgsBHqk+oMjhM+FBxs56C1rhSdfka2tONJ4oo+cHZm
iWCqcw+7tjisdEleaU9Uc2+KzAUGQ83EVQguSmZU18kRAbS7BGyrxRyV/J0OMPRwMV1e9zlnAURn
+gp1J6UBYgRemrDFgYIpa77szLit8Z5DvuMmu2xihQE2F4gWTwNiW8QhCv92J+XYTefLMfSBd7fU
adxqoW7PPgYFQkjaodacp6q7GsJnMvd8lg6qNRIokyF8ujbqBSGIfPXItfzc+3vlnmfaK4/3BIrh
nHEnUUpVNZJqx9kggVZcYCysYpRR30r0m7KPftwLFCgtqqNnlWJsWVfRZRuDxsA0iaF8i2aOaTV0
VlvbcEG4TOVoZL5NW4lAdJSkU4kFmxFlIozP6wxMAX/os993rdxgZ8oXHaAKss6VURAVN3VaHKqx
llOGbG27aA1L4XydNSKrZ0Cn9ijUoh2ij6PREov9v1BguT5uV4e3XLphK4u+wZhvCVuecbBBb7lw
QNia4mYEV1nm8iRX4y7RQJnoO4c1Ko/FH4PyLe48X8XarKwY4h/+U4yabXsmJNe1mZYXv98pYUkd
McPUTqEBsVtfgN6FGUygFu8UyrwP/CQXHAk9GGxr1qfT6POGFLTmetjGiAW7WRlkx56h4nhQpFE3
KCdCU/KPSLyTvT3PFwcKJM7yj4ze4O3MKWcYFlYp60QG16TkIPcW+GqZA9FQ3lo2BHUqhQ1wzp3I
qyToVvHy2jsTCYvXn7nx5fooNoi5yo4FyW0AjeCBecTWN6DbgJ0aeeOWLFUynh3lkQfFiaOcC2yF
YoF1CjZD0zn1PRBzCiXxS64NqK+a/OOY2zrrkmWu0W0CssPdisY5aK1bq34usBRvHN+5vLNig+y2
qm/XIS3o3zhKcy0ItAcRV61cI01TrjcTBNmEt2mgiTSZQZuDgdgXKnxNiLB1EgboSy+DnLEr29UR
KIqDs2iYqfAVC96jRtW93beTmL1npdthALZKB4zLfa79hME+wEufUTopBmX7Rm/JakzIibvzk+1V
VxoDL/lzXlHhaPz9Q9pVwRWlJ9kOns6Vfp+5k90vScjxUC1aRBSxBQwuWeNGWfzkLppaGwPPvQO9
lIfYPXO85ej25YuzuuQPVCsVxXLlpoxWZ9+KVrhpZ6TzEiysEl6wmTW7D9F2RycohwJtOOonXNJL
f9REbKyEyW6n1+GcM8CdCo2bLReNlc2irO+aNq2lUN2QXPV0F3G788L8wG8lnCHF4pRNE6Rajnpe
mJ3xzh+vCIqONQoZh7NvXJJzAd/TnQ/3/aUx98UeUiUBMuHz2Dzhk0BiRHGEsoaBgNI/D7PkufG2
1fck5iTxOuNeg6obrNpiIhqmyCstKp55ZE/uKEVOoX9hjVgRLAOoOZxjHc1+/mjtiEadeauYdiGR
zXVgzVBLMSWEFf2fSAXui2BO1Kd6G4zBBiz0n0x5zKgqIsunCbEsidU23OoidVW5j0uxN6DRzY+f
RZHIx76y1OrxBCneXHnhjIjtnXkbr14xZOoN4mljIhkiieQ5tZJhOlsHCeTV9D7vVGUZ6PLjXrGe
yjZ0+lAeieJCxts2aFNWe3Efe7UGSueRWbcxebUS8hkD1mknKgC8eonN2g9P0wmBcXDsFk4kBx4R
4xsSeG3HBPO9JE4/R6UGx4zkLnV6ozYzaKHb41yOP64N7ixaOys7cUX+3LKerMEvlwh2iBYmKRkO
76heylDQA3e0rn6jdSncpQ78K6wWSZJs43IakPWm3oo6QBS6hOhM0YKwgMPfpcQswZU+RdJQuZss
Wwt2a3T2lHZYleLd7+lpk9sVXUwoTe6bKra8OaNEbPRa6gxpdgtoJSE/1mWR3NEosSoIdnhpe5i5
d//dJIeDr6vqErxIq2KaR6ZJRwapkznEcD/ooupK5UyTC1wfl6IKLVj/mXfN5nB8GxJGhhyitbXq
3hanGEJ9XDBmngYZ0q3tQHCgXk/AJP6qDx06pGq6AfZOYaJUoZT+yNP2Xr7vGQsMiTYj/6fn4O/0
PFduYfu6ZID+MzOYvtVTJ04cxNjkhzgw/XKbQVFX8k02XoBE6GdzeRqaSz03jMGmkTUVxn5hqACy
rLh+b+g9DTlG87xOSNtvQsT2+qFKVFentz6Cz67DTo/i9FQiSo2cltzZbpaPYuxllJu9w2p9KHAE
neUrtFLKDksgCwp7uy0Z45fevXjBvjBfd1D9IYED1dxN6qaJiyb9Mazjfxh318py5rgmJ1iCHf9y
6073KWnIRu1VQW1FBaaV8oIEnVoZX74klmdVXgra0vYfZmQx7CL73t9p/Po6Zjvsb8nrukGpGXic
QIC1C4YjIZqa+5W0h03hzUtWbg8i0m/4ksmWfswUFTXmOfkmUN1TWLFSeyBeRKsAoryA1+Tmfui4
T3FM3B/ZnyZ8P6B44TrFa30WMHGZgc6Bpj+qTpUxX1rYKkQ4bToql/eyvRN21aDsvNR/gNbw5D7Y
okwDVq8O7azxdoxbopDCR7IhWu+hl1ncGYCbErI6ifKkAP/T5rj8+Svcvh+muLxdV2/lxPeId51h
/VF6ayUxS1lorQMFhZ7dEdpEn92wzThM6Lahezga9ESqyyEqneegem3LTUyyhEdwwkpcWOnm0n5U
5q35vM10LX8qN8dZjlJTXWmERCfEK3c9fCr7XLBKPCTZ7JU+I1hnro9Pu5lmxRZ2mjKCh1CsnlJJ
BEE285r8GHGexdHPeCvJhYoDNVqyjDkFrfjh/t6jwYTeAjWzjzD1PuzkakRbrBmknE7LDNJ9huU1
j6878TUpo8IZLh4uOisziAlRHMNXkYJ6k64438B0sE2ydvmT44+b/XPo2nk3ZWLgnDuFGfs/ffZE
wWh/z2NcFSIiGtDMcXBYFsd+AraXL3wfUKPtWdHU1kBBX1VqIgy9TlgzeU8ymKSB3dko7CaiK6uf
WidfLWmzU25R26IoCMokTt5sYqhOFrs4Bg/IyCWwrxCS7ARirnvZWwnCcITaNs4+m44XkIYRLahc
m/KiJD+a0mIZVwTzwk298l/Ap8eJxoWxvAACEAlWkbe1m90wK0kz3UeVlJJ/ylmNdr9nSlghamRh
sAM8hW5F5EX8DfKVHEknkCwHSnrF9yhvy6jQHJTAtJZdAJ4delatacvpLY1rLNZ2dTeJkEiLcX2I
y5X/FjSoy4BVWvd8RetbQx+txV2+AeULq6SbwUESqzZod1oHxBTTacEJi4k26sav3N3VpGlIjVH1
8rVBjZY4tUSXCH92EsxaTbOshIn4WM4DSNpbY4Xec5z7InJX9fCwB1t93gxREeUDUsb+sHpUyJaZ
29PHBmNKDGSkaw+o1Hb0Fp7X0WTnJxkD+xosOAE8mCpWmp7QeP0sHo/Jhp1pBGYROwq33Zl24k6o
fcE68fzEHrWUxAvrZzzknlPgBhl5C6Mmw7S1CiawuAawCzCgrK5Z6+re+3TToCGWXAAd+TpwuYEf
te0jhbdRwbmMO+OyZfON86rMIGd/2cN/naInneF8SS21RSe0mOYFr/dN3Qot7vdHXet/t1scVLUt
hFTIdq2xamlyGjTd66DBojHeNhdl8Hy9ZQD8n380i3Gntow7WTMk7Ns3pmeKpID1BU0IZGfLmpD9
dXBiL0vQjR1/aafDkMjg8rKs3wmjGIcxEIHv+QJPhHbPK0JwirOMD15TihQkMLlU2W9VVTWgAi0E
r76mzUGajm7s/uywAk/qi1s6ZkSsINdeJh8TFWIz5PBgnd8D+NtZSPLh2a2mWhqrOVIcDR1MtSMJ
v+DHAVw51/BOP0fuzzP3D5QjwLFjaUnwvZw7J3leJEx4R8L311nzXQaeAr7N9pq6kFAZhCqDq8iZ
XDtDxWHz3EKAN/vzHGRc/iEAIwRl8XFEQBKssCHSsPGYHdfYjyMjv2uXWjG+g62PPEg4vWBtLg3l
ps3Tno4UbsTx/gJXCVDIvBqkPYJFffTxLwa5wR0Bv6CO2zy2r+3GGEXtJLofGNQFzgIDkAcWGPhx
aRJ1g9CDn16ELrwIylicNo1KZ8AO6vweEhtLYnzTH7lp4m1SPpM5MnwNJTMuNTkbEU9YCC6NwW+t
r76N88o3yFsBYYd8qvzuJ2ngkPto97Bv/COv9xfwzClZS8745i+ozttXQDQ5VhbdRaHI8ls6lSNH
dEnvZWR7c/UJ9kCQYqJcwSXrv5PgkuSXzSJ4bQhQHLR+06uLByJPqp0JvhJOdAVJuoACoy9ujbWN
Dy3qvJIr3PxXPRJvXJ/ejgjDT078zTi3UCq2EjvYmaMfCRae73V78KQcMF2ZAfxVzXNRB8Tx4d3K
mJMBhcpM5iMT8b5amUlAZY9/29/6FOsvpBVUbgphQ/7NMlUhnEJkzR48P5rYqrhCniu95VskWGCZ
Efasdv0whUaxXLw3qHau6k1cTK05Fqo1LprBowaKSX44CAa0g0++dsuidjaAz5E7550czyXQXVx9
jNvH6NX6p5X8gfO71jUVG3GZrWwXDJyLpkrl7wxlpamspNjyVqAdMyrx44AjN/4sytDNYabsGZjn
c4mEqF3dkOShHLk+BLowhY5P2Glo8aKILuxiIjLlTPHBA0OAzhuzm1TlU8qOXX4Hvvu4nNIlQ1Do
LtpBjyrDH2Liso6cIPY+V5iIThhpryK2FYWM+uVsnIQHtTG9M0FQvVVOBXyVzXdQgWd6TaQ5e1jl
M9q0LUFIzes0AuHZnDljTuhtE4lgl3gYR8ziv2vQiCwcmo2v7UXi7yv1g+nREOydYLbgoxs+h1lD
JJvXPXect0fVOj8zS+FgwdBP0UDI/mB3Em0c5t99lAYrlcwP64GcNnJajRRdS7eOjz8XkWRJ9R4x
2uBUC6aH+v6EMVoKaGf48Evz852xKr9Z5aAgP/F7Q165QmamXDqoqTgkdiYqSsO/RtjTjSXZI+qK
VXts9N3VKJDoQM/keM1sQ1bC5iQ/H6oiGsGAGTTdQcPy7zS5RxHvpG7tUMuQpqMleFCHJyoSsqo4
5aqsJPo8b54HiWUodYwJMD8QYWbbtln3aIs+coxjhx8z5elmMUsotEDBxh+vd0hpHDcd2hzMyJpa
+6cG/Vy2U56HQjDw+tHmTg9p2MQGXKr2MM0EZUMA0Lse6irbKqmgP94ioHcvFcGfOMszt0ZKgSo9
Vz9hliKCeMVhcQMRahZg4U5yS69hvL5VB59U8YGXGmRh92u6u/GmCMrAl1QMRm4qOgF7wWcIWJ5S
bYNOgJ0749l8YIJDwX5ZsItVVPQJ4BhXJ1kuiWrhJAEb6vqeieMSA1q5ryeJXfLqU8KUMiAKgQ+S
EMsa2S+ZRImY5SvILxvERuZiDkntYpm6l69RKwSQa0z+9Fujw7aXmfSaql+Ij5jCxufe966jr36g
tuushw/S8pORtz3/wJLa7l/IknS0dUqyNCJZlBCriMBgk2bw7Lov9MbLH2AEHKMWn5V+l9EKEx+x
J/69ygE4gxmVwnc7Fl8j/jJqO7IITXjOyZmQA0W1H+yuSatC5A5P5jANvSTo1c6r09DdGw0BdtpG
iGOKDDnel9jF4BexEPuCEPnGhnnGnRsL+/LDptOEOVQS8h5+2cUIWme+vrogxh5eyGgGnXh7/XlO
igLcPUFx7U5KY5243ZWSI0GmYDNJCEMGAiDtunhSupDfSMtA8MogyFIhNdZ5M/hfRiAPvAdRtjEn
iuvp4vO0ePoEGkrT6WUjxCUwMDtOIzU+ytdS5I+9JwsL4GfOSe9zZS13WZPbmB5zg6hwIkNp5y3U
vi80hq2RTFyp0vX/VTpUFBCd3Zbtf1YvGLurJZVB0SxRBkTixVinwyW+e0thVMhbDubUKsOPLRG1
EPuNjeMo6cu/BrzmedtdqBKeJQ+nr1L0B3aZ9GB683a/tpenlXVFPEwI9dbSCh0xLgBkWtMcHybI
Vn6+OQ3WqPGGJ9AzJBOG0EjDN6/1/dfUMNO8IdwWlcS8W4Gqq0x1AYz6Gf6RHsrK7dd5opIhe+yn
acyh0m0c3yyMLeRw9FKCKnkHDz851q9/zYYf2Q3sdSxFKnuvk4rpGtGO1Uxizy1hwzquEh3Z8zxk
diQ9axVs6My9F4G9f0HQZC6KBn80NvfDI4zkHQ3HU3W/o6rUxACExnNl7FO5BiBsDeiyCATNFYrc
xI6xxv+u4ZPNgbsSmqJRsFq+UhUNVb59e/ALWc2ITA30nCJoOPMXyeHSUKcKJBYUmB3Xch+h/du/
iWbUOCw9DES6jgBO7r/HTjUYRbux5F+L7o/2B8g6dOvQTuL9uRasTGnZTK2L7WM/Q6Z5X7rdU0ds
9LOKxFjo1qE2fgLM2289YP0XiDcQK4Ksu3oL3vWgAGGAg04tr2snq9Hr4eTg1qS3k0jQLiglV8zD
y+VLEnUVIOU8Evc8y3FvQEWVrHHFRy+ZnRsem3j86MsHzw0OHmGHCBihFQ6IUgNzv37nM4M6x4QB
DrtjV3opiRnoJeHDl9P8ZNsn43sfFaf74jbor3wkcmRUesxvP0haO1mVnwlcwfMlQf1xsD00ifj6
hRlcjEdljZ5oPOa0R+rmnBP0stOUJJ/wRqzLq0vapOhLU/DVYipzpxa1Fc67CrR80CWI+q+cZSUb
AWNsjKYV5EcS6d/kD+hhilrF5J4+rN+++GPU1Yh1ia0S7G+fiq/lenQRw/pGLl7A7GUuIZY0novP
Lm+mh7Pje/UR0PXyj+LlpOzRRw6ou8c89EmHVBrRr/equq76Bi0TW+Zvc+o2KcJYX7Vf39Y+0H0a
ow5U7Z9ilurHb3PcXHl2ea1+nLupnaNZgwATZ+5YP9tS1FaMfCiagHmDydEfJDXA06sRUPh+PnC2
LMiGz5igG8pesz6X+KhZKr/Siz8Tx6Mvdza5OU0XuAgFx5ZOD7IWHrB8YrgvWjw9azRZFSSGvgeX
Olhyuxc9tIQupmm84yTg9olN3AWM+b/tmEylz8EqutGWoXyEi61wtxVaUEId7MdtcYqKz7aP8WBx
GljMQrj5o6E9VBoRiKLINAfWqpvb8RkAM7rTCvEDaceisU/exCrxTDQ6+RjMJDYe/TU+NDSuLNYe
1ypxhGhn/e4Ft51JIAUNFKwYj4r3qGfsEM97196SwQkJPD/I70rDcbPs1B9AARZJEF/t0OZViStY
x3r2QpHoMtpY0PerVTYuKhNAQxnK9CAIMV7JYw1RRY9tj4xT8naL8lDqf0SSdmL9JBV5tfb1vG4J
Z7gLqW12e2bTdOI60wH/Ab/d9e8p6DpHlMV674ow7aPzt5GwqybdgHsHeS5tNES+BQPACvtTzrJu
Zs/npSB5RtP9yCwicR7r6HbeGS0P5UuzHgYEJC42bjQMWVTGYyqc2ykslzUHqk7XB+EJiZWpCy2U
/UBxNvWVLCP4G+0FCxjWvoo9ho1z0E76ZUNMt1l+s6+yDr+dRkiDwim4XbRuwo5N/rEnAhCeYhjk
v960IZZLtvdv45eucOhlVLTrczD+6hVExMShcFxpRpcHmBSWfr+ka9vgX7dF2IbZb/PxhQcCN9aP
Wc1/hMuH+fl3q6TMC/w7plQx7/nrUKhTh8eUqSFpAaDn0v5LsnZiMOzCZPWmAL1Eo7sZIYiEmCMx
Dmd50pFyDqP/vnxaPQ1A6AknlFItuLhAgokcbFrFnJUw3xaKhd8ytvZEvTGIW4xI2gi6r9NMlJI7
IMpl8aAmBJ984Xs6Of8shCCsbr+2CtBdNwIJLDezks9S8XUzgj2SHQk2ahZc1N2WXYGhC1I2L+yd
PNW1++MRwDyn+hE6oYpcLeQu9+q/9g5bbUE6BzXIDvFU8mh9zSpZTxXT/QxryRQbyPBp30Mzizzq
/11+Li6yPhEC7RGyXkC/kAtbdnkX8MyRBAqHHOiNcnwdI0DroisnuURy+BCidZkAp7rPBz01G/Ih
lyfi3Qa7nAOYj39NtcWv2oEAJ68XeJ8aK1/irkzRqREHGHy8c92CD2omVVU50NF4YRPqMX6TaNzp
MUvYOuMMga8zYd2NYRx6DFX5rNcjw5eWYkB55nwBQUUUP3HFYXegAt02gbR7V5PbU75wmVOcqupI
hGWjZHJNKebXq5N+Xf6sbNMJ3bSHeBcshV8c+bCl0ZoM66heRLMJUoGJCQafTMI7BVqdP06GcHbn
x0lcaVIibNErXLMk3Xyph6G/v29idv8ECXmJea7ct9oV/2tIYG4G6cq5XvqRJbohb7TvEFyCj7aq
bydZGyvVzeRaHk7kFQRHd4eciNLPPy9ZQevoMJ0khtO7RC2ZgY1ff1joywx94fe6oL7lmPiLvEvJ
3M0ovDNaHB3CVdyu2MkXa6jRQEumKg855jkZ7nOsx3CSbOkMrDe4dLOX4L5YWsnpL7si4igWD2yu
irw1lcRQ2TCaIP1q3jkyIkgWFZw9vqeB0E6bOSe7C6NEQRsA8usiQ9IQyw8eonoF7mnkr+LM9Fnw
xyKySTrCBfZaXyVmpPzCtCq6UX5QQV7csqTaAhcpvLym7LRZiMsPgvgOCk2Ci9hFBkCXM4OZYdNo
qPQ3et5E6digql5coSICSVkEpxexzn+SLLuXs79vB5P1qV0rHprwgHdDeoVtqVMttNuczN5+kc/y
Gs/YGlQVUE7bcFz1GET7y5nWpHnEkB/OFO2sjgxsUnJo5n1RtwPxCToo/3Oyk/gU3cweHCcQYuuB
ttsyeKc8Sabh5kDI3z9dF74dDc6ZO1pNStfDbYyEo1JffoXkkXAVhKA69sEkGlY/jRV/miTuOgBv
NGRRGZDDUC7qhs2EXVl4aze6XEYXWDe99xiG9SDqxyrKii3dQDdT/vCrVM0CJEA4+wX9grwmJr2s
w2VrA4G2X0ogW8hWEUIs9eEMks6BQVjWDMNAgMKLCck8L0ec/Eo/B9k/oV9uv8j4c8nBwnOmT8Jk
NdFRAeKhTcNcKmSUZlj8WggIzgwS3mF+UpGWJpyhhkXKeEnscxex7MFFpRlnt/V/BKr5g1cCckns
xPQYSu+wLlsY2H2dSIJzC9+YuR3p4CteHwRjIbZG14oqMCt+8QgK6rPrFINyRvMHYM99FphRex9G
xQ2my9qx5KtNW1iu3oWOf/kesv4WSzz5Ydpr1aIBy/dykShcfzty+QD5JywdsDC5hynvR1UbdDk/
yFoBGdWgA8PiIJJTfWRK+X2xeBaY2jNKuYd47eS5StSZDsQ2X44GsD8OnS4MBv9wB18T1jTEBces
eLtb5PHM/M0SngNEF7xAWeNUO+MeEJFIHM69SKK5XTqt97E90l9ktb8xdLIF6hPvZ7VbuBHv8ZbG
ghaT3uMKx7dOzUA3dps8nBvxKgKvtIW17m9t/nmX80x3vcxKGwooB6MSFmwnMD5OazJBNwPtz61y
WJfJINiGzzJChIPmqUzoyIp2crd46AOMeW9FWewFFyh5lSI+c2k6ezZ1WiX05eupgO+3pZgHlEy2
liG/jC4yH2o/cy5VW0P3Ltx9SQMRDmgzob3Dx6hsAJIf+jFmUgnZTVXP+1wM6q7P6Yahd4q5+aGC
yCawG3xudaXgBO/COP2Vq9wxkLZRL+qzeDXmEzd7BrzjEvnxq2kHyDgtrv5rzKYanHS7veqeLB/e
fWwulKdlJ8RE0pBQt91owOWUYqyk8BSVBqGAR6JCATpe9bQMPUsIl4gB4fmYELGEbQxlKw4/KfJw
tvrIFX4Tw8Jid6JKxweHEP9aUxpTEurQh5k65rw9NwExMVVIcB+2V0S/eR/87jQDRMVNgFdPhdw5
PODH8EnCT5vqxMLkfKhV2pPyJwIPFfVBdaB3HMHRKY1RquDMKHQFLkMWzRcOZSOmhGd/mR945YhV
1zS6UFY/JAaWyMbTO2VLSM9DXtLLZ11Ol7PN47jbHpUFLGOuqv31NqpgeGsGDS578PhGG0MEPiHk
Cyhe+U4FPBD/4BfGKREMVANmeMadkgLRDfo3aOlIcIOV7iScfIYzotvW9IugljESDpUvvidL2mQE
TJB4VdZCY9gnF5xVfOzwkI2mNwg/3lkXCNJ1JwM4U0nYpFMDSjaX9JO5HEfXxCqqZjxtz0t2ntab
tVsWR2+/BFeuk7CRYaRCW58B/ZnWltjH+IycjAqjG8daNuZizKA2cRwvQTDIu6T5RU8oHulDnRgX
hcuMLagNT3AHaVWCM0eGGTUXAusRDBa9Yz6Fc8Ssypgs4l5htuneI5n9QG9dHMPgtzNupUKoupfS
qGVNVr6gPb3A1SsUVRx6/tRFYRvZYreZuP5M9M4dpfXB7QBRqt80+K9URtS+ZYyFdPu6JlGVil4S
916Wm0Yf0dYdmch0zPlObuqsuDtBOr1Xp3bj6gZi59GNMADloMltHaKj+yQFZ4B4kasD7RROiofa
hk1KkKkFAdt1cvR4hnSVXXNWr60p3b6+7GOl7pPvxzRB20YCIhuxgS3NiV24C5yTpLZPAvQmyLG9
pibsmKawWIHzCC2EZEAd96kXC8j3Y1p4ZEhR52OuQosZ2E2LuBCMa7+yDzWtL/9TEXpyBBus+S9b
lV4Gpu//tkpFPcGHp9a1c7CVnefdg0Eu+CQHLE3jYViZy0IGIvNAKTKcBCVbRCTjYnsuBODxt8MR
y9j/8bun0rWcRLvXn5R4Em3ViPRym0tXdqh3sT3vwbWEfyrORRlZmxV67uTAjOGYJ9x/w0geiEXH
sfcW6G+EGocNDB4cjc4u0bjwd3sfY7YG6uzFyecfW7wFLYC7gZlhq0yj4W6Sk/Sjmfp5fOb8IYBo
+h0VQYG9O3FOwkvYxYJ49jWfOvJpgqmnRi/U/DJBFdEeYuZjrfktPJo3n98rjdIJZTyj9ERGXKDg
NmrerhCGbIaCIABRcHOE7GqW3PNHpTTD+pVtYLGGixSMJmXlomFVR8BNFc1d4ylxZgksOT+mQCui
hY+BWPeFQBnO+oz14ROCx/FVz0ykSAHxaBwZxyU4df5nvP/mNcW+TffA1bvKGEsYnCWAVRd0KDzX
rKMW51aMzXjR/KxjdFFHIZBmpnB9peMvcXVEGCNgj+yPB2ygVmI6GpaNrzwLi2TJLkiqS4ZnmzHC
CdL/r3cRoO/JeXfZOwe8l83AgWy4Q/WGqhW5hiNuHenvD7Z5yf4fVS1yJl7c8B8ffD+kPsc4D0WB
PgI1/zW2ERiGQEcboBkY/IrxE0a7/v16ze1SN6Jb8YvHhh2rV7np71bMWYuTHVyom/Dw3gVEpGxD
kTYVKlfRErHwyBJUjD2LUczNXdH9pO9LoktQZ5dGRkt5KuNqghF+inyrYqS7GscKmo18keQZX4Hp
q4lXjAVkxQIqMehwKgwkRN2IEbx9eRBugkVQ9PT7SCOPcLXBbu3Dv9jcoegK6bmeR9x/p9zq0sNY
SKuRXNOCx/HEpeBjByYPoN45z/JDPubYnFY9zWUTcOXo2I/p6VMd0dgwKkYv5fzm1b/0UGy8PS/U
lTfvF0iCTt0dxYIriXddxy5/1eUPjxcs7Az2fxfGtU99Kec3cLiEpGq0QH/wzk+YRJdyzbLVf8tc
2ms4JLutwmPueGqlEg6dI9o52CltbfO9N8Pz8aG7eKcDb0nvMvEpjj4hGfWhs2zFhiLGQA0A0VY8
AC7O1MwUGP1mTNx+KwgpCRsHO6xaJ+zAWcjMTxvrRw5NjZxo+0yxR1csED1Bc8Q0BotwmwUslYBL
XhCLPncuFKxliie5u4WnuV41CFsOwp3Knqdse3etSQ1mbSSOpA4AR8Q/aWSPKQQ40Kl4GoyOq18M
bEEM7UkfSlcnHXQiTByIm4ievtigwi+tRRzbxgL21zpJex14/XXtbqld2hBH2oAWLqXVRjstuxpp
3POdPSsc4sGgzLysQD1TL3JO+WXpgUvPPVHxg/INUC/CRRVIu3jtCrW496OQDwGCXPjyXj+kZB8n
LVf3hf2A7Xj0h1e8lLqzQfhbFfSuVvj4hUDE4JQcjgtCRwmn24jqBYDhBnxLu6X6SltsmGiN/Jmo
Ndakx/VD6EatJP/xx9PqRj95oBX8AM2gIi5qDY0frXTSbgeDBenQCQFHCW4svC/X0CYNAmcWhpyg
YY2jNkwfk5FgcRUtdUMk+AMW2HSxtAYfeyVUGJ+bSDoaAieXa4CuRhWSdJOU6YbXmegXXQS//c+y
UD34/raNXa1IW7ULpY8Gu5niuohKoj+w8k38V5uw1GDeY1xUi1OiFeeLvp/5rkAvmOgsp3/iaCP+
/CvKFHKEJaOx/Pngu5eTvKvVKibhd2DHL2Pa09yYSVG5OOezID8E7RlkCSbvg3dctx5s5SCA2CXL
s4kuJHSxNq31YtQDxMjC3wBiOCGgQrmLEg/y5VF0oC/58GSFefrfMP2sCX+0rHLJOoF8QxeOjlQS
ZoFPc1tp98xSSuq0OmGJgsO2YgVpSFwjos7D5gBrbW9ZBTDTX1k+ZXeesJ0wR0s2iRJfAjzKedKK
Us13Vye8ngfvH3YV7LHwpEfL3NnCzmxF84tz1Y65nUw97aXrEaUtFsx0QjWKoYv3CCTaFhKD/kiq
6qFhoYJimIfT0+4y55gCFHf4WV40QXlAd3FfYKi2WnWSrfxsxotQb6fe9M5JltS26dpPLTL99ekl
WYNvzHZiQnBtn400DiKexAgHNU4wTkX3TbJwUOYLoyflmgv/NRlWNjf0LV4XdqwHoNiDP7wj8Zdv
J83oAKW6tzwo2kxH7U/YO9TLrpRrcyvi4oo+74Qdq2w9dlsoKCSnbeyxNJSVR/ah9mjctwua8M/R
sjNYN4ktXia2yIMKNnaBdzg3at1/b6BMDp/h9ZggGkMPCTL296jbw663p+24c1cIYbMJuoimPIca
8LKc1ANSqR6YWVy8UjaSCMQyG84N1yFVnZcghFp1H17z2TVGCEansaMxi6rgFiIHxB6k5bGfSUtC
xIwybi/UVYN9wz2yusOqRkNNkIowihVAaSsMpg4+D+C/MNT4jGZWM3fl+v9FGQoe3/cw/bqVL3Du
jv7V9XgFK4+Inx3wTzzm0YkgHe4Scd0aBpmHSWkCAuR8ATnb9Dw01635SN+MJWSU8boXE05pxlE/
6MpPoKAcr8dB8+W24+0NEwMkp97RqXc/ppZB90al/WbLRO3ukeaM7D8WH4mimzrAcYFJe6eCFndA
XxYFbMrbJbIcJ7DcOwU9QArtal2acBiidx3USDYMA3V/Jt8Hk8Hirx5jMWcLNPOtOoTv5oHmQQAY
XCzn1NRC2TIDVKOjOamoWniUf5hwWdDAKOqxAg70s9yuw9wcDBmxmgpOHgtKdzchdVdu+cejenI+
ZWAdgnzWfJwzRSje7z/dewpRxmwtr1tPYxNaZopxytwuZ1jgp8/TF9CBrhbXov36wfosQzqziHUb
+8szw99mO1vK2xTaGEaTO6NzaX02oKMh6FQHaryAUbD+MbecfwuZUiRfIVd04oPRv3UGmylNAPj8
BWxpfs9POT+U3o/gX62QvmZgrqujmqM6jsOnAZbNpXHsbOhLY4xI7cwD25wLWB8Aj6cJfixLP7kg
pz0I4ZhSI/vTkAkoiecISP6fEO5fxPBUkIJJFu6f62wcwCK2NQXzMc2WmtTw79fTkZqs9oHnvveI
qnu+mYQkBz9TLkVGNCUj7hdWzPOuBFXmU0vBDW/T8twqFG4QNWNth7QhlIS1RqcexEkwluUSxfq6
TKOB8P9siB2zQdQ7rEYDhL5WlunL7bG0mVpIyT0ArUlU5Y9+HIZj3YYJfVrHxdhyAmKbBuZD+6gn
DuCEwtvwlfSmQUrkQ4M7XnzFmOs5KvG3rncf1khtyg3yNaU3sIU1br7yc071ObS5xLrk/gpoL9/v
MP5vxXQFkZ9LM56P7F6BtflPxRTyN8d73M3MzQwa3fUwjqK1bbFTAd2JNiYxWdbukC7ieH53TlZ0
whOjKUiKs9LnEcoFDTsIYcfNe2loU2Je7FBYOLy5JBTTp/LhIq0kYOs0bcH7okmkYv9GXTMMlwnP
ItTUNVBNPf1byXLiFIaxVoNIsL3Q7oI9QPedu8cxzjRqF/S9K/FEQw57KObyfqbs/s6g1q87LnV+
w8BPO0bnMAW+1ND3jnnr4pdHFSWqRQE2jHvPsR/0WVwloObe7ArhKhtgcsAHz8kZbMj510BpTJB/
nWbmLtwGwWC+Y8UXpp7XYf5D6CcGYX8YYw4hkrXRuG+e55EJjoE4J6sJSqV1XCbWOy2j0bzhI9jo
JSPC9BCL3cl41MtsceXd7wvrRHKxahlgWOIiYvvf15HaEjEQu7NLJzU3hiZI7gdvfP+oUgL12sw9
w9vQnamWU/RvglF/SGcpNKcYDIYOXIMMkvSw+JTqexuoBPnFsf8JqzChZ3JbJh9I+Kz3UC55mXtS
xFIQV51vf0UTnCASVDZBNMw5Br/WDmMmrhfwdESjXL6IkWEIXUYv9RR3AA/LhkHOjMmDcFqyi0hU
gy7r8kLaek7n9l3AA4In9lbD02z+23VTIP8Zr9XWZMXeroc5bkq5M1fZ1MHPW9ACOj3W0tPmYhHK
AYQPNdilAcphmkAjn3QKViOz8nmoU9s7LrmYcrJN7BZHgXeKs5AQ9fyTSDoClSvNBJBjU3Zzx8Dn
r3UJuQ+3d01SmKToMLwzRYDLoYPjqppL5qJ6r6fOXOP4bJn+2GVHwD3AADX702MsXLAgglZKp29A
hC5Z4S2fWFx0fkYT9rW0CjEL+8pkclvNzNEqx4g6O21nSwCeQaj2W86C/xkBdYmYBsmzkxtdTpNt
p/kem5jHpP6DRFE7M9TMGT1PYf/UQS8RWPe3/7wZ6ffqpLVzM/S7p1OXekD7fRC8gzHkftdLh33w
U13Ms508FduYuIuqYDxBXUMe+43PP9PaI3la/+hN4fAVIoyyIt2gM7XDAYwVp0Ju3ek0bkmiv2yj
HpWql+yzeHGYpOEByRuwzqZEYtxPWZJeDHSIsY1QxXhrNLXXe+elRgc9ekdKJfK6Tz/NwIvbnstI
kQToNsTp0mmTn1qkenOS1jN7zg6VXmyGXdYA2dZom+ZRK97/9SV1BheSVnbCZv1aosf3kAsm1ytY
Cmg/nZ5DCGIjd3eaiZ6GiP8bxOvR3k96JsdpcYzPC95QZJvhbRmXLBfSK0GUr9x70ZXgzJxk7kG/
0TZiLPvs5F5exr/uB3F7BJotjsy1DIjopKwR8gRtE+UnI9nmVy0iwesozSF4Si9Ff69d8QW4V0Xn
+nF5LlX+votUVn3fo79GOmoQ3Hfzd5Rm6Edpe9bP7iHebvUm6KKr8gFY0iiPlt3ORwzPHO4fB0sd
+fc638C84BM457kWXZAoGiykCYs9ZLazyJuW8a7brJUeYZnGcDK8/MH47ridZ5WAyXZkTSHOpsRt
p5XozuQqNOKvKM9WzHFvqbmTrAK4Idq22fg6zx6Ug6YJn/QZ+yz7TqJ517YtPiLwOzlX1+D3+4Ht
UAW9PWXbloYhOoHI1XB4484dAKtLvYEypni0sqOWDXvIQRd3S9bcBRf68UWEHsuiD0ZMaVgSozGu
QTNY8qsJ4HlRcsxcO/EwAhjw9byymxTq15Enw9d60COlJv5c8EJFruYsu7h4yAZ0M/JORfXZgAow
f1qJrVwCov0NNuKGBm6pVXyiROpRsYaBHGRfdlTzFODaaza3J/VYFa8+DMBoq8V6Sw6gKttv9lFr
zj/wpJjivjd3CS9cwXlwjNgh/Ylf/Pm+E26N/4v2gnJMqpKewMKRNmpNav2rxrqXDYS+do3xrST3
uCLwzZ+jBF+PFZ8rNzF2vGtTrcDTVaPapDTHpf6LRc28qhKsc/tMlR2TbuOqwlQd/gImJkTVfta0
gFQyfHSt3dg/L8YvhT13HWqOPf8YgO67ueOl1tzo1YGn/8M4+ZAYq1BnJRnBE0OSXwbVZEp9Mdeb
xdAi1P6iYUUuQUneNvw/YuM41RRKW2u1ldBgvHifpvC1rxke4L5v7kKF73zvjiEx87lvZWK+ce4j
OO9h2t5IH4TilZKBcnwyIxA6nD0M+vfguTo3BtUPdmW0TMksdIpMIGM3jaRn+mkBel3LJHM7Y7ep
WodhOlSdS5OdoejaKR6cyQyvVxBUdJntgpqLp61MyxpkkZhZbEr9spmvDwj82cj0ZaQ7N7ZsWw2D
KZwBRH2gmzCes+YusTN1OgHVCSvv6TzieGV0ajxLcVLIBtmvAfHzHJgIT4anRiDE9KdyJH/+80g8
GRNMXVlYuzKjM95is2Zcv3B7p/W9ZDQnO1nWK1o6zt7ny/6BLcstsVhOAY38PpZYd2D0Qs+jLz+Y
cxcd46qW8FCAyjmxkqYKKkYsi6WP37lz/9rR0ujmTiS3AFg9U0POfDmxXEx4bKo8hrV/pKLB0Ui8
JY/qzesansGzP02HGwLKPovc5uRx0ZYZzh2G6k1sG31F7KZPQ/y7d5UBHCYuVSP+WKCRgSp8Hilh
bBPwWJVy+F6l8lRVYL3+PQPRyY+LnnAUmuFKDUlWdbmgqJnKDYp/9HFEkt0WyZY8Yu61cRVPW5uU
Cdh0/1aH0KNNMWNPRsFe7OUQ1QZQvakBYuQKxnG7WGPr6ovbHLk+SRA71FR8F9chgZ56qstYHx7K
rJs2nIO+N2yv2sPXHtEg+AWmjyTxQ86efb1Gf/zdi9bSNLwv6iYvEFOPUsBtADHyxVtIIOjJoS4w
yE85tmzRvh6J61NspTLg0OcxXSHk6uqG+OfcfK6ihIbao4XiKDoT67/ai04GTd8PY5BVGonCwJ++
dASLb+e/QD8bShK6Ph2vzGtciQW5aaJ+yEZCaQLnyyu1T6kPEti2qQFsU8fqNo8dZIdFhVMdpueJ
o6V5OL+7Ewzh+gPPmlzVdnnZeF+QrLOKR91Qt/mV8Y8aPWVqjS/s6CmCggntJzWCWsz9HxXwHSns
67Rvb0r96FhhalAoqIuSSKZFUrFKzhjP+9p19PVphbJLffCGGpTOVO/oq7Vzge4TV9GfIDT31zTy
l+HVVPj5uXIHnP6fSwWJ517yN7ImFlvgK3NvM/NsecMrYgXur71JL/bzlLvnDlba4RpNyu243Unb
A5jZZjmGjJvMfeu1hJ23EjVsIVofMIjjT+TWvszIekFDntoAwkV93+KobzC2e7Iu6K9Lb4eGtsJJ
++K9oubCtcSvDLvEvfPf3jE2vYuRtAsF2p9o23vxQLRhPTAZQ3OpLtAtob1/SQKvi/643W86MRNB
bj+o7OD4AnmmLPY9eaKo0jPoVDY10Sm66tTxCdalat8RR5ZKLoUKSaVcAd0oGApuxnC8nS3zt4jx
Cmodhf0WrB0OlTKRt1EFM0obfGjRavI7xSzhIVApOehrSUVve87ZTOSzopNcca466BNjRArCNlqo
SC6vHvDkHYuqqGG1HBEjkdsfu3rKiSW3t82BbZcv0MP7r5PqxEAhS6P30SctaIojJS9V96igGrkC
TMB6MObc5hiAkAJXry3dAl0LV59EEMXYcKhUxHsME7OvFfODh0Ui2G4ia9SI2ZxrucQ9hcDLw4+3
PknQ2QUhnz4laFyUXy6txjvruItQ1bAMlzuAfvP18+IeYXzz7FYlJySB7vuXMulP28mcJlulBIvG
/lO2ZkQUaiHMeTc/J2QBIcsPVTIkkQ2nci5bWAX+RfaiVbtK8JAYYh+ZNrB4D7z3VyZDa+R3KSIK
j2/jKr4BDZExb4j5wJC6ZVSTglYsCUTSBYIjOOBiUGeTqIQZ8T9c14+IOqiQxJZRHNmMwwQPSCtY
GsYFIyEXnc7N40XH56jJnH+4mnL7SImsFUWGUR5TFl627oXqHRGcFfxGrShFuDcgzMqVvhiB54Do
15nq+QmIpqVhqbi+vUgx5HmaS+hVM9gFP6Xlrxy15VrQaO60cCL3m62nsbPSmAOQI7LOBi0ExOwg
oxrs4qrjueCTyMm/mWqdYxYddzBm17Ex6RDMLa30OTeN4I1hj2TWBlARpE8DvxsqHTJAtIq+GZaE
YF9I98ApKHlXTd3N5hf9r9ogg3EjR+ohkelYeckm0tTu+uDbfdLWKi0gJn5LlMzoBbYVlatNA5Qs
NIHoAHZeGirxtyT2vOIWvl8w4vG6dbgYs6wvS1DU1GEKrE1Ecc6PAntDgUCEigVYhoQ7XIIUwy0b
YaVENx5GFm/dH/h4oXx8Eg2gqV6sP7sNBguLm7YyPmbPOntQll3dB2lik5xEWBY/u6EzObgkbsMW
gSnOd2D/Q256T0/t/kZAsdIH8Gye/x2ISFtyjTYJdBE4nuxjRjHXX1VYIWvv+8X9vdg7i/pxoOVd
4n725PSH00jUQ+qZkbl/EJRjdWwURlxZKI8w0PnCyq97xkC0QSywhmW7wS4liVLE9qQGP6gViiSv
sgwzpBjJ8ggomSCIXfn6wR2gh7MFZAsf67O141IMKO2Psaa00pdZsLUHWBF6eK8xy3SCF27ysH0l
j6e7cnLFkE2vId/hEyeVpKa+oNnrHL3MwWMJjlRRY++eMsqHRS4BSUimGZih6qrYpPWHipxCQDE9
6jLO4SxCONRwaY8fblVHPFdBlS1XDElm3EBT9p0goQEQO4moxQ0B5iu+rohT3oshXgJ6bw1XZhyv
0O9fyK6pFun5QRCEGcgjwczqfL3xyYrBdcI9aYlf5JYm4UPhvelSa2jFBerSK1SPcD5GqWxv4VCr
RsWJ3xCfur7a+eQ3ojaHLciMZnVhrnzLrbv2DMIg51x202By6Cs34v6HeQ2LfIh0h16RhUJhVGZh
9E2M24hWXAMwfQg7RqAvj9/R6p+UVuMbuDoXbZtrzmynCvso5HjVD+FkX0lPScx0utsynKGLUS8K
rsmdnxrl8uBnVrkVcp+CXboMzGsnPGjpsVp3+QFKFZgsPVn8nYjcDM/osWs/bw+LJTljJHXTGMJ2
wlKu/TnLhl902wAfkAfTa+YpQe1e3JkQKuTY4kDOCE5feKImYnaoOPMc9LXDG0DO0ez1WIWXSDNt
6DO/wmcwo3r02wW9MW9zPWdPLpyJVLhaLWZq9MhbvuqQGSKa/oxJfTfngUij0ErWbHsPTID/IZpw
5lpv4wKl/ANi+b2NVplsDgT+U0j8xtZKW5rlS7zud+kbzQtW5OBdYqC7321YmLoImq0mzT7luGK4
Z1w4GqVl/1paToJarvadPRq37Z38BFIhHwXzqrNVURwmEn9Tj3GWOiwknXxCjjyvn9fwdgTny2dP
lJd8pSMyLqX7UOs0K1lKGr9qnKL1uXqjkuDplvuzbsRiU4MsJ9iJl74d3KbG0PFi2mBgiwQ/z1Fe
mivQhvDLPNubFaJbW2MqGMFR5RISxB5FTBLJLmI83As3FTAiJr5uYjN5xd3/SCQPkM36RzGSj05i
gG9WYv4AF/xD2GyoAUBvup7v16DXU7hpUh3FIxXBjbccPXp03ND7uQzxUfUUrKgS7LR9oZQeBJuX
2AJ35MvndeH7+jM3B5ay0NXJ6puoA2ylph/GpVJiqjIBOpKOKRK4dHLpZvXbXsYD/5xoxmHzv7AS
qnywo3qb6ZmDZAeU3X7KE4jhbUrfbGrXivNfLrdEhQd63E+1Y5KYXcx6P6Sm7qF6Fzy2yFeRWeKz
hKqMEMoP1cLozuQK5724RsK+MlpxqbuY4UJYPDDsNkWecXPXyRnJ70pKmgCOL7kA206u5ZVA1/vm
1TBWAe66AJ6ET+PflSadjwAEigruyF20L/i+Tvy0l2xjLnMbPIhtWuVQIzwUaTjjYBEMQDgqXRFg
f0w80Aok1Ezg139vUkpVjrBt5TwyiHYmCucRsZjCvUHeto02rr+WhvhBZ1UvOEWeCHezmDm1QNLK
L5/Gc+G9Fb3pcuBYt+yhnHYleqmrywcrokLjmjfJJYnpcRomFwJTh6IwyQrLIiNiIfaNdMaaezEa
2/iV663mGQjXGM+Umq9EKPa/j8HPFQgxm8HEgOBg5Pcvn2WiokW/uPuyMBOzIuPhu59dUx+vEvBV
dt7KDCbb9mkmJ2ZNg40q3zER3VXDGNB4VwVU1R0MSrgMC1fYEFquaDNi2g8qFizgltX+WxA0EEq7
M95wU4qnDM/loI7X1hFzAbSfowTdpbazwd8V62sP2f1mGE5kU4DHo04U4oIw3hfbmxUDalJNP4tw
Im2/cS6d+sU+f8X61bIc8m6r/4yHop+f1Bxzo+Li2oWBQq4oftAunW6xsNIU8dkotqiL0Lp+wPSM
YIM1VxCLwV4PjAP2tSI1rPeOKAWx7nH6vgOPaR4IuIhE0z1ftMOe5tdaJKIXH2telh9KmObTUgAf
GO1c18sUnPtDxmCMNtdqulYXIxdq6RLyW7xYTzS9qyDYA9YlFNjS8Nb9OXEeJJC5pEmGe2ELxOp1
VDWxP7eilm0lETIbryZ7tdGbNUjW16E3iO153Q86DJZ6SyT/NQKQzO44yruOZv/4tDqkM7MycL0M
zN95/ZqJh4UNE1k2WUdvTUQLkQJyzVLNHWj2MFn7+6LsJ0lXPYSHYzIwgEK2rB1urmNIxfNUGgfl
L1EEAtgFGR0RC5t95gM8Wl3DwEqPgl/R8bwYxufJZRUrE1JABNlC6ZZ4ISOG3Cpf13jymaSzXVhM
7FQhGec+rLQrljRdGfaCbtLg+75kiKburl473BloUtBRxNZaS4tC6sJW4Mqdv6GEZDcydWF3Ao8R
IEUqP221iKhUVj98jnrDTO5/zHkzQhVvz3ST9b5QRIy/E071N3AQ3H4pvX5+HUYOpm0BG+r5XA45
rI85n6LcFHWOtWcvOoNRk+ANrCATi5hidAG5jHGFCnwauj+xPsCksFaFLbtn962FzfYkjxapMdRY
Bmh/VSy7l1Tkm8t9NmSxQ5QTuLw++bn8jn2uw2G2pjmdXoqpyrWNiMVke8/3YLG+Bg3BuRLVPKWl
Ca8tMexdWQ9LfIAnhbLBUGDGFwoEaWqR6P6ZLH5G1TBfBgefg5gGvCM4Zo9zeTpBwcrOyiVI4imr
gHEo4glLLasCHcIOx15Aa9Kwb4gpmJsedvGt/JM7Qw7AxlqbsZYSx75ALxwg0dLup4GUjFiY33VN
txYWVV4G9kuXEOzb4In9VfpGM4ETjQh36GRfoRDh9JU0nEZW24Bv2KfOdPtHu8CqT5zEitOvLw9j
7sWAz9rd4kmhheaHBlr/OkDji5zHPfGUmSEqqfCE11w5aCtb7IhbONg+aFxU0YQ81wOoxDC0OrDn
KA6OFeXCfjbEPziJ1dEE2lUyLeWUXcn97HhWjRFmpRBAAhwMtTBVuZAkXlKMNSkeJtgiPwFOJMY9
oEmu8FHycNCNSnMpraoykzuIryllbmh7XKkVcJ8SgZgwnv/ZtnHZtZLwrNla6oF2uoC2gv5/tcpi
uIYlicqtvy1ZxBTH7Z5rlyl9Mg8I+HoAXt5DaSIvxCyOTH3DZa7zHyVLEzlLOJvP3CfekLXuJy1i
LzpXQ+rsjZLkxbOuYj6xlxZlBsYiTcNv/1du7+8sn+0Th3RspfWl3t+9bxiLAgX/TV2xDGJnYuj2
+CUjaEv+Ogm/c16q216K+E/bxUpdn0/ULxg8p/JoGGeEg4IrUJjOg6yXAXDxorrZQpcpfclz5r95
CIQ7SMGm47OxVGBtVCUClO4GkZA8tR91hy8u2r1GmOejPNueOZZ17tinbqhNIU0pmdaGqXNPZxzT
WnYgHwcrXQal6SqqE3UWi6q345zRPki26mMGYCMyIZtQBu4CZpfzheNTQxqSNDe0X3O6hNlcSP4s
dLf0HPY575tpCWOMwIdHVjfOJ6JGKZPOgaT+fPDJ4PvhSC3NOEXW7CPtlO6WYpDxAaIpniqLQHMH
dz7jtmoLxXedJf096ZaA39vuVpoUV24uYMDCOz6/KX+S48aeZAnl0S7g1QeEiaQQp3Jx0CPa9Doh
E1/p7Cq7O6Uxrd1Iz8495rESqXcpxo6DSPNd+HEfJisRxy6y58jjaXF3WtLYdjQdfE8s+T7LeInE
+ZXQDbPFOwwITYszYOQIi3ffi9jjaxoz23ch5L6pjHMmFrFfHPXjGXE3bAb2kMgC60qAaa18odU1
sP81L4n00aDSLUhZ8vguWylLYCwxg3ks6xp3MXJ0O80ved/PWyVy2Q/+yFhOx2XyEjNCZm95uAh5
QrMExEJ9/+p9KVPlfSpUwPbbB38vn1kaH2DZ3v2JK8rZlt16PHxOnWwlzcUfJ7T9C7n7MjoOrXNi
eChg5biJClf6aPSwivJnoYmKnWWCddtjZWbpJoVd/DHtfHlR18/l8SATw5GRNBEnAkKrat76YWAp
gtKerj/CwXgTqeBVjGl3mdhMBZDLUTQU4lfsNUBna6mws0+9urd2ExQ/y4ZFrm17BK6YAMrxxepc
Pvzt4423q9RSVJpPmg0TY9kGlnwRAGiCvnAjYaxj/bL05VZfG6L6cfCsuc24NvTNXbz6+avXR9Pi
JePdgVBRDwzDHhjHR4vuLeizJcmPdn1jZogHF4J+cLK1+dNeME758Uy4B94hrBqqsY4ecgNu7xEh
YBvImlsbCAPkEKES5RM/cBrcpqcxbonI/WYIFbKM43roaNpJ2LhvOY7Zzg+uYUmmZ5taz6ZGOkkj
XD6JqkAzX2dawJcRrObjMaFDMlTSyTVeAtGPXaFlnbaKGQ2BEPyAPYVcFPtmtlI0fYhrsLfh63WE
tfgdpXCFEd1iHObaQ/jqN14iUgcJ21pshY94ysizWhOuLY9HjIXBOE3dD0A/p7nGEuGa7sQ2OIJ6
I9h4NQO6ThCuafxcSGG15blWi/XVWHuhnHlnB3N0rOxxinpgLEgHIrjQ9zIRs9/gAkvfgX1Yc3TU
oxC2DVF5nJUckYwwmJk3SeaeASkbp8PyZex1D8iCBoXCR0YP4vGV3MrGdDgHZc0qbznvKRWs/NKB
nRtl6805c528ORQYz5w5RPLIMmUb+6rOZegZR/L8E17HVLkgrdA0w/BPYjkLjkesyCTPfJhRRvtr
VQpntiOHph1IhUvK2MUHfPzGo1/jmjo0ZVO2wIkcVX4vuv3rzXeGuC6WBBONU8BCVUm/ShaeSh2V
4JvHBluOMBIOlHY8xPV5NCCUzfSRFxvzqsgeJkKWpGahHjWSwNYStjXJStxhoQMB408rvwCcEvrk
2BYvm7dwjZOpQJHPWjTeleW6IUc/KGxU46v6NrKjfsL9J46tVfDCATdEUGtqvL7JDAPHHjH0j6IK
0b7cAWlaK1qXeErTiQs9WeGXepy2TO767fWXWs/O+d8ZQ75pjU4sNTy9zpUXhvehDtzyFcIn8F2P
rzjY5MdMFCo6C3ASGW5+T1ZrK/tHErygCRQ+7ZnrxIO287XYTZzMhfU6zdPaezyF1CeIaau4U3kC
pf93M12JUZDeiY3nD4cimI2qQTC9tTAt73/H+uXVYyqyOwmf9fm6SHbLWmR+et6kYHlrVz5PfHgI
//F0p+dmxozejat9LKC6eAK6FMXEFmcz0WllWsbBw/4T7c29UDL4DXxrKoSrY2elqgqV0l42qgNl
jg5e9LMH4a4uJVu0vD61Vzx+eYkBJaZ/pqbdNwkijepQGi7UDtyJomY/siTsuRW02rSMG/9gt8QI
clwgVHEdjJN7jnkG/QDSc+g+H8AA22rkDlvJYCltB7BC7dHAMg0WJWQVVeqdBpVZUnmGjjNzMDpo
FLhPb5O9M7+gDCHmk4bHJjc5TpwyasYqB+YW4e5pGzfZyo+LH+8Yo6XpGQKaf5oPsEbGNTppZDtv
a/Pry6GrPO4b1XEYS9YIslVzkJJ+XBIIa5Er57FLdbEqifRLnX6Uyxx6y7q8r8rCCYpYKsd2MbrY
0YN9kNc0YW3ztFJcRK7rvtWYLRreb8w2y0MKbP6oNMmihCaJ43bc16kxNIl7ywqHmEuflHKMsCq7
MFdte9GpYWIaNZd8ieD0ZmB1tEBg4kCiwhvLilytut65hBZp9BuBV78CE7oZbpXQInLDk/QBvIX/
A7QuBb6+2GdpqbTpaub9RcywWlqfHO0OxsqmuUljvjaSJNfuUXc9P5NuYSjjVbANnCM9vuvqglj8
bJ/51V1eJfeiQu3kSRMc4Dli80Zs0fOfaeWgiq0khhI8nn8Oyl7aWbFQdy0YKMla0akjQwiq2SH3
cOM+KDqCOTszdz4XbRkHGgsWV0Vfjs5LkZjVUoy3gPQ7kya9gfXe8X+2wpAc3NuBj6S9q33cOVWs
NuHL08fMqPZ+Fv0/7rIKE2XmdMgxluQW32sX/MzS7cZyBCL89XMg/rdrYkVZ+/O4nPbUgkE3BUvg
d6qGAkDJah/7yV/PinQUzBuAntG8LV6Zx8rm3r3Z7guqnGdbvPywhbNobA9/HcEPL0FqRWTa2YBZ
BRySdSU+PpYMsSCrQHNlHYXG09WNO2VK2fl4OsHiuMmDCzE0Rb0QvQWqPlZWKbgj/XQtVyin6gqF
ev7DDWZrBvJCVOGupN0TVos7KT6ExqtiFAN9d5TwxMVfgCQe9efksFVkct/AXapUsXTQDwYgQ/zn
/GhMYb6ABwboygRPF8Ar3cg9BXqGJL+lQGnX2YWjfJB9zOHT5Wc5/9JaYTHacZcCIY8qRrgDqCxR
k3GicKaVVZYGzWz0ej5tGo1slmoP13XwA5YiJBvxN/aiIGQdWGVwDOjBo8Jo7OESo9l3PSpctcqG
eHEBbYxIgw7UW2OaPfxt7RJOUzLKPs3ctF2VVgtST8tt/n4BjJwvGqhSpNFk7CoVlAfynj1Cezbb
h7KvtFOxVaEXR/DgmiNlGACuu+d4FQx65DQ3w524OC1tIoGFuwAaCOjueK84zuiizxAL1GyCCfiL
r9BrtftNxA/I7wuXrI6aGtSugxaI6ujqcJWfNc873tSlrq8XJmuXaoFDIZ/llYT1Dw81KjHWYYzg
HusflDOoOS+SBsB/5aCUPNk/gjP5Iv2UUBiLY76ktzFKOzta64Fhd8s9jTzbz1VjeYeWRo4NGokB
gUdZbQZZxDVWBg4S1Qvno2I96bj0bRWQTPotiVoh9xPEMkzDvkONylcGPR+zX+bVu4CcsUbgAvnj
ZZcGn1QnmW8A+ts1rQ6HoHuXWH/e0h6Mjb5f0h8ogr3Tkmorh+7tsI5ahifekTFGiNTANscOPhz/
Aju/y7Ik9vHEBMMrhuCKG3ADaFTT5UwryUKtXIQctvKbHaAUQfj6ND5TL3NLjvLd4Ta9g/uq3XEf
ECQE0qs/rvBtZF+vFCEVJGfEEQZM9B8qUNCKnvaND+PpKR0fjFYU0kjK9zSLSLeTRe0J/DktK1oi
i2Qg7xvNXkmDr1wANrOP8lS5pRfVgHywnS9NH9jVsVz8UtaPcFiBzEnLGocKy4eAOi6/rhjeNnxr
htdR4Af6M9qjdNMpwA7r8RA5EqbFLMNVLmxoDIsjqUg8b04G53uSmOOcrSZKrC39x+QDkUb4W2Ux
q5w3MrIu9jfvMnQorEyikTwaf0NaSXB7d2H7hPvyyj5TXlFZGArhs0Zeil3g8naKHFDd6Ri++WLh
Eb5+tq1UDKRLrysGp0DXf5KuoNlPyepRZjEVWmVtDaiBephrOJJCeDwOxFoXBUgVsAAhRT+Z5PK6
9SO2GVgY0WOfJh4jIcvrEGTGv+yfECIBP2w0Y3HawqYduvBdiLWUvT47SeDVYWvTAQag0fcvSs/Y
CyqARu/AqX7fC7r48+JxTjeCDwY0sAWsJ8EVDPkXZqoYzdoEMF0+9IZ+ltJcTmVS1ozvzKJjlC/C
dSJbbRpNOdyqMaVsQBXa76N0nnRTkytzJc49RaCsJP9ZXKrs5M5YkfP2xIN3nlb4EcLiwmuWpPc5
KhkYwpFVha5cWrzzST8pZMFfBmzB8EkhEYWyKGFKgasE++wA+1JtSJog68F8Edz6+qoZC6bQox3O
NspF2cWXtts0kXbMJ4ImGfCMpdbCo+bJN+DPgXXeb4awbYe+xU6BWCauPFIFU+9+7QW1Gyxpw9oo
z4Mjz4uIkx1IR80ykMfpfbzhl1DJMNDYXJ4udRm4TCTIRqi/8X5nFudCyeCWjsYxTUVgCblJo6vY
OgChzZ+qZoO7OSslnkOqIoGTO9jKEHOyEtyTX/gGnfVZJrbpau3VxeEdnPCg7lDaGfbBbXTnT4MX
kg664NXlezdiOfZ2OwaGwSdlQev3HqcD0PKaTTLYH/Cq/CGy4TAtsc/FqRzKmCVD0pk1dhIlQZoG
2IwLX61NgJQio+hFGU/SxWa+NFV8kApPVq6upDULrQfd7c+Y7lrs4U6D33sO73qabjb2xCUAemt7
7lRPq87jNrw0pvlSH3r9MkI0loFNcg7ljt9aPYd9OeHHaq+igUV0OgQ2RiI+/GUpAKdxJH+QP4e7
LVWrAgrQYXZPM1wMMh51/lxZszPqtFWZqGQPccMD8upOiWdYbWeGwDPXKvhQtIaSHUdmoy3n5hJ/
HGlkRIcSV+NpCCxuOq77YVBjEocw422Kr42xM21wXKHNhW88SwQKsWnVIGxaBCSJ9g39bU7Gcony
ZqxxPlUN6GSBPtoyjm1OUo45UzZKvktQyUDJd9rQ1r+yCoi9m27rDbUdIYpqmNlGuA3D+Ce9BxtA
LyJPdLEu24L/VHZiHtzdrbe+6RMrUCOKMMeSteTE07PCADS620iLtmIFStmLYzS1crpdA/7ERYvP
P8DkY2ifTY1AjjqVmwq/y7djltphBPah74lmAUTSieY64DEZbUduOLjKNbwLt0YluGxWLa1ogtCb
mgg0bGmpAzWolvyBrVV3TPjgH4DQs4T1i+BxTn1IDRrhWGZ+JKwvMd84t+t0iK7fioKwENhymGKR
W4C355q8xUfqjj9pNSLYrn3wNoAkjrdDce+AxIZsMOMK2lSJMHPPzDYaOhH0qFI+Sy2ltUHfJ8ak
+yMrxYeM5r0eDYsBI51VSkL5PSB+6TVJlskj+IMUVYBcnh1taXUHUAnRU4/qVBQbhVGLev+AvYsU
jnYJNM2RlPYJZ5rKZQwia7kdYXATXmy/7vYZOKGU8tYeOlhjyZUW3qcVu5pE4N/B3ZsINYIgHFi/
FUuEi9J5KxZR5YlJISAHD5bFXKevQzTXIiKpR6qgbw15a6bu6C385ywg4FUZ45PYUk/F2CCd0sCf
/B4C/j+yehPC3bygftb1FXQ5sKJOsEhRPTXPn4ybEbcqzdqo7X7bjEzHnq93WbjydCazJRj3NhR2
Y2kSx1GzKUFkifwcr4kpx7VPzpTRCSkllhI86+++ngpC34Fno0+XRQrxRxwROKsejYS0v6ZGYhmJ
E9UDmnvoTzPspIt5zBaFuSMNZTFQbf5tE790KdIS26p+n5ggw7+PuBWrVry1j0xc0X3Wzlq4dzM5
CH6fHXHQDUpVCV6WDrUgHKZVsPg43JQBtSJCVAePUx4196WET+Ji9HMqWqI9RMJeTq6wPVgSYaPP
RQeOZu8jYecaqH4Ua/Gqb0sU5irutRDJxQtu31QSMF3RZ4siB6dPs2ghjdOSe52hsGk2MD19RJCy
p3xqOy6FExEqEFTaUQYFbOZzoNgrD9m+6aqifJN4lJQowOnObnDldUzH3SIurLTJl7rp5UzG1PZv
DjjaGwdyUh1ESbmGPa4VWg77TEjazH0dBugdkSJv3cWRtiMBkn9PEYabK/2HnVh6nmnCw6za8tKh
Q0xSfx7PAh8FBTXuE4fBMoHEdT6uSVmhyxRdgg++uZyrblLsLH5GZ5QyNjsitYjfuij9+sge/Oq/
PmM7np41RkNuXUihDYCJszzVNeg4SqMu4FmH4DFn935l4/FjRgW+Qvv6vNI9/11FZXRcNtbQ4l3x
TE3c9jZiPqxiPZI2+BI0hzE0JyOLYJMW+kGScID5LL2oaqaxRsHIxozlkxbVSqE33WgMfwxVQQTk
6BhKPrhrdXiodr7cC8SS4hKPBGkOdQEMdyHQmc6jvMTB0DTI4s4qANrd6CA0/0qOPgcRoJ12keYD
1qn1Nk1j/CV4kdk6ohRpxlBIlFw2CblEw7PsKGM3biaaC7HICp8amkxUXbBR2YjdZv9JW1bEhE5j
ZLwHnqlCc+GMw1wT2MtJVpPwc7m9mMXBL1Gi32gt52Ff9JtYg0zh/ljgo/VTk6Zy/EUaTO8RNhxw
1oF434bTRQY7yE+EIlazX+7NwAVbW12QrwGO5uoyjkXU+N4cScmdHxhGiWjuowIXTTzoPoTL/k5c
YYqRHkQbeqYWXREKWnWFXTmC6nRCwyuUjUB2sm824lqPSImUdKC19itpTYCCFagkL55p3pt4MRO0
tPUYRBhDLWjfgrghmcL0EmkDpfH+m4o6R8U3FzSpTF2iHVw2Ti/0h84Sxz/OZOzIp7T2XEz85WLA
IsY9cZ6I9VXqgZ+fZw3CTEX9zBqqMRn7ziqtuwA0b+XOr4hmsWSXjMA2anDviKI3+XFeDHALkyM+
P6wnNASqnc/FoRYCLBqtVkiPxZgWK71oHHHH4uT+p6/9d8s96k2HQwT2HUA2OZ9SCeqVwlOtikXU
GaOH5mEzzd6jfJKhEibAqp3EIsoWt29xHy9XfhE9pL6kllalp8CrqIJPsccfxqScgqGU76uWU4t0
1N+aOA7DyaYGTI+nAmZrKfG3cMq66Lm1BbZogirluH2PvrS7Kye2ax8RGSr/BG2LEILXRq7glO7p
CyvpxlbL2I09BidOfDTHD+jmIRNjmntZ7ZaSIHL8lCanm+F+Qq9LA9WqxC6iyVHtAvexYLF8Ppy3
sr9R2VqpNqALYtUCF1xgV+lJFpym2mgzCUPsC0zTZ0Qhay34NjdZNLeV0w55P4E7oVyNg7HwC7Yk
/vuDgEFh5/uZCJv5Zj4oDOdYiDcHNh40ADwoSgVOGK8fp7/32/S5zRFQW8T0uroBKmynnq2XVEAH
laI3YKJWPUt8yTmtmEOdj0TiCWsgl5LFhrUJU9lShH2yfSWhuYoLjS9oiLPCz/b7REy1F6Ml1kRe
AUYd170EKe3iOjXUefcsDgvWQ+bdccV7VfukVeWpiFdw2k0UZpi1yzUK8vk0o/WOgX+jelLHeT6d
3uUzI6pOQMTFoUxhGSjdx7aIxWgfGphMfVSJQf82rIzSrWEB5yd7RUbabbVBRcS0fbfsTWkymTx5
7IyuV7D6VCtqijrCEge2KlM3lHsjKPYto87ibVerQR2l3xHy6DheOqnyWycI5ZEvvJBQyLs7T4bt
q0QSnWQM+r8z4qCKQ8erhcNy/AZsmhGzE+zOHYCDS/yvGqyxtIPJGB+sIpjiN3ol7KyyaxaCBT2a
mC1EY4pkHub8/tXoN61njbUytkvGEfpLIKFzQoJDkllroZkCWd06ueJETWOAFAmK/SEc4YMf7MJ/
hff6ynTN4lhthd96Le0ZJBbX9ws6RG7hhSSmENWT0NEDm77SMN2dGrubw0+x4apWHd3aAt9N7IlS
VSBOVC6ajWYUk0EAmGpm72mnTu3J2xxB6u+R0sz3iXhH9r2moHvksYg0zAEAAFbGiuW6yD+03uxc
1oM8PP94V8YrMpyM6h5o8W1MaI4akKIjwxHrcexbXUxJjKgRO0aweNZ9p0TScu1EpwrclOkRCnqG
y+iLFT8Yge7jce4PLTe32rLHPaCcbWYgwoLIfb5b48ML3RDpBBMohAP4/Lig6KL0uvo+ok0zeQwX
nu/LXJmsFUrfy+cjeIjMrNMuKktJoo/QcgNIcICXbbSDg6gRcA/dPPzjGeDcGBQJq5e3JugznQak
ilWSB3kMNcI0k8dYnlDAWPdgBS6knFCKnPzu700OaU9A5rV01YWRWxwErxAtSuQcvCsxAs6tP+UP
lZzBlFzkWCHwCsVlIFn2dksFpRg1u0JqGz3TqQnTUkedfprcr7P4F5k7Jz5DkQV0+4rxfg9bQeNH
LZbTJebyJuGS4tpEsRTDdl/yj3WpGWAZKIaAuZOkHd57GDTaO4L6wqztchEMBe42OOMccmCFUDAW
RJ7e/ELD2PCV3PJVkvdaYG4YB3tibrnh/xj7GtaobDDKaGAJl/fo/jky0bUKZmP9pjTKLcPK514W
ZxDBnCJjNvJJRZ1NmfM05vhjSfoaezfK/6gW8uxxJjj1eiccLTKlyVbWgN/RTrh0nLo3lY7f5s0U
cuMuy99iaTniZdDN2eky80Yt1JGnL9E+Kc+90gcSDdum5/88YCV1LwH1RHSINPKVHX0We7YnEAay
9waxFysXi28eQ2EI1brQnjxWBXar63rdCRuR71TTXGCIgkPcoWsCIHHqM6NrN6jLzM/LJRKDKt4j
vSlJHNvPk+n9bG+PfKEoRn15udpoVx71SKHL9/DkERyD2haKf3vglMPVWWa9K42wY/MDXozMfiPc
X6q6beKvSvHIK6wgwqfdcEsMCBrDg6yOLWYleBGpF2SXyVO5jiS5t+dSd385D/p5MJ7k8ATJThci
4QXevWVwg0yX7/TdFGRAEgcA9r4ZxxLlYAwVA9MyXaHslf/Z3Maup0QR5Dmuu23BAG68odUcf1+6
z8k3Q/yGA2YkUYPjK+NGxcpSC24WW1ZtxXOG6jN5jpdcJosjHXay576xJEFY4rLUdUZsn2XHbEVu
WY5F6gKb8wXBsgq/IAcJ05/ZVQMM4sBvPfD24pKdJLUF8Qmc7pu2vEFJAQJULDJMEk41Tp7DqVMT
IJbPOuUrtRiYDqH5mm8LwzT7vweZUICg5HGMZN5SEeb2PSh/O8U2o8nl7lRhJSIeh5XtbRJK+o9s
4iVQBfMKUgeg6ceta4mkh7lMRr/Kc7bWgPIwxpxyudX37CGA5HkQ98h7M5/99PhKifmOXlojkfzg
xRiUHCwoSIg+cGZJe98gLBWUkzNgs9U5JD/QryWi0nGV50tEvVItxbOLqF4GHx+U0RPvtKorDUEZ
kEKiT2BJauryTuoANW7jo0SFBxgBivaTZiuC1CKnsYKM2nSYWNhOUYe9EeyTOJHpma8Ug8eryxbb
wfndh44USQqhzb7TEvdOVPcYz7gogUWFdFxNx/h5UMxYBsMku+5NZHm9efIYqFksHhHbn315ZZkR
RDSEAhswTxNF02M70P8yqCxOeYNdTpVIUu0WNT8Vbj8DUnZymOXIkAtVGX2vjhz9+5AngI/w8Apw
Y49xn+0udB/P3GYdxJ1go7aQJz7/xiMhfW1emWwSoyvZo11S7lYrDjZAQVzpMWSa0j1I1bWHBp0+
p3igYCm7KrkzxmVsTpEKFL/8AgQv5Vvh/kX6hO6N0c8dM6cfbKbKAMsFgdX8+KsX3YTN0EsXqIgt
GCgXlL3rYjnyLk79/zB1hMCy3QDVk+1xYOlDaXGeMSyFi6g4HMOENENHVZjH1YyTnDsXavhCSoF5
oYo2zmn4x4uoc3sBFsNnG1fQQwy2qC7cwYHs3CTG24+Qu65fQ4ihDcawod1r1M/U1OQUwN9KUOt5
OvkNtUxKKOP2rxCsTUiOz4ps+iyyQPz7P+0Z/Pi54JJ3+8zMqxD2GlPpdfOr/johtAXERBaYJegJ
eBjUdjojzQQgZNlItzW7nDetnrt7Drdj2wvXmRj+eSqj4rP6buxGs+MIwUyLWy9t1SA197rdHn7J
CPqN2EBLwKy3AFGi0Tx4RPOL5i0DOjVxoIO41wwzZ+togKSCnobhV1jT0orkyPjBEkxY16WX66Cu
X7EUCYE+W10mY9Vzclq9h+3OobkWoxaSkzIJWQAWSYJAO3mDtXPPflHvSaQp7zYV5qUVBdWGDupX
Wdmt4Iy6cV2NDk5R/iEwLE9+kICvpgQw3OuwyhXLhgqJwsNNP9RIucGkFAgc2khIEueHt+wLjvSd
pyawPZoemb8wI+Ba9Mc9p3yXheJU4pzuIVgLTpPN6FLD3na1r8sFMkrMAia/NgnAygdCHy+CIJ6h
fNLhRBaj1qhb/RnilCm5VDQLvP1AfQiVrYYHg2YdG+vqbgIQa4NLCoJz63wiGbgBuVg+qgr1MUOp
tkdoWXIS80IAmYTfzrlWGj6doN/QI8wGCPew/vCIXCQubqqPpnRJ5io4G1vYoDsUvwBKk8RYehds
HnWsNAau45R7tnu3wSDU5UXj2OFUqhOrIkGn42YJfwKmPKt2IEab6reIFpFg93+84FUpLzvV3ZlE
FIUQ2Siqol0s9c4ehKb5W5RnZFZCOmJvpMKEXlVb04cGDpJHLGzCxnhaPS832CwPgBULCf11Ztfq
BXVKIbAI7as4OzX32GtaEVwZ9EzEq0U0HWMZG8F4xVKTNL21pmDkJ7LVUqYNd0Optz6D/waRac1K
UqekHVwhWd7Z4JrZquO4er+ty7ysUy47AoGJS6DNdNL0LT39MinuDOMhrMFenXuvXbVo70vtLyMw
VfUKEGkeO8qftO/I5gxB2GCp4MsNUBTVBNgv0ges0mWH4ZJzatiQE/3s+Mjxa1ST/bs8y7yYVl1z
qaqx6LUF6pAQ6e8Y26npowJ8MBfll6CMcWWH/EAIjdtSuBb4WiGIMNi1u17Yo+Nb5G+FBVyACQPA
+5ingmudjS3yj5roZJlXmya6TzZjs1e+QGgo5rOeLWlhjPHNLzMck4jImAc1nEpegpkPAZuYgtgz
La6fAFd7vx+VbIGsoDIq+ihbGtB1IPzLRn8jsboNkHrx7xTAO2hZZbwWO3kJFJ+fK8jpBN6t7Ff2
T/meXb/sQPYY7NvqsnYSY+6D0UAjaYe2CJs+W9VI0XMQuZHCO7VQuc61pcQB6McT3bNBs5NdF/+A
SbtCrPiuhwnpUP566Yc0BE7y3jXo0IldLWRpOCKLFTpvun0a5lddIfFiJLsUnogzK7LlJHNOoirb
Q7APliLF7xKsq0AdJ3eS6CKffT6VUDlNOmq0nqAE477ELKTXzt+ajwssFLLdiJymUy3tWesNwvTn
PQ2t3CsSOrkuVhwL67BBMMLW/ZkjSlKmtP5XvUpBMdClry9qOpcouk+s0Q4X3t6PDmFK4IobsQBr
FDnM6uKZo7XM7R+8sGSr/s+0oX/+m9mHr/i5Loz/brwoFBXgGq62aOOAVgcXZr1YhWOIunoWcE/Y
2yNpApmuGLwMr1eE2So5yH96UoWbJZyE8QegCXMxHvODMfCsq95iILdYurotbJ2fUbfmB4vPqURS
8P2oLrgISI3Iwv7mzSvwxjjSukCFyWFaOjmv0IO9hDnWVckBxUnzF31O/zCEHRluutZP8PLo9bSI
jcvt4TosyQ7CRLF7cQY/yOzalw7dX5FrOXbp8N6fFd60VPzqYUZwSjUYwSH3APlR/RgN9s3s6bk6
o/FCM90TjfbGKCjLHCwNW/ezgSuZ/4MT5yX/FxGLsT1zvPng2I3/euTbUhnZop1oapaU5S6dk5Ps
6oC0eh3U4XfoGAMdYnvQor/zMH3lXcBcJCFJjr61BynivtXk96kBTtu8HVZkxSwNiX47qcvxSTT6
zuCtMbO0PliQtecl75leqJ2newLQtNeM4dEbRe2eppqwir52XcgQwcIA4HVQyvAxKHs+Q+kThT3G
uIrTNvqivYxoVjIIz1nkmpU92ab0K16loF+O6kCbiLE/l2G42AnZTCqZekq7VcG3SsXnwos7fNBj
8ja5V6YxZelCkchWADl9fOEFrndayv70ACdIYv6RIeNLd6qeP34jlwwBpkCgS68vM0ilUSr/PpUJ
H7r1AE7LgSh6wyETGBo3CIyjavesMRU/GMJTYSndhSF2me0lM3TrgfPYAogaW5iDZAJycnLRZf+4
P+xf6BLspxu/eWG9y6W3S57kRPuIyPE1wsrZooCaXOmj13YqZltBBSqrfr3BMin/NufRE5IB8po8
P+a/hvm6Ko4Mf8MlyhLCMnj+hOxFU+MPrRcp7jnLL0d4DJZ3q/bXyUg0WF9M8qV+ALv0CESFsfkf
W+xRlEsErqSdu22kElEawY7ci8jW3vMGu3qinM5sBmQNrTUYJlnhxzxUYWlxJ1PkjInN3IbFGd5C
FQk7MRKn2efXVPwxChOwjieqKBCBSlzarTF6ARZz4UEgeuT8TggP3cZxoeqmQVifa+u/mVpKFwJP
SEPy0RgAQEiCt0k8NsK4n3K0UsB0A++Dgi9s707HmY2bm9jFUtb4MSM896G4IxO7szhvIG1yLJjC
kdpGlpLwGZDkJ48DPNWzSkIjyXqfu1w/8sRW04fCWqVUlZe820at4tWVBPskONFP+yD/v3d/wJKE
DK/UofLEElVwr9xM+DAItcYlZxMtPy6gyxY3kPqfHEl+vhAez55LCgxfoViYxbfJXWzaSa/p4Wsq
IOAPGqJDadWM9t41U5q6msm6h7cFCXSDEqXwiOUdhZ5iKczaWDHFCU3t+3Su6bLLi5W0YDH/ynw6
HioOwdoCWtGhjpzWTTY1X3uKde2fDfWNbdnJ3ugIdcE7JWJA+tJzgtM7CX6OPvlvMR1yDhOZP+Cu
EHRsvFUOE08Mwjsgjsk44mX8FZKERHTtsuO0xTbHSFFDQvgemudhu+kAJ1Emm/wKsyudqgNMI14m
JrZ1GEmLdjdwDvyUIWt5M4EgBx2H4nNJzyUgofTKEPPcYEhpp7rbHSGMwnr7p4ann9AzVp3+076q
hr3/e7I4gX6uFzLB0b8OJjHhPBR1hvU3p3nqQJhUyjCp+vp0SGisIorC5K+ZuQ/NQ/wvTF+3Gyz/
QtnFDJOh4KLJ3of0Tnxoh20gOJmRkAMTKFO5NdURNXu47g9LAjAZ7M92VHRyMmjxYSGh/v/VEVBk
Kdf+O5HPoAEPoOmZohOxwQAsEQe8VbtwVNk+ryh7ROO185/NyUXkmFKGNXYUcUTMvTD8bFFGltrk
m8GoQ8McksAza6PKqluIIziC/Rw4fSqwX+6Tywt7np+w/K5HXT3vBIrwWM9xTws7g+JKs7BRuHAx
9okW5qFjoXIuWMYBIAj/iH4vMJ8u7X1gf57gsVT5hCHcRXpwMFiu/qHIYg7THQlhEpOECTxpfikO
igzx+SoOvgOIBuw5Mwo7zbu+36VqIbgZT9v4jRuefl0v26xYzJTWi8gy94etSYooyh3vqew8nDuP
hQBjsg5P35hD/wHao2lkKQ3e12AhBkb7+u3GHdo0M54XQOj5/6JWNio3Q2of6CXQePtXx8yOzXg1
TC1GB7om7n8cafK6YLey9DcqSYyx0T4z8gwFDROaORVklgnufWCtQUI0cewex+9kok3M+BRVNcL9
UxgoaYAXixeOqnVIvcyWCZmPAMqDeG5ldtTtnAcTFCaSjMZ+e34Tzxq1l5A2LrNBpiJtimcppnT6
z41b0YvQerZMdzwT8airTYeqwYlcjXfgkA5lebVz3ZBL3tvmBGOQQ0c989soSiRbAmsHyPIED0YT
ak/KSBLknaNfXDqSuAQ+sYIIFHgJVq1z/8n7peB6LWEpNutXwgHX/yxewpVhEbm5EYM2eJgfraNl
QjiftHS/iVf71FzEBwF8fADM+E4fk0CXSBltxLm8N5DIcOxaOWi6mY/f4qNUuq2GjAzLfWw8Nz2O
wbhhvA5kJugED9wHJft8fho9sbOaalKya0LW4f/ZzLmi2bwsXXiXy2XEKIaztuPcHqBqz63kp0/K
NFMfhQ7dc5UxYddz6nDfxmq3WmiYRjcVbI/nsaGKV9ohIcNDi+mmbADQen7G8eu2kJ1vbGZq695O
mHv7LVhJTKGvL/ygDh+0uOZtk4TZBA5aBf27ziW1zHB8peYRhY7004sST7NzGOYfS6GLQvXlh/r+
BgSmfW/AQo46ZGkFpgXlIfNWVUnNUhqPn8kz4EZLqQexqZ0IM2uGu97H5Pui30RZ/zxJwIKeTQjA
G/ts5vvvq62TfnAkn9CiayGuAF8Ga9cR0so8iiBZn+GaxtLja5Vj+L0gZeWs26P61XYXnbR4oLLh
Lf1IyvVLU8PYL6WHB9dsbpFSGA0FNuFAXBX6ICWKixIwfLJr4KkKcOb6pgN8wFqybO4WmK0PCnT2
c0waKtBcQmedxQ1KUkrX4XThY1qa+0pqQjbAZQFrEaNr49r1A62qcZbQxENQVb8YrbyMhtMpM3Up
/uTNi9LU/boQd2NhcnNcP8kGr/63A/jsXNOviM933jkS7DMZuztnbt4ktw2lJJsp+8qYAfQsdvPE
xxYL7p+/eyDJkp2Xv3Z/b3RN/YaeR/B+KLCy6Euq/fVUOldpqwA8J7+gdHxZJXfc/geqBlowWh7p
7w3jYUtzaOtKxrneteJN7SBk4oRaK+KpKyud56EKEq21cHhFUcz2nokN+xcaNgMvcyvLP0uLaDmI
1wHiceCuCVGlNBQlc1X+XeJG7QnvG2bm7lk5SURICDu/tLiuwz3Vhl3KgbPgGsl+guuhDdQhbRV+
MHpHT4DtdGtQkWUgEp5dwLssCeY5j49x/D6F6o1bFH6HQRKaXZN483hxNn386S7y60AJh4RZA4iO
9cQLO7p6OdawQukpFe/BAXTsD8gNOwPcs4f65VxpXh1V1EWJPHVCTopqvQL1QHgO9f+TTRF4iUyt
dJeddirj3qTmo46Fe0ylOetGIOEpLNicCjseOL9R0H2Wde+2yUFg/lSYAe/70uL7x1kFYeYSLsCg
AgIUl2YOJ9UJffFCvc/vvHfOR30wIptVan3vtuVdK2qKQnhf7HMFmi2oEC0ksefhEvH6GuImyInU
OZ5/3XJFNcxbX7j2Pxq+siT4Ge7bUSj5iB4le7nhLH49FoArwHCBu/xINIm9dkhYeGXMyl1pQ91i
eVeI6HH04oYIZDwIHwnboFUq8zOJS/1fsMEmdy2Gkqz6g4CvVw6VoB4IL0WbiHv1McnKLGw+shQe
w7QxRWxdpJ9g3XfclAQF/yV+F+cS0f1+RWOth3ds9SlmvvGpnrdiucpk1YBsWFNMRdz7VSw1d/VT
xm1e4FT560/+FpoPp5OT2pfbwDDYAnkyclZpkyOKNliFMJ9jtoelq+mq5lT18VRHcuoX3TdaSU4E
onLK4ebczVMdJjbPd1IXoYWPYbpLWYevpTr8NxM6C26rBsQD2UTh7SEo3QuR5vyLC2CBRt+vKX1t
Oqqvbeq4Q/GrLY5KmeQiXX+OdLlTcI+mtHRAB3U/fCplyw8EX+eQIqdSMs4LIQo6u57a2w9S6UwN
RPdz9J28n8vkrkyqEnt4fg4B2RICmc3xLqPcaIKUhFOutt46Im/NJMTW7a2TYyjYpNbY0wphL79U
4CoIIq3jI8CTQlck1ERVZ7wxkMadCqJuFIydChPimX0mbAfLsx/mUlqpozQT29A5mCiik7e/PSzT
0ri/cNOdF0BTr8WSEl22+FUimW+/lMQSMKrYabZNYENqjzRG4ElcHI8l/tOWkSYQqIz+9ID676T/
L8vhfnlZ7qdqrtR9WX7NEjWRb8POXKbq11fYrbAwQeiCmgRyg0fwdQF58NgHOw220flGqJKcaNjv
pVrUyFPYR2YmBB/WZz+fNBPC9rJqzyE3BEcRFvBBs9MMM6m+vMPydpQYN0I8X01rYx5F21NVMykp
IlXsIguxVF7z0JeBw53/7aHnd7NWHYjCtcLx6J1KNyiXBOTN2IFim1dyMYMCQlzHsc00Mybd/Gkk
1X38P/+trUFp4g7NPORqFuEdOUN9lS/I2qsYKqhB3yjmMRBhm6eJRArDMpvk3ltbNIHGqJHTOeAf
Es7W75mX6bhaH6cldBn0PheaYtjjK0JwvRoV4xrZknUdCvkAIhqiihQ7+mvQsNTMaaVJh6vche9W
Dq24e/8frCGfyPFd8/c0mCAutpDP72VBqTdgCPVtqfT6URfEYhf15EWaaFBgDpfe+l6KBY5x0dqM
czFE8QURUuxVhOsTHc36Hf+zCoh+7YOacrDcXs0vuCQ3xiKEdNWgj0UTUgZDNyYy1hTn1hOjnrHH
hTKWlJSwnB5Qj7aHVmcGjVZHKOMNPHM9O2yQRyKFYCpQ73/1R6eapYetOsHS9Dl1Kghza//c2JZL
DEdlAPtbvq3OF2xvLydWFmLCLjb9f7cb3Q97J9zbPLxiVtn+VLWy9IENcIqq0KVi3/AVx/Qia+x1
YY8xRfNrtS9SyRs8THjfR6OAYY/8g1yeO3UyzkOltx/dxx5G+NH02EqKeRacTznJPWN2XVxhCJd2
/qKRqtztc+/6hfxsLbUJFhnm3/agUR0yB4v84kip+Xzrdj4PKcA/7L++VvvBejsLRk6RHg32HnPl
jyOcvLEozilH87Tj/soshcV2ZgEr8T9USK3wq+bMpsGjQwKJ5CJBjcEtJ5ir0ZjuXJuR4Uk9qnhp
11d+4+LimpcWFh20mGDjKDjuaKoQN7Bh6dz/djzX3EAfjuFkWYoDFLUiFF4J/KWQO/Sg8VG46BD/
zWe/xZ9x7c0BfugwtpHIr3rW5vvumOmWpubs/bI9yhWRhOihrXNxGm7N4Mf4XTOJ+KGP+Sn5b1a3
XxfEr0Sm/lsBlDe4UeSYxG844WayfNn1mvv1qncIhG1u5Fgrf9GfgLe6w8N0Dl5OGE9gSrijvSue
GQcABkr/vT4cnAqKWWPw+3s50aPSKtMQ2a2Iz2EiVQFPOyAQYjAUpk8rFeTQS6IzOgl2n754XYrq
9WsG7YICNny89HWUfsJ2fw7i+ArMnBHwDLT2a4tc6moXYl2oa1MnDvFl2KiMGEccx2qb38amoIJm
AUoP8Z+H4hgJnmm3lWvPnYoP8PcHZh058MGswAvJyTyQgyJwJm8DJqbMcJ6yEM4K3oRII3HGFLaY
71UhHVq4QU5O2nQxCONMeK3THN6Ue4g+cdsYc53hkTlU+5bsCEHVw5Q6pgPCwWGh93IJ8nxH97JU
ZCeKgB1FOwBlLwMOOid62dmuxqqzM8v4UElhYf7m3sktOx1/6mZyBIEhJWAfUHqPScCWTzm2pjwN
12CtdLAzLMayWupS1zNK2lvLOT9Kl3Nnpkj1zhBfvBuPNIsO6P9bJaECs1hGtdVZMmG6ne+r3fks
O0+pls3IKC0SvEYsIW6NMQGENT3VqWLH2ds4vI+8qKNao/QoKfOrYc54JgytWWSwT66OGUzPMc/u
FpLDY5PO4FizLfq3w7oXImP7nyP6VgxmDTqe82mMe8An2y8io6H8JrUdi3NdUJioitCFzgN5Zxof
6JCRyy91PllQaQeXVPmb1sfZXJrYsGoUi6CETBhcj/KJrQgOMT/mWWFyofGKGWiBNDWuoG66eKdB
+wG5vdSvDYqme2+NIUtBTfFtWuSQErrDLLvHmDsf6CH4p4D6iUbKsm4GGBeVWOOJGcdvopCn0aEa
jxIkVQiRWUTZjhEwJ+VEgAoH56mtbQFr8hETLvMBxk2SeQEaPFv008v4AJh+ZRsfz7U9hFnnN/2N
WptqDZauJVKzJwt/RRMz2vADG7NGGliQU0L8Y5xVByrzZyKYUSK2H+U87BxeyNQBjK0LtycFRo9M
BolcLSw+66plhJLlux+YXNJMkmyMNLL73snYs752CWcmJbg+O1zlbO2HDIYbC0HOTxPMDkR3WRH6
45TZkhE0v4JqaS8BT3bvurPVuVbkU2dr8d8fQqTHwFbhn6GeD79caa7rcy1z1x67FGymaktpkzmD
kU9NBVpQNK2IQDB0v98VfHSHKDnVBFc7KZXNrUgZKL5XX8OrsONxAsfJVaHr9qgLIkEPkbBaco1x
AZfZcBVRyCOTmN0nh31tjRChCdPaCUMzk35ZiIA91oCr+7WRAtSMrs/13gkiMwLIxbFnay7NWUpg
65kmmrN+5SbGrinqc33vVGrAgdk0h3TqJWE9TrWUu56b8B7AaccQAWjyDNqhDIrIZPVuKEP9Uy3k
VWt51YA9KoTGTcoO73XXiIcOUmONPYDEtJvQWfPIBLNbMl7+AcvMml7nnp0FHTPlvbL4Ew3QyxcA
n3OnLJjARdjnWTSX+FYs68WD5h9LSoel/gQmyslMD/jGTnDwfv6+3oyH3K1Dgi2dKbKRPa3vNq4N
tQhTF9NNgTKtMKinITqSiu5uB3eHZOIcP0w3nG7rl/7Y/uL/P8XPJ25EgpywWOSRKFta8NEouReP
Jl2xVeVGC4m19vbjTOdcyk8oMbKebY8lkDZDuxxw2SJPTfeu2e0wD194MN1ZmWKzabj2lOMO4b0k
ebi2b6JR8HSTMFs+CPlLGttbzgZUD6VAN3JO+I/JzcuE0/3Rfl6eHgX+0hfCB24V3hogar+YB2e2
wrDVezq4cA3TUcZI1/1tvlJ1R5ELitg1+sd30rtvIC4bmY5TiilqG9HZ+efsOR+FEp7L1dFAVkoh
Wm4lDrdzVEs+h8MM1cwYwWMG+NXVRj1NIbtv2D6W7E+3yxseydcnOsxjOxsW+V2Asqkg3R5an2Ag
fetDzwHgAhqrIJIN5deaqyw7sPE+mFg5FQPN10oXP4jXu7AlbZkYIHveNmAIJ0ztnEdI/UhEx0pN
aKN+EbKqna4G6ByNL6tQu1ZkWTMq2PlHdagpQ8mwrgFnRLZmUmZY9QEze28rkLpTUYE1bYcOurTA
oPsrqw69pnj//Va56HsMB07WcK2fyUP5eFlRfBE+/BYfAZK3e79PHGToUd3fx1ctkXwGz1y9WbIH
I5fwhcwh/PeakXx6AHH9M90aPvbo6GG/j7Wu+giSoZFrtL1vLVA4+POfpxpm/1eXlb+wsnLFSAq3
6uSMuUq8mvnLZjZjM0eJQfSX2GScpSgDRv/0zWRPjanupXZDeswpGRyC4EOn9UHTNmSJG6Bvplpl
7ULV+wZEXKyW5aEpImln1r9UEm616X59zhUO5jyZm1tTlwSEIlhTEWacnLrFr4x0bsPrMbCWQxuC
c0t3R6QoL590y3RL26IUXJIck0iXP2mtMO3DTvX1gHS5IPP1Scy7OaUFYZJp7gKCMXRdmXOSnHSh
D/DheQ2j6BNAtmxzGi03Hub3HF/qK9ytz6D15eMxx/DqHpbMjCvR5IG9dc+uDNq4uD+KmCU3jC3H
2HjpFM3Ax6z8WQhx642/6DeDa62o9e0cuk6qat9s2u4ANG6F0yo3PDxnkNhEBy8CqYDZ+e4DZaj1
jYgfnCZXaxI6EusHPPijRw37yoai3VJ3BCYP5U6cmZjGq79uyw0/NPkZfp/i1UIADhy0hR40Qnoj
A8PQFeSgGDbfct1dsBNIYhFqN1fyUvOU9UBB8IwERYi/bs+gtw9gj5ndTfU/Wp+hkHA5SQBbvgYT
xyNCAXhdXcfwO2MqOXWDgzop/spQZo2spiZKBjLmJpyKykxPVlw0cZO1R2WeaAt+HUQyD1+d5D3X
qdK59WmJRy35NY0WhdNmgFlh6ifPoSOfcyE8SrPxeHwJW7LW3myXpDQVpm7dHUK4XnI2mJ9TLEBG
nIuOGfARIzMKrA5L5c20nzc4l78Iqr3P77j3bBBLJ4KVUIAZ2BVYU0GqLYFQXa+SERCBD4DzAzuc
nSuenf2ffvuQv18TRLulzcP7aGWBIwad6JSe5nNuX0Nh/ZF92+twH66lgXMKRUnfFbT2+PE4i0Rr
JWAtIEDvEShVVcNKAIWf6MX4hioYKaaeGrsazKwGn8gaw8KYjaEiL0fbWXdToetVhO6vvuWnFBd5
d8ImLIDmdK32lVoG9008WMv8lNF6KhBO3omg3LB+3+j4k5zenJtS2CG9wo3obRVZyWiCnt1/jQnc
iVjIqQE0VqCRdNa+4dcGve1geqTIGKSjxpofXqz4m3nsE0A+HsaHM2cnHEQxpO7nYBrlLm+ruhS8
H9k+W465Mpvo1dvgVqfz/fRhqUjKBMV5fmChUl4RsWGNMuklmr/n4RZh2oO6O0TUYLWDk2Roirut
tG0uOEFkTxTukyAAkd7b7QQ4o2jJVs7vC5RGmF8ijfSOit+sxWJBjZoQS9DlgWxKUk4Avuo/sFFI
YlNaFvfHeQyKWA7ixznJSAAKGB5hxwISVxRhNRK73LSl+aVIwwA7AHAjVKHQs5wFk8viraddf2bL
NnLtcLJou3U8siXT6vpOBylqDpxYcWP9/But+D33n75u1RY8tNid9NIb64EXRq0h0/KftemoBzh0
9C+QHozx7v6HsPe2m+WS5bP8Lt/WfGQIm7KypRt+6jxqCvkQzav4cO2rJ3o/3dcEQL2AdrHVlXdN
enYGuymlfiej+7Eiwx7/08guQnazhmEYHCvrc0tOvk6kL3jhtpnEgfYGRat7CaWpoQk+HXlvkm3o
JLXm7mOJNRR7pK2HR4+/VLBoPFuUMaFhAtW4Gup6lCpPp0KTvnnhcNCp3vCAcI6vsCeTRcI78XU5
tmajtmGSpdGCWwHhKxGVuRKPxu9o2oAOTaF9XIzE0F16Ie/VWGWsqRNMIPaT9AYGeQuB8yGv9fSV
Ejd/3j85cKfNkOR3mwAsO07/17+OCrni0HZG48etnxtuGF2tcuBNxGHGTkJ0mhjdK+JX7tgjR3Qq
8Wq4DcgQB9cxz3awCUbMd0cTk6Lsj3DCX8ej6SyRRioUfYtyCScOo7+vx6Gcz/ldesA6sSD2eOmi
Yieb9h3P5ARNvypvGW9WbGzqGJkkVSpH92nsTVXPxCAT2IHQ9w8iysZg0UfGTN7JPHy2Nb6GZqEe
LOEXDlRmUA0ybrXrzrMjUzbXrsRARWrg6qHZamhL15ciNaIZAd8JbzxmPX+gKvJ4XJMIxs7fethz
NyG6TggneAvREokeDdjtKHJQAW4OpGP9lyTuQT4JplXxNQ5Vwiksu893yl3UKrt7fy7x4lB3wm65
1272mkNp6siBgrZyBFHpxiQ7VyjqyfKCdL6bdQf/sXFr1IS5Va56lJ9PITUhX4tYzf3qzire78my
jD4MO9gU77+EcVIMd7CDjg6FzyOTN4Rvt2ncUBY8by8noLJjlSe1i8xKIic6YyJZwM93OQ7UhK+A
00/GHjROAv5EWDLljCOvEF+SZDvl1ZTr7rxgHLNhDBENSxHiGl7ecQ6E8c5ZSqKQSXIDiWaPttOY
MPicd+nOyUXlN7ypz0n3Jx2jNuI2zk5QXq/Tt+pfebiO/+XL6DHx+B4VjZYtAr+Qr2v6Tu3ZujtH
LaHqAfff7WIf9ih9sKFpxxbMXAEHvBta3KojQFXbAWyRpMhVzU4yS6mxKsuH6p33gAR9CUllg3ZC
nmMvwv8XcwP8KjapEbJZJWbarZbAUsA/E8jeNp2PE/kpuUwCtQuLmD1KBvEhwLk7GVf5o9NVYaRc
dJ/SGPJzCC2v7BDBDg6vxxYzbE2Xc00ytd38Fkidg7Kh44R/9LDibIgN2O4VSJrcLvjfR8xdABFK
xNlB2wgowgqD0zcm+TCns35ZzbrIvbwFkOUmR7jHhQ3hSs6QcPetermGabb7/BxkTJrTsevEIZ48
RUFgnvbOlak+shs+/HmYXs5dOebNzhTUNIzIX5UKWWcdEV+Lk1wGyxu+aXN2IIUuK8zTRA4Frja2
dasM0dTFH7Re8/or1tBSGkbl8KwEJ11PWIhe9TRKZZVWCFPpzJStUxup+SrNu79qMJvSSj7DayfS
3THRDiKhtkb2GwTiuW+UiCu4SOZlAXrEpPRK64qE/wU10E0+5i4AMglqEKtlM9votE9fpDWHLSnM
HcbEwvx7fa0PE66zW8psNTT9wmDWpqc2QZ66O5rXwMJSTSvnmRA3acJuyXWBNu9Jn2tfAEtKL0DW
15fdm12cY05twIiSnKJhmgf75vrKPMd1cZ4P8qn2ZuDg3T+0NNxzfkQGX/NNditOc1CuMdG93/aN
tGdhIKWd++LNnZGF2DeQW/NWW5XR7OMKB2b+l3WYHjn5GxshJgE3iVMf5upoobNXUOLJhhtIJYNB
3p9ItPwWENE+Rmko0b0kBXLytQrlmzwGgAySZL8ZzO7jOpYfoTQbu/Tp/yHjvE+0FWXBd3Ue+vz6
U1lXuoWB+5kMGxd4u/CGPr1ScJzobQTGr9mGf04W7//E2a2lEO482N49DOnNIEp2BcNLbFIgTiCg
srAfSYOMJGashgtRppZiHSGFNm+hQQQBWKyCDgisV266aGXhjQk9qaa7S8llCAuEXrw+2kz5FFw0
qVvFAv0PLNkm8JI7uDOvQWcLyZaSgYPdxA92CtGyKEnDkdiqGtoSeUN3DS60YzdQF0v7INjYdsAx
Ex49kvRDfvoDeB2V5DF/oXEzMQ0jnrqywUEiQea/Le4ONkgtnGh1dAoGnbXHhtTL7cPr0zt3pjcX
sVvlJczsPYPVEeRTt41lU9dZ+XaiMsIN4r+P7srCSwA2MxyxCoDRDBsWfxbf8YixutnB6tcovY43
Ib6hQZPnlASB40FwOBqth5towC7HPWb//dgl07Klo7TY0QNowKAmp6vVMyIKNFwYZUtTLNlaQiqi
xn+C7KPEay2gZpx5O450+IDEmw0+oSBhzY0E3yZ6aHHV8MTHRHu0Sfe7moRczgXWNl6KykNYLKPF
IYgg/8gT8UHrNxreVD0cOqWrS45h0+ciCtpNdA6pY2GavxiJ1kVrVEJkeRopG7K6q5a6V7GxuFfk
T8IbiUV0NzA0T24+V6EAaTMzGvcb2hIixu9ZF4jd87jqiSB3hWJrZ62sxPFmzWALDcEGzJ5fLk+r
83Fi/IIXuRDvDbXkYKpvyuV8VPKieoFjXAKm8rl/D2UUwyqfD32HhGMTGle5qVH/DJ5ev+gFztS+
HlWxkXjX4bKHpdfoGlZ1oly5ELQu4cYlaIP8YG7m/+TRDGOo0PEAIOhR3X9vsHf/tH6WAFqeRFDf
Rly+9IIYqLCMJNpAQKEUItxSz22UHIbH02SFQgPHRkpxZcHtCsJ12hvPUJ+0vhUW5oZ5tSbg1VDG
di+Gow77gVEcuc/NGLttqBQ3sgrrvpUQ2g173ogxE+2HL/Sy1h7tJSyvVb9b8eJBV+VNf3eoC34Y
KZq70WR88NP4jSZdRDqK/YJx3SdWGUL5eAyd7tb07SwaCYQV532P18VFuR3Y9qgNOj5COA0ekLV/
iVX/uWKyeNUpBrqskyMRY2B1MTo58QPMmXDLEygF5JTG50whtIZY78/ChlYcAtIYirmtH7IB7lWT
/AwLJila3Aj8FN0eBplvN8AcFahZm89ojZ97/IZDX+chup4Rvv6yx/8D2B/LcRtwjddP8GoX68y4
ECUSjgwOo5Rg2AOzLg9zls7Fxb1K1SQZvZB3diMPilq9QqhrscBmJh77XUClkm3OBlMmJJGdeBw8
hu6QsDzdph6UtZPE19SpGMJO6IgHq26ec0vhxQu87UrImBiYtHZPsO8fIjNV+Klnd7yCgEMMgtht
portPPjUwauO5Ug7JI3KdIPzHS0rqv1rct7tah8GeHGYKgOA1TjBE2pmzr0r7dlFgIGejipmlBpx
30/qZkXhAeWFM1Pjbsze/OqYPata8xeI9BlYAJlUETy/Lvp5pM24sSKyz7QeYV0JzxRUj0sIWqR9
OEu5aFSADzbl76gxvJfEANrgxbraSPdeoOOQ2XsFjI/YfV5CoqlSV0OaQZILX1UgHAoY9IR7f9Mu
sjMERxWtAXrEQ4n1ov3cHv6CiQVnCuMVq+ggU2hc8lMnCTv63harRIHaQcZJbDNnVUjlCP/9TEPh
HRAsaBdKmTPfxVKuzcFLAIE5Vma1p2+2qh4HYROFq1ZzHrKQcEQKrOVcQv9sV/Lya0MdB8fauI/r
244VL/4dkPc0OHAHTUQN24t4VpWpu7xys8I8uG8q/e8fag607W0gdl9ThHVNkjRMQGQ+TqdR1CUw
1MP1y2MTXAJ3AqV+XQv7sei+wE1bHcoQTS4BeoX4/Uq/3FDDCYSOFkIyqwbw62iE4IvHTo+0f+rG
1OOTrhDp8GjP8ontwkW93cRBWMORq5Smb7d/f2zQk1zKEiVFj6VvsUYyweSUqFxyRLbEwk2qHHPR
Xr29sxFqb7hqlkT1YcR9IA2OvJRS2VZkWwNPK2OQTREUtXZP7g49rgIUHpF8TxxL6M/Cmo2ONSfl
2fXntfOs8glyefgGOGJTStQGtW4ohachfiaZ9Zt7EvZK99dh9B6MGTQ8oNrJ361D9nDxTmub+xQW
so3GrudSLYvW+LGz6QFye2mn6nd1dODk1kaBbw0hEKnUQQusmKVVWrheuuGKERc8JVa7ujBIt10q
L9S/NuIKXKaGKtCj3NqZBRJ/tg5fyOs6b08Rj7rVfowPv0u5IigHGhMf2VagFf7g7i7q0Fbz3f6q
ci6EDuKXgOjy6aFclHvFYXKTtdgqTgsvtTi0K/i6AYO+E0eWaO70rAYEFTTBEiTCqd7isUkxj1+e
Wn4d34Qagb7RtlQmt32daz0Eriheg5HjDPxuLoSKfKjo1/hS5VhXlXwDpDCK8Yoo1F0/tjGETqGv
MA8qvgZZ682WMDdHd/ZNzhsgmAqZ4T0ji7oVZXNs2+eid26sYDpwVDNlKjcKoog4P6r6B2avVAYR
cMET6xSenbnz/co7bCQbvvEt6mlhWK3WkqxlTJHgQamsyuUh04ociAe8cM+zJKZQEyXcCYppCYAu
bgWltRlmAI2JoMIgw97EpEgyHd6OeGaz/kAd917xiDE1v10JfMznoFmqyslsBbed7/P3nPLXM1UX
cK8FSmT8bjRqTuK3qbrGKw6QC4dtxQLdXPucbWnhKM1zaMoDUu0adqC8r7Hsh2y9n+2Vb4DoftT1
fDUnXTGrXa+t9qbS04BnUjAckdaVylYw7Tfvv4Td3by3wqoiGq+s/jbX9Kq0AtwWG7XLWHuyex74
GsHH54GiS8v5x/t1W8cWdhZ5nRAnyxQCvQZld6BPfMygM480bhnsNYn8El4PyI57BRnNCVzqFWIw
461cMJn8YQ0j/2jxhP74Su9xBy4MlMLQVYEV/2SlTdtpqdolb+17t9JA10W3z1/4Hc8AdpOWKAxy
pdHZGtIDcgReJ27CR4iJ70snw5uV+kAGrcwNHGTtJ98BU/lShAmjjNYn49rLRINnblQRq5p4qQpZ
qpuUhU4p0v/YXaWl52YIN9VjFTxmne+XhFvsguJBfsMCSsUnMPPXQVQC4JDY5trgCKJpPNZHRg4E
7rfobkN+ceSRdCUYfAJRBaK5ud67f6419t2OkKlsFAqvS77fHQeHTfyX5iS/qFo67BwxYW/gibnJ
6jWekGL6E/pOGbzHwDObnI27u23EYP0IpEi6j84ZsCyTAeZsPqKVHgXtPrU/l10IKMOAyH/vESC/
OPtAm3+WuUoVlggi0mc2BqDz41sM7QfK1fQRmjGaZ7K/dbiN/zkmaJimk6R3jODBUUQn57XpmPVH
X3qJBmBMCfgArnthzq4MlM/CTmORBjUTEG2wTb7DHWDAKpOUEXRvOPkQLSgCBddy/QYnI2gj36fd
PncLIraN0VNJV9bbfwSBmlFbULyNr/AjZwRhTsTnubibwU0C1Z/quCospopKhBm5ufvxRaC6TpTs
nL9Dz5AcomwYmnzGgRQg6yMTTSovdli5z5M7SpnnvkNwq2we6+Cjx5q24OJyVcQw+7DTtE6VgsE9
inYt3z+J2B7bnx/1FnUxI1M56ohpQ2xUBCs386LhdNO/S8JvFxbGWf93MYnJdM5n2bQJ7MKj8FAt
p6tnFJx3OWxnMxb3tW1TnwmT48rLSVmGFOUR8X+h9IodlgummO6wcreyj4G0mVEGaD5semyVGtIN
qSxZRGwD9vxVwCRLE8KdiNwPBzmJsfIExvoKqJyCfo1DsD0agg+pa+0TvaIDHr9NbZTsXbZosrFQ
YkFO5uxd7ugM80+UNMA3eLDyIkmDz6BcvWwsq7iPT94CQLT4y4CHVfm/vkIepVIKtjIf4QiN4/74
adWhlRe9Cd8o/wP7XKaplxANTsmbMCFimvCkJbiQiWZr8t+v+Mh1SncRchzvoZBxhaMz90sFrRo8
ssnbmhDBLiC7hsMpzt+RxcX+0zZdzMM9lbK+IQ7Vh0LxfPXrMjbmAok03NEJRdjMqEluStmMuTc9
2dyrPy4EhnycaozlrG+UjIX0VnX8A3w0r+rEpaVBNptN6m3xfuozoMgbpfCtXmlPKWY8ZX8JYVai
D4dDRx6apd0emdobn/8+rnhzs5wQVcljfaGBeSqC0rUDvF0Iu4BSfj8OZrwV0UNu4mvg6Ado9idN
5d4NB6AB6z0RuQn+bNkJ2wCAMrWqwCuaYhW7OweOhq5GIRcH3ErmtoEtgK2T9dOG3BVXEKilUItp
ZAXA55Pp0GQel+tEkBQ84CYUG2RTfhkZ6p9BkmSJ3sKpLTFGsHsXi6wXkTtJzuHuT25LjIHq+Fsh
PYAb/LMwVq5SYBFXLopt+2vUOTQMIaIRAJUehJjoxykIKJ80HtjwgZZSYDmH9csPobZvV5gGxGa4
rG+g5KcQxcrZaAS7NddyZ3qHHsl+iyGsVbGGlHBP3/xIyxL8YX54Ae2fAravTEK4LhNV3wRYO8DD
R6M5XB5WWrDWcj+7TMypCpKO6D1mKBdU2xwFzthAhN6NZ6f7D21dSMb/gJ9cwnEl17BNrAPq5tl/
ekp2tWwIcrwQ011GRAuutbXVAD/6x+6rp695WKKy4YvFz2hCJxl/esZM67Kf5YwNwiF/rcBLpNGy
5WeRVHbHkzdQSqcXOU/B++ocdxIyDdJX0+ZYE2QJzNlR4FIlg7epN4Huk8wQtfc/z20CakQDYDXF
V8o2CdMrhF4rcj6Ai7Jf+LuZbz8e7rp7NN1s6LHyB0RYQ4N69ekeqXp+GOS+jPyHiXplyc+S7CRK
GRNXprfnjrD6qD5rsdxB4Gb7v81Pan+mGdkPOcXFNjsrzWaA+QZ0X1DqqeH6276ITLs8KByFaqsm
yjsG3nyJYOvcDVjskcfJOgP8gJfGzLADC8DKHhAhwcPsis7/IQrPDBmD8scTZwuts96tjlluzINL
D6uWROXuxidr4Hll/QLjtLpiCZoRxS9qb67FtlfMpgcrnJgke6EeCK/lJMaZVIxOLX0CnRy9NwP7
Eo6KAxJ4RhMda5sy9udS4ZsSICvnuiPr5xnze9dhIZ2wJOuEvxupe3h29wASFx50uecRn3/lCFok
20pk1MNm0iWpQUNljwrfQTv7ZyXBWzHPZXBwbJbq1ITx89xZ1zd7kQAmwBzvIkBQNFJXly/00uSW
qyJVTYkxOkww3S1WAakblqFCA/l96m5DAZX1t/uD/4dUCThq4Jr4H+PQnQZNqGWAU71s1NyflbwA
26kjFQn+9vFQzJY9/bz9acRzb3sYGriKZtYDzzDMlwi3lUwtCLou/AzzU/2pWObrzCbRLkucvfmM
bO6K6nziaRhREwizJN1ceI3dApsBPkCKweT3TAC2OXT5p39K7mP51Y5yZR3JEEzePzBt5JAGmqYa
88Hz8Ofxvog8H7olRaEf8kZ4FbxT+u7C7c7IbTfq7OjuHzV87g0hVVw1yzBqgmfgEY4fH4WN8geM
YZ0mVfQ3JuwLq1CzInIQvQJyWaXj/EISlaNVj9z9Wx5CZzty4JCvdzbXN5DrHhuZ0L/EBx3oKVPW
hX4pTe5xYDPPNOGgoLKHHcgevJdvX8pUlG5lMg/o0KmHai0Kk/WagPHm7hTygs4GTSCErCssktLx
TnIYeOz+wHm3ZPYb0eJUwsFStJk1VvRKnrseZxgsSaSqkPCnG19ePR6ZGoq66qbC47xSH1NNtAGE
WCNenhgPEeBjgp1AUKY5hhm25TglxoQzpWmuXtA21RBmfOPre1J/PVCr1f72YFduqCYZbIY3oufK
U/cLgyVkuSTd1mtvuglemjLDDvb4X1RH2iPQwFDno96FmMlRRGkZRSZNNjKUgM6qN4iiUMiKsIvT
FoVXek9c0dFdTIa+W6L+BnCxDf2fFB7QSqMWBUyt2zKhG6ABPDPgzcuyRNl3OwYguaCVlydMrtFx
/k2kqVdPdhyBqY3RMUsIGKU1KoHf/SeiBMn79t285bOEMDSSKmox6f7sXxpmtAnXlWaOt+OsJgIP
2vC4koY5PbwqHB12fCVVQqVsudqcfHZltc0IQuoHOnnn84rJI1qdl/DYlBy1U/CUrGV6SHh2xY5m
YBRbevVvwmkBF71mxlroKVdW6/FUqZE45EnycDqn6jddnnhPvIOJ7EayyNwguEx+VG22yjWGdorg
+Z4vzrehVVjr0yo8vXGHWnDfieSlVyXnsDnQ400h0CkUcGPLYRKTUjF/uBrgvcguJV95K+cyVQE5
ohCf2XwRZeJ0kltajTrkRtxZtfoMx2Ir2KUOC5IjXuTsYEOd9eqzEbXhV2ydzsaNh3en4/T/9Ye6
ATxbhLwEIQX/njZ3ms65itl6zweKxCBi9C+P7lav9Vl/Dnj5cJsqGfI/uVR//JE57vdi/sZUrrWy
rTIK15FxLsyBIFqbiZmz16Qttx8+/d8kNehLmfjPQ9ZbhLVDL9JKMBI+KMrwcU9Yhm1O7UdStL2C
VHd+b8GVu13FaV4Pxvpa1HG7SbrWs2APJN442WMKiGj9zwqiiidtetQ7i1St582OaFaY5rb1sVNK
W9PneCqM0oyI1AFDUfm1jjhB2KN81p7RBH1AhqS38z0e6DNsgBLbmeJsQVS5shojuI8q9t34dkGv
aNhe8LUEVHDvoZ5Qw4dRSuq6DZ7eAph0qLVxgqNDmZNv6HbYICVqI/lRxhFIBJVOzmi9A5vEfKly
4hkplN5+hXza3z5ju3vRTPQXDj65TlQKAVh8iMh66oXNGS/0HLxBVT0GTgQRFeH8NKPDIWt3+DIv
XxUYEKaBeKuuQ/9jQ39Sgg3+lWIi+J8SIgoJ5Xpfe2oehxziQhlclrJtnRO/xGwohEhiyb17vBbd
OlnmBIms22/BZjBdqPS52cxvZ5eRXiWsY8KqtWDkewnm7f/MmfeB0sm3zdkQwUtgcxTmKfHVUdFi
AD27SWivmi6ZRsxhLntMDvmn+m7QlcslDySYp4uXAsnuKm9HK5Fabcxrq80/3N/BHMwJtA6hLcKh
0ASKSYr6CEHFw34ZN7d8a0ULRhDCNjzjNU8QXgnehNM59v9fu+yAIDIA7ZO+BZkABjyl3poTnwkS
JOgy7Xl01YCYHaBg3vgqBxMS8LtJMQM+cah67s5j2roU9bFOBkgDjm0oOXztfWqCstZyv48gQ+X4
f7I5BkskbpUUc/R8MRjCKCYOv7s55sx8gPHZmyRz7AS5euZ3oQ/7m2ZTsNWwQdUz3CH7iqD0pLdQ
JxTyUeA4o+EZ3hX36vy/sWrs2zHX4w5NZJ/Y7IZND5r0DDRax9wjJFADFtAj0BvUDRx0yLeEHbo7
jtQ7q5AJZntiBGI2kUXHUEM880kYzxOa38NBXfXNzPKAcJXR+7INAZhMqBCUBD+u2eXDb15td3y9
bOGvJ964cDS4wF2PYSoR3qP7iSwBpodQqd9hIyLdfA1o3aTY2h6F5gq05vwDh3ax5rs5GHMJUP37
7r0vjp/O3RS3L9hWatn7ndm7mW3cDiG8elNrQdg/+w/AkSiAPgGiqkpX4FNYWgjU009CLyEvkW01
Hvg2PxRg0CSVA9GxZutvZzeGO5poedV4lwkGd9zCyZckWr+yXufk9xqI72+NOznCwX15cTHIi/NN
pAi+M9L2ikoMACBsUmJFXgigyXKABd9lJVx08kqFEd9JYfidB1q4mYDc2LFXX5uit1Be2CPSS4FQ
4jMHg0hUy50uu7LEUGf2Kte72wJkJPogsAEZrLTPn21+516iSmlFJ1Cf1gEKJ2jWNfujT3hKq4p9
CDsJ3KeLeles0e7NZY3pVJno5W6OnexvaEZ+Q3kxLS3mnkFgoENhP2w08DQeux6ee6Tc88bVJMjU
bX1L58ixMV7WKcjTYT3wpTpt42s3lQhH9sLgqPj3sIs6BDiJhdlCHU9DXwTzqEuEIygQl/mf9pY3
5c8eZk1EllKo3E40V4PsEOKkMdGmT34aZDuD5eICUnF5WTfjSwBJG2qxdJ0T44ZiWjuBK56LfYCK
vQE8SBAoRVc67ARsaKkhbNCxWKy2sBa5o6jTnnzp9T65LhnMBMzwDbZiILkfNdSHB6WDfjYxDTCq
pavXtmCcqCXPc10Xe8W7+p1ZaGcXx79Ga+sVJEola9XQK1SaYv8d1czJyCFGo4nyVZcbrMKCD6qf
9ecwn7r0FvVWutqt+Tz3W0PJPVomO6jA9huwAtLckoIvDtM2f5X1SEDi7SMLbXnOuh9Eb+4X/GSU
7yq4opgJFqYCi+WDg+IfWSIpQo4fGQuBvKl0n8dcP6huTIyG725qqP2QX54N09sL/SzyaJ8KEvDN
ji1uedB3A1v+JLkpmP1A5E91/ezZwfT6tf63j9FXjpuVg5qnBjt2xqjJakCyzAvA/KOlJO8ZwhnC
mF2Io96fCv/LjWXSWX64fdQjA/srVwe4fdbo4Wtwi/Cv1C+QbZg1jrtm8LcDO4rudE84JlA9nLTa
um2QYi+1pWKUi5Ed5uBuwi4m/5feoH/Rs7DqEqc9tr5AyGqw8r7JPp68kbORjgmSxZegsQar/0r5
+Fnpry0JmRv3/KMeUFcyMdQUAh5MVV7g6tDm58C2PKjqGqRxWC8MvhUCOJuqGxFuZ+nD8He2GwVM
jWYZNXdAEYewGHzV5/EOjGG5aaEnY1CDd6151B1p5hHAeQ3ZJirx9dEU+jC/YhlpvvBJQRcUljOf
IsE/kyNPhN2esDw31iKuoUsPla5DFYZxA4fpqhQqF4xvXNq36tYsx67fedqPiHDCv5nalltyjLvC
FHVG331teG/LFqUwLR+4jV9col4dr/63S03lZM2zTGnB1nqc9HkRxSZELPmeYN+H4uMDaXXoAglQ
rzk2mpZp9ZXL1lMnNNs6QNA3UknDsKouvQjOeYfYggCWQbRE0FgrMGIBGFKPyCtiPXCKReXvd0bl
e27wRL4/4CXqZlwHaJ8RsFMoQhkBdiiMXnnEnVfO9qITl+ACQioz/xx74Qo9P4DsLcGC2Vz2W1wN
s9tLnhRxgcUs3Ix9IWO4lfeAZMyreLvf/SFioqviiR2h8vz0oA8zwyCrUmev5kBdYAQxYKJoNowX
W4oNRql6t5zHG2LJF16PAxydapZFAjoRPUsTPJ2OoIg3ASIi4a/jQbNuydh7fnlDpikynOLj+GhD
RTBGHeP6Du7fK1PjHRNOxh+rqPwcDCOLI+nK0neUeXKDfHAHwY4yatv3PLMhGJb29YNBdjimIHJr
dg91D1g/XW79kiNtXmSRUItVRRh0Ur01rGN7vTdnePkkc5bjG2pGRgm3tYx7HJSi2h307USbqsDW
o4rX/ure+4A5W/almTzDFWyCpzXaktHCJws/Nwyio9K2rBqCKl+YR3H6tzP3JOP7uzBHqB885KZt
SWpchGnSb3evO+84ke786axnWgMx9LYn2B+1ri86tiO2YA6InVFV0Hv7ZUoCpo/RjEVOyAFCG/DS
754be9AZ0cwTsvf5o9FaU3wqEtDs8XAmjYWsVIX/IzZ9fRkcJQf8Fy/J4bUDJ74EXTr9GynfA6lY
e5rhDa8zH6XP/p2AUS1NmrPafOoKHr5cvX+hRFdVbrX0d9DWspJJmOYX8yEHocBx3tLt85eyvCGh
aO4P9kdX0unXJ+AjGfRkr/2qRIrS4yJdEvpd6Hj8Rx1aIxvX4KcXQVkzdV5HhsWbUrml2VVcgDqJ
UWg/Le8//y5DSUsB5DzcOhx5CRpKH5+CUHF3imq1pOKhC343AKyc0ZMuJuuFUO1z85kIkGqF0brM
mdvNmIKhM8G0lDyqh4Rf/dAFx2zH2nkl+ii84Jy+hFkfLcSoHAuKamPk40iyfAPBSEyY2j2a6dFy
qhY5XjmDAezYp4zb7Ww/5jA5rA3AFE3x6m8xC0xBQxhzTq2bafsuN5tWrBZm59EFNjo7nLU4jIa+
ulxCt4gQeqcC0YrAVFUgdpZcHu3cKiTMRXLY9iiYRdrQPO0+/PgU2pg7LqllpWS9Xbd9xs/YWzvi
ljA/RVvoDhAY3AATveMfZQ56wS+lKA4xKB7y3DsMKM9vAtXPkFHLFFFTCtL/jNrwM2FFEuKhqoK4
sSYJjA2f4hroeM5jUV4mm4oCEozRSUaRh41ZAFok3cpL7FSb2RqJdgRfS2u7mAkR4wgP/wUv9/X2
We2KkFLtUE+2UOh4dr+Dj2LBsS8WXXHyKoO5USXyQelHvLSg+EoHNNt/xddNdmyDjCiE4mdXgeb4
QMf89uoOCJEbqG1y8KfEBPtONJ1xLjQygWwDaf5GvveI0bXV+N4lvkNFk6HpVjg7ESMx4DGY2rjm
czCUG5bSFmuXS8gtqfh1Pl63s4SZ5p/fp7/8nF1+IvDGHnHKDpwPz3mpSSdcGb9RX645kX0rBs9V
GUPcyquy5rYKVy8LgOpG+XVPQbdtCASd4y6nJoX3/FfwJ37YKA6Jsh6f7kMtFrr7rcv6vT3HdQoG
yTPKJsTl58Jx73bS4e1XEbAmFs3i6IM1jRsrgaxjMh2ToriAoKuQ3/tsmA/p9d6zxiUr8wirSj68
DOxsaO97PuPSEzWSh5oVO/3GNCuZdiu5HUtZ45YO3a1iB1gRS3pBpXHHicn3zdJ8j2L+zxcrGwx4
iwp+PA8Rw+/VIGR5Alo+qBFjZwl+KL2ONhkZ3XETC3W/To22o4vqKl0U+Yow84qZcmL8vIwGYU5m
9i2h/VndhoI6nYmJc1/3L2gw0/hkRdj0GmgJ0INRMBC87AgBN1ECzQ34VcQwWMo3rE5bzvTYYguX
o8ozfFUc5I6ydOzQFtFYnxEtcCuQ8nVnc1j81Xg0S31nLZD8zFebqwUHue4hqeuJJauJsM3Y3q5o
oLwAmxl/rSdEovXBnE6HDK3Ro6blfe5N4AodcV9OdOJ/p/DFd29vQ7j3ocWL7w4R/qpBUiIkwXks
RrihpWLCBMXLAzVMmSDImmLwbvuDzMjC7DySQeIhXQ1Oi2DKJHRzH0Zy4Xw9mUb9Yh91+/nt0F13
I5dUEyeUpgm8EmlRsIvNc2HL6kAnGp2pHCS6ZUITI6SbmEyBgz88z2dK0/y3vs3fj1pqovz2le3a
XfxDjIa2QixfuF6+EdjrN651uX5xUEZzsJ/vFvzakv+B4NjVcWN3kA80TEstCLAu/ai815eLFoXq
HuB+l4r2+ZHLQp3xVx1NZBLaBnVS03mch3+CVeBdNqmuhBSUyZtkVHN0C9tOqKznv2dxUEIGght/
tUNPuIBcEP9x9puJZzELUvEtGDEoHo2wi5hjnqV2jbnneb7aML9eRrrUQ6/pPzjQdJINKoc/V+pu
CSD2PrYMh9rgFyA0AqWR520gxV+cGXT3lmRZtWwyY4A+9+5vwl9G4SSJG6sx1jFAZwpi2FMBsPiS
Jhp8NP8GQRQa6k8FgXKbFnSCHkGkRoE1u6jsNPnbfMLr0vJaQZ6Jr7BypmSmPwPZYJm0eckafdN4
CU1qkbzh/4YDf/ad2iRIpaFUKpvu+POyejDJQEOZ3rYvc0XMN4fhCNI43JoNo2h1NQEmLpBVQpp2
jyKCrtKmOpQdJ+T9hSqCY+LoNdAO6kCPeyx4doXbCZh+OPbEBE9WWxgne5UeBXFku5y0JdhtwFtV
qrJEbx2yi/e6HR7koCXbtb52ioHkA/+TxhbnMUJHParpu5F3hKBH17zUUAbHOyCTWo681VI7xzYm
9d7wyO/kAcDs2OXgRzmdZc/3x49scXLcUOnSds0y6oIKUWtswKzmyVtJZ3vkE/baYRxBE2cQbR4S
dhyi6hvIeMFLKOsMYwDbq/Z1dxk1EJQofadJqUWjASfevv8eS1N9p2/HFGs+gNVyHUWMu9Omez5m
bbeNK3vlCWrdRnjizU4ow2gSzLtI/UaawWwmEQpSyq2whNyw2aOcFxobojHJvIADIlWQzLsC/1XK
1vxJ+av/kgHuEVVZxIcGMQurfj/EqVlZMQiFNOvcgWfZMex7FyMYIUAddRs+Ejz29lcLRIO1EI9K
TclfkwJwZn6YBOyyKKkQqtsf8j1TYofAHAvy6ilslvSQEuVc4U8NI3HP368+HcQmPPMXIH651ALh
yIAm+dIZaHNj6rfV/XrkiZ2qCr6qRG7cIVTbgCjZQ0hwkxSq/xdSCzJrsj4Zv3LTi0Hc/KJKvRws
/hiRkC7oR2ZZDLLKAri3lm2XAwFj2k8JMMiC5W5jO5rBoYTwwQkNkPpXrAl81gxmfCLfqs6rIWUp
msBcQepeTONbRWx5DA2/e+xzxxvQZP2BwFeRfivBRG/sj8Cf5Xi55Q3M8OoYtpFcNHL1YZJk6wA7
UFZ0j2v8VSkIxQK0nfpyXzxi5eFONQMlGfwudkVjsrK4d67lahW+hFdzooPhm55mXrTbAAiSiEDa
HZifzVMTz5a2m9Xc7YqmF5Tjvw74w6iwbATC50WfjFYjeUsJG04K/0yIbbplHgdJOvsILMoiPQsw
s+i6Ui5KYN+tC31tF9cOmYQUJDnEIQOd2Wow2tWUqcpC5LYi1gthGY5CtR2j1pBU3ws7BY9O9LCq
WfpToZWPkrTGTyJDYzrU7pHFyCycOOBrm7IykpFtA/yRMbb3Jh5l+pHa+fs9rIUzSUrUVf8P6tFf
f5x9jGU0mwvbZ0xW79oj6h/suW4VrMkOJq3a9O9Fb2q8AU3FVFcjcrV2kTbgLhJLBPOZ1sCtcrk4
8cZIAm/jsIRdlAbbHuLhpn8ou+fr6xUfqiAZANzSD5zCr75GTiKxtCCMOI5+tXZxdbgns7nl36cF
WRxxTMQaJ1a1RkAGtb9a2W89e2kZDddSQYOxhCJTAE+jyxk3ca50LJSIXyXyV1iZQEevBVr7kR1g
rq5mHB/YF00ziDmDJ/VJiD1knEThKaCG86ghoyOtA9HBHjM1BUuzaWvGeetExmSHlBOWec8l1WBq
zk3EhcFqEOwjwuDhY0+aWhqIPWcQs918FKudYx6Ua5w+1scvQ8cgHdk3C3bGILjfgMRXP6BUsQ8C
jJEXe/FsyDy4+YGxR67fHeGK4CVd6YUnMRvFflCzFFeK07Ia8Q4suaenO0HoFRCuktvbbGnFktKe
N96a8O6XzOiJfnJ6kYCXebpONG9oBWvhlXnVRx/0wYRHcm4Dob3ldWIcE6P0hxbjaEqZIKXzIyWP
lLwEKPebBUTCiKZDO45at/o1UAgswGU0+ePTR5tJmJJCCD7D+MNv9+oUI2/56dLbW2Rvaag9K/HM
QAjpo0y0mtYGNUPndGGH0EUZiCLgPrJKUqk4+nLM0DQlaJi7nH0ja70qPePRFzC+EV8arN/9NF/u
jeMLRbhqveh+xLMnO9Q6JVzHGGMno7bvwmRbv2zRbMXrbHzg+YzUqwMMCEFpoW5lH5GLUBzFvaIg
/HoZvob11vSusOqdm8ijMwTUJeKrSKXS/9ovuJJCUL0Y0Z3xfadkGbHeXA8dyRT5Ny3LcNK/piE3
2vwT+4WrlveL2RtkwvVnKZnBXB7i8O13di30y1FvD3pAvbT31UI8R+DSf7naI5nX6eExEtsE+eD0
Tw1WI6C7vVSYhuLMf+ztS8sLJBxXo+XfftnPdmnMLS+PZK6iVsRpAW6ctWuZ4jjfcMtCiB22+t2k
XycLoLeamjhtMFBjacPhAqZy5iPc80yHT3JoH7dNn4DzGkW7dMMGeqUDOqmnlDl/R2DhIwWz1uLj
ZQVG+bmNQ0akkVT4G0D0iwvIPGpBjeW51tLkADh388vDwj5FXRDlTL7/4qI+iX5OfGNBLuXJai5/
0EpGctMcxhLs9OeRLiT1NpU6oxccmTuDn78sPUMXnLuKRhG7WK9XrV9GM4X+XMLhUuNxVBU0P+Dh
E7WSZDvMUecwwP9ScprsaoMhEPVXgJtykmiF6xeYUx8LpmGpLxWBcne6rMMIJPmr0c9HCNyfJHEa
tVeeoHSa8DT3vs1TEw8TsUud0ve6khcWn/fCRQ9KhmJqFvY4QQxPEkoUPe9x7hBhnaofNDZQSZye
Z0qnMwm8ZEe0iGxLq6SbL64Tj5+o2xMRttADRUJQPClmL++h/hwnCD3EG7G7hQYWI6BSZfJM/7zU
6963dcS9MWajZgmapVqAjicSKm6ImirneTX4fWXFGw0P5UUgD+DXSBT8E8Xha2/rT5SOBJKl+Ifg
BGz2UWVrekhA0ObyXH1bzL8etMFTqIzzcIIOE1RVmk6nfP4gjvEL7DlQEMTgBoFtjsk5NZRtzc+V
lM1JQg20rJ9CeXuGA7OcWvP6d8RYyF/zqL2lv9nasA3OoZ3pSKRCKQRFAvxC+hznjpO1HLfwwmJ1
xc9j0y4MLa6cwhK4iVlptYX3x9E7j3RTzF+b9wvkMhW78G1WtG6sYs759lstI5s53zb85M/cdEaS
JEI5Ds7Kmt+ErsyfjqgaIRgAOhs/uWzSaLPZTh1TRWGPdQagdZ/aetKdJKMYqghXX+/X+pkUNLxX
QHS5d6zadEa4jbSudecPrnsHi37ZSugqYZfE3tYiCpGLZahoLXLi4GQjxjgmfnoGXb85QBSP3T6M
VQsynyaKCP9m9o1eaPGmnBuuneP7xxlAEYCoDH1KYls2tZVZRMrmamrEGhYKbARaqbXnR2Mn6SkN
o9tuJrTPGCHXH2vwG9n4cRaGJmvDAbzjt5NJR7hRQqD/K9GBPV9uo8/WFF1nsWrlgfv0nHylGyBJ
JO/HpHUTv2feb0aPo8cvKju2Qqytbwyb9tzji2EHOGLKxJOps4HuJmg5SJ0z2cTUYj5zE+5wsEV2
xdw1Jrsj8hUsKPSva/cLXEc5x6frIN69vJcfyngCzArfSMAVXTupZW4kno0+231XJTN8IiCS4F/d
ssPTXq7okh4rvaQbOITg7LICLV3M53rzUCUvwRUc/0bzPgv7YW2MHqYx45SAUR8b8ujZpj5Flsy+
Dv6nj7EUhNnlwUqluKMSiycrZvB6OpxgZu8/JGDVcwSXYiCMEKh7ZSTBQcJ09QBOHdVoNYByq2qT
qIu9tZfHGwOUXalCIdyqWItsRWDYvZGydU8Q/21zFC1krm5DDGxyvcC8u/uNHuVzY4juCRluJuPX
CSFykDC4wsmgRSiR+iGDxBdTyBiyhnUKZJdbpFmv+c6QLAt9KlsU0T0U+Cmf/v//artjVa4zYIzK
hJeBZQL9jIKBGmoRqLGuGH/1Dnoq55zN6+PIrgcLp+0kvlb4DSMZFK1FyAIYoetcudr9dL6AVFfb
NzHk1Md4EUNVky/VD/9xt+dSjN98RLT6LIwr9h6WFVvzEQqd45NA1AwPnCRSSA/zvCtbT/k/E5xe
5wz+VJzRhKF8TOxpzjIaX2mFLUH/tBbPYXTeKpciCmEIYJVr1EJbkanUU8Yw3ctMdZ+r4FtnNexY
Ur7soI3vAAqbhgPGhrDtZBgWZY93PYj277PwmLHsaQiqa9VYxtCsinUx4R/gT5j3O/Cf3ccwKDR7
y2efK+uj7iik07mc5eeaLSs6uWBnqsLNlJ4LLRIj1khbr2nOT9tvcQLflm77SMHycikJ2cl/SSUA
uRqZpraAbbuRs/5m/AHSEtcORZqNoJeHxlTzEjJlRjLYseiGcAs/Eyf7t99IU5TCj6GmkwrlRmyo
KXjqs7RIxN9UtDHyW8M1tmyPwdiXb5rYoeKJMQZPTGX/Ew6uDCo8nJVcJNx2syZBQ2gQDoYxctuO
zhNC1jpogCxz9dmrvKzlR8JYoZY2xwtlsGUa0rSE1YKhPZtaiNUUvcLj46R4C8d+WjcbNISfIUCZ
WxtelbhgpRrvpsIPDewJRXSmeMWON543UHdl7pwiDpVbuxV5cuvcMgaOxuYw1t4/AEi8uNbjfgRA
qq1LU8lJPMqaAFeb2oHDiC+L3Bzvx0KLCPfInXMqOUtiSJjBoFP14eJQqJnuBh3yX2Nqo2nXCPH7
ccglzMqpnn05TCcTX+4fWRWaQACID8az06DEDiAtWTjCxjhzr5Sf8sk9jGH7wYCy0N/UoIDHeU3F
yb4nSZiHhJnMX4n/1jiW5gRFrG4VQCjd9C3DX5MA7FtfT4zkjUK2VWZsJj+Umsf22mByoWYwhG1v
DHXOgoFvFf+5L/oKR3rG+o6I/eovJgTRQFmFaCw3bCbPpCHzh5Lp7/d+AVnfaDujq9PpZ5RUf0Gt
lOEZFvYe49I0ENumeU6mymt4e4CLbIcKvwEkTExRHOBMBseMvLhchORgM7dO8KIaXFmqpxyVSggl
AcOSQW6SI74qgs+7+xGzgIFV9ujMVd7C+qcBzHuLuCgvFVTBoTXaiFJ9zWLZjpzQAvLIfeDbBuVt
CYYVZMMfxVx5hl+wtTDSWT/TIh+rl6SvGMLOsBAynySsIdoJf0agI3uKs1pi+smQiVc5Sok25jMA
EUZiLu/HpQps0TlUTRk7LdHzOWK/bEbN6LzZMi8brh9ftjfUPIRlBPNjBWGdLZ+gzo7O5v6jdU26
vV1gnUfHHeJUr4eDbfDHJlffOMKYYbOtjz7wlujhVC4Bk27yW0dcuf/xQifZ9ZnDLQtwc1kQbFDz
jlc5zE4EpqaWN7spvkDfsJ017AeddP0jqa+tbX7ccTPhi5FFxiGfQFNdOFmjPULfrpuZmpGYVF/m
JlBTyZH/Jc4FnFex6V4EmgS+yV1DitqC4hLo2+NIwszx2qShCeF471MtHr8Zm1pLqVlgtFG2qYBl
+dsBhRtMp1aIiJrmB33fmY9guw1vOx4vbvJTDM6Sgm9VLMZ1oCjAnFR6eVRgIaVjUd35HHnbmjVe
KvGdq326up2qT9epGzhccyKsKdrX2ooD29W8IT+UsS/TpGd7bie+sk7pBIKeqocLi+BWoeRcvQ5Y
pC7CRN4F90HoXud7OLC8etZKR5uIa31wdYD1CIwsg+fmodVR2v45jI4XDPQNPeuhbDU7Tx61n6xW
F1Oot3MGiZPFzS3qCVaZIQpaJn/Jz7hTf+7TEUOsPM4qS0t7Y7rCtSlOUULEBbEzEFAp6YTf+LAP
pOIkxagvKnpMBlYwNPQRYI1j/9zKkeLSdtUWKRsJWsBZoZt1+GQDiefEI0Fk9j5lDjgImiHEZA+T
a/AW8wG0Zmj3K01TR6nevb+jAxyWbYXlxDx4bmi8Qfj/6vaCXIv+XWy/tfHz5NO56VZbc9C/TUwu
f6BeaH+Zhi4Qg44FAVq8FZVz95o6Gas4c2zIS9ucOpaOCZHCR8PNIJm6OlW0dQzuwu06TgfWesQB
SdIdw8dJnG4hGUCKae8HMg/PKU3QZt03bOFj3o/ba2asXpntKITH/cR9SlqmXgoMpJD6gt4J6zwY
VehXYSqKNcxhHkaOetpIStdtU8sEDmtIs5kE7WQSW6FMGCa9LMWufIhc0WxKGFD1jm6FSvQfPg5L
JEz5NWLflwnLSXbB1SMGoXnyjt8cokn7XW2gp/Km3c6+XjKzB0k+AzMMy9xUpbIWgcCDELiSkSOn
uTdp4lixwAHyvNqK28AY5rpf/vJWOTBJrzgMlssdNfjdZbDLe/d2umruflgxUBF9MYTGl/Il/Ot3
BXJUByVgwBEcohFDyNCaBnWaYc/UH0lDoSyUDswHkKfkhT8/LDaSRLzSt9lMUs3NxoT/AWutMlu3
1QZvfqanFBG8rz3TSrsB+SDO5yGD5T07N+m2nB7ZB/mGBF9roBuaBjC+dW/EEtcQZRABNeOEascu
RwyU3CHfwoOe9YwjHhCRY9glHY8ZoPj8OxTAnmiFVgMYIi4BjyD/TpRmeSN+8xHS8w99DKgr+FN7
PNv+ZhqPZca8bV8ip+NWb1gnK4yLcqhJfLRzsuCLT7WjkuepzOwTPNQ1W3ct6eBxbpvmeDlKi+Bh
LrmOdc9nW+5li8hNYuA1kPGWed3uS7ueuYfzH+ks1aq5IWchRCr+muZU09hW/AaDCyzsGhUjVQdG
cwgPkK1/+G0pFTj/CwKnfrQI8bYyuwDP8mThE0pSjnPmUbyfVlBU6srESeh/6Qe81FmfDjqDjSQn
ZSX1vjqf7PzYBzrVSXKSZD1ZtJSKU5ysQdUKm8BPq+23bs8bwbIdgOMTtrTYj6aX/hh1XOCko0+G
TBCbIsysRACD9yIE9VmZB4S+6I47WqQCaWm5wnyslGlHAFuh0El0fOMNVCvJ4hqkxPjco09UN7kW
22m9fzv63uI1tmTJvTRqILfq/igLfMOXsNBEL9cDL6Hvm4qV0dZMr4YVyKsGsXDF4oGdY026uFFP
CD1ipMfY/ng05P0E7I6pcpEa8jgP+TQwPWfu9fHesMKJk7njw4meYOInlxBXynWKAo86wv3cMJ7F
jqu3/9P5rLAt8qXtZMWdBuOcGMop/19zPL4dFlndGBPg1/lKEOpDLlXCasYXjNzPtDmecbbwtYFt
lMXhB4CNY0v8etHYIwS6AwDrf2uu8iN/lMGjdu1rp+bevfD9MuMOncn/B49iQPBh2n0cdAt+dphC
zNJxkmUD2TBf5nbYlErxb/QBn919EFXI6LS+rZrKvSK0AeKXFYOBafXm/t2d9nA5g3Or1OlwkFJ9
Rv82B4xxWYvkBTwKbm83z1AdMY6PhnGfs/UV3c29TFXatymU3iQPG6COkyH2Rqysur2QBULIRAWv
lfA+AK+k1j/X7I2SPr13eoUKca+//htuiY3XfnQu9ZLR7yivgYKmNNpuaTAmtfxY2mBK4apKzulV
gfSFSDYrShG+J86LtIpZymQGCIoj8VhOE/trzeKlognh7r2uKD7XjP+7D0vPItf70Oo5AJaUt1Xu
Y2Jg2tPLgTXWDRIgvRglbtgRnUfElABgAtI1C/0KOBaM+KBLQrXawfopDcbOGIH12H9vDlMonKA/
OJvwyfOxx3al69iMwJMXap0nuKkyR/By7Dn6sNaBN8iW1Fr8VlFhkY0qgJC5CleLqOZ1SQ06y2PW
8fI42fLUfUJAi5TJSREXYXNzM4V8C7m7Zv0PUO2Y4Q/s6/jff1xdQCqF8OX//X0d8WHkZ34Ol8C9
nvckWM+mr3Mo+5HCCEqIRU8UdgU/6kRm5W2G7r9OWz5ap4rgJrndmuFH2HjPs1ziZPttx8IidZ5I
FjZRfqlzAuUC539Gbejw+X2MlEEb9qT2dDKnIqNl+h2/OO7G/YVOdriDolohlFnCrj7r4D/2shfl
7ETPExeBW1phYXSkXN40OQ8kajGmDADh0R4wMuC70qS0gU/x4cnB15aGnG/9wBjNM2YhkXM8tegI
9mgGN0gIEG2aA4vrNCzc9Ms4MmUzRc3n7idGfOzIU+JA0vx+PgGIZF/CjDRzK/tT1sxKSS5R8782
hRWmxFO4x8JGFzg6wmVjAMWpu7gw9+5k5hIcsr2TOb8iOS+GtbGU6vZ7bpN7zVNGQizp8gsN4GWe
tlcdgcxuDi8D4HH41aOef1vc41cq73jUzgUQgMxhzm8Dg1iYLGFQVP7zIpJS/3uhmPfdv8aobPHG
MbqEgH6OUmCDPntdUvpaxWyqWyRovkRWb/Y7fpoYGzEs+lIU+AnF+CG/qgrjV42yKdbIR+21/ENE
kgC2ySE1BsayoxMZquEhCR5H4tuMLwk1hSDhsh82u1SuWgKKdn07U9pcqGevhKb6Tms2mVkfTT08
FXyZxQJ75FoR6g3R58SqA5uvZubLcaXhBhiG3NJ1bLxbzW0f1f0NmJkl73sdmv6/ZwJQb9oaIt7E
LK3DhnuWum1r5EGt4CNu3/ExDe9embKetmnr6fX+a3vAtNdxiZD015svB57DPF5tUTEmDoYdN1IK
Ra8lwkqEMLRlJXoPuU05btBhEJB/bxbu+aEhBaVEtypxQ/UFicZdKgL9h1MoM1f3WpWgXqeYxXWl
lXVCkrhfWjCWrGzT4m8dCG8DtwPxoxSx9+8q30nvVVJJ0crV2yh+O7Om4E3tPvVIs8dh1yLHr6T0
N442Y9xl+Xhu0r8fM6cbayCuscnDfAwIrbY1m3+Aa6FafV0q+dAjjKdnHTm7g0fTQkAW7US+FE00
vhd5jKUhvYz7t/8msKvFH36wHa9sn7QjT5sawwoqP1Xb0TBnf0Y4yA4NG9bHpw8Bh1VCeUHRcwAY
BUdscYwbDESURl9mXLZbqHECz6s5WYK2F5Iqk5VyVrfs2wB6EhIY9DGAE6eVTngc+V5Xho5o1wdw
UNYh8ahLMsCvPq9FJRcLprpYrVa3uNPIRzbFhcLbSPXnl/bwlYrjSUSSeDRK35ZJxBre4vuZVuiQ
3Fz9C8YhHwKTDpYkGVz90usmq+VEJEORnTA3+AV9iKD2DcMygk0PoCh0n0jBjkwHDl+AcYVvNadU
2/SwgNyGfAOT5ojBoXz6T3li2V7ikQyQGtwcQkNPQb66vNDcxepKGFFu24lSRwIQtLsyQqnvBZD9
4LVj3++ShpzGlRR74xWIDrVEbcDAN+XEHx2sneNqpUyJGAQYgVmRVEIu8Tlrl9q4t3NSzMsWUCap
lSS+2/oRVSsPTX+pGhQ0u2MgnHtZtl3pyGBqPke0NAe9ftNnZJXjNy158Sly6pVHacAODj4yjthr
YOEvBss5RGcqG6nR6ATPqkKcoyWXMP5EXvyeaB/Ml9y+ihat5q2LyZS49dU97hlPxLSRtrT4DthB
1XRisVnmssT6wewSyF/I0N/mnGcaBSEAnKOapcei3a3joDYVN6AcLQELqBxPuV7tO9MB7PyoRsX5
e+Bdn3tyUHw620MHC03wCraK/Rbj45PdQtIrj6hakHT5wihYwdBOmLhYSsJHXSTmx4S3jWDHEFq0
IlgrbP1REIxl0UQ0vEC8bGlvO3Iv6eVjWQrik45CkftL7FVVVUJUlfnZigby0MauC4aZs+JVfLJL
7ID/OuleM7Yza83J5WJd7d9ab0q2TlD+LTlckEVfId8bSg/2NikZaqbpwWNthRlmvlLu/VONzqJS
QW8Y/fTNxHeRqaWXl9AyxbgbSE1WfoFLvTopLQjUdcUvOD6RVQ1RgqUGi3F/gmUgzyIfSwrARvks
0v6K8hWw3PObXp5btMfMI/9l4gVHoO2q9XSgNc6EJ/jm378+s7fkIa9FDixkp5axduaccb8b8Xvs
mD+dngXzq2flVdisCpb7C/AqtiByhUUTs2KhbRd7PQzNjzAB61NGZ4B97sC6zWWbuy8AQm2UOobq
oVV8hPpbVsN1dJO4sYFMzkX349MBeztWHsDMyoKfis8k7ZiikVC/WZOLXnr2YuWoJlPdN5fSYGp6
ReA0KrDqq667tLAxiOVsTL7I6eYXzeJNxYPHOeTA7z8jJPwn2t1U1HRAL9PFiJd5tYDXDVc1LnnS
kXZLVgLq9GOIyYplyE7kl6b0BAZcyGMRRVE0vkqJH/zSb/r/y6G4zcm61JPokzTpyxZkvP5qHmDc
59KsZd8/B/NH1sxzaCLdwG+H1tzKW0evG4RYRfNBOpSNCXB7oj5V7BN+Ro0Mp84b+6YrIVCvgeEm
vVY/busMJb2T9blruCx3iLHl1KFLnOm6iAaoHMgUYP1GOEYjdGpT7mVvMZicEwiNxxW+ZJhdXozL
2n+v6g3j8GYxKSyuCeU63hDqiQgubAC9T5y/XM5/O2sUwRUcLciwTgH0JvzNXY4sdUCzpJf1sEtS
uzDk4UP76hVDSxVexEPFWKO9QcgpNXzDLCzzPG7tdczTO/dbgZksrDPTULfqw9hK+X4/lcLKrLZa
1o6qFbtUohqFwpFcc0bhco+73N65Oqn75HwIqLTDzOPT/1Hv/MsjLHEDgEOtNxG/gUVoUqVt66eh
fnVylQaHtxHZGodxuPOGHBsuYADnndBJOfd06Y1FAsmHqNwacfwbDPtmI5ktfaz4m5+SvXzlZuL4
DjloAKIvzqsj5lRW2S/4kWeffqsHyzTZWoIAYKQRpdWveClhPi1zFkQEI4dWVQAsaIkH8RdyWyG3
Vq6KI99XtGc+ip6TjvK00dq+J7wVcUkGn9yuRlISxhZkCHWlJpbpgRdUCQpzu6HFwFxuG5FTbT4A
+6o7Ebcu6Vqjss/46X/cB19Km6HdoMdQgJvZ94wNIxnzs7k/MOlJp2NRcVDyX6eqeXAGQukclqVr
9Is9ZWmjfN23fs0vw8zt3UZdZc+70BTJHiLR4bErzz66lS7dJQXaC5cYzYOLBCn0QNqfDODUT8wM
61R5lANavZhwkD6d6KagCGPYkKD41N3mh5vHlG8oQoGueq2mRP7nhvvwMaIU8TjsPl96YNw/Pn32
xt/l+DGPYCYRA2quRI9ya7oRIfTOn/v6gbGGu9V+YIGPutB8KWgRwhATJ0tWeu0m5x6/Wi6yDqcp
iabYD8wkJT2VnDek4KDamxo4lG5FtLkR5TuyKUPw0I6ZJvAhuLtAW2IcFIDUuKiWp8eYMNlHvBfY
mvMgLn+SN7ffn3dvrAVAjt8c95kLhPmtQ8JEr9zrDtHV7hn+2SUs4EHr5Ce1fBSWnO378/cM3IM+
QAXZC2FYqfyZ9kuMlvTmmLmPfYyk138HjK8S1GoDVwO7cPMWBoAzMvAqL3VkbDTROOjq3nfEmYym
bLFvimIx4Ya9gQ+EbwNvfrIrewKM9cWJu6ZdycMAHtJiE4DqSZSRNYFY/fDmhROVcvwhV72tQ7Tf
zWuDSfC+nMkRdsgJe7kkA8QRVjgOB/96DfGhuT3y9/mpfkkMlO2ReTQGb+BpLoEQUQlFZar0Zv2m
k7BQtnyBV9c8diowzJLB+L1Exj9HXpggu4sceddO2/v5nnMNBu2PfCK6thBKOINy7fw6Mnq21rBC
A/r2Sj0LnHJGUIcZbj08v+lpNzHubSELFGM5N9sA9Z6RklDAz/TkKp4LWEzoXBa5DBmSEvQm+1Kb
vM8OeepJRlioXbDwTLKqI941Fg08RVexs8KjoQEISVKfkmk01xDkSe2MaOHSjUf9kmOMf6HWlHgg
GaG6QLcxWqnAd2/ZI8S3pXsk/K8vqZ2WXgls5pRK7VzU12ksvM7bUmT8bqZ2QNgVaEKy99I+lWSu
Lre7OqTKWL4Vsuk13E2r7awWUrq/tnwpBA/Nabg1xUXOsKQwtB0f1W8nq7GePoj5PiIUbuaRT3PG
CCfJOw3qy7U9+B4iLA1hDq8VV5vXyDR2baF6p73N7tJnE2/Croqrc28KnU3WkCl9edVXtDgz/Q3R
m7yeTY9qz7VC253+EWFoYeSQ7sKu9HLuJszyBwxvQpD0MFEhcFGKcumwac33ru7Qqi+ujdjvNHRb
OBpwemYETobgUsmr9BScmBqQeNVkxtim+h+pOK3xHTQJyXb7I+PZ+P7zP0KWD8twl65ya1vbQVtj
b43+LnYcWTg5Fi4h5rSlZ5BYsm7zqcsjhEi19ZmHz63+ajXh/v+XdOX9nZvYraaYrhU/hWQN8DW9
KqrpbMs5OJupqtMhzQgyRmmzzeSStpT40xrzhxNNZe67fzLJlieL6sd4VpR0PzzpkDSeziSyIXtZ
dPmlmZ6xjGrwJ78rvUjqYLzec+srLgXplbIIaUt+y99KvRd5wYWaHHcT4TIa609s3GFLh4EQKIN1
Tu7FGjHybEtayi7qBllpXphKRVOPoawMbXHWVrUe+XJVPG1+/TkdFUg4PY0wGysfAyfsY0Kv+R2b
/BUXj4A9y4568pctmn3OPhGyQUrcokAC+SWDvZ7fDOBdiQNkve5ARc+lF4ieJE++ODrUHqIb+7xI
steVmHJkwcIcRGEZGVnLOD4PSYLG3vsj+sad8G/IR6asVVtCyS/CcxAOSScCQBsqG6/oQV5YHMbu
o5NFYXeMFNUgbG9TpwAU9UU4IXbqp8J+l2uGchHzskZ4sy3+x4x1j/u7jXklk82w/5KD5WiOduvM
2IKQZM/QXsBWbEhDZey+71pFmrxOZtawrlV1paCRXOXKW2YwkNPxJ5GYNVTrMV+gN1kdVXpnPxlv
drpWt1xpVHPIEpwyK03tUZNil+jF+D/5u3uA1dYdaghIZeL+7izApy3ztA5oSpw6218Yx6DOx5l2
hP0SnVBuoKjL3lzmPIyLuBCEgUAlbklZyalia31qQqVk9aub2a06tavOi/u5CV0Mmvt1OPwa+Tja
21iY1dlKxbiejA0AU9x5MXuE80ehRtX5WpQO4FZThbW/KyMZ3ExJirH+0or4gsebeJlNS3T7I23q
oSKmqYD9AcwWT8iLLFgKQIJRFMZy+6fCPPIxzG2MD4t6hv2521g77PLZY0vZXNr04/lJBsEYwMiq
XvDt6mfcTIlOka/F3DlRws+G47bIMuYWg6we8AgE9qQm7Bnn6avDavmw5xauRnJikZM1x4XRaOU/
fufJ3eeSh4XC4XayvJdCN3z7ZggQjGXxhoOlk6qqMgIMlYAXPYov+/leLJFLYAqXjyyPG2yO8H8B
zsloATu9snmLZy3rn6BsGuQWJfT3qMuCx86mPPbgwtLAh1GggQQ9kOQf+xFgW2U4KpmfCSqGkxMP
iyP4x9nZLoAVDBRgi6cNfz/u5Y1oXMyLxxgIxl/6ACghDuxkVefxSN5b4JKjCOOE2p58PEo1RiUe
ueRRXsdQPnF69qcPhPm+DEdp+CgXDVOqD2SSTrpgOWGKXqCJbwgW0OsBu482c3ScJuh3ga48+lVJ
PK76nl+UXpnVrfP2zIRi0TH27D5ICRh44My2yk57vCYfElyarmkdzgFwsh+atGtHNeYc4jgPk0Fj
OYnMgFJN0aPg96l8KmFqTWDy/YcrfZ0AqwT8Im+SfxvdS4Cq6GfYmOQNuZnPJxYL6syJxX/Apa6t
K2A+yQSBgfXuO3LXcCmL17FAiuxfRAu+MX1e2FppbFx+7FVSokqlinQ/xRXfecmxsz2cBf+b/aWW
aRiLLLsJySXKIX2JZwwTWyjsqbn+vmjdkM1aO+goW3UOtmmWRAhbX3r0foOmNI2l1i/Q4fi03b7w
RdXmqSiNgPFhy0yZRb/c/OPXHuSeKKDnxqZOPN3rS3uGZk/6+0Pb0Wxi8Z6jM+L4ZDnWEfJbBYC6
S4oGUa3s/ERpUkai9Ryh90EFyas9b4QsBJvX39PXPat2WH7nn7CcqNwmFzm6A/9EGwJKmCMTg+JZ
jSrbOGzXOFclTIawEaiVrCfzxcDTaGq3mVJqjFFsN5vc70/4YyMPaArMzd5a4+LfGC/tYS5RKK21
xX5bHNz0MfsaBpVn1zeyxSP//E7QXGzBo6ztrAngUL2mO/aloys+k4ZsXyt/CiTgg6qoPSq81eGC
g6jaydJnINq8xYggKimJ49/hv+r59eZ54I4odaIvYS+bXzgOlvPEkUzEN43Ltl5BwjLU8mFMuG/E
XIefrwa+o3AG7Z8WfzLeLMC8Smj+LC5IZrTeLSVJCinzg1zDT9XB1tADp6biUFaAsZiZtOqzknol
oDDF0kvwSAyZKpHBUDaUcVy0/Evw8mtD5wibQZRaS1F9uuno3C3OnYXpUsngQyZMH35NoARzyzLk
d8ecLo+Py3jcHFwO1Lx1aah4j6lj2yGnsXHoq29rhftcUfVLaD3q3x7DMjKOvzunafFv04D9e8k0
QAMT2hnByU5KRT8fF9peYifTvVJkirA9/9rwtkW73h2Nm5jsHLU+eRCYdSJhb3sYOobzkwHsJjKX
/F71X3Xow8a0uV25TxMd/6DYlDfnD9kTQ/ZUrzl+NXwXJKjWKT/kYX3+/bZFoUHotDEhGW5JwOqc
+Xhy7jqttgD5xiwYXvZM7uX09EoT6EJ1PMpIUn4fGCYkYiaPCSnJO4oIyJHkzg9Q0+3RPjemAiJi
GuBOOtXXzDBS+vknR+FHhCRutYLAz9Bu+6gRlORgtg0vBHYx6DgAHSt0wa7G+OObojBPbDg3qMn8
ytb4FiZjitQSVZAG/yHxu44TaeZRMoAE5WPAgq4QCrAiphb10ry/H6gGfUPfWgzcpChEt0QgicSj
eUcFxoJ8W6NVJwKgP4kdPR/ReZ7jP7/IpkgCGRcdI9l5NKFLnMGKSUnTzy9GAaUp8vKFqVm3DuDH
q/Dfi0ALjCqQXGhEUhrEwgdNKgVxd19V42k+z53zlEjFdgdfFhjqS+v2VDIIbw8p/9JkndfU2t78
m4RNwK5CyjEIUxJ1S9FpBfXccIkO2D3SVdGomIfd89G7cIodbwNxxUPwlBx5wmRm9FMpfoRCrypm
9sM1grENoNd/tufTgVnV3+8qN/0uZsMZH3JsPVtvlf/GnSr7Evi0VMaIjJfxi8n7NXMdvet1NH0g
SVTAHsuuX5sSn/1LIOlEvsUd2J4lZGq8w81cbKAbsW4aN5CodCdj9lB6tdjNDGroeDlL2Sgxg+9L
jYd/wVvBHMeYgaAUAwcf3TtuV2Kd9OLNi5XwpuNfcMXvObresV9N42vjlxei3ma4qGJYaDwH69B5
+R9fJJki9PMRMW52o3wDxadAOHAqp0epoXmBMIiBtYPf6H4uJuxI0HgWbzmy56dPi0NH7xPlPGGt
esstiZAyanAtkJGTdi/sQUMbFGVVQ/xzMIGDHpuhodA29cf/65zsRTFURr3/HyJzDJhNnNpNR0U5
4+X+dNP4HYLXTiRsBpxtzeh0e1zSyLHgisH5JfAY/YURnJ83mbMuMyH9xUY7ln2609rKLYZycfSm
OVmtl9ZebhIvhZ97wrTsVQIBcRg5VtFW7KdoUD4JdlN8sJgA4/Fqo34pJqgzjO7wiRVD+OR0yvZ8
5OisYb+y43JsiYyJGKJWSb8SdbE3bOkwK0iucgn6QeBKCYzQvLABeglUEf3hrItXH0GbvQrKn6m9
O1J6lCbgW6/E2HjPuO10B50mEvryk97aFuvja0PKzrykPwcxW6AWt2VgvANc7VlnkNvUK4L1o99+
LCAf0ZOXaEf4+p/HC2KQYsQK49IWeoWdjhRnfldLH4WVg5wjlRaxwPHimU/oFUy6jh3FJfXkcy+H
hQ09xFud34Csh7tvJNL87ykyNRbAavUMceyPIjz8toBLUYnTRyS9kJMw2dU6IIQj4yD/N1t3Ifi6
GWvDBCvkV4pYZeMsIQbxKMgo9OnMuQLoZyVBLr4OuXJEhOL3ex6e5lqIoIMOPNYjuxlw29wNC6na
PKu7DcETHrgXA2idUOcF277Hov4tu3r1cB7Z+PTCcLo2pm/ErVzG2n6uuUBfL+EuPMLEgIoOW1Qq
AuOZYu1AVn/8ffAs6trsQkYPqe7S1pqb+Je2j/VVG2Gs3stVsUhPS4oIXs6RfxMcxEB51uWKkFaU
S310BAZR1MjjyejAdPedhcq3HhYwYXa9TiCZpmlH/WEk0FTXruBxboCiOdQ9tp7saEbto/zJGZs5
XIWd5hnbfSKRMCbWTJYty1Cv6oRvhLSHeQn2Bo7jRZ5mEPrYc0VcCAnBnnwStGidpFhZVgdHfbN0
AUHeWJrWKuNgYLnYr9VYJIv800CPLriATkaB2wxkwUDfhteLdwzfFmkVcqpMPYcAXbx24Htd7E+u
QhLY74nzVPq9Nvmjw+8R+kd6Z5wzQva8NJKVdOP+PTT+rqo1VNRPhU/1I2xTlF8WAY/dtIvPRjoN
AjZNbT6SO8MbulcIRbwvVi3T+wBB6Eh8P5C9sROROalS0OAyKqMTRcEe0fNEJ+8adNMiOsYGFcEm
tLStiegcEN9FTXs0NxNVh51nN3hH5F4MIjmV1dBQNNn2K7n9dnCbgGXRQeE/7UR4FKHWIfBqGvg7
eBNMO7gAFnDiWTXUtUiNHb0RNmsK2OHJJg46bcrd1boNTW7mwu+bwAIWmhWJXNvyumXIvXyZn0Cl
8V9eBdaPGmhbghLztRy/242P5kbldTwtkCe45p6CVlfFZAL8BVI35r5eMBcpb3JlcJu80xVVBpjc
ZqVdSvr5mctTt3JOnolP0d4Ohk1G0tFoBF/UQcCjlAwhPk6qqILyNy9x1nmmocZJnC6v1bhci2wO
6O1HpQLpR2uCOG5bztoNmj0B45JZhCPII2DigXW3dwH/FXSv9YBhSXJ4oAoXVnm56xdLmUkq3l71
Ay7dBW9768P1eI/Uqvv9bbmZftpNmneI3bVrJK8r3b/9ClcVxZNi9NubMg+q9TcIPv5KmJkgGA36
HCefLDvEa00Af0hv/rmlhV+gYe30xrhSlw6IdUqoG6b+h1XENDHh0h0kFNLDydvyS28oauU2vVUy
3II20FRpqiG21WwAeoxrPZniETcQLtPOqrQ6SGr94UNM1pNZ0OSlud6M77O9N2mI6HCIwWSaAUbK
7GQlmG90WLqN4Cm+Hf5t1rzotk7rAsP9mKuTxsk3xZ9kfbErUpsWs9IXUDE9+RLMKVRewOlRTRQq
pz7ae6+7Wr/InqNr38E/5rrMOgrxWV1PlUdvA7etWgEMilrHafmIbk1PgBwtiiFQw+jGWC2ET/yl
d9FHZmJHAIYUjR5YHraN+zoQGYfMezPBvLyunbUgOKcLxCXixfMvaiu8trXgVihMy0suUjy8Zpp9
wpZkAOjnwjfV+/UFrCcmVpUkWdP+I/8Kg3KvxZewM2JeOM/38U8PfARl8GeIFOFIjNVp5DVS0ey+
vZY9biskxC9bXZ4tcvkmDijZtzVgy9H69UbCsMrGHjYF7DADFoGZp38NlQRpz3U9Pbjt5PgRwg9W
TvyBFkU6CADKlL87yOGJ9dptdm2BZgWdNAZMKzCRr5YhkOTczSl9Cq1mDLuIoMMDAl23oaHoM/dS
x2fD+pW3WKqyHZgRfeCEVvITfnF3XuRGKxxj+b8QmHtPCyUgTdD5GBTsyTKROPROHraM+FWpfCl9
vXQZ4vET4hlEavLdQ7VFt+g/EDFHyMD5Wey33MgNdKU0/KmF59l8YcezuFzEy3Qm8EYcLUs6+5nb
2VfyS/mFBkQBG+CR7BJtWZFDdBsJym3zKZRQTDaxRIv2QolQZYd5I8B7u05HRcV48AFjWmuiD0t6
9Jh7pSkMD8IU2Yd7onEMAxp7EZDZ8GRzZ3fwAeXfnajjCYJXQ7GUk0tu/Ycbye9fzTsaNakdqp3u
kKu3Nnh4FsU3/d9WZMzAAdP6awiwr0B9O0SqHwHLYHzxSM9O5fbceUnIl2lWCwSt1zWfTcLuG6an
/CfkWCLGjF0jr16v22G5E9k3shL6JvJriCwjCAsit/cnKJ/sjeHFnIL5uVlspdc24U6lqpqAXhQ3
CTKaO+yI0xzXURVACHbhu9OoIyKHcdQVzMjwkOycdTQNyAqaRkbMoBBXr8C8fEw72ad7SfKuD6t1
HJsR2o2a79svCp8QaeSBZLPfvTxRDfN8BJ24uFWHtr0vIB0kv2TjRprI858VP9cAIpmuxGLxukNw
R7EGAn0OiqFbqIaS5LoqevVrtzlol6VdkOEZf61fNx9Un2Bz1uj03uGnOruFX7Xo4jy6OWWLuS4s
lT4M4aUx40OHCwZNWBflND3VM8UP0CNDcpYfmheRFsg+Db74K5el5col8/MOJp/VJfxHRJ7A87CJ
nDoYRYHZ5jgX20LrGayZs7xipV7nLpgRBRaZfHUvvCG+TkW2najv+YyToplD7mPC809oRAayY+CS
2GOLa1vkJPxT9WNR2ctKL7PjM/oJYd1bCnI4HAat4/rpjYtvIKx2d9bhDAi8k7+ruxFtQMrmk7GM
bHuZOcOseHqE1H0p9nrgUSwTmIt4II/fgjmlmklfzWulnGowsr7pAgVeF18pkNt6x2qH6vgkI3+n
khJvFNKo8fiz/AQ5hJHlz9jcwKPh7TSCsmFr7fs+iX2wIoIOA/4eewH3QxlBfUExYqJD9W3XMpz9
QyXZk3OBNe7f7+PPj8y+zHI607OH5RzZAhVVffNvOgWc34UE5/mbqzpZTcpTS1RKEOA9j+55+HPn
Mocq028/lHfGaXk1neSXiw8nzYld3mixGSWhT9oM3LuLIJeHabe61ALOr42P+SfwmjY3KCBrfK3u
WF9aV2XUvod/ZgAsnEEhgN2w61B89LY6uUMxRGmsGFjTW1vBeObaAucCThxZ3XUUuNktxKsGJh7C
3IGAYDndkfsRfwyk4Xn4tElwJeQp/A0YAeF5mIFBMp0/DLrO/0b1oBoebdOW97WmGWa4StUL6WJa
Qexng6Rgfl6w8Dy7JQW0b4MW2rHOMWmsckaRLJ/QptO6gquydCxt3aDBfmstp7uaOB3bgtd+SxgJ
9VfhmlF6QojWAZUMlQ7k5nmsCPrbAiBiE9IzaqLiLeHe/2h9mO/iskxIipQ/g4qUDuL6IDPVs1sq
2pY8aSIVNQG3TIA01OJtvSbxsuM4Cxsuw0AGAi5XNMbUvYRPoG9rZeO4+DSujPvEXzTP/4l5o1rS
C1qT/nXdfU4BZ8GgGs/pn0mren5MCHRrENSBWR4ZNsCr4pwX9wVTlJhVQ4tqrOp61NaFdu8Jcvwf
0P7//7N7w7IQd/0Pfz8Ez6/mABcmzyfHU+A/vGIhPxfGMt1TKm6r3frX/db8RZuKNSa4BaWtZSyi
EtctDa4qKnJhVdkQ8HleUO6xaeGTXXkFSb55l6OHO2y6uDSC4wN39u48PLJ8D5QmB9vorcHo4Tx4
+MRZDCO8FwGx21yE2/hoQX3uEAvTLxedNCJ9F5ZZdr6ons4Tj0gwjW3+L5XvETtnfTn5i3n+DzSc
5NhVIZHd3tbGxNxaZY3lWos24sAHwyFXXgu8i/MwXcAfZcdSqjRycZ8Mi9OIpscERU2aRjwIKW99
ZuGaJCjrJKy9Fv+8Wizh5azqP0HV08tk6nH06ZRvnaNNmTsp332IKH3qjLVoo4hJkJYMskbeJvG9
Uxo4r5YUCsWfjKRkG5KyFB7FdZn+idmOG19aNjdkN/ozKi4JwQpomDkjHx8cgouYi9dMmu/AkKsj
eysDIgRGrPL+Si4+ckDOaKAMr51hPGSZqG6iCWTmza8P0RyVNyRLJzx3vFP08KFxsPgszso5+aE8
hv8VXSqxFURhXjcsiBQ4slwgGzNwDBXD/I5RIkeaQn1/bspbDnGNt1LKsrfDPiF4zyj48OtOeo75
4/4j1Iq49TRaNikIV6zQ2l0UiuB9D6KvGNO67lQdFjzh8lpuZeUuJGXFp8fUhV/5pAWXxJnSDEQt
0cunYPBGGHvgSG0ZMv9vbNvuYIM1oDz2+DI3VJNzdKnHcMoPNbPVYXhUAIArn/QvbqXozbNL7btZ
CPkm0/kqDEHgnP4Fujw/w4nlLUQvBFwGetdhWtWmVBMcYBpCG8MaCj61FwI3sGe/+kWxBsv7tKCZ
y6EA4BZV2sG1u4ra1ZqYbBIZdABD9GfTldgBYTraSN3ygIR9u2W64upkaSYUykVOaxinJ67f9f2Q
HkvneBjcdEIj1isSzoqolWYJbGCa2Cq8vxqmDtCtcabeyeqxI8m8ArsMhb7XLI9laWErFUNgGovw
JnQmSwyudB2TYmB74eXg2TsHQfFgUXdeixdKVTFo2ZJ3BSzUZvwKsXLTyc2tohsfGa+PKKF5D/NY
xPFtDDNfTnDe2+3w6adI3ao8MXlGJvTLDNGsCxwatUbXoIjlNdNifd6C6TsoDK/hR18KSu4F8V42
XYQ7vTMAytKrcRN2XGly6zTmPnrcpVtaGHasRY99BNr0/sUhJL0ysoaF33xkHuvxyZsT1TpWK0KA
NFGLpBrewgtyC5lfRMHIfIm4e6plDnOCgFLAORSs65UV6rb2jiZQzZws+T9mRccAGISjBlcd8nDf
0Z3MKag/bgOYWu99BiOgfUDUadFi/ImkZTS1IIrKbkNGA+CG7ASYID6bHLSnysxpzXwDUYMr8eGf
0IB9TPmDDoV/EfoCmcYf0clnWkOj8OTZGsBxEnnl1VgwCNjTdF5IVLeEP3flkcm1i2u+gLGxSkeL
MK9gpBUCZCTV4+QaYbF1op0AK6HXZifbdOfWWCMHEHoAZj0P9O3/4yqfj51sgPDTUbwqNCnKAYOD
/j+dAzMw8i8fsVryxw3os0k1w1VZg2TEA+4xPlnQbACFbvl7qfUDMxucLYi+6imeKQQN7RkdSFvf
8FN9HUI4U80jPuZJh6yHSaMUGWfCCxY3hiX36b6OcSVDmydGfQvHFAX2LL+FssaMAIo7BIQPL+pR
FBY9nY/TJUJPQ76mrJF8lpC8WlkH9nrcoT/dU8vuf3u8IoYRIzZnRU7jQrKfryLOGl9V9tYgJYO/
+8i1EcQWLwOTLNZijvJsFvtF4nHT6Gysu93wAQ1QzrxWlYxfxy5gvlv+NYj/12qtajDAYYVe7RMt
wr6hFmUXg7HiZ2M1Gcee+lgp3bMIDFUN+3xLq0TNa2hBGfikAbThZDjocS9ESTKl1vNO1ZUj+8r6
vyC/EpfZ95RifZ3HV7oQ+DOIfhUzDfRECVVsHcgXaA/yIJeWFMczMGrcOLE21lP02u4AKzPalrUd
oV7pHaZEWOQXpoVesMBCHTKpkCa7peyRmg1idnV7i3L3RbEvP8CavPebDdmIij0neZUkepQ7CeLg
xP22u3TiMeLYdKKaNE8pf/WsPYr0/pbNxDxmVkVK0YoNBv+snIgcp1ce+eM44t5b+UOKBxkvoz/S
IEk4mav9gMpPrdkapOjPTHEybKPQjOKKfvdSngYdN3xOaEmiiO9egRgBNUh303ZuYgkKMozzYixC
Na52Uc0YLeND6/YisFXoxa5DxdXxnUZ1MTySO+i0eVYRutnYGYZ/8nynDetS5cmwvBt17aXYbLmu
DrEYbZOB+72VUfPvUzRRwT/z+lMEyEVm7ALFJUo1hx+uLJtcg4+oID0spPqaWqTePk1DRbOioxFZ
FrxMF+odbWl8EySYnT3JzPJss6E03AnlcpxfawqnpQpGoPhHSQerLGp/xD9IdJt9hfJu1RLASmXO
7xiCtB+kYojZ1rmbjPrr6VqFOQhESfxEFt2p7ZMgiLEERsOw5xbXPfOgFI2QmNx075gWHi1LK3ic
xkdKdNQ/Wgsoad9ToUazCUXvbchbxexqkRAhELu4a9Pf7iaA2KB/3pym82/rIEEs2eqZSOzVStDV
Fn8Y0ufRzwjJoI56wLn9QlheGF8WhktVNWgZahyzpImu8oMix0tjChSBPhCuRR5TH6q2bw6zEfcI
OdEYIg5WnBxIqlwJgPycUAH5iXYKWmSuJel3PioENAd8AAteD9G+ApJeUMOQBue+zI8D++VyCy+0
l7YgemKCLrcbq0ACBE5gAE1WpgSpZYDxf5DP2oZr5pXs/MUaoFbrxhiGyoBsiRroYQxh9iR8V0ZL
KmlG+jmb+OeIuGJr0SCUUUg11KeEGOCSR5zMqSf3F0Ug4fGAYtWwB+WAXrE5bE69/bhGqguUpoEN
Xn5nWdbyLhxWgkFFHhCSF5gukpwp0DkktHUy/bkZhLftC7ceSENkVhAjS3wZtVn/+HQdbMCT2vTL
di+AkjILRQtp4Qi2gGcofYLrEoV3OQOAusn1lpHh1wTxbFTBm8CmrdfZsrSqjGT5LUWEc4FroHHK
0i+paIR5iuJ5J8UqXy5x1MpUROUPgDjWq4tYDvfWh71edpqX7f7iiJ+r4RAKXB/zSXhaEXAJTzdJ
eWBWcwvWEzgjn3nH8PS9vnEh2gjHxBA7NAIcGNb/TiTV3uxhKvIS4yX0aYUmJKhGgr0ng0T/JQbZ
zwIi2hkUWnb0uPGkfxiIJHeq8dTFew8ZKKxYpAE6Zi3mZkawC0QjYFwJHiLPxM1ibntNt8k6LEmq
aa13snR3G6le6hOrRcAyvEhgVlRdzIZ5RjLBPyUtzoLGczG8U1G5D+f1Apz5Lxy1JnLobs/elOg2
MwREBoLVYRM7qPslLXlI/nhKmDl46Jns678IJE+gDbmCnfHW3qlz15heKFFZRn6xGMV3GpPRY2C1
6jrlVYawuLFqtXk5oBa2m0HHoHy0T51f2wLGCH6BcdzDx3CWmvdMpEOhavp1KJ/0VODFH5/Pugen
ZW2MB69JZw5rXpTuKNh+IS3BRpVZpQuTQ/5Vs2PMOVEwRHCXbRBaH9gTsrwnqDU7TJhnj8+KwN7D
lgoivIur2sfqtwx3SgthR1KNO8kvVZ3LYt35BhwMmIYWSvSs6zEzX9o9suOnZmTjYJ90yXYh/6aK
NYdXqS+LXAH4B9CoBRgXY2k+jiht216TH7kFlnTOG/gSKSLbHFAhw52nbffdSX6zhzTTQaEDDLps
g6VwVBOIjJmnNOCJjaTOdLCbLEagx9vJsbmtNr1iNmLHW1QZj73k7RzMw7rychZUWnRJCdoI3v0B
ucoHdjXKzBBLGcrA/j39Cj3/cDkETFDeY6tAc+z0n2HdbFpdKfp+j7jMSwdsd37NiMvWEKc9X95h
sro6kizbNphxbe2GwOYvKF3Rue2pmVsZEaNtKMqv4/v6o/jqf9KbShdMWyOwwDX1tiAFMj1Mlfbu
Aj1oy8H419ajNWHDD+/jGA/dfYkrR/AA2pQneGC0QV5bDBYLweH8ZYIUK0zi42d5PN/gacLHvesX
hhn3F3wS4BTYOqhXuxtmcaemz8wk6nPJw4ilUVs2ik4mnw6Pvakk28/nB7vJzuVAuzzLIFBSb9gJ
sha7ZMJd2CRBoNcPueCTZgD9Q5tnNok4sQfZIGlceJx5tK12BXAecreeU6F+0le7rpgxZW4+5LMM
z4Rk4gKZwnHo5aj4Vy09ZgnJNmwFfe7XmizjUCvyEH0N00RNq1pjRKHtVC/sNnJMiE8FHxGCX4Jz
f6QssS78cByGBrctCdMTmWJ7VF+rLyLKpR9wKDRHESCjRAzElglH94zC1Y/cud0hhUyu4SIhIKzY
SEyWZ51AV8TUAsfhLPV9U0fmzOVp0SLo7hlMoJyPPzyAg2FvAiO6pIBVr0sJB0q7DMK32Q1DdiSx
uOdMII24MSLLdPjdIFUBB3DymlsHizgbXjdgX19YlOt1QUmqF/D88KWJUOq5iR1fXKv0kQb9PDPU
KS/U0m4q3namsum5S+MvDh2us5RZs/SiuqFPBtG0FBixXyVZQzGt58ZqNNcCk5O2XPT4NuhBQtES
9vqSpQRNEhREUIi+g2pg3FdhylJRerwM8qAmeaecQ/MhVkc743z7dsiDRM98a03yreu1cRRZNlJI
Fj7aN1mBcFQGA8OZ/Eq96UuZdvvIHW2HKDvhPrvUo0c1FFhOosUmBYb9s0pVXPVyJ/R3i1nonrMy
J0euODD6ubQV6nnQnnDUoz1Me5ZZHeNP1m2W6r+Pk1MfMnT6Lnib+5WlPZOCYDX4GMvpZb2Vi5/M
t3gamG5P2orMuhUCr82QfpMVAdgdGDXg5PIKiVHAYlJ1u1DDzXVMywFCR0pe75iYJ/ksy5/3TJZv
DjpLORQJaTeqSY/IqzPgxanIRPOyD/ItrgNzezTbiO3EiO6Z431bGTSy2kOSzphRZbMnaKo+sqik
XRkfGIHMQcp3DLyNQpUByqV6x8w/yIh/ZvTLZHi6ra3iC/k0etqvYvekXJlxuCdjBMrJ+N7VlU1Z
kkJ0VPOPQr3O0cjFoVevjyxvw0AhZq2Pla0OroFvfWCg33/J5piwhA4d7Vivd55dbm00dk00KE6E
/ObDpCkQxfZvPHkdDX1xFGSfYONkaX5+4V8VuoH+c0vWFnz8K4uRHt/ywZTN2XBf5zjrJyA3PNlE
ssKmHUa6f2AWWRNe0Uod0JtlzmIKk6FvL5fCmiz6sfvz8vqX7WtGSBCXXxewEFbob8BBljvEBLVX
eJXl9ah/ZFGEuqzlep+6dntSTey3k5hhmdtXRaHVwNdQVzupA5xLRLIcRFFk2vi+mDarieRKvMfE
pi9ju8rf8nCqBJi/eTMweTczzDGxAoS8oai8CVPtk/gDj/wmkcoefbGmMvdOqomVcLiK8v+2Q2uW
RPpyT/JaowzWLxEID2kJn38iDcgsDE5kDGHDQ0C12mRiuyYWTMB5/sBIrcwUAI+bnh50HoHiKfGQ
6O8a+nNj7w38i/yak/tm+Q/h4oCoSd1hPkUJN4NvFcnZKhQG/yGUVx1CvJ8xe/R4TBabvOr1SUSo
+/mApOoG/0wv9VgW3lv+GCMdFHhA6ARINmNQw1zgwLyqOl5L6+A2VzPHbx/YFI4Gzx6Z/YJWqrJk
WC3cNL7/GlFcZrzRkFBgYMGzlVf3FEI47kjm8y6r1vimElCiEU696NiAIYJPktDV5QRPNiXGF8Th
JLnE7fXw69Lewj8OoaJDpYHWk6hhyP7sHCTyWTgqGhDjqmx/wA77tBB+vToz9kjYde8kHBVEHSvR
3A7qojeKWfccJjbnksJ8QY9zQLLphhb//JlcIaF6h7z0GKc0ol53huizO4zOxcMKHBtEFqz1FBRc
TaPoVzI7mBX3EklLWhUkHMHZFOfw42qBxKhMxcXFvoKCIW0/Vn5oH26D15tSMoZLJi00nkRpf8pB
lf89QJNMHaQt6p4pQGN7OHFUFIDid0qkNXpmbpuTPWCmtKjrPgf7LdJino7gR9DslbI2xMcAw0FR
KlgmYlzT3ODQU2p4MC93T6HnWnHr7GzCCvCy80ciTUQg4s7iO93QN6THvqmAdzbCX1uyY65N/bJm
TGgLSn6GhwbtgnSNlp42o8L/CcfOPlz0cbjoL+4KbjgWcqA0zO2G3j7qBFoZoGSbUPsUg9dBubvO
a7AXZoSOPBz/jZKvRa0S9Rk3UlERxMi8uLYNCXV7o7ke4QbVA32L1hCr4mpGuVXxc4s/kNVPNYjX
IY4kNUo2bqhd2FGN6li6IN5TM6KsOAYyIcOMCLJQCHdFGSKOPMoL8Ud44Gd+vDitwW33tGm3vXrR
3Nn/xRiRhFHchvlJplqD73Z2N7IoL6rJcFcOqZMP4NkG8ot0cvZo+2UVPUD/Q8PWJ2OKOC13EAES
Vcvv+QREwp3TbOPiys0jzUGE3LeyhQTmcCu/shYw3aX9WVjafUA6EbMQLcYxEWIoyW74M6tt8Zlc
kIJcfXnjy7hybtg6YMe5qOOVTvBrW3NDsrRfuL1HAgeaBZ7tVfcw1UslpMwM5ERiXvpifD3p3DL7
vhXkwdEAAMHrvqGt/Ps1ui7BeKqs6vblennlbXQO7sL11nlT6DIkT6gfCRPwJIMnUur6Zd/5DVoP
zaL9tKM7dmZ4RNzGBIe9v2Cb/Od7N1ZyUy6WrlhkbSAYFg/wQq3Bn52HElA6MORIXKM6fRF2zX09
6+yLVykax8UkSQQebgb/lS8BB2BljVHhYseqHD/tm4+y2QVUXBwovJDSl9x7+/As9hCuxu49I9/J
36TZc9rfUNAoD895AuvCB0RV2LjKa38bCvH7sWX+JHNxsxMtxCDnrs1Nb8kdQaFqXCP19rcaCdVf
GDe6EVHvXs/Ie+czBGlIN1Z2OcGJyg2R4kbRq3YWFSl0G4uBL9RYOWGO3U+tStqI9EdBrytaltQq
+qaElUOluih5bi8kIZsxQBO8Hp57mQTNRwK5ouU2/igC9YNaBe/U97iP+tZaoUCF3iIYQAP088lG
shaZ93L2K37EtwRdD+8pW81/WYOKfVRYt3+a6Rs6E/ffwAamMCENa4MtlnztO4wG5iDXKz30j66W
ynqPz3tJHRwkguzsu0RIrQwpBxlSO33iBrLmqBeiwanpIM5tUK4ZErvkByT+PA9QTm47/fUjN2jD
fwnvbQuVN5OjJgn+0NveCoAT/+kcf6RPCpUn1lb/EoyGFfrYZeO/ORbPizatZAWI8Aj6Nvdfz0GE
miKp83enpIDxpfZalsFmWC9gRwiwiIw9Rh8fZOFF1IvdNC87MMrHcufgQ8OlOHjeaT88nF1vn0/I
TbhgvyLO4c2qQ6v0/IY9uWy5I4ruc6UDwFtvkSham7ZWNZLd1PHDQ4s9yfeHXnz3aSX/Wz4U1kdM
wwgmKqHDzes5T2qSOFvZ88o7mONlKDu+CGgiLqzQdRuh7cfs9yX9Vi7E2vesO5JGbyMKqNCEQQg4
OVUFe+tmPLWTbWgfV+fK5kxQWmgnKGctkeJsXos3cU9IrRqeP5ryXsp8KF4dQKix8U/L8SPBwWOk
GL/ZqWJIJaaXuBCw2CSshDRoi9wpHQTdbtwi1tMOlS0Thk73pbbMqkY8C/NiaNuTZM6DHfw3io1a
rIfi+sgAPlrj79O0aVa+GwYVqvlUKrg0AE+TfNBsypUi7H+Bab7gOyUSaD7es0ye5JwC6n+00RIX
WkoHbOyeHLlQW+YR/35zijS+iuYOk6nnwn2KPz64OwwcPcGJ7ZGayWnhw/LzaNmX6f9SM6cTWLyl
G2jOVHbkHNlji6aGg9CKrK6u9MHrwELbZVy7oHoIO3clrZHJ3e8dWYuLoZWURvMGB9K9KRqSlu2e
0P770TsWoGhH6NQ/c4V7rLPLUWZ7Mrz5kVhWKBnkuujtMhe5PXOKgFGq6v5nawyX9xiD7OvXWfiV
RlrWfFaoHSZzA94nRUB5YmpouukGebzmEra4nZlmxIIKnsKjZI3ecj6Qq0/vTi9f25dMx5deLr9/
tMfKryHjPy4tIZDs8ew9LEiDE0xuB8WzIgEGEqfPnAckJeQULwi+wzQROwNqrUUkbFMlYgblCrLL
D9n2qM+FwYWIwH8qWuVl9ykzEgIpz43aTGKZxuNO/rNA9l7sQhmJioz8UNmI/FmSsJgX2Da7wafg
oIA1cEbDs/B4lvJEcCcVAxjhQr93SCNYGhp4zaqqfavIjYv2l5LxOofgu+8SU6/x0ieahAWuLem0
/Ai1Htv+LRjPG0Yblh/2qhmcBLevdeuM8Qd85e77p1XJn7nC8j8k/0PpdtGuJvjPdcWE9T2yywed
2+g/Wc+627SPkH2UQ7kJEUMBPHk+jY+9gf0F+WBpyWXdYhzMGQ6Lb1mEhbE4ZlwcLkoTFbv3q/Vw
SmtTiHUHnUQD9IYQV5ud4R28S4CaR99K6ImMgq2M2Xy45xiXBMvwY9RHDZ5oJT5pIUIX9WeAMqRv
nBPywba5ua2lP7XBvTMz4Up0CuWmvq/S2a6LBBZtKJCMS30J88VObBwLp6gbfBuouxV8SE72ym82
S00bxcVJFRQRXk9F9p/mVjgeL3gHMr+2DRL0Bs8Xkcmz90R450yZrc08wV24xNFMymt0K+GEYj4k
HM/E2Hbns/jtzXs9V0NW20eEVxopgSSwgZm02aX9eR+X2yFQw+AhdjtAPL5hKWSEujGAODHwt4e+
rNVmk64eV5dkfYR5MP/tA83pFi9O3LBtaCMmrXV3N2G0td+VDG+QtVxAVsqwJUy4aZP0u731qoqU
QOe+S0hvZhrbOVo6c7ZYmA5tKcVlbsAMobhf7j79xvbs6wI+5K6Gf3f3H9q8MpTFlt3s0KI2yRjb
KN+vPc2B4E0/sFI2m9MGG+GpS357FhZerqr5RhYPELfTeLmmZ6asgQIkaODiUUCvkIsfGlmtOMOA
stQe7pmqzfUB+b96/GqLfMawGGW04SGRwXa1HRZeALFowoaJZscAG/X6NHmCxzRzfraIWAm1MnHi
NQfAgGtN8RnQ6Ue+pW01boeTNs9MCg6DJN6YyqDWd9wsEyT/AZXG5onQw2VERnQeia7EMo8qNgyo
Doc637lBCf5bmRRq7k2Rgit/pHGrGLEmLTieZY8G8jtR/wBU0x3LT1LwApn2S0fzg1e2Gm+ZtYAa
WU59GP2LGw7vjpak+5bl9Me/J2Anos7eMNEY1KOQzr/2iZwT8pKhVeq4twd6eKcTTcxp3ZAXF9qL
4WPXtAV1a+RtdzkzYfF8jLysplu/fVGUi/datTpYLXrEbm7vnG44GOg4RXotWArNGopwvhzx7i/I
drzHxPHtRSMiAhRdcVP3NfaDNG01SmuJS80gjVsZl0g/j7rmPecT98s4+k2gj89NqSnwIg74FHLb
46et30wzZpTzRpa5KYr++UXRZaXiiC9+rAYA72IwUX5uKgxb5U3/7j4uY7+jBT3ifkR0yUUd3ad0
j+bUElKGjKNifvyi3oO4nEOZNRwtzabF9lPh7GpNPnge0D+YNjEIrGFfj09nS9pXXQcRue9SoDcq
PubSjFGIQB11M8WZwxKDD67sL+R6stv+pDXQsv54o17w1dxHnBF5al5UeHO1i9q0xmOYC73Spscc
fhdCYbDghba+hLu2/fEozDtzTV+oP+jRU2g62PQg15k0mmWkBd+5HYFjbX80cxe+KSz2c6t/JlGq
IgrIs1WzoD81KCzuWiaf4RABD9LsR2Ij4v135V9hzDpzbyDQl+XEiAKnjYaYXsOWzLKQ2zE1Oemh
WOL8mqKx2tg1/nZIKbh0GvOqAmYa8FS+kBqLwbuRf3Em6RZQj/BaqVh6XZlotC1kWQ6N+rQGGNPn
A1HcfVYwCZD2cOlY5PCx2zSXvZnWB8jrcUX98SpfFWhAZuKmMEVSTdla4sbqUYs1KQDiHVxMQ3cB
7qZS+DJYkuD49fJz0DQWFbiIoZ7foSjuV3iqtSbNr/gtZnV6tCsS3182kIrRKEltoBJfKdkfQmC1
AzqoZIBqRpjZhfr7LrYnDlD+2LnYnSsOVaSyW8DW6cI2mm5Rxd+ZkrLYnHQEtDHG27WMaiDx9z7i
YEpN/KkFl6S2oapRVljgs1XPFd7eGlI13qK4s804B7YIzfqYgzjikCqS8auGRuDh1F5ZeF7twwuC
pERGJWXSlgFHZZTBqn1kT1knAugIQttyiZAvJgXNsLZVX1h4SXbdJ9D6mz/R4cSJqJpZ4+yfSJj3
SoImv848gNCG4XFDGBaudU+I7Xysvxxaj00qzD0M18c/RDPe9RwO04oKo0VsMa6svCYam03OuAkV
0ze71v6Jv5kJfUc2sbBEHU+S+atlEF/OfnuWR+8frrHV8NRpqkQPpAZD3htEsRl1I69vXg2jO09m
5i8srFPEIlta3KuWJYAkvsPCKKoZ+qEqOxnKPzcAst0JfTX5gzqekbvYEHSwSoqU3TgMG2R44PWP
V3ku9Nbbg+/gxnQaCy2IAyoayGQ0kkAjnsstYxzjcwIms2asbYk7Vkedfv8aWQ+VSsEzoGFd6ncd
dplfmOh3spq7+FEcvuqYIYhXCTdyL96T6FCBN5mVKQKj057roQs9BJqEO2KoHHMFk75ZKHoECFoi
mA6UIKAawIPftjBvFw4/FfRujEqlI3Pwiw6zOngHBtL4JSHsAPG8kMngVslNTLNGtEwL/conbLK3
pdTYu7ts+gLONvrNgmvv3woQBxOCdXxxigSQ5NhXPwZrEiXLg2uyCnoormhDq7aXWnDEa+Isr3Bg
ED53qJYJg0RrjJR15lkHKPWhI6Cl/MXQ9UTkvi7FtsoxdR3/9gjLKIabSP/ewGo1LGu11wzwFgJ0
uX00PLxYB0BmacJc+DzY6PooY9y+OF0DcCePP7zO4SI2hidQCweh0Yl/ytgxSibdDnCQnA+32Es0
8P/kaC+0QGecyL4soz5yuqyH0oMVgymBSaNQW8HcTqwcbpuw+c9Grak134SgAcYVHTV5vyiJ7uzU
yoe4K4AV7nEMBKc1W3RZBZ8uiTVyAkj66+pKe1uLmXN3RvcAtoFu2to9EtCsdgrxy6VZkn1edYpX
5fdD63O+lCX+dcIxW509ZAFQF1nEsxjTCUWy7x0Io+rcbMvXzzJbZph8GGEdnw2N9LXSo7ufAi7f
GZFVx7wwISOKY96v4BY8JK1EletnL8RBCLgRAEzZaQgnW38BN1PfgdvD4leFriaaJrs4o6X81LpZ
l1B90+H8UuACoBA9g4QlS3fS7bJvZqeq19BVRV0+ZUuWW3xPk6Ys8cbVplpDyn8Gq/KTScnhYlAa
uocI5W1GxGvDzB+PsSpIhM4EDocVITTm9R7aXEhfV0xH/isp+SP6MTDaNmbj520eVsDu2iFa5Jrn
x3RrHvN4vFelT9KzLGUccEUhX15KufUbxGd5WLNG3tuW5/yUIkkLgH9J5DZrFhPycwixtMYyLjYD
dTkxKASnnQutx7PRFs6ZK6tVUwwlm2Taho9B1Em4fAXBbyJSKvifLR17mY6imRrGbYUApqxhEJ1D
DHL5l+vCq/WfWbChyG/Nyo9G9q//mIpOPOQg4JCk1Bj+JxFYE//ATdlHkh8m79StONdgN4Jindwl
JSy/pxKCRzGokip5fUM+FdQC+1mguPlkTvzo8FqUpoJ4wLoeNwC1rEO5fQ1DwDvthtAMTl/WRC64
i7kU+r3ghsP/AdNm4wDV6UBuNcCpF7aNMglyNJ4+aus9BaH2MkfauiRFGuiGgshrAZcODr69wxx9
MfPHqcNBykAnBJ3i9Zm5ZVgym4R3y1W8ar7bt0TT/z30IskcSdNHbrmnaBmy9nveGTysD/0mmlLg
Xp4Q+A1z/Q5vexXb9JBmEu4VAGfiv4MBtp1gVhXnYTNhxKLpa2XbmlteFP9MyVjUvgx6S9OhR0/5
oeQClSbrSyl2Wv+E14egEdQupAMeVEn9wHFeUDz9b4xp9aa+fZrFbNvPD24+s/P+lXjz6Tmom1nx
DXWOSI+mQ9m1fJR/nfC6rFJV9fjcxJ5aAne0Ti1vrmIR6IzmK2sDFrA/LGC5jZo5TM4cXS2x6IWW
k3johBGIhEblUAEiWcDZrNWgAffiPSDaMyyQFwcoUGUNDC3+KrW0TEqQFzK1t7jGNjZLiHuaqXqS
Cy4kd0YivFVr+Q1ST8xktg37It9vUKjU45uJJ7B2flbUkxphF0bVRyv6cbC1zVLdJuPMG1GJfd4S
U93PeBfGQAa+404cDIXRgH0WZpjNtmglVQxZ6iMdATQDL8B81c2aVQGTad0/G7eDpX/e5XhXr9Wf
S+BSO9MJYWmBmP9I9WjPIPw3pUQP9BsCrvcGNN39lm8fnvIxnBzp+u28rCNd7K21UiQbzNRSaXIk
hEI4QkHb4Oz9Nt18IIaj13IPdevbCR7EScaM6FrrOqeVShhIntLJVAW1vlP/q3gigNsXEGvRkQN1
h/bPEMh9KLPhK5Y1H5tXwRg2Uv5U2Fy1XqvVWA82W49CSSPV/1T41K391ERjG0qQW4n5zX5Qeo/m
0CwkzrBqXUN7afBcP/LJL1XyIMyTT9EJpGgBzSdB7oSVmav2Sz9bHP/5Jcg5X+16B/Kkaq9Ir+sV
DIBhoDzvjvciXFPOh+o4OQCI+3qbtFK/g8GYT+5BNiul2VOSpXRBWR5LBjhqq2AJQBFoZ8urwCtn
P///1mkuVvIOvh3xumaeXnrYhUqCpWnapUlaN08ZaibcRIHVs+2vj30yTzSA+4+CS0H8pLMScWz5
yW8aX14oLzTRv7PRH4Zd6BJ3mIxpex72skldLIaYPXCnaPM6a5ybdjSgEIyIGUMEycMktEbbqhvY
psUZVgilX8TjhB0D7tUUzWdzZcWOFE+p/IqnN1MLy0L7r/9HjemgyyyrrXHGlSb1Dg+O9uYyS0a5
fe4AuEnpTDkCnE+Tv2L7pgU8pmqtMerfc1OhXeCcdifIFMsjuLRSz68KKwQTVOrAxH8GkLG5+zHs
qFkYSxzHU0wanS0H9IIyVwWBspxK3pbjcHSlrZI6StuZ690F9EKXzg+8JAntF/a9xiChb51udV+i
Jrs/+6+7HoxuWA7jR2bH1JZsU+e7LudaL2xiDZd2mPJt9mJAQnchXjbUR1Z3UZ8nY75JAt8vAuHp
aWkceonTh4TzRooz6uVzan/vUNuI9FljbLmL1blMsPcNZSEMspV6WJxNSTrJAvoE7rFdwXLEIkNe
3D7rtu5WVB5yRaJG/F+4HuwP8J6GAtDV9aTRjwEpSSUcMCD2i4x1LCabGNgfghCm8pBBT/6qv/eI
FCwrJ4xUO7xYU5b4Hh4s3Hv+41kTJ8096nOOddZ9Xz3s8tSpc71K8bISJiMOLasJpPCphdozYyV6
ryf546euzEbsf4Vy0cGoNeK2WiLOlCyS4t0lJT5BGZd8UUYXi8bU5BVaAfx9zX5ZXYZ1dH1qQWmw
yGqh+I1eCfhGLmsSnCB4sI/b8CxIYVx/YAktSUI+OMIGq7XW2g1aLgcG+BbC4upers/l4oeb87+O
MhoXf0vuiBXqiNJS+/YRpXf2vtMGVzvmZrKOwb6pk2wcY5zQ7y6c9vbiGdZdPDgs4A5i3Dl06GpI
ZrsUwr+zQ9IZgWgOpNcbyo7WkN+2IeqAOcGcKYIKbc0LDLAkzl43hhntdh+sEDPQhs2g3YdtelVe
Fse2+dIeR15AD6T5EN+l9p4xL/AtcU6iAUf3crbk8Rl2kus+xhrXmvMIrDniMl3lv4cFElzmSHJc
Hi3r3lbIqMJP9XxHu3EvdWhccSOi6TIlIltwItZzF/ivP85yRamTN4fsmNQnLrybcIHS7XkFpe8C
odiobxfX/7OP/wowB5bSCFuZ0QVAf9p9YV9hB37eXSM7hiWXJUDQ4s4hnrwr/hBJtKcGLJtBIeuV
+GIQ9Tsr6ti5Vjgo2Ns5utLyqBKwnIc7qPKreh8XWVaj5JpUrk4cQrYIpVNlkIGgyWCjcKbpx1A2
EPXba8m244o0yoEwMRqCN2cJiOpFBhbNYQSbHLV1LJzEySLEWTHIqIVi5fZ99Q79qI5oTM+5fNoP
PO48jfGiqDriDocs5t8PCs75BouBYsUefydePZpvywW2yUPXIT2vFeJguNYcrc8p5LY+A6V/uLIS
Lza1Z9ul+gE2zgSaohX9sEzS1MDRO4Po8L5PgSk/eLldVkxWUsQTfgh9aOv7RIy+ZUQ5HrdzDbcW
F+B0NTpy1+ADjgLsQfDG0YEO8kutkHv4q5U2xzDP1z5GIJswdbTcU8+qkngGpGyRYHBSZ/8MjJbo
B2+UBdpn7jPg44hQVi70JTEJypSV5PvNtOYZ0oJoJjXQpd/JNeL2Odqv+A353a8eiQxlzY+ksnN0
a3fOHjReESi42M298+5zSEWVI5n9wdjqZYvcOVIDULA8yl6FQmh5h9RYikr0+bPT45f4WYMMMOrJ
2D2vTEqnibUhF3m5BHzyqkb3PJnCYOLBOLWHx/6DAggt6o+xKRPPZMmWDkm4WZuWVZByL1rzRUMc
QXK9LBZrJDbJvN876Jk1hgw929XuS126HDMwvRU93C5JbSbXOhj7fZ+pjXLGCmVngEHENBhixH9r
apQeenN1mHJtZkveE2+W2nWd5NZKGAr92IJMhQ69/BJobDTqPRIQv8ETzgK5/ptZKmtxSXWByTdY
AJi/ShUyI3LlOfxzhj8fyTJY7dOiawuyj9VKuAO4HUqsh1ECO7hnIv7zrJYQXPJMuXJO5gbLdCVy
JhQMgiBQ/6rAh22b0u00qE9DpbbEa9T2lF3qkybXE/jTTVFkwm1LisT4GLauFMNAf8uDH2qF1lxn
dmPPIidIsDErffG+3jiX77I/wd40KC6CbDstxBmc+SplgsG5lYCqe/VBKBERAh4DuUzdjN9+B+2r
Djdlk06eqpGyJfbN3iTKUROK/mTzj98XksA6Z45mwyLda79wB2t6GZEuNzQns64sJxHk9ILEYM9W
82qiD1FXLOpiF4lM4gWH5iO56Pw34DL1GhJ+tdDjX+X0oTf5G89g3CbTzi3Qn+Hvf5IhvdZDbet7
sdC8mHROyDUEmieXayz/eJoO6MQZCoF9/4rUQS9aLNwIYS8R/nk/8AjeP/JszxRFJXCaHuZMjTnk
TCpRC7w7O8XIjYI8YJwCN6twL/UWO5jr8B44aRGgB24g+ZwTJ3n8lMl1khXyPSEGRwam0YHVaDNU
bwxJ3hHL6vzSC9wQ+hQRx5Uy/QxwubogprluGmGV/Wgnu1aTYnEiuyqLwnDWwyDrHwHg8KxRSu93
m/74K4sz25LMUFqJYxssfEQHJXAy2aK/N1SArxc/M1nC5Qk+ccpkBUQoH0UQzXrFxnkAmyCNnGYE
E6owPXA0oQC46ZDrRAfMquKowYTmeDFJ9SWCHjgk+P+uNCgp42ezrVe1jNiMhybqpRHkAKr2Rqwe
Wp2/p5L5DCQOB0Nh1+5RHRGRjCX4ojdqxIoqspNHvQKb2mgh4iDd6nCniri0++uqRYxT7ivTTa0r
c8MVs6f57Hfvr9ioPzPIDEjJetJO2g07GFdV+aLAZHMaFJkrgRgnhbwr12aogCCPqRsezuiogBeF
zEj9uFIateW+U6Q/JO9JQDb6QCIDaPRBGqiNxsYcx0TpWtFrXWTesKqMIL6LvhQo0CRwEFzzoMyn
n9PyNEzM2ojc3icPCAHLwwUe5MjnNsA+B4W1FV8cjW5TygU/0HVyNfxn7lTB0PgNvnGYa4c/9Vu7
EubJOUyV2hx2GctuYCD/T8UUvg48Jl/gxe7rRunSQcT+JX0M3SnTj7i7c9uDiiT133O7PxrKW+SI
CBehU8gCR+0kMeP7S60amZEO9L9+CR91600r7KUuVI99n3sid+oaD56HbaijrC8EIeNBesvSzCtz
a0Wr6/CwIfimQAOeG92M/28UYQP25li/8fE1XqqsDSRwHs/HYD9u3CsB62mfVnP7tjJ6M8aHh+3x
SdmMr0kARXcGu0UchDiLVmKWdRmP4+l6W9NYan6A1H69tCvQkiAVvgcwiISDlIMqzyGFYzuVdvyq
KBQPTS4orLH0Lm6n+pbzK9+FCpOfsc87I/G7iIb04WSqPUVqFUhWBO293qM3dboCNKD/YIapQfYw
/M8doCbEAJ049x5pCtiX2KGLfAa6vacxheXfhM4i2ojaKkrr3HUDXdk5ucdAeJKNw02OmEucGNDU
SNt7VddYW/uQUkciK/xkVPuDMVj+WkcM3HZtg5bGzjH5n3OhgjoCSxNDSHwnR9T5sBOzP3sqNCXQ
MZki2PZDBx8YVXLxOCPlJ3c4DnYn/LLc6uNdLelM0L+yN5S2m0/QhYUbQnsXJwdRkI8sBL7JsegM
z5CvNidn5s25oC84uN3ZlC1zyswsT9/sPYVUg4u5hAr9TP1bNoRtxmfGVytehnPIFXjYUZy2bhjp
fqKFj9hiigsT/TRAL0CA/cmAc+s0ROASzc+EnKWDqsnKmGS0sVbmQXXQ2EC85RuL8d1ZxL6x08Bl
ZMnwWOGRgpsouiTpuSOrGuk+lvZW1fZXBiXSLYltkHUCsy+dtB7rSuFs9//HbshIuPDea3yMpkPS
6Eqz+eSeNoj5bk25UUCsoHDbodhFIpRugM0vg2o1l/+sUx8jAZU5q71NiVMtdajFH5ETx7ZgCg9t
V7k4o/IDN0blfUKSfVVhi/vmAd4Pqn7vLylU++YLMNr2Xc1hxls7qEn36xds8YN3T9/+jvpSFa7J
NcfFn/7mhBKjsjEWdRoFNfcIN76Y+Y6H8kHHneWr9wMhxVkKGqwqw9BZiKpM5Qgf0tN5r1qY/okw
Yn0wYnjUoDLgM+gthIUdOtdB9vsutMws8LZNEWFB5G64IupF/0RWfT/hVHHIP49WFjDjxeXu7ndt
FLyc9U6Xy5AY0FS3lgNRomBJ3eITD2aH8xvnJGrvgWkbsNX9SyXnWaCdmEDbQ4Jo8txBxrvb3OZ5
8CZOEGLcW22sYNGnF49UGhEJhTHb5LynfBMExst/2ZbRMkBQfCfdrSWF3vExy3POiMlrejLrGjDK
KUUeDeRWhCvHdFAFK8GyacHyONYY5u0ow8AQSCQkCGFd1FYgeGLqRC/cl9I3cWr3B/s1zLadmRJ7
Dra9nTNCX8okCBp11ksEqBkR6NhVPmHJpSWnktMxlQeViqVsy90JdTv1KqKKA2cTzFcCtNwbxl/A
tHXjBKgPwh+zm3ERB+plwBaAlYR8vfqH4DL6Lko2Rw8mmKWS/6ql0yvW5gl00rp30v41A+TTuSuF
EbaI+2+hzpD+lj5mTrUdwB81xvOUvJhulAZsNWmwjH+9ArjCtx9basciZ4ug5Je3gWNi6/m93gdS
1weHAwPQ2yvU7IJnAfGifBTXZtPPFBDvVuPB5XeN53UpyniRPqPVhsZSzQLrfop7P+BH5u8PIlsN
t8acAP7PeZd7bQejI50lkzxGfZ9laEgzo4TYdcB+z1pM41p5goSVjNY6kQueus5Vdv7zRIF2VbYZ
pbRHR4wbILdBHMIAvGPZyFmKe1bu8zAEzv71qGCLlk4RM4+vJ3A8MOtOfqEF/JrE/89zIw8jZX42
alPKi/J/nJpTJWBe7WUuUx0ianbN20b1WuhJp/L3jSW2WsIg89uPxMHSMQb6p4i/q+OriA48ifvv
FGRsbtVMR0ZMDclczbbQSpZupDQaxZVOO6QNhO6mD3Q9chQ9yg1sAb63ovcmJ0nqXnnEMqPFGBk9
nF0YB/IFUEGMqkvic4HvsfCEmWhgJs1/b70ZnXvZCc02lRLLm9J76CFElHg57Z77pME05Bi7C6/F
6a72qqOFuUoz9MmoTrm0VB74WMXetpXvDm8bvpqZzNlXQ/e3c8vnJPc+EPnqrRmImACetforPy76
s8Bw1cKOgc48p1G1SmmMuU+o8mf37oLy18hpPHN1ljghSNSybXPdQMOvgfsUZ1khqv5bUD1ChMic
5KdE1Qn9ukbhuxONvSVe4GtQPW7NITqWXjqrrMhkCU8ccYJAATGy9S2eKeRp0HrfErtWc1CRqTiX
pAEAZKFWE4IJIjyOKwYLo2Dk5YP1Kb6IyRkswYVWe6m10/JohOPDjSNg18nkUEVkVA1KO4ChWIOy
oAx2gZAfLLayLIY4DTby8CPs87K0Ps+3PocdW24+kTOC4WZVihgii+uqDHm9SPMa8r1W+myOODKN
ja5LX4i5esx61bZyg1aMgZgw2ULV+LJ+xsMKXuyvP5vD139VlWdtPtXO2qQTMOPW1RJQjNCSq3xr
tCxCSZ0oKkLiqe/dPlYNX3QVxDhT+BT8JPq8hX+MHRXF8jW5EqIZx9DPVhkqZr+p1CHBeThmmmss
lWWcge67hmTcvWVJW8D+xVDnNCHphThujdopRLVxuRRqDP4oPWy5nxgv7NnDsjvrrBVpbbxsot+9
4PZ8qt3RttDjfrPmtbHaVApg5ltyk9LsTUx3MP6Ar28z+948Lsgf2ChMOs5o11cpHhxqc7U1MMX4
CUSUx6SZRN3txwYtAQGje8fvr2+7fuO98M6oG+23zbYvcxSZDWUFAYgWoErzmP7D3xv1pTuZLkqP
Qz0yDqm4Dz6zLz76qBJawfwYixtWsr6cBDMRRCl74muZstElVae0aD2PaExpNAel9nfCyUCOOQH9
VJGa5uWRC5Nl0SNF3l0MxWGv2+rofuE6fq3K8fkc6STapCqzxSxRZEp/QpAvIh6ogaSH9gmEd6Ki
A9kDe4yKa7g+8GBRabT6eyPkD/whRpirOE+VSBRkQm+R796anasMcvvTM/x+wx4BpMaWYaKsbsdb
2kx6qJcZzZyS1i77igrFPudTIk2zI8BLHzOejc7zPfzPg0fcGPETXcXWUHDqjsEmcWygAlQaFrQx
OyTJpm+DVn5Jw5U0OUvYGbcTbMmxdyWIqNk2H4SVjYGbXt4guY6jBDPuyz8xzrvTYhtp5H3e5I/B
2K2nuXh36lJSBXZrzAGaaq7sWIwYkqzB4m9vgrnQO315VUVz19ehDZA5Kota1Grod0hfZmJnl7jT
FL84iJQzP94IV9U3Q1iu0iWKpE/hans/LpHt5y7zWQzqt2fvD91MCCNoFRhVg/1eltMym28CMLVP
XTPcQbBjkn4pweTQRLjvD0IteHV5cpcoRBndDxhwMG9xOljNKXjGydyvumNruIoAgbr8D5pyfA4i
hdWVUIB4r60uY62vi9Jt+U2wdGUEqyF72CFoMdDD1f/xb+bfx31br2kizq7bUcowaZuw6Sg0y/hP
ZJI2D60RLvECa4gO6FDpv6F7B3cdNzDjH9gQ6BkUdW6dNhSD7NJevjfto8rvPCYrffcmCntkyC4D
x77aU4E/dhCydTByh5a2oWv1niElohj5Gu5JYVoAbb3fFP0ujc9JiI1CiCSn5jCwVaY9iWtn8s0k
Vf/Cw6q3gNSNiWxgeBee3iwi1m6ytrgVHKKE81x/z3PiE4qMAtGTVSsMHQpIU8KhSB3FEXempqXN
ppEeyXJVvoQ4CqOTjeIkkhlMp7wPJwVUmeEe8TZyxqSTDxcDghcfOI36Bp8L3BJw3DxdPfbq5wUX
BWw7V90CJVgLRyPKSbP/xELg/M9EPoPWLCXs2Qsg9eVr3cZXcmcTTXIE7wk82/au/87foOeEFNzY
tU/ONjzO8/cDfB9uP7BR5EDI82/gpTa44qBDb7l1GbEcYHfn2j2GZb41FchShGmjqCLdhVmsllZu
4w080uuO8SMVOQlTblB6gptoOS197Ed4M9e3f8/GZvVO8YTWpD/pvw+rbXzlUZKzfyJj8h1ldZo+
51eVConxo7Dl7gRqsgfDh5vFbXNsckn+Rnh1Jejpi/G47Dow1u4LB6qv4xaCZ7RWko0cVylc7vO/
XG9MSSqiIeIlXUjljwuuOH5Yn0i7gM5i16nutfgZPsB1JHScSBDWtgcRbh7/s2KEllPiBg5UeE8G
7P4BiVvlvNg3GTKp1p2uajzc1SIwJgFfEi5+yBpNgfHMWyOmVMSPa2WHuPopEDh060FF9VqZoS9g
qm7UwKrwoYWBzLkWTb/p5ri7eEZ1lf2Iae1fGd6Mo7xBDY10GFnaT+b0AXZAFnSyCz1uK2bYuMwi
RaDbb8GOjIey3R1G0qrwyAr0NFTwDvg3sBx9W9BzN7oTzt6S2HDbrgWitIByX/xr5MXa+xfF4qnj
XSkvDpjdQxgl0OymTR2yd1c6p7RpFIk/BaZl6xM9cHg/k0U7FKrhYz8mUNPLmCAj9YjL99zyFVkx
h9hmsw15oesD7mE/MNm0cevvEv9rgOKYUEZV5vrLtKOL4Rmkcfygym2g+2v6mpZF3iyfp4+a2MJn
13y5cfO/ewRUdin4oZZJg2N8IB/fZEMN9yxmAzwcDnU188jcHHA8f0qY+E1gsn5GeceZa1SNa9gR
TTJXeET0MOud/nu38D0xbdBzgkAnXs/ypIo5Wr7HOxKVQ7ujHvs3wUh2AGNp8jRwG5hKyivWrotw
hcTyyJK3HgZuuJOSxOkrYkWv4v9oWNZf+WDdlNKD5GJGxeymkkJrQDQMZNLL/fOmBQnsaJxwP6rp
Wto1lbqIApMWE+TR66t2o0Z6vvXNQo9KmQ9VufVkXHlhsOdCvpVBTY8NGumYxPIUsLA1Jjz07Fsg
O8TzNIPE5edw/XKqeCKbtZFtvIeHvdtlOiPkCQL0+hV1WL8eAsSfgrLxL9vvn+PAQNXAhg9L2qn7
T6q0D0cpPBZxrvwoikL3s1IYPmUcYKgOvmGwqSDlM+eh9xzAqBwbylgYJsTgEKT1CFzZQIBqBppd
mWf3h3V+IKS909xxwbF4xWephCytPMV4UVNJQgZPsDxak8fKJUOqPNYmOe9fyyZWpuPmiqUpM+To
lXajnxEY7p6IeD7OJ9w5gj+IhNd/q6sjj3I1HapO0yhoimuzlta3xr2ahs3tmf97ApekiSM9267K
U7hWBoLRri1Arj1HaZJps6oVlK2KkApazPn+/mOCj1ML395BiZ9JfTBwWtcLZ1t3jPknPX3ROSnv
rc+iyojoi9cRpaxt9eVJQPCtzVKECEpHz0Q2OtgdtfQGq6jlMuw/I3jzqXdysyEasZ64W8WC4TYv
VIs5YNQUKbxm8QzG6mR+Ojlv7vLE8OZmtl3Fw07B5mY4pJXOaLqRJ1j4XHXW1NTrnQNRCcR7V9hb
rsOUdC3M5Vl6QmQKruQz5sNPzoa8ykCHITxDfzEamLBZWb23DRrhiOMBcKlCv6vVZ0ZRp3GEePFJ
2/+EKerB6iu/U6XZmRPjn390W8E8M2hyKkQ6DMdn6ktPsQaIbkLpUdy1vmpbzwRftTOV2gMrtkLi
pG3QXJFuj0trsy8EMoecFhFuM4g1khpw5usLlNZQEwTbpAkOzsi7iL9IAGo7A2iQ0b8CJD5pbnOy
HYxzedSe7ZDT7FI8+FBUGxrhP/lUZFb/+ZjJjqg0/FVKN809E+QTww9eK7d/ongCWcKnvUjI5R6/
bl6HqiG2pWAsyek7/HUHFy34jZ8ZXejQjNpGjXoa1IUnOXPzXGYyWrYRDEaC+dvvBPXU1aXgMlJf
aBqd8XckGj43XiKSrj6/NXqF+j6a0gPu1sSG0UsVRmFkhJFfz3/kGaEqdDxTRWXoCj/cDEDw+ZTS
FmpFGyB7tYdCdT/+G3Vzs0oeY4jofRxa7V7ghC3AEWo5o4KmhG+r/uVrl4ejNWv0EgxZoKW6Mm1k
VAsKSEIqkKVoDekBDbyT5/VtNdiO7w9Fwu6dLAWo0+8HccH47G7b3rYiNyjv5WvC51Qoq5FDJeBS
cuJLSxopD9d/Ux91rW3nfJj17SFD0ZlSGnpSHVwBVgoUUsi+YuXD9IvLUCs+JUkzht8ZTsMLmKhU
z3HgXXxTZPvjDJrGszR05lPgbuWq9wQ9Ce+XQ1Ubpr8BQA6SDvPN1DgHJ7bHYMsWEomXUzsJRpj/
QaC4BUGRJ2hlWJ0Zq1IETFCHbt2gCR/ZTzugHbPSF2CC07j8/kYnDanEwCjEN8XSUaovzRNziziz
k4tdqAiyxNeWOpAeKmu1TcpS8zDcjVRH+IxjOIfXEkmTk5qXE/X/HWgwCzKUxY+fOMEd5u1s52At
7Y4WZtIZQiMJrh6lO+BeBT+nlc047/2+g+l2zoq75Z7bpair+F9XS6INJGtJXCOx5kxrqF2fedD0
sPiLGhQ+dARnGYhoxaGQndFR3pc8EF5GamTqup0dcRNiN4vUjFM8ozJ+9+PAHR0M5wpBkDQWbroI
MGySWW3j31z7ikS+LPeGsgNG+hWRkO1FSsEYK2bJb0MSVOjJfxvgJfarKgCDtAHzePE0J+zzeu/J
INMtRfArDxLqyzeEaRO5R7Hj3OLAWpJ3jPYWqku80kq+RTky5XIDaKiDUgyBUWZsQgeobhwDr+Gf
eLYx7fQkKA9EKVbQRPn8MNJy0QSvzRgB/LKRLxLbfGhaT+ZS62lWX5yi5O6hoifb1PZJS0m9KGH9
MKf3lLTplwQRhs0iz7WdeaECIX8yfE6vWrc8LsL2H5EQvG0dGv+ISKEPPQj+9Bt1Op6UpBJCO2J3
jxqQ4H5bwKfbLCzmA1ZGibcaBJzlN0G4rGjCK0yXW0E9zA+q7E8Rc4OSGzOLUbgWqLqda9/S7YoK
wVNrNdLSHt8YIoL2Ve1YLJb+eBsfQEQf9/V06Qyyc3pIKV6S+Uc8NO18SlIeb+e6W6A2OQdHgEYi
uIJUoY/3jkaT+TW6w1fq+fUvKcp5YX1dwWJUaKjnzFSvGSVL3ETk/OYjekkMygKRTFWZcHvel0Wp
MGFQUfrPXuyE2yb68LvxueigEizZdjC0y93VeqJc1wdUBJeuIATlsxzTYOA9JU7Y+rbefeTbEgih
I2AXadmVUTwNyQhn96EPPNB/aKCrWEX/uTBA2awHCR26J2tKsOWX16dSkhEpNVia/w0FixJyPTIp
kZRzK2MTH0ZSuSMoHJsK9XH0MZp9t/rtj6sx3Mas4Gws0z14CGXwQgTmtMwiqnNJUOBlcGoyLtOa
jCDGVv69ee1IfqXhqza8aHupECB4Ezj0jhDU/KUhZkuOaIgOydgu9p8KrIha6Mlyt914oM3W0VN6
o5HLWFj33IX5+KKDNBNU4vHlxaDs+twKewu5bQkaB5NH2G7tMx9fg5Sl1B7V5Q5oOnKOCb2htUI1
TJpP6h9tBmAj7e0uvHyPdWySB1DzBnWX1z0jwjGlAod48h883iiKn3u/sx0KP9uSI41Tg52rZa90
YUlmp7xuFdxn90WZSbTV3IWv9lGjI7xbsewWZJ29GQcmOKlrPAcq+BiDKCinW9aqAU7ZOGfeptBR
ScTfvls/SbRmQlWLWY1VC3bs4s+w2lED6wRfigxfCGmY7KoJ/zjh3NMRHUp9gPek737s+8awaiw8
6wkf7UzXsOwX6j1cMCZwumvEApTcjf/DLSX5uRapNSzzV4ebGIrdE/2UZT3HCGvvTeyxiwvGKfb5
bLEuLkyGi+Kygjdo1opxH3rAbPGf+cqTnVFu5rb8kppOiFjJ0Sdf854VpTFZ8lXncOV1nAOOSv9q
XdByEXPni0oI5QQ8QonYTSJcElhAgpZkp/UnAey/aIvecTIHdfXtsIPsfF9kFo0mCHUlBvF9E6Af
x8zzhBbBy2omFBaEEVIaJ1xQJcRuYgvRz6si1FcBudW7qf2gn1q532B3Avbx32E2dHYq9OeRTVK8
bLcR8jpOjFFbhHypHDeZjSYd42ChzYzcH/lG8QI/+Zn6FfjTpVIaSlZw5YwiWtCQpt7b2rC0oLz2
wNki22iKSDESMepE1gUUdnd1Ol3GxX2icHUwJAlZJ6miaE8JVK7/A7Lg1yZfLGzuGnbq+PWvL2yD
2fz6Ox9ueEqjs9mxmG4o9uSA/MxH7DH459VmjEjkGwayyaGp2j5p4wOjYuEjVk4x9lLPP3ir13tW
RQXgsMuGTVbNg63f5Zuu32MeE51vOHRpIpJdhd8ksv58EVvTi+FPJTS3uvgd4EdFMZ/tTns2pecG
iEsfxsc7Hsih3VCLem2oF9QbgMA13yqNa17OPeERloiYTpRPmEzHXRnnh7l9J0voqvq0h5lR0ed2
YuPD3+oSri4InZJdUG+0ivlKXefW3EwbZFDMbiR9HwKsaV0UEcClrV2lgL7zqzXQmihPD9pnPZgY
7EyyaJ9XlXHBb91rorCZ7wsz0uKCczsyGzVwTmTfUL+0bh13tg0/T48krs+VO9mwBHupU54hQoQp
iQTdz/mFCUcz786xfVlJVIKLvf2+3+0gimeBxw7XwqGxhYtOHePyzE8anw8+qbkD47EUQN5bAjil
Nr4evMb+yWfzqdFL9S9umZ0RdbBSQO59icWeExzhyqIuf6IdyTeHrqMKvBtiv9WU2cOgIJgK/VPs
uPxqqWK7BsJKI3gZg2qrfvqAfSx1iU8D3D+gPJh7f1pQzK43aYvSzCuHnvb/yoCp6P4nc04LydXu
Gax/OxTWppAWKe4lhh8kKrtTaNrjaFQBPRUAigdhjq8ygjsEX5GGmTuNLHzdFy+me4yaAUJV2AZr
LGd0OWLo1sc4VO7K+hp9vnDxJxCUd9qGXJqhP3Nib+NgflKbsLIdZe9crg2Zq6vE6xFcmjjeJlbg
FiGpUxqUtD9pexA5mO7EUKcObH+UZw+67dZJWx1pDWhAl9Zb6dXQVh0Fd3/MNCFbsTCrG+zEDJf/
lLDdMQwfOgU2Z0q/fxU0g+ZIJFhIqhoaLbMPA/unAzwU/ItRUlUDTNhh4bTpX3GPJIhAEA696Gk+
6uxe45twkISKYNowjVvlRE7ghS1RaRZyNINQamiotiRnNb77AT2PLytERK0Pyo10GXngU/CtCHYl
98jinVJ1juCP/OP/8nq/OqKsWg5ML1yUeTJzEf5OszsmmQt56Chj+42+lMDpK9XWbh1jXqluW9Ly
IyjcfFUHIVpUzw4cLO+3tn3W/0MiZlIZnu3x4i+stW6rbbbH6JyJ5T0wJ2Imd1O2JZCSgVRXV960
EppQ75SJmbHmQCbFYmTy//PQAbLwFya89350jI3+SkbNHYDLGW8BExNe5P7CuCoGqxsoB5nws9Uu
rgRq4HFH1td0Zt5ySo3O4Nmu/drYBBl9YUmmbRQUAy3QjRMeHH5Xt/rVuS2O3ppwBO51Gv4xjvij
UmSQbDenhZnMafS4HYAP2o6/NDBY6u+UAW8S3xFHXOYViN4/NeVc7WsHOUDDUcEfUPSnl8YVcgMD
DyEsFJORPMGCVXhyDe0tEFs8IHYiTgRMYP3egyNdPtrRaTi6SVR05KqSgeZdyeOOiYoL+GgtpDBf
bOrc9wlabr+vD2PRMidOQG0jMzGS4reHxteyD6MH89KyV+0c7Sc5CQsuAwPzIGKrI06TDUTH/PxG
dI7CwKhIM4V7Rk+evmtGxKtTjmnyefVxeoCru+FRdu1YFbq+i2Ch5HA6IcRTL+yV9H/vz4tOsI/x
pVQHaNrisFwLPyynXrEsBGnK9p9Mu+nFYW4xws4NiXZaeFtzDkI5rnMJHHApU/oMSAm2cUUd7T42
UxzRZ/2LeA1Va9lIsAXInV0qODIVOMSMRchtUNLjkFYuQyyEjAEv3SBaeA24X+A/O5+bT49pGJZs
mW5DSbx8i0/fTSeCO5b4SL3G7DxJr+vqZ/xaEHA08/VSIcyd7lxqPaK8yaaXtMRtmcaDCrmFFGzr
/7DPJkwqRaMQe5vBHEMoRZS+1jFS0o+LJiunT1F5ONiTJga+SewqzWhj1gaGLDG3My522MQfM5kc
FGPLxZYQLEm/wb8M9NlzfK1K7Yk9NmHcm0oS4G0TaMGgXAJXK+PE2RhjJTxWSsmZK2Sjc3EI3QH/
roWL9Ofo5DGGJKxVkUuezMekhU82NVUZTX0/jqvPjbFv4SKQfNSbQpCJ2ozZIyPs68hOwNEdgq5T
ah/uc5GZrBCzra3wt/NVRKDA95JOxfnXR9i0p2LMGOMgXVQ+1K2CCSs623fc07R+5/0r1SI6SaYq
g3IvEhXhfEZhvale331LulYBaFvhwE3fF1kfLQCOZlH7q2RPWYKcjnC4LH3W8FStWkMb8YJURwnU
QIjtusbSHNiGWkerbyjzELwudEFewL8O4agD4611Z2xHXe1uHGJBXqKBlonsY1IdDxrIK+HnkacU
QnummWeR2GAoCpdoeZzaCRuZGUDqVK4NPMbEmbh6HTmFX/15ElkLji8F7fDzGETaBES+86gIuW8T
nQLqzI5p9+S8JjGHjwrYsBvB+Q9teglbW6nZzNEKAvkIQcSysoDK3c3fxJA1tEFoM02jW/rjt4VZ
1+C7v2sNWz54JcYc5BVNt2is0EBBWgWa50SHceHzPvh1kW4IuOiEfYNSh47DL6O+wDJEcEuILq8P
G5Z9ojbNpQGEHNKMRL/PHXVX8Uy69OuxWpn6sfGFxf3SmhaJUqnWxNdvGf4+F+bYjrRdW5i3IFRx
p961CeXK8Lr/jFq0JaiAU3SToqgW9uAD9mMq92+KRJyAj/Sx1FA6hzJpfJ36XDSsBvtftQ45ql6G
fU2szbd+8nY5fuFLclqfxNvYtcMM7TN+EEPhuYB1afEiRQXsf/G5aM5IACH/BJNvEGGEEQIEhX4Q
kc0KGrhCUeyNWswgnRFeevRtTkPSsrLtFJBIt8fwYkwPOGhY4fwy+KSpBUKUYCf4Ii2fM/UOzOpN
zKVMW6l/Bp/zZ9CclOhaCtroOErVo+LB49P8WT6ISknZoOiIFSGITN08b19XX+lsVRu+A1LmjNSe
nIneaNMJKRtoF8Fpcl4cifHXNOnbDUSXehZa0xrarMqKbCyaIkqHV/Ex1GHnkEm0JSU/N8bCaJrT
zQWmWNUHLIVjdtNPkEi+wlfoRQZk0GsZ5+FhQKxDRW1N7or/0nAb8WkZ9xzq6EaD4TdF7uSYcI5y
z3GV2R86cAPah952FLO9dTY2/49FAM+bKERwoPpBidXaDxDyeblx1XD13ZDJXEz02668PT5t6YmB
I854krX7HG1lda2UkMOSI5vUq9+3GuA8a8J3zTbKFg1+1gJ8lGzM3hpjIxshIPKpBLRgXG8DAibD
GjfZHDtYMkFRB8xHMlUkbhH+PD+xicceDaXyBxvMorGxx8DSz6gVNAm5WnHo93ppOSWuFJjI5JVE
Uh2YoPna8qrD/CFHyll4mnUxekZkX6+H2tJ3AaHARbpx32yAwxrxElCY7g/jeN2bV/V8LVpdQsoW
2A3mYwx5V4564anNob/SbHexvuHNyVYfgdGm4lLwhWrGgSROk3ujXkHWZKUkiz+0YZF0iD2A+KMQ
XCzv+T48W/MduEU6ON8MN6R2mRS7P9VbylEXiIjj5BwZVR8t5vO87vHEwizLnR3BWzETWoaaUJve
HEaihuUN0o5yIq4OV0ZNpl+IpLDIqxu7IBN7GrbVC9vaW/Q4uiYHUQKqVb473Wk3nsq6OocQOxkE
DsCt3cVo35uRRV/vvaB4Yu9ZqoTDu3gVdUU8nRi8GWlShPH9hXiikXaAts+8ZzraZ0PrVZIFhwd2
e45Y+ZMsip7f30yUTGoNtnx9jj2rwrH+p6RFgA922CpwcPsUmjDcbLzgcc3OlBcLVQQHqVFt2Fhd
er+5R35s4vBGq9tQOWwYj1WtIA4rLEwRtH9UllkfDbgwhtOfPNq9x/EL8beIYppfoCFC+UINlLaO
0MEIqh6rkIpKsOWzJS8adYThACWn0KJbYNDdXgtPUgEKMUT+tRdY74lou+K56FRMcGjGjmOAzBZv
m6sk5jGnGHIVdsOwHM2/XwHtPM96k1yLVzL88Zd+Fs1bwV/gCVDdDVYxkkI2pK5W8btO6KpbHHGM
P9zoQN7Vi7ehATTOSku7DbJr22FPG8CHqyaGaaypVswozLvU9MXUozQ5CtGrH+A6ONQCGq7+ykyq
xQrtUw96ztlxIUsy8xpPrmLQWiCK4FKY7AnVLm0jyWlpwXn692gAw++h+DrBmiFN0T84og2lc5TM
V3xVKDM0uJ6tfXo+Xx3umk+T5ZJVFlmsIxMtHO39uXHsIVnHnFbgJpzqc9ohlOfeo/iNreNE87TP
+qd76PNOx/NGyP9pqZT6jk+2f0YLfLTnqnfy+ZIppZZ5iR43vJF84jjIr3eUVYS/xXB7czxypTSW
WhTZPf2uHj8tO9nADxW7c387WmBG9zgZAs9mZFSXL7fisG7CiPMniNnDehwxcDlREPLao7LQp3sp
ONJA+JBB/c6C+31dEj4h1x3IvAET9tjaNnRe1POc+//D8YVVQgxlsOVNj/23krZKKPFTwt9PcRBa
kd5NuQXJ9KQ7dSgYu/dJTBKkOo3ODXsLr/BnXrIld09HvQa03/OwLLur7yaPyMmMiy5MZTh2Uu86
0hrhlK4it0XVdTuWutt7aKYBOFe/TxPIYLPZhMGVO9n1wFE4eq2MC6zEZnhIiYUnApS/ElgZeo6P
v0kmeWK3Y3eZsCpqBGDNZ0bBe3/QNI6T3uMIUgUA+IVmVTHq8ZH9DLn7RiTUbsccTHK8PLtPBdSU
We2AlWe8WKVjt4QbEHtLpz34q86q8pAQ0ajTU8CvDeDF/uvVJx350agLsAWXBeVZ/qLpeJGLFm/z
l+Tw7A8kO4TPV69GJ+9T7xb6AfvYzPWF/OKNcXZQENlZzgYXxsQtseVOq1pEuar1EtupE7d12uck
eegDYAl3GDEJpwNK71iYNxiVpunoiSjqxVsRIZksO9Dt6Pzrmsa7R7mwbWTOvJ3Op7e4TBPH4k+h
d6txxJrQHdhw0C92iNb4+chb6oTmQ1EjEVcMHJm8GdsPegIm69YSXyCeTltNwMzj8kM44d7vAjAc
ok7JKvzfIivIwkPJfXtKai35XGr/Rm0tEQ/slUVExy7s8YdD+5aLQMIJVbXel1GkQ45hA3s87wmz
D9HbEHpKOITgEORipZ1yHE+tBtpHHxVRyVX2wtt2mXMXxs4zeDVR3XPM+RDV2iZtz4lowHdoYlDi
Zft0ltQ/Lj1q3Uba8vJehqZU2+jXBQINk5rIyStAxFxPAnlscjC2ayPGfZw+PxxgKu+CBU5TvI4J
J1nXVkJv3C9vwAMPkbk6tLOyoeVbE3XEo+uAlI1PmbDvqNNaOuSt0MTpAmlxwfFD2eaRqgchGlzq
WdeJTEVxMMV5TTF9Kszm2l8bifBmxBYjUc8yWCGZfaaMmLuPtt29UFapHiJaCkVXtrNLnVTnHZJJ
oZ3hihjMq7oGW5G/7eN2Dd+vMWUAVj8RP6Dj3MZG1E/pBoKEViJ3kI3YrZI3X9dUEM4DVRwGokU0
yHLd1gDBuLLikDxp+ObFu3RoOA80TA/3HXEp8OfwrVMhA2b7pJZzEFF094C3GGCMaUjAiqr2Wxqg
WpVhIBRJMYnB1c5qKuoEB6UtyvKU9CmY15XgNtKLrIr7Bw+9bwBIgZYpQbCGXwcjpwvlF8Xkfl5e
Yav7Ishwcvno47ipSpyhZJoyNRyYvUMBHgzB1jGi+IvEhxLKHM6mfpl70lDj8xgO7gsrLbiy3AL/
oA3krbuo9msPhmTOLkt5DKVmFC4G2eor2npl9h4fK5giFvzUgCppzjbD5ZIr6s/XOFGCVpCWr33C
iL56uFiLKsVe8cQCNMipnlWljD7vY/mLyCJ7WNvJqOaukUhYrLvJj6Q1YJTHl2lgbe3+R0RBqsYx
PJ2lln8tOt8c7+CDvo1v5CiMgSOsdIrHWnXfTf3B1Bn+jEMrT/dp+tduH9ITcNR9q8DGTcfyIyrq
w+ZAKYd9A4OLjmMIha05NGPq/RXRYof/LuKf/Wvq7F0YdZ+W2koNjM+jRrE+LIbI0N4T4Ur1OuUB
UDhezuyESKp5YIVWP1WuyA/vD2+9FT3w2QMLY5jZJ7aNe2JOREQyVCtOTZXDnuZ6wJjhRlk3xMG0
4vtSA/CIoMnTanTdyVoSRNOvVDm/BMlMEed+eKjO3PQPLysvDTn73QbFvyg11tmh6pMQOBHdO0Nd
05MQIo0dFM154reZP3jbHMGbBAgPSizDJeMLu4hiC8WjyZWTyUHA2codWwqONRphUak5+oQEGMoT
0EyHPTbmR9OBTPyWvd2lEGofjHuQybkZ+THNATBmRei3PC7zjcn4nYAcEaoT1Sz9+jd3/4ARnfjt
GQ5IaIGMrQW/Sxn6EbnDJ0X5L+VGy8pXHDjjh8x1o+9o8TppDnDSbxUnfCIEy5+vcSPhrAGzLw/8
fvQZaPN4ncmjW0JzN/VnLPUkRG3WfgajmlRf7hWLZE1oZ18VBL/PTzHavQKB/bM1JUwP4UcPPyae
+DiCJZKB32kvgsO0AQmjtFfeAAo7vUWbwROFxz3de85+AWwDpXMzxWaX8P3DxMCj7b51S5YvuXdh
hPBOYhqTZOiTX2wn550WKx7lr3vQoYXqgXdCWSe5epM+glaqjzpb+79xxxECFlZtlKbo9Jc3HgZG
TvZ1xN0CTV+p11wo7R3OBMGUPCNZpk1v4PdJqO1ZrypaZQm3TCVOMsLdlBMhm7G3uyGSjYrIFaeY
CxdtH7++hlzhABrVQPm+bQoBGrN58J0oXHV+SF/0pKo7cJnKZQGIZomKW2Z/yAzKqDeXy732MQD2
OhGnAYgmSzOxFApnjap/ZB4lpU1ytQ0OlKSm57f6GdMXScJyt7/HN14fQqeuORg8RPzHzU/Oay4o
F5G7697ot8+++AJqTPwoJJmjBkQvW99v/9fjGeteKSNYLLGKkK2zWcaUFo4noxQ2R5TMfOzPHuX2
9bGGMC9pCO8t2VUa1zKlWPm/eUcTBcfDgc5ffys31WqW4R2UJepjVD/tLffJZ6SZEpK4p7H6IalN
7T3dVTn1BCsWak1F5Pftzx46ArTXh/tjJh7r1E9uiaiu7u5zmrx7oPMgTV1z9dsySlHfmhTD6JIW
lde35wD/eTf2vVz4H0J+UACqee+ZJ6ICv9y0VNnZA2oJLxHIS1InD3Jpnc1/p4ggefnf/zo9nF0F
rs0bMSiz/veqRAAVdkneJrwndNbAs17IMi/iEyygRZJBZI/UeN31n6RNT1h+XZQkBNG4WYXv6qR5
PRNFaVzbfUXfeO6xeV/cHYd/Gx1/2uzLNzzEysxdi77vHalbOGsBmmL7q2gFjxnoAN+MTi1trD8u
WmRShTnA71NSIYKNEkuwMqkETQXVFMlv+ZpR3JZC4IuxEzTSVYIL3EKLCHd0ZjA23FJ8oPPyjhid
BATc4TZHLEGPmZAb22aNBT4yFXyF8bfro6+LSViQ/9L6tif+aVMOAE4MVe5h9bw1nySdFull6rXF
SaDAU+7VwbQBzFpXIjTL4Dbf8dYcLS96/teFmfFth6bzFGMJmIv77lSS5if403DnpNKu4LiWeDBy
y3NkZPQoisBqWFSC9iqUJ+xgG+McJK+itfAkUOde00Q5Q79zJFOxfsXlbcljKbuBZgeLkYB4fOVt
1jFTEBybdnFg9zfttHyS7C1iRtPblZ1qcYdIKOLPR4Y9B0YBy0TT8OncCFqT/o8+GdD4nNev0ITz
++7CVnku75hUDMFA120dOoGqVBLCZohtnlnFBlqrLh6/+h7bJjytFIk6Rk3HRgudDkaE1hW9v0DV
WdE4jB2k+LnrTgfr3qEQdNHx9Fvi0SiXAfGF95vhMYd9KzubK3Osv1ig2kcv2Q+aFAvOTg6Ym9T7
GE6ofZcNrHNc1z94XC61CHn29arPf5ACaOLAum9yRXAb0vl7VW4qPjDV1gvJVIfzl9QEj5w3VZGq
ct+VKlEqaJXCIsZ3gtpWq3ZFrRB4Cfy5NycGXgJUf2f65eAE068MW297wyZbwJAVMBJpYd/+yEIR
IFIquJ+yGV7IH9RYojy5vHuOxl5s8VVabZrnsvV3YUrsChCFeWiaQfuWUAT7dK1p72d9iGGR3xBy
FkvPB6QEY1xFtEddE+Uw3Z7z9QI6FQj3iK9Vp15gLTieS4RxmSQ7brt0TtFUActdLNLeoRnsM4Ta
TIDUqE2LWiOhkxhkRYx2jFBP+jz/6ZKmGDPslFlGjrdZRMFkXv6RpdRFjY1QkhIR+De9zU35jVeL
j1RW2dYtoclRZe/zFcA7I6co8frx5nuTPz8JHsU7uq3kBwInwpOeH9x0isk8AGEWdl5yQRPZvrl0
IuIFi4aB9P0LsYAb1zAzZxUqW5mKlD0gLum830GWrdcPyzf/Lo8INgIuH2bHyjNOITJObicvAVXh
YGQmTTo3+N3ZTPIRXtjI4srzn4pHHejg8ZlR4b6viKv97ILTAUesIgvNi+tqXgyBH0tWmwadPiho
vj/QAU0Z0Rh2g1Tvd0WP0yX30gBKTJMVTIDep5SNT17NZOGhE+iraw7WhvcXpS9n0Xa3FSxNJbgI
cNKTkUdh1YYLEfE934vJ2s5pTuRuyceYc6GR1VfMsQWl73KUrf9SQu2zH/8sCCtCKHOwRnHbOrW4
64LkNXfIfRCFHxKbOa2RBGv3uQ4jC7SGmvYiaEy/B/kf/ds7jj6e/4B0gcS5kK/GTxkRAAdnnJbu
+F8U4EKdTunTwl2yMScqUYTJ29iVVg2ZhCDrJUu2H9mnqKVEJzfiHb1CFVI80D3CVZbmaZcP2zlD
GrPy4x2imYJ7qPvDtZGkERNJ/NSETdqvs5OqYObxQaH2U+knr5sIODNZc5LMAIXZUplxeBy0u4ST
Wp97LJ5zEG+zC0+mY4Qb4qjY92n/d9F4C5mv6ETDjOKjiVtYP5//obgsYzxPJJ4HG+hDgXht8ZK3
CFXIPDcdzxzu/iqhXRJpOcpdH+DO5vkBMKumWPE2CM1bTjVLYoUJ4aOOBL/wCJNae6cDx3/y3y+5
KXJT1naQXlTMWAv0NrslOrRB/kDmZodvVNs6vc5Hfw203R1lGHJErBR5J7SJDv9eUDNxv1iXDvDV
VNsZkdDR916hatk+thfWrMQE3i/fZFg5jhvGjzXxbc/VLNmJOK78FCdTYlk9cBKJgD/6kqgMf6Z1
PWyv/0Zs716BSW9SxfDGSoR3Kcl9zX1KxwjTNfjbmiHl1ppHFQwwr6tXT/BZ4vFdoCo9l4IOXv1x
tvrDTnnXUS9agvHwL+9Ssyioo5h41LSPNeIhiQrBuWG2Qq1nLyoeJ1ic8y+n4pwzuygXUEz1pzHy
RSMjI1PBmSkixGIwZi3cP2zeuHpeLLXfLfz3A6SDr3oFtGuK8yBWrYO91uhxBKHSuSX0G8vtFJZd
YHAwc8zKByf5M0aownDLYRa+h+egyY+l0dqqar0XwO/RLIPCmZJKECmt/Rs0vZ6rpGrchIBKvIBZ
2UScW6jNssn+gVh11LvQYjlOCQLpSm7cfmusxf3BOvdyyNSwFbB1Pi4W+2XlqsQZCTu7dE5kyNPv
1n4eknD+2pdM5TYcP5k2TcJI3GDMbE38r8XZtnQRppkMDIpVQ0T5KttlEMwebZfhx1TK3/9iKfQe
oHOp3ghGWPxJ3V/fnwGxyzbmmI6j0Cr4DuKEOR19ImgKbUjHH84y96f7qUyT3alCxJvY+WgX5kV7
4riUyZX1x0zy4y9RCAmImkltvwGDSGPj5RqVTROCMvHmkMSWAD0bPZrijTTOyyrDe64vDd3kJIn5
lN12AF756GyvIFMkm969HZu+EkZPY0ADFmWIRbz2yjVmcSvcFGj3UD0JaX8qTtreyAq/iS4zqjMr
ZFkk+fxQ19MGt28V/C55q4tTV9mwm5SzGycRvRgdcOKx9ad5nykrW6uH2eBeAO1ffQLvrvehfyyg
rf95+E6maD+07n6KV4Ym4Aui6DxsOgnht56eKMAip+A3agx/skAmgQa8gZauHmIomJIjAISafy/B
Y4OnFxK9or99A1WdqvwhOlHPn3vjKcqztvC1EMzXDs+I+CEktXG8ViY3ow2JEvm19i2X6gdEJimW
OTc9dMjEQoLqKdOJrSe1zEYC7Y4CBI4gEwWFNwXquCe5R5MgwDcewC82IPTt/vLXR01TVqj2tigO
EV5CndyX+eJOJDT+4Mm4WjKUewx6YGxkckdjvQ5hk9RTZJakQFKQnkiN602H0SaX13Qri0n3Ypt/
kZAylJnyMG9GjJce03VDkO8CovwXSUeftb2f+SK2IedMt3McrOAmWnSVKwoEBwMmlyVrJd6b9WOQ
UiyeUOCF8fWWp1SdNzpW2FXVO3i/9EMg4zD7XMMSZ150QwHYY0jZI+Ol9Cll5AOzblKtccOGjsPd
wi2E+hYPUlxx07bQ38jYFUlfuxFjBPTM3lqnb422tAiOv9bZI4TbcZMczI8PYVaUvbv7/XZCgU7Z
9ewA1lnoM+Ob4ZQUiToY2Jum4YKoHmdNAW1Z3nT8Y+HeqXMOaxceePQ7F/u+1I2AWr1uI+SYq7qX
jSmm8N7jjRFUa4xPDNrIoOjElF8nx/5eEmqq3xULFar+/+3nVebkAAIOlT0KSUEXPMFBhtzzTMAd
ObI1PQFCYd2gTjQjzxXZ8K6U64Y8sFKnydrwcIprH1+ZF9ecicv9FRf8qIjC+cMuUZO5tl80UQkQ
F2/mz61N02dHvr26BbifguVr1IThaEW2QwMPnnQQlmQ+Lx/VuVGzrZCg24MtkDgu42x7HVOcz6Rr
PKMDkNa1V/fu1kIX40WE3fP9nJg9KKHJllWaxSrZxWggV8G+T/h4l8bWI6xeefTuiRJdJkUj9ogi
YxOU0n22U0y/fqmmou0Z5uq99nNTtiMcwXsielEjtwXqmo6TfN8Cb5O6foFPezb3lrfHvsWv78+W
8GHVwtHb3M52LQZ2kIavcMjN4fbps9JojXGXJHAlWq1DuwlsGhU4UrluyewLGy5uhC1DnxpFle8i
Hi84yFYe1Oc94JFeZKD0zmJzGh93MTp9I5x6XfesvEhZMt8hxgRUZBNRQswAU0UYzFJVibh0owMP
8VY1LapZFXxfHGEnsWg0CoQcT7r0gg693d+4aRjePCqOdotYY1GrTJOYtpQJJOuJ2BrBCe2Wr5vF
jfuziWByfA9EHnhSLaAH0hj0AOj1M/HXtvrnrQPWxqaO1lxEYUbebr7s7uuAX4ZmRhKR44gFUisg
ZFoI+cWMU6anoBoJgkYBJ67wqPVhJPmRszukvj2tSuWfqiQ3rWNRrut50AGZRrpGUcLjAcA4gkeS
UpwkmY7ktQpkjgNun/kjVfsKyJYYLp3PPcv61y39M72g+UqTXAqfNC3ecKEP9yb8ca3eLTkn+HWN
M1r0p1gOpAtDLf85Q8lsBWTX7srW8UKX54OURc8CwI8gsEROq0DbIPfry9+1gsdLXiydVCfKcqjb
1+qgFZCnEVon07T0nfL12MRtCNJfstVNwO3UkvDFki80hxIRjXrgUCHMMYS82Cp2FXvZFSjOKB+8
MAzlMhwSIoQ0gPywJGL3gGtB+BOUZr77r5pbsofVu42ppflVrvcGeSyAjGODmt0Ere7fxnQbo7bY
oXiTBFXgY+10Yu4eivv2vUkx1JDwuA/LjHfbzpi8BjoCn41bJC2HcPhtkGkPuHcaQPHqUfEQwct2
TqB1rlJJmAZDk19ka7vXTUb7Ta+qEGEzG5ZPE19703VwLQH9b4RuZtBYb/ISXGyCbeZ2eAgnJsys
5dGyw03/3DH6/AYExV2oph7QUSwX7A+JKZYxesX4VFH+CTttByyJWGHhwHthqec+ZRh8n+wR+YkI
mCQ1b+fia1DMx34Yv9NNyA61pXIMOevVmE0AwOHVfhkuAxGEwwq9hbyf3htAsPOp9qkcCCVdQV5i
Suot0FLySGgEL0WDAl4uZetoIRydBaWQDuKjQaQzGm20T9Yfd5fcu5TNNvPX3P31dS11/iotLuBs
z1TFizp997RP6RSNvfZgNIWyomjtEit8SlFpLS3iy+rwqbssG53+CPLEqG57XnsfKpb+Kf6oDTse
ifX9y+BIgZwgtfY8A5HnkvEMhgaibJsfJvI5DtXvsBAZr86/4ZYYbV/yth9IPXl8GZyuRXu7f+bm
T6TZEc5X4ZHlAGDVQ12jwvHIGeacSytw2FkW+05l6+XvMS7HNt79cCtNQZ68woErUAkUhY7dACgk
dtq231hE0nBcg2QN6Eqcyznf/C2JnnUNWQbekGUhsv4+s9zWnycy7JUfjuyQ4KRR9FUMsmsvB16H
ZmsNbdnvJlG4/t0yasHQ2s+zM6W/aPFh/oJtXObXKCTSsS0qu2gi1I6wCsUOx4Jsk4aa/a0KyJLV
2Y/zao1mVc/KL9U6p+m0AVSywKVJHBUlItIrCBAtM5RdCe+UDFVHWAwRAywkq6G2WC/An/ruv2j+
F2212NYI3BgCon0wqVTlKIAtCRVGLurQHoYHt5DDH7/34x9sWk+iNoHdjoWZ9P3dJGKlloEFBJ/z
4aM1T2oMI8gO553FBpdv/rlPc4HtY1VTUS6kM+xReHrsW9B55OHyN4Mx/eMDQ8VhylBp1qtlkzdM
oVjUmoQgBBGSRw2vznQAvqktyeW8EV2/27NFkdkV6GSpm0NkHwx/xJAIBBsmtLK9reMc/La5qo7z
EUd2Sh1mtwu3NtXeJ4xlyDKL7Wm8tAufUocq67215ZxqvuEjBhlxKTnJTLSFuFNcNILicLXdwyPO
JRR0qdu1Ves4XOxG8iboAgVCGrsjzpHTRmMb+IH/3LOLMbIRT8OFkee9JgHIBiHAJRltMDPYk928
V3iASW6zoqHNCPgCLdTpkZ/q4y6UmVuwT4OCAQKXmMj/qA1kJGH2k+tqkFG69IVJm0nu9SaeY528
/OTwaUkLX+8ErkZm41P+QJV/xK7Ezfq71PouVLEaF5DA4jwaJf/4ODo2kf67WmtMgYHryeBQ9fye
p9IVZTew89Q5lDlEUye8PrPQx0DHtxyu0ekMG6I2+ZIoWIKrjgl22d2EuosoEqWSa9M8gWSA7V+k
7sFhvQpRbCEGRtNCJRqtjRBgAMrx17+qFO+Fz/w1B/j8oelkIYcKUClKo1nHmdTAtHNrK3eKQSi+
zYADvcoDI7E6rWTVrdT2fu7x687qiTtXriHMMI1k4pEekjiPeFmgGKSiCoTu8n/xLHN+hKSXlqY2
p34JH9SwzVgUxXR0a7mg4++8YR71SsI2urFWbZIinptG8jsyePIcozuTdCaTo5/ia7cJz5EpOb3G
E6L2yipkrpfpTGcJ22XZ7y2/yDuY1XcjHjgDwCP2x97mKo0fv07RXw8gSggrSSctw6BGkXAfNtKO
OXsXRVZqkoRs7on7tVmhq/MV+jzBycpA2+ERjVq4hFzGffENuuBGD0bnK5jvT8EuecHWWTjU6CHc
wJ/hVJJwf6k1JrwCWDeNnwIOCm8/5Ew5xBjd424aNGhTtRbRWLZXphYSswIP+mOEAG4pPMuK5qU8
NSZrEyqwR6XYUo3TekwZqlsQfJrtNB4IAyIt+FEXWUA6gdDjQDo6K/u+srIt3fd8oeWgW26Rof1t
L3JUD4jLDr8rj/leue9pqnlCEfyq/rDUX/lL3cDJzYOs1pUGHEv0bmW/OZOhK0yTqBKAH/jfBZKC
wBR8Hgr7foJ/Wh+gcJkFFSccsCgbXR2KYXHUmp/P+sh0bpY7ogo2iMB3l13vl9DxtFvtKkKuL9VB
+Ky3zNRCnXLSX1YM39jabjJtjXA00GO/KGftbo2Z6EENE1tuvuaGXm3q5NvLwddYdf5PyW5bS6s5
UQrYWf03vKm8hVNkTAAzuQ7d2tMlvMOUhnxNvfqq0oQqOj+qYuJtl8Fu9XPrMFyOj5jIaDbOAmgl
0VgIFx8ecmAujyeNpib1qEYAP+GWOr3Eh4CgS40j34+wa6umWV9TUd8ODcnsCAHhGW2St9z6kVFE
ezkMsQs4pjIJpKssgtuG1Gd5Fdkjw68SyuMYUzAI5fTAmvAM6AsLPY7n7RL251U8NnwC0HquPHRV
G+fCxB5+TavZnQG8RA33E3+sTaetV7Eis2YsI+vJZj70hZtP+364uWyfUYnfZ6Eq0NeGOB5u+YbQ
YI9WGfcm11TSHy+MkDzqlPOx5Cn+tOLvkWgVVTGYQzTuzVnX9HI8D/1eKWvgHMKTYIEgzYy2YmWL
IAbr1sVVHzCui5kY4WCl6H44cMA5ilquhymQbFeHlJanhr1W1RF3Y/8zh6khn+Dv536nQQwEQgwz
I2ETx5+E71mpbhRMMZNxqoQwNv6FS1Bk5zxNoJzMd91ne8hCbF1H4qpWuVB11IJ3bDCWh8f6ni1p
XdgiRngl3fXnCFXKbfYL6OuIDPL8D4SxBlOdXeQ/NNWW6tcwMf9IX/0rwyPqPAQyGEk7UeIGuz4q
qlDQp2c21iRUhbhnAhS3B8Atz/Gur/aixMf3qTBNHMlMKhEs7GtCREfsHIErzU3ofPSo7VxhMZhl
KKA1KpaFh6VesVVPvApJVlCG/Tt5oK1nfM1hiwEpQOKQJC9I/yOtXEmykHj1YDpeLY1/jLlWN7zP
5CsPGQ0xCuuHIGvdX6dW4RXUwjHxlt55bGCXLsWnC2JGhTGU/LoLc+YriCIBJzCeV/PKe9IHMiM1
GUTbhxvjFnWGueikLSU7Rjiqv5tff6WHU7+wHVNlh/tjp4KidRkAP6BhElwDb0B/LiROGOrims7u
6o5fmt4GCWp6UKBMKm7/htwyYDN/nDvyav66S9R5sCPiVO7IkgyTQjNg6nFFnMsM0AY1lUTJNlRA
RpFzEXP/H2ozJ8XOIEAqB2uekgZsYUDgKrocjxg5HgMrsFrGHm8w43CQfYfbrfQ6U4y5LovoD26K
nCw0MuLWKlyLa+7qCphBQUYQ835X+KAGNpv2gGPzE5T/ZySsThPgt95z3gOu+v8tP9XHGICplUXl
swnN3n0OqdGCT0koV3PDKAB1zZzkAOR5/4sCeQOQ9daqN1iTgMfCuDiCArK4XvcZgxkJTEw6TGw5
MTCgEdae/oqBNK8tp9hVX656qHN2dvu0x+nRgjzp2B2cWgQAgfZ8mHuge6i4kW98md4IykshvIFk
6yIYAiLBCidSX4y/Agn1YEUlX83cQqN8J7bAdkacXLzTJsZKLBl0Wq7sYI4PqbOPqFKuCIdHHXj3
LjoX5apv+hAHVdKGPlP8HxhfU+rA5l9Qr0N4ssSD+lp/KeSiPz/eAffonZtjLJWHdA51kBVB6Zmv
Z98nIzOUCX/cB03oLF7HFaO0tMTxnOIWCb4Shpa/NvITN6PJzejqqcKXug9sZWXFuOex+oK9Xw5M
qqwjYGaz8HpXO0wAJ35caVBjG70UScCAk44AS2ewiqximgvE5FFulySS3/cp8a2iTp/gJU3JUY3h
OkmSAi/ThTF7BTK92Ui9j5imN+l+MVtAxKn4w6DUH7k6t5wZQCB+756UPWCuLTrNPM06t1uXlpE5
dyrGhHPS6Nk2cNMkqmwdQ1iujTFrg/aYWgNHeLeZAvbNZlkDzReRDh1wv3lovSzZUfU39SaOEXnO
4cT5nd5GvndtitMcRNGnD2REIkKS8wSVoku+nP3Ne14hubNctLdbnaG3sWxBC61lrMN0oc5tP6DF
mRh/VMCbFFDRw9kIEFEEUmcCbsWxWzAyWLgnFVZKzJlQzu2gj8fcRnXShnJP8a4T4xP248hBEyIC
5Xgvw0+JmhW9e4e6bTh/huZpUBqthqddSrAo4IcjeTZ8QVqbeKbsaGPhnYiP1Rqq0mrfFaHhzs6R
Wv+36oa2M6qflCYYg/5ggWeXQZdzuP7FwMIp7VMDuwvy/ajL3Mc8xMABVGHEhxlYGPR1Do884r6E
lp5/bSVe61shB6ZhS67nX9X/ODksv8DRVnwSmRju9xfuqHw3IVmYSrw74flzAt4b6vzVWB9K87+e
zEktWJ2hF4pju7K/SQLlBPrdGML2RnHaUMk3tr1fgC7DVhgeuZJIngh/AO/ThaR/5mmiOiMP2D0f
norNfPAzM8PF7oWxxXCShiBj0HPzhES9lNXcv9BXazky/zwLWNHUvQYI7DrQu9cIjonZrPs4lEKb
+16r2Icimbb0jum5iU0sZVtKPht97UiwdCy3rAzOEXqQo9G9iOb49Sk2jeDytvM4Zc4Y8tkoWo0k
3FzgPd8t+L56QA9y4DgckJkkIa8c/cIyFwYX0LyGJxnPA2959V4Rz1j0XZwnVDwXEdkOQ96tZYHG
g0ADKpfWFhximNMUUX5Lfsa6gitCJNc+wuxbF9aV0UKljU4zyyJYD+m7tPF/uOrefKYvNwLCogcp
3jLTgS4sX2gFkSCira2KwNuApPAQRfbPBby1BaPKQG6sJDFx1PGFKcFHxnSAXWGCrRJipsMqML/8
jg4nlDJrJhPIAmABTcTXfBR64hDpt0iVW1fE2A8OTBWE7NoMD0TNMPZQxm/fOpk/3Ewaokz19bMS
VUTx/8RfcGIdu/D3q58035tSV2cDi6MIQBWzXTvatu5H+n2b8xw/TgeoUQB66YeLQO9yxiiCigmf
oHTwyzzCUF+cQ5cKF+tW4zhq/BAKkdgzVtOZ686wqzAwS9K4YEcJnR4HCvzEEJnguaDbq/ajkY2t
8eYExaXHdfNHfxwYZDF4FQgdCt7Z5MW/aSp30SC/ysNXgwg5+YjB83LZn70iIUjMrWKYBFlucL9p
kkwRcw0/hI5hw0Yuy+5TUAXqbvDKfCsBWF0I0tiHLca9vMzkewmwc9tHC/9s4L4OqguiSLcykdfT
x8n6aXiMyKxE4zwnVwRlCYJTmel+llomPsdL8bFidXzSM/ySSKaqnmTSmgtE8/nW6A6L7Wc8ortk
oHCzKg5FmWV0FICusFW860V1pXoDWoG9uyhaeabW0/RQbIHJpSuCjf49lTSrL3HIe9w4Ao1nlHyX
4kdLCIjZj8mj5bbERM6p/SeWr42QqkYf+kkZusiygE3DcvPzWiJRjmx6YpP5N4R5w+NYreh/lPkN
Gm1FcuyNJpxcS+ddYuDihRXee12XvyV00gWl87FkUS4gT/Dx65cWn2ujcdkBxspcv+BcNoUQrIa7
KRMj/bJyLmY6cbhgCj0uCaqHF6SP/VE+0jPFLs1+Zpm0Mj47u3x/XYvdkQb9Z8vM6J7ynPNMIXP4
/8H+AWMut2uT1EiSPDsVJsu7CYcEBEL0fefQV3ADNoo6SXRH8c2WmtS7XTTzknhvFxMHC4cZ9ge0
sPTzTj1U3xwo2ffo52V8ljeoUyodXnHr/Fq8qDDKNqPthKMtysPyOjcAgx+HAlMmnUy93YQqchKW
o809jG1d3BIapPtv1M2asB1DtO33RCJxBdWdLTgDbFliZGi3JmI8Mru5F8TldB4ATlMNT8uqRDqm
o6pnxDqNm1Ecw3dkFlnwgaQ4INRU/OMkimaucefJAZC6cZbTaHlAHTbxwZxLloQYFt2PcSjSzsma
vod/QRM79Ez3YiP67CZ4R/8jogotWUdb32aFl6Ff4ABMPdqvCmOSQ7etIEny+Wunqj21L+VGyBbY
7RYnbO/ntV+6mPEXH4AmjMpVFx/2SA66+r5+Zw9CfuqbOtxqc3mHAyeV1M+bT5pURGOR1k+szH0M
MovVpgdSz3tDNQ2S55n4hfRFrNQRKybleOon8owkPOSX8Dc+TBFny3LgGvuWvxKIYAc1uKZnJImb
C0jIb04Hz7qRb4EbYXomIAPkwmbVWSG9mkVzyk9Eu32WOK4Kue3eFTGBZ30emlfMOmJdGUuUQlKJ
dTa3B0P9q6DugmREGBpBtmCVLtPiG72LhWRjQOyNmpRcDbioLJitAwu2HbnH7aMG45aCHhiwH2yw
pgcmUvCepXMPir401VZXMfhKcV7TQLogo0VQf1ZwWH2UWLoRxete4wqm5FLn1dsVi/PbNiXJDjBX
tdIQescPtkyWn/09NgkVemluUXlB3gBAZ0qprINxV8pyn80aNeGq3YEJJGlb17FmPKbVmsl3mzjO
1spMV7uZajB5eTFUxnXbCdQWjjCFQNqpZMTCbMeFBlvQrUVUDdaKVNroIJqLOP8ivKA3A0icTcrz
ipQEbsL8841q7l3vmidEoFOpSauUz3bwX+/5rel/SU83/aml6IssMrBm/wCjTHRtFfaA4A7/jxmK
JQtIMz/Q8DO6AAAMTvseUExMZCBfzrdX+ivEM2Uo+MEuIH9K+fXgDOoNGXE7gbwaOrMpUfNAO4i7
xQ50OGaPfUCLOvQHxtcZX6qpdQegymJEqdOnUTV/8zSH89HjLXR6Z1iqv4Ni+N86E821MJgd81h3
dyGQYqppxcSzSxT3XiwKs9aFqJBxMoVS7J5K0jN+EYC3gBrr9LP9witMyFVEqd0HuijZxXPzZZ9g
nZ+Dhp3tRbKcv5dB/pINq34z6JBEykIKbhOnm79UKMcjQijlOvo3E1FylSFfra2CIk3TJ9vGNbAb
5/t/XiJ7YpJgbTKzPu04wiVA6kPAqiLhg0Ivx72Ic5V6raF1YNCwD59+gMwspeH/BbRWZg6o0PHV
my40xzXy333kgmDtNP/rmwrw6tbaHEKmVRC52BGFVLgmO39NBsNWoQlhDNbK0eVzUHzpK+addqbB
QKcrou9TrljsAKWUgt8YgVMGBh2MaBqUOzVOC7JRRUG6k8SYOj+4ESwk9Fsv6l+1n6EP0u3GC7hD
zkDwDJerSBLxCQkovm9aRU5YNgd7EkIeHUI6nRqpY4wzlMhHvJC/Pz8n7DZf0eL8g59x1RuuuMVz
g+8p+fj0ZTp1ArQHb86N2taPaVCwx5H37FW56B5AmvL5CK4SZ9lf5GuxNOXjg+5q7nvMuZE2B0Ci
gi8cFqur1zkUoiU9d3/DNu7p0JEKG093Hy2ElX8z1aqLa3QhewkB2ibuXjRo8b8LuKUpJ735Km2W
yGinH2fRJGr+9RpvhfMiLBWVYGqWk1sr5CjpeLeWnK8AzU1ZdwOmP2bMhqDezkodFsKAJROgh0tE
kACsN8fwZ/jUCp+pPyEJXKpxG15f8SJtE2mGo7zzVlTF0PZptI7uW4YGWtAkXCAlxljv9YjXj+zc
IV352oevnYz5sMlxL6kq6k/9eGq3n6gFwm648wOgRReh95E/8J/HYGEHaJ3yKzLS5F3CINJCHd+8
+oC6fNcCuTOKNoYNeIo6m8rz+lXJCSMBHnuPMxcZx34fw6Cn6q07VstMy9pm9bQKA2KKMjriHG4L
RyAHmi5AwchE3h/IM6ZIfdjaLMKz1TT0mtugs7A7U4K1Mbgt94yyl5YMgrG4JtTDJRBjk+tZUROC
IIc1RipYhavQHPD6lP8qZQ83RmOdr3J6DVvIOsJ3cI0qTiej9ecpsfXLl0ZXebuiI8vnLLn77ZJt
FpNG5T/ktQ5h9FZ+YEBVU+kC1TgZV3qaol70Sv5SKKk0GEEPqHyuDsSWbRrH/uJ0wxFzJxyXPKI6
qGaWD6la+HXAv63gFyw1gn2NKbHb/J4tf7bqmoMSgJ2Ec0CctgF9C60sCcTOsAQgjA4hf+uSGiSe
TBC1zoKHG7mUHf0vlx+TBAP3FippIoaPpwoDCrHR24FsyfP5aq88B5eJ7qzTef8ubkKU4BkZwn7d
V2EjQtu1fLtJ77cLV96FYOH2NpI4OfMw7g+aRxlGGabuAxUYZoCsM6EnjKP1BW1cBMdJdRPGjzoX
YchSuQDfnEU6njqaRX4plkB16xwSLBJEvS2qbWmyyWUoZU50+O5gXA8qa7uU0xE1qj3wbQ+KU1Xr
JSr4hUMB3cE50cDSd0nON878FKyI+cQNFjwbmpaEK3Gnb+4aXP1W2qsT7Rba0YY0Gw9qPA8ROCxx
Wm0FkmsTwfzbS0azpe11eHakI0amn5wpaQaqF8DEWUO3Rfz2819Z0g79tXjx83JEqQ0ED7I0IwQh
7QBWasC4lwevcpuHBIlqzm9NP9oZnL7/goSAyR3Hx6Vuj5gSrGsm41V7mv5Js6cCSvGSwZRLnVOL
cPyONsXSp624bPg9E4Feb5LkZlGe2DJSnZ+lEzXU+oFtYic0S1EwahTgM3Pp4DB80EYNTcT/Mysq
FfboMIgLDhJZhxpph2f9pHK4pxVCD+pNcfCXXGLzlYkMSARPMZJNKZluh8Cz596MDIqpL+cSUiXP
fNB7kXEiLR3k4myfucu1JLajG0m43LKzr9YKQ1iUf2QN9msMofqfTOHxJ0gKybLkI9fouVVcm3AI
DVHVxbVYmHuaBPjKAy6emLzeIMMrYlCGHcX/V7jk64mBBcUF+X/piTdRqhBx4oT9oruKt8AgaBR2
BuYwFFpv6AIC2bJ1Jrn4l8Nm/q50uAP1FMZyTnkiTAAq5AFMKL753kTE8cZp411bc+KoWPyqjtBy
roi9a+0nxZUWwQTge2Yqcg7l8GMNfwpRpuoBi3B/a9DOJVpBspi6vgC/BqFDD4HChrYi8V98D8K7
WVMTWDT+euVBcMtALZZPsih4Y4vqXxUALQ8T3eCOGDz9QkgrexJiP8MiAXx+nNhmJ5liV8tYGtzC
gkyrW5ZBdU5wMeK5ljAOX+ENZhii9wjnlXZfvRBbb5xkjOGU4AvQACaTrewm2qdVhsiNLN8yZdPW
9+tA4jUYi9FTdmX+irUOCprHXjQ2TWGir2xifv/szk7t1W+njRk0owRj5FV3bMCe2vBLmWvM3Zw1
mj9G8EWIVGPFIdJhrGFdNTGO5irnxwT+IkuJehdIgTbfp1JZ/SiIfu7e8EBbXxJppq4k7kQDJJFo
rNCjjhqfhpnX8CuSAaNpqwR/o3Pr2/TmuBkgFKttsExSOvnTtxdgVcim6ueazzR7IjxHhDvrvkNX
UQUPEOD7WMvyiL8ls1LYEB03IPxlw5XjDTuOF9nO9Plrl4WmJ2XiQqI9zjoXHuyygiFS3OgZMHS3
dS91GUmg8tLXEPU4CSRIoDS1zGqmTp8CII3k9C+SMdPEypiEyDuEchpw7H2zNMkfZGVpUm4TuzEF
FU64EsDUt4U1KmsGZdT7ygBKJlNTXfp7r5dI/QIozuaskQQC29nBKEtFiR7tQkGVz9kVdJj9gIJ2
zv7OinnIvATfu3rMpY6CuNb8UBd+G3y4ZT7qB9Li3qlcF7zxIDcYslRaFImaf9ILp/jbVUMnUnyW
rzs/avJc29k3F3BLI9K530JDtRynmLZz7Oj9je4287LuUPN9H1EbcU0gcRo7PZP+cP3LR5Cic5aD
bd0nC7XaVu0CIE0uZJk7N2uXXRWzFs1lgTDvsagbVugKP7pYG0CaGGPFbkEO7E0qLGM6Y2Rlr2xj
Pu2u7n79fZqVVAbBS8Cgzd+VEb+ESNnmruDReXprnJwWbtmmLrZmrFa30irjlF6mJc5XJUJwcds2
YRWCZD0LtIYCX7+GaqLxWa3xq4mMzjToHjPsdTybjvGUaM8BiMyuFaS0lyZV9yKwC0I1hm/cJ0Wp
J0pHDBshhKEfrWwDuxrT8mqZTynO4Dyo6Sq9TsOAysNYznNjVobPmpQ3QLhUsN9ztqIXo786AjaZ
rENqe8Vnc6AW4bCcAElB8I2E0HQJuPVOKzUVFYcUwICmqHt7Xjyn0pNm3TasyHYZOvXY9QQ/u7V4
dUFeqVeHG7OucgmbDt2FqhxKHYcit51Vkj0qzyi5W7sZajOqpMJmd2jZcdI/gZDut1Xx1KmL68Zp
2PEUgU6X16mpbExJXzPdZCHPc91c/GGQ95wyVfLE/C2VFlPTlZUnlkYCUXwfCd8Q9KrzX/j9jI2p
pErMYxwLmotoVgQ5YcbX8lwVIXO7xfaJfoFxUv/mVSO4PU0LkcJSQUm30u6oMp8O/mtg+C7Annwz
Mzoe1k5MfS//9u1sNWyMRrFWFEwsw5wA0x/qqF6k2Q1L3FB4zYbQuU0All5f9UyAToWLWF7MU8dj
ySzoQ/B9yvVJwmmoCs/eOr6YGdloDXZWZwmVkbW02LPBvdO2VvtYXIM+ZG7DcYFVyt+XJ2ZBUOgZ
8V3FyjcaR7atRwTHTjtkauXFRTRHiUGUz8fo3tp2UqvCseJYnneGRcze2wMEQBa9UJ6ZEWzxYWzd
j6MIEMIn5Xcx72t9diflJNhhUA5q5k3jD5emrTN9AsZB7/F0K0UNtwGX2djtzFOcaka42Ckv6ip6
byZtwNHziDTYiscSLjkKD7WJ0Qz9ZpG8SO6cDAuEf4lph4ivGou8dCgw+hGeOXfUjHyVOVj6vzXR
MrhltIhoNeqA+07mUHMiaPmKMOcjBm/li2p+HbqPZyuzJicuy+R0OE7tur/zNv37q2UbmioQpwQ8
KzeWimvtctaz34nPkuF9RVvVin9dtHv1ONGhmHvmQFpNJS8e+oI/ngl5wFYq6nrMm5NgGSfW44bj
AqQBBQZxcrL+QbJN7fuQs1S0U+TNMoBj0//QythbepXArsYLrEmKwsKbud3LTTXICK4saZ5b9bRR
SzQ8Sm1qiHNgtmeTtftp9nbvle27o6OvC9ha55leXpKLyNsOWaOAZlHs30brL5OjtFq4u/m30suy
bVgiaBnR4GC66/G488KXVvzIDu8oSGSq6B8bh0gGqvKVc4uOd11hzKha6v3RtomZrcSdrcGIO8yP
P1Okf38X9zsZUrprRplW1KxN/slSY8F2mJmJzwH9psxpRj6toz8/6HrzokTuaKN8tQTmdbFat6sZ
FbunIY4PfuuobIwhateJ4Yjb/4ZF3rlBF/maJCppLUZYQWbiOiPWTeEGzeiEIm+8uwPPcQNyUvuW
VEWMzObGLQJ4czGsvyJWzcv3YBXVoYeUw4KdLUpWnzYqhfpmd+rSa7ayU+gAnIBVv4piFcERO5uP
G9hMYL4QTj9E3uu6PABSiN49eoopqne1c5QKvAJ93FtczllSxfSCXbR7Te6a14bao4CEa3Qq8Cae
sQH1/zwlaiL4O+8cAfJG4u1c/nAD1+OilEC04jG1qj3NS27aRd+d1C/wxC1lP1xwgfr6B6frEILs
Y4Y1y+5t4KBGiYhshrAXPpJr7P3eeCnmzHpgOct/uqzoy4fX1sceZy4efYX2sR1oAMiRxIx+tLpd
R9gKhGDYwAOUpilzBi7j+jor11vIuVa6qzqj6vGjo1WPxo4hCOlAA/aYzk5AN1NJ0xGGBEPlK8o8
0wGXBu1tvTrrPAsG8vNZjFD/rI+cvhJLVX0mhxIajbk5bpvXFAQoo+PCXZ2SkOKmH6mKHgbyPKgR
5HFeirLJ6qQFi4uEJZURmo0h1dbeS1AebkNqtj8f3FctSfaUNEnSVi23Ib65fZN/ma2WWO5Sm1Kg
uHb/aTl2nVZmG7XC63EQEWSLzIaLcGtTN5MM7K9Ry1ffpMfbX2UwXw0ztx3n0g4/uZgSkQgiqel6
I19jyK4QViw0ckZkPHFQQv4RJNe6AVwr4lozl7mVLlnGyqSmeBIAi8L8LWkEApNpa2Ndd/bIr5/M
Ks1KfRIs2TBF+Avu0WlilaPpWYlgaapAohf+KG7I3OsiQrn+Xt2oNSReMpsaArCjReWWWNQHAVZn
LsIZxpRS6xsC2uFsB8HWf375f1ybYsZhKGt3pAQtRGT69/VuUSN788xatdlhmyM1GIhPuDNs8LpS
58d3m8QsCtUQbjQRRZxlTSak6rUGgMwWV06HWMGpvNZK/9nqC9aKe36KqrlqGKWFBLaFnDVIWCLR
6LBiQvmDKtXQ8257ebUeslCgiCA6XxHa/pj9QH28R1uUYklnaEAU9nUbemicMD/4Ru5VyGPAWy09
Xo+nK1pqXayB7qRQniTsdU14feLeArWI78XDcNoaRDS8TgsGN1DlHUFRuZHj22rdRir8g8D1U/T6
1RJ+TfEyzTKUoRAs3W2luuTm+iO4d9KmeqgI/pafvKRiztpUBj2oZQGsGA4yqVlKNkM4r4coL9yj
EAS855oKDSkuQyGm5RZq/v4DaYbiTV97+WMrZRUVNJTCxXSyKVJRG4ZaJa1fFiNfWBNeClvIQ6wE
nDpBtNREueHhYKB0Q8cI2AocTgxnjdZ553tvSxHLoz94Y6kUnD/6gVL5sL5uUvlj3MrNQ76a3Vfv
apG1VauCfFxvfTMfazATA2CVkc54PNY+ULnukuGkADWgJEOHq/lDSCHsdIy0vyRQjArmb1QCzpyi
l2jK+1ysXyvFllnUg1ywqZPWl+hL5FXIvNx4j0qu+rkSvxvmpmeV5tmI5TagIY9hUPkVgr5Zemmz
NzS2qprrL1h7F2JaK2a7gH8gvW/+sstR0otjOFJPzFENvkyBDewnACp4H54UGLVsN/nZpWoZZyO7
4i5M3c8zz9v8frgVRN8fTVRW8z/t2lfXsV8vjMr+lyQyuiC8wem+motQ1yAFIeoxbWNAYC//iDt3
3mnS2ueNdEoJEwN/SAzdGYHur3i1DMO9OWa53CpvtoeOZG0dsddZ7sm0wUkXsLNWPEcRnF59Tp2c
u8Bit0rYIDDc4VJO/Zbh+wisPrBjF/GPw89sBDY1FGlA2Yn8UI70OsKtF4gqEvtPHdzH2xK0SZ3l
s05OTLmuDpxRNHxDwbOFuasp+Z/hV/9ylC1PUvXfW+MMof/Azvb5JZHG5cMISw92JCnDoz2/S7on
uqW4pBColkcd43fbN/smXot/4El1ReWpIx1jT6UpbaoxbC19v9L1QglQLxk552fzIMgLNd6K8U5c
tsUvZws8AXntg+TTy333Uq7Ue/6Z3iF7Yrk02zvmauKXvYXFbDofecZddjz4isd+9Ef0RWl7gWNu
yBceQ9c6sorgr3GmXFDZLAP/tZ70NAkwq91ZT9b0dglByknnWdhe7yRj3UgV+qKYHiLQVQeo+gVo
a3XQD35f3Qs90Wlz6Vhso8tHzQnUJGyMnjvS1P6WesH6w0OWAQBxPPEl9HOFQzVoCeZDnkeDK7Sl
lposq3nfGs5BD4XK3SAeBwfeQIklNsNEC07+i+tvZzMYwEHQy8HW5pEE4HqMRv+8/4WJeXudiyda
8z0s3TkqHrhh+bs5Q1Uv2Z2W1aKs+jsnTrjeuvwDC6zrjEsZiJl1H6zTMUXqhKLJIuEJXX2ySs2G
HNUvuQEPu56Ipk4/8vJOZq2wtteWHJ15o2z8TrTtFqabg7cnegZpIoMRm13t6UAXCEHIKS7YWH7f
u7zRNr3vyenYrVQqCddnmXxF9uc+GmOtrxuaCTWE9PVj+OcPbvYVzIFAm4H37Nk2KVa8J7jYALnG
AtErRMTBg0IKFni5L3+FW409dtyrbLL1NnptWvwEZsjoq/Czee5jDy/TSVxchfyjw2isP4APhFAu
9EYyVuqrdyPP1V9wCAmtEEIOn3Y5skIbxOs1jk4uqb51qJdeJlPXiE1pG1C4Z0wrIFtl/hCzXccQ
videSLxA4kIs4jkJq/w+mrH+IIYNaQtN9DHj+VQVARiFEBEPn7I5pp1aohXIu+i/6kTcsAnkTpk8
KySlGkMtO9+1XkcEga5uzlU3t0DouhGhPitpH1zE7Pxh4PFswXvLOEwD6RRAPgmKudmS9o7bCOQj
AqOVj6BPD4o4DvPRUkLBBDyXQ12UJNjzCNfFqTjHDqO57Y5CIoVpyF7TZVeZeokp5lxXQfqys7bJ
3N6G9dRb7V541sBbV14S+0NuTCcrJg4KMbosUCfAf8Sa5SthX0qh6wxqNiGG7WbmyLLqZ2bLUT0M
kHadFIxAmGILWEhTSfN34H73xlmxbGXO+rVyZ7sHTzdLeExMlfTzvomx76cXVMFWEjstyVOJG1TD
KI9PZ6y7blFDGjn+y26sgY/Smsz+VpVdZPb1BVXE/5A93dNLVxkq1x8mRn0AogqayD6lEqSgv23q
tOxAQ+E54jZ+RFurRAhgaCbvcQVztLQWs0LbMD7tm8p1ON4nX2s9ubreGWu72mO6PufyuqxAGJ0z
JMRqadd54ddPTEVJYJM8V3nE3Tgx9OKszACpY5k4cDgVobUR33MZwzhrGT8EOksnBCGSTXLKAVh/
YdIhoFUn0HqM8h+gMaNy6hcMC43d3r78uOhq5SkLqeTgM1Wp5+rUkXTGYm/2wovmVOnJ8Sea8L/7
ixT6CA/h78UW1ezYml9WKzM1EUWM/kXgxqczSVp5o8VoUgzXLti1XNWuHtzeesK4CF6lk3QYbrWK
vOW7Q4LmaNJXWHcx7xZRWruUOmidsbqidGWVf4MOoew7UOdiPZ3wa0PAtip70axBEurMku+85xw3
utPuq7kPl6SGC7uCdFGfVrrGZ0+OZA+ijJbV1H2Ojo7eR3UmlogKpeuxCDjRYv0Br6lYwo/sfNOY
1nlEywVLqy/aCjZjzIv1FXZ7QIoBFYxRkKG1zbiPuezSetlmkIcO7oDZJOa5iCvNAz/ckumoP5y6
CRC6kcDXH/kCefH0LIOt8sLgd9w3htZqF3Emkj6yexFKJ/VDUYc2bE2gbsrUGSfGCWcawHtj4rxX
CpSJC11aZ60M6/wE+G0ccq+I/ogLbOE7Ikv7vUSAnWWS2cfG+rIipyFc5xeUr3IXr9a1+mkvWOzF
vzw7rB731/rsKQIhRIeTGBCpJFacHNt91s0YI4Go44eHevEhl1o8uoL9nrx2yPBcWXQEAT3J2J/M
1Y5GwPp+RljSFxymsSokt6DJlCG4/ouAybIPngz8u/8Zx6Y6+Xd4VyFB2Hsi+BFdHEdT1oS46R5D
ZKxRNyRz3XcSvfQ8lNWgw/Y1DgK3MccaqqXoW5zMoBOHZ77xpNRejSKFhOZoGbC7mLnkcJ1NIzu6
QAC64oaPLF51dT5BP8xp3IUPcxf0ZiTm3q38iDWyCqWOyzEe2NSg6PzR9HRm8s+xW0en4ER/xp97
TVtrj4XE5qvi4sAEDMTUX+xNC52FBALVRdhg87qEfKdBJA6Mzy1ROL6zOrnP4F2wiLDFwjo7nwxv
mtgPO3yfV+5TTpL+eyBlFZrjcL1idRmcMuOpEo4hPOhYRuBQk90x/mcfnCbV4fwSBuLGtF8ZIHfL
imHtZEZr84KaHs5V+053txeim2hXEM7ppgzXiKoAbIAjJIq933fMW7le/EyOTOdkzgHIea2gh98/
tbh5al4KcF/MyS8k2RQjCIbS9yAtB3vu9sY4zT9TfyBOnGG0CB1jU6lTlH9jEOYpaRHi6NvPzGfy
Pcukd6aQ3wogQ9UP93s0MgKIy3mZtto+T0yeSoe2lfEG6m/s5yOc89MTAX29iOlmbccZe1X9dw0J
H/9syKgxOa3mvn6zK6qPQ8wESSP1vVOoMPmHc9WKVhTk5FEhfrRwiY7oov8S9L8PZ5O4nJEQ6UlV
nsdnmQsbavaJIYcPNE2JbKpvol0MFt+CTpsFntOMyY9ZC3kcE11yZ6DHrbAJ4Mk/JqSvRbndB7/A
PUyRx2sCJcgUJH5hyg8UcJG1UUXNzA/NZVFMLN+W4ZtY0aPzjgRCubqk+LAzVPtcG+hbkSMJimbJ
zhY3TvYSBQjkSXRYTy5DYBi9H/2MLZYYDMqScFJDvXnE7mEIvKDeku2N+Robaie+oiW3g/Z56Kip
njECinvfJ49tCoxZnjGGOjKl8JmnwYjoacSWgauFabrtszzrsle+OXVPcgU3U5PxoWwJXrSkSU3P
QEYsj0uApnwtX3Kdl3N19atZ6pMIeM6jnvG7Degjs5xpTBRAQQu+Xxf2gDBRCqVNzJCcLhebWeOm
MVHQgVD9o5YV8iNxGQEgN3MVHz/ZD1C268qOG/zIyhJR33oO5MsFhVr8ySQmpCx1J+sEz97H7nno
t+YgCGacxGdNf0MeA+ReUl+jm1MEO/Y8JZQY9z0U3cxuu/c/mw63Ku/ABh/YKcaGs4Nh4dvbfLv1
AHM9WvSz5EbYsRmSE4n7aQTDyC2G4+UfXtwYz9X4qju8gxv157ryFDHl3Oj8ors2VRcR85OOIMzU
xQwHhAdSCmXqmt/Lo3CpoUAr1GcHeKxMqG/zU9jBwI+5L259x1PvIA/JENpoBeqS36NI+iFkPnpp
YQ/WgG2MFsQrFlj66r+9Wj1OVeZwrkpElFL/rFgydWb+GvAWmQ6fcl1hHvijeZC/E25LkV5HKRt4
/K+uFFrCcgBPAW0M7pcyMxtOmh0AvHpHtLb0AaBQwxTMKhw2ifFsnuM6fLtS3cBxGine5f4r6XUQ
qIB90v7jlXa7J3eqjWZr1W9NzdknpkSTYXyPRR6Z9J6eg7kIF31kp3sDMRhF5lP0m24He8Fef3uy
ZqYdvFciNUDGS7ECK/urrmf3A1qtDRbVQfEPjpiagiwuPb8Gpz7HvSy0/zM6WtxJ70rUdOMVafQx
VYuY/x9OkS5tVQKRmfRHKAk2I2MKg1fqUoJEXvLmVCZ9vRPKEIQ87P3h9k5l/pB45WI9a6ft7xFH
nOj2MlVBcD/SIo2Q0WaHJcraY2vniEW/gG7YvYKA95NKOpmCXHfmTiClr0N4ctchCsGmeHWYvqfF
HOe67ILxdoGPZbe4VjFk1pQAABfw0wGkHfZDwkWGdP0E44ptP5DtAL0GCgzmVxryoC/IhCNYGZlq
QhYHDAIf4J2uQ8XVrzFThaAWTKEQodIoPYcqHBgkbR04q6NWXDoo2OdTnSpS31QQEHPgVV05nGIQ
21wQBRIlFEQrgZ5Ai0JZ97eMYRNecQ+Zca9+4XXmelv/kDUbu/UP5LSa5uEuRjC7ORppWNFaz1lo
FfKIwAy3SDMoD0GET2UIr8if52tz0FqW2on+YtDf+AhK8xYevRCr9+pPIpdpe6XoldWBGedSg8Bt
n8EtZtE3Ar5tphl5BM5FvO8M7s2my8Lp6gwWVaK+ktEYlVZ6rlb5VzmAi5T7JOmOBvcVY2KxdLhC
/t51bKNkOpGY0CoBYBZEcqcCTMqvKghKm084g9W5GOrm1X4PGXX1O7Axmrjvhk5A5XVKwFrr7dl0
n21qqoMFSpmDcDB4Ptb4tQUPN3PmjP6Ffy8VYPrJV+ZJal98X16PH0yy0v4dX22ttJCDOcevZ7QD
Q1uXmKIeKGO3dMJp6RvLSDVXBKPQjiN23LRuuX8o4XncagKs2ePzH7iWVyFh7cxSgTLMWkCOltYy
ixhwVcsGTgYswtXGrb5XSjBaIkCq/Dhuk/lxSEqr83P6qoDU2vZEWtAnL3p3Ki7IZHIjVCDDps8I
IjDMVgOQHnI48cfwSrZpeq1hAXkUA18ORqooHj5DEL3ATWqxDbdvwiwqahYHO2/RuASBLNkrW6vB
AuuyN7WOggm/05pp4HkAiZT2VJFI9rxaFK0lA3WdUyVPvg203fwfifoWEM1v0ln+9uT37cyenJM/
/Hn9DYGe/Nzg4aJDUtO9zmFvw898PiUrQsUL2w+J5LYOA9rQW8Pr9JxWekj985+oTMFVlWsFN5YY
yKJ8svkfPoz2pND1VpsM6ek6+qJxiFxZQRjzeyeFczno+iLJ+wOmCy8wIUDzmjUpFNhUfXWFaCXt
GAh8PR6KJazqpG/jgwIk7NyQWFVYX70NLb3Hg8nI0SevLpgAxG0NbzkTyCJgUUT7Bt2EgN23SxBJ
lOeqWw8HcehMjW0KDUHt1AH4KjDxCjDxVirz+Xe3VEVEexMb5CxoTKhtmzQt1yXYBqmqe4gnJeGd
17fqNq0HHzvT+5+MkxfLo/QZFY+Rcap71MWbrCl4KW/iKyOa3LPbZ9wLHabgKgtGoP7RwWxdgtfd
VedG0A1bAos0Bgq/TFAKQ2vPCCIT4CJlH5FN8ElTPfb0b4M5pJKWYrPqEMtOvUlPc5h2it+zyUCL
4ag/PzDDLC2eEkHJVe9SDmES08KykjuzjDDMjGSXmb9cmbQYpcsvzAr92fVQ3so/yLJb4g3pZDia
DDBwO55GtGZJXnJwy+2ZNbZ2YMNtrmuR4dEIf6sAmVXYJJjj5cvn5UeBQRVEz/Hj0Ur8z0XmDQs4
KWAMCrP/Ym7HrX87rYXEwzeuVKkVQ7gz1KqfdNqJnkOrtNjX4+XbmPxbZy4CBSsEwI9VSA1GGv/O
eMEX8GmlaRDcgaC3p3CUNkl2yBxz4nwcJuSv8FruIrKrsvvtnt0Ww1jOQPEvpix6mzyhn/AkiweP
dwqvPPD5LKltiNd/KPnEX1+kOtMV9I2J6sKzsfEML1YTlZkZF+4UsKtlZ3oHndyJu5bMeCSlcZ2x
+7TTeIO53HqoVU6I4VmlFfotTyu15aIZN4jhmleBww5BuBaRFFvcIVAn5N4mxKyoSS7yq5n3MVtR
bPblVcc3hijldsiVDJxxvF0QPWzYhgztTdAI7ESR1OFExARa2Al4Yo5j6IBF89S0ZZPgynXbczp0
KK+i32hMfjRl9tDEmFsV671c+jFL2IpsgSIzaXxHkDUq5TP+XPtt9F1YKWqMx2b0CG/aMeTITeCl
ZVzXHDC936drqbiIrHZUvPCk0dE9cE6Y9M9mpmkLNeTDKB1KYxjIS6y1er6WuRjWPiUvimWndbw8
VRbMXWv8MCWPBOOqmmTyR5iQfsXU1+3k1N/h
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
kYrcO/E+Jhm4R/4R3+CukKYR9M2FIvcsEHYDIEQ941LV/qe3nw66ouV0tjU2K77WxMp0KzE3bUaN
EkHZUhS54Zbapq0AAlHGThTWWu9TToic0Fogfo0uxbTRj/YKvsYbGHXn+38UtVT4gl+Z+q34s2Mx
S+RksJLLbqa/UjuB2IA=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
k7VYfhbczr+tglBVnP2dNpzQUg4faERuh35S6DlbOXKmaLBzNWJuLZKd3/iHJso+4/ki/NZUVDCo
PIbVzwxMtfGyW1fMXDvveUi46OnejPwVxk5t1kIbtSbcZCd++dNgqg5UzMEgptRWzheZuzX0GigU
yFrxhwF/EKgqip1pp6C9cstz8ElT8YbfLOW5ZqJRuK3p8wRTUD9tZ+3ZT4AUQNnb5LwhJYd18bKy
gCZ5WG9Mj+aMW9valUSRFjEY4oFOYnca2u9dC1uGlv48Br0t9pUhfrmTbufRCalBxAR594dFK/W+
13kLKPWgZzIiZRLopKxSb3kx8JrEbJXF16BnhQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TxEtvLMShWARGvALMwAihIuShrdtPpwirMDR7BzuLz8WzVhoqvJSM5/nLMHFGqovxD5hXGIA2TAw
UB0YVlq6K3gG1/oM4RpzHTN3yz8Lt5YW3A+UfuxJr1V9UVkS6LmvF75rPoruMKpllkRnQaQkrdOH
79erJYgSSdvNFj79HX4=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Jd4QdSkhWhpPJfQcqGINGTBbyQi4fwpgiNWDB3Wd2IjKeric0AmdHU7UViuSzCLh03DSaNG2q/XP
qatCMMw9/14uzhpUJU/1zUWxXlbRxdCkB/LSsYsRRmVRjaX8PHa9/COyOOXOwziBKCZ4EH/zCO32
LML+m8CiAQ/Hl3o7OkbgzReeGFKo2yT0AlTR1mlGeI1ujqvvwRe1Fai0g+TwEJcmsDU1/5bkvxQ8
aV49pZh6N2SUhTCJ+wLBZlcMIljfD3Bu8Sp/4tL/+j+yW2zEEf4Sl33jw0Cb08EifW3RF8BmuSm6
hUeX9HuDvEf347dVCR8t8qRzeC+0nGD4/fB1NQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nE6k/lSQEQ4OmPB4XqBcP/LpC07K/JJ0IvLqk0FbQzQZjzqT5yDvPsiRjELAcBvPJRahwOqlfyes
JDXxH4G+XSbtKQtE02yLheyEjNesZ0dv/v3vL+wA09O8khSrVyP5ijRndW00Cf5Bf2IpNiaJRcds
F1ushZZu9jXeBItrh4znBf9fOoXggbdnBLyNjuw7bRfvTeY2Xhe1Z7RpJLgPWMz3yKmlUVxO5Zyf
mjNu1+82dGuZ9x/eImCHDzcLcpca/TdMV0iJAkZHrvuhhu0GfQ7zgBbvuyb+I/r0q0vuL52PeEET
HDmGQS2oxiFTbcwiGY3t/ioXPJYkEEqNFUIzSA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EYYoCPbR+OMFlmBfBNcQ1RKQKD88wkYgxA5pkdacb5EuwAeven6zC8gsLrmbmaf1Y+GE+exjL/E8
csfwUz3cQq4551Y/pgVQB6wc+K/5qus2SV7wqxTpqsWY/Yu+bULiGuBSdS51qWlfxDNujKEBhRPN
GKWkQK8KP7xMHh1W8rO4WL7cLP0qnZ7xSovnz379iAYpAJOGf/f5GjM87wrRCh+60BUmNbENwN6h
Un/7huetrD2tvDcD67Ox5Dkto+nybbrNNH3ry0zh96Cq8sxNBI7cJ/iRp5kCBgqxCxELTa7hlTHW
RWkLjA2W/Y2HjatDbYo5U0A7bO8ORiG66IX0Kg==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
q9bGXHBOyTLb3eTSnDNZfQbfjyoc3yN7NB+1C2N+mReGSJxWRtlWWn5HWbhvjoAJehclGC7OtjK2
ZSTJ0A3pHY3St3rul3liQXKD5kCQ9+vFLUhyKlQc08mhaOXPkXVrLBkSbJoneeg+zcwJuKQzPvv8
Se016G+DYsP9PPIjvWbgYSkDDPBmrvDI1+5mRe5HwZFGFGhAQNqFMnPAskAW1MwhObzaIpkQKTZT
7A6i2BjYT3UzWyOCYK2zgjiB9ZFwChUw4Bwh+H8Xf2j3ysF46VVr3Y/hfiRxPSHR8Jb8iMEkCJjf
nRAfkr8Y2ZxDL10aUR1VFpL5aHsLiRKnNRdZXw==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
nsakC0nZIZNi1X6ujQodgmUw2UIdYzuFQ4iAZwA9YfvRrxXUL7ynKQCgPpNVzwJk5S+CJlgNjRvH
avhNsBU4C+cBB3dvqouQ4tOLrtjvGCn/tgPDevuIaG5LBxGdZZ/MOgVEltPHWIYycz6nfuA5/Axp
6IIz71mUhQT3OW6kWYR5cK3zVKmHXkQGZxfNAWG/Pw5DHuc9xxTQpswaIv4ECw8olrxqfoRkzz/n
gmc1riU255Qanc8CpzTXkB0TXLYD8b3W4k0EIAYhAlKk5HVAVS9D3DfcWg27dKxRMm5dVH7ddpvn
9W7az/Gv4/jAcQ/A2wvn+5RGmVdmY2XJTvnb42j3M+6+R6PXkHvxDCRRgj7df9TYddZWyOeT0KQd
DnIaIlkFA345xytHveeTmDy6qVwsD6GrlsYJS9tCsR6FloMwjoQcZKSxBqfWh+rvQ8/8NxsGVy4v
3tFI5PwOhr5e4Nw4hm2q3u3mpmtv9+BzXIuf1HXxWr2eSaeu22WHlCsg

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WuUgcS5b6yfqTuzjufwmIVC5kWm6y/3mx22Aii+Dgdcnv/uLoI9/njjHdhb7hUlsD3Xs1keDNIwN
3pNTWeUxyZTJzKR7udvlJMLBMym3o/ECBMv+uN4BToB/hl2qqhLvFAO/r5AFOlliZqDwiGcbQvyz
YxE2I3qA+lBeP2iX2/4t2ns07deHzxcGsGDpvkWpwNcM3RmD3m5puzv13u/mWj0iTjzSuDu+lCO3
EIjElwRdbJl/F7N/czlKYgmKd6feg7/nbSKTQgrJk+bEOJwzrhlLGQvovZgtfM2nxWwlvulcT7sS
n2ZxTDzZIZJeakYPGSP3PRWLzaOntLk4/JYNoQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HAfLWwf5IE4nVH0RKu6Ckfcag4YISAB7GxmA74RLd0WtgVtvSg/hiI6xjdDBajL3WlsS8r0EeRuE
7k3XV6Iw18PLWYY7xEqYXN+4UCUMJuuhFnCKbupuHsoPe92DFCS1iQmSCu4KA4if6La2soKs0Eai
lizBuddfJbplTj7Z459Jc2VAD/slvgcakh9coxr57R1xf3xL+SqtbztnNWXTWebaVsMi9o1R8+q2
Bw6o2bthJTK5AjuaNFC1mXchmICuCVK92/JyceC3nXwexvYK1qRmiOyoTPwPOS9/j/gup9+/1Be6
vYxlYOcskfzyxWLNti298ohd6UCc2uC5C4Rl3w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DzCZLHkutR8dxKMJJC1uS/LdG9PoCtj5GsOR4GKxJSZTHbAW3Lwb4zUisDiKbo8nzvAc+Pc3aKIh
FZY+iEihN/UyNBp/ZVBx4xfw4KiNs0WcNidwHxnj/AmT0YahVcv3MBdpFE4TvDgOFqEqCr2KvrS5
K14RY6HsADqifYcgChtDVh4X+2Nen/oSD8dZS1qLOsyQr7ETEhogVmc4Gi3TE4/HYjm8lV5GRuJM
x1+0GPRONu+RFuc2B6sidWODYyJus0b7HVqnBAA8gMcV6twjAADrnyIqZwnPoiUCKAMzsDKVKhW3
GrlmNwP5uDSVq/4QrLJ59GIzFy3EXCfFTYr7nA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59328)
`protect data_block
8sg7oDP5gJWo2DiyBMNozfAOE7PmiElqkSX9wsVnS1PWXL3C7Cm7OKjZo2qjSa694nWLiOWor/Iz
yJPjFqqr+SEsAw3hoCYZpYUJeo26SRApJ/AWkAoAIVvQ1uQRlCLy1VLNMz1d+Ip4fUY1MTIYT6yz
/TD8rnrM0GvIZUYuol3CGaccI6YEH9zgNcg0ogdIbHYdzkkFiIyjrawCpFEfWggVMYeHP0qP51G+
zjPnQrAMCm4XdnBtrcOkS+2jVwhqhsfD9uMeqSwOf7jF/s0QDxoUkMuWqLZdHJMYGD8WpFqDiW2l
vwwah22AF/coFcP/jCybq6EAgRSqAOgJ6IM3JBX/z6OZ1lVTBH99qxCQfhpio9TDvVcjghF+/FOJ
YWNlNLQJdL3nbuwXWW0kX7iLuabshzKKHNbSVU0F0NPMI8kM+ne4Pu9I7+E/lSVjO/xi6fNCLKOw
oNBlnk60fjmiZO2Ddc3jMJkrBDSW1E/uPCHNoWJlKYL10zodnPXuyx8CyetJGvqViYh9PplfVVxC
p7WJAPj9yZTkHVPB7ElUn8BMGZyGRi5RP5irpjOx74M+sopUqYqmQeMJGXEX7UdQ9qlmzVsQ7IW6
i4sE9hhyh7Kj9Ir98J4kb0WjOuI2riPJltFsPS4ObrsyxqqUX9XJl2Kqb7PbhUAKD36T0IcqsS+H
T6YX8+hzI/8Zj2UQQJM9Wh4ss/JlMwt6aOI3QauxFiG298dY3xOXvWAF24HSu/4J0altv7b53gZN
gA0mY/4t6INUeiACFq0X3VWNcTBmi42ljOcn7wR+uDpyZuqLoXAuE6Q8ILpSIs1L+rWW5T0Oe4V/
5fDm6vQCDbM231k2yQrbl+ILKUItQqza4jT6atXdlYUiUCpiZMN1NcpfzbR1/Vis7mpesBICQaBs
x2RtgooWp/JwCDKNRSnGxcl+7L51QtHEUtyQnRUZAhGRlZTuc4RB/43a2VSElKtiAmSrWapt0OzO
Dze7dCvhZY5eF62K003c8OyruAuzE+0AN/F2a43u7UosRjlFIbtFq1jMxQ532ppJQz2Z1nVD9gIT
Ao3EBE7El62Dcj5Nc1ektiPJORWPhf2tLH4uxpqrSVTs5qTlpbYlmLY8GGOByt6GhintFd8PqrLh
tKSCx5oN7ZkPJ9yrO2Q5MPaOkiOmLm2ZxCT3HVKLbzDyxtPTUBsT759lH+T6HhUN4xe61t1w5DP0
jz4tYsbbDMeZetTnURDslOTCieK+4MMEOI0c6f/aOv22zHTgJbkI6xSxjDYzNg8D87UE9HybrzGY
k5o8AK7UOC0SmN5ncN1EdyUlCcZVDvse2dplrf2T2vkqzNEEE14/jwElwCqWmO3FgGLY2wiI8oiG
+ZlnjHqJkR3xtlz6078QyofEZETTo8MyBHy8KnNh8I8iisB9M7+5ZXF1ONy5FE2rvUuDY2xqeLi5
r8BiX187eKhxEH4hmWba7beLewjtBx1CVu34pGtb0gAYmPG4u8D1bK2PTxeq3QWfmzReAN4P/J7j
7QrQ6sfXVSm6rVnEPoxRdVR0BZ9r/kVft40JYLdPLXaKWB9BLP2Cp2hoUfmG5pt7sPfqYLceOepT
wThhxm77DSwytJgHwHETTVmOy7CTnH7nHfpqU4yCiO0+Q+T50UzzwXIgLtPANmeS5UW3iVI6Vrc5
CaYu/zasj8sU1ThzHafMbCl4zhsGauPMhjyOfzszwsxWFx4dFIy7lYYUP47AzMGpue40aw7eE1oO
BTQ2h+dpM1vo08c8R4oX0s7ewGTex/489jiw1Voll4BVYoJgCZUdyXOlN2eLSiCinfIjrdJJp59/
0WfymDXfPuBFrXn6Fz0WeefTuTc7iuoBswJQcCfNORhvB1cJHnw6kgDZ2BjbimKFvEIvWn/Cdn1Y
+xCvUqeLJcYj0SbxgdtIS7Gky9nDAXvYNJAAoBe8siBhHNx2SedaNukzO4APO08S1dP5/pavfHdo
n4BHAxFXn/C2hFo5JviAs1wL7ceOzvZnUnCPTcIvT9F0lbAfBlZ34ZpCGfMeRcqFknPj5c8l6DQT
kbCF8euE53HcTwTsPTm92/ZUYdD7fJOcLO612UXRexeBbYX7VQwFEtN1AYsW0JjV8kfnSo2ob7Sk
owtsI1luMbgxzEDIFhIJL5AluTWYFR2fyvEA5G6QCjDeEuDExI6KuuMVFkysOrOHKyHfXsGjqt8F
T7FmKU0oPfQO9AP3WyLoxv28cDzeQjskjR4PhNPirv8WDURO1dMTA7dIblPOSc1ybF4b3uRtN5gP
a0GTseUdMyl0cmrJB4dmtbsZHE2DEgDWF5fe2DyTffLgF2P1gwt5EQLBE1+ARwTHfNFA1pXjjsIJ
+Mo5/cd15ywmxmP6h+5uBg062fC1csYjQdXNjsjwUR2cNuF8DdVMxIfwoLP/lpV4/7b7HNeUai5w
RikHhuSu1a1hqNkMoPjH20NajPQcWgVVQNPRIietKCPqGJif09H8oGH/Xuv8Ug3mNPo9mXKHkx95
95MtJyGqQguY0DySdVUbPUhWu3A+TFJGJDbWFNrjiKE5UMZxjsnHsXFra3QVQ9YgJGcJ14XtP/dI
Y42DP0NpxaHCalZ3hGunVhePUhd6ZNkGGmUksWf0H5+oNfAeAV82qLQMVLr4kQcY18icm1XquLfU
vclI/Yy0ckFl5sMNCrsUWYmiSIQK2njFaw2TOCQlTBbOpFtxN29EKBuUO5enZ22eK6eralgiIGBQ
H5sVIPzwvCZanSYMen9TW18KCkQ+KMlxYz3TQJgbw1j4l/q7N1/jsy3a0JMlhip2R+2Tarp0Ns9e
VLSx8y8eMkJaAbhjQ0x8gBKIgMOXEqZM2ub2gxqldrecbHnHTKai/UFG8jR6tlnMbLKK1rNEfVjM
D25+MVtyRbcpx5lS+E7yaWxyDg890QaADXd0FJj1ija6W9nQutZyYDQoqgQBXE1GKcMMXXWKGIe6
nfcWsUBBHXb5Z/ifyv5IWREA7vBvgrglxRLzaLSm97QRicwz2m443Atoc4DnpF5mNPGzO1OD+Bud
gr53YLTEwJb4aEDoGhkAgw5TkcsgkjKKhlZ4GotuIkJgAByRNp7upz7rpGEvXNtuBc1KzG8sobWf
IGoEzQLd36VKMnqSkCi+9Vn7Xkj220XdMMJbMra6Ok+X5pJDZAIskQryuesN27xtTFcKgquru9HE
koSxCInfenpB/+wyQQ/rRyUe/K6u9kaa3fa1rstkTNMVatD8wDOMYOLcbOMWwENXeyXzLaERfWjY
GL/36+HSgW+IHJbZBDkw9K8t2DlnWAoEPiKL6vAWrxv6M2iwRCBEPukyUnRCL5SQr2S2+7GB0Bgn
cWBlil5dV6YgOw4haaZakmPZrmJ55lfAmabF41GnoRnbGFnuDXQ03QGpFhnbdHlfIvE5qrsZJ4Hd
RUTj3M9mYsmkoCiK0KO8vUh39Em8BNq0a7YSR+9z4rKuXV2YpmCrNJaSDAnZPJMvKBYQNPPGyZT8
EkqJ/iM8T+7ujisgLRvq2PmbkeTvaSIfPuhYU+iWk+bDiKGTwCNtcZx/NlTg4/QsWyQQF3VPVCxo
gHHWoYOUyzTJ1siKJ4uFFi67JB6TY0KHgR1K9ljioUk/aDWH+eCEFmDwwb0ON+c0JmM0kICONT8m
AfCu/q1p80ci84uxSzrVh5HpTmgYANTckXFC+VylohvSMMywiVrm47aWdEy4CGV+b5I1uPs8lESJ
/ZPboN+vCHRSYmqgsgkd7mIXcsKvkkCdZzXziVhVRz8tnazgEZcMttlfyjXZ8nKu/4f8g5b6f6LL
ldooJSE3xrn8rrJ2mswjaUf0IAg32eNMh1Up0SiKopd1C1QiV/c41sUoKfHmWXyaPJdSvPqLizE+
T9ijNiPvlt1En74jBZ/GP0Oq5Ip71lz3zzpVLoSnbLxvWP0MUzYar3og42uocvRVLJ1sP04lUEjm
xa/wo5tQskkHKDv4KH0NNParrqHqhGFUm+m28rQra6WHDoczo0k3Sjx5jqYjVJxWI3lUx4Opk92x
woz/UB0blP71O9WDySpPUHIc6f5uzwMx2rcjLnnIHUGJxVNQDWVXPvtF/Yj0fqneVddyBXyg4/X0
qIhGKwA/aHz3sfFPVH0VEtjpf7KlqtpTPggElUWPY6W29M1ynWbmzqvRbXaakZGZGdiBCUgUjnu+
5jKwnGj5xYVOw/n5LLa/RBJOIpjCtYFZkwBeK9irxdGE/dTAsxOLjjs6+MOZx9eKAjLONeHnkPEi
+a64mXsse44LsnmOGqYqUgVu24lpRShOnQdQ0AQyVYylxhDlCGNxKtH1ra/RNkHlF3s9DEykplp+
u9YQ68gT/jAz49/GWRR46dWAoOEOGg4p8LBI3QxrvxQW+U9VNpb9hbfObX6GzhsPzZW5Nkb1aHny
Vn94pWjsXUtDwY/0oKJCesc+xNS4jhGDhbLpsY0X1CSUC4ljmWG/R2h/B5cvUJJVDc0Qd/1FCqI+
+88BUQLaeVs0CLCoEUw2P4MFd60TE0ivaHEDJXTI4D78jh6FeKT/gzEUtciVfEzSVPvGrHaojyr4
lcot9kEKrxZYf1H4NojfyqhYfjN+gbNUKI5am11Hs/Sdx2AU/JObrOiaSBLa5WP0Lni/7C5ewdmJ
WCUsGLOKIp3iFWKkfRUtmjAUNXLarNAwNSBXaaV6fR0DYWeteW51YxvG0n/mGx24utG+pjqVpHYI
XqMynQz0Y+3s+3F/kdEqmiksQ++w8iNn66sbOo6HOFFZmlKCw/VXkpz29yYnLGj1PSg9V/45xAbO
j8WUWVSvlR9bnQF75cMr0FJbMD4ekhhTqELRyLLiW6OcE83Da0UYjTzQCRpu2oV7tkDJEuq6drsh
tJ5AT5aUJAZuYRwm8BHMwQO5Ta3e3bbyKfh6znhWzRFa267TpBVwNupZMzoqDo8+00mnRqO+jyFn
jRiXtDZA2iHKAT+UyfAo2fRyvu94G66i0bqg1N8m6H9KiLAa52h47/9Dj1oAZ68VLQuMGl+PZWzF
+MgDG7R2YkWUy2W9PIf+R17sTkemcicaUfq9Vzg0Uw7Qv2269fS5bLhuOCdoteAjBrngAX2ksqq8
eSJBaxqMsMq9r/eQdESq+z94WN4umi7s6EVDDe+U/Fo7GdzCTkkF4gVeZ648n7p76x6rCTNirRLz
R5E5w0rRy+LTIx2at9Fy8DkB8IdtqTC9eMQdkXtQwa1kU2JmqKik9PaktlccYkiUuLF8R+F3fWs5
a1vnVV5C+i4rfdp6ruHDSdWwLzHw1OCsjbsYS9EjwZQYyz/CKUoFV9frSsqHu15gH/w17+qrqtMp
Ul3tOumUo9sfVXYYRy1gjahaltp0FAKxva4GYXNJ8M4QcHLJuVU9Tn+vMF0gUeFgXqPiNhGevAtb
FuvSjhg3KeJ4QME6Cn/b4URuL5k9FOKi5+vmooZsOD1+bXvwexA/+UQPW0GF13Thc/DIK/FBnNZj
tp8XScyNJscU+aONpruNdK8WvKyHGm7k0UVGLwA/P2N9IvVYAEOpS5COSpx5chlQOYm+sXztwic+
TqxdPrZCRcGpoc/NE68x3g2cbfVoHhrTsDgAPURat9RybFDGij/mynUVoskwcmI47Xd/7Dsl67TB
f0y24XX1Lz3ZRqtlzHB+w3QnIihQxXwV7HZVhhlQrVVEoIbDXaU5god0QOG7GeOQL/T1fgCZOEm4
9wbjaBxpVvnhD6PUlqmjAo+2yNVfN78cCevGCj4BaUBhoRHv+kPUCeZ35EJ6ePsLm7S/hED/a36B
eWPX1HoSUkFxQeL2/Q9SkG4kJHYUVD1Ch8lJWzdfU7snUV8PweZgbEnrSmbmrjtR3sf+YCh5z42f
YTB3ysXlPMfvLEyY9aT8kLRGY+dfvmz+3S/XMLGfEOh4ucDLDrYIfVnSD3t35N6cRqJJDPA6weN9
pafIC0QI6qdnedMOcUBN7zTstMy2x08Q3b3/V7sjLohNeLdViRxbiOlzfj7htVGsJIGAEj3gV/ZH
R1Bc8bNPjC8nttupojTkxSQTiESEq6w9O7b6SEYNRJ9pQbR87FUo1tOOnOCl3OmDpYr02srjjsSG
HDgJAIR0V21eute0Ia+n0cIVG7hsAANzxNjTeNuLkMjkX+jhZg+PL0OkBZIaQ8qaqvV9klKV649+
T2t51nzCbS4ZqUZd6rNdTwVZfqTNe73bldL2s8kNYu3m8fsitDmpOOvjW/YzTvZRMN14tDqt8kxy
/53zerz3X3BTwivaUxv1caQj6Sp12nahBln89cFL8Sa/agp+4eFFnP815/8GDZ7B0nfJCA/OZzku
wfcUYKNch+SG+cxAX/RQUG5HttAb5lFi6qPLHKD3VSqACSAu5XRzGTx6qbCcpGHhr3REyVNbXaXi
esoUjjv1gazSz0JxTJHegKjPz7wQJ8kdq4Vbq/Rvcqw8L7cWkchM/1Ox7vhh+FkXZYrRE8T2E68u
L5cDUVUFjx37izmyAL+XM2Xx3ud5xF3lcN7XHfm3O3YJZB5WweTT1WbIab3JIS9cs2XOrqmvMjHj
K5vsD7cTbW7V+E5WHqTXkmO0Hg3XnqzZdWau7F+0b0/Qgh+CZlojDdQdmJhbbp20RgffMRtK3woJ
28WS/vaDi2u25WBueRgLGi1+H7Og1K6HQQOBouFxVTmIsOyDqHbqa/nF1tdMYff4T/5yfW9PJ975
653+nXsobFNkpiZCjW2teNL5PxhMDvjnLPw9AOmgAR9xDmYNBSrtpwa5Q1128rgdQ84+JY+fLZ3J
rxW0rt58cG+3hMJyyLBTvf0SNpgA/oXpFpfhsTx5GzA/Si/U5NEG6rF7QCQhVE+F9EyjcRsxVrXy
sVGFmsVXLmvaLym7CCVDK5SsQSZx1DQUw9C59nqskOzAYKoMueCkcJGaGrS0HrC66CWbZxZ15alV
wFM4mzH82pF44vjbWRcokp0H4VV2wlA6ggKaSejP+qFAmEusRFw7UiOEm2LwhpaXAOneH6TzOZ2c
2Z+GqQBYl35x6E9Bf3tC4KoyJlee2Whby4OuEscsQYTXeg6Nhn2FDEz4dNZAKwWSBVxGd3BLffg8
pzg6e2JAQPx6bOBtJcXn8zb0TkOhSIbNAa2/1HEo6qyaNmvclz5neoC8IEuR7GZ6YfPI+POkbyhW
LfyVIlgFMp3kgEBrjttrdArooHge/cBrBpSZNvWyNOMXeCg7CckZi9k+2Fr8zsL9BTa/Vgk83Ibv
UrHH9sxO9UypBDdM6/2FZyvi3JJHMf1Gb0J5T51xPGPovRxW6tQjN70+RxWZnLFnRAv7BE4ew4fP
/Mh5NcCng5Bfw611KrUoiVrCcnXyqAz+qRLfxLyxjQ9QYa7WBNogl+tD3wQUltiB4gzQ0qjY499/
Dp8rxZiaXyFY3XsgRbrLLFA/Syr+4zzdCQJkyZxTMv4h66w8BTfAapm3X7wBH0ytkojucNx45uIM
F1B38nK0CtlRxoAu7O5vehWrK+ajJmFwQvDZP+93lUaT0yKxn0zTWpLqL92w3ga4Yg4IQcNIZXdG
T4eZ1Qb4HxDxwuKT7m+DHBUNvF06A4hfNG8QxUynl7yGNeIi81rbniz9484vFHj4KJa+f3SG2RyS
1Tl5G44Nizmk0m1ej5PAf4e+SARwyNbxLr9OcZ8dF/vuGWYhUScVCta+IDVJ1mv90PP0xZF7+sMe
+u3PmhTgGLCwPyD3dMXI6aWvpeoLyO7Z+Eh5kDpIoLjNyeTJiwpDBPt1aeUvShxHiwuOKP1eoI36
GhbkXJsB26w5H7tR4JTLMKs9kSILcwRcZ+m9fs/NiryXSYsAuNFup0vp8+GxJISuGaOZ9M7YjPe4
CIS45XzviwTKHrTeoL6ZLqrqD5Na2Mm5ru8wtTRFbwObqkSr6RLmPXHnnBCYQs3Dyw5OudEbn3ek
UbALf6qPhh/vChJ47WyEJvAy2ckNODsaYYl58+JsXinDkLWCEpo3boRKc0VcuoT/kaYoVJAp4w1E
p/9sf0BdU1zGj29uZ8YqIgut4W2psY6OrCQKNuIwPT0iefYVVxLLQca9Y8FDHNsIX3sn36ClPWPa
JsRLpZ3jvQRBOBlugfaQOQVcJFtGjt/QV9TuKRczPuSFQPXpDLVepHripXWS/uq/9PvBJ0xFBnJ7
R7C5Zf9LzyMR6IzppF6GisrIsEpE6Lr/E+CsmmeCBZ0zBIWnM8hvToOpCQ3/PZ3sTVUufHwOIqIB
4UYY+Sio4cn4v4udDrewOq0/NWS4Jznb++85OSU3l+TLahnm5KwlHkQStH9N8G+/h8TrBsTIwOml
5ORJGJjig4m1lQ8yuZvWclpybokKTx2B/OmIRMYUmJSZEMRivjEvVriTF8hCPQxnY03M63jP6Zvu
PagD/A6yAGIWofXdU8r0d714LjpoYlYESRyQBy72975JXwIJzaoYYbes2yT7uTCkozWj3SiQtIfJ
NOe7e//nh8Mio2PQTk6aiAaAUAGAwK67K+7ReoED8dClqTZJH8IxrncGpyoCV6zSzVhZBiAhOwoz
j8aXCmVLAhsf+M24e19N5CNl/bVNJTbt4inXWvJ2sVJazN2dTuKfh4An6AITel0mXje0JZeolHfJ
xv04QGUSzNOvQMl/VnaPBulqbmHANQh4KJrdrZN06W2O+IySvCr5wm6VsWMb+k7LAUpWWwUK2yWE
xYldAIiv46s3DspnTyhC4Bay+5niNQKUGDsVHoJw5vjehaXph66sqZtbWNoXRq4XWPpVIgaS3dOF
34Pom5Df+J3RGyd8QFQUJpki3XLjXMbt1aHJLreyJbmjtsPHE9uH+XJ0t26h6Fu+auJ2R6BjFDQd
pk+vZBwDGmnh/WtV+e2J60zR3XN9LuS4Fq0fDySoxjNJhYLZqZJsRk1/ScXawJ1RorgsWY7Qtoce
zqF7Gog3mmy30KiDXgHIEvwuAZ9s5FL8wgrYtKliWEPOAMXNBTNUcFAY0ZYxk2iK7BFWdmnP6UlU
+YB6RQPXNzTx/mXVuaXz1Ssg7RZ3n8cbnXt4SA+LderimNvM53iuXCtN1cHAmqpfEz2NeX4qH1R+
4d/bqcjjrvUwaLUuWzbmUdT2eR6gQaramf1a+Tj2Q2FqyfZ8K4oDYnv7C7pIEv6ZOEjOCCyIvVvU
vb7n7x7d4iMZnZqXd13nPyTeGCyhqLeizJe7mrYVKCCGGkJaBgggVbmh+lfQeFzpD+FtoEeQ7ga7
TPPUqMJ9q0yCW9gjYlNSJ23fgPIfcJmRiXfXfawUxwFf1RxYRHoMuVDHErpRDc8WekOzU4HE8gtO
PbDigHXi7Q028ub2JioYvaX9M+31CL8IOXw560SahwqnZS/ECkW9dxvQvIDdM0LUT2y+UXT3/a7O
BmM26jKEed5LkD2XoU3cRKH6Xayx9Fs/FZ3NEy3X6guhrJM91l1IjbqUdaqnl8obV/MFoc0surG7
O/C19NDp+41H0gIJBNOqYMbjqfFk15/wMzVQvUzFgPdjlS9rsFLyYVl5RxRhKkOFmnC1Mp6whzWl
z/sEWvGQDKlQfQ7G9lZqSJFkmyLVR6YgJRo8UbWJOl8pN+D77hX1YKWOA4FE+P+b58jrge3GWY6R
OOIblIIPDWUVgIVGVUNMEN3cFoPybAXmjjwk0N2eCEGuV70BXF+WPRo4tM/A3qGY+ccEN8wlN0Rr
pcqAUrwsRKnegAQZzqaDDBjZyRfW3mcpVjI1jf7iy12LizIlPAz/kWR2oUOtYbUZud30sJeBWUAo
1EGLfilbHpz033YrHYMrCbKI0WjK4sDAU0oWSnQh/m3jARqjriCiv0XnXhvJ6CB2M3+Jf5EzNrZV
OJ7HFu0Cve7pO7DJgPdWqSXpVEWVqbPsQmL/x+qqFJmGLi5ab3ulImxBFQy8yjcH/rFkHR8QFKih
Bhu0L8PyEdklMv8B1Yk0m7OOKnIBuYokhDuzsUmm7L8MQ6V0nQMVt4TZcpvCHGK9xMXaWWX2vIwu
teF7sm8lE5FxJGXH4eZn0rFRVKsdcIIl0R+rUktfVSyzJCYXPXgmC7Pi8uMtBXKfhAVC7QkoYymv
73BXq1q807uiD7YptayX2uif3h6KJaKUErmO+umbFqsROgFKLlv4KQpIZL8KaxAM1DOkq9Fnq+Lp
jbngQeV90byHVYJjV+oG394FnI4b+rtWLCwLSoewjHTWtS4WkUUt2BocVyjIFLUxiF3CxCp29BDs
bpyA3mwlL7AUPz+J79juDa+rWQc0b0w6PgWaTsSFVUZQoJ4MX/QeRD0tEJwzOUPct8p8cNfcCAc4
biszdel7dkLrgwOC1UvQ43Cy47bKbaDrbGVc3NE9VTQQVjUVcW/2ROPa5sCy3NB0OUV6e2xdcwnR
vH+4C/LOoXItsM41LwfI/VYoeXe9RtO64qr/52K6xnIa9XrAYK3Vw8GApSwa+CB/fYGavBJDYcSh
D3O17V/ssTu7+8OBNf/HcJLfTbtdhxei7BM80cAXMsedMHAZZKrnlvHeZCEwRov7uZIVBqjZFlwM
BYXMw6s7TdPyG0+KTFlu+iOBBcJO2R2dsBbGRxTYIVOJcpD+865d4+Ys7V8mF8UhIZpL/8juFp57
y7VooTB49JLwh4osfgSFtwugdaxzT1apjxTNWz6+UT8od0h93IFa4mnZiJmdPBrtO3q5qUs2iER4
ybkmamK2WiwA/VPkVaDt0LGnXC+X5I55BbQX5XzNksOr09QgYh8F4Y+OPlXHaeBtSX26e1Kkhlco
kjV1/UiNCWbaCV3TQMEiH85/L66MQFFAHdrK21v1mCNwBcR9REBrBs3Rraw7UhQfFQDZTtKpAs4E
Helh0dlrxXFj9hEnb3gxY6ssbA4jUq7PKcV25oNaOMOxcnGFaUL+14NnFpuy6eSluakSHAXgyT62
1dp4f8LJ9snRZQrefMW5BRTPjA8Pe0h0kS1wohm5m/ZzHt3tzYj5R8h7zldWJm8MdnOP40QkePbi
g+5RbZR+H+aDIDZg3UB9CPxX2w1upXGMN43RdZUSAwh7JPnmChw9Y6epBCV+vKG/e8j+3437fdun
B540LgM+XdK8qEwrNklCniNdI2pqFu12j8g5IL134JniS5fXgejWQ3jn+WmtHekTKXi77o+bPhQZ
OFfX6CGk2kSzJ1aWJ3QgZnZPU5hI9gonxdlsjD3GGc1DiRRabCnAuncuRf6iCwjK/gVCAPHANMVB
S/OUdJbBb3leMW9zY+/cQ89OJPR2aH5BJ7Dk0Q2D5m0Vr4KOAJd8TMv8wetT39kdOiUW4vKx7dsh
4l0z0yQttIbTPH/CrTimgR2vWUBebw2GkmSrfhVJ33twjK7T9Zc2Xs0vtonbgAYmo4op3zSrBtzn
L2vZsBqcyZFq+jjgD7BAGaOzLfCQEHlCn7p28Z2rSrPtJKCTBaDdULjCmHDIbSH9nez9sX2h4Vop
BCb63jQgDO/SJNWzkacunVV3SAT06El3HRFtRakLP6Wr3VOhUjNEkSMdoFAfqYrChfYskiyNe8qV
6xHN4380cSNjIOPIBEt4daSJqrRJ3s1SfDWZfDFYtruw4+sqfqZ0HwvhtxsHI0jiP8SoTYZiW6nm
HymxhgWdmYNdqg2TLFfG/x4/eMQ3+oxj5Flfk/Qcb1DgAJftySbBRW70jTMugTsSqdcSbzxSGVwf
zt3al+6dS9Q4cg68p6x0VZlVWqI/L8OEkDnCxRIOWZWfGRVL7yTKcZE0j/uVtP8GfY2Zk/Twfq6n
belCuwLDoBALM0P6V9czhC1xFQ4oeuVC4QmrxRmkr300UseT5jVoevtxNtd9fObKTjuiKSVGIEUY
NQSmon/C5rK//UoVkrSJ0Onk8s8hS8V72Gmis3yke0a3rri3S0vFJr4PxHCOXuI946RqD4X7T3m2
qcOyHy7mZvMVrzjwkIDLMYjZvw7iVuHlPp/WVPWCMCH1I9aF12soleyp9VEPh2QNzyZb1GhUavzS
86jRhtyVW6GhRAFDIbw7jVXJzf2IH80RiX1NQCmwplCn8MERA+hzN8jT8E/ih+CTnAG8+Lq2h+aQ
pluzjyfPQTqR3g5lxA0DdU2yWwT5cdr9H+yl2zYgVSeId1jwBfKLfWqNWjKpYiEEl7e5RAsbnSgj
ic834+et2qAdOaswKVv3wp+nnYzipiBhPpV120YovHCBCF/USSofXAz8bAp8kJg7nrPLkc8b80Sy
NwQrnaPALKtzQLomQY4gJuLY2bCMQeSIQ8gJkq/QH6WMv4CVEh8oEgxCunC7ua+C6aYBUTNSlqUT
Qoy63p4TSHl7pyerTBa8DdFXJSY/AD+NdaPjz2F5tRkkCU+mJ2BKWT/HzDIdGJ0cZx13JSAzX3i7
hqiVihbjqOr2igKWSziHcssZVjRfKr5VOOGXFAAM/R/QijCZ3f49A/PcUn2ZQ7SHm6q+irckpB6R
wSClssUuymjn1mgPwWcLw/MIe30ZwJxW6ckrQQmHXuda2CB8fGljfCZ+tZ97LjZoORStj7qL+olZ
MevQU+Vh55QK8G89CudYO35Wvs4nFOMCPX8LJv5UFKXHyuGiVlsonI6tvKX7LsaubLpm7WdxYksG
ZTFqeyLLeD6HwckvpC+Dz8HMzoE0RobUYxP7hyQEundHBlls+rkDexxYtTt2pMWNWEL4lT8p8XkI
gycTcl+KBJco5vw0xekwfGkXXWs+a9xt80g4RPCHxmmwInd5jMnRiBoSbgwIftty6yOczutrk4vj
pktGO8AhY6qt/eL809uADaq8UYCEHbUA1T+J3KVc0QoY9o/hhef1VIYGpd7QmBjTPyM/EiG4s22U
MxFA7M+AE6fWqjGvUjX9TAp9UEM0k0T3djxmARWc9zu0PaldywHZPBRBvOvIAJ0GC/1K3KwPD0l7
NE+NRvvdm72iSnL3tIiCCk5GDPmuDej2z8X7PYLzTeYAPk1JTzBSW1mgIXtw6SZdvJglFdB6maBH
yXTWCJEitlN+YIIypft0l7+PLpkCKO7YmAkK7gsl7E185ZiaTPKpT98z2t08IfCrFclB756vGp32
PgW0ZktTj24aNMP2urQxzU5IswjmK9zbJbBRfPnVWSeq058hXXRweyhrPiExRqJtH1XOCzx90jb6
BqEns4MmMrFyMxE39Cai62dwScX5Jy23tGO16YpwkKpSZBlBHTI4ZGmpLFG1xJFhLPnbhTfvjIOE
DMB0ohxHQ9jGmOvrUToBJ9lyMyvXpGeq7woE1Ff9Clv8tvkYAX9XHTUYL2GksU80oF1SYWxh8GUL
6maH8GQV8H/mWjNsJzrVD91nuv2C2ab+mlxY6Gqtj3WQf1DT2mCVPbnX5o2zl4GurZZ4wOTqcYIB
jzbZezKp7PtEr9heaRSrne7c6ov5kEyWCUZ/x31AFuotYhGcOmC/sRuT6CdH6wlkuvdwhWAcMTk7
KTQNB3mbNn9v3OT7TQwlBn5gdIXdVtUkS2NW9cESyJAw/veGl8nkOP6NG6Tk4zpu2fJIEKJi2wiY
uAf9B6oHOcOEj9NlvJMaba5vqNXxnQyIMSDWJd98azUXr2q9pRXeKxQ5UMrY+UF+PQAcQf5/O2jV
qoQEvnRU555woMntasBJfu9fBEz0VChIfsf8DjXVnX+LRQdwZmE+jAkMDbmn/ZH+shBzXQcJsE5k
HsO8QKPULb1ZMwJhb14uWo2SqygYLOmtnWLm73YWomZFdB0qWKa1k2TP7XKyJXa/6WBJ8LoQec/9
e9/8EzivYp6WNWce8GZi8JfWuiDe7jkKwdkqGTMmR5CLMce9lPGi8ulbIzGnSRNjHsLwPmpaOdjV
IHqSR/LaqHcgcbpZaDEVEUKp6m7WQN46F6J8JCIImRHND2FWchCX1upVSQEWXtGy8GDvls5hbPUh
09MrgSjlx5qleWJHL4ilzy0NCw5aq0+A07cSH/CNME6dWlJdozCIVFXOiocUMY0HJpGNrEOKsAMa
MFI0AjeI1l1YpDuTWtO3KOTHnCGLnt8yEEq8aQowNvSUTVP97qLkJEEdS5GqjUrTcWYkES3Iy6+Y
E9FgirraDnO42uCy62lMyi2F0eDtKYldvWCeAYdyjEhKbMYw60DTUkQCe9+1gsN29yjqbqrHax//
u2Ds39nsIge110D5q/tmeciwblSM1tJcfMR9xEMM9tIKhrp+d8d5FVrj7qvbeQEEmkZCY/KyxGaT
q+lG1jJt4glz7cRukQCX1bcoMUo0+9HHt1MtEozfYfGf7GdpXcx7WuwU1ATAQ/NHsNki9nc79Yg3
5OdO+jEFYLlrdaj2T+5SqeYCFeBd5fFYubQW+3dEeESPgfyqWJ0KgW3fYVMM4v7x9vUaEFQFevGY
pjfZtndJzC6ci+tzmr0klu4Vn9xyEBWW7lmkGxyNZL4besbL6eGSguZvlwrwK8pRCI5FNAK1Pc0g
9TkeWDjIrNHaaWrAsiiJNvu8d8CR/dfxiacFcZ7anXZ2jjGJlsSCI5Wmwu5nxgXIAPfFYxxMoxD4
ZnD4kNiUys8sFWhqnzjyY+4WiU2OpN4hkS87waA1bc7PP+NKu56eg3LoyML6zO20DatkCkmr7cTV
m7zY3pNYOn/jTm6NlQ/vJ9yDKDjrP1S3wTgt8EHD1dipyBy6O9dNLK4JIn3fJpUgAHC5nsolMivL
YVB/Tp8p81wvpYd45NZIykKNWStpoB8Of7Zr5pMTtOh6P41h+OcqwCJx08F0H7pViG7SJmIjb/bk
7O2ahXwiLB2W+Ouk7+zfuOZgAXgoR/vurG+A0fe6C7U0OxV9fsC0bdnHPhZSInz0HoXLh9MdkvPs
7ti8buECjq4bItpD5Q++IIsBc8I0g5z0Af6NCbbOh4pl1kIemKGjX3DXd/JLIXKG/RamT9fm8IYM
ZktAkGv1kzhv3nyDCaXQD4XXaUc7JXGA/ejVs2dG56PNElPdW8ZUsNLu8ISC9PoTvboh+zmdOaqO
uE5fiLuDwJ0USZl0vwfuack4PDZ92pM2D0SIXM3TVgihPQ098AyUE3UbNGcAm6CH8ZrSIAcn7LcH
kuqql5uBDNKYlWM5NKOXrczIJnMM/830yKhLcIz3pT546sVRh2MAjZwDzjXKBt+1AIoGElusg6zw
sJizOFu5pDKvGaFFY+WDaJi+SDdv/Pa01btl6RHRgX32wjtABFmCdiRY7sA2fXi0izZUhBdHnFtA
WwAqbMiVBHYZXKjZ0OrFThmNt/34TEv/4U/13Suz1u4lVlfuDG2xyY7g3SokgFGJ7kb/38K3RGEb
pK5FBw5FjyQZKGfW4wuFwcmKrQfbWUULF+KXgO8oSJ/APRp3lZ8AJFA4J0MQxQmZn10/yBZt+lUa
tUGlyLopi74BhVKgBFE5tygu97vtJrxMkVggGBRJVNQkykTq3ZwSsZVTkE62q2A5AtOvRW0MkV6K
0pzdx6/joZHB96zL42/owHDkDkpxPMElixEu5wL5ArJo5q7l39XG0F2hCOZULdfS9x4lJFSiRO5P
GRnTcNNO+IC5bVC6x9Mnaf3gbLhinLsY1iqKjnaq0KFQ9KFtB/cLANKEQ6tt1hk0FBTbGLGczUV0
rHruA18UDZ4FaTuOurq9EXYT2ua4mGmZTGV2rTtgE/H5/YuQpiviYG8VMu0Dc++C4V2+WMD1IiAV
2SY9v3eYtPRkpzXcL3eh1859Zx24OxA/HaB9cE/BNGqWGTLyUg8qzhsW+iNghZUjjUGhC9Yu3q7K
EqTrjH0wbPdLenqgWml2EYV6YRY61nirkuqBub8UOpyqpeaT2CrwcPFb5dUiOGdg8YUsH+qqYPCV
X29C6RYx9lutJROTQpKhKYiFOsHgVv0xrJd03txHPQLQqGqB+2fu3MRQQHRVnfDX1EcAmyiPbEBA
pJXTLIPt6fBgH45Nu/oYbDp2MDtRoaeBDpdKHItvnB4Tb9jMUf1x28h8RmfzcpSlEtzRTk7aRDoX
ZBmQpb+7ts9WtzzhpRuH6jruNV0ez84IbJ2R3NeFXaQAMUxqW2JPq9BLJGyR3aZBK+4H50t1qBAV
mvX4z4RmcE7q3VqJZzeD1flZCMEtYMKGA5fr4mI5zQ3zaXlv8DS2LIIaxs0kwRWIjFldw3wEejzU
GPKFWwNAAEElfpNlSQi1eH+VkRgUk/8SlY/uluNy0yzpTcezQcpMCh5V+G3k5ZFH/SKADGXQN5c+
ONYIiI1TgwabPor4uXLbUDkdHqNF6DKL7yTobPjRAHvgrO9X3UOrxtgD5IrzOC0SlyKY0OcHuyLM
xiyedkg9VkeUiBpaXg2saTKY8nvfwK81LrLCmnSwNQ0jnUtCi58TaKFEZiAhs7ndTqXocnz1Ov7H
T03+8fOm4xp1cBz9vQhvd085w6az5fH7nyaWs6fgE4iPK+aKNS/yXTUIU4gRAEzBhFCWDgQo6tAB
2wXSdhP4bBMtGRr42TZZO7g8jcgkO4DUOl1ZloMUMdp78wsWPgNILb6REzLjHgYS9QumZfeCEE5M
c+FrwM0Iqn9SoynfCcxrdTfZePg3AC7mO3N74oB6+IatXKTvQVMnMDiy1WLXuHz73OIZVdM6lgqE
uhh98Isop/iLVXVh4z37JO+/YvkVsBDA0QwLCSOy/R3KeLfWYbKFDr9hveFgKZamvxbPx5JZ4mEf
W4KekDEHVPuSnJ2b7/Xm61UIcOxFr+1JnTfR+QsB4SY46R+eAbffqY9sSsngtcH/weHPYvQcgbCu
6BZTqVcMzggoSc2qq/w7POnN9K2fdxZAA2H+r10Akb5mp8gnknTxjDuJ2Zwd9qQnBhzM2YM00Qmt
R0tr5hevNaLZqkEUmfOunkZVNyBMA9KIcAQhWm3m8zd0bFCfV5PfBgRLYjhzX37ZxR/jE7txziCN
Wl9TJ6Bg66meu912raUmKlMU3lX59Xy9Av24JJuvBvOhWpnxrMJcYoSZ4p0qJxy00wwoJNzl0lCB
3OWcBRqoc+GHeIdsKDv/CodyYqrRtTTh9uBnMWduHIOeMY+bR6r5d1b3xLpl5rP8Fs/NMxl9yFyK
leszS6n8s1C6lia80m57tQ95nZrf37GOhEHxL61wPYR8oRxl+UVeI3QJMi+E64pWq8L6528mnq1a
rkChzQFdrgc4GK3fjgfRy39tKUUbgxN5yZ7NxXXWJr5TElbxgfzUJvw8hlnA2h5odIMm89hfQQ4A
GbxeKR9YEwNk6vZ2k476TZqkC2WqtcFn7+dac+SqnSQvcjEKHRYcjWMTa+5deftW0EQNFz66tbFB
1b0Cq7oKCTRjmI+eZIOyHwrAJTLoj7PT0yKkvXv79GFBqTEyp1bapF2G55p3jWxu8snLh6Cqpp5o
lbgFUjqcoOAKzuY4r/dFmbqhxrf6j4RPMlVyakhJ2yfhrNIqqyWXrNtbxSTUk5baNkHbFu0nrtVR
fVEE4Skhp1oYgox5losrdbQxKiIMXYCXxNjWAQ1HAanU7sbfC3mDH6V49b/pRanP0e5dfJPpajCt
AofyxynpVfNUM/tn399IP6kz2lNepwNNLX9SgPl4g4/3rb9s0Qyjbg+qb6APYDPJERBNK6S4JKja
OFWwVSQtZgtb8ylqZ5LMneUDJ9+Z+rxVvjebQDc3uSjLjsogmiFeQSA9gFDCsxR9gsEVX1Wh3HCj
2tiNqgn4sugascshxWPxEXXTskSUCMUcfyCFXZGlyzB8waBZBlBdXo+FbIXlsBcB9NpVapU1i7W8
VXvlDBsNJEQxNtED730g4/4OjiLHriCSSUHbQaItyONRsj2Fp6KGtZ/fGG5TM38sYOPEJrJgiLgc
oOe+FmywGVBc4ZHOX14SLALR56Mf9IqjQNQvgQ906LORdXoo7fUfyAZe0lDvKx5J8wiAaJguWgO2
9de4bajTJkGTfFR4LoCW0D2WnYoByIoTZzc0HaiClEKg12sFgqxuoX5tVqU0+aLth11MPaok4BLO
Znz+/Kq1X1DWl5wIh+2lg6AlHKHcpYQU5UPPQ5In4BojeFT/arCMhAsjDUG8qbSpRKswt2V/wyjc
pBUfh0Ci/3uG6hpikYiWkGagtz9VDtRFqAQEiTSY4XzEVXve7zqiBJtjorPW54p5zs4c4HXFTgpy
yX0AxAVrAOW18OdLd4t1f04pVIl+pC+rwPcXKUpXtV3v8sMmYYOTssQWSo0s8TrLTtb99Jb9xnZk
LF8tYdeVjScBmdbBTWI+tNAIQhA6TawwquL92/v3LHTXCiWqcznJ+0i/HLptKxSrv2KA+0Xbbllo
NyFkS7Y/Q79XyaIJ8jgRxI3NQuqxJZPWZ9moHg1Vv9l9XU/G1fHm/H7yBussdawbkdmUHHtZOamS
bw5rVPDG9PmgLiFMpBnRxn2sW+MHh1B83rtaQgHPb1eaYH/thcL5cEsb5LpeGg/922efWwGeWv5t
xZ5HV5sXIWyqbIxzH6474ymh+sygWPQRpF4kb6s76vzOxU1H0tLKCka2DXT0drUUzkDPftjXWtkc
6tmi9YfUFHHTxpPnIuWLXqfiSsqRize76FGiW5i8+SdosNVhmwEiKjRxvV1CF4Z1RireZZTe1V3R
GVN+RprnKQMt9QqkjQQeyFK7J0lVxQP9pDdGbIKs7MI5vSveVnnoKKXAZHbmIwrX+EqN1FZ4Df6S
2nshBOOdR+uZRjcdzd+ymmDcyQ4fE7nQ5PAkzFivTPFJvJpPzPQE//ozS6meJnl+zo1ubMFzT72e
+AgWqwdMJs0OzVZJmy6YpRVxqf5hCzZGDKbmcuPf5bkxPDM35u32JSB8fTnqGdIEb6CKP3QShgk2
ZMmp1fJqlP30eqejPgmol89K/9430pxCrJa8nNZYLPZ1DiMuZdDTc1m09C05+po2PWvcM/Vssl1j
MNi85aFx2nEBhSOtOnQyCbQyk6HFa9ivUo2xZB8xbV/3jEzJexQvTTFqzDxt8WisN/1rghPdZ5Pu
ZAB1ascNOhHOBfyKZ0ZHy7eyTWChWZPnB9dE8QtEMIhaeZUCgDPS4G9kI7h4kzX27WJstoZv7NWr
WAhKio4wcnT0zMcA2tFmWpbNA65UZbIrxct4UFgylgSZUzr/1HwFo5q7HRTwbGaJiFTuvIoBQhX0
QUvcVPo81JkYMG1Y8bOwImzLr1pf3zfEqLYFZZc0IXS9tMpIGGE6Ii6vLba6mw3mmvat2eqOuB26
jhz5HRVoZpRGDoH4oeI6uqf8RoneaTbCP9JDPU9bxD5dZY6hXWqlU70sPRoqmY1Oigj6/DW/arMR
gqWZsyLX1Trywagz3tFHetSgHzgqmqV/uRfzyyJWwo7Ba29T7wz0jk1p4XA4JfBMhhQ07JtKemRP
20x+EjhNAsY7YoREUzu39wVAfZkOB6N3rMWjFyB+u5LI3Zf3+veJoB/SOMXRJ+UbIjKTdwSK+3e+
FxTtVvP9DC0U5dFyg27FCqsxQ1o3KQPr2/xSC7QOsN3uWNo6RHSzu7NzkTPXqC1ID960h3/LpGe/
Fnqw22gKWrHqCxYElZVSy2UvvrqMvNZb043mUWiaySbwm89MfI/YTjWxUOD9ciRLh+LIuXNH7KDZ
MrGFbVnd6YBsQtCNJpHQQltwg9opQzlUCRbvIlzuriniPkMibglMjgxWXJy3BxZFAQujG1PZ7HOw
iNS9jRDxnduqTSN+sCBR5cXMCOvnzklz6Wl9azyTl5p3gplZcFP/H+Bp9z5kGE8ASFQiIaayfyWI
y0ak8+67/b4ePiO3eli5mkirtw4jrANscRPQycwQHblAFu59ZA0VKjyxRNQpU1nVefp+wVb0eghP
iAhFTDRFXPCZAfP/wpgo7F5q9VUnm3S6Nf/wTGzoRCa6+75qG3C9M6xoDzVVjjinF5hHCuNZksEe
6DVuP4M6p3e8Nymx5Ii0L6gnZ/eDcbH327XMJICkqirSYpnT3r+a9E19VIS6AhMRtuTBLPHMEwaG
aFHtIsNqnkjCR5znK3Ox9Vz1jpWa9AT0pzsqQ7byoMgVr6VMSNoNN2fu4KOR/rYsplePeNIQWyPs
w+poPRmk2L8/5rjsNWPxZKopTRk9rC/GgFHhmCpa80Rs+LjvtXF3Dbi8u3XGOy4H+0qWeNSbi7sD
gFscO4yCNzuY+85LGNIWLFm4GztjhnlWRocGdMblx6+IFlNjTnevqg9SVE5Gwo40jI7AQ0xXAa6N
1pqdFI5XtPOWTM3FGVnXHVXevjfMyi9CL26WonqaqOQt6mqKej7BswjD8VS8PDVh4rvzdGpeVN91
cCLJQRWNGRl0cIp9xZn1/1M4xOfRxK2ZdffnSe2qIIk7EZFmzLkiOP3ZMmX5h89jLNcp24mbEggP
DmMuOavToewRQOhgR5BodnTVZtMvOMlq9/hUHQtytWSX7XaFaLMNi6+53TuXmweiBmAWTUP3twin
1ZqVgbHm2D+54U/aSpnoUAUu3bcfWRPChF0j8Cel3VAPD0jdlXPFWkBT/VWNqbabnjODUkYqY7tF
wgWO7QZuF2FiIZN+4MCPTpGGRyTErE+0jDBqLKPCHONST37ACg817VQ0uXBABNH7X5Mkqa18DUIh
06QARus44nudDZcqm0GdeTXmxb4wQhgz6xJ/PkqgZ3cSjhtNjmTzgPOSM1ck9DnwHoazeAE9pcbQ
XqATdEflz2NOCOmfTYWNjLcfjF06h2A0BqiVxoAvvnf+PZKjKpPxLdmaHa6Vq5tkbsbB/oZ+NAki
IbCaLPT75wzgEseDahzml4J5R7dkR8q2a0vSfRhOEBZopn0YjRWs5FvePL9nOgq14uCqzelxYVjT
4JgFpzWAm5VVMT2goOssHVP9GNdvrMfaVzmC9vLiKKSiFGBICXsvb/RJYpUp/dEOa/Y1VYiKZrXh
mHEwKK1PBwaOj+pc80B3tU0ZB/O88slk+rkkzphKTGXmnFqNn1AOLiz4Cv/gzfFT61SKN/lgy9Eq
8+AW8qisSfdLBSRK9pziPZsQ30z5nXsBNTWNQ/dnwpStoNoCBU2Sdyxx13JvX8figaVF9w9/jF3s
hTlbie6FRbaTK4tN04K9w9IIzJNFhRr/glwEqTLEvLz3iQuDGw8bRSe0pS0LDHiAlIyZBUkasiTB
TG+HP2iNzb1wjTyZqjrOg2Iupz+DRI+L8zqpUs9oVPcprexy8QROVLSyRQ0fQkAK4dK47pEgqyU2
LOAup3tXyzyYQ1lR2paTMbPTlL0UZq1JKxLLHt51GIs/D+UVUjrFDd0g3SdzaIUJmfYM/1iP5iKQ
cm2ezZxpCXskemH2DNur0SV4MPqUb6gLW9TZtMiq0qfq+pb9u02OXP1ifQ6Apqc+mIhUuk049yZc
SeBKW2yhYwfmMmkIMCZo8iCIEIItXW2MLrnseyz2CsYIwv2CHhM3ASwgPkTriNPI8yeAlkaLmJCX
v1RKYAsB69yt9UDJe0qKJj00WSD6s/KiAlUWaGG4Njn9MmV5pi7eHT+bJMXyBh8BnJmpalMdC3hJ
1DLfzDMl34B2drO+fpRUbKoX1w0fc423pKBAvT0nYxIt6+sVUE0gAKnwmgRzA8jMDo7ZVLy76wvM
T+8h3fIvULrBHYe5EQbUh2nK2Vyi40pcmdHLHxle6Wh0OeWIzYhYZHdbhNzuKDI584HDc/ngaKnt
ndQ3fUbAMNWsn7wBfLCP69z1U5yPH5v+52PAraW1j9vs7DuVa8WXPrJ9BNVM5N2UL2PYfGXAgnZX
SNYXcuiLeTNzjJcKdW7gWG5pFTnoKyEr6wQZ3nzVO/0SyvbajlZ9atjSSSS5OWEjNauvGjtJgAVV
hRtX5ITLY0lQPZWkzqk3z49OKTYowp51KE9DV2vmeBfIzDKX7Kb+95Y8EWkcZX9XoOUh5sVUKRuI
2jj+DA1QlNdGEB/keSBPMCl78u9wWgzyCZYFJao3YwUWFLLSF/ZBXYRlXTg47XXrs++gkfvJrk64
3IwOEdx3KkQTBknkeJ3hNO4dtOStMUhJHIzw/z1K77/5sGfeH9B90f3y5rdvfzxGxeuRbAmEdNd/
b2b4+DrEzeE25+64xdI7oTwdkNXbThZ93XQALXZT4RjJukwGkVTpGF1WQijMnTlEPc3G1UIwvXqd
gt4SBBcFN5GgWLqvvtMI8iWghYPEmKDxbtMFSCtVwE60QjSYrwuMbz/6a5lfsRb58juE8+WOBfLY
55HUvj5NGls3RuWg0IRyHIG4SRQXOEmMNrP4y0pkOAeBK0lAdaHQidpi9WbepXad3P0O4GxUouAC
IHexNFXhW+7rxXZBvnXnAVWUYexgmq+fc/5kk8LY+RbvpBHuZiwZeB4fmzhKu6tTelZkLlSGIubr
28UiAijpZiv3WUEnk1H4ufFXVKLDEeHDhM3m/3MH/Vysg7ZwxcG/gSB41kd0nvwDzp23b3xij/8t
MNSSck46z5lxHKntblOzG8cpK5k1HLVuzs6JSPgEy0HM7o8Ka/zdDC4wh4kZHVjv4h68vbPYRvyH
+q5iJeqa+uWCe6aSh0EtzRCCMhVBcr4aYJd82OtAeHjGIdYxwQypTwm2kUQTdaJd3k0wpsgfsyqf
mw0VdIUlRJwQ3RmpES/nibdUGWfM5iGIUrXhzR8+PHhRdaVLBdgqHmeqfDMFl+MCkzE4bn+oDLjU
r++DmC6Enhx3hk2T1Cn6OmeDO8y7wHIfnMTkRX3FJbfnm8rYoQINDpOncgtD9uVG4/mU/sYTon6/
NIz+1XS/a2ZXxNfPGiWKZZhRFg3dI+IZNPTR+ztWKsd0aOdodzWBrB6JyUCtKhfMp35c0nnokAqZ
dXuS4qn0nZWc7ggNZuRKnueIiY//Oboye9X2Rr2iGuZRs90bTIPIC+51pT8jKepAP/F7ghuRRYK7
5G7Whx1557iYuivKAXmA6w4/OcugItZ0k7Gq1xkzT0zZqt89tOf9cbyx/95q4Z+benyf0ojMIqRn
pZq4eSJnkom2yOI2L3cjRxjv6vQ21TsUciU0R5JeDaWmFTZg2ttTNpya4u0NGziZE/Y/KjeQQKyN
VSCvgtDgsRc+7qXBSDjshl6KQe/5osVM2TXZrzSCws+of+jruGwq24v9479A8pEq8nPIhhxCTOBK
Q40mC48EGcZQLn6pQOKdkQnHHwvdjobOoiagIgfA9Ghn3EESyJYqpvA7WTkRuV1oFH7Ok7+McBb7
PC8B0dHtlL8eEB224rTT+miuglsXBxRDegvJ5sY4YiDY8pslyw6Einu5ldLDUM4i9BGaxdLyy4GX
NtTsXBVbGyyJd+6YT4jK4tkgyamYwwxF5+Y4GsVK3bVnTk2Dy5Wq1Xuq3YMCMvRdbYLr1RgsDrlu
aCAV0jYIc3xGZxE764Jz7W90NpAMNFnlqPsW88MfSBwXcsQ/kOKiSDvODW615FyCVjxEDXBymjjk
eTl4bHLLfout9qyhZH4kDwdiySzPEsj1rFCNs6KeseaeTEsK9ssWAKh0cxrkp+M6V/eqgQQjbjpZ
bTVFNL0KuYJehwL8O9K3HmAKezMFUsHRROOoTYsXFHHNBwoKX/HxBjkv3bTM0tMLQ6q52Uwq4Cdz
sjihSMgel3breBH3VyJMzxdeDJ/VKuPD+Q4JtafXKh7b0X0NdWbe7senspa4uBQIX66sp++t2ZIQ
XAgdrk50XpLt84FqEHo/HyfZ3kVljwBzuICjEEvxkO3xmW4DIgIREbyHu8PUxyoHbinlY4+kPyzo
kU0s9OSE+7Wbm9y9p9q/6fWd6QiuVaNBv3Be/dOpJ1P6z+sQDE17d8YYK6M15qKvzzoPIucdL2T1
Jq647NDc9N5/OF/H6QwAQhhTwTfx1sjAZvZj1+9HiAArCSYwf5aW4TKc9/38i/rgykKJDmUIJMeI
K6E+tVmppSCQyD8bXyMlFRyHuV0TmIRt137WX9Ye6VRmWmaTuNy6s+SpW5h8NZxRe3DWRb+Emb+F
wxRoi1/FB404UxA3dT2RvEG3bzTcxYc4NbrmFbKwsQDa5SnSbR02Q2eE9sL60kC+DtvPs/PuDlB3
EXPqeHhQU/KV5iu4RAvqlczr4i8Uve7mOKFGcfBmo6bBbVuDNAwfixJgjlmTo/1d1U8+Gi3Y4mv0
ALWJYuFrOU0rj/zOazDqmKp9zI+4hDIP4gZNbWz3F0jH81nPY4YXgzcXqg0pC0Vze7uYZ7OIXsQJ
N5lPIyEPTUGAjPQIFIt8IHphwBY0c0NRWQ24erVMJkaJHm++tHCTflOejjzmXK+NzVVZCFXN35eC
rWcsrFhA9XlQ+okEx2mwc9jvyh561MzVeiyAYDPFyqgrBAyMVZ6QP0YQbCKfABRx48WnDl7kKQEJ
zNWtPg3gxPM9zkGxrEGt2g3FQTBzO7Om9D12aY3XM36chj4RPjtaYQPWtTErhPHs73Acb7uGYZBl
T2JbvwZ8DRzo41R6tdK8sBM4q2NOfMxKOtTT/AGDIXjQXNyM1bVPdvHTIfDo6V+JA4G324VKrGVd
ahLGAYssH2EltBw1acAgLlN383fBZCLtT27bv5ddxScsDejPV0EvejRjoQz5a8mdzoOYeCnsPbda
kOmbN25Smn9u3NZiFBe4eUj8qizi0zCg0dkVZvhpqJTpI3/spmcgQfnrxOlhS5qm0bCoULNzVmvb
uWpxQWx8Cd4/zegEzFuYPjR+MlWwud56gFiS3F4RPqJikREYfZ496T6ijHqTUYHkoLf7aUgetodY
c+MmFWbyMuiAa4g3X2rhKxk5a9Ci3wZ4Q1GT4F5q9fnXre70c4gnqZ+gES/RFbIf21cpIdl2VF68
i/c24asgZjCxYgFaXHdjK3hYpxgCbm14zVzUAsKtPuSAbA9QJN4XvN1Ua64wr22MbI+lgNyRu7+n
HiNobdRYJrU4YmJC7omLfvWZTHxlbMAEQfKo242mTJ6Z7sePgkU76t5nXvUdvhmyahXE8vIkMsLC
Ya47OQ+GNveSUhdWv3nt+1XtbefzhDhaD9i2n3VpGlGcc80Ejs6uN/FfdJ1unMlZnVatQ48Ml19w
ZYFTHgb4GqJhK1GMnDk/kff93P5dA1hTfSfClEx56ZvmGBY9PTfis7RhuHv6K2TWcT/fpHEUwQYw
SlSZqEzRylG6eAZfWoXsy8547/veLHGsliHkYR6QQ5/hTsJTHdJCBms74JeyUdRYXAobmDkXjOcW
QJLyU8a2N4Ok0J+8FrUZwpgEpjkpSAtIV/HhkGPcq02YdSuMJmiXK4e0rJf9HvIZWQrL8PLHVYoe
+RYkcJIPBGaEChouWn7sjwu4eMpPwY4rgkiJVb+T0TrT+rqCDaKtD1uq3Le2s1PosVc63/4mjdiw
tTrYlQm4So8I4YEhR/iltM1V/s1idaEYp4yFNEKrZqV25SNTkMfaaOdWXWHphORZNJFqaNrDNQFY
dMJd6jHzvjR+8elwJx56rfAjCgvEi0r3XoM8rzB1r6n8xWMCpdEgaZzLi1z1/TL2q7ibW1HH/E+8
Ls7dLmlIgA1zUuMs+qlnLDzdEn5EyTnuWK5cVGoh7Ppif5ZU4pmEoZaPnQoE89HeMEARxcYrFrfy
32FWmmbKYDsGihEW399aeJIqQWJfte2ydm2zDoxbn2L0fp8WYsTa7raU6efyNDVdycbWeh8uRluk
4+2YDmXWXlujTpzixhp3vCxkHWs27yRpHRkKbGjGyQA3Kv3C+0DRaJKHgthdeKgBoMFSMsthGARF
5JTjEiHcJGKXzsPsPnxzbNyqUNPSfz1IXiXfZguGR4tGU8uvHmqH6hAEjzTEnrOVlYbn1+8arW4U
04yMfOnmF3hPql6sN6UNzsd7gl9xaWh6aYvw1W3aUvkSy5T0TjBzvxYDdSpQJuMSHHmtoxrWayxB
XjMpaXpkzI7Qaua62JkpK0zbat9MyteCggELwIDq/hQ88AziGa/2dqLWCCyx17Htv75xRIcVEgVJ
fdKZsH3wWbiMP8f9IdTqiO6OZzFJ53dkdFBlIeIrtQozPiMKg3PxcR2jlkutDEgQXF/EW3fd33DC
xsp95PH59DAV7ODBnCjar975tacyVe73qNQGKawuMWGqpF3jFpafu9xFWrMHr/MV1JowCHjeLmuO
lT9qNarN1BdqZBnCQicxrH/YbZTl1KOzv+ns2cc5Zvocqw/yCgrpIRsWjfpIiXLK1lbRwsuAV4Qx
WqHS1Ozl6SruirwUjYPZb1TRYwVNmqebEm+2teNfzyex80al5KmLriK+bSaip1LqgZRzTgTmn8+4
9iOpuGO7lcYiLfHoc1RpcV7wnOT11rwa9KA3wNCqktUpbaHAEYMMq4l8GMhQ/XktM4Pv2+YfkAFY
HpDIvaMG+KwXbzLacNhR2DmD2WzhOKbf8AZjK0Sz7MgZ5f2ibeDo6TPr92wHxKF/GhDXoUcYTXd8
HPbafL18tyC90Jl0zp+azR+2f0RG+CcIk0Vwv5Dw3ldB03zvTi8fFVaEM9iR8IPzr4rPKxZv/aqQ
DBUboS6HxTf8gle1Wfn/holkfsp0USqhE4yADHNS1WthlJu9KJO7KEFdi8eFsNsNvUs/yQ3046JB
kKzfNNiw+E8mXpkFIu3CnPRPGle+pdoC8wDADhk2H0lEap+q94kY4VTtvz6I1gIADLR5hksFbNyk
REdoOSKS05iwCWgN57D2i3yw8+Fc27uLnC9be1zx/JNY+1uQ2tCHFiuCZSM3lpmZEbEh0YTWR5H4
CnrUtM6IJfSDWXSnYzQZpP/bw7txUnwOsDX6WByBAeSxovl6rF8ZXv8tN2ZB3h7r/cKiR63LpMh8
hjpIWTzLUfKtY8kyy0iUX5xEc3V/Ug0jLcuNABik67FOtsC8vz4h+LYW85UF9n3iYfCwgHOFt8LU
tn+Pq7or7dur66Bt65wd3Y5IeLejc8GzBAv/qM6blXzNsdLGdNKmzyq7dPMcKWy9oDCLSdh7AmZM
GOtlP3p9XFnDGowcyAo9kViHOyi5uYCcAkxYVZgbASumpXQ96DjNZovhCm0Yp+TgXiCeWbozaOkL
NlSuOpbv11VTnZnNTvruMCTqe2nEpbWvgCOb/+0E1QCXV5ua0eWuJ7bG2AEYsmSlWD1Kxeg/coyE
oT4idpJY6Z03oe533MYgRLehBbh7kudCRMZXOekGZ8vfAydUe46Kj4duiFicFQ2TS7XXt0SZQ8rJ
qV3p46vB5S0P2cYKZx3N8PiOMbO+F3gArJEtck/srcrRVFhdrKb6cAWsA8g0ezQXVtSTRY1uoUtZ
aE6byBNaEe251Lv1wyEmJzhqnYUQjog2cahAyqbFlplMcqQRwv67aEev00XumPkAoWkcgxRD9CDr
p232b37MLY2+Cv+aQevE01EqWhoHIaERhIqzoZmuYkmqt6puKI9jA/jllk1DSvAUtVsAQZ92sKLz
uKOLlauETY0NIsZSxkhu7hj9UwIAreDPHk/2qwbxsornROf6mVBTWHhs7wkzLHrVhVLrWfv8TqEg
DcXexkEzyjpcdjs9R/dSjqi7W6r4ZwlziD/8CEIe8giaIdxrLWaBZLBBgjjRl3BcvVyhTfjsH89C
HWgSH6pug7ZTu994tvveLKs8xqxE+IDiHBehpeKpC0Vqiffzpy36o00aGU4kztFeG/NaTjcx6fXN
ftCnijcOWdzpOTK2Z7mu8tp/86r9MMw0J05LQgZFCMlsRpBsFvTue/i2ovYEPlXTFscxN00mOcW0
MZTYmF/ihEV7VwabPaPJRB+JS55qpr1pSW1Mx19NVfJy8uN3kA2DkIx1SSP6fb+XVcY81V1SFXKP
Q8z9joJusHbq4+AMGMIQB2nPjMPLMCZvDJ8JyYq5Mjix2Ma5XDtg/a/qN7ooNc3d5aT0oOEOXk/Z
LgDk40/THmTt9zWZ5ruSLDr8T8ZkT8hQGwlM3GaAG8k0vbZV0jotUXqWx7eIZ0fEea6pwFAEzm2h
4c5wCvOEe9GlJOfee4nmIMBSjSoYYbJ7mWPVCd2hecJ405g6TjtlZCmrqAzZfT/SRva8agwHryB5
7PmTnxbHfJku9xhwqFdM9bBHGdo+zlYUxhMKhNkp26TGUichGBJ0Hmedx7Gj8TY9xOo5P5dLC7AG
coWl6SIxgrPvceTVilxo09a40pCHIF3eClIY3AzkJOFivcMtRZvsBQySglvaRs+ofM3dDaXXVfbp
J7GNkEx7quhFHI8/8oql5AaYix2WnukZcem/d+5mZtRmH/PWpdJUQsQdCKLFkWacH2xlTFCxwiEf
KkKL6TrhYTtf4YKsnw8tggdcp/sV7k7zVtBwXe3rPa40NBHCZ1j/ua55J7aYD7DckhJ/aONMAIWf
PFR/5gh5SVtfztOHLRvzuPYowxD+tZs5Jn2PGjNF4iGJuooN0aZ3AkSePYALW0rNssB+r/woTXmn
psxhG24mwNEur0h4bR55ZA/EMa1UQJLmRP93GkB983O+lSGcM/GVx1Yt+iW5xMCs/D1+QuwjHPti
Pc9hJhzGDpsZw6UQtr6g6PAZ2/wtdAVAR6irqr0WH/B676HKcGrXaLVaVua06IZj7w3T/pkomSLJ
CAqwqYKz4y/p8+F/r0D7XNHoEUmYKr25vdFk/mwoEgCoXGgssPoOLTIfTAuZ1YqyRDhnl4U2c8hz
xQMf39ylIehmtnDYiJzNL5T0MLqy1GpCenN0aBCW/E9LB4mQC7tf2zhjwIGs1l55ZBusRGLNmpiG
NrWPkBE5QXX7/JncpQ+/l2K9CwPpaE6RpJRZpEsC5aU9rx7zqOFQ+kf3Ry9hywCO3Blm7/dlSUeR
xB9dAVavseRmpxA5nmid48Nuxg8ePF0MEm862LAo8eE6ezvI6ZjkyFHvVzeQonU5pG7l6wbfv+37
pAYn/VKhuDZEQJXGG8UBx8K1YOcwXnXzemh8M0BF6Y8RBxgHW0fWqhMp0dCJLiZf4j3TH/+n9lEb
uY+py5Xo0RWC5pG4uWQ8o+ynAbm3cDkCgw5+NXMe1v5wsWikUr260r2fQifxDPjMcn6inY/mNHOP
P5p8fhfjmuhdd7m15jYGVTwkUqst8w1UdEMC0R72vVIZwEvYwetjIzYt0hdAmkfAH/M/7UWZ300F
Sbq8GOEC+yUvrNiZxXcZUeDbduxRPcgHS9SvYy47TVsGN4raA5dC4cNBuzs/YUPC10Bsn5J+rNvB
QXtFmAqbcIv5Io0M0K5iLCrOoa8Nt40XERtmhID8lMX0Bf79cKt84dHaEsOHHldLBxqnFQJ9hfJE
lVHnEpy8biOjPdVyY39RyhzPOa0OvCITBu4OPs6AkPV0iyTkXqvrWwCc+apaY1wfpss2ftIXKca+
zfkmYBq1US2GZYcpuljwVFgw1HQhA1A9FH6prwADAFmPlQxKQsDOpm1E/VtQyFjkCWuqkqmg7c2/
gp9iNzvRrYD2zSx5V6dxRaXro+WEyLN3LKNDx4Eztx84k8fWWFlZRmEglQx10BPnM2U3mxMFo+5Z
slRJwvSYagaYzRzVi9hRUMAovaTKbO1IbZQAg+K0EiXYhvDAFw/Y6wpcwyDlNXqWHapTnUc7P4XC
Cc52YVDlKX6doZ+xycMJDM2WLaHirr7RbLAPM5HGMc3E01hgAuVf1Czpgs45qZz5DHAIpKqxoiK9
/0/q86adz3av+2ZzAiDeSdPqc43WX8QfKF8y3KssyfA2vUPrdXqQg/7d1sZRHc3gpBA9GvxGTWZy
udt1J1kauayclArEnGeeL08NU54yOk7mAogkoGPCl9KB1iAXJzOVkbBYY8oNIXgk4mB+CNHzRT1h
RPBEvLu8aWcnY0/X9FvC9tjWpuUQb96Q3uzdnYTXrCI1DZEZhWD+D0onpFpLBHNveP7N2WCnTCC6
RetzKLj20tZA4nQAuWG+Ipxn80YQ0Lkyv5s9cTL2TpgWkoAbtWehtvFrCfrNYDY4FXSAy473jgOc
KdPqNuEgIA9eKUtk8fnauHEAtdVWTCAv+a4byEErrC0y6BUlQIela+jH8lSXZlUC7E4Ea9XzuIAl
Wk9Zv3AIBIDJDsLLtw22LRqFFzjZ1mlFlZAWk3xbxzgKHhR4d59Hdmc3ayivptcvyZqrYdP7yN79
mvGe4pZI36BIjcF99dyZIoiaMxFA8KPUcvNIrVsiXOZU4oR+Ci2pjdeuGy1FJTgo0WqifwyVZKPZ
imoYjjJ91sYUTWVBaUwzC+0/Fom9p9xXSKhGK2Dh7Zdo5PPuMOGra9toUHv6NkDsbcYHzO9nVw32
6ndWDicObcqS9f9gZxJ7yzo6MG9x39VL1n1Z0yjfbiqBYvYa+EnixMM+CXAGEICCXNkmzl0Gcz38
882TwtZYKqIPwGYNvlQ/RpkCKrz+JtuNlmYZNMw+rz1yYeRRL7WNKVwTUJwortJ6F+fmaKVRzBYx
qRD9jtW52MDHVUYG/waMguxvLQShJIkvn+1GLXbjfxpYrKALCSthBTcnVH/pB1oc4JsgsFtiLe5u
7Zhu5k4W6c1AMVIeldKhz9uH0ReuSPRaJIUHQbXxv3+zlI313NHlE4CaD1+bK40flatSbjcLFqfS
2JZ2prnhlQzzpCLyPlTUL+iDrl9e4HB5jdT2iLOAM0WA5MvLo58uSoKG2aXyllQhbQ3kInuXC75r
vaslTi5Hp5FMiQXugpWuJUyGVslatZChUan473AzbJp6j+sxxXUpVrZahMwuM4UOG09MC09fOD5/
rbfDXe/ZZGq0KgsQcH3UYAEG8CEBcoeONiSUoLJ3ZF3hK3VOiDrPNDbS31BVa5O8V4SqZiDCz5k1
oJWTo3wRdemdWDHdMwGbBVj8ntI1qHvMeZ5vJMiFYcZOCcbfvEjKHWODSEYZjZBuAsnFp/QKwM4e
NXpoUO9TrVq28bpTqoMg5zU6XDVlEU/BQRG0iEDDG31gCZfke9ZfRkH2V6gNA0NdaFy7eFN2Bszu
aAZRuOSfHOsQLVY8zsfexO30uKhXStyORH/fbIGhso1YJkcrKWOjBvjdagzqNX6tsyTG/SQdpVPy
GwBG9TmCY8mJeXVgpV107qilPjQWFOtc1vGdRmMfRg+J3ywPiarJCOcmchgASWVL0gBVkIfbK8Rf
QPSTcYfqhdhhrwuQDDgyLZiwlRnzDs3/dKy397NaeR5cYZEjOYFfwD1Zj5OTkyi4/zeKeQQoHkgp
15hgx7CEpUV4ryjK0NBpgMW5stp1c7A9btcwhlARA/JGB9OrAOulUxlBd+u2J9zb+j2cgIlIvmkt
yoazw0P8BZbMEINQDENPJwBttm209lXtg9ADXKCJura5kIrdJcw7h4Xj/Px4DhSYxQMMajUdjqrE
Bh6PvoTCYrhDaaf1SBpEu+WvSIZiWUW2SvRKbzcgAnUXForrK20RSBsK2XfrOtrehZxFpmMlXxQJ
ZHlDQFNJNnD06HFpa8CGVhASzugLsLE2AyK7hvw2h2bekg0c09Z1Dhi50LkS+NiwZNQwEpbvPPPw
QVcwQfx+Q7WMJippP9ZcXicD0/M/42Hu25t+O2kZAqZgGiAAMCdPViwPLHL2n0BMyFD2C8mK87Cv
ltfB3Yzj7+E+JA6WlkD93dXGkBBvhkA9I/nRR5nTeGr148bE5YGNLu+XnM96H/OPWey170s0l99F
8Gixfjaz9nmprN44y2ikbpFB4O7sOuYNBvqOLycyXpyh79nu6Y6oxgVppDER5ft+svZmcth7ljWb
RnQCrRgmJg6rUcU4AL1j/SFig/BcAWfNnO1jGZ97TtdZK5R+awEZ+8Q1FB4Ou4oL4jHCLVeJp3bY
bolh4LcbkR2GHg9HzpsFrQIor2KJP5jTEa+/intOxGT2r9v4T4AFXA/skAz8/rRnTPEyW1N6q74l
RNvjbVR61a70/bY6qGrsSCQBuAtaan0I/9JUL4opiQjgovTLrdNCilbo3WlytjfJZ0YFMA5jX1DO
2PzzjSy5zxvtaDHUZEKiJv2kf5+2svFx1Fmq+TItrACmA7SXUHmT5lxsTIQNn4YLd5/9m4/ad5YC
V5vAre6EIkLp0IlyA8QkJLMKHKtI4IcHXr/L48+3GDrkxOlPKFHJf/J9AIubLUo6+V2Pix3BLUiF
i+V3aFtrFfu1mIe2L9NymajwJ+1t0zF8ayv8fYmmrmT7pfOynzLOal6i5jI8TuxweWyiXr9z13o1
NxrhOc5Y5QslEAGkp0qYSTvhlfSNJ1Pxv3aMIpiG91w+RxsGviXnWrp6keszaZgrDP1ho32LacAo
BYLi1yaLKhyLG0akKFrOiMUCh8jrTaWhNDs9/9BDbtirCZRkgHh05NqLpOo0gHJ8COaFBFr/MLpL
9vrl+6DI4/DGFqxczpLXNvtpWecjO+Pv/lEmJ7Fz6w6Xw05KlNtHksz5nuvyNIIezooTHfPWEozM
AYmtvdrNNrx2greRFQNjEah2jlPBCczBPiQ+HKchCNpr9X2j8paDxwoAGx6jjLxrsw2zmwfiRqlO
eOqwW1/G9Ger39iUHmABOTyd2kal6ebyQyt5EvLIb9FPzjivnIzuX1xvvAdOUqtoTN+GQ0wVzxEY
xbtkKfeV6fmIZQqGTlQgv7SwHGGapuHWndgX4m0bdddZKmDNA87hWT6PZoenSVKacwecGXiJJsoh
cj7bEX+ONQV1tuLqzLZ70DWCl0X85BQoEfIcSUZA/CqynlOje3uViei8ZoUWAQxsey5laFMu6gkm
+kRjqeOYJj5Y3W5BrUG3Mk9i5Li2osrqTDdVedUps/CpouemmoLHa0cG06cESj5A3Xn+YFDsnsfP
0/CA7aZdc2LAikSmmFZ1WRv7d2mMXKvkdBkYWUDgmYU2X/E7jwZmEUrIQnqYfGVqZI+LSrYgN7H9
AhCvHhx3R6VRI/eC1ZKkCvhOJ6q7eNIngYGspFPqRNVHP1UowDUZ8ZscfJQ4tj7wUqJAL5jjjrBN
Y6wQx3/fOU4ylaDFlZ9aIVNgHZEhxfLPQHeWc+4xbK9DbS3DQxh02BDJ7tB9Ir7tHA7OvbhkG7iP
dn6Tqx2BkuzKzPR3/KUU4wiaSYJpLnkpG2z9cFyT0iAN3sekUMxA8Du1nxcMyPLLAqRrFHeeq76w
1qVT+NdBGoeBnRWZD0XdMGZWRtA22HMsMVWe+mnt33hcHe7U2zW50ZAZqpwhSThV72faYaSBpKgT
VQMU6accbCr8f7O1VQaA7yYV0/HgPTSTewuItSUFByBEDrvrDLaGUugXviAb0oGCCm7kBcAfXvVb
CUFfqv8eBmI1JK4hGjxC7RJG+7U6kll7t9jqTJGZkuEyD98vPP3DbU4xdiiGgH40x8rtEIYka1BN
FHg+QEk4RrY/DZdxabUX4B4nDaL5NCspYgBfjus5c6rAWhvu3xqkFvJoSW6rcv4NdG3/EDkRvBNW
9IyMmKHmmOyyc9lxOhYmCFD3rR9CYBwXkCruoPZRkSfsNUCB7IFynBqfrGfaOnDZXOIC7XeRaDMk
BYz8kot540u7yK+nG+iQmyT2pVoYzvxQCm9xk2lJHjvVNDYPIcyJwKBoazWPRpElbRo335+HFlKk
tUgjoA+lAa7Au592kG/zoD40f/QwdlLcwhu1nEY8Pcgwpsi5Syl02qq6bpp6KJjkRVKEF5xTrZTh
NTWKepibtXIdtGm6GS5xZN6fMn5ApgaUg260HrQNUjXqxWxnDmvG1qZl/qsqex8/D7iLtGwJmjsd
CCn2EZg2N4+j1ITAL3bdRPROa5oLXJ0BhX/i/ZNjmUVecZHtaVFN6sVRBVguY/tYawtHX/eRtQ/a
EjDlOcTxD0x5McgxurzlQtAOii2K8U9OMRI4HCkwhefOgKsRH/kvr3aRiuHkUaHOSsu0Gwie+1Wj
OtROjCoHfwjNO7EP9Q8DOS3D3xu3ZlUELaoTCpv3j8pB/DQCZGpC/O88LVqjZ895E0GAio4ELJbx
9FOd2cs/m8JJdWNL6K4ukvoq+0Hk+1Bo5XgtFHMnAV37ZtvFFaW1rkq1Nl31xKgvCwvep9SRpqsl
gW/EhTMhffWiyRBlVIhi+r1YBrXYSz1Vuwgom2wO0pT1z9bCgFewx1NbtDFsum0tuAFftaBGfhgR
Tvog0vb8vdwCtZNMASQixeDnCZ0XQfw+f9S63ijcm91BWVc7njZPr9FREjfZk5c/PZNu9octT8ZP
cktDck4UXQWqgjX8oB17L84eBr500YHdtEBNEz4IzD676zgmIZWw/HeTOOyt2kdiX3hUuxQU1HJ2
I+N0rItQNZ3Linkzj9f9TIHX1eYo/u2B/BGLi52YczBYBrhao9AfMPfGZkt4QKav82KfKhB3R9hE
2+QWfYqD0d8eNVLlaNYn4SzifqmNuOIQrtxNVFaky9XbF7sFwrk6Z+eTc+/N5zZKbTOFlxjDKLSB
33NzCd2L/XsM/10PmGJ4ATu1gmFRGmb6znlyjvxGq5z4xcdzwaTxzB1WcuPtGPlPg+eDyBaBkCig
x8uPtK4EJ38Stg/Fy1LEID9hAoUDNKgKAs+q00PK4x5QXNZkqYVpajPyaLnog+n/whcKyWTqNdFv
hJSci1giX9rp+vTGf2kq4ssO4T6EwmqQBqCjCiRRtyhxEtYPEPXtanlT+tJ77ckl6o03A8jUY+y9
J1LJWbJaRj+78LLtmuUxb5tRLc/4q1ijY5ic8xnaKEIggdfTfPkLxFkA17MxsMCZZ6S9pGi2EE9q
zF7BgrXKKTfDevV7YNG+ndO65RuLZgD/4QTeUe9PDJNYoHCyNxYm1DApOlVj7fKu02lDrEg7AS5t
ayZi0IDrmcPf2NoLUrbNNPCNlVssKFdUd6hRO93YRr1HEadz+RnICOL3e2+kMv/v1JbH/DqokED5
Kbz3ageQJfTC/eE3+Y9+cM4ig6BuvsiDDX2c3YZGaN8ZR0jXds8rkStx5kcMxf0yGmFXGWHmPa2t
4g9gY2wn1kCV1s6jXigXni4J1LKDivWeB5KjKK0ZcI+xeiljj0umMxC9107zHk8HY1A1Cff5gNyb
Uj3rKFeeGcAHE06DuCOdwZapq6omv32tp9g8l/N4PCY1y1Cn3sDN81+rZtzoRRagB7LQBT8ryPe2
r0NdnWiNNYFyVZaFFTuDqQ5GnrRb/5dX3BCrMCFCF/et3Z0JLbwrc70xBsAVeHe+yLSZXFkBY7St
lSDHS/ls7LMOVdC1ylHFfNuikThJUgFmEB1brfDaP7Ck0d+TFZ7WrYNt3OaWxGA4BUBmuB03dy3D
HytafS4IbG5QMuM8SG9pfuPxOGpwtk/BbFdrnslJYI/dxn1dStSFv8yWpgr/5HjTEbf20jcukkcp
3YOizhb46jlAgi4jLPijfxk9iKIcGNF5uyBpCI3QThwTrRIfI+RMKCENRzieQuCImq8Otd4T85r5
xAQekxF1BHmc1zFlOdb+iLMXkDMutAxfdx8cUleGbFiHEsBW9lca6CkwVU9VWiDQjRgvwULEoh4O
D63vUqhtNKWbkgqXod7XXhtLa2/h3SEPBUZXkxAZH+DuKh5JCifcELpmnPCeaMVprs2WQR7l1MQp
98oLoxRxgqkZC9qkcgEk0qsc6lPeT4rAxGx2bILvUG/CiLMFb/ruyzv75TQRi9jaiZKkdmAOGKdh
i/PEMKtt0zAqmmk7A/TwevbLAovW6c2FDZaAiHA68q9oDDL/amieevbeS9bChFIKTONOKv+73A4D
qwWV+fmrQLN+TEWKwBLq4cOsJ8tsFw5Rbq83gMSuo1UCme9EHRReFi/8ZKBY4WDDijm8POAsWJm5
kSLGroLVBDURda71T5V3CbH7mOlCRsTuqmFWXP5rqngyJWEup8bF50R4lnplsox/khp9H/sJ/f0Y
ASnVXGrmHVV2JRcFXlFkxYyVPNEY1fL8k+7y1HATr+9TeMJ83lw+BsgHtxAKR+1Y0NLIQkre6WZO
n+ZR9+gmeI05IS80Yo1Vz1RKXzsbhjlaq/u6V1yCjbTMGgmkBNJ8HbnDM/CKsOshPe0rzvPL/dgX
I+yhYfmKn5MSGZqUMvlaXBL8c/np2CUyS0Vdq/tGc4yaIz/hoAvGOyJCbJSJD8/aY9YSKm6G7PjQ
vcliCGhOGsASOPHlgopN2lZ66mioJDxMHJT5f3zKLwP0sT83OGFqFzwoZUn3aqYZTitObueq6R1M
MRr4HCFOOxy3tXmMkPvoFUkOC4MZjKQpo/hPMbm7TvQr+GHR86An5KLh11q87DDlqGr9SA1jUq/E
9DPB583JtDhOFD7DEyVg1hRb6Ae6ysMgXgX+oGHh98bcrtC5wB9BswDUWa0uYKUwlY81PDfmjYeK
FxVH2t1PpETpW6hGhrJ4ESrVb1bHmyIcAmDvqfPnKae6T+J0Hm5gLLLoLZj8S4p7fT7X4DeY9d8e
v5j2/vKMwkXHWT2phnC3cVtEw/6/FMqFp7qPOZuLbWTKncu4pDcRrkiukX/pEyo1MRJmK8GtWFNz
U0xl7agtW1wE7y3YCJPSTO2eVHlJLPG2hwJ0nIaqbIAD+RkA81tVXcOYXpF91HqVonqt4QoYLdxY
itIqAXGdzBx63+eLR6NP8EHeRs2xyTzW4fFoPwuwnJCszK9TgvMeh9gGQ1xoX3mbO1uJzdXMSFO7
CeCX+sQnO4p7K10Tf5hxj394cUolZWW3jisTQvE4vsheV+wMcWg8mmuJQaV+zrsQxokIrZT0cZqZ
dHnYaOGIel1CPYUj21NIamxYDQyXH5Kfqyg+amZasXk2GvvddTy9KOl5Jjv6HhLykE2z6tvclz85
bCZUwWZB+weMAvkH56EIO02HQ8L6fKqvqe0U86CpLOTHXuGgbDeY5eGc3Kw/j5ee9mTQJNsd2ueV
EK7mGoQriH5oAX3l+EiTQDSzBMEsvNdAWaRVsJBoEJjgADRFotkozZC/qezkR1fwfcD32HPkk8L0
gBJSTliR+ugDhzK2vS5FjGqoc7FZ9kOg1gsvaDLVrXuJdx0WY+24V++jIQ3aXNPf2pRgAvWV1rbZ
qPG/5/WFVRDOWby2geKMN0X0tB19ZrJ5EeiHXLGo2CPh50j6bqvCuMcSDxtA5+h8/bICwaQ/0Qbx
4gF/XjxwdcU8upjI9uhqgutE/7quFgh6XYHS9D4P/PciGxVq+5aj+UZFsQTMnTsWgov42l2v2Gdv
KXsD7BqgvymWdPM8yGrZo3x4GQQRzALR8egMgYXaJKY/frXPoIyN5qXZBaCbOEEf6o8fBamjun9P
t+D4DHmNOgMsduhz9yQ8w17ODz2oYA9GTstherG9M6VdpEMjwYsswahQDjUy7Cuu4QZDLLgCRha5
58gK9Dp6vub2wVh6jmpb4UQb8Ee1AEsdgoIUJvqobVTa/MvwoW1MasIxxHDqis3MsVVxpDI4utHI
nfHreBZXckSViFdKiFdvnK/+QE60v4eOW3q10trwRgo8AuT7DdrA4xDI6M+yAHeiV6hd7ce+mwnW
KaMHxz0UocQbusSC3G00E5azlsSKkqKXD54e45vSg8gXz1sj/O6WseyenESUS25bC2kMJile1Yu+
NP3tvR9KzcuOOWLqzoaPHwbiI8dNgjmVqHdU6ihDgJ79RReV5GYqvETfhlWuvwuwdcCJoTh+lIu5
uaoC79PBzqjTnYniIVjTxRDD9xDIZqVm2tB9JZXQjxyuMvaRi1UWeqMuS4aNl/+HvHfS+5egF9FO
1Sdl9bp5iHUJ7S0J4makzRkLRtF+cBK57TxUrqzIUf3BhlhNjA7a+sx92tkM/pZMHd5VSqPv/B/t
eWr6I1n3Kse2ij28U3PYTAsxqpigOAOA1OxSSbnCkFOnSa546sQiGACd4yEL9VbCUtQjJuQhPdQ5
8LcAjCx4OWNbJxCDUQsc8d3IiA3CJ/yiYKRgYyu9GFiI4rXh3oQNsEUwJi9AK/d/URWtYP6ANuNy
YuI7TEczxx5lVpkk/1q6WAOcNmXcszscLxdShXfguen1MZyCD9Wph51yY4SKCCoZX0uJ3ZDeea0X
CLZgG1PP/q7r0zUdhqJWAOVFpEleKAAny1/I0h3nJQ82uD7/51nSnR5vwwby1fLWnUNAe/Oo2yeV
7TW2mnOIZQQOazHruUolHpQofkE+o8gggIJnLtlcranyT4YW2e0/SL8CJrpIij/qwzvekd1MoqPC
Yja0ghNE18PaduybDLqZ6hDLSfAy5qQmnvarWPogjK/aTF75rwSZ4lCBx371U2iDyjnp7ivYS/UN
0uomvx6ym3LpGfv+EOUqgTG6xHZ5qCuGvUj7kWKshHk1ouBzqNiX0/XE0NdnZNHqgfik/pjeB2Bi
XbG4xdNwJ7oUytWqluCBi3wpJWh0vFe+L/2c1iobah2mV19C70m2hvbMK1O0SsT+hB1svvAhnOla
rj4ZZ2F4GChRJZVWCQx8yaRE9drhFCXrw0n5f2oHh5OFma286uvcxpluM8H52evefDTuNxLMORrO
9VeR1FXHXF9vXLfdrPYPfajbWW9+plBDCeW45QRb/AXQLvchzl8dSNR87XeAju5QocmRaZW8/WCt
W49b3ZDn8PXo4ugndjK/A2gvQ84OIVm8E6CpB16ZKzUx6VLV97BoWzaSYB86k2uBUEp2HVyyV37f
AHp8uRTmFbQQEcN+4445OB3rS5nILopsRFhPsu0Qmdn8ICxPp+468G4dheU6YNwvjs1HQbVTZPUg
KS4tBzVKPA0NP+DDV0CNjP9wcjLRj3pNDT/avfGaW/WZJvlnyLQsvd4ZuoAGM3JptaEMRd0lz0ri
r3BbtL1ILN+84i1eTYbZl2R/+Y8qrA2HATu2iK/8zpqfZmWbbrhAG/z+eu2jpzAHqZ7GtspgZZ90
3cHSaE1W2hKKKKOW3TmI+VV/fwo7LMrA1jx1TCGQYQWA4UgsGaC0F4REdK44kfEqWs346Kp+x1dO
FZHrgDaNuEWHXuJmlWzHRV/cgIB17pFyTX9Jl6kt8SfBuZN23XZH/OsYV5+pTzE/jbALL1bQL/9e
lavwmmxilSyhRvadBnzp5mfoRWIdbJLBjmIth8efBI8HXsqEWa8Ara6WHNCOZUbSY4y/Npa4Z0Ep
jkLC4MEGreucY9qX41HWFIeHV+VGu388HYVJcLjVOcclqZKrWn6/EfcE5OdkSFtmfKNqGx8Z3A4u
4pXJ6rbX/eogcxuWAGbAvRYg8RKV7GzEiBPep+7QTqzKIQWhfRR+suMosXMorRcnoJrBv7DGbIJc
+Goarw64gVFAcTi7FsAfEPCJB1eCiBzE37/NDsFF9yTMF3Yz1Yi5tjC8K7qaz0ijqxcOqPSENDnC
3KsNdXaudZNDEEksT1DAwSPwxYGmx/I9mmvKM4GJpbavCVNjR72ZdDtPLgQDBkKcWI261DsWHlAT
mJs429R6HwleILJybDow0y0F9n7vak44w0MqC4EtUAxTY6eroQ0/bQuU6IXgPuIVmtBVuOCixrnF
yHmkvKO3ho7+zc8nbmX1ZrEGblG8VMdWt2Xv9B0dc01TQ2FS72lhynBatKvpRgKrVTsvctguIoAx
f4V6mSyuiGwxHO/iKLBAfSnVtN6OMNOrCnVHsKU8vvis8qpWtEReBmCxw5d8R5gsomiz7mdOn8Cn
KBIE8fKKe/UTB7XccNhqBRWV+Gf5/k5IZkd56GYfHJ1OlZlCAygF+opGdEmGnV+1ISg91fDbdIYO
bDZeuDff0Qk70tGaSP02fV7tbhWc6RrDABn+WVfGTBulKTE7BCZXlH42IVFTkgpmZfZxZHJm080y
RBe+WThz0ktUrMpH6ChmCD2ZFLMkjXRSqKNvYQBgHthWdQ1JsR3GDQhezsRAk5ihVzuWmarh34o8
UxAcsf1m49Dg553FCaMmyDJcn+LJvXcCMU4NhJ35srvjgk6RdYmUfhB9UVgurfJ32Pi9+FwByPIs
tCVi7Fzz9QMPMQTg34WiqNIEuoenvsM15NWLohL+RJrqNoSREFvS/qIA/DRzvQ83MhLvi5g/g+gB
pFXLyqf+BF15gHp6Yu0yEo43v7c6cez4gKrgt849t0Wy4bATabqX0CVp222udlhRnuGlAEfKp8iy
VYIjRS156FFJsygJUCiMqO6btMykcdJ8G2BjmjEnME12yOG43ZVelf/PZNHaWb97O0Uxp4AStsKw
CxX+RGsZG1qJWDxtKTqCfv5R518yaMhntGcHuAR+DiPse4kVxCmEkc0uoUQj3v2tMDNN4EYheIdF
ejOCtzFl8s9/xR4FgypOW5C9xP54OFKFVUe4sob48Zm5Fp/S5lTBKgUZFmD82oOdvfeP4UPbWTEx
bNQuLpG8R6+OIrZsa1Lnxv6KxEToelJfxNEzr7pvddZsNBRHpxvmvT5qYUs951fyAQxGckdbmKPr
CKymQABigD1ufRBDFMg8KvUuMu87XMeEfV1tmGCINbenueoyXjgU9y9YQZ4xvSQH2Ch0lfE5Y2gI
Bn3DRoytZeCoViKxxo/09UgAmyzh8BowH1jSXM4+FJzhxB8Xzb8hQcVoOm1oHek4/rsy23EvvR65
YhbebCuGNIvHfCvi2jmv7FL2cgTjFCY0CiL//B4rKKc+3nnhnbH9qzNc4w2eKVlPuOYBLc2YS0q5
0Hh/AxoskbqnR7Ws/UISIja8aNMuJNJcsSfA9JBuFnDYehdRR46/9N8ixKs7g913RS+IiS/muyv7
zGezhfqcZXxTx7wGgDSKSlJPw9vPf3Jsw2povrgVXEt9/CmpkwH1laBngCNESM01jiJMBCIQa1Ki
IAD7f1i0g9x94kO5D2MGsuC1iel0FYiH/trYAqYGwbUdeSXOQP9G6BwI14/skdq1Xj0WfZ9h4jAK
R16uadSCLKcJ32fMJYc/PmhA/qUq4T+kjGrhEy/lOpU6ViWwnx7h3hvPK1ajCrZjKUjYZ7KySzwc
/vSVjwQyp2+V+PNg+IIXTBTdoMsRM1VR225VAVMpweOQtyBB5lQcPfOoPEXoez6zrMbZ1dZQvumR
jvwdfKvjngl/EoTaW6++OxpoUws8jm7n/CPLbztcLjCCr4r0tH13S7j+PmkKshf2G9FrJXOYe5/W
LwUgg9emcf61IevIpwXavQ4dTJINJybOj+qO83pmOuI9OyDXVAgHAXeAOYGlKu9YPGwiQgTdm3jf
JFcypfkUQHqY+Rl66TsD4+6mhW7mAM2+DM4+zYaQ9x67yUtqZ4W9XSJ2DXsFBBCpiEjEVnvapRwb
UylgbyWzKBtzPW/NZYbkXzo2+vhfC+yejcLGBO0yiBCrAgnmtpmvm89MLopBPkD0S2cW4Jx/1lo0
YcJMWP+iNoCf/lJIyURKN4is6q5ZY5Vfa7+SjRlJDruuo5umFMq+5h78eNpXSIb7fNHqwo3lVB54
HywiYUt2vOMQowmcCOqF37o7Dhh4nFKiKiFNMAEtIxtb45v3YzG9/UZQpyQdUgeQKKDA9bOuGBV/
lYzNDGR9Pnor81z0nsogDKYydrePg9iQwwbxC2y18/+tz+dNSBG/PyiOHuo4KWP0/jl9w/riqE08
0uhKf4SJuhc4Lxb/SGj5iMk5j6TRkeG6OwtsIS7tXIaU67zWdcxF5fbtTd/dUEt1yg6r2YmbPd7i
PJMbiiT4WTkY4jIDNABhVQ/ZG2hxpGS3kE1dREary94ABDCgkCQFjSKHeC/Id2ZIPicddxyNVKoV
gjHDCANY+qXkXGeQd4t7tStTNOj6Y0srXfyXTOJ358D9y9nwn9CzyVJsrQ9jK64KXrrjPO4fEWLm
PO/XX4jN+2F01t6npvjGNin0gO/5/RUJnsspTVaKQnmNmOeb2sB/mlRCwkbodMlqdNnf0T9JKCxP
th0VqwgdAPQ3XMDCJhqU0mnW8bNwGjq4o87MMSb7Y1wXDNlhysF4NJ3AhGC5X/QDnMLO7ObAJxao
YEP4PTMz2SCYsByZsDIY7eSv6DFCrb87A7IANkRhHcfuESCi8VLKKnCBYnuhUSX5E9uctQKXUOdl
2FbieJBDLIgQyotJtNOTrchUs4SAzikU8nUJ5OOB3sAzBh1IC/4Ya+7c64VTQ9X5VKutyPQQhXUF
Z0aZrjAScUs9L01D+eDrOYaBlWsPpsslAuaJkcJhJjGRTKiVwDhUacriE5NHyRW5rE+3wLv1bP4p
9pBKWaJasZSWhEZMJznMyzePSSXzgeWcEN2dHJK0UbKunEnv/mGNItro4gw1bo+bwxhLCQmo2yNB
2M8/20tJVH88XeEae6qEpqpyO7O34ffQ7yBWERMUTFBdY6eoCubiGlgsJ72+vROTn561eoaafAIO
5jbkXfJ3lBlLQpVA7ybWtsiGSBU6D56EPCc0xjTm9aPIERqmS9oUkMpmCiTkTTGa0kyZxWdG5ebz
rHGxiM+JH0qZeMJmpuYcryfmlJcEN9R4QHTnYx3Ak+CkxOX/uCvktowDWo2QY+bz3Ru60YnoCwoy
9Vm52cqgSrCx2GMuFVipujj8HnqHcs4qvbZ4YuyFEjZ2AXf81z8uH+bRrZCax0QrJSuvNOSX+LWY
yYJ52CfqAmlWzxRAapmGdzQAESeWnayXb1L0HahZ/c/ww1m+uZwRA7AbXiqu9qE+xoZoHrUTcdVg
foHCn8kG0QZYWAMgZBfFwDUgp87vmXd4MBxamAHz3tjv2Nv0JNE2zotrCZyc1mYpiOvCLo+6GrZg
MirLQF7UFlyhFR+YX7t2LZ9tLX9hXE2kI7LpJqI/lL/gnUSj0pIfOw1KqWEE56xY2Odlp0fYVKiF
Hqljoye/W/2olw/xlkcxHplp2gCtG19paNnpD19uGpPJO4RxMC/IW9B+87B98Jo+6Dwqh14tjw83
6GgpuJGkzZ2KXvUQ2IJRyOYZ06/o0sJbVND3mNjQtw7HVclvenNZ6SvxwlM4u99QYwryG+HqnWXO
nO6csl/l9iIyPEjU8n9Z787l1rQbz0W1AzSZQwMdDburX37E6trJbd7FK3ZJbIfNJV8nzN3IYkvb
f6mC7qp70elduu/Cbx6ip/WMafa18JHW3+kAxQpMK7gzgEJ4sIkhm9u0mLJ2Qx+8mJg3b+EWoBdM
WPeXzPGz+KqNjKxwXqp6/M8ozDWFW/RQ6jBYP41XHAsGR9SjP5KLWm2hEr/1HdeBd8KR3xjhPe8M
diJw5gQ4u1LAItVZDzjEk3jplXdedfuBegxBafQGQWE6DtZlKNLpmZtcCCsPQEQQziwCC1eUvZbJ
0kmuuvAqHJi1gjDxFnFaRXthlR6skiUEAN5BeqQIHZD49P+MT0WXgiMx0liBXurmYNv8t/mMxUkV
fbZwJbvlyZ5TnTQwl0SOn/njqverq0+kWqEj08BpEOXMEjhDO/Y34Ksk46puUqU72+ZgzdU9nYTq
LtItGTS9Emk5+NPGRlUWzEvKXURwK2Q8KXg9MPUtIzcJqgGyS9h1iuK9HyaZ4BGt+Q8t02GiT8cs
F6cchJcRxjKoqQaP7XplzMTivlXFXpIAkhUPq4zW1S9k90rtpsXT4jauRWOpRwZoMIW8At7nj2go
sNDrWu4ZDELpnIleqG8FhbKmmJo03gwPgfSpdb9A+J4VSBIHuhEmaAdDxWkMVeoqBtVD13kYvzw8
1MDNyhWIKFd9JoVOJM1KPcCWLPBsQayOAIblmugE27so/uV7uGX9RMktSFq4UUe5HpTU0J2liN2k
uF/sOLd0R3BAxi1aMY8TULQabjDkhlYcYqosgIdaYPyPLXe9JpGMXLHlxwymDtylmITYodlnWCxA
MyId9NwjW5spHZ2YmEwF1KqBFSsHyy0R7Ps6wfUTU4NUZMrjUXCgw+p07/+LPs6oXTBaEkKu0C2c
dA3QDT0F1f4XffWVgpmSomvTO2ZelPtCjuTbVRKx4qV/b4KoXebxXwFGRxrbpih8NqASxa46CA3S
VbrcQZ4TRMNc9ca0ck0IvScnU230+KQR2S3FDXN89evTp4mEC3f31XKPTueNE6TBVwEXFJd64oBU
cSrAaEtqAC6D3gnDeqi0qydoFPMrQhfN3N63YQ5arc5G6hb2+y4+Nd31z11JBhejjut7zhwUIe6f
vfBFY5dzVoelo0fis40tdKV0RpEoLk4yvUgivHUSkIN2zG3PSGoMNs36bsDQ13H297TEqpUsMqb7
Tsz2qwEDw8g7Bp9+Q0PyzTorMYQ2lPtA0RVpQkSxlXtaZ0N/b0ftgVo8n0DT40yQIIvZeW4oC/QB
kJX4LNXb7ah7T89lFKaX3Xpdt1fR0E3okYywqsWO4yqeQ8PUMQ8JlI669/hATpyBEH2e9u/r0/AB
qVft8Ke7Ei+KCgjgy054P3oStwSaDm9UKW1mp8Gh0WGRoBmq4UgUIAGRb//t/QQy12nCAO8UxugJ
UoKs7RdpCHcw76A90UpCnSdVOCfRXXD5ovxXeMAf1ubUNSEFLpE/S0aGYLri2K8CINcKAnh3mPi5
JhMy/Ly1+uqNcFZo5sEqsmS2t6BTu/eGxhiyTrlGF1lsTi7aZUtmwS4cNai4COdqIobCKW+osodM
Ri0HMJSumIHIl6aE/n7HJzKzh9lUh5ytHQqo7Sncotzy4IEF2wVHgqVCZnSaBAxCxaayCNO1JZhT
JH812Tjil9yZvUZ0jibBYfruIC3T5Ulr9Ll6beXgP80OHZz0VLslwgUqXjoFEqrp+k9vHWZg/hs2
TtebhKfrY4XtcSus3KlmtDtW0tn6GUMrbrGYq6jZ2UFYk6qDDfn5e+0TFbGRk6hMb9jjCLJS8ZHC
CPscbsEqgjrE+s1CrwOJqm9MNX69zdodj3AD3Np+Jveca+obcY7O88UIE1bNuLOXNGPqdMP/xK7h
l1lqAcnCQjvnqg7Yn/nzwmVVe9VehFXLR4XBiws41NEECwx5RRLCUSNXKvHm3DGhZAk8tKiQyGGh
1TK/5Q1Om7pUROqGNSkKpuwQhvGwVzbZZW0fRRT4R9oUhoNKu6hpJMDBy8979TVfpDqnv+zVIBad
c/1Q+g1gD/dWRFOj8ZZCI/umltM+wbaX+P9b+oKTVHp9oJabwGoUbkxGSHw5vQ1x5/qkN6SLZ8UE
ML3N6YayXuaWtgUv9bgzdT+0Fzwymp8PQbbWd1GMQnE+rm1hflMOfqcM3PYAjK/ToppfU/lk+yXh
2PwxCohEFvbcLVavzv81a/mfwXrMuRhpl4LxZzXTuaTD1emJmaB8vwMjUa8srcJYNxPUTzMYZ3in
KK/6X4vN6jCxaw8mAwUIxtbyVRw5n0SaVmE/euuOsa+jZ76KY6QRixmxby3CydC940PFQbFz0ZuT
P95ua/igXvUtzpBaVwkAuofebI3+3dPIEtASiEMyWcY9hyS1Ov5OyjCuG2LpEyupz/uy4VB2RyCH
h6pmhFv0kZwfU9JYQF+0LXr/76NxGz0Buwuh8VM+Xkvp27A9mc/IuwXnIFRhn2GwWXmKA14ASxD9
jkT5x0C8jyy/j+99M/X3sY7iOxWa/+C7e2zvvP2jWcB3CDQBP0gMwrnz84IxLXG2sEMsHymY1cLp
accnvHiAxJtLpZlcZHm2HjP0cUBWUaVrG4lt2gV/P3vDxTmqkQf1ulTBQ5aM97EBinrcPj+Wxdg3
lQo6J2FRHCqQymWre7QUE7ziTijq+c1ZAcKEF9BixeKWnv1DkPNzr5JsB2+8vKbGzS5QEOeMiSMS
ZzBAP3JSrFvg+6t5BElzgktXVQN8l1y55PXMvBxDyOV/K/84DefTj6syiEKI+UjRU3xQPE3K3YHT
a5KSdCPWOUxDKBp5Ll0S021MxnfX/Yk5NbaRvWd0RdzIv4ubLEYnUKN9WD94ixQYYwcusb5+7dWG
OzWiLpSf4DqxdFEZxc2mzJpkiyNegEuaNMnhzzdsgA+CeJNz3vsqNxqOPnugXTDeYY8R/68m1wNs
0ganLu3HY1d20qclaJtZYRSaIcrgNcI57+w/796/hPOQJlkR9Tq68wu3rAMnk3Y6/Nrlr3xqCUaS
lbPSYRN5Kxv+M9Pxmib2oIY3KoakoPeWSrsPV1NVzNfIVaJfrcMSK9wrYYZqQ4gbRYSTQMQ6d1ow
ZAWUdCqDGgej7dtkzd5JQ9rwY+45IYcn8zjqJ7W+ENcD5LGCx9BCauRT2i6Z3XlIR9W2ZvWqvlDK
syomNIjlBpJHdLHOyWwdv4ds9UuacBBH2B7SA4Y21fQPe7xag/VPGx7zz8omWCESreyic2tzyeks
RE6Fy4hL93CJwYGhjGpPwBWE5sAYsAHNC+Skv4mV/eY+3/Fc+Z8FUr5L4hC7Ew/77DcGKQUYxJmb
7dFAiZvXLmF7k8ld/S/tIauxQaGPZW8q+M90htqMB4FNwjs/C+9chsqJLUUrtfDXVAaEAXtUnp0+
JtuV5wSH5XMKV+Ry0eMfR7Zry2o0c499p2g/BnzL0JP548oEWKpaLwBpHtaswMlrIzMk1Ye5LkuO
Hp/OBdSIKYe147VHudt9Mq9t2PRonQPmmaezVVl+BigyQ47O4t+nOQHF/hBOTqu5H5AJr3PdA5+I
vj1RZk1gIlFmiX0/sSVpnjT7A1KUtIh8+w5F0oid+cymyHQWvCAqNBGPboWcV9wEclyqU7KEiUTw
pzVFeeC9gd7DHqax7bTW4sxZxA/T8DxCfRvm3oqMwIhnruztBB1DYqJDtumjOgRnI70pJrEbXFbq
LgSI1N7og4w9az6/pB8mx/w3iJzAh8X8f/Cirxpu11unNxEK7UtzKvDioYw7b+gpJwYwxQMN7v4P
tS4jRd2JmzQWTPBefYrZMg5ub/i53aylMceGJ4+1At9TZsTIvcId/nNzBTTdxW5lZ3AJYn/iEknK
/jdqZgja55rDcdVxylVkVrtyxpzeR9b6Bx/Rhw9bC9N7SoGE+LDP2al/Ev6qEj+6S42rdahd2MQ4
Z4bVzpnU4YhYYaKtCnrz0H/8uchyBa8vXlf0eRSsjkLYaiVSOd+unjt/VBA/J511ieT5pY5/bQDw
hEnUy/DiH8ZfUq0OnHaNWUlAIZSaXQcsBahyeg3Ks4UhzZZDXi9skNEDFhnOoHiV9vF3zlJwerQ6
OoJ4F7OxMvQ6/qranMJHkBsS0vIXYRkizFFCwYxxT8i62QgriuK4dGaIvxBa4xJFB9UkJYtqdfJ1
uBWLmPZJhlxXbvHxtIUo5ocM71zjTQu0ovMVvjS2bmvxEz5y6ENN932EZUp9W0XIDhUOnaIZiUVG
Di+LjgGSryAMMRj2yqWDqs6PbwtD3Zvult8B4K45PhgqfaYGOixlCbMYj4O1Fbk/qJ5jodqBWnB2
7eTqvP4ztYzN+Eh1oQywav3KElja6RQwNgI00CdENBwx45b0eMqkgbicVQ+Mdsctoh8GX3IqSFYg
8RoRsK9ICG4Me/pNP1m5mGo0LU7DZY/yY1XZP9/6WNpLZS63W0jgroORFV24rVALDl7Kwix/418F
m6LPNBB7Sdnkf8Z6zY2wVVnP2u+u8+ZuGSMk5kZvSjDV1k8B0/RmS1CYgpDX7X8ibtdNW4lTdTDd
6Clt/GJqudGmPsgD/UwdMlYJEY6Fq/qcgM6x9SWT9Sujggtf2erq77khCuKUSVCboVynwpNe69Dc
B1AOCyKTGIElxIWJnudBrAkvF/0s0UtbaJYKEqL6V98UHc01dq1we88BQkMFX7UJcjfn6F37MaM2
84xTFmqnWfOPqWapYFCTSZ0f9Vx4FJQp4pc9DtMs7nKlPKqInPcMcICDcTHZHWlxVOsNXNjUmzNO
FzRoY/xt/tqZ09xb/2ud5IkbvJ1RWQnFQZQeVzOJ71DvqxjseSKVaIYYC33EsCYT7Aed2TW1GRMs
lq11PSnWjqKG5WVJk8BKb3jVZt0uC5sALS9IG8cDnGCVjNwnwVcAY8qBGcrdnKamQWxZiATPf83l
acKG2gkeNlLwsDffhp/V6uhhSfv9TL6QrwHXXUcYqnw0W8y+wYzd/bJqlS4OQhHACgGkhNkM4MUy
nlZlAT0qEHIJvcwodzaKoln9NQ/FtaR8LV3bJsfmIowEBOUzF+sKkQWXvbeMLa07Qhr2P6C76BIW
Lflmi1Y8erfG7gamy9SbRXywnczUJlid1fqThI7TFk9Uksgj8VSJW0//mXq0kp8bPEFId3c+7up9
1EXnq9ZNKZyuYGOQwoQsJ383cvQOWnI3/SvP5pr68NOwyOf0A1PgGpzwJaQWp2ztl5DWdX8/o5+Z
X58OTdh6Uu/EMRn7Bp2uNU1qTi2Z+RLPlTsOHXHUmnYkYnwzU2A7Xu1KsT2eCOx1zNd71yEfifha
csizINgoQe82x23B/uhql35eaOggE94VdzXjPO+RDMnH4UHVW/iAywzIaj3+Rqqm1YeJ7EojexbP
RF8hzg/mv6Sk4h18dF3W7jb+WIEfPDq5DjfH8om+axcCrCDwnHYqmDlxGkMOoGBc1+muTYWXQOGq
J0V9VvhqQN4OO0mB3qWHUPTVDve08bqsS9WCg4MUZRMZWwliQZPb0oY3nDbF7GRHGH6+U/cH6sXD
VR3/1woop1nPOdoO4YPtHcv5tfoKlunr03V0LIywBF+S84qwiIkeLeRFnXbJ8YEXt97XUzeJKKtp
o17rssgOjZKAEQGX5dZZdCmA5sxue/5hkhU+sm785MbTI8wv8bhoBJct4n8LIUQVk9O0OdYgEJax
+0tgLniOrX0yjcrL87h4ZbttTO2ZJ5bLjXVOrq6JiD6AStJpPiHshDZcF0QfZir8jkbsZuz5zHAJ
PjwDywgzncXAAE5XkHd+cA9tVjLTL9cl+zQIJIUmZw1lh4ASVTtQrLN8wOfa8g0q2kXbQInM+Ksv
Wm3vlveJwysfcjIM2h67OZLBVb0i0ZJMRE9VLTS8or9tvyAZ9jVdw/Mxvwt/VugzvXthPXGZ6Pw0
wlZP2W7liOpZx7JKfrQIu99CEgd3m5/fZR+z+D8NbDVs9XM20VuIq23eZ3zWVkcLuDOlyopgvDpq
OOKV/l6t6rmrulI6mOu2RVga98xqXrwwflPSIZbRzzccTqONTQROGhCXG5VyEsHh1zVSrJpx5xqr
Y7Zkj3LNrJtboniAqy6t3cUrmq9eTHLFU4oEwrMZ1fVvmQFmOwE77fx1PaSeklp7mKuz741UBAzo
RZZ6H3Q4k0Gd+t3YAi1twtFSh6gXE8aMSPctFZmLhxnA3Ma6axdpQU4mVTvCFHzJOwcBvcqlnauK
8VP9bPRKImLG+Bj8gIrtdEipp7wT1ZPB1V0KUiceThT5YJVnuyCQK+QBudSK+6LzfSg2P24m/soY
vN+Ut1o3OAwTppTALR5EKHUcnr01MNEF4mWrkdyD5fVyj+lkZGnajdo4RKE5yJ5LslObRw8nspr/
ZxEOrzfRFuu8EWDyJkRrDI8BB5mbcz+zlkeipqwyjR2GYQ4Ifik6WKnFJSmWUCcTIb/anmBThdED
671T7KpCYlOCkCp1Dv8w+GUl1MAW0THexm/sk4qnpij4s2HrzGXyzyYMd1od8gHsPdE4rIOmvOe+
WyrTAG5MVuoDN/2qQbydI0A/p89Hko5i3fAbECdXzDzhbq0v54VdiPx8B4dt8JLkAo/Dzjf4OLl8
p1KaXEGaax1r5sDdSj17z00xFIREmC4OymypVobBac8Ctyi/JYqO0G21BzrgIbuQTQAAdWhzWzyr
J0fMykZmftfkq0vk+Z4GQvX0EgER7mb//yJT6ZqFETJrgmThTCaM8uwyq2MiSczCl8c9k1Nxpuft
8eS4r/RzWi4G6EuGoy9fzRgSOnh7LLUy1mvyRJ9u3kPV7+llU3KS1p9hlB1rgizcbrG2StBHVfUb
wH3q5qETOtrl3qhCXn1ZLZ/aWD88dj7Ww/hPhFTFSkI34KcCdod5jJl5gAefuVoRwH39bNzyblM2
XPhf0pJL1/hwFLdvr7aIh2P6zhaus/t/YPR4niFXKHnCDHZM20Ovwt1mNLfGlSUYejsqkHJNemoE
BgvFgW7ugVVQcGFkUNotEi0BtWkhIk5QhB0sFmhSD6q1n5dwcVRJsQtMDC9qTeyw9rgGyKLg7em1
RuPkabYolyHqa0gtOVt6VQov3/+UNMEK0u8g5OPHCcXqAlY3F6kDK4Il6siWYl9F6FR9DiztkKdk
IfKQck7Gf4cAoPYF6ebVIkJIAlAipAkYDDNe4oTm+CXLPnnDGL7xZ60UAsB1YocV7TcO7JQQpDF4
yOv1zNm1LMv2mY3hj7OKcUolYa/iUU65g/iIPI3R+uEXlW+EJTt20A35MWZofM79KhVCJX7DDQz3
wf4q5A6SAm9qBYizbwQByrmITetyjdrmB3XejS/JwkvT65vGGpuW38ISdaPoBrkNgtB+TJtRQclC
g7fQcZIg4mqt4zgwGOvD5eKUdMLtNDCg6xXNjwedAJqOC6HI7P7utADA18m0QwrCJ0WuK/aRG27A
rEi3R1mm8IJYOJf3m7/kZm/Q0y5NkiqaniXXuduU/extaZeGVk8SY+k38kOvPhDV6spXymGoQawM
ComTC4ymB6AQClz/lqhMKDOox/pIYVDEc+dvqwcaaW/ENzFWgEVnfdmBdI1OPzjm/w95hJzZ3cwU
EP8GvWIPdiX3VXE92nxZzHuYHZDDukSp9JiMnHKui3/XnxMnVLb0mEMtmuYiDgFg4yrUwBxIz1Mj
ZMXOMfpHUgfRsjBZ1sNKEuP4kDB7ebM6ve2bDhLH2Vn1kwFY0t4RbFID9DI91CQE5RYbK0JckR3r
xmuhCTrQYed7MtY6E48MS5kbMBhIWbOlNnuOw9Yt1WnQHiXy4w0Tje3JHmLQf6AmFVfCCWnF09AV
alLFh6kUZAcJs4qtPMJzFNXrMn1dYsd5VhioF0Z3bIjhy5jcR2q3xBlzS/G6hhPL2eQcBqRaOMIX
of2YnB0kS+8bfmBO/66OUPHEKVoqTXwnTbdVo4V60dq/ZIKMYp3cnkPNGMzP5i62G2/Ke6lzoQDZ
ReLDmCojrwf9Ihe+6GclXeVEiS872YerggVbCdITexGG7xZQQS5tJIPXud4UZrD1935LXzh/eE3y
oE00TOLh2OhveiBIKUtPom1bXd5hr6Ud6MKXiBHJbBodw0DFkQM5A1sZY861nipETzE+3ug6MOak
2eejmlPiPaRW0TALLX7sLfnLXW4UXtSctffMMoA9Oj3/aOQfG4IUNlyNhraQ33Gx+agR4nag9GvB
Am7XMa/JLynehIAVMiPc7ngIpVy9A/n4P9I0tfSS0Tr0Wq+aSol6l4K3IirdDcJEaBCwbMKlFXy5
iohItFH1rGS+i3TxllD7cBjWOETa2qsz/8A1fBfMLZDqZOh5cODASB6KPkvyU8yMG7eNrqvXRugd
O0s8w3qcU53JORn4CmXjqA/28cvBYQs1/TEcwM5BCEPbtLRKZ/yQ/528GGLyCy/oiuEAjIFc29DE
+H/1lKzJ4B5Bgaqeqo2X0GaldtaJUedaoNga7OR3KSIcbOjnoSUoPPrUXQNyPlW1exTpIDxFqvKr
UTaZRtjYNdZU8Z5vkqWu5dEyob9tkA5mnYo3gvMkpJK0SweEsYR8jcPDz1ftpYr7kFF3kp1IHUAe
LygJDrvKGe7g130v1k65WcL/CWqqPa+wZBflHJJGDjLLl1qwmo/fJDEVJYphqwtwZZ5ig/OWY2rh
BWkelcJh3rfv0ifdFGgKAWFAOQWmq/zD9IEiw0yQhLtJ76r2rr49EPJhKkVGtWicKwzgT8u7nqqF
p42tU8wV7P2ilDDEJT1QEAMdnSKVxpTqquC/sUkR8G+T/Sug6G/X3nK15r3iAgJ6Hpsss3/wnboa
aQw/3I8SQbm7Krsc9XO/1Qq29cjgR7QtXy4ALNqetO85w1mtC42qBT6vE7KnxNDwAbojgrYHdDTq
AZSd318aNM8Sh8gRdlraAtyAnT2lsnB7do1vXbDBEnNJt4hiTnnuFnvtqG0IwdDESuBJQyiJtFUd
EtcRnu7Uc24BssshDyAS0/I5jVkHYex9mBe0V5MxR21FDXy9EwhJRbDvthXRVn0SvR8g40hferXY
SgbaJdwgd2HlXhC8xqMtcCYEA+DxKKPBnWxJto9wtvc4h6HmBqdUk/mqymt9D7ZKEHshh89dcPGa
LoZu/wM5VWipX77F0/YNYSiVw032FP5itNzHpW+Z3PdMzDgkQDbDuE118T5JTm8xu8oDBAivByoB
/89etL1J8SeNS7A8g3W3WxwrSi+nr+9UGhZBMQAsy5K1lsDd54wY40JBPrbBBZ5xLb6+qeWy9p6J
arq0XWcqwUx76zodShF4KeoOqBGeA8rjiUfYnD8mKsyZcJ7eElGTMPM0m2OuGqu+mvVv8JWpLf5H
py/HL5a8ShYT8lBYRgOM6/DeM9INBr4N+1wYRETVrxXOgoWIuFcrQ6v8lS7INk5RCB+Uy+r9s48a
ntH34yrCmxfi5UGR/vlUAi6L3R66EbrA9BpyGFNKtVdD/DBbvcbX9QXFBmUZCmaSaeDYKrwStkLY
tVUnbrADbLQ5KBd6pcRsjLS3NNBpyzxI35aKocRykH/dlkZG40p/JdfwLczX8P+JN46cxNT1XGig
1MLc/hYeUxcnDEloawVO3ionaUyvmYrbTLI03UZyq/FY99mVoiqvNfaisy/sycWu5B5pfKJaamVU
2uAO8Hq3fhGcHHnTttl/uBdoEezryjcE6VcLY7O+q3taIHYw1glwbk5vjA9ACYDYtW43zqLcFkLr
usEgMHkX2xwflZb22ZY/s6trAgLjHvWFy624M4jbtJ6zbLCPpiDEvdC5GzJWwkaWbArHNmkVmVDM
U4DSmA41zK2uC34z9Tbu5E9rT1UuDdnkFop7Gd43i9rmmA1HWbyTVYY7ypP34ASTyUy24mzYbnH/
bt814sBYDM0HeRiT2/k2tT8EAVtPLLM3fOkqPpzdciFgO3P0hf08tIFqqLEa3JE15nrq44WgCHmj
FSKYqCTG1RYoHQH2r+YP62Lp2e+8D24dnm8782RMrsKpIVo03QhfCzsL9H4lLkZGzpXhBpimT17i
E+TMHTKgGoYzBN8irOH9WLgul4YMf1N9Mf9tTXomXCeLjpw6Wt7oRb6vGQAzrQq+GFjnxODzASxd
hOxW//cbYhKbpOOwOqnK0zceYgmLIcP25WKUhGYXUMlQqif3phjBSCa9jlfJQTUN5wb/oB3pZ7MA
ECFZKtdO6NONYjDRRvcw3YD1Txu1JErpRkpx/s24C7dJNmQy29h1QeDlRQlZumYRXnaYUZWWSxgu
q3BTcSjLGdnuwR11UcPDoml+E1r5VbywTMbt1MMwLT3uRRcR2FnGbZqqT+co+VQOeLyldPw4RET1
U4C9XvvACxLMwKew+RZ0L1jVeKlM5YtZMA6gvXnQM11tlc6aP8QlHw4NjJoXDH1i1QDrGocjsZSz
dm0Bzwzy32X/T53F3t9+/Q2F3C14O1OO+g6McwADQeHfcduqHYSZyvTj+yJO8Xn3lPLi5kr5e2yQ
am6J0UBudYgrL58RJtbiOwPDHiIOPi4ZFc3gWbEn67CoX10Fe85WmTqDeetWmn+9z/Eoq6mVrFv6
oPgS76GBDAriRO0qJi1Q+lpn1zvRA0DZwdEMRoUcCUTtxwydGlMEI8o6SsUeyF0M9zhmT6XmsLny
sT8qZKtl1ft8I7adpuHav67WvlenbM8RyJlw7Sbhze8Lv2T9MRybAbkoyzPr6EhmML/+u4S4HpGl
5f2IbW6ck0pKJd8npZPcR83mYz/KwmG6g8KYg+HXIejan70yBQESTl2BqlnRkjkbM3qUdFAkY2r3
Jh8Qg5VJ8fDEZEk6i/T9s3I7NLB+fMkh7w949TQPwE+Ym1ErxXYeMJPI1md7w2+e2f+y9y+LEDW9
4Z535h6tvaZlFI0PvvR78nVo2NlE74iue2BenB52/bn3+NHG83S93UCweYoWYaf+WFlK6AKN7fwv
0F7E2jY1fmfJXMuXxk0Ohouu/5Fvh6qQhqbJ4d0gYAP/SqXzuCuTfDZdVmnQ4wHuSleAgCCYWJA/
7NbTJpG3NFdVUbnQJdjH94eUW6okzytszd5rtGyNK6iTY298wxEjyayun7Kgr0khu1n7euVQqz/e
QRt6OrflqcDJNIcf/seyxXBtNTWH/HQfNYMqKgBeM7C5vyRj+6VIBDn7a1lqGtkR3q/ijO/aTst+
kDKKVkt+6MbQ3v3rl191UWAUEdf3KEGBjJh9j84cPubCjtpKVTCH+NyoXQbGezOXir4m0DDPKRZq
C/OdCCFnAI7se5cJieDksQAp+adOOgdQQG34HZomI9jaDczIV91yZKQ5SB4IDWkXbiW3xbCvzNtN
pilXj0d44//qxIunJgvyx3R+uBhk4xX7L/hQ5IvG6ucHFiKaq93ElNgzTJKZnLCo3GWlV22dYmQP
v0XS1Pd4p/r4VP4HCqbruHvW9vcvt+KgptsFA1al0R6nsLGnfJoqB2l4nIDc3xOPdJHDKiCGuiZ3
eFGuWWS2HZ+offDeGpK62WJIJxbhyonB9D8xxJcQyJxf6fZKyx2nEuzOEGaYfJPOJf0AGH+gmQH/
Q6YvKVX057CRYwgWNEhdfqOvmGXl01ot3o7iCwRVluS385XdxUAjLy8ogfWWTTj1Z7l6muVB6Kqf
GgZu7smLNXRFyTxzllVWn6J2hQheUAEfxU7nCoqH4SwHp1r1ennWcqIGp7Zr9A/QJ8Tukq/TgZWp
aUBn7gauC1dxMmENbu87IdqCGxvNxYlnvCHhTdBRUKp/OXGHRFZu4lUMcIipiDw+I00PAKiTWvbY
r2a3KgTbVX3Cos+rVItslof1cfWBvOZdkUTdj1fEIAwlW4LgD+gX1HzHre75GsKsnrVN2AqLPFGK
7Q5EHjIZNMZjvUfLpyXgTYKbE18nEYBrReYSJiV/xJ0zNmopI3qzpcmtiuAnUA49hOxAxC+WGwUX
RrCkbJwsCSrJC3NbQBEQcgQLWMpK46tUonhVQ0kFLVVubYmzRVV+PVXFB6RGk2RGPVDJJxdfaI2t
k0CdkaprGVeMZaBlxJCY2YPx6hMIG7yv+gCEYNd0xSKp8MRpNPWUwdQKDHuee1IGo2FgN8AZqXN7
hpB7YCg9xAfAi27thJ1GvNTeKY7JEdd4hRqo3A3fwqWFWz+TM0xP7yFml+m4vn0kjOb/mneA/+RE
avul90ORpuPRV+mBvq5if5W3/z9/gZW3MLOB0WNHz/NXfm0ex95jDIyVH6rr2yJaSQz7/YVlkrUt
5HlRRDCdzWsMymWHUHoWvTjxliUBS8Zj1XbvSGQuklBUgCIFVqsUgOu4b+WriNMLSvoRe1MuXUBD
S31vO1abZHdcJ0vXj010Okgx81oQ00g8DPadvZpTkQ2av02v3RseL1EGOW7pohbleoeMbdB0Ae4w
9GYqi6U6LdHiZ5yt0tzfafEoCMg5kiIwRmcg7RZCktSo/mEmAE7YbbRoQnXVTbyZj3xvIuSeiNMM
Qzi6IHTAQm1pqRJbc60uw6IUQrALOuXvDgpzPHegxm4heMwqHsPUfCSfMhwGc2B5UglYZ2mZKkOz
gXYcHYE954iLMp9Uq5W+a1Tu/xhbIaQGUQBEbVfBCtq2qXv07siFNmDuQUD8W7vkgNLxYZvd3m/N
CsZkyhjKI5w34LIYRDTMeuw1EwhzGzfZNCpcdGJlo4OhKRvtUSMypWQRtsa+Gh3r5Y/B9l8ds0r9
mDB7ZGn8KSR1Fdr9eS230Fc1404x+rGc4OmoRoKORehwusuhSyq2YqMGx18KXp+6rKWeK4jw0Tav
GsGMPV6K/uap5RmHxIjpDtbmYiij+xGn9/5qI51eZ1sYkI+TlJaHllSKSoXYv+4Xld4x5EXlOjbM
ggiYNOu4KzftXs2WUS5ZFTk++go+7J6+D2rsDQ4jFrF6UC4V9UaFlfxiJae5Lcmh7aJP+xiNBzMN
fvctz8IqFoE7tWqerCr7ZxRmWWwU2ifZWBmeOLZmIeG4SIBk5zeJ8x43qZuXM/rnn2l2unbrpCfY
tnLY0YNzqH08jewgylaeE7vR3vitwzXAD88oCHW39W8+XUF1+rOQKkNg47eaTJTboe9FXg1PeU05
RhUuBdVGQ+5j/INX+Y28LFCsTMcyVD12JWXmUTIAcA+jWpMLISyY9UJAo3jJ93jiOIUuMNwJ23ru
ty/LhRHgqPcs7szP4+GifNUi3saZgcnQGyuqTrGPeezgnC2Zy6iQKbGpupm9oKPSGnmi4BFohCvv
oXwDWsmwxif5SGCcsx9PE0P4sjCSd7doJneRc5fFdmBp//YLtxlB2u/8XdsmGd2JaGQcRptaDH0O
TyCCIVbQudAs1920KYlH1coSntJKbWcZEmW4l4ocQ626ok5gMwgQLVL1GJDxaGqhMdPCpLaIfYPW
kvkP4vZ2J/MXYHgX62X1s62baXmF9ibhWa2hoZQFrPeDpngEeD9XqcztyCbcayZlbnKwhv4euBhl
2ilXg03rMxtUMXAMuVFnwP7QiTXoiJGUBvFoIIffHuSqTi558wBUht+N+87OYjnXdy42LE2jlrie
EaD3W0zvi/r1bz1iYItGYPsUOwyxZvo9taolI4djn1XwwJjqPtDhLR/4xQAlZoVwYljataB8V5zu
lqU0vjsCmv3MzYBcGfqsVej8XZHWGqs3bjGIZu0OUGcyXCJnqvT7sA7pYO8w/tDM/M6T63PSEKLf
Ogw77e4l1KkIkEEZ3wuax0wfobEbRHd6pi/gTSPB2Do2LUEmPrZsAjw+ZIknCXatuTXqV4Xoc/Om
1J2dWvnfyMHn+ykqoT9jn9FHVIOQ1Yx2V8E1Wd0M1Z0qh8gPJfQdmqGItSku/5DmH/7j7rvMyaKO
KP+KN8/+bUB+zQ241GVKYC94gJUicYOONin8d45AL0FZvZWVY8cqCvk51Moy2jCBVZi8jhadm85D
/wWbChKpwcpZn9dRzktF7c5ruyMW4DBWzfWEMYrPMYgacEjXAXR0AvOGwFPI+9QLfUyjK+gjH1kT
Ps3gCwa/W9z3scnPft3QxTxhvdBTElT05Kugeq8zHONAs+girpD00v/rI8X/eo6MU1EA7HQd2yaR
sYealphHER4hC1OR3ar6jvtEM4A1r3qQWuRbYpccozGT/G+5dWa5GaA7xHm/GgYidZVLR2QKZHVl
5b/h8Kp1CFeLdifeJKIRjhgqfuB5gwi7dtUZZzH7GX4kC9b2G790NC2lly461R3f3k6JpAYx7ISk
892QuZsvn6+S7gHmXe5jE0Zj3YIXwhxAZUn15HmY5Eovg/Nt+rMC4z78zXiUYVvQ5v0QjDGdqaGh
6A1N9QnjROBRFDcK2wUa4DQv35EFgYqnP8yEFefWfPq9wdq4fh5P2Bfz/Xx0ykNxBUQABv2UJILp
IrvhlBYGpWA8V31sWsDSalvKZ39vGcdy51E14Uyl+NYNUU1BVEyO6oz4PHrsisHqHFbbOTi1KJuO
5BBZc1Hc/uHDMbyPN/u8qeBmQ1LhZw0DGDl55A+Fw5b73kIkTMje+0zE2/aj7nzBFPby3vKA1TN2
OU0g8G+yftVvwK3yMlqV9PqwCdbtoNhBEZDdv/ZwnoiIGnlG0uSmO4MnVLcRZB+0iqK9pG7nuJ8V
9/XBjVu47Jk2RP+QwGqfrGlOa8MgOsUeWMq20vfe+aRMEtggyd7LZc6QOE2Q2+GufjNu1kEz0EIl
0HZYkRrP7M+9oBHp+OcXX/+jTS7Ys3kazC+to2d8yNQWMaq167ICaOqvQWuchOW3BUzEWZACezqO
h/uNYFp9Mp+z5lDP0Chu8DlDeZtFsvVXEEbVu7Wzfe7OMkT8zHg2WHiQJZGRlejhO+R1uOecn7GK
qbhYegqgNAhWEcfsrruv04CvS/Huwp3UV3Hu1LPqkhfMnlGcrryTFv0UL7tqMrKOQeBrtUHjeqTz
tY+YPmq6mjpH7vuJwH+Bf0SMe4O+0zcjc50QzTqfDCtGE4ulhIFenTX3r3ja7h86sdwLYw6QmoCl
ZO4Nk5e0s5RKTzsv2hsEKsPexAGenQgzQn9ik2N7pNO/xcCxkwDdgW7h8S/r4N0uZZzJl4Y5dGLG
RffhKP8ixZXRg3NlkfLlHkFupMmhH1eAhaNl8MzfYXwRyTyAOm4X87WBLAvYgdxgqaIv0AIna1q1
obgi1QId+iLXpU2kvV0bDDnZGaNZA+KLqoFQq4nOMWnmRlBoYgl43KVE8SUW0XZPNpTYcPD+SlvK
OpAHHzdOEbDkNIpexWvAy1jD6YOqRbTVsj3tevdv0sLjq8esznHANov71GdWnkojrkp1OJ8tYkld
Tu5pwldPTBhyOBJJobiCVUMdMNapB6c84iv5YbLdDVCQmyRMIg3NJ9n3ZfsBoSoDA//0R0UL1gIe
4rYqr7mtVUPDZJZMYYq1msCasaSSfRdxGR5goYN9ZCDiHAeXIFjrq2vAo20dl3uBtezG8tR6d58+
dGt5rlQtdj/k+VeB9fWjunRaw536duf39JoKOP4imKSnx24OvhW+64tJzgbnl14SHYcPWu5w2wZ/
QXIwSLgG6o5Z30IDTVFvymLBN37s32atSAAi7wobB3SwkutTeJrs75coP1U9aT6dzRW8zaFfFmo1
23V3TMbgsv1Ezjdxl6QYNXnhsGrGGE2zKmQ9wnIJH7a6KReenCgFQJmFHXchmsrvlA3HdDNx0kDw
Si8gm+k0XhSuAtZQdB560bF8YI7nYukDbx3Aj0RdWn4t6Uk9M55ZI4uCeRUvwDqgA+BKGbxab2mE
IxWp8RTE85+NTLO9G7zNBrfMhTaOW9NPlHSbmusqjy+l4YoVeRxEarhU8Llhn2BhyOa+uoz+g9vH
vYSCc0AZI1S/ubef/FFV6vSkL1ngcmLiPOLmV7V/WBqAbGyuKW6YRAfdteDAqd/1qAREGxsioW53
qbevU57FQ2K+T9bpTZf7Tck+7KNFQw/MUqOdgEsvqh3yyl/EtbtP/3lKP9xvJR0yB1RmqFRU9H0W
e68LDs5v0y7w543a843rY/bUrp4MtJH5yP2FSG/A0Nrn/o/loGaK7c/hn3k9xhHCwezvju9O2f0m
vvZ2gNslS4E04333NdFgmnxYAauXXahlQGTdpcgfK8BShP3TvCB3JUBPkvXr1rrCF3FyA6f6w5BK
my/07u7PvF0TQzpkAnP/s6e9s8MdUsp0yHqAa4y38UGNe9Czdr1G09pUxfaeE0dUjoVg4EpT0y20
H9QlfC1r+e7BeImYfHVn6HTKgzVHZnHnqvRKi7ZvR+WdEBOz/cdZliSsfDMUqzUUETbvPNEI2OjQ
QTbqWb6t9xWdspOlqbiQbFFqLbY1oSGeJAxb+/5GDTLJkgUsKF5YaEoxOWkewHIOodgAcgz3jj+e
GZVj4IgzZFcBp4sTuujTkvkgRJe7fqqqjbFVsITlsfm+2keSY0RqhF+5XC19wIKk9d0TCtsWXd4+
1GwTYGQUYx4rSMmOqSFFH4oIkKE0S7Q97NvEBcBjCOruP7H3Lcd75hcuvYRZsn8EKdpBc2NKnwws
hLZuCB/kbRvdnyDLurUGvofC5+S8XPco+9TGdcc72Y3MS8sMLVL65bq3xt0uCVlW204dqFtMfN3W
ZQbUa6I1MuQMl9xaXYLh+PZXJ6mGyU3x8ISQG88xth+lniI/oOkuyrF3oL8TcOxsgIcZGG3C8Nmc
q92ItQBBKVW3gdlGws4TDh/5j8nwZ1xC/qFAOBE8ct8L7LMLdd2mxv/Suu3cBgmmVP306mxUOTAO
WB4jWumWHtB/O7OILegfirZjj8JaiWz/AS4Cu/CVzNPLozDdEnLrj8sPV2kwgcWPuT+mNuHcorer
uTU/p1yOstG/paZ+5PapVsVehy3rqFoItjie+Cp6P+7p1xxsLVkr8auW4hGU2Z+DP7/bFEGU07e7
3DDjRLX+PBCTTes+mHzYkW7OiF62UumzEvkJlB8I93qTx2QZpkVwxK6pl4aguhq1iuXQoZ1Xqup2
pUHmpWPYVY1K3J1VxBvH8sGW6h/CD9pSn7tmLHaCpmndIQHdFXlqJrQNngxF/pgADadDUBmYexTH
y5+Zji9V6ZUONvc5nUr2CNETmW2tvqdKNBtYuf2caR8Ri97CJphGai/JTuUpin1Bz+w85XhAfxwL
tWhkeuZ3HYnH9wssqylUimHNldy3CpkQp7gwi5DkoBxhm8VUC/laJhtRgok6U96a69+MnDKXVr5Z
83b7QdI/x/4vvr5EvYgvoIR2LuYAvKLWYiSqhoQ9j2eEnF3R3E8X8BfIVGQnXCqYTvZ+eNEL/nTD
7RLWl0xugp+aFVTKrBIdrPTM1Fy/Jh7oYuhVnn3KvVfbuPm3pXIXGLz082kTtwgDAa4au+3IOsYB
Wrf/Jb9CPvhU7NSH2qCr+qhZ59ULAtgGAumh7Iw8ub+WiRo4+RGHc20topSn2bAdRWHCTLEQ+amF
Onkbp+Tux+GDC2d3YuXjInJqZlPhtgBBNCH7cNPjz+c/PM4PUbojR66JxjqDak5dSH/gHLE2r6dg
SmvUXLWdfgfibf1XbBB4CIwljSDvFm5bW75NquhzhhgbACfnDvOQhMaRCGaesJSDctEJGt9ntsv9
RqtOe91l1kDgf5ir1IROAbhvlwTPLx59+gVpZ85PeB2MJuw3tRCGWzAuxw26CsMLaaudeXJE3zsP
3RUeXIjjhAOqkd8s+rTR1+pik9wgWNRqLRr2o0vjywlFRfhuD0dE8aN0OeokHk1b2hWbVVwjtl7/
0o1XYoIjwJOhyvcJgDg/3upRRHaWkHS48Rpv3EXREV1qKaVO4VXn2LurbycD4QCjBFsAwltAMnRA
DqDgWTEEidwmGsWNdJt6OQXATu6gWtcQ/NzQzKG3fXCOa/Buw3e1qMKqzXAGsZVGU8wd0NgruyOg
tmjYgHptJSpJ1IohfAlQoNnIureUqd74L52rUrXlA+8zHlnUnfMiBKe6VE+lxn7SlvEHFSmS3rfz
w3Av1/KTHwX7/5XDNhjX31QEnygvwa+ZySfeIIAELGulvv2+Br8XCinM8ZmSUfdhGggkZSgP8CUB
vVALM30O0oqeiYr/gJ5ZS+ex4AN5NI8AA70dH92kPCIHLpjL2/scU9Bl2x4+LtJMvnswdamZqGbx
NJeoUOOse6Go5t2Bi0pvoKpga6epY+tTutgkmhwRWR8k03evya9Sk6KxW+iS/7FhxDRurakbldkd
81efi/Jb/h/10qPsAY48ShBX9Tcpmgs+7O+MJAumEUIKTr3iqac2GlZO5lfvrzL4nh9RM191sEsj
YlM2UKR7lAiADW0Oe6DdByhwUVE4cMCCQDQwrRM4kv35WO9aDCsxNgU+ka76FoaAFDUGrajf6rBG
+zsXuOFkyKHJ72qoA4nqM4wIfW8bE3Cnqth0yoaOJhV7ilZO6DEJuSDVRJsFfgCafgw04/VIIYhE
sT1Bj4bfWJvUvb3zoUQRrPM79KC+ZxrurZ68K57sDk6nG6oyFuoxapdNT92tEdIPb7DZm7m2Fm40
99CAD6t4uF3XHMH01IX/xiIQJdr1Q68+dbIqPpeOjwryoH3ClQZInm28qVZvZA1K0LjaK9b8pFnZ
tpIzp2PmNIpnkB6V8AZnXBqgqflX+Yf6lUJCr5tvN6YGZSBpcH+Az8PE/yz9nv2tX+U8Avl9yl3s
zQo2JhmKPsDoQt1nPKhZiEuyVyujs8opHokpGVVSNmGm6FgaMAmQHx89DWe0tWqlZG5oq3s7YFxG
NWmgKOCqK77HUMt58ewW07FPNJv4BD+BAw5dJl8HjE6edmrbjE/YlahVoUCn97OuEHxQTrDEW8AY
z+Tj1e8MDbc8NMyL9uyiRy58yj/XYkW/qUcl8ch9B0N6bjmicD0k+1z+Qb9kJm7VE/HtbIqksVfd
BxIfHmQ4KXX8+XhRhEShrgPR1bkwL5b2ZocKPle0Y/tGBxpLNQSuKMre4JBkUFWxIaFdkLmhxdLM
iPRPeMC/hVf4+VlG4cADW9Jf5vO8KYr2/hdPOdlAWtGc4UejPEZQCByk06EjB0fDWkE/B6Dvx9Wb
JbbfAG4kxM+Og6We7TawWI8kyxHX2/Xr1hr47vQNOAV1THftkdyT2fGxzuwBnqvHNUK/QRYrZEO/
DQ24f3G8EOnwJmWvntwoCRUIZKS9mNDhy6tVAZdHw80RtdbduafLEe3FWdlcN6olpuG0fKb4+8dk
JgL50OKO1dYhl0jOgLS+62rBRxArHmheXLIVrshIPk0cCYqlNIp4J+0YTf4GP8grMGc3bMjeh2Yv
Jug7t2PXcLKUjTZJONzmWSqOiCy9njJ7Pr7xhvV2Y6hA72Nm/G695LTxHiJ7gd7moVDGT73PHS1b
vQxPpXys37UHtWnylhitb88mtKBB0hD73WrVJeU/5hIdY8XBUcqrdt3hGjhjLqCpgknzMwNo5XT2
+ajJV8OOpG9upwA/f2w2gTqYo7lWAuO6klccvD9PBQ/yLF3lMczi/2TDT069OUmtYuVKMB5iiePC
mvZFSDnIdSZv55qvEZwFL2qf6sFbBiaonwGvxNI821JsklHKWwSkQzXxbTutJYf1F49S1Dk3kVre
1bUGtFLVYFRJ1egnw49KfS2C29ca9bE8wrhxLOrpAkYai7I9eZHwbGSKLFaP8BdRgVqiA6T/MlbW
XHdPyUCgk8Kcu0MFvT8TP7CleHe/whXHsMoYgT+rmlzBTdm/DBTrjUkXFkjW0OVB+0QayVgcOxF+
fQo8DLkXEAlb770N6sDt+IMhwZmDKiF3rxClqf0uKVUzoExrPez4W/Wr5ouWCLUMdToI7ttoh5Fe
Ghgo0K9tFl3AdarjMgBDjlvkpxtHZXELTAA5hKerHHG0omovVz9bjrhyQsm84S39l2v2Fzjn5HF0
QgMryz3No+YnlcvBo2ybUqytJx0RlG6Kbm7EezTQn0n2IrqotNywRB5sgrCkbB/DR7OpY2WZ88fC
dbCQx34Zqz5vSq/cI+JPJE7vjEFcSOam5gqm/bw0THu5oODH6pMKNbcsEuzy4Nv800Ns78GK66Xe
7XQwxSqoFr50oO5fKON+pXQ24/WB5qESBVY51WSMNKbFHxoFYcpJ6xOzxajQ2WMcSWF1NBJkQMqu
k58YLmay13VYiyS06xTYijcSohI5pyM8e7+VX5kJDmnJXtpoxshsIYGgKHgJBPdgfLvaSTz8oxmx
eSUfygd8gSF5QA1XPomYInir0es9GQ5JPfgmgQ2XbldAmKBUB2ArJx/q7IRD7a7NDhr6+6iFfv/i
6FA92w0K4VUs1aK50FR4aXe60/peo07RqqVAcRbwJeoCDQrN1ieFpMP53+3DL+6VKZInU5zZ2UDS
5MbJ1XmS38XrR43NaxG23h2EHv/JR948C49jrbgGXI3YIZYtFduLu0pxYaeTNDjtW4Esv2+s8pUk
Pc82lpo4IvuvqtHKgl4vh7oqP4oGbcSu8hWbivtnkii+JwTC3ZMR6MMBe4crGukq8ND5MhSKtknl
mlpA/6CmSFbofyJ+K+WRng6XfyiH/qUUi/FWSCKnU56zHGbbghyiFiW8d8IbrYAGy5dcgkcii7Es
Lebrdy37RYlK9lIiIM8zVYrIJx4cHU/+0KESFYTRwD9FU0eeD9hJdduuvAPPeLtkeVOgbjlr7rkH
JaNcqhUZwwYjkD3I7dKlQFM0EMEOerj1xIZiDqTSqlKJRvlYQVGLKK0oTNz82OkxzvaIuoHgUWpv
irzHsSTLvFDJnjpZMwtm4JO3Q1vNeU1xCNd2VkLyWAFAT0mskSRcBuXjvv5p0Grye2e4e3rtqSAG
sAFLM6D3WAZ+JGufbCaUZjjYc7Qnlgt/y5bR5/JXLuDg1OmqG9gqIledZd2rU54JuBGrcKf3ZPw9
x2e3VuCLmOZ5slnb5khVMaa9gSsBTYNJVkUJW32Zz6J/xDsgCrUEEPtq2g0kUOkRP13Jz7EFoYYQ
3Eu21rmaumb3z/YzrkEzfjSfDOVdXDcPilJ166THbyXQboRQpJUinxRzWcK4F4SJLgQmPGSe+KOl
mvtb8Yp/Uxnqd1Kqhp9dy5t7TZyLz/uLOhp4Kzz+ilahe7SNm0HXj22CF1EiRLirWVucSxfmw30W
+8q1xXuD62Je8aMddq3R4mkAZDX2u/Vq9l+u8HsChitiPtp61xsy3ObxOadWErprnNLqV9V0sz0E
472adJxr/L1OzfHTfun1WWcQNLszBbMGGSXnIBwLEnNt8M9JnMe1eK0ZRt2ktyu/5tiL4oTrfu2Z
jI9eq8qOg/GcBgfnfEGcSFgqRJExkd6HK79cTaYAlD06CnsFQKy1bQdC//kCPl15aopZ98aZjwQp
rdE5LjcAhs1cWAxl+bwgXtBo4TKZKn5EKT3adIIIWyfNoULbSDYIT6eE7IWzBgwXEr9WbywFgv95
xtcIYyjbkPrFwwMsVabf8who3xLvySyOi07f1G/vkd45DcWmOAhq5x4fBqNVeGhS+LpGBn0xTwQ2
tgb6UfoAwWswyT5DLCJi9bSybyp1oVIuCrQCXhes79hdZ8u2hHoIRDtUyGq6SkvRKOWJYxTExym0
XHrAzIqW+R6XjnN38DLxIczqzS6r0VWWujjacjwQtyF5ImSjqDn1zEtJw9Y2F5K2DtXxoLSfx1yZ
omCvQcuzVcmzMzOh8rUskU4PcZcORkqdUYD8S1xgQRId7GqgRDurvbe7WlsqOROukKaErUyB+vpY
ACaLLl12KT+goI6jBk28udfTcymIl5fQ66xHv9T+PFo7OZffyCtI57IWxJyFFBsLiEYgJPk7cUXt
1lEWqVOMIGCmjwslnH5GsvRf3J2+VZ/kRET0c2pI22P6FbZGN6+t1OipUfcBarChaorQN+zuVaK7
7LJ0JNWOrclPjBSIGQvcrUJ9Nx4UbF72DsZnoGWU4JYWW55nLCAWaz7c5DycC+sp2tIj4CFKELUt
g+DLyKC/Z8+SHaa1wdqN8rpNoY/rngZrEk8HFgAQPM5oVOvOFdkvhD5Uqc6bHM/Smh9dQThPiWij
6pSwpm1Q+lNlwGFrCIzyGUNZdJaWvBkqfl7AU/cU4617sHEzgw+IrQXwOZGvAXSltGtkqsBeyDn6
xALIgAKbdJpxFgX5A8fhoIzR9Y5JpdYAuMcD8iOAE3ejtFvnrgEZQEgHc3b+bXWjfFRvng7NA9eb
9JrfVVuoR+NLPeFTyfiWSUnDVoCQse4pZ+H/ktHfNW1m6d0zHztgs+BhA+TwqP8ulgT+Q21MzomB
k9LkKUB+HNuBQdakkFcZ2qB4L9mvJY8bWEdGum2Ry8QWK35O12XnJoGdc5sio1Y1e2pkWpO8HqGf
g3wjiDh1438N4FfCmQaeAkEF4DAcE6xR48PjHPV/8PFdvESjyrGHFyXwnvvPsfuQE2ztA3NUh/TT
WeqeCyQK1g+K3B9Y1XnZT77j/r+Wcvwaia6E5Y4tl/QfEw/yaJmS2k3e2hgE+k9sqFkQPRpn8XVN
Rkg0WpO1A8vxaew6PVtlilgmDBXLdWrU+OxXdlqClf0F8QfSYBQ40BceF9OuVeN0kXm09ZkSztsB
q+UUvYve7STxS+pTY3wz+1dKWNdbG2i2Qsqrm1vOty71ByDwq79LEaApxrd8lf2ZGemX9+8azPsw
5TPZ9Um7xgcsxpYuhIwkxfR5HVu7sv3CRf+9TIHptyeAdVa6kqZgBIzqi6UX58GMstQCg9l6APNL
54QxDx0XVT+P02w12m8QSzChwByqFvLODK/fY5p9AvtUNMwtUvQaQd2l1QxoWkx1K0UJ8Lcgjsqv
riioiiX7h7sRFGpgXlZPFCTpWGuBymkjuc+VE86LgVZHQ4x7253VmY5Ff23nsgDF5lJnY3X/En1t
OkKdhJrMvhJmrQ8yRSEj0I7EkRyctbPcOr8xUKfwtRrMPtWPExHUWEmb2EZCgy1R916WwCOzua7Y
esoH3JWWtVXqeAVZw5+SfbUezUpwjkIIcFH17pI9s+NFPRrlgeRd0RAdzQdYkw2N/wZwSmsyup5H
Co7uFEFKDyuE7hy/rmbvQh9Nlqd+P5T9I9+xe4agqwHTwPkjibXc7GNBeVV+xzFRiLpL8rLjYe1z
2plbQ4AtFds3zceQD7IqIpLOrEcb1Elb0Y2VDKwobIW6LyeOvwcmJWWYXZghoLkvlyvacYvSbP4V
Tz9j8TWGSZKBTskcbJuhjLL5PHumwCGYxLWYRsO0Jy8wD/hGETsvjzjTARYuj5iwgeHV/XV1WHxS
j1E45pz9vIRN37VNlSPmF0XPmy77z7fUmQgUXlPnWq0I50y1bS8pJEXM23Wq24qQ0jpyFS9nYPFa
JwxE7/egMlfpXIdhxZm38DtMOmT9PJEF+Ww4ANv5NWTzfELmIZaMZ0OhVb48rU6SJTEItE5a/v21
bEcT0JvdaUJ3CoIYrjIUDJj/uyVPSbQ2TUf5dnYM0pMav5Yygai9mT58I5+IOI/PM1kji5nGRWPI
RVr3en4eMX2ZRN70+4lkPiuvasmechgwEDlcAavb1Xn9xIYVn5IwASGfQthVWxC3qid+br7/dRp7
C+TwB/oGcW9qJM+YZ+hNVi6peuCArUfHP0FoTYI0Pg1a+IzE75cvrRc3VGq1vQIwHJwK9IwS5VIJ
t5oGUkWFM7i4NMrnVPMMoT+Qd6RTjNe8RxawWWyUYfj/J1DOclPUpN8/kDl+GYN7oiC4SgaFhBhg
BGl+wYuEaqSZqWYMoOiS4s4UczMSLSSt+oXDOJQKtGtFFkWymeO/D6kgsK95yvpe5h9bYTmUuNp8
RXHwKp7K29o9jhcJ/KsDf1rWBF+b47glLQ9A2vqsTgVb7xhwx/DTgvmmKw0l7C5WWOWVfjnWt4Ox
ZTWkaFqD5ZnFkmvyLel7NHgwjwiGqHGQuzvHUy555cFNYRcQXZl3UeXY/ImzKgC1TKRm6lTLR+tA
zBGG+71wLUsnOk8HxD1mIHdYpqx8KC4Q7iioOc3xDIC1HPRy/iTwQcCrufoRRzjwkaf7TiL6Kun3
TUgjgimFrNTkWYMbJ9TYVrF3opN6UoCCDewFLB0GXdEj7Q5d+xPUxs8fy1VPrH/DyBrhe5juFGjX
KMUO0Rsy6DZsIxIY+y7w5GFenzZI+l126iM3F8unGaBFR9HLVJYzlJxsOmhIP7xMdu91KU3YcSMN
ubWgDIsPgJOXJkoBvCmuB7AW1WgOeD97LRZoFm7HAH6nZ8mRLn0DCNq+/Wouzn8P8BgeS9+9+AkN
Yv8oupVbKNi4agNoubUoQOdRfrFkmnWI2cynHDKoec/Fd64w4xsslT4vGFyq+JvY05OBH4pqAWEJ
a9M86wF7mKXecZhPmLmP/Mqvc8Pz/F0Cbi777PscG1orSU4IQAJqs3DTkQyoS4Aldl6OIqFW75sh
tncqZd6VOwqj3YL2L6i0UDpaY+4/x7U76E2zn7ARO0/2FlAhuHpqIQm+iUHr3XqWEV005UfwvpjZ
bHlRe9jOpmhIvvn0s8LwQ34hDUqEFroQ4m/Cm3VVa9UK0YUQbjmvOMWW8Ec7424TD+MV219eW7pj
b+agD29kmD/2wZYukfpU6s+TQerGtwe7ZI4FaEZ+GlsrQE1Nic/2gdEApfbruhTViuEMtxO4ldR+
uFOUcnxzuqVm5uLsO+9/KmqR4MS0phjTUqBk63b/IblKdoplvUKfkyEVtnuK1wgJW3yHJaNgO5Ln
trmm17fsCGQchysdoyUJFh1LcFCbiEq7eDvPUSR2B3GxWSHfJIXYW5P/TZeEd813MrYw9rkloNXx
PkYnD0CtcAwmDQgZmmMRR58jHlu8dXBYdAoWEm1e2kSo/rHQbGCw5ULhj9gFArPxfuGHbD6At3+q
Qov3mA5EMZ5HCoDJYmrBAwwuE74lyKR6suebmlgfbOZW4VqWoUt2Hr+tr3qA1zYwrcgvoUnFK79S
sfyu5M73K+Sb/YyvaWpPydVzojL6At3LkBJIVih+Os0saj5w1ZkzGV/FyhhtroTA7G11vmdPIP8B
jbY5hFDD/hD9lIyzKxH890MaELIZzPGpj+rmRpgDuLl/J9qopGGzOGjDHi1Ay8qZzBKV8DxX/6TD
9Hrf4AeWhMPNiy6iB9cEgapO1v8Dtmzo8kBXcVLB6inKMzllF/6yUSIAEHenBBsdxacnuy8Vb+sW
PiV5ZZLl9qRkIG32pE5VXMV6KEnTE9VZ7R6HNkc9W+WmMi3YO9LGqCO+KRR+AjGurF6RLdbspuFN
Vj4gwecKylrurVcg+MYr1mJN5hFOrCHfLlXrzwiYEe2hmMV6g/rGso4588RMouNOe18yygKnX0XX
t8ETJR48/TB0dGOeqZLaUfI74CPHi3xiAziAgGNIw/G1q2txWmOYKaNLCMH6sT5eVOuuCk13Hd6R
qt034fj375PgHJ3UXXhRy1CNAoUh280RvInE7VEzzV6bS2tvkvfAJLS5yVNfCTYbLhrKaVrFfZ60
5J5Wws341I7tT5hrhG3wyLi66xPtquvifJhZLU36lWUAx0OvW96Y0J3AiWJVBbds4FmU99TET0kG
0ya8IEBYKNghcvTg233duOU81y1H6MZLHHwW4W7RX7b2DLhr1Wv5C/1YskHMgo2tH46oI2TPMmxj
L6Ateq7U6YTlppEGBEThujvAqqAsBN2CZ5B/ztVajn2veYd9uthy5mq8NOQT9yPBasu3GUVG83MS
bqIk/Bnbb4tZP/7+GFelloKuyPLw3oUW8yTSyLNEwvqELZ3H0E3scFkONvUujrdmfyhDJ7e+pvoj
ItAUYQS41yOUxcCcAju9wFE+wHPJyXHVHi+NNh4xL6NISZrTVfX3R/AzD1gaD1vN8Z906q5bKTI9
br40l87hl3TRqIOS06oOcDir8NQ6fSf6dg+oTZyPCY8nNAoOEaIgkzvaG9FDQVN+0xglTHX+hv//
97ET9kpdG8rrvn3QikgM9zUySzu25+P0piuzQxwF4+RdQJ7N1NqrOj/OupfYMckTnwpyrkUiOsL/
cUbIj2v6IjNaPW0wujxsFVpNf+t2e/2W6XE4cl9odxHV8B5oCtBJK6xSAs9vzQ6vsR1CMW0iT7CP
01CbvCRZtpZdwJyWJ7kCFHKEfeRYnAP1eo0ioxkAXguU0i3XRRVqPptn282s6CZyWQu7PBPHsdG8
PQg99V9PfWx9JMp6ZyYAFc7/0cao5UQY3087Ulk8EraONkMuN96r2cDNc15BkxlcJqSS0viI/k16
px/DDPxJ4jAtp9nEHM91inWNE819SDntzXinj6w6B385h1fL8roRonuKIjeAIcddEa5KY2Wt3DtW
ZSxiPeCUIovCYGovexVSTa8Fnu4pVurl69LM0jS+Kv1RNM7nTNhDIH6Vi0JZhjYvgkryJmurHWqt
uO1wwy4PICNnwaEY3EU+YqGzdgGRmF7WnJ5nwgU3ZNKpHmxe+5FH50KY19bqF+xRBMm9wD8t5nQK
Hr68nos1/Il9xhgtbUamd1rDAhUODDehOgbLN/z39jtQiQu+sSwR36U2NDnGHuY053kI9BKZosh4
0G9PkiNDXPICndxOSYnSZbQj406FwVhF+rCvh2EDrGY5FA1fzJpFxeLGRjaxRYQYMau0griUY7V+
zIlsuuX2sbtElsChEiUwRP03lQ4By5CbrjNkDrnawcPtV2Kk+5h/vjypY4ln65kv0yRGyyeHsFfN
bLZA7qwHo9L3KI0oWbPRduGTP8yRuWvrurgcus/cYYiB90+ehAFDcuYbU5VIT0DBvNvw68J+t5Q/
bpJOzR+xeDzReyaWLHIwnQM8P4+hOhxZlv65jtu5wuO1V+rOo2gya1bWluQTXJcDswAEsLEzR+oe
7HCEzGeusBPo/d+R0LlxDN+tC+FAWxrrkt7hbMqKAVPOv0gPVVNf/h/eGUFeUWOiv7R2Nhrby+ii
kiot5kKnFN0F8TSFrmY26Kur4Rx/NM+CfJscq46rXn6Dg1OTphPV7Lj8uAj2F2UcCAK64kUkVgEy
PA6sdQl5fe2whaCbFY2ioZb0aqr9iWurtXNnu2o9EaK+4FBS2p+R+J1pp8/Wue7VcTTaxKAAx2Iy
GSSJyZkVXrXTSfKJjoOkOIEfgulw3z+bDxnDM5QKnf3AIyHiMz1ESTJTqWxZ97W5XPHsn9A0nedU
2j4E7HA8RAm0i8iQOyUc/hZ91Tht6768HULIQdaZu0mFsBVwZdGQSBwVloJeDz0J4KcePmv0WmBa
t9U3Dkw0wNb4Og9N7jsnNG1WjUO383jH8prhuyYBy6LjuQQcTSJr+5MfX6v9VzQZDf8er7WIOmki
NWa1WeErACQ3U9ZcZorJ4pEFQHuR97a24fH9d48pPv2yU2omaK1Y6yj14DniNIpm4kOShhiNxLYU
mchFl0py3FXLoWVhooVB+1c/+m7LmXXLx2R//JHpsiXi9yS5CwFhPV+aU/RQJY+Z+vHlt1PHi9fN
SZ1125X082NpCHzq+p2SizdYzQDkpAc1OlSu5cwYZ3tugNxiie/rO+KgGkeeDiMf1YmyEXVwdDDO
P5t4iedb9ijYD/0Jxt+c82NLDQiEGlptGo1uwYzOSZpj5lKckbrmzS8liq4xBpEHcUwzRXQ9QVAX
qDwUkiecQVgd3nfcAQw6xIu0gqsyFUHbxNm396H5ZErtkeayoZKXH5u354ItLTLTeL8X33diViW5
fKOZKzscPKmKbjJqwcpvORfTQh4muFowY9RRYz7byyRujehF9d9dlKVivYG8FzA0psXvDtAcYXMX
/10yE6c8Z/GjMpdzaOG0yFVUFWaD9eWOjVVXd9i6tbkoYFksmkdeRj1BpC00OReLgWgMt5nrJAtw
7P9AH+Gmdqgk/BUriPZn2O0M8CixCM1zEG5mbIpixv6P9WBxvZezFxkyKUvFAS4tw0xhpFCjlIvN
h25gQAwrhfALwHeBH5QR+2MIX96SgljTVNdDBSDdC8RUDfcHqO/eR9H5x782iFBn60UbRW17wMxY
uniSZrR218Fb3qdhYl1hdyg1d2C597kTFirzxHqHZUH7uRzEllC5EUB8YhHgbN1C4L/APINsP6W6
b604JUYxKCpcaRcOi1HTyb3AMnfW8B3sY1g+bKd4vn0oDggNw+u3OXl6Za24PzEV4AMGp+yAo+nO
AtqlXEOfIxMPP0vFHG6hoWxUi+HRGgR8MseMlMCAFBmkyRnEJA1Xczg6iV4XMiQ4vWMqVZ9LBq42
JGx08MjYx/h9DcOvRCIiG7utJ6ORaVxj2HOoxRTa8FXXBqzHvCAdVv8xajBftAv3SfV7ShebcRNi
6/yyYQX73SSL17gc93d87QrPenelSPtxI7T/gUFMB1jVkuapXDy+VTuKdETUIv391BzPPF/+NSO5
8m5AotlaH/g4gVzIMv0iFiherHrgWoxPt/wochIBTxTzxl6XK0G0JSWDq8+K2ble7+AbydyRqz8P
VtHPfZZ1mT06Xcz+QX3LuY3k0sF8JH+5MLbQOslSf9zdHWYyv06Tq2/0nfoi6wshhG3hO6br0y6n
PJjDzTfsyzvobeJ/EDMPphAM2a8+feSN6RaHb75yHVFHachQz4DP50temDZRYi2GakZr2b15R8wg
nmThWPsVs0bDuFPGwyCgt/G0DWso0WG2n+e1lCg18bgiNxikzNX52EAy0t5Ah0MF/GwHgr7Z2Bdu
0iQb0FbGK+dgELt651mt2lfIi792PCk/Rq5UU7WC8Y6o3FA9Eugz7A3wW1koB0CD/oG5ozqGbaGx
fVkhaDkNdCJ4d4mAdXhq51WFvc3TiCQx8DEvEXVIAharydkb7UJeHkburwwXdmf/KD0wXlHZ5pYI
3SX1B9QAGoNOstFe1qMa4nmrYesx/Qf3CDVlFKn6UvZe9x/MU4FHUyyuDfwYZ8NE0ObfAdMj7Obr
Ciqc5Vtqmyx8QcFSwcrRqtaaqKAHWcU5nPX54QXGEiyaXamAOVz5WhpomKbdhyweWeNKtbt1/CAA
Aygnwd4RoJL2monJ+i7lU4hqcZKE6iRLnX4oPFfVXnTld/mVLWWNexE4QFwB0a/sOeCFos44HZ5S
oOUrZn8NkzPvUs7lcqA61DhUSLnItvVADkv2GujrwiVJ98/7qSxvEK/8wXWJll/HbyKqDHve2M5m
2dj4gbuVzQSjh8ZxL6ZGrPxWAjwgp1/hcTU5Q/8yraZ3h4AZe621qb9C+NK0nv9d7B7jX7X7Y9hM
6+yWG5HfBx/AQPRXYxDZSOC2Dn8H/Z1UOWk4DQqjlX39U/Kq9sQpxpBRh1nFJapFoHAp3DTXdYJ0
srpmoXL99e1DRfC8qzdfuNkEop6b/g5daTRYQVjxsjcP7DXMMTD1Hkgrbq3LbY5eoB9gko7RbtMl
S5IWIM19b+kmxnsDRAKaNXWCJ30Hf8iUY44f/lVbfPFrLCVDFrTJIauBavpB+iinjWkmgv0RUV4w
I8NpUZz+NqNLOJduDqSUNLHYYQe2phNptACDnuuK8LEhpu1iXeekl1TOtcfSIE50uIXe2ye9IKEE
JLg3JFPTypF0dq7ZphGtmB57YR3u0/wsq8mIZQC7sKrTSe9s0Heg5HsuQTqekjjF9DbNHu0cT2Tc
5X0kbEXVPKplS7hNCeB6sm7RLE+nO4zUWD9qeV/eLcV9IMdi6tK8gPYtpCHRTbR1g01ZzlS8HEZU
55IrJXHkzhcBUgrpcYtNcF2SA7csL55866m9Xen1pozCkfaYItjJLMKkwJ14VCFXicaTlTKwMw6u
fSVi7xQLGrAhIPtY3MpUnj0NDVHbSuh8Lg0h+RjG0tjBpUAtTqLcnXTNR/an4OogsYPItFsCjVzM
XMbNGZ7d6GVSeaSNQ4xRyIJD6WVpCtjl4jTsv2N6laZxbU10bD7kxwgtqOeb2l7fBcdnrsPcAu7a
FxPXQzZhv7x71ozEt7GZxTn6u1JzQaYT0MsaiS7kfntaS2vPUStXTVvwXIq6P1/6klso4r0/Neot
kfYAcztQuB3o94PNa6YUWSx4SV5yAtQzTFSZPVFw8iBv20RTGk1CMM5oaYxhwA/DpwZ8qTPa48k6
iXuCQXg9EmlS92MyMdZOKSi34LgQvqwHWNW8q4bev4adii1amoq+iihzN1UItnQ4eSAwMoNeMt3g
yXQyXwE59u0zlK9AAZWZ5eAcrxhx3473pyZvNqTEn6SdI8SAYHyJ5BVi0T+eUeX/iLEUmWmqRtqU
qTBfwmH3e3wzqzK1SdYmGKn5HpHr9ONEX8FqsKjezGa8pyXSNGktBLBkhFkWdmvglwRimjEMKKuD
gLfmAe7HBjjEFopyll0+5mZg7j/wq5DqsnR4S+P4oPeivM+7j5Xcwz1lRlJJvwsRCXPyaCCLhM9q
x/i++it2EC0q39/gKeVH9QiYBlqcSygJKHHKrNwMEitA3gSJcK6W8i95PveDHEaPf9Henh+HH9AZ
nHod32n9IvA7H24QRL+etl4F1odN/Vmpupfs5vk6XqX1D4c1aqLA8DkZWpuw3qmMjVJFiJMg1AW6
3kHASiDyJbFJOV7l0IXgfsx377WSlTX2gkI+pa+3taD4Hm7YzPScC4YYjGpVNLlfoVtBJpdGQUcH
/62xiLpQAmdtLiTulQ5N+wJCjiPyrn2HIHaARnKGT0q5IngJhR77vfVwzl5MsfAfboZZLSXbVjRp
1etcXZlekGKcWBwdjsPxb8awuxqgwgT6NZ2B8uOMYYglhCchLcmT4yu6I/NaZWm5JcKUgXoHTAUv
8GUcFQ7SHiDtCurDXEibgQa9KCoix9AhocfgeJ+KM2/L2isHsQ4NYuSslJkpt4JAePi9+2+rxHYL
xnU1IUvthMprnW6DI67jP1tW5NzuXkg5n8oJiZzJEdp36P4XR0psg4WwplHmdzPAZPAOo8Wd1rTj
NeBpXaZY4rTbCxJMFYE1+3CHIapvjjG6SXxVBIA/x4vhz65DolOJa4XLmQFymw3szFS/uHtiaXyt
I/QSmLDiAaq4BUndSDUsG7STLCfHS39p5jnHHyFHM0ooJPq2lZ3uUEzaIRRgHtu0uGnuOQOqqRZY
YwInBGFbAW7z4rqOkxmX5IsaJ5y4Fw98nwhRLfq+zcYt1agFJnS9LhgvFKUreazN1VOK47Ef4xZf
gD5lXdKfFyx9TqIhDodEnExt60Avm61aI/F89P0DLPgGhyqmIBf5MCT56EWgaBoapY8HBpMBo2bE
RaScYJADSoMZXVaskNBtd7NnLBBz23tfj6i4deDU0qS3FNZMwEu5wDSHIjZ1EqnFn1q2hP59hnI4
IYaTGM279E4oRmSZcwRl25H38T6Mb08CvPu7VOBRWIGNPe8K2jY4D7me1a6GMhxVaIduUkQQv/os
Z8bLDGntbrNE0+ZduJkw5adwca6rAfSm5cs+w3fLynRmAGiDQ/UiPNIVGf1k0jAoAET1Hw7+O9IV
3R9SL5hRPtMgJHvUHxOUSABGwq+P+BI6HyqLyfb25HYifMpgZ2FVrkitR0OMUyrR83LOu7i0LOlg
zm0mBaDM50L8lFZrmaQOEbpQzZx+qfyVBkRMQddKJbLU2IeHfIiThNOmOePXmf0Wn5CaRFIR1VfP
NQv7Q+Aliv3kvUqXpeH9yUTwm1PduHbWmP3vLAXprlZfU9A5+Uuzbsx2f3kPP3QAv6NCkx85JE7q
Qw469d5H+Ow3n5opVNxzDOrkCXnMHhF7CUWU73g6kIbK3IIOtInxqLdR7dypRrBr+qQNm0upEBan
CjHYEup5rCk/CtYXBV6rq0/qgnw1PMR+e1TvigeRE6GtM9QJfNE3IfYPN9sPXYg/S4kAhiVWx09D
ZK7QfJhvrTGBtLztnDUnayeDje1y198pAtLSdRwQPFtUOWur0p8SinM85kJ9Vqwt/Cq/EV4+rrni
nVAF+485MLL9/LBBowERINiDcoiHRC6l9w7ZS4chTU4W/HopKA3THKC9vaNm/gKdKmK9FwxNZ3Tq
DBSdwvOEotOuZQZuCXYBFdRuBL8poFcdboz/Jp8eRfIClHOS0T/PlhnO/d/YXMOW3SKHNyBP9yeH
bVI/ZPLC1+cE5Jjbtr9flrw9sFqSXxq5IShAM76Bl8Dr1G8bkatBLjVL/XNVSQHyTPN8mIi5GzY3
eYb1LRye1pRKOFAV4twCPVpRoTw38NkY8mcUwRPOFxr/5ywHjQPoPlO3M8ZHJuk1KPfdelsl3PF8
KjqawrKRz4NNGFi6uwv73ge6FP9AZHs/NGI3loAjcrXZvG66FH2oZSmb3D+ADeNQEWI/WhC3um3h
NkT/6wJkXioiMaggEIRj0joRWitTOCEOI49UAIaPsyQfE/NErtgKDIvQ/b/lEYE5gv9EdbQgNeCw
UDz/7Uh2amzMtZdc35aVDjdOaePfjlZRM2pzUhTiJyNTypPyenIlTPE2X3y6aW9wUF+XfcM6Y2KR
Uwz6dCo1JxsOeLtmdi/eJuJyJDMF6W03rtpY2mTwl1UBX+7CC7HyUZCr3TywUiPgneA5AJewDRPF
xfVlnzfGWoCZGj+5WzhD8dYadfHe9Vqee84jnBgqpdSiaq+odtISq6hHV9yPTV/4QFY5IkuwupAy
fqwricZnwrIuBmEhZiwHiVJRV77PiiDCBpf2+lt9fTJhxMsX+Vx+3I//2W0C/pBD3NvBx2kzt0n/
d//CORf5gUtRJu3ZDruYk0xuLHoZd5ArUxGR6THLj/rclImr84L26QfNdK+Y8jT9+pxvI8gUczrr
37d8guGrVDvqbkbBgUXNd/4nrZP7ttN5TJXQ71OqDud6+VWgRFYuf14+nU/Cac9Ox9vEHXwOkaIt
GfVGX8FqWzWEA9yunOpPVgCdhxwo5YXvmUTpyBZe+jlNO0iVLrU4bn6YrYWiQrLkzOSQ+DFaoQdo
N3p8ZZbr425wqUdwqINNkkZFjpFkzvL9vRXU5DNVjYVsNFZzbndYLmHsFMwFJWBtzhHZ23SUs9eY
FYaMdP2w/ZSV3SDWwoNaWUmvAZQJHFyrEtqutZ4OtgQp7BPCSkGrm2x+ZGzNOzmt+szi3oxa6Wly
KS79GHbqJlVAPzEOY4+VZ0IQqxhTLV0STfxcm5V9DEeiYGLqUKCsI3J6QvO6pQC+DzQoinxwp4zX
3z/BEGWqpEn84piRvagaluia5KDLOGFxcWbC4pv8lRHf5ykkr+FJrUIyMf5xWOzYLbJPX3KF8ma0
uti4OOIUNRXLkRhoaINwN4uKOL1W43+liu9GAUTthNug0GnBbEEA/eSC1FRWoBatxOKVwLwWzDQA
vKbwZ/tB8yIiP+WwSOwiH/tmEcZgh1yok4zhRS7NQl0yPaFvVkx5iOtEA7UkMcaXKSd29GS0eQhB
CGWU7g8VNc4obU8aoF22Ok7ZtyPKquzh8H0kcL8Yh9nhRrILAR1ag8F6vG1b+2h6Rs44+lOWvlA9
KeYV6P/zthn6uXo1pyH3mAH9Wwfg1loKI4s4QwLsd1P85JZVgOS1DENNp2ds7cssqoUztpkB03Ge
yChNuQdVLJAQC2UGI4B3KXJXR4QGY4b5w8WU9D7iImjgtzTAd2glTKWLAXvqSHruTsJqqVa6m4Bb
XY2N8DRUDWR/U8NAI4e3vs+lMtp89UbUh8WahephfFDoLy0ouZUoZDv8lDUsL8Yvskuvs0h4d3Pd
DZU5UBIInXOMa6/4LV5cJY80gv6KfDaLYsXPRF2Z4hDT0UZicz8tw+8t49g4QI6uEjnqspFTnJH4
yByKqovekJEVho/Lsc+KdswB+Qsw1ssJlsXTS8lZDApXinkOR/VOCRVqSxIU0nSHxDkNePAHsxZ+
rYPNsmrU3TPrR2C2TdkcUyjEchyFwPAh5/840mX0SsatOzEXnMj2NwVeMLX1TNGYd9UH9ezuePwu
CQ0bBfQGiMHTZ2xrmMJ27pgI8Ep+LnIsO/AC0X1otNSkV2Dzt5t9DwUm3jtWnYRRF5eP2NTkEEgO
xvhSFDJ99nxebt04e7srKTXH6iKwHTavJ/FBxL0riAw5aMQWC+GDdECgVA5K0FUgA3xkyA1alO9C
REqGpbFmBhPoe8GuFXlW2Np1UB9dYZyugLZF+P7ueBrrUDYbudYpUpkfVuvmaVQOYXkfArJPrnfb
hSs8LzYFqzzZZx0xyxOopukid4xWgrNHUBJoM7zpR6BVmXPXPbrE1F7uvn2XzEXHg0lR6Z935/SR
imLFA0FdP0YWUOJTT2pAU+DevsxKpTAuP2DR2wWbaUz0hDzE827ijMik0v9H8vqRPzaG78fE+Rpb
W8QQ1C41OsVBDM0lYnrNy9qyYJLiYgXCST6B4vIUMf0gx8YmgPLjUNBaKbyAqll3CvjjTyPx5lzs
Ft7vmLcngeeIt3u1hXln/D+1siCwVp7A6sRWcIRKZ+A4XLmBlFQCItJqsFmdUU48rZ6x8TtORV66
TiTQ5nE/W2x6Ha59rt5vyRojRoa5yoxuVQrSm5Uewq94VkO/UrVt1NrcHvbMvI55s8o6xfiY1prv
X/sJjVA+ygqrsP6yZDld2+7oGoUnF3BlHnerPao+kv6M4LsptT00LZK7bdnBfgeuGMo2cb9LMUsD
83lk5Plztgs5x8Fc/NjgR+owS7cw8Tqi1AS1x+sAM5AmpRHzexTUBNFUawJBoTEBn7szU71f+9Fh
cfF20bApy5qpPgehKNlWL/7ODQATku29VQeQ8WhhXZhgoykzFr3eEusXcLVJmvFKU2IdMBS0Ai/y
/NodZQtHox8KJwsXh7OdoVfMyE21e0wSCaGviejN/hd9qgwMGtXuUCjf7mR/DerQjsSA+aELAY5C
LUya5TR0Dny13hFXLFOreUa6DC9bct7lOXIiCb+udGAkY9FEGUISY7J+xS0ZA41/SpvmJua8npgM
UMkQJO8f2hIDivCbDuJbvt4DqK2LLdbFQzoC+KAdNcc/1lPvUbZ91T2Qp7nPn8503VZ1/yjpT0pE
qqCbrUFgP/yirWNRYhqtTaS48A5cEaa3GX3NaUbS/yqAmAPlt2aOEOYetl1UMR9Dw+q+puY51Pc/
AgnviuWZd5hqAV5JZTcfrAcpC6rMYN6e2WfeLmt+f3ZI0nTq4UoLFZf4t/0MWb0pOIJ+UuggEO+t
lp0R0nrGF60A8YVu5qA1qQVUuoyMPWf6LiVNpDUAkxx0udc/2pq8NYIx6/6+hc+IWvHweaTr8ykD
Cbyr1wpJdccJpZWyLldzyDBrZMqZEBPHrNGXZzwtcD+cQD9WaZejh6mQPhiaAgpFJir0aZqrPVfB
r3gn2S1KE1zqFpRRpDdrpJLWLcdaBcC7yU1QeY7DgDAQQELocZL6lthKD9Orsrx/7TqX2VExIJbZ
ZPIXDlVNgNNmDnQJR76rje35nrIB+zwNuZ64BrH/pYjdcjePDl7SSTtdSccClUe9KTL6Janae9oi
dGM3Myqhh7EBg7dn9guQ139DLxCJX/uIt+mdEIWgtVg1aeSmyesvSsmvu/Q+KU34oB1H5M+ivFt1
vBLZ8cGrg6LhrC1uzf+B25SL5WoKn7LGhDQXUuykquRJbSwh3fFUOUijzKq9JNYSa75LmCvk4mgT
JXgX1fzaTMOqxIAbE34LzXL5lYeduApaT4Bp9sFYdOBWGiKBa1q55rZeFRPih5H67Cr+9MgJ8F8c
mRGM8ypcLXs7zCHBUMHaC3SBsbeiRKUcsO5mBe0MnX1iKFQK2sWt48iMDVyhygBxGjZJFo074Gv8
h1+Mj3I+lXtaCvxcbDdFoHNIZ917ts9wkEsp8Xo+GDcYkzQMF5+//wHKeGkZ+k0UIy33fpvyZ+Wp
WV+QuwTB96JZ0fulal9SW+FFLfAdd0b5EHwBGOgs69p9JSr3aOea7j5gHiUydvjBzw7v+nhL5Oy1
nLvjVtJBIvsYEddxd7Z+uvhB7A7XoV8tZAl/evV8rgcEPH2SVUEGSOWA5Eolz0fyqwgn/keDc7nP
Q9mbjq7Zxh763BWDgCYyUqj2Cl1qA+s1NP/usAXQOGwPqm3e070gc45QsogIy9KeCjpmFb7equus
1Bc9bOYR/ONjeHQPBjyY/l+GOxjDhk46jpdQYLZRm7dTO7cEm8TWSgFqqQ2zus3OqatAndZXLRe3
qoy50aH6i7MqQnUOpDBWq3hXkaBJ3/IkJGow8hMZ1rrJoyDdItJj8nRqPGIc9nsqxAompu0P93Ii
KX3ZVXV20jarr9+EEtM72Lhbzgct1lxJ+FR70xmWAMjLYVO0mmDuLApOBeKs4wnemPil/m717b/H
Q45K502cqDgpu9j/Uxhadu23tuX+r5TanrQEcyn1yFSfBh+DTHN6E+gAILjGsin3vdB6douLY6Y7
2N0t04xXJvZbjpo6FCPrfBmMuaWI1U4xJx4R5hRaEvVxYnDFk2PwIojn7ojR+OAiQ42XntPhKujq
tBGqfzp7qHi0Cfv9kBJL49fcV5TYihku3H/GFb4XQod0UMSBcT3KuEjtiuuoVa9JJCn8kG0dqr6E
IPE/E29cSumlbkZD9R5+6n1dfEflLWAPgBlN4l66qRugcplzfFuMd51w+sLHihKDQo5Fl67lcQ6p
j5HBgmqE6H837ofR0Pd/T9L7+U4KvDpn/6yFrGiXyHAFn6ciALYoIgr/wvONFRMjpNB2kep6RJdZ
GpwVTEwIwtE9gTnBAtmU+5prWdVsy9EZIXFAH9L9PMkU5Wz9LivkPEHj1/gZkjcV
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
kYrcO/E+Jhm4R/4R3+CukKYR9M2FIvcsEHYDIEQ941LV/qe3nw66ouV0tjU2K77WxMp0KzE3bUaN
EkHZUhS54Zbapq0AAlHGThTWWu9TToic0Fogfo0uxbTRj/YKvsYbGHXn+38UtVT4gl+Z+q34s2Mx
S+RksJLLbqa/UjuB2IA=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
k7VYfhbczr+tglBVnP2dNpzQUg4faERuh35S6DlbOXKmaLBzNWJuLZKd3/iHJso+4/ki/NZUVDCo
PIbVzwxMtfGyW1fMXDvveUi46OnejPwVxk5t1kIbtSbcZCd++dNgqg5UzMEgptRWzheZuzX0GigU
yFrxhwF/EKgqip1pp6C9cstz8ElT8YbfLOW5ZqJRuK3p8wRTUD9tZ+3ZT4AUQNnb5LwhJYd18bKy
gCZ5WG9Mj+aMW9valUSRFjEY4oFOYnca2u9dC1uGlv48Br0t9pUhfrmTbufRCalBxAR594dFK/W+
13kLKPWgZzIiZRLopKxSb3kx8JrEbJXF16BnhQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
TxEtvLMShWARGvALMwAihIuShrdtPpwirMDR7BzuLz8WzVhoqvJSM5/nLMHFGqovxD5hXGIA2TAw
UB0YVlq6K3gG1/oM4RpzHTN3yz8Lt5YW3A+UfuxJr1V9UVkS6LmvF75rPoruMKpllkRnQaQkrdOH
79erJYgSSdvNFj79HX4=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Jd4QdSkhWhpPJfQcqGINGTBbyQi4fwpgiNWDB3Wd2IjKeric0AmdHU7UViuSzCLh03DSaNG2q/XP
qatCMMw9/14uzhpUJU/1zUWxXlbRxdCkB/LSsYsRRmVRjaX8PHa9/COyOOXOwziBKCZ4EH/zCO32
LML+m8CiAQ/Hl3o7OkbgzReeGFKo2yT0AlTR1mlGeI1ujqvvwRe1Fai0g+TwEJcmsDU1/5bkvxQ8
aV49pZh6N2SUhTCJ+wLBZlcMIljfD3Bu8Sp/4tL/+j+yW2zEEf4Sl33jw0Cb08EifW3RF8BmuSm6
hUeX9HuDvEf347dVCR8t8qRzeC+0nGD4/fB1NQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nE6k/lSQEQ4OmPB4XqBcP/LpC07K/JJ0IvLqk0FbQzQZjzqT5yDvPsiRjELAcBvPJRahwOqlfyes
JDXxH4G+XSbtKQtE02yLheyEjNesZ0dv/v3vL+wA09O8khSrVyP5ijRndW00Cf5Bf2IpNiaJRcds
F1ushZZu9jXeBItrh4znBf9fOoXggbdnBLyNjuw7bRfvTeY2Xhe1Z7RpJLgPWMz3yKmlUVxO5Zyf
mjNu1+82dGuZ9x/eImCHDzcLcpca/TdMV0iJAkZHrvuhhu0GfQ7zgBbvuyb+I/r0q0vuL52PeEET
HDmGQS2oxiFTbcwiGY3t/ioXPJYkEEqNFUIzSA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EYYoCPbR+OMFlmBfBNcQ1RKQKD88wkYgxA5pkdacb5EuwAeven6zC8gsLrmbmaf1Y+GE+exjL/E8
csfwUz3cQq4551Y/pgVQB6wc+K/5qus2SV7wqxTpqsWY/Yu+bULiGuBSdS51qWlfxDNujKEBhRPN
GKWkQK8KP7xMHh1W8rO4WL7cLP0qnZ7xSovnz379iAYpAJOGf/f5GjM87wrRCh+60BUmNbENwN6h
Un/7huetrD2tvDcD67Ox5Dkto+nybbrNNH3ry0zh96Cq8sxNBI7cJ/iRp5kCBgqxCxELTa7hlTHW
RWkLjA2W/Y2HjatDbYo5U0A7bO8ORiG66IX0Kg==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
q9bGXHBOyTLb3eTSnDNZfQbfjyoc3yN7NB+1C2N+mReGSJxWRtlWWn5HWbhvjoAJehclGC7OtjK2
ZSTJ0A3pHY3St3rul3liQXKD5kCQ9+vFLUhyKlQc08mhaOXPkXVrLBkSbJoneeg+zcwJuKQzPvv8
Se016G+DYsP9PPIjvWbgYSkDDPBmrvDI1+5mRe5HwZFGFGhAQNqFMnPAskAW1MwhObzaIpkQKTZT
7A6i2BjYT3UzWyOCYK2zgjiB9ZFwChUw4Bwh+H8Xf2j3ysF46VVr3Y/hfiRxPSHR8Jb8iMEkCJjf
nRAfkr8Y2ZxDL10aUR1VFpL5aHsLiRKnNRdZXw==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
nsakC0nZIZNi1X6ujQodgmUw2UIdYzuFQ4iAZwA9YfvRrxXUL7ynKQCgPpNVzwJk5S+CJlgNjRvH
avhNsBU4C+cBB3dvqouQ4tOLrtjvGCn/tgPDevuIaG5LBxGdZZ/MOgVEltPHWIYycz6nfuA5/Axp
6IIz71mUhQT3OW6kWYR5cK3zVKmHXkQGZxfNAWG/Pw5DHuc9xxTQpswaIv4ECw8olrxqfoRkzz/n
gmc1riU255Qanc8CpzTXkB0TXLYD8b3W4k0EIAYhAlKk5HVAVS9D3DfcWg27dKxRMm5dVH7ddpvn
9W7az/Gv4/jAcQ/A2wvn+5RGmVdmY2XJTvnb42j3M+6+R6PXkHvxDCRRgj7df9TYddZWyOeT0KQd
DnIaIlkFA345xytHveeTmDy6qVwsD6GrlsYJS9tCsR6FloMwjoQcZKSxBqfWh+rvQ8/8NxsGVy4v
3tFI5PwOhr5e4Nw4hm2q3u3mpmtv9+BzXIuf1HXxWr2eSaeu22WHlCsg

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
WuUgcS5b6yfqTuzjufwmIVC5kWm6y/3mx22Aii+Dgdcnv/uLoI9/njjHdhb7hUlsD3Xs1keDNIwN
3pNTWeUxyZTJzKR7udvlJMLBMym3o/ECBMv+uN4BToB/hl2qqhLvFAO/r5AFOlliZqDwiGcbQvyz
YxE2I3qA+lBeP2iX2/4t2ns07deHzxcGsGDpvkWpwNcM3RmD3m5puzv13u/mWj0iTjzSuDu+lCO3
EIjElwRdbJl/F7N/czlKYgmKd6feg7/nbSKTQgrJk+bEOJwzrhlLGQvovZgtfM2nxWwlvulcT7sS
n2ZxTDzZIZJeakYPGSP3PRWLzaOntLk4/JYNoQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HAfLWwf5IE4nVH0RKu6Ckfcag4YISAB7GxmA74RLd0WtgVtvSg/hiI6xjdDBajL3WlsS8r0EeRuE
7k3XV6Iw18PLWYY7xEqYXN+4UCUMJuuhFnCKbupuHsoPe92DFCS1iQmSCu4KA4if6La2soKs0Eai
lizBuddfJbplTj7Z459Jc2VAD/slvgcakh9coxr57R1xf3xL+SqtbztnNWXTWebaVsMi9o1R8+q2
Bw6o2bthJTK5AjuaNFC1mXchmICuCVK92/JyceC3nXwexvYK1qRmiOyoTPwPOS9/j/gup9+/1Be6
vYxlYOcskfzyxWLNti298ohd6UCc2uC5C4Rl3w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DzCZLHkutR8dxKMJJC1uS/LdG9PoCtj5GsOR4GKxJSZTHbAW3Lwb4zUisDiKbo8nzvAc+Pc3aKIh
FZY+iEihN/UyNBp/ZVBx4xfw4KiNs0WcNidwHxnj/AmT0YahVcv3MBdpFE4TvDgOFqEqCr2KvrS5
K14RY6HsADqifYcgChtDVh4X+2Nen/oSD8dZS1qLOsyQr7ETEhogVmc4Gi3TE4/HYjm8lV5GRuJM
x1+0GPRONu+RFuc2B6sidWODYyJus0b7HVqnBAA8gMcV6twjAADrnyIqZwnPoiUCKAMzsDKVKhW3
GrlmNwP5uDSVq/4QrLJ59GIzFy3EXCfFTYr7nA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17760)
`protect data_block
8sg7oDP5gJWo2DiyBMNozfAOE7PmiElqkSX9wsVnS1PWXL3C7Cm7OKjZo2qjSa694nWLiOWor/Iz
yJPjFqqr+SEsAw3hoCYZpYUJeo26SRApJ/AWkAoAIVvQ1uQRlCLy1VLNMz1d+Ip4fUY1MTIYT6yz
/TD8rnrM0GvIZUYuol08FFZo3MzZN9IGtJkdY0TkVusF+sN2IzsBFp4caG3/wRgZhk6b6yj1quKM
j3Cik86ytahXZMcN1gk+SSmcfmxpNhaNEu6TxE1HIerffiZvzJhlA7Ea9JqvCwH68WkL7YV6sp+7
OpjEbLSXXuNVM7lBsR1Tp4JMrnR4zM0pbmEgicAyV5oSrgQN4uq0gKup4UvqMz7GxTn8B5jUuaBn
T0r0RpE/ToYIdFfx86fxPFqi8QiNZLJVTP+YlD/lXpWfJk87SqXT+FD83qcrCqq3FuM8FvB4+ZEG
xiFg2bEz0Xf96bx2T1qO9bK/+jszsKAOgHjWjJcWDjH7JUEjEulu7VNlxLnr9ZF52awOuc94RZwM
WHI7wtDtuYWYFoFYUtWfsNWKQUwp+IgG7Vrn/hagkVQSFBBOgnZovejlhG9+nvMeIuWDrhuWxsFT
JINQBkl9mKPwJGe3a2MFaOgH+RWHjYBTqGOJdDpnuebvyO7okH/nptD9ySNjk2DOIRFwgKVcL7bs
pbaKNy7lt+bft/U3Y58NIL6xM87BO2I8+YhQ0IazwCsjCwtkefWjRNyEXX85sI3fW9h8DxoqglR/
jXBLJuzpywRS3tSjY6aXcgmMmyC2qvc3+hk0GjIO0Tcbhu65tstGOdWo9lNJl9G2RQpZ7J6iCNtu
SrwgJCs73yH+ND8AUuV/19DdAqOItFz1q5K8LkGVw2v7Pc5I7W4C9rbVEcYeqBS4w7RouNG13vFV
KZspemUwDjeKb9oZHWbGqAfQ1Z7ynokdz2C0eu9P4xchzJzvDURqFVO/Np21SLhWqkFOg1dSv5Ka
LS8zwE3w8TnUz5/DIEDdAbUxVrXAKl+knQKAwsZG8JQF4Z2Qophb5GnE0G/XiF0vJROYd1Ti4eYK
VZfyjQ/84SsUwal9QJnN0EAqees6YtQJ4l3c/2ksEs8YincuXVE5OVqW3qcgdbKYG8k4kaEGhCCm
kRsavWUoX07zUHhLznBtXNQcJ/YIcNcqsyyRKWLb9xOTFs2RytKZ4eHxG+QtzlOEBzRGP9Z1BA+j
BjWom9y+E/SgYni/qGwzg0x1OJUBR2DiHxENU/tERuvJdFO7111/zHVwsx7m3m2qvE3Oi4pFC8rU
Yd7AyqebfSOxfrTUqxMI1guOt8v9UxgJIJTwUbF2Xv0mgnuyyd1qtBeIrGVRF2supe3vxI0kv14D
1wR7Nkltjkdp5kmJeMbdrN1fgtxkfen6sakhOnAeWZjtfXdE4ODwEHerIekZsPKYQZ6pTBIqytwj
mQTZBWhFANJJVET62obAZ1/j1/cjeEhKcpcVReaVFqxD0pEoyh6np011DAgJ4BtfJOWBg/jo1PQs
+6eDrwnuMz/xmOvrHllki4kb/8/JK88r6UHeLgWeWlaUNFb5LxHXPcbJq2XoMD+isyICZ48PUOoB
ex5W9GaGw8ltecu0UM3RR0/WRqM7y7/VxB7ZRfTJN8rAEzzywEcp7eIgxJ3CUPvToLpHXkVIi6f6
tqPIE720oHivGA8wLpjDUSAeJDpXO3x7sV2XuAvboA3neKsvTiSBnJAUKFzGm2SQ08+ywow87yMc
ANAV8Jrq+I+tDuPERrI4G6MW0FWfrDRHB2INkfSkjyLD3XfXXabMsdQ/lKvf6hbzRvdKw/bDZvsT
2UEAK97FT3yyuILPMF+kELRS0y3GYIjgNE1NTtpmQd+NWeXvmK02FN619CxJjm4Lj9yUDWGXEvhV
XqS6f4arvBAa0NySfvVkVJrOGBxbB4xfVZgnhPRqQefYztGVc0e/2q+ENuQYCdifdHnYO92g2tLH
RCYIf6DapVm7Fh1/vdmds6bQRxL+hnneyI0DwDU/CDyJbfbmgOu7Wy0a2hajxtTbWIFcFG7oBACO
IIrSWuD4KvSH9qe7hgQGuCS01rMldznaUTD86zOrP503dul7osdqpKZMhOGWoQ+HNDHckW1aqBLZ
ynDBp9rupGSM5F0m/+WcO4Qh9CjE0KYEVgHg7KSso6yJFkHDh6WxyRPYiVIOumwF/muy8N68iyUB
xVeIVg2jgtzKTE8c5NgJLvUpDTeupbz0THlADwYZwldb98eEFcddcIqQqK5YuUaBhOc0f5tT7gDH
P6U68HPUSIvZz94Y/qNQcFbExXnMFHF0Fad19T5GPtEIdf0Y6iQkhyMywmDBp8iLI/PY49qtSGLu
cXoV81RFzGdhRcBxQdS5kxLnizH8qAwU0us5vICrti2/0sEjsWThVGpvx7moGvxUxNjdrP8bPQST
pVsstcj2M/f+8FzEPMCZAnlAllxxchbkOmpGClXuora0t7jJZSKAUSXTrMg8lwFpfbaSLrZY7LSM
hCcQteR0tYO8huR+l1G36gniplVwkh5Hkeg6ymwqblfpT7uciq7qtBAx3/Kh4yNlGqoRfqmOoWQr
KdnJgy7l9bBybDAnOPrs3FYNg1qvepE6+KK42km+gaFW0LqVjQnkX9p5MCD13QYeI0LVfzk4sKgF
AcVKdCbJahGsMwK49ufV0cgD9XQjBdocaR1PRs2k/Iwnzff0RSJ3MHaclDxOtW0/CuZAam+T7iW0
9AX7WcwJMu3wuyfhNpenv9mNjctDsptIPi7pmJOrgOQzzZJDGd9hdWbrp2ekBCHEPSTiPuYGCW3l
mAhbCed0G4CBLtNjLFLk8Vv1aiYxvgsqxjIx1SQjVrBmL2ZZU3VT7OR9PZ5tgk81wPuzkmas2j+N
FyPrraqvFvtjHFjxSXSaFhtnT8hMo95di2nGTJgUIMIiiRrn/M758q4BWThzGv618qAKfjslyUya
66YlnFtDgkwiNzFlVTXLWEtYfqLtSLsrkSTk33SeqdxoSMjeK0d7GidGW3IzNcgtcXwWPgMldmC0
LEHaqJrfsq+KbxKPVbIEeepUhMvhXb6tqKYPSsLKIuTq1flpXBMyMzXGYGzga2fC4uRg2Id/hsXv
mDvAKkVpfACAZSvAi159e7Tj+tsybcjc8AF1ArNAqD9E8n9XHzcXbUK2IfNrDhVnzeNHqYiFxbHk
Vw6lzf3NrBfPiIM+LfwQvUU8zkVYwMIPDwNcQohXPxT45TLLMViBJ2SdGcZAwjtFiBiHjml6+k0Y
iiH1EhmMzGj7gah1SflFIShZB0hLl9kHsTSL2Az8LqLWgH7euS5GggMJQ1jm/zg2noajbzO2XtOB
96uFn5KUbBuu9BYJ1mEjzoDTVKENQ1+Dytoar/1LBg/E8qygYmyXlIZAW0Zml3SMTLkGco7v8ErH
un+ENGYpHP0aVxugpog2usvDl6BjqNkNYN6BPZ6XXJhPCEzeSOZxVknD/WoZztOy0U9sWoRYbQFK
uVxYjZIdjQ7c2Sq5PjOAbEIGOVWgmbysbvwzo1JyyTmPvV+6o40TAxttKlO8xBqLPrJELuoPbTwZ
a7Q+p2PYBopQ7tO4N6Vdjfm0syOFh1x0YfHhJhjtABJsENNr8tA9kueEENYtuzQpvCdHkjq/sBMx
NfW8fMYbyHKwTykOIjjmgZIlcuEWTIiu5t1NbYAI1cmRpHNvwC9PT41C55Xl+9sq2GEdHjF831Qu
hoERV3ecUl5LHb2k5EP2ay+5crj1kqZq65BEK9L8JnAHsQOEa4Lycbk8nSEMQ5cNhAbN0KdGHi69
se3JjLaZxZsq2Stnv1im0HTehWb6IbLJh0VfLcsIDAGGJMzblWZbUiYYddZ2vXIpH8VQr/nGGS63
7MB/G2QwWiN3RbGmgkfYCLLefxse33gMLaCy2OKCer3hi481r7v0edAgaKORaWFsAtfPf8ZxexLj
7ZzmF46C1ZcoBzZEFyYAjtA1UtePmnZ0DmbQOSYpH4h8k5SG/w/+rXCS84phQsPofnd27rWSTDvC
fb9m80W/HJ9qmbWGFhxTuqNr4F1yNRe4U+X+NFPwjqw/SHovbdJ00DoBfh5GwevYQXmDS5PU/ucI
UKZ5wY8Ls6Z0kt4z0U2kNW8702CBJrQEUYJkiX7kq2YQTorjVL703cRUYkCC90fpvjTSqzSPinIK
qV9pB7/OKJ+sGBGcIH/M8AhawHpejuODHriLfHp9Ndm3AwtEauCKJpgkoo5IZREeg77kCfvH8Aps
5pvoXX3/c2jZpKf6UeIWKtJRCc+uvfc+c+OMO2qapHfhrA9ygdMECmJqMuxlsJB612TyeBgdvY/E
cJ/Yr8dazjUPhgGok0GGvPqN5s45baroYXuwr0pWDoccSgvIcrNsIg3HH54DrpRVRSgyOPaUmBW5
r2nWLCEMSvDX+ftHIGOF5NAlm8n5kvl9bRbTSBAN4R0GLhfAE3W4hu6fvf0cFUGHt8vV4o9i7UzN
0ZY1oJkh1FLS4wL4Q3vQ3qF+Iif4l+iq2dciz3cfT7DeOiSe7+GjeSfmrG5VR4jHwZKKMnAtYbfF
dLjo3PmGeVIeh/+mCVXy347MmA4QCQ/jTsLnw1YlcXRhgTjlmUEaBihinvlT+tgtR40CmIGw1/uC
LDGI6y/pn5Q8lNtaieisSiRuJtzXnW2rsfIRrg+KiYSGEIp/VnK7I2Oe/WBitdoac5yez5ckyW5r
wEhpHmoOflVICMTFGVA3u5BF/yd+x74vzMHRoGcN4Ibl/hH8pOjCoovDT0WK/0yNgIimaGdDXbg/
0dZeyfr4EAdBGTqhES1P2qweET7mr1yHBJmXgxcSetrwW2JcjSPwGWsu/GHOxCXlz7MQzd972Peb
ZtXsJXclItahjrRRAhCewtzPovdSyrD2crgzjCfzR+xPRsAfVe8OzWu4FpKM/Q2xEzvSAcosrs3i
QampQJJs3YOCOxnwB1VEKMyw9ty7FKrmHeYUcOqh5PV5RIi7/PEaYMOeJo5BTRxcjxeo0wxxNCFa
uLXSqbvwUKwMt370eeoaqDg/lN80WJiFG3pQhVMsrZGRd/VtDo8fEysPHLy+JOdCBaT3xOHvHtYO
91HrPG+0VJAGP0kFJzP53LwsdNfORtM+ZKwNHV4meeyiXwBugDrPDLSYqyuboD096uZ0O8ZWMxB4
6dsUXymaPsaPemlVqB9+Y+a+vj8PDcqDy1XEfkFok83V/Bq5gAhzrCqUBeLAtsvtVewVv/yAY+oA
Vlc6Z+w8y79Q3xt1nsH6g/5bG4DRqfyLX3h9XKphrn4ymWl/K3r9ptvTqy8hk/5jiDuliKbl0Hkl
YESKiwQ5e8b3KzbjX8nx4c/DkzsuJqX/69anBnRgHTZoDql7vqdvvOfnq2QssntjNM1xJzLvTefi
r3LCs813laEfQmj3pK1O7LHDwPhqMtamFtAgSSVUzECyW1Lp4l6CpZoxxMLeMXuV4uQwPSQZEHJf
08fseXGBd5hlCyO3JbQqxhO1Qcj/nsjW9sd/Hn5MVTXUDMvAOmh1ZgsmUbIjXlCc3GSf8SEQvQAw
TNKgqF3lMu15WN84ePgwpBrM34xrDGxoZcneFuPN+7cKbzGnqbsOCSqEK43daNpCA7Bqg8BeeG5L
5G/5xPHEkQvq4uXOk4viG70aNW/x3KrdjrzhR7dj40Hk+iizpiceurnDNPukwkzRo88/SMSKl36k
cw0BsNRB/n9Gboe87m/oN2ZulCYoXNaacZCs9zmjorVEFNgQlmVYDOoenxbmgad9CjPFKGaMmj8R
PMRhFhiJQyY3CP+S1hrVJdz2Pl4c04WwMkQ2V4UmMrY7FVgOSJyxgFvKzLDHuoKCXZK3sYITUv94
ZFHV6pe6pqKhuvkFzSyaH3Zcp6dnr3IBpaArTxgba4cv+lWsI85cYRZTJ91k4/qOb1OAvUz5DGy8
oOEvrgtb1GbfWAgOqhsLUyfJrxZtsArm7lspWLZH55ny8cosf6OOocCUP98rdh04QoVq1XSYsc4b
dPFFae2rlDtUc81vYF58j+40u6EXyXp9as44Sd007qYPj5MHUUMzZLmNl9fxMfydPZM3vrv5+gxy
NdR7GK45TRfjnTQ1fNIyPHrBNdGCRGKyCRW3kAAMouXu/AH5N4JiaJo264Wkqv/94K4z924lS9vH
lNJrCsALg4I0xdOc2d8acDBTDFpLYIDjFMDYvrv530BsP2dsSjzlAFuFv05s0UahUA/+izMFAJDY
2thbvnGdvES2/Q6I57l24I4SW2+JNMBko5oQ3KP3JzGRsiTzdtREsZIXLrej6Fba+RZheZhkZqZu
Hj09sCilAmiUguAWnxSrgdowJ96K8YPhZZriwHoLL5/kbvPDbuIsCWQcV3vLadExvEMlBDJ9x/5n
f5fa/e/dEmrX3C4bvDnTUwVpRs2g7zB0GWzHG1GdErfpoNw7MTN6rgyXfJKy1R44kmep+ohZlIid
BgWCGLnNhcg8dp7SYeRwWlcYl7ye8HeoVVixsv6EzA3KIarWpLC89D8JyS3CGyhzHU6tNS5e0IBi
c+Ct1ugWenmiqa1onSCeQc9nOWayiVKd6FAU0Y6qLUawMqnrPQX39lQNfiFPBqhG20TIp0TIo77j
Ryz7ffvcuiBukCTKb8gZCIwJ+iOokaUczeTQ3k/YiB61ub7SBtV7R2SIqnbOBSYgaZM5yCc7gtE9
aexSlj+yG5UbDwXOZZQeIJflTOoAeMb+Qk2wTBxkcsjttgTi2Lnt3nCrQxmrrpN5UCkT3CBVFDr0
cNsg/AMgaatYEkn7wqPA8EPN6ric5iELhKD4kQalFbXBOAtmp0PIKk1uIhgUpsa+SbK5IYpna7vk
fZkihF00UgXjV+LJVRt3aiuFNX6m4oTO7H0ua5gQ0a5yTszMCWnbfjyokALr5ksFsCJ5GCWFZOuF
N9AwxMV7NM9KT7SJNhI0Jf++uH/FyKDqN43IeAGba7vxsBLVWVu9HshnpyMVzBvbWee82Vijtbi+
BqkOC+wuDpBOLmeqwqsVBKUUQdTm2CnG5ARUyFPpYjxB7dIgPAv0vShL7TzwcKyilPCwuAwZzE1O
I7tNsG9ilAJJiR6KVzbP1vNtRYAHYDVBFUXEIbxXmyoin6p1YV7LOg36/SV2wPej0+8znT/7fUCD
8D1s/NZQuvjfGl1Fpm/5wPOs+YLAVMz7kMKFgqU5M8dKkZ8FEYotvNBdQGmM/jB053UrqyrMk6AV
xyn40KGuK7zP9S95QZNxGk0MuKs+G0/JlvZ6i5qqQXwxx0KZ0ycxh0uhPDPCoj02Mg0cVtzkG3zL
P4zMRot07TybJdbyjY8PNmMuQh4F3PNYl3+NEVb1C9lBV3WnGBquPCf6tStTwjdkzJ1f+1r38LEh
9UYX7QHZsK45drwYeoMrXi3y9a7gkRrkeuUQPcs9GWgcojf+U0IzLF0aHnJC9YQotrYTXfbDDqA/
17OW8iThTe3vkYUh+kJeM1SOrrJ4zuxYZ9b/GE5rhGHZxJNR6lz1liSphTZKQZSqPBsp3FhXTel9
Bg9sRf03txkQFhqj+zedENS1WzZK/LeqL5YN8cdmy0+aHaI+ExK9ZVyvRauX685n3D1qol0Em/LG
J5TSe40ANSm3f78OPAFBhPY1Z0lS5l/fBS4mgnxWCXID3lBafLKnVu2Guo0kbMcdCJOMxrcKobLK
wOSMdENEFXJbD9Xq9uNmA2ESnehBeidVD2UhqaM6Dy7Ao/Pyi6nRLByfSJ+H9xAfmCun+shc6T62
AGFfK/k/fPWeeX2mW+8XsXpByFbNWUaIU9li0VV4IEQKST0nFN09V1qjALrTNxmKjBYkup36ns57
XuM94vr4R4EVceJpiWWYfP5xiFRZ0/0+/dfG2U6n/x6C27jHHI/P+c9G45JMPSaltOja8/RCOccZ
AkcoPAtxYug0h8bzkYnpR2CWxMcjwmzZH0Cg3CRHJJWMJOpjsuBq9oiOBHCfPLgDr/GvJ8Uvs8Gz
oX6G9ulWl2/4Cp+IzSraoB1m5GaDeHOpg7PqggZ6VIhUEbEoS+YUego2GlSHeL6o1Yua4AIQs2lv
X66P1fy9qP+1saU/Q2sYheCt59TI5R5AErMF2wfTCYBt8GqWFkzHpSzgjpnWNLvtGa06EP5Wxtfu
W3vzdOfYJ3oU58urtvM7d3vwzgqFnKQaNH9cUe/8wjxQHZdmcODPHrlhjkKALumtcMljHrcu8ISB
y1TMG1XAPGe202ZiCZ+NcoVTkAz8ykZCj6tGVjiao7/3jFW9Xi/i9YCaXOilMPLjQehegFKkeNuD
ayA/UWLJvT9Tc3qz7fgjm3KthwllYiZJmV7SUB9t7wBTh0zrA3MY7KbP/ss6KXBuddrmkUvN4qFX
uyfbluiDAa777AMnkUddOMsYr0AHPyN1qq3MLHpp7a4TIhtc6P7sR0a6AF0eyIAM2mmqZl5kb107
wtrDefzRWaRN3inq/nUsrmPtwLo60xaqT7qbtMUStfyqYoKYHJh3GC9XSRS4coT6TyNMZx3MJkJI
WBnY3s6gVz01ngENq59roaQcbGCvGe1mz6Q2jRgkXpeJwrL7SyAiQNmbr5BYil4dgtVEtcnxF5Vv
vtbMWiI8FTTnL+tdzdJcdgJvH+ESt/ZWKmHqXNW2PBKdSWeLz1e+6Sxra0SBF+oA4oL5cJYTmjq/
TwIdB04Nq5hU7jaGZLi7jPCoq2xrJXGSxdsMjP/HkoPAv67Sg+yA1U+HBVN1ftBIm6H523oRXNDX
27xf1PotJZfr83QYd8EGiFEFvfNx6Ne2KX4zuFTAuVSe75RmSJCvzMz5YFtk+nDT85ZBbJjrvzeJ
nvzJhPYhmOxvG5C585BIZoZQa0X1tJWxWgqgNmItVgvO7RdltMkWn7Dp6QV0B9I2wSS1qUFA4iUk
OKPMj3EBZtJRv5Y14xDgdJg9W2rGG4Yz6cxX6/dokv0p3OHQaCpO6VM/5hP2rzR8X3Y9wk6yYyUp
b8BAPOkrRiOXcUnkhDcJ7G7Q9xWGeheheE+ljLNa0O68S7rpBC7DhM3vJ7WjEks346P4u06eKOvJ
pZVcsKXFWOQKq5IMv/RE7YvHqk3qVPbWhaw1VSa76oWJtcRiAdibWnGk5fbzxq0nDaxdRSrin0hi
lbcEXVcgqg1b3k3rj13F4ZvuAtcsvCy2oBmDOrIlRGLMh9BmTiNSEaY61ptB2Sf+JwNs5CiQaKHk
zu/wiSWQO4PF8XsTzVPay37Qu6C+8woUBtxivz+3Lh/2pneO8IdjPovDqb49bbVqMtxIaiQ2Hmv1
fDvIe1eGjbogAZFvZnFZanjwQbuaIUKVaYhuICrseDTnpole9SHotOSGZ4DIVps3Ifp5Az94w0Xe
iNLcyjp3jkOQQ2AUHmSuXd3iHJCDIjY5TEa8gZAlxW5ybLawQvm5B35csiTPFSui+uRKdv1bACbY
Rlp7JFcBl7HdFFkDLc1MqwS/uQyzcUEFAVoZI4/GxSE1KLWVxSZXfHorWR2Aao8oJJPxk8xoH9ds
BF0PDSAEZ/g6dfAtnl8Zjn9YZm33d/oy0Y+RTMdIGUaj/Kyt1t7wlpFaPwKttGhRNWDkYW39q3Ku
vszT6O1qqfeGTUC/UXG0AI55vFxhWP/XEyXpHI03JG7zFf7q8WMAbvZS0TYbYjSewI72oezLTI8B
E1nKvfumPCXW0xiNXdeIaioa2eUzumy/uRePhqG1/Iirp07VsJKt/SmJByfcXXJHOwIOK95zS1K+
DuXjXlDSUFnxhjIpIlPR/DaNKpXR3SUfwZ1oz0pPlNlhEY+hkX8LWL8Lb/Ut4Yuuw8aHnNraIQ/v
C9DEfFNoCYfr7VJVI1v/T4Gj2EcaExtkjGzhfoT1vGp5LPkrvLVTWUIWHR3qY5rX/0iTaWrY4Nn8
xv1lPlTRSj9ySkZb5zU67f3BYZfeHkbCofTJe8HYbx6eFmPkPSDF/PVvD7jNU0Na3agMf1bf9vA+
fWbeM8gFbqj0LrETi+9J3Fa1hUVVdUovhUteBCltTsH9QeqQD/9KDfepmZeAmA8sue+gncKbV5Mq
TrgXVxXhzXth6RiPUo5aQd9CVFeF+7uNBPcVecOwjQ0IVMXEWHpxjsP+QQoaRFiWmsC4Rc8vGZvn
rBynpFBiXQVN4ThyKN0LTqNU2UFcrPBmfWnu0tE+HYCbbOcPSpHVn8b4mNLW3yzq82TeOD97kKgE
K1HzqVkI9Y/GHXDg+pIJopQl5WU+J561/IlGGZ4SBTe9jvz1xOoajsK4tHWCsq0JospRNdGH2zqc
Bf5H1OGk2mrNFPHAh1xgx9SR0R/JtBADK0MTcMisnaie1+xuA+Xec7dP8tISLW5TyjSJu3Q6gjNC
YEMRq+4l5Coc6/b5QOMb6EyPtAtFhhJKbOqxKPkfPTjZVLq+npfkPVtHPww9vdtNxOqJgmw7u64w
Uoo5eY9dE5J7Rq5t6Svor+74xsnXNOdC9LDGU8xLUmrFn4sJAqBdYNuvS0W9fZG+/bMm76scZUGL
Kod16kenNL2xM0nk1Nwp5IDG0OMjklLE+oAgsX727+FE3xFefy+eklIYGvY+GyW9JNIEr3luZuIp
sNaNvgc2L1HHwDRHya9LIYyq6qRib3a0b70XIemvefDvSGwy59FcsePpGlccqyj4/bZ3JOuQifpD
hAS/Np+hBe1M6En1Iyd49TvOor/rcYZ4WvZYcxw1XSPWGvOqHuDTzgnUDBT+6RpjnVbKElaDdwSS
fQFOFk5p8zc6WAB3BAceaEjTPGmo86M8h/pSknMMozr0oWsuSUs//6VKI2wS16CENl1QuopBqXB4
MJ7ZXNwpI1LC3CWpAbqdIZPEbUzlNnKXogOlMOCplT333h+a3QgvBjk8tDTvln3j4Uq+iLjRuIYb
KyYu/ZIt7fVSR9m7FBkoNeXcssOok2OElYYMSeiPJ8/jWDE6m0EW/Ph9TQIPJRyLboCJJqr3icwf
r+fO1LEkxjiv6n4ILACy3XzPXU30qFGmyCXWWvqmgabkmw5Yz456OEpVLHFk1s8XE6FPLaS7CEiz
kXD0nM6LjMrvd5gFl5Ff3afXM8fXc8Jd+Q3tI3Deq65FcseQhx71XTDPa0U6hlZSC1CuU0g5+QT9
XONkX90IqAH+0TmWGUQKfYcapNahwRk05hETD9bdAb9fbMwJSld5bJcwfxJrkipndn9lRGeJtxif
dEOUtyoTbH56GjDjQ+P4Zw77/uJyfbU2JYqxX6s4V8LiNwFaQ/htBRpC+tCV/qPnFd//jI6dxvzw
YTCZt9s9RawD5dJzIr8IOM65GV0fmcttBwgFjSYhzf3oYL4oFOj2SaR3CMP/hIhrU2GYQQC4DfK1
lSSIvRYQffgrwFL++n0K0BbVyEfBSKcWYEIYq+Svn5rK/JPg5gatuUhG/T14cvG8ilB4SCHOyHid
UDNq76pFZ/1Xwp2WdAZOPL133UrFJlyYWNALwOaxPvrT1EOlXbDF1teSj4dJfspyNvtZVGdOTdsO
/4vFsnA7lUUIEpsMyC8EZlhkHpw/3xeSXffl8EZ90ofmxG3pgovmWaEE+++BP7hq0uvbJWV6kUX6
kWNfV6fZTHqtriXZulmDqtynAP0nrnZsXv7poF0GPGGmiRMeqwfneIkhOg9pkJLN7sxuM0GYULvD
435VT8F2dWgSELQoqFS4ZPrzWKeEqe1IcxPvWZpb/aHBPbs9PXQ51QFtfmGlbrTNguL8FCjiUU14
aRfBxfEUSIMJJrflLzZc+PHXS2DI6/hunzOQIZh+Sfy13vO+5fd0qem3anSGFGHEiEmlqQg++onA
8dmJV/hKiUF/6Zdz0Vgv50PULzq49U1Jk6TucsVIehfO9Xz9V1nT/70tYEc8R588i4d/irQVymKL
9EhvnJKZRfhGZZclj3i26gRmCCxc9VVbfB9V7+0kUvz1z4UCFH8CAPIDSr5qCc9TcDQBu6mHSO94
uevzWTkoKYQo8nw5RO9PPlyHmYSvQgcNP3JOvOPz6XV/QyTJKTOkThe76dgqxaoONxJyQ1taBCzQ
Q5DbfIHgNL8mujCqWz1cMVRs7KRF8M7fIR9ZTJ/ecY5EbrsL+UdWYLb/yh+Kw0WzDNjA8gptdgYY
Sry5et5jEWqLoMQ4QldBOMqXlKPtck2vyfYeof3nmdE5L2cRZV3kB0I/artpM2Vsr73DoOA4594r
Q1LMcwlGV+y2z0LAKgm9Pbd14onTYqDPJm4hwv2e7KFx/gsEZh5fJYDhMBrqKLbbHfalCVqXH//A
b+4CWsMTtu8z4cTmd916NChda3tjzzje41x4zC2D1+MDBQJiFbsRpRXPngDw3RQBdlo2iEeZHlhJ
4rfVR46gQrk9n3VtUiUEuMnWaSZ9AAVOCwtWT8ys9SQAA66hAK8VVQTN+GofZ/VOjtRC/QxHR5n0
7oIXidXc3w0oN8WsNrCCfuJjR6/uF74K6dSqUl6MkSRKXGDnheRWqsGqMh0ydgGe/Y2TfWtfFtSh
0Lt8rYkhTmBxhEBkfqpZb/jUHghb2fVfMEL+CrBSd2u5fwNFIYVWmLQl2B59ki8oiNzFP/RPPMUy
H+tON+3ID4MNVCFPMhL/9hD+vV9bn0qHLwzE3qfSWx8YLTfmT9E2FlHFAssrvXHaw3xEmKSjKsel
moc9nWwRYWKNsQhXtIDhzi/1jarQm4cE0kiooo0ucU2AbtgMH5ZJNn3ZbYOrPWhvn/cFn2gpsv0o
lMoLMQPmjiRzmC8D5cKM42vINfHgYwak1cUI2ze/tAAwDmBZhJSaL6ojQr0AKIyTyJucXN4elmqt
+4ODQR2q2iet0OWPkyjrRrYZ+N++Tk/og1kKZkMZF4KmFhcTR76pqzW5AW6gX8uLMK3HwjjkdG4R
GNa4ivyE4TVNBll7QxzbqxLVFZWNeN2O/xxLsgWPlTwW2jcGwIGeMxhtH+HGVyV62Q/6oezvMalR
pNvrABSpLog8PmJElYpsLucY4x5JOoLpgoWIKo2cRZRlkB88Ph3AbXOzA2tsyXHTIn3d0lRlGXaa
IvJwyr39zO9Q4GB3/xBwqyvmmqLGFpnLp793UKjjrYchxjT5hKUjojJCJCEiLgZPfPmJXM5kmbFH
/5BnNzxe3uH2aYEOk35tZmokhbplZk22jRnt24Nw7PbORhYd2ZClv6sqwy5+CUxutYOmSjUTR9+9
N48H81DGAjY8EES1cMucUTBFZwAhPL24TNgnmqxQSEBrL54r/ElPNS0JfO8mOEkmMBCmZpHc2qSi
WjUY4+lZkePLzI1mtLboGSd3PEoRAX0JMWyJu11Drs6bOm0tHSXAi2vMZukQEgfjopk4YdxXJ5F8
i7Jeawo6rLWsw6SqEzgb5EPzDX3PzDk4hbvtquiWzsCMT/qiQsQKmrsSd5tgYepl0l7JFnqiooyi
a/q3p9ynbvLM1bK1DLjeMaqF22VemQdB5FTxLiOoLH7xVy3uWDypuhREsLXRxZ/8WpPKmmBhzhEd
JUdq+1Kuy29rH+jPNxpasWu44bRYNsU88ICXzpLxNqf70qUkGSHeUMGofKjOybFB2fcxykTDWXnH
fDMlXjkdIaEdiVJ7BYAA/Zs2Dfw1Jhh9GN9j2hxV9utZOK4XkvEu6Sw4zWKs/XQV4jGxyfO8fm1r
a5Qoy1ZXw94pabedEbH+NguWqdBl44+3vRHlAL0pmtW5ONbBNEwMVumoKqXPnn4V15LTL6fFKJx5
r69fXEh/0iYPbI9K43DD+HE0N3Hd9EgbHLq3IpxSH58wExi34x14lW8Ont/33nAm+aerjxc9bGtb
SOTDgzY/ytu+U7POMTrzS/ZYhVktdb4L9fj2gccjCCJemG/qqVdc2BADB6C9pTMSrrERft1YqgrI
7x5u8ntm41HO3yq4ZKEz3p21r1Sdrd5HJOzJnzSI8n0AxCgrcLJ2YJXo9dXGkQiJJaLhq8Ga0s9t
FJ7TeTd4H8YBZjV/Ezzm7byDoWZmrOSKBhf1cFQadw4LNO7YgyOqLUllu2FSyIgUQmVpu+bwhUmS
crzl4LHe/HDH5eb1JipPRlhcpihhLgEMyVJPljST/d8cWphJMYMfhF88puK5WvFumD25EXqZBXk0
v2zKBR8T7dlla7HTd/Xf9FboFxN4bBaPv+KhMXP/QB/D/fOfoqDuuPsR9nkW2F8YJZcT/rdgCXYf
WPY4HAEuzxAPIGLcLAu+pwen9z0xco7C+h4cVsijbuK9DKQswX4nZcqPKeOW03ZAN1v80W1AQkVY
PRBJ/QW0s8v9Itv28msfb16LPzZ5G6ckrbzUl+e7MOIpBBcFS4AdW6i0abO71/hPeRJT2ar1q14L
075A47yVM+V/JId/6xlbE/if+y/WxzFgJyzYKLJkpiXlv+Jf+yM/OFOTLezGRHDsrbD4ACBdnTpO
UICh9swPdi+OaDs1T0BD32b3Q/vKXa8kJBvXtVz5yWgV0T4bBmmUIKpWvGvdn+Rm6ov5Rom5tBHd
RR2ogWHTqa9rOC4CdgC8FfjfWUWAM6d64GOOotFwFnZISid21VpP/hZYHguCX6Oi2rvm/PoPTKc5
ogW6icsXSsbjpFES6/oN7VVGRPQY2OFhfQ4wSknyLFuju0ZHkQCC8hgqeHwqm+YRQ/OnR1mv4XNv
3RkVSi2yQr/ICVeQRjzEtyZCMPEInZElm9JAcP79kFcrfavkpSdEAaGTuy9r2mh1k1vKPwYPCRdb
HtUPSkKukG7t500XrcgBvNcwnuOheuwSzhB0q58RbVTWiqyZgLS09oKXV0CmiutvfEzcpNTMkZmH
1L4yLtaR82G4YpSiipo/law/jR5bu9Ws46QDbv9X+/X+W+OLxSqp6iQ1mtdfAxZ7zYMARIxSLNDJ
qj3lwSvye1qSIoIxvWv4vPPJt5wG6jT2ZHK89jeG6P8qjOJNvUr8GzRCVsVd2aXXwTepW9rF3Ilh
Hvacksz4mLHPDwp5GA2yd9fNtx+1i3W3+N8s4NiQwItIgONRjzdeV9b5BB5u9ALNteoA6ubZzQ38
vy5AvTCTqvFNLhMOxtc+SnAQGsvyZlrtIMxBX5YpvrdTVP9W7fuGMmIsyylyoq/rnsN6sMzOjNXi
kAKcLiodWzRaqNLY61/0/bqkEd42zlBxo9gSrZEs+juXrzklEU2maLGmFNFxjqCQG2sftOWqqE2C
eYrNQI21l1lzFjUAjWahufTGj8a1hF68zDCI3nw9t/6635t/NA1Lw/ziVxqGoc8bIsAYvWU0V3Wb
3FvjQrpdATnuI5WkZvCHolXgWeQbDdPMefgs7TS9nUpBBW7sY5r/3xyMqdGGT8cxDsArhCYNY3SN
tqwbCrabh0ei0gdxgrg7vZ77xj/Eyh/TREgx99XWTFCegyQUVBbF23+14wZvdJVxCLScnaTM7E1j
VSj6ypOeRUimsmLLCIpDenR4bMwFhV4f2lasBxkMIOpWxnPC1xH/alxSUKDPAKY0oAFe5z/Zjl69
/Z3KOGPa+JYxBW/NqyRxWDWN2nH14goihZLkljGxgGJQwvvZjFruOUQgzA5HxSaxF5gMZEcJ5ZTX
rZDSnHmfNtbhnAdxSBT47ruKMwl8bd4a4DovOXymxfPrcfiJgFRCR70cCEAx4EnoXlTq6vzW0tln
GRQiC9E8qFrfM7uwj4FJTEdEwvsS0ifPn9j8b3EdTT7a3TQZnBKWpuk9lEzwv0UynQQOgCs9qAXg
kSZd2o465rvSkrPxJ7bMyJUyUgyF7J9m8XooSXFVxExP7eOxNK5sHVnadTRIN7Os/a3Vb2n6iIs5
M/tYK9swDUgBW8qFvF/9dZ2TdkBARbbhgJr2MfUrRlz++jM4SQesDChcKyCCvjZSsl44m2ojhPWo
Q48SmVTsPjp372N9qcNPSkhwvgDIFLgb7G5OyVw/Wwob84gqeoL9VlKCv3YPPPsl7M8Aae1+RqCz
dW7U1BBrZ2+TeroIHb/FXxw2d2jDo7svoTekWDxvbZA8Y/ezlkrf91UrVmpkGWadBh/90ea8a3BN
obcZ//Lv+Sl3nRAw/+pxoy4uoch1a5nGM7uLoJVE4iineb1+XS17vRVOJ+SyUrPmT2rfuZOEZdSH
acYjArrUV6WHu1E/dq4O7uaUYWFqVbddoW1YalOw4Qmnf8IR5v6TjhpJJ6csF4iX8uDxC9NLXoVT
Brtssj6/hA2EJR2Kyxz29kKoLQUUDfgvBofEeljVrNt4TsmlvXZMUX/zOBv0I6d7/y7WqYbPy6Cx
JGc8T93ouMH7M934NLvaCDq03mloi/4kURxsWTfZq9ZWgPK6O8I2cXoiNR6pp1cw6FRYyQSx0Jm7
pzYsvpCNjYk3s8OgeMAHsEFdbbAGYNV+728Wzdzj3pTJbMX+vBwTDSSlIgE6+Ep1DiHrG9mST0n5
zAWjbVw/P9pycMBCpNWgTnAy/Ee8j0C6KDybyBnM635ecNYFJkWlf9j7s4dLd24UGa767O+ItJoT
4bBvOU1spPGpAi1JzNmYpKmGOCHTItygkzjXsFrfVifWH3K30aHYONrT7z9PI2ngDolx84qku5Hv
6r7uSYu64L24a/a0l+UT83WBEgkLdEP2WeiiDXC25gcP6mUZLRHy6Kpc8Evckq7nrzDqjLjwZ0zI
sjQpdGZCD0HqLp7lxOM2hWQbsJSe1jSxzUzZRAQaPp2UhAfDN+TU/cSoqoFqYFfl2M1x1TlYBsTB
Z4AqH9r5LCGK3bfb0OXKj28FsKJXxG262hCDCtl/s+kqZRjOI8IYgw2b7/iVdbgYlrZX2Z5SYkN/
+KDYLsQxvdiaVm0SpARY1tcRq19n7UVsJ0yy4WdTIuyT1e+eZK9CRvOXHhk/U7HPA2wVE1xrv3gs
nhbD3mq3WNfGv8WVlHZC9gzEBsnwuobXM/FCJL0kcH6Ofu6cAre37CJVLrvbKf1tukvkcdzonkPZ
Bb+Jksy5ko8gxiLGjPxweU5AMsUEfet8t9OI3/mKPeGXxCcuPHvARk4GkHN7Qgmawxf/JC3+JJnH
VFuMMckrl+FodYWPbog1j4tdEmHRNf2Yvr3yfjfKqxmLVt1MzvyDa5pdZujHWLVwSV0Md7RvmIgx
2GwR5gKLcO7e2+LmTL7MwNOVspJnEyhpGoPhcjt8I3vkTblhCWxlpl7k6xwHQs2Io29hL5nTHZh9
Aagx3QfRKkhs8UpbfQRzdSnMbqrq1XoVny6guODnZuEXVqKoCb4no8Ck8LDKKei3Oq1Riljca02h
Bcnus4+JrRawmTxIHA/OVKRy2EZnTxZILdKwqlPz50zFaQ0qjcuK3Pd2Dvl6smVe7pmNEJqmb+pq
T/sAIjdcHjqXQhljIdFb6cFDGhXeJPWqTNWIi/d8Mm5jH9wugrhzD6uw33TrVbqelQUKYSxRNSPr
EzDRYYdwUmAIQww5JLrL5RA8yOs2YNiETxtFMDCibmHmZpifysFqC03X2iil4kvN2K6iatfgFZR5
734SMUS2mEsvvYtuAvyZUN/ZGDiAHAowwig+IO9bvXkceU/u/eJf+y38pHkjHRwpBeWvtv64z+Ay
cGsZqGKGZNRwWZNGCW+pRY+BlU4rD24cFsut+cPeTzbEt93Bw4fSUYHSd4EDYj5ZrGlwqGAwvbZN
xuziSaBAM71nMvYXBC1zGiePDyMuBLWrk08syxFqXxWMa65lQo/Ueol2WHo2g93Di4Y/Cw8D30wv
HVirXebzkRf68IAqrg4srnDAVf3U0y4/G59PeK8kFUfbnIJUXtrRZn50kJEl6RGXbbEOQPprz00O
aGyA8/sj32HC1ahEWP+n2RRm7M55V2a1uX0HHu9znk9qIwymeHiCPFuNGMn/dpkg2B0R5em6jc47
e2eZLoWG/NQOiwHaokhCD6nfO9Z8NZgqgJ5/+zGWvQ9GMKAnaKwvBydCpIEFbL/ZcqM6nS1x7d1T
EkScCiAHyrYgAnmrR9z4sg8Zr/WewVwtbJibpmQz8AO+FMueJU4FfK8jGoaRKfLE1Dc5VBqr17dS
9Z+7i337E0MgM+a4WtIdtHfYSNW9zf6j2cLYnPsaB1D3lQuMuWUzS7QrxYi13N2gSLICdOkXZ58r
7m0tnkAEnRDhkhHzHOzCTpGPqXVJab/G8ZxkKiC2qyh+14+QYk5cKymt9sWKngAqoNuV9CeshGTQ
+NuSf3WLIk3yNg6AkNE3gBIEGTpM0CdLIQ1kyYawoGgXJjEALxI7lFZF25axUy4C7aPI4tF6uPTN
xAJHn19Xb3zwO0Fzb2yyv+7w0znXKnBklnHdaymGxsCf8cavaB4DBC0CGUnbV+RNId4nginNzqQv
2tlPOtB5Wy5uUz38N7frfWxefW4bvl63L+FuRvMnkNp4Flc/n8SBetxqxwNBTe5TemzbYMgl+0Q3
tv5n542SGf+w4oM7QicMBaF+3uHwiLiv9P+1TVVosUdLzQKXZI2OOBecKIA9WhOzRGUCzS+QGYEF
FP4V9oJ3wJMXA3+VRrrxsVx9vJJ0e0ICo2GgP6VHGAO4NwK9IymEdKLi+EJPdYLgaiCoEFkbR+qt
+HT8XqM2VAFiHpkJ7t7apHfl3ScVdhLJ4H8DU9bEN6WWD3AsiQRNmcNDh/B/KuviouD7jTfdl6/k
P48Aq22iLfGbhE7X+jpL7jilsd3o706dl1AfGhP9fr1aaQjq5RHe0IuGCxox2jy4Y1LwRurh+Gqm
PJ+fFOukkkzcdxt3q+3sfUm/l3cx/qCy0bh5VKVx0f7r3u2XNyJb/czk7BO1INhCJCUfEQa4IsCt
uBaYHE3pTL569mEimAvDmxCdY9SlwbFpdLkToDj+CTn8K7dQuDpdDWqq1/nO3Ygvot7FKfiIOaaw
QSCbSlrTvCsIW3OhNpOz2trlaUzThTk0ERDlv2GTmSk4js2t+t3uXanSF42n8y9KBHeRsawE34jx
BO4+f5wnXIWmrCtyDh9EA479+oQ+8XjakdgZJqOq7ekNFu2BGjrmDDWZte8A12MLI7H+SOU1rPCW
YdiX71SZbEPyro/rw2komqC2Qj0jrK2KXjdLquBp8l9fK4KYbb/wpvs0lzj+uDimquPs0O+z4mkX
LgB4a2kwYDxnuAm5RMnR8WL9V+WfnTB3ktgb1irMF57WUxfSOjYBvioEYGbyhAxE1W34wbJNuM83
iKGCklYC/kLPk3FWJy/T9Y2Rc5NLgTmUlIRTvnu05hi25/n8Ja5ckkfvLrkFJrwf+bcYC4qpS6Pi
LqOWuHfdW3rCHn7IbNjQJrdwbTEy2dX7gq87oOhvLbJMeUPihoDX6KXC/BKfo1dslDk/0xhfvwde
Yu1WAM56MZ3QUJ7c9+P0fEYF4m/ZtL1N1CnAuSJtyNgdNq1BEIBZD60b741TXhwNDlhCCBwAyVlG
DfwV+olKUIw30z5ntNQkL5WqeOrWR9+4IVIe1TkiFVLv2dO6LgAQ7RT8vbLavjxl17243LZIuB0f
+qYQuXA8qvud5q0IKjPAcXPG0sasG70rV1r+ZK4ePXb4Tw6r8k8RzX3PzVyFI9JcOJshtD7UctrI
31qWIWk3DJhVO7oJVWD/f2WKu5Z8W54AFd6e/XWodVGgjDp8j+fCY0oQuTfZbzTWq+1iGkSmdEZ0
24CJZ2PSuJj2uvCx2a4N8v6Ke2PmTcsCQGmogMj1dy/RIg+MKyAHJP6fvcqaNrRQnesOZzMssz58
2dYJVCstYQaMT6kxh5S97Uu6WIBxCQNeW9nCWsIdhDFDYjI1U9J0UyAMiQsXIEpUc4/MGfteViXa
rVvB+kPjzg+k+TyfM6VkP4+LI8MIcJQAJ4RyP4k5KPOSBjQYjPtS/uVWHCoP1q6SnyH9FCe6x/9u
rfvP/V+sK6hBbOWRODWty+Ye7pkvibDgwOGRJu981pWN8tw7ipgy6KwvZzxJmAuPiWieUc+fJEzi
hry6YYKzoeJhCT6anv1/vznzxKTig2LKKMhSWafOrAfLwjYy6emFvvZ8El0hKRxog+6OWakDlmcW
G57XQ1ijn3wUlo4mx8Vhq+zC0Mac+DDAq7sMOgBpKMS9Tcp/j4jtsHZhafrP1ybzPWfLX+2gs7E1
tzmw599CqjAWcwyuhAnsI8IfuIbZe62yeNyAt1YbcltOLsQbuBrDTYURZlX1vQRMySIAYC3m2aLm
UiwtXWTrYPR+cEcul8n5zRh63HGeoRJfXWkWJX1XLVYIyXxqqHl1cn9aiDrRnPD1kGHQNG3ihuDQ
SzKd43AO59AVG7iWFgz+1WMLXQp6nGdNy0ZXUyuR/e4Um8KsOSzC7g81OR1PHdCHy546m6nScpbK
Txv8KPLUGcuNKnaq5JaRttRjB4sccXuPsVW3MiWLWcK9arQiGm4lWj+lYQZEXyWtYZ5eHgtZjdR2
aB+PtvM6yFIDb/4TI9T9PcGtzOHkZ+jUSVlrXvysMJ0zb/KNI/IVu7j5uT6gc3AXsTNskegrREey
QtFExKg905hAOlXhmUv7dm10p3I4jaHtk5j5Q+hQyOIKoawi5lglTJ1NPzW5uVyAu7PUDf0wgnS9
uueIPn3FEgxWVrEh+tESlUH5nbw8I7JAICESb2P7kwHmLo9LwSKiawb33y48WPI/kqhTbcUpxSQB
oGFGN0OJo9H0XKCl1UnEQD+cJLxtk+OPcU5lq0HjcdxBTAg+0lN3JhRpuj1J7QeZ/Kd0fW+RXMyq
Bg5gV+HiPReoz87lVUDurL2KuRHHVKvqpNMRH5C19l+Xgn/Mm58rB8VxyyyBgD/NfpZs5PW2xHcd
4YKQNOHj3R0X7fVaSIdx6NuXtpp8nlUNxLOXbA/L6dYn7YNsmbs01+Dv4MWYg0gNsVRMhgouUo1n
HXpowTVKV9yGL90Hg1Hgz/rzUtJWdsi3GdTyKt9K5OqH0txCw5nflR/HeqZo1qWB9cH/IWGiY/pW
8lE3aMWr/67U8TLvj9wVi5viudVHoo/bL5LaithH9gyZPkN1owgfi6Qd7tSXXm25UODvF3KAHT3T
zaZS46CCRH4wOccf0o1M3ApZ0MT6vhjSX85XM5BwlGkqAKEQ/Zb8Cj4pLy8J2LIrlVgU+FE93w5T
gaO+L0SO+LEpkYdeleqcodnSSAg9pyNudOT8VB9xDpG4J9hMK9Txp0xMa8bESUHEbJk5H3JQD3H5
aN56OWTTuPGFfLsFSlltSpYzb6kKlmPulUFc3BeXsz0+wWS23mhRUut3Ae43/iV19nkpSFyG/myr
iEKbrXix7bArj7iU4I2KS4pA0f2SGcNlM0367V6DiWl10wlWeawEg55LpvkPKUnVPi/Cuhulra4w
HSuMXuAcu+vST1rSeboE81CQpmfDPs3XPDmxOj06Q7fiACT8SLLCdzDfryn8JD824TjCd7UlsbKR
WoaZkMK+S9mN1aFh0C29GBbqAWDkMQY8bTDCxd96fSr861GOGcGFWuBeoqC7sWsceOxs82OMfBAM
UJU6ti5tYDfeAp2jIy+N13zl+qS+zK5dgZmb3wr0EhyDPiUW1e4wghXVBrKgF2Su8pGykTtrse6C
UKByVG0DgY46xWJdUVxk770wZDyzdaTZjKHyntUiiww0NV+3yEMLJi2wZfYFXH/qiCIxH19HdLyD
fuebKcOqfPNgKPcvQ6gaihdzb+pD1ilARh8Wn7e4rPBSifZD0QIzUNcUDG+Cf7qX4NA8oBmNdOkG
gua0XHLyaMOJ/r0lYDm5bT8wCIDflOw9fWZyLLvoe9ZvP9TQQETEXk6IuZlVawrkz53d3ueuGWbb
HpFFzY44dy6jc/Gn+Ai/9wofzgGDag8zw+H9RZuQtAZwpMhLeTZ0kwm7V3bX1gBUSaBCCJh5NRjZ
lHvq6nTY44NlXI9mCVr6iwYRQokUNPa/EtuDWMnD4xjsOkcW1zF5BzlKC2iTCGk+/ModbtregV5z
S5wW7/8VYuC889shL2g9iQZk150Hzjszu1L5FwO6u+WnKD4sz6FW6S7CK+MuILOuFwPnmQnf7lCT
drNO69/ay8SLaU3CyoLpPverJSogtNU3NbMcLzuZPihwovV8H0wUKArC31ftPp/xUN1T5l+WRsNK
SQxFOmMSDhZQTJNevCb2gIDjjiX0+O8qyCZdKw5OVAzv2tPOABg2Zzfnj+09wjveAMFHCEe4Qjpy
Ym+pCH5/vmE6Hpv2ygMJtz9yNdMr6FZK9Rov763pmFSsbD4oVKTtbjDVurccOlpGI34s1MpJ34PV
9ctW0UnCB4IExtuUV5F715ffVEFsZ+iad9OKBAeUZbUldeuDFvm11V1BUN9CdUWySgkX52YzgbKh
7/CLk54L19vYRc6JwwbonOUu3v1iDBZ8hbWCo/xOjG+IKJQJm82jVnqnPaKd2BIQwoRcSGklg6i7
xckBiXeHojchSX3p90QDTTuoVIPvZkFlviEAvreG55+gePFjZLjBGyBQjC4Xa0sxuF2lIaDBIbb8
XY5ndCdLu8ArTRH2GrE/AyGCcR2cMzJLeV5MjkkMQ2Ktu3VnCq4Ki0eX7br1XP3Riv7E6hy0cReb
VFqkxm9cC8zlDynTkkSNERQ+wr4z/OtAQazvoSUlQ1oaeqGGqEs/tan45+Ei+N0+lRzTffEEUwC6
mFCOVpjQVsXZGibAimNebUxPiJ72xNUH5kR1DaRPS6oNDhMSkYqjjMw59E+s3KJ3irM9rBUgz8/4
zGNXS7R0jP2fc9CxFNASLFzWOobuhxqOySiFrKBQeOrARjpD3vuKTgsSvxe9PJtx3/wchmHFIHQE
rBFmdhN9ttu7TK95cMXPqX+KuB54pYMfHBEaPDABNQhbSsW1RF+uf7lDrtnOSuN/5TwnqYZVfSUh
IoNc1BQvx5OY875XnHCzko1L1hQWPiWdrD5VxIYMSiu6OZJ9ZKUM5S4/Bj7v9nAiRGgrYCyi8lvm
KhAKTcs5GnXL5oXhjIdxNFf9T1tUuTolTo7lJ31T2GyF5aP5hKHLgTy6QYTXuIYV3TDxoZ89GWNC
h+9a/JhoWqCskCbIu6l/QnSQmBLmIQGWmW4ucvOj/BW1ezMgQRMjfFQ+voWTOEI2/0tX8Bid1q4m
Ux2p6dt7pvxHLh9ghUVBv5ywBDNFfpe2woTlq/PuES/PfZP7ZjLMfBy/CrxL5mj6l1Z3H5dRaAa+
5GJGfb+oxMxcRrln2bCCcdjf44KTVObotclNlo/kr3qdXXMXp8uDpBJdZJNilj7e9zL+YRBdYfkN
n1xM6Kyivf1YYoV7uxgdD8ROnKzp79cJB7NqV1XQPCMkDm2FYaZjwWTlVkz2y7aN5zNFYNyAeDFy
8j7fOMu/lggdY4TxQ8zXIBlphfdyVZA715D8UGeMLV0qJE6pAT4ZOGWGYQLv3Z9AijpoSTQrhTO/
zV3jHEkPG0hPdlD9oqyk2pncqC2FWWpZAE+rvPxa12LwovWrEEBH/YN21UHEPxmelsx/MzfGtQVt
8fu6pND9IE4a9zRGM47CFlBjcMfC2JuTUdLmZ9OixxQNbaP5MI0dODLcv6ZJ+/DU1M9TTrrEWJaB
hg7HYo3uu1ctKe5sNHYxPjoHYbfAjIV8x4sTQfxcIFW6JsRRQ2C7cUGvtFw3IxUfSNueQcpSzuLh
0gc0wFM7a3SF03VASh7gytBHCaTL9cfjXqoBbI2SShHCXxKlmEhTvdlMcCYZA6m9UFVXccLJJRbd
40iET8IG9QKlo8dZuhL9ovvjPVgf7a3cqrOVgzJnryzYUJfWUg8SSzeSpYBof0QrLYCoS1VK/gxg
tsiLYo3lcRAqdpA897Oxs2NlDqn/4fcR0bUUmL3DXUUP
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    powerdown : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reclock_txreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int
    );
reset_wtd_timer: entity work.gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => reset_sync5(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => reset_sync5(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => reset_sync5(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => reset_sync5(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => reset_sync5(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => reset_sync5(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => reset_sync5(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => reset_sync5(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => reset_sync5(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => reset_sync5(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => reset_sync5(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => reset_sync5(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => reset_sync5(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => reset_sync5(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => reset_sync5(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => reset_sync5(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => reset_sync5(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => reset_sync5(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => reset_sync5(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => reset_sync5(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => reset_sync5(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => reset_sync5(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => reset_sync5(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => reset_sync5(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => reset_sync5(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => reset_sync5(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => reset_sync5(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => reset_sync5(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => reset_sync5(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => reset_sync5(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => reset_sync5(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => reset_sync5(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => reset_sync5(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => reset_sync5(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => reset_sync5(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => reset_sync5(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => reset_sync5(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => reset_sync5(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => reset_sync5(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => reset_sync5(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => reset_sync5(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => reset_sync5(0)
    );
sync_block_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => SR(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => SR(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => SR(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => SR(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => SR(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => SR(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => SR(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => SR(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => SR(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => SR(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => SR(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => SR(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => SR(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => SR(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => SR(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => SR(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => SR(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => SR(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => SR(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => SR(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => SR(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => SR(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => SR(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => SR(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => SR(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => SR(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => SR(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => SR(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => SR(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => SR(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => SR(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => SR(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => SR(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => SR(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_9
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_tx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_rx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_support : entity is "yes";
end gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gig_ethernet_pcs_pma_0 : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0 : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of gig_ethernet_pcs_pma_0 : entity is "gig_ethernet_pcs_pma_v16_2_9,Vivado 2022.2";
end gig_ethernet_pcs_pma_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
