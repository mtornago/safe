library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.SAFE_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity VFE_Input is
  generic
  (
-- Number of channels
    Nb_of_Lines : natural := 5;    -- Normally, we have 5 data streams coming on SAFE board
-- Number of channels
    Nb_of_Bits  : natural := 5*32  -- And 32 bits per word in these streams (aligned with 40 MHz clock)
  );

  port (
    IO_reset            : in  std_logic;
    delay_reset         : in  std_logic;
    bitslip_ADC_number  : in  std_logic_vector(5 downto 1);
    byteslip_ADC_number : in  std_logic_vector(5 downto 1);
    delay_ADC_number    : in  std_logic_vector(5 downto 1);
    delay_tap_dir       : in  std_logic;

    delay_locked        : out std_logic;
    VFE_synchronized    : out std_logic;
    Link_idelay_sync    : out std_logic_vector(5 downto 1);
    Link_byte_sync      : out std_logic_vector(5 downto 1);
    Link_bit_sync       : out std_logic_vector(5 downto 1);
    Link_idelay_pos     : out idelay_pos_t(5 downto 1);
    Sync_duration       : out std_logic_vector(31 downto 0);
    Link_error_sync     : out std_logic_vector(5 downto 1);

    do_IDELAY_sync      : in  std_logic;
    ADC_cal_busy        : in  std_logic_vector(5 downto 1);
    eLink_active        : in  std_logic_vector(5 downto 1);
    DTU_auto_sync       : in  std_logic;
    ADC_MEM_mode        : in  std_logic;
    ADC_test_mode       : in  std_logic;
    DTU_SYnc_pattern    : in  std_logic_vector(31 downto 0);

    MMCM_locked       : in    std_logic;
    clk_delay_ctrl    : in    std_logic; -- 200 MHz clock for delay control
    clk_160           : in    std_logic; -- Capture clock to latch 8-bits words from streams
    clk_640           : in    std_logic; -- fast clock for bit capture of input streams (640 MHz DDR)

    ch1_in_P          : in    std_logic;
    ch1_in_N          : in    std_logic;
    ch2_in_P          : in    std_logic;
    ch2_in_N          : in    std_logic;
    ch3_in_P          : in    std_logic;
    ch3_in_N          : in    std_logic;
    ch4_in_P          : in    std_logic;
    ch4_in_N          : in    std_logic;
    ch5_in_P          : in    std_logic;
    ch5_in_N          : in    std_logic;
 
    ch1_captured_stream : out std_logic_vector(31 downto 0);
    ch2_captured_stream : out std_logic_vector(31 downto 0);
    ch3_captured_stream : out std_logic_vector(31 downto 0);
    ch4_captured_stream : out std_logic_vector(31 downto 0);
    ch5_captured_stream : out std_logic_vector(31 downto 0)
  );

end entity VFE_Input;

architecture rtl of VFE_Input is

  component selectio_eLink_8 is
  port (
    data_in_from_pins_p : in STD_LOGIC;
    data_in_from_pins_n : in STD_LOGIC;
    data_in_to_device   : out STD_LOGIC_VECTOR(7 downto 0);
    in_delay_reset      : in STD_LOGIC;                        -- Active high synchronous reset for input delay
    in_delay_data_ce    : in STD_LOGIC;                        -- Enable signal for delay
    in_delay_data_inc   : in STD_LOGIC;                        -- Delay increment (high), decrement (low) signal
    bitslip             : in STD_LOGIC;                        -- Bitslip module is enabled in NETWORKING mode. User should tie it to '0' if not needed
    clk_in              : in STD_LOGIC;                        -- Fast clock input from PLL/MMCM
    clk_div_in          : in STD_LOGIC;                        -- Slow clock input from PLL/MMCM
    io_reset            : in STD_LOGIC
  );
  end component selectio_eLink_8;

--  signal reg_add              : std_logic_vector(7 downto 0) := x"00"; 
--  signal reg_data_in          : std_logic_vector(7 downto 0) := x"00"; 
--  signal reg_data_out         : std_logic_vector(7 downto 0) := x"00"; 
  signal channels_in_N            : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal channels_in_P            : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal channels_in              : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal captured_stream          : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch1_captured_stream_part : std_logic_vector(7 downto 0);
  signal ch2_captured_stream_part : std_logic_vector(7 downto 0);
  signal ch3_captured_stream_part : std_logic_vector(7 downto 0);
  signal ch4_captured_stream_part : std_logic_vector(7 downto 0);
  signal ch5_captured_stream_part : std_logic_vector(7 downto 0);
  signal ch1_captured_stream_swap : std_logic_vector(7 downto 0);
  signal ch2_captured_stream_swap : std_logic_vector(7 downto 0);
  signal ch3_captured_stream_swap : std_logic_vector(7 downto 0);
  signal ch4_captured_stream_swap : std_logic_vector(7 downto 0);
  signal ch5_captured_stream_swap : std_logic_vector(7 downto 0);
  signal ch1_captured_stream_loc  : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch2_captured_stream_loc  : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch3_captured_stream_loc  : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch4_captured_stream_loc  : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch5_captured_stream_loc  : std_logic_vector(Nb_of_bits-1 downto 0);
  signal delay_locked_loc         : std_logic := '0';
  signal delay_data_CE_shift      : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal delay_data_CE            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal delay_data_CE_del1       : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal delay_data_CE_del2       : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_shift            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip                  : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_del1             : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_del2             : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip                 : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip_del1            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip_del2            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal cycle_pos_ch1            : unsigned(1 downto 0) := "00";
  signal cycle_pos_ch2            : unsigned(1 downto 0) := "00";
  signal cycle_pos_ch3            : unsigned(1 downto 0) := "00";
  signal cycle_pos_ch4            : unsigned(1 downto 0) := "00";
  signal cycle_pos_ch5            : unsigned(1 downto 0) := "00";
  signal clk_40_pos               : unsigned(1 downto 0) := "00";
  signal synchronized             : std_logic := '0';
  signal loc_delay_reset          : std_logic := '0';
  signal loc_IO_reset             : std_logic := '0';
  signal delayctrl_reset          : std_logic := '0';
  signal DCI_locked               : std_logic := '1';
  signal ich                      : natural range Nb_of_Lines downto 1      := 1;
  signal first_bad_pos            : std_logic_vector(31 downto 0) := (others => '0');
  signal n_good_taps              : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal n_good_evt               : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal n_test_evt               : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal pos_error                : std_logic_vector(31 downto 0) := (others => '0');
  signal do_bit_slip              : std_logic := '0';
  signal do_tap_shift             : std_logic := '0';

  signal ref_word                 : Word_t(31 downto 0)  := (others => (others => '1'));
  signal ref_mask                 : Word_t(31 downto 0)  := (others => (others => '1'));
  signal work_stream              : Word_t(5 downto 1);
  type   header_t is array (integer range <>) of std_logic_vector(3 downto 0);
  signal TST_header1              : header_t(5 downto 1) := ("0000","1001","1100","0110","0011");
  signal TST_header2              : header_t(5 downto 1) := ("0000","0011","0110","1100","1001");
  signal VFE_header1              : header_t(5 downto 1) := ("0011","0011","0011","0011","0011");
  signal VFE_header2              : header_t(5 downto 1) := ("1001","1001","1001","1001","1001");
  signal MEM_header1              : header_t(5 downto 1) := ("0011","1100","0000","0011","1100");
  signal MEM_header2              : header_t(5 downto 1) := ("1001","0110","0000","1001","0110");

  type   SYNC_state_t is (SYNC_idle,       SYNC_set_headers, SYNC_set_ref_words, SYNC_start,      SYNC_test_error, SYNC_shift_or_slip,
                          SYNC_shift_wait, SYNC_do_bitslip,  SYNC_do_byteslip,   SYNC_error,      SYNC_get_final_word);
  signal SYNC_state       : SYNC_state_t := SYNC_idle;
  
--  attribute IODELAY_GROUP : STRING;
--  attribute IODELAY_GROUP of inst_IDELAYCTRL:   label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch1: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch2: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch3: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch4: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch5: label is "In_delay_group";
 
begin  -- architecture behavioral

  delayctrl_reset     <= not MMCM_locked;
  inst_IDELAYCTRL : IDELAYCTRL
  port map (
    RDY    => delay_locked_loc,   -- 1-bit output: Ready output
    REFCLK => clk_delay_ctrl,     -- 1-bit input: Reference clock input
    RST    => delayctrl_reset     -- 1-bit input: Active high reset input
  );
  delay_locked <= delay_locked_loc;


  inst_IO_reset : process(IO_reset, delay_locked_loc, clk_160) is
  variable counter : unsigned(3 downto 0) := (others => '0');
  begin
    if IO_reset='1' or delay_locked_loc='0' then
      loc_IO_reset   <= '1';
      counter        := (others => '0');
    elsif rising_edge(clk_160) then
      if counter = x"f" then
        loc_IO_reset <= '0';
      else
        counter      := counter + 1;
      end if;
    end if;
  end process inst_IO_reset;

  inst_delay_reset : process(delay_reset, delay_locked_loc, clk_160) is
  variable counter : unsigned(3 downto 0) := (others => '0');
  begin
    if delay_reset='1' or delay_locked_loc='0' then
      loc_delay_reset   <= '1';
      counter        := (others => '0');
    elsif rising_edge(clk_160) then
      if counter = x"7" then
        loc_delay_reset <= '0';
      else
        counter      := counter + 1;
      end if;
    end if;
  end process inst_delay_reset;

  inst_ISERDES8_ch1 : selectio_eLink_8
  port map(
    data_in_from_pins_p => ch1_in_P,
    data_in_from_pins_n => ch1_in_N,
    data_in_to_device   => ch1_captured_stream_part,
    in_delay_reset      => loc_delay_reset,
    in_delay_data_ce    => delay_data_CE_shift(1),
    in_delay_data_inc   => delay_tap_dir,
    bitslip             => bitslip_shift(1),
    clk_in              => clk_640,
    clk_div_in          => clk_160,
    io_reset            => loc_IO_reset
--    io_reset            => '0'
  );
  inst_ISERDES8_ch2 : selectio_eLink_8
  port map(
    data_in_from_pins_p => ch2_in_P,
    data_in_from_pins_n => ch2_in_N,
    data_in_to_device   => ch2_captured_stream_part,
    in_delay_reset      => loc_delay_reset,
    in_delay_data_ce    => delay_data_CE_shift(2),
    in_delay_data_inc   => delay_tap_dir,
    bitslip             => bitslip_shift(2),
    clk_in              => clk_640,
    clk_div_in          => clk_160,
    io_reset            => loc_IO_reset
--    io_reset            => '0'
  );
  inst_ISERDES8_ch3 : selectio_eLink_8
  port map(
    data_in_from_pins_p => ch3_in_P,
    data_in_from_pins_n => ch3_in_N,
    data_in_to_device   => ch3_captured_stream_part,
    in_delay_reset      => loc_delay_reset,
    in_delay_data_ce    => delay_data_CE_shift(3),
    in_delay_data_inc   => delay_tap_dir,
    bitslip             => bitslip_shift(3),
    clk_in              => clk_640,
    clk_div_in          => clk_160,
    io_reset            => loc_IO_reset
--    io_reset            => '0'
  );
  inst_ISERDES8_ch4 : selectio_eLink_8
  port map(
    data_in_from_pins_p => ch4_in_P,
    data_in_from_pins_n => ch4_in_N,
    data_in_to_device   => ch4_captured_stream_part,
    in_delay_reset      => loc_delay_reset,
    in_delay_data_ce    => delay_data_CE_shift(4),
    in_delay_data_inc   => delay_tap_dir,
    bitslip             => bitslip_shift(4),
    clk_in              => clk_640,
    clk_div_in          => clk_160,
    io_reset            => loc_IO_reset
--    io_reset            => '0'
  );
  inst_ISERDES8_ch5 : selectio_eLink_8
  port map(
    data_in_from_pins_p => ch5_in_P,
    data_in_from_pins_n => ch5_in_N,
    data_in_to_device   => ch5_captured_stream_part,
    in_delay_reset      => loc_delay_reset,
    in_delay_data_ce    => delay_data_CE_shift(5),
    in_delay_data_inc   => delay_tap_dir,
    bitslip             => bitslip_shift(5),
    clk_in              => clk_640,
    clk_div_in          => clk_160,
    io_reset            => loc_IO_reset
--    io_reset            => '0'
  );

  VFE_synchronized            <= synchronized;
  ch1_captured_stream_swap(0) <= ch1_captured_stream_part(7);
  ch1_captured_stream_swap(1) <= ch1_captured_stream_part(6);
  ch1_captured_stream_swap(2) <= ch1_captured_stream_part(5);
  ch1_captured_stream_swap(3) <= ch1_captured_stream_part(4);
  ch1_captured_stream_swap(4) <= ch1_captured_stream_part(3);
  ch1_captured_stream_swap(5) <= ch1_captured_stream_part(2);
  ch1_captured_stream_swap(6) <= ch1_captured_stream_part(1);
  ch1_captured_stream_swap(7) <= ch1_captured_stream_part(0);
  ch2_captured_stream_swap(0) <= ch2_captured_stream_part(7);
  ch2_captured_stream_swap(1) <= ch2_captured_stream_part(6);
  ch2_captured_stream_swap(2) <= ch2_captured_stream_part(5);
  ch2_captured_stream_swap(3) <= ch2_captured_stream_part(4);
  ch2_captured_stream_swap(4) <= ch2_captured_stream_part(3);
  ch2_captured_stream_swap(5) <= ch2_captured_stream_part(2);
  ch2_captured_stream_swap(6) <= ch2_captured_stream_part(1);
  ch2_captured_stream_swap(7) <= ch2_captured_stream_part(0);
  ch3_captured_stream_swap(0) <= ch3_captured_stream_part(7);
  ch3_captured_stream_swap(1) <= ch3_captured_stream_part(6);
  ch3_captured_stream_swap(2) <= ch3_captured_stream_part(5);
  ch3_captured_stream_swap(3) <= ch3_captured_stream_part(4);
  ch3_captured_stream_swap(4) <= ch3_captured_stream_part(3);
  ch3_captured_stream_swap(5) <= ch3_captured_stream_part(2);
  ch3_captured_stream_swap(6) <= ch3_captured_stream_part(1);
  ch3_captured_stream_swap(7) <= ch3_captured_stream_part(0);
  ch4_captured_stream_swap(0) <= ch4_captured_stream_part(7);
  ch4_captured_stream_swap(1) <= ch4_captured_stream_part(6);
  ch4_captured_stream_swap(2) <= ch4_captured_stream_part(5);
  ch4_captured_stream_swap(3) <= ch4_captured_stream_part(4);
  ch4_captured_stream_swap(4) <= ch4_captured_stream_part(3);
  ch4_captured_stream_swap(5) <= ch4_captured_stream_part(2);
  ch4_captured_stream_swap(6) <= ch4_captured_stream_part(1);
  ch4_captured_stream_swap(7) <= ch4_captured_stream_part(0);
  ch5_captured_stream_swap(0) <= ch5_captured_stream_part(7);
  ch5_captured_stream_swap(1) <= ch5_captured_stream_part(6);
  ch5_captured_stream_swap(2) <= ch5_captured_stream_part(5);
  ch5_captured_stream_swap(3) <= ch5_captured_stream_part(4);
  ch5_captured_stream_swap(4) <= ch5_captured_stream_part(3);
  ch5_captured_stream_swap(5) <= ch5_captured_stream_part(2);
  ch5_captured_stream_swap(6) <= ch5_captured_stream_part(1);
  ch5_captured_stream_swap(7) <= ch5_captured_stream_part(0);
-- Output signals to device
  ISERDES_cycle : process (loc_delay_reset, clk_160) is
  begin
    if loc_delay_reset = '1' then
      work_stream <= (others => (others => '0'));
      cycle_pos_ch1       <= "00";
      cycle_pos_ch2       <= "00";
      cycle_pos_ch3       <= "00";
      cycle_pos_ch4       <= "00";
      cycle_pos_ch5       <= "00";
      clk_40_pos          <= "00";
      synchronized        <= '0';
    elsif rising_edge(clk_160) then
      clk_40_pos                              <= clk_40_pos+1;
      if cycle_pos_ch1 = "00" then
        ch1_captured_stream_loc(31 downto 24) <= ch1_captured_stream_swap;
      elsif cycle_pos_ch1 = "01" then
        ch1_captured_stream_loc(23 downto 16) <= ch1_captured_stream_swap;
      elsif cycle_pos_ch1 = "10" then
        ch1_captured_stream_loc(15 downto 8)  <= ch1_captured_stream_swap;
      elsif cycle_pos_ch1 = "11" then
        work_stream(1)(7 downto 0)            <= ch1_captured_stream_swap;
        work_stream(1)(31 downto 8)           <= ch1_captured_stream_loc(31 downto 8);
      end if;
      if cycle_pos_ch2 = "00" then
        ch2_captured_stream_loc(31 downto 24) <= ch2_captured_stream_swap;
      elsif cycle_pos_ch2 = "01" then
        ch2_captured_stream_loc(23 downto 16) <= ch2_captured_stream_swap;
      elsif cycle_pos_ch2 = "10" then
        ch2_captured_stream_loc(15 downto 8)  <= ch2_captured_stream_swap;
      elsif cycle_pos_ch2 = "11" then
        work_stream(2)(7 downto 0)            <= ch2_captured_stream_swap;
        work_stream(2)(31 downto 8)           <= ch2_captured_stream_loc(31 downto 8);
      end if;
      if cycle_pos_ch3 = "00" then
        ch3_captured_stream_loc(31 downto 24) <= ch3_captured_stream_swap;
      elsif cycle_pos_ch3 = "01" then
        ch3_captured_stream_loc(23 downto 16) <= ch3_captured_stream_swap;
      elsif cycle_pos_ch3 = "10" then
        ch3_captured_stream_loc(15 downto 8)  <= ch3_captured_stream_swap;
      elsif cycle_pos_ch3 = "11" then
        work_stream(3)(7 downto 0)            <= ch3_captured_stream_swap;
        work_stream(3)(31 downto 8)           <= ch3_captured_stream_loc(31 downto 8);
      end if;
      if cycle_pos_ch4 = "00" then
        ch4_captured_stream_loc(31 downto 24) <= ch4_captured_stream_swap;
      elsif cycle_pos_ch4 = "01" then
        ch4_captured_stream_loc(23 downto 16) <= ch4_captured_stream_swap;
      elsif cycle_pos_ch4 = "10" then
        ch4_captured_stream_loc(15 downto 8)  <= ch4_captured_stream_swap;
      elsif cycle_pos_ch4 = "11" then
        work_stream(4)(7 downto 0)            <= ch4_captured_stream_swap;
        work_stream(4)(31 downto 8)           <= ch4_captured_stream_loc(31 downto 8);
      end if;
      if cycle_pos_ch5 = "00" then
        ch5_captured_stream_loc(31 downto 24) <= ch5_captured_stream_swap;
      elsif cycle_pos_ch5 = "01" then
        ch5_captured_stream_loc(23 downto 16) <= ch5_captured_stream_swap;
      elsif cycle_pos_ch5 = "10" then
        ch5_captured_stream_loc(15 downto 8)  <= ch5_captured_stream_swap;
      elsif cycle_pos_ch5 = "11" then
        work_stream(5)(7 downto 0)            <= ch5_captured_stream_swap;
        work_stream(5)(31 downto 8)           <= ch5_captured_stream_loc(31 downto 8);
      end if;
      
      ch1_captured_stream                       <= work_stream(1);
      ch2_captured_stream                       <= work_stream(2);
      ch3_captured_stream                       <= work_stream(3);
      ch4_captured_stream                       <= work_stream(4);
      ch5_captured_stream                       <= work_stream(5);

      if synchronized='0' and clk_40_pos="00" then
        synchronized <= '1';
        cycle_pos_ch1    <= "01";
        cycle_pos_ch2    <= "01";
        cycle_pos_ch3    <= "01";
        cycle_pos_ch4    <= "01";
        cycle_pos_ch5    <= "01";
      elsif synchronized='1' then
        if byteslip(1)='1' then
          cycle_pos_ch1  <= cycle_pos_ch1 + 2;
        else
          cycle_pos_ch1  <= cycle_pos_ch1 + 1;
        end if;
        if byteslip(2)='1' then
          cycle_pos_ch2  <= cycle_pos_ch2 + 2;
        else
          cycle_pos_ch2  <= cycle_pos_ch2 + 1;
        end if;
        if byteslip(3)='1' then
          cycle_pos_ch3  <= cycle_pos_ch3 + 2;
        else
          cycle_pos_ch3  <= cycle_pos_ch3 + 1;
        end if;
        if byteslip(4)='1' then
          cycle_pos_ch4  <= cycle_pos_ch4 + 2;
        else
          cycle_pos_ch4  <= cycle_pos_ch4 + 1;
        end if;
        if byteslip(5)='1' then
          cycle_pos_ch5  <= cycle_pos_ch5 + 2;
        else
          cycle_pos_ch5  <= cycle_pos_ch5 + 1;
        end if;
      else
        cycle_pos_ch1    <= "00";
        cycle_pos_ch2    <= "00";
        cycle_pos_ch3    <= "00";
        cycle_pos_ch4    <= "00";
        cycle_pos_ch5    <= "00";
      end if;
    end if;
  end process ISERDES_cycle;

--  ch1_captured_stream <= "11101010101010101010101010101010";
--  ch2_captured_stream <= "11101010101010101010101010101010";
--  ch3_captured_stream <= "11101010101010101010101010101010";
--  ch4_captured_stream <= "11101010101010101010101010101010";
--  ch5_captured_stream <= "11101010101010101010101010101010";

--  Synchronise signals on clock falling edge for iserdes :
  delay_data_CE_shift <= delay_data_CE when falling_edge(clk_160) else delay_data_CE_shift;
  bitslip_shift       <= bitslip       when falling_edge(clk_160) else bitslip_shift;

  optimize_delay : process(loc_delay_reset, ADC_cal_busy, do_IDELAY_sync, clk_160)
    variable pos             : unsigned(4 downto 0)          := (others => '0');
    variable n_byteslip      : unsigned(3 downto 0)          := (others => '0');
    variable n_bitslip       : unsigned(3 downto 0)          := (others => '0');
    variable sync_done       : unsigned(5 downto 1)          := (others => '0');
    variable duration        : unsigned(31 downto 0)         := (others => '0');
    variable skip_clock      : unsigned(1 downto 0)          := (others => '0');             -- Skip 1 full 40 MHz clock afetr tap/byte or bit slip
  begin
    if loc_delay_reset= '1' then
      delay_data_CE                            <= (others => '0');
      bitslip                                  <= (others => '0');
      byteslip                                 <= (others => '0');
      Link_idelay_sync                         <= (others => '0');
      Link_byte_sync                           <= (others => '0');
      Link_bit_sync                            <= (others => '0');
      Link_idelay_pos                          <= (others => (others => '0'));
      SYNC_state                               <= SYNC_idle;
      duration                                 := (others => '0');
      pos_error                                <= (others => '0');
      ref_word                                 <= (others => (others => '0'));
      pos                                      := (others => '0');
      sync_done                                := (others => '0');
      skip_clock                               := (others => '0');
      ich                                      <= 1;
      Link_error_sync                          <= (others => '1');
    elsif Rising_Edge(clk_160) then
      delay_data_CE                            <= (others => '0');
      byteslip                                 <= (others => '0');
      bitslip                                  <= (others => '0');
      delay_data_CE_del1                       <= delay_ADC_number;
      delay_data_CE_del2                       <= delay_data_CE_del1;
      bitslip_del1                             <= bitslip_ADC_number;
      bitslip_del2                             <= bitslip_del1;
      byteslip_del1                            <= byteslip_ADC_number;
      byteslip_del2                            <= byteslip_del1;

      if ADC_cal_busy(ich) = '1' or do_IDELAY_sync = '1' then 
        duration                               := duration+1;
      end if;
      case SYNC_state is
      when SYNC_idle =>
-- Take into requests from IPbus for tap changing, bitslip and byteslip:
        if delay_data_CE_del1(1)='1' and delay_data_CE_del2(1)='0' then
          delay_data_CE(1)                     <= '1';
        end if;
        if delay_data_CE_del1(2)='1' and delay_data_CE_del2(2)='0' then
          delay_data_CE(2)                     <= '1';
        end if;
        if delay_data_CE_del1(3)='1' and delay_data_CE_del2(3)='0' then
          delay_data_CE(3)                     <= '1';
        end if;
        if delay_data_CE_del1(4)='1' and delay_data_CE_del2(4)='0' then
          delay_data_CE(4)                     <= '1';
        end if;
        if delay_data_CE_del1(5)='1' and delay_data_CE_del2(5)='0' then
          delay_data_CE(5)                     <= '1';
        end if;
        if bitslip_del1(1)='1' and bitslip_del2(1)='0' then
          bitslip(1)                          <= '1';
        end if;
        if bitslip_del1(2)='1' and bitslip_del2(2)='0' then
          bitslip(2)                          <= '1';
        end if;
        if bitslip_del1(3)='1' and bitslip_del2(3)='0' then
          bitslip(3)                          <= '1';
        end if;
        if bitslip_del1(4)='1' and bitslip_del2(4)='0' then
          bitslip(4)                          <= '1';
        end if;
        if bitslip_del1(5)='1' and bitslip_del2(5)='0' then
          bitslip(5)                          <= '1';
        end if;
        if byteslip_del1(1)='1' and byteslip_del2(1)='0' then
          byteslip(1)                          <= '1';
        end if;
        if byteslip_del1(2)='1' and byteslip_del2(2)='0' then
          byteslip(2)                          <= '1';
        end if;
        if byteslip_del1(3)='1' and byteslip_del2(3)='0' then
          byteslip(3)                          <= '1';
        end if;
        if byteslip_del1(4)='1' and byteslip_del2(4)='0' then
          byteslip(4)                          <= '1';
        end if;
        if byteslip_del1(5)='1' and byteslip_del2(5)='0' then
          byteslip(5)                          <= '1';
        end if;
-- Start idelay optimization when ADC is in calibration mode and if the link is not yet synchronized
-- Be carefull : in test mode, we have 4 links but only one ADC.
-- The delayctrl clock is 200 MHz, which gives 78 ps per tap
-- With 1280 MHz clock, we have 781 ps per bit, thus 10 taps
-- We will select a tap after 4 identical ref bytes and no reading error on 255 samples
        if (ADC_cal_busy(ich) = '1' or do_IDELAY_sync = '1') and DTU_auto_sync = '1' and sync_done(ich) = '0' then 
--        if ADC_cal_busy(ich)       = '1' and DTU_auto_sync = '1' and 
--           (Link_idelay_sync(ich) = '0' or
--            Link_byte_sync(ich)   = '0' or
--            Link_bit_sync(ich)    = '0')   then
          SYNC_state                           <= SYNC_set_headers;
          pos                                  := (others => '0');
          pos_error                            <= (others => '0');
          skip_clock                           := "10";
          ref_word                             <= (others => (others => '0'));
          ref_mask                             <= (others => (others => '0'));
          if ich = Nb_of_lines then -- Reset duration counter at first channel to get total duration for 5 channels
            duration                           := (others => '0');
          end if;
          if eLink_active(ich) = '0' then -- If link not active, put it as synchronized
            Link_idelay_sync(ich)              <= '1';
            Link_byte_sync(ich)                <= '1';
            Link_bit_sync(ich)                 <= '1';
--            if ich < Nb_of_Lines then
            if ich < Nb_of_lines then
              ich                              <= ich+1;
            else
              ich                              <= 1;
            end if;
          end if;
        end if;
      when SYNC_set_headers =>
        if ADC_test_mode = '0' then
          ref_word(0)                          <= DTU_Sync_pattern and x"F000F000";
        elsif eLink_active = "11111" then
          ref_word(0)                          <= VFE_header1(ich)(3 downto 0)&x"000"&VFE_header2(ich)(3 downto 0)&x"000";
        elsif ADC_MEM_mode = '0' then
          ref_word(0)                          <= TST_header1(ich)(3 downto 0)&x"000"&TST_header2(ich)(3 downto 0)&x"000";
        else
          ref_word(0)                          <= MEM_header1(ich)(3 downto 0)&x"000"&MEM_header2(ich)(3 downto 0)&x"000";
        end if;
        ref_mask(0)                            <= x"F000F000";
        first_bad_pos(0)                       <= '0';
        n_good_taps(0)                         <= (others =>'0');
        n_good_evt(0)                          <= (others => '0');
        n_test_evt(0)                          <= (others => '0');
        pos_error(0)                           <= '1';
        SYNC_state                             <= SYNC_set_ref_words;
      when SYNC_set_ref_words =>
        for i in 1 to 31 loop
          ref_word(i)                          <= ref_word(0) rol i;
          ref_mask(i)                          <= ref_mask(0) rol i;
          first_bad_pos(i)                     <= '0';
          n_good_taps(i)                       <= (others => '0');
          n_good_evt(i)                        <= (others => '0');
          n_test_evt(i)                        <= (others => '0');
        end loop;
        SYNC_state                             <= SYNC_start;
      when SYNC_start =>
-- New data is aligned with the 40 MHz clock (4 bytes @ 160 MHz)
-- At each new data compare headers with reference ones and increment error counter
-- Do it 16 times, then shift idelay by one tap. We assume that if we have no error for 16 clocks, the tap is good.
        if clk_40_pos="00" then
          if skip_clock /= 0 then
            skip_clock                         := skip_clock -1;
          else
            for i in 0 to 31 loop
              if (work_stream(ich) and ref_mask(i)) /= ref_word(i) then
                first_bad_pos(i)               <= '1';
                n_good_taps(i)                 <= (others =>'0');
                n_good_evt(i)                  <= (others =>'0');
                pos_error(i)                   <= '1';
              elsif first_bad_pos(i) = '1' then
                if n_good_evt(i) = 0 then
                  n_good_taps(i)               <= n_good_taps(i) + 1;
                end if;
                n_good_evt(i)                  <= n_good_evt(i) + 1;
                pos_error(i)                   <= '0';
              else
                pos_error(i)                   <= '1';
              end if;
            end loop;
            SYNC_state                         <= SYNC_test_error;        -- test if we are at the right position or not, then try another tap
          end if;
        end if;
      when SYNC_test_error =>
-- If we had an error tap, memorize position and if we had 2 error position found, stop the loop
        if pos="11111" then                                            -- Don't loop for ever. If we are here it is certainly due to lack of data
          SYNC_state                           <= SYNC_error;
        else
          SYNC_state                           <= SYNC_shift_or_slip;
          do_tap_shift                         <= '1';
          do_bit_slip                          <= '0';
          for i in 0 to 31 loop
            if pos_error(i) = '0' and n_good_taps(i) >= 3 then
              if n_good_evt(i) = x"FF" then
                do_bit_slip                    <= '1';
                do_tap_shift                   <= '0';
                link_idelay_pos(ich)           <= std_logic_vector(pos);
                n_bitslip                      := (others => '0');
                n_byteslip                     := (others => '0');
              elsif n_test_evt(i) < x"FF" then
                n_test_evt(i)                  <= n_test_evt(i)+1;
                skip_clock                     := "00";
                do_bit_slip                    <= '0';
                do_tap_shift                   <= '0';
              end if;
            else
              n_good_evt(i)                    <= (others => '0');
              n_test_evt(i)                    <= (others => '0');
            end if;
          end loop;
        end if;
      when SYNC_shift_or_slip  =>                                       -- Decide if we a tap shift or go to bit slip or do nothing (loop on events with same setting)
        if do_bit_slip = '1' then
          SYNC_state                           <= SYNC_do_bitslip;
        elsif do_tap_shift = '0' then
          skip_clock                           := "00";
          SYNC_state                           <= SYNC_start;
        else
          delay_data_CE(ich)                   <= '1';                  -- Error position : shift by one tap 
          SYNC_state                           <= SYNC_shift_wait;
        end if;
      when SYNC_shift_wait  =>                                          -- Wait for IDELAYCTRL to get ready
        if delay_locked_loc = '1' then
          pos_error                            <= (others => '0');
          pos                                  := pos+1;
          skip_clock                           := "10";
          SYNC_state                           <= SYNC_start;           -- We restart test with a new delay setting
        end if;
      when SYNC_do_bitslip =>                                          -- Slip bits to get the header at Byte edge
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          elsif ((work_stream(ich) and ref_mask(0))  = ref_word(0)) or
                ((work_stream(ich) and ref_mask(8))  = ref_word(8)) or
                ((work_stream(ich) and ref_mask(16)) = ref_word(16)) or
                ((work_stream(ich) and ref_mask(24)) = ref_word(24)) then
            Link_bit_sync(ich)                 <= '1';
            SYNC_state                         <= SYNC_do_byteslip;
            skip_clock                         := "10";
          else
            bitslip(ich)                       <= '1';
            n_bitslip                          := n_bitslip+1;
            skip_clock                         := "10";
          end if;
          if n_bitslip = 15 then                                        -- Don't loop forever
            SYNC_state                         <= SYNC_error;
          end if;
        end if;
      when SYNC_do_byteslip =>                                         -- Slip bytes to get header synchronous with 40 MHz
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          elsif ((work_stream(ich) and ref_mask(0))  = ref_word(0)) then
            Link_byte_sync(ich)                <= '1';
            skip_clock                         := "10";
            sync_done(ich)                     := '1';
            SYNC_state                         <= SYNC_get_final_word;
            Link_error_sync(ich)               <= '0';
          else
            byteslip(ich)                      <= '1';
            n_byteslip                         := n_byteslip+1;
            skip_clock                         := "10";
          end if;
          if n_byteslip = 15 then                                        -- Don't loop forever
            SYNC_state                         <= SYNC_error;
          end if;
          Sync_duration                        <= std_logic_vector(duration);
        end if;
      when SYNC_get_final_word =>
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          else
--            if ich < Nb_of_Lines then
            if ich < Nb_of_lines then
              ich                              <= ich+1;
            else
              ich                              <= 1; 
            end if;
            SYNC_state                         <= SYNC_idle;
          end if;
        end if;
      when SYNC_error =>                                               -- Didn't succeed to tune Idelay.
        Link_error_sync(ich)                   <= '1';
        Link_idelay_sync(ich)                  <= '0';                   -- Stay in this state until reset
        Link_idelay_pos(ich)                   <= (others => '1');
        Sync_duration                          <= std_logic_vector(duration);
        SYNC_state                             <= SYNC_idle;
        sync_done(ich)                         := '1';
--        if ich < Nb_of_Lines then
        if ich < Nb_of_lines then
          ich                                  <= ich+1; 
        else
          ich                                  <= 1;
        end if;
      end case;
    end if;
  end process optimize_delay;

end architecture rtl;
