----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for SAFE control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.SAFE_IO.all;

entity SAFE_ctrlreg is
  generic (
    USE_GPIO_WITH_LVR : boolean := USE_LVRB;
    USE_GPIO_WITH_DAC : boolean := not USE_LVRB
  );

  port(
    clk            : in  std_logic;
    reset          : in  std_logic;
    ipbus_in       : in  ipb_wbus;
    ipbus_out      : out ipb_rbus;
    SAFE_monitor   : in  SAFE_Monitor_t;
--    SAFE_control   : out SAFE_Control_t := DEFAULT_SAFE_Control;
    SAFE_control   : out SAFE_Control_t;
    VFE_monitor    : in  VFE_Monitor_t;
--    VFE_control    : out VFE_Control_t  := DEFAULT_VFE_Control;
    VFE_control    : out VFE_Control_t;
    APD_temp_in    : in std_logic;
    APD_temp_ref   : in std_logic;
    Vdummy_in      : in std_logic;
    Vdummy_ref     : in std_logic;
    CATIA_temp_in  : in std_logic;
    CATIA_temp_ref : in std_logic;
    V2P5_in        : in std_logic;
    V2P5_ref       : in std_logic;
    V1P2_in        : in std_logic;
    V1P2_ref       : in std_logic;
    Switch         : in std_logic_vector(3 downto 0)
    );
end SAFE_ctrlreg;

architecture rtl of SAFE_ctrlreg is

  signal ack : std_logic := '0';

    --* State type of the ipbus process
  type   ipbus_rw_type is (ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_ack, ipbus_finished);
  signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_idle;

  type   XADC_status_type is (XADC_idle, XADC_read, XADC_write);
  signal XADC_status           : XADC_status_type := XADC_idle;

  signal XADC_control          : XADC_control_t := default_XADC_control;
  signal XADC_monitor          : XADC_monitor_t := default_XADC_monitor;
  signal loc_XADC_data         : std_logic_vector(15 downto 0) := (others => '0');
  signal reading               : std_logic_vector(15 downto 0) := (others => '0');
  signal muxaddr               : std_logic_vector( 4 downto 0) := (others => '0');
  signal channel               : std_logic_vector( 4 downto 0) := (others => '0');
  signal vauxn                 : std_logic_vector(15 downto 0) := (others => '0');
  signal vauxp                 : std_logic_vector(15 downto 0) := (others => '0');
  signal wait_for_XADC_ready   : std_logic := '0';
  signal wait_for_DTU_ready    : std_logic := '0';
  signal wait_for_I2C_ready    : std_logic := '0';
  signal total_seq             : unsigned(31 downto 0) := (others => '0');
  signal total_conv            : unsigned(31 downto 0) := (others => '0');
  signal total_access          : unsigned(31 downto 0) := (others => '0');
  signal XADC_eos              : std_logic := '0';
  signal XADC_eos_del          : std_logic := '0';
  signal XADC_eoc              : std_logic := '0';
  signal XADC_eoc_del          : std_logic := '0';
  signal XADC_ready_del        : std_logic := '0';
  signal XADC_clk              : std_logic := '0';

begin
  reg: process (reset, clk, switch) is
  begin  -- process reg
    if reset = '1' then
      SAFE_control          <= DEFAULT_SAFE_Control;
      VFE_control           <= DEFAULT_VFE_Control;
      ipbus_rw_state        <= ipbus_rw_idle;
      ipbus_out.ipb_rdata   <= (others => '0'); --zero the response before the actual case response
      wait_for_XADC_ready   <= '0';
      wait_for_DTU_ready    <= '0';
    elsif rising_edge(clk) then
      SAFE_control.trigger                    <= '0';
      SAFE_control.TP_trigger                 <= '0';
      SAFE_control.clock_reset                <= '0';
      SAFE_control.reset                      <= '0';
      SAFE_control.G10_calib_trigger          <= '0';
      SAFE_control.G1_calib_trigger           <= '0';
--      SAFE_control.delay_reset                <= '0';
      SAFE_control.delay_ADC_number           <= (Others => '0');
      SAFE_control.bitslip_ADC_number         <= (Others => '0');
      SAFE_control.byteslip_ADC_number        <= (Others => '0');
--      SAFE_control.start_idelay_sync          <= (Others => '0');
      SAFE_control.VICEPP_Clk_config(31)      <= '0';
      XADC_control.XADC_access                <= '0';
      VFE_control.ADC_Start_ReSync            <= '0';
      VFE_control.I2C_reset                   <= '0';
--      VFE_control.VFE_reset                   <= '0';
      SAFE_control.IO_reset                   <= '0';
      SAFE_control.delay_reset                <= '0';
      SAFE_control.CRC_reset                  <= '0';

      case ipbus_rw_state is
      when ipbus_rw_idle =>
        VFE_control.I2C_access_VFE            <= '0';
        VFE_control.I2C_access_LVRB           <= '0';
        VFE_control.SPI_access_DAC            <= '0';
        ack <= '0';
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state                    <= ipbus_write_start;
          else
            ipbus_rw_state                    <= ipbus_read_start;
          end if;
        end if;
      when ipbus_read_start =>
        ipbus_out.ipb_rdata                   <= (others => '0'); --zero the response before the actual case response
        case ipbus_in.ipb_addr(6 downto 0) is
        when "0000000" =>                                                                   -- FW_VER
          ipbus_out.ipb_rdata                 <= SAFE_monitor.firmware_ver;
        when "0000001"=> -- Clock selection (0=160 MHz, 1=120 MHz, 2=80 MHz, 3=40 MHz)         SAFE_CTRL
          ipbus_out.ipb_rdata(0)              <= SAFE_monitor.reset;
          ipbus_out.ipb_rdata(1)              <= SAFE_monitor.FIFO_mode;
          ipbus_out.ipb_rdata(2)              <= SAFE_monitor.trig_loop;
          ipbus_out.ipb_rdata(3)              <= SAFE_monitor.trig_self;
          ipbus_out.ipb_rdata(6 downto 4)     <= SAFE_monitor.seq_clock_phase;
--          ipbus_out.ipb_rdata(6)            <= SAFE_monitor.clock_phase;
          ipbus_out.ipb_rdata(7)              <= SAFE_monitor.clock_reset;
          ipbus_out.ipb_rdata(19 downto 8)    <= SAFE_monitor.trig_self_thres;
          ipbus_out.ipb_rdata(26 downto 22)   <= SAFE_monitor.trig_self_mask;
--          ipbus_out.ipb_rdata(27)             <= SAFE_monitor.calib_pulse_enabled;
          ipbus_out.ipb_rdata(31 downto 28)   <= SAFE_monitor.board_sn(3 downto 0);
        when "0000010" =>                                                                   -- TRIG_DELAY
          ipbus_out.ipb_rdata(15 downto 0)    <= SAFE_monitor.trigger_HW_delay;
          ipbus_out.ipb_rdata(31 downto 16)   <= SAFE_monitor.trigger_SW_delay;
        when "0000011" => -- I2C and Calib status                                           -- VFE_CTRL
          ipbus_out.ipb_rdata(0)              <= VFE_monitor.ADC_calib_mode;
          ipbus_out.ipb_rdata(1)              <= VFE_monitor.ADC_test_mode;
          ipbus_out.ipb_rdata(2)              <= VFE_monitor.I2C_LVRB_busy;
          ipbus_out.ipb_rdata(3)              <= VFE_monitor.I2C_VFE_busy;
          ipbus_out.ipb_rdata(5)              <= VFE_monitor.I2C_LVRB_error;
          ipbus_out.ipb_rdata(6)              <= VFE_monitor.I2C_VFE_error;
          ipbus_out.ipb_rdata(12 downto 8)    <= VFE_monitor.eLink_active;
          ipbus_out.ipb_rdata(13)             <= VFE_monitor.ADC_Ped_Mux;
          ipbus_out.ipb_rdata(27)             <= VFE_monitor.I2C_lpGBT_mode;
          ipbus_out.ipb_rdata(28)             <= VFE_monitor.I2C_bulky_DTU;
          ipbus_out.ipb_rdata(29)             <= VFE_monitor.DTU_auto_sync;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_reset;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.VFE_reset;
        when "0000100" => -- Calib trigger settting                                         -- CALIB_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= SAFE_monitor.TP_duration;
          ipbus_out.ipb_rdata(30 downto 16)   <= SAFE_monitor.TP_delay(14 downto 0);
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.TP_dummyb;
        when "0000101" => -- Delay locked signals                                           -- DELAY_CTRL
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.Link_bit_sync(1)&SAFE_monitor.Link_idelay_pos(1);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.Link_bit_sync(2)&SAFE_monitor.Link_idelay_pos(2);
          ipbus_out.ipb_rdata(17  downto  12) <= SAFE_monitor.Link_bit_sync(3)&SAFE_monitor.Link_idelay_pos(3);
          ipbus_out.ipb_rdata(23  downto  18) <= SAFE_monitor.Link_bit_sync(4)&SAFE_monitor.Link_idelay_pos(4);
          ipbus_out.ipb_rdata(29  downto  24) <= SAFE_monitor.Link_bit_sync(5)&SAFE_monitor.Link_idelay_pos(5);
          ipbus_out.ipb_rdata(30)             <= SAFE_monitor.FE_synchronized;
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.VFE_synchronized;
        when "0000110" => -- I2C spy acknowledge lsb                                        -- CLK_SETTING/I2C_ACK_LSB
--          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(31 downto 0);
          ipbus_out.ipb_rdata(31 downto 0)    <= SAFE_monitor.VICEPP_CLk_config;
        when "0000111" => -- I2C spy acknowledge msb                                        -- I2C_ACK_MSB
--          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(63 downto 32);
          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0001000" => -- XADC alarm register                                            -- DRP_XADC
          ipbus_out.ipb_rdata(22 downto 16)   <= XADC_monitor.XADC_addr; 
          ipbus_out.ipb_rdata(15 downto 0)    <= XADC_monitor.XADC_data;
        when "0001001" => -- Sequence counter (32 bits)                                     -- CONV_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_conv);
        when "0001010" => -- Sequence counter (32 bits)                                     -- SEQ_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_seq);
        when "0001011" => -- Sequence counter (32 bits)                                     -- ACCESS_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_access);
        when "0001100" => -- VICEPP specific stuff (clock mux setting)                      -- VICEPP_CLK
          ipbus_out.ipb_rdata(30 downto 0)    <= SAFE_monitor.VICEPP_clk_config(30 downto 0);
        when "0001110" => -- Debug register. Put here what you want                         -- DEBUG1
          ipbus_out.ipb_rdata(30)             <= SAFE_monitor.DCI_locked; -- DelayCTRL locked signal
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.delay_locked; -- DelayCTRL locked signal
        when "0001111" => -- Debug register. Put here what you want                         -- DEBUG2
          ipbus_out.ipb_rdata                 <= SAFE_monitor.sync_duration;
        when "0010000" =>                                                                   -- LVRB_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_LVRB;
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when "0010001" =>                                                                   -- CATIA_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE;
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when "0010010" =>                                                                   -- DTU_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE;
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when "0010011" =>                                                                   -- DAC_CTRL
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data(0);
          ipbus_out.ipb_rdata(15 downto 8)    <= VFE_monitor.I2C_Bulk_data(1);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when "0010100" =>                                                                   -- VREF_MUX_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.SPI_DAC_data;
          ipbus_out.ipb_rdata(19 downto 16)   <= VFE_monitor.SPI_DAC_number;
          ipbus_out.ipb_rdata(23 downto 20)   <= VFE_monitor.SPI_DAC_action;
        when "0011000" => -- LiTE-DTU Resync command                                        -- DTU_RESYNC
          ipbus_out.ipb_rdata                 <= VFE_monitor.ADC_ReSync_data;
        when "0011001" =>                                                                   -- LVRB_FAULT
          ipbus_out.ipb_rdata(7 downto 0)     <= VFE_monitor.I2C_Scan_Fault_LVRB_2V5;
          ipbus_out.ipb_rdata(23 downto 16)   <= VFE_monitor.I2C_Scan_Fault_LVRB_1V2;
        when "0011010" =>                                                                   -- LVRB_CURRENT
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Scan_ISense_LVRB_2V5;
          ipbus_out.ipb_rdata(31 downto 16)   <= VFE_monitor.I2C_Scan_ISense_LVRB_1V2;
        when "0011011" =>                                                                   -- DTU_SYNC_PATTERN
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.DTU_Sync_pattern;
        when "0011100" => -- LiTE-DTU Resync idle pattern                                   -- RESYNC_IDLE
          ipbus_out.ipb_rdata(7 downto 0)     <= VFE_monitor.ADC_ReSync_idle;
          ipbus_out.ipb_rdata(8)              <= VFE_monitor.ADC_invert_data;
          ipbus_out.ipb_rdata(13 downto 9)    <= VFE_monitor.ADC_PLL_lock;         
        when "0011101" =>                                                                   -- LVRB_VOLTAGE
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Scan_VSense_LVRB_2V5;
          ipbus_out.ipb_rdata(31 downto 16)   <= VFE_monitor.I2C_Scan_VSense_LVRB_1V2;
        when "0100000" => -- I2C data from register 3 downto 0   (d03-d02-d01-d0)           -- DTU_BULK1
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data_ref(0);
          ipbus_out.ipb_rdata(15 downto 8)    <= VFE_monitor.I2C_Bulk_data_ref(1);
          ipbus_out.ipb_rdata(23 downto 16)   <= VFE_monitor.I2C_Bulk_data_ref(2);
          ipbus_out.ipb_rdata(31 downto 24)   <= VFE_monitor.I2C_Bulk_data_ref(3);
        when "0100001" => -- I2C data from register 3 downto 0   (d03-d02-d01-d0)           -- DTU_BULK2
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data_ref(4);
          ipbus_out.ipb_rdata(15 downto 8)    <= VFE_monitor.I2C_Bulk_data_ref(5);
          ipbus_out.ipb_rdata(23 downto 16)   <= VFE_monitor.I2C_Bulk_data_ref(6);
          ipbus_out.ipb_rdata(31 downto 24)   <= VFE_monitor.I2C_Bulk_data_ref(7);
        when "0100010" => -- I2C data from register 3 downto 0   (d03-d02-d01-d0)           -- DTU_BULK3
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data_ref(8);
          ipbus_out.ipb_rdata(15 downto 8)    <= VFE_monitor.I2C_Bulk_data_ref(9);
          ipbus_out.ipb_rdata(23 downto 16)   <= VFE_monitor.I2C_Bulk_data_ref(10);
          ipbus_out.ipb_rdata(31 downto 24)   <= VFE_monitor.I2C_Bulk_data_ref(11);
        when "0100011" => -- I2C data from register 3 downto 0   (d03-d02-d01-d0)           -- DTU_BULK4
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data_ref(12);
          ipbus_out.ipb_rdata(15 downto 8)    <= VFE_monitor.I2C_Bulk_data_ref(13);
          ipbus_out.ipb_rdata(23 downto 16)   <= VFE_monitor.I2C_Bulk_data_ref(14);
          ipbus_out.ipb_rdata(31 downto 24)   <= VFE_monitor.I2C_Bulk_data_ref(15);
        when "0100100" => -- I2C data from register 3 downto 0   (d03-d02-d01-d0)           -- DTU_BULK5
          ipbus_out.ipb_rdata(7  downto 0)    <= VFE_monitor.I2C_Bulk_data_ref(16);
          ipbus_out.ipb_rdata(15  downto 8)   <= VFE_monitor.I2C_Bulk_data_ref(17);
          ipbus_out.ipb_rdata(23  downto 16)  <= VFE_monitor.I2C_Bulk_data_ref(18);
          ipbus_out.ipb_rdata(31 downto 24)   <= (others => '0');
        when "0110000" => -- I2C spy acknowledge byte 0                                     -- I2C_ACK1
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(31 downto 0);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110001" => -- I2C spy acknowledge byte 1                                     -- I2C_ACK2
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(63 downto 32);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110010" => -- I2C spy acknowledge byte 2                                     -- I2C_ACK3
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(95 downto 64);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110011" => -- I2C spy acknowledge byte 3                                     -- I2C_ACK4
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(127 downto 96);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110100" => -- I2C spy acknowledge byte 4                                     -- I2C_ACK5
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(159 downto 128);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110101" => -- I2C spy acknowledge byte 5                                     -- I2C_ACK6
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(191 downto 160);
--          ipbus_out.ipb_rdata(31 downto 0)    <= (others => '0');
        when "0110110" => -- I2C spy acknowledge byte 6                                     -- I2C_NACK
          ipbus_out.ipb_rdata(7 downto 0)     <= std_logic_vector(VFE_monitor.I2C_n_ack_LVRB);
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when "0111000" => -- CRC error counters                                             -- CRC1
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(1));
        when "0111001" => -- CRC error counters                                             -- CRC2
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(2));
        when "0111010" => -- CRC error counters                                             -- CRC3
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(3));
        when "0111011" => -- CRC error counters                                             -- CRC4
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(4));
        when "0111100" => -- CRC error counters                                             -- CRC5
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(5));
        when "1000000" => -- DEBUG1_0
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(0);
        when "1000001" => -- DEBUG1_1
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(1);
        when "1000010" => -- DEBUG1_2
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(2);
        when "1000011" => -- DEBUG1_3
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(3);
        when "1000100" => -- DEBUG1_4
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(4);
        when "1000101" => -- DEBUG1_5
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(5);
        when "1000110" => -- DEBUG1_6
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(6);
        when "1000111" => -- DEBUG1_7
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(7);
        when "1001000" => -- DEBUG1_8
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(8);
        when "1001001" => -- DEBUG1_9
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(9);
        when "1001010" => -- DEBUG1_10
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(10);
        when "1001011" => -- DEBUG1_11
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(11);
        when "1001100" => -- DEBUG1_12
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(12);
        when "1001101" => -- DEBUG1_13
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(13);
        when "1001110" => -- DEBUG1_14
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(14);
        when "1001111" => -- DEBUG1_15
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(15);
        when "1010000" => -- DEBUG1_16
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(16);
        when "1010001" => -- DEBUG1_17
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(17);
        when "1010010" => -- DEBUG1_18
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(18);
        when "1010011" => -- DEBUG1_19
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(19);
        when "1010100" => -- DEBUG1_20
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(20);
        when "1010101" => -- DEBUG1_21
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(21);
        when "1010110" => -- DEBUG1_22
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(22);
        when "1010111" => -- DEBUG1_23
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(23);
        when "1011000" => -- DEBUG1_24
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(24);
        when "1011001" => -- DEBUG1_25
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(25);
        when "1011010" => -- DEBUG1_26
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(26);
        when "1011011" => -- DEBUG1_27
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(27);
        when "1011100" => -- DEBUG1_28
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(28);
        when "1011101" => -- DEBUG1_29
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(29);
        when "1011110" => -- DEBUG1_30
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(30);
        when "1011111" => -- DEBUG1_31
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug1(31);
        when "1100000" => -- DEBUG2_0
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(0);
        when "1100001" => -- DEBUG2_1
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(1);
        when "1100010" => -- DEBUG2_2
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(2);
        when "1100011" => -- DEBUG2_3
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(3);
        when "1100100" => -- DEBUG2_4
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(4);
        when "1100101" => -- DEBUG2_5
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(5);
        when "1100110" => -- DEBUG2_6
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(6);
        when "1100111" => -- DEBUG2_7
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(7);
        when "1101000" => -- DEBUG2_8
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(8);
        when "1101001" => -- DEBUG2_9
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(9);
        when "1101010" => -- DEBUG2_10
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(10);
        when "1101011" => -- DEBUG2_11
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(11);
        when "1101100" => -- DEBUG2_12
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(12);
        when "1101101" => -- DEBUG2_13
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(13);
        when "1101110" => -- DEBUG2_14
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(14);
        when "1101111" => -- DEBUG2_15
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(15);
        when "1110000" => -- DEBUG2_16
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(16);
        when "1110001" => -- DEBUG2_17
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(17);
        when "1110010" => -- DEBUG2_18
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(18);
        when "1110011" => -- DEBUG2_19
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(19);
        when "1110100" => -- DEBUG2_20
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(20);
        when "1110101" => -- DEBUG2_21
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(21);
        when "1110110" => -- DEBUG2_22
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(22);
        when "1110111" => -- DEBUG2_23
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(23);
        when "1111000" => -- DEBUG2_24
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(24);
        when "1111001" => -- DEBUG2_25
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(25);
        when "1111010" => -- DEBUG2_26
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(26);
        when "1111011" => -- DEBUG2_27
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(27);
        when "1111100" => -- DEBUG2_28
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(28);
        when "1111101" => -- DEBUG2_29
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(29);
        when "1111110" => -- DEBUG2_30
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(30);
        when "1111111" => -- DEBUG2_31
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug2(31);
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(6 downto 0) is
        when "0000000" =>
          SAFE_control.trigger             <= ipbus_in.ipb_wdata(0);
          SAFE_control.TP_trigger          <= ipbus_in.ipb_wdata(1);
          if ipbus_in.ipb_wdata(1) = '1' then -- Generate a ReSync transaction for TP trigger with LiTE-DTU version >= 2.0
            VFE_control.ADC_ReSync_data      <= x"0000000d";
            VFE_control.ADC_Start_ReSync     <= '1';
            wait_for_DTU_ready               <= '1';
          end if;
          SAFE_control.gen_100Hz           <= ipbus_in.ipb_wdata(2);
          SAFE_control.LED_on              <= ipbus_in.ipb_wdata(3);
          SAFE_control.TP_mode             <= ipbus_in.ipb_wdata(4);
          SAFE_control.G10_calib_trigger   <= ipbus_in.ipb_wdata(5);
          SAFE_control.G1_calib_trigger    <= ipbus_in.ipb_wdata(6);
          SAFE_control.CRC_reset           <= ipbus_in.ipb_wdata(31); -- Reset CRC counters
        when "0000001" =>
          SAFE_control.reset               <= ipbus_in.ipb_wdata(0);
          SAFE_control.FIFO_mode           <= ipbus_in.ipb_wdata(1); -- single event (0) or FIFO (1) DAQ mode
          SAFE_control.trig_loop           <= ipbus_in.ipb_wdata(2); -- external trigger (0) or loop local software trigger (1)
          SAFE_control.trig_self           <= ipbus_in.ipb_wdata(3); -- generate self trigger looking at input data
          SAFE_control.clock_reset         <= ipbus_in.ipb_wdata(7);
          SAFE_control.trig_self_thres     <= ipbus_in.ipb_wdata(19 downto 8);
          SAFE_control.trig_self_mask      <= ipbus_in.ipb_wdata(26 downto 22);
          SAFE_control.calib_pulse_enabled <= ipbus_in.ipb_wdata(27);
          SAFE_control.calib_mux_enabled   <= ipbus_in.ipb_wdata(28);
          SAFE_control.trig_self_mode      <= ipbus_in.ipb_wdata(31);
        when "0000010" =>
          SAFE_control.trigger_HW_delay    <= ipbus_in.ipb_wdata(15 downto 0);
          SAFE_control.trigger_SW_delay    <= ipbus_in.ipb_wdata(31 downto 16);
        when "0000011" => -- ADC/eLinks config
          VFE_control.ADC_calib_mode       <= ipbus_in.ipb_wdata(0);
          VFE_control.ADC_test_mode        <= ipbus_in.ipb_wdata(1);
          VFE_control.LVRB_auto_scan       <= ipbus_in.ipb_wdata(2);
          VFE_control.ADC_invert_ReSync    <= ipbus_in.ipb_wdata(3);
          VFE_control.ADC_MEM_mode         <= ipbus_in.ipb_wdata(5);
          VFE_control.eLink_active         <= ipbus_in.ipb_wdata(12 downto 8);
          VFE_control.ADC_Ped_Mux          <= ipbus_in.ipb_wdata(13);
          VFE_control.I2C_lpGBT_mode       <= ipbus_in.ipb_wdata(27);
          VFE_control.I2C_bulky_DTU        <= ipbus_in.ipb_wdata(28);
          VFE_control.DTU_auto_sync        <= ipbus_in.ipb_wdata(29);
          VFE_control.I2C_reset            <= ipbus_in.ipb_wdata(30);
          VFE_control.VFE_reset            <= ipbus_in.ipb_wdata(31);
        when "0000100" => -- Calib trigger settting
          SAFE_control.TP_duration         <= ipbus_in.ipb_wdata(15 downto 0);
          SAFE_control.TP_delay            <= '0'&ipbus_in.ipb_wdata(30 downto 16);
          SAFE_control.TP_dummyb           <= ipbus_in.ipb_wdata(31);
        when "0000101" => -- ADC lines delay settting
          SAFE_control.delay_ADC_number    <= ipbus_in.ipb_wdata(4 downto 0); -- Bit pattern for ADC delay setting (b0 to b4)
          SAFE_control.delay_tap_dir       <= ipbus_in.ipb_wdata(5);
          SAFE_control.delay_reset         <= ipbus_in.ipb_wdata(6);
          SAFE_control.IO_reset            <= ipbus_in.ipb_wdata(7);
          SAFE_control.bitslip_ADC_number  <= ipbus_in.ipb_wdata(20 downto 16);
          SAFE_control.byteslip_ADC_number <= ipbus_in.ipb_wdata(25 downto 21);
          SAFE_control.start_idelay_sync   <= ipbus_in.ipb_wdata(31 downto 27); -- Ask for Idelay tuning on ADC streams
        when "0000110" => -- Clock phases for DAQ (sequence, DDR strobe, shift register and memory access, in step of 90 degrees)
          SAFE_control.seq_clock_phase     <= ipbus_in.ipb_wdata(2 downto 0);
          SAFE_control.IO_clock_phase      <= ipbus_in.ipb_wdata(6 downto 4);
          SAFE_control.reg_clock_phase     <= ipbus_in.ipb_wdata(10 downto 8);
          SAFE_control.mem_clock_phase     <= ipbus_in.ipb_wdata(14 downto 12);
          SAFE_control.resync_clock_phase  <= ipbus_in.ipb_wdata(18 downto 16);
        when "0001100" => -- VICEPP specific stuff (clock mux setting)
          SAFE_control.VICEPP_clk_config(30 downto 0) <= ipbus_in.ipb_wdata(30 downto 0);
          SAFE_control.VICEPP_clk_config(31) <= '1';
        when "0001000" => -- Ask to read (b31=1) or write (b31=0) content of register b23..b16 in/with data b15..b0
          XADC_control.XADC_access         <= '1';
          XADC_control.XADC_WRb            <= ipbus_in.ipb_wdata(31);
          XADC_control.XADC_addr           <= ipbus_in.ipb_wdata(22 downto 16); 
          XADC_control.XADC_data           <= ipbus_in.ipb_wdata(15 downto 0); 
          wait_for_XADC_ready              <= '1';
        when "0010000" => -- Load LVRB current sensors with I2C. Control Power up anbd latch-ups
          VFE_control.I2C_Reg_data         <= ipbus_in.ipb_wdata(15 downto 0);
          VFE_control.I2C_Reg_number       <= ipbus_in.ipb_wdata(22 downto 16);
          VFE_control.I2C_Device_number    <= ipbus_in.ipb_wdata(29 downto 23);
          VFE_control.I2C_long_transfer    <= ipbus_in.ipb_wdata(30);
          VFE_control.I2C_R_Wb             <= ipbus_in.ipb_wdata(31);
          VFE_control.I2C_access_LVRB      <= '1';
          VFE_control.I2C_access_VFE       <= '0';
        when "0010001" => -- Read/Write CATIA registers with I2C
          if ipbus_in.ipb_wdata(30) = '1' then
            VFE_control.I2C_Bulk_data(0)     <= ipbus_in.ipb_wdata(15 downto 8); -- Send msB first the lsB
            VFE_control.I2C_Bulk_data(1)     <= ipbus_in.ipb_wdata(7 downto 0);
          else
            VFE_control.I2C_Bulk_data(0)     <= ipbus_in.ipb_wdata(7 downto 0); -- Only one byte. Should be first
            VFE_control.I2C_Bulk_data(1)     <= (others => '0');
          end if;
          VFE_control.I2C_Reg_data         <= ipbus_in.ipb_wdata(15 downto 0);
          VFE_control.I2C_Reg_number       <= ipbus_in.ipb_wdata(22 downto 16);
          VFE_control.I2C_Device_number    <= ipbus_in.ipb_wdata(29 downto 23);
          VFE_control.I2C_long_transfer    <= ipbus_in.ipb_wdata(30);
          VFE_control.I2C_R_Wb             <= ipbus_in.ipb_wdata(31);
          VFE_control.I2C_bulky            <= '0';
          VFE_control.I2C_access_LVRB      <= '0';
          VFE_control.I2C_access_VFE       <= '1';
          VFE_control.SPI_access_DAC       <= '0';
          wait_for_I2C_ready               <= '1';
        when "0010010" => -- Read/Write LiTE-DTU registers with I2C
          VFE_control.I2C_Reg_data         <= ipbus_in.ipb_wdata(15 downto 0);
          VFE_control.I2C_Reg_number       <= ipbus_in.ipb_wdata(22 downto 16);
          VFE_control.I2C_Bulk_data(0)     <= ipbus_in.ipb_wdata(7 downto 0);
          VFE_control.I2C_Bulk_data(1)     <= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_long_transfer    <= '0';
          VFE_control.I2C_bulky            <= '0';
          if VFE_monitor.I2C_Bulky_DTU = '1' then
            VFE_control.I2C_bulky          <= '1';
            VFE_control.I2C_Bulk_data      <= VFE_monitor.I2C_Bulk_data_ref;
            VFE_control.I2C_Bulk_length    <= to_integer(unsigned(ipbus_in.ipb_wdata(22 downto 16)));
            VFE_control.I2C_Reg_number     <= (others => '0');
          end if;
          VFE_control.I2C_Device_number    <= ipbus_in.ipb_wdata(29 downto 23);
          VFE_control.I2C_R_Wb             <= ipbus_in.ipb_wdata(31);
          VFE_control.I2C_access_LVRB      <= '0';
          VFE_control.I2C_access_VFE       <= '1'; -- With correct DTU, we use the same I2C bus as CATIA
          VFE_control.SPI_access_DAC       <= '0';
          wait_for_I2C_ready               <= '1';
        when "0010011" => -- Read/Write DAC registers with SPI
          VFE_control.SPI_DAC_data         <= ipbus_in.ipb_wdata(15 downto 0);
          VFE_control.SPI_DAC_number       <= ipbus_in.ipb_wdata(19 downto 16);
          VFE_control.SPI_DAC_action       <= ipbus_in.ipb_wdata(23 downto 20);
          VFE_control.I2C_access_LVRB      <= '0';
          VFE_control.I2C_access_VFE       <= '0';
          VFE_control.SPI_access_DAC       <= '1';
        when "0010100" => -- Analog Mux to measure Vref with XADC
          VFE_control.Vref_Mux             <= (others => '0');
-- Only one mux allowed at a time :
          if ipbus_in.ipb_wdata(0) = '1' then
            VFE_control.Vref_Mux(1)        <= '1';
          elsif ipbus_in.ipb_wdata(1) = '1' then
            VFE_control.Vref_Mux(2)        <= '1';
          elsif ipbus_in.ipb_wdata(2) = '1' then
            VFE_control.Vref_Mux(3)        <= '1';
          elsif ipbus_in.ipb_wdata(3) = '1' then
            VFE_control.Vref_Mux(4)        <= '1';
          elsif ipbus_in.ipb_wdata(4) = '1' then
            VFE_control.Vref_Mux(5)        <= '1';
          end if;
        when "0011000" => -- Start LiTE-DTU ReSync transaction
          VFE_control.ADC_ReSync_data      <= ipbus_in.ipb_wdata;
          VFE_control.ADC_Start_ReSync     <= '1';
          wait_for_DTU_ready               <= '1';
        when "0011011" => -- Idle word from DTU in sync mode
          VFE_control.DTU_Sync_pattern     <= ipbus_in.ipb_wdata(31 downto 0);
        when "0011100" => -- Start LiTE-DTU ReSync transaction
          VFE_control.ADC_ReSync_idle      <= ipbus_in.ipb_wdata(7 downto 0);
          VFE_control.ADC_Invert_data      <= ipbus_in.ipb_wdata(8);
        when "0100000" => -- I2C data for register 3 downto 0   (d03-d02-d01-d0)
          VFE_control.I2C_Bulk_data_ref(0) <= ipbus_in.ipb_wdata(7  downto 0);
          VFE_control.I2C_Bulk_data_ref(1) <= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_Bulk_data_ref(2) <= ipbus_in.ipb_wdata(23 downto 16);
          VFE_control.I2C_Bulk_data_ref(3) <= ipbus_in.ipb_wdata(31 downto 24);
        when "0100001" => -- I2C data for register 7 downto 4   (d07-d06-d05-d04)
          VFE_control.I2C_Bulk_data_ref(4) <= ipbus_in.ipb_wdata(7  downto 0);
          VFE_control.I2C_Bulk_data_ref(5) <= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_Bulk_data_ref(6) <= ipbus_in.ipb_wdata(23 downto 16);
          VFE_control.I2C_Bulk_data_ref(7) <= ipbus_in.ipb_wdata(31 downto 24);
        when "0100010" => -- I2C data for register 11 downto 8  (d11-d10-d09-d08)
          VFE_control.I2C_Bulk_data_ref(8) <= ipbus_in.ipb_wdata(7  downto 0);
          VFE_control.I2C_Bulk_data_ref(9) <= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_Bulk_data_ref(10)<= ipbus_in.ipb_wdata(23 downto 16);
          VFE_control.I2C_Bulk_data_ref(11)<= ipbus_in.ipb_wdata(31 downto 24);
        when "0100011" => -- I2C data for register 15 downto 12 (d15-d14-d13-d12)
          VFE_control.I2C_Bulk_data_ref(12)<= ipbus_in.ipb_wdata(7  downto 0);
          VFE_control.I2C_Bulk_data_ref(13)<= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_Bulk_data_ref(14)<= ipbus_in.ipb_wdata(23 downto 16);
          VFE_control.I2C_Bulk_data_ref(15)<= ipbus_in.ipb_wdata(31 downto 24);
        when "0100100" => -- I2C data for register 18 downto 16 (x00-x00-d17-d16)
          VFE_control.I2C_Bulk_data_ref(16)<= ipbus_in.ipb_wdata(7  downto 0);
          VFE_control.I2C_Bulk_data_ref(17)<= ipbus_in.ipb_wdata(15 downto 8);
          VFE_control.I2C_Bulk_data_ref(18)<= ipbus_in.ipb_wdata(23 downto 16);
        when "0111010" => -- CRC error
          SAFE_control.CRC_reset           <= ipbus_in.ipb_wdata(31);
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_ack =>
        if wait_for_XADC_ready='1' and XADC_monitor.XADC_ready='1' then
          wait_for_XADC_ready            <= '0';
        end if;
        if wait_for_DTU_ready='1'  and VFE_monitor.ReSync_busy='0' then
          wait_for_DTU_ready             <= '0';
        end if;
        if wait_for_I2C_ready='1'  and VFE_monitor.I2C_VFE_busy='0' then
          wait_for_I2C_ready             <= '0';
        end if;
        if wait_for_XADC_ready='0' and wait_for_DTU_ready='0' then
          ack <= '1';
          ipbus_rw_state                 <= ipbus_finished;
        end if;
      when ipbus_finished =>
        ack <= '0';
        ipbus_rw_state <= ipbus_rw_idle;
      end case;
    end if;
  end process reg;    

  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';

-- XADC stuff
  XADC_inst : XADC
  generic map
  (
-- INIT_40 - INIT_42: XADC configuration registers
    INIT_40 => X"b000", -- No calibration average, average data on 256 samples, Unipolar, Continuous conversion, multiple channels
    INIT_41 => X"2000", -- Sequence mode, independant ADCs, No calibration, alarm On
    INIT_42 => X"0200", -- ACLK = DCLK/2 = 40MHz / 2 = 20 MHz 
-- INIT_48 - INIT_4F: Sequence Registers
    INIT_48 => X"47e0", -- CHSEL1 - VccBRAM, VccAux, VccInt, On-chip Temp, VccoDDR, VccPAux, VccPInt
    INIT_49 => XADC_VAUX_PATTERN, -- CHSEL2 - Vaux9 (GPIO3), Vaux3 (GPIO2), Vaux1(CATIA_temp), Vaux0(APD temp) : 0000 0010 0000 1011
    INIT_4A => X"47e0", -- SEQAVG1 : same as 0x48
    INIT_4B => X"0003", -- SEQAVG2 : same as 0x49
    INIT_4C => X"0000", -- SEQINMODE0 - All internal channels are unipolar
    INIT_4D => X"0000", -- SEQINMODE1 - All Vaux channels are unipolar
    INIT_4E => X"0000", -- SEQACQ0 - No extra settling time all channels
    INIT_4F => X"0000", -- SEQACQ1 - No extra settling time all channels
-- INIT_50 - INIT_58, INIT5C: Alarm Limit Registers
    INIT_50 => X"b5ed", -- Temp upper alarm trigger 85°C
    INIT_51 => X"5999", -- Vccint upper alarm limit 1.05V
    INIT_52 => X"A147", -- Vccaux upper alarm limit 1.89V
    INIT_53 => X"dddd", -- OT upper alarm limit 125°C - see Thermal Management
    INIT_54 => X"a93a", -- Temp lower alarm reset 60°C
    INIT_55 => X"5111", -- Vccint lower alarm limit 0.95V
    INIT_56 => X"91Eb", -- Vccaux lower alarm limit 1.71V
    INIT_57 => X"ae4e", -- OT lower alarm reset 70°C - see Thermal Management
    INIT_58 => X"5999", -- VCCBRAM upper alarm limit 1.05V
    INIT_5C => X"5111", -- VCCBRAM lower alarm limit 0.95V

-- Simulation attributes: Set for proper simulation behavior
    SIM_DEVICE       => "7SERIES",    -- Select target device (values)
    SIM_MONITOR_FILE => "design.txt"  -- Analog simulation data file name
 )
 port map
 (
-- ALARMS: 8-bit (each) output: ALM, OT
    ALM          => open,             -- 8-bit output: Output alarm for temp, Vccint, Vccaux and Vccbram
    OT           => open,             -- 1-bit output: Over-Temperature alarm
-- STATUS: 1-bit (each) output: XADC status ports
    BUSY         => open,             -- 1-bit output: ADC busy output
    CHANNEL      => channel,          -- 5-bit output: Channel selection outputs
    EOC          => XADC_eoc,         -- 1-bit output: End of Conversion
    EOS          => XADC_eos,         -- 1-bit output: End of Sequence
    JTAGBUSY     => open,             -- 1-bit output: JTAG DRP transaction in progress output
    JTAGLOCKED   => open,             -- 1-bit output: JTAG requested DRP port lock
    JTAGMODIFIED => open,             -- 1-bit output: JTAG Write to the DRP has occurred
    MUXADDR      => open,          -- 5-bit output: External MUX channel decode
    
-- Auxiliary Analog-Input Pairs: 16-bit (each) input: VAUXP[15:0], VAUXN[15:0]
    VAUXN        => vauxn,            -- 16-bit input: N-side auxiliary analog input
    VAUXP        => vauxp,            -- 16-bit input: P-side auxiliary analog input
    
-- CONTROL and CLOCK: 1-bit (each) input: Reset, conversion start and clock inputs
    CONVST       => '0',              -- 1-bit input: Convert start input
    CONVSTCLK    => '0',              -- 1-bit input: Convert start input
    RESET        => '0',              -- 1-bit input: Active-high reset
    
-- Dedicated Analog Input Pair: 1-bit (each) input: VP/VN
    VN           => '0', -- 1-bit input: N-side analog input
    VP           => '0', -- 1-bit input: P-side analog input
      
-- Dynamic Reconfiguration Port (DRP)
    DO           => loc_XADC_data,
    DRDY         => XADC_monitor.XADC_ready,
    DADDR        => XADC_control.XADC_addr,
    DCLK         => XADC_clk,
    DEN          => XADC_control.XADC_access,
    DI           => XADC_control.XADC_data,
    DWE          => XADC_control.XADC_WRb
  );
  XADC_clk <= not clk;
  MUX : if not USE_PED_MUX generate
    vauxp(0) <= APD_temp_in;
    vauxn(0) <= APD_temp_ref;
  end generate;
  
  vauxp(1) <= CATIA_temp_in;
  vauxn(1) <= CATIA_temp_ref;
  vauxp(2) <= Vdummy_in;
  vauxn(2) <= Vdummy_ref;
 
  LVRB: if USE_LVRB generate
    vauxp(3) <= V2P5_in;
    vauxn(3) <= V2P5_ref;
    vauxp(9) <= V1P2_in;
    vauxn(9) <= V1P2_ref;
  end generate;

  XADC_eos_del <= XADC_eos when rising_edge(clk) else XADC_eos_del;
  seq_counter : process (reset, clk, XADC_eos, XADC_eos_del) is
  begin  -- process reg
    if reset = '1' then
      total_seq <= (others => '0');
    elsif rising_edge(clk) then
      if XADC_eos='1' and XADC_eos_del='0' then
        total_seq <= total_seq+1;
      end if;
    end if;
  end process seq_counter;
  
  conv_counter : process (reset, clk) is
  begin
    if reset = '1' then
      total_conv <= (others => '0');
    elsif rising_edge(clk) then
      XADC_eoc_del <= XADC_eoc;
      if XADC_eoc='1' and XADC_eoc_del='0' then
        total_conv <= total_conv+1;
      end if;
    end if;
  end process conv_counter;

  access_counter : process (reset, clk) is
  begin
    if reset = '1' then
      total_access <= (others => '0');
    elsif rising_edge(clk) then
      XADC_ready_del <= XADC_monitor.XADC_ready;
      if XADC_monitor.XADC_ready='1' and XADC_ready_del='0' then
        XADC_monitor.XADC_data <= loc_XADC_data;
        total_access <= total_access+1;
      end if;
    end if;
  end process access_counter;
  XADC_monitor.XADC_addr <= XADC_control.XADC_addr;

end architecture rtl;
