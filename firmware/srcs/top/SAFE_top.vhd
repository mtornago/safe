-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 1412921 Wed Nov 18 09:44:32 MST 2015
-- Date        : Mon Apr 25 14:31:29 2016
-- Host        : volta running 64-bit Debian GNU/Linux testing/unstable
-- Command     : write_vhdl -mode pin_planning -force -port_diff_buffers
--               /home/dan/work/CMS/cms-ecal-vfe-adapter/SAFE/testing/option2_484/io_1.vhd
-- Design      : ios
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.SAFE_IO.all;

entity SAFE is
  generic (
    -- Temporary trick to generate weird clock from FPGA (need to change also xdc file to get LVDS_25)
    USE_GPIO_WITH_DBG : boolean := (not USE_DAC) and (not USE_LVRB);
    USE_GPIO_WITH_LVR : boolean := USE_LVRB;
    USE_GPIO_WITH_DAC : boolean := USE_DAC
  );

  port (
  ch1_in_P          : in    std_logic;
  ch1_in_N          : in    std_logic;
  ch1_out_P         : out   std_logic;
  ch1_out_N         : out   std_logic;
  ch2_in_P          : in    std_logic;
  ch2_in_N          : in    std_logic;
  ch2_out_P         : out   std_logic;
  ch2_out_N         : out   std_logic;
  ch3_in_P          : in    std_logic;
  ch3_in_N          : in    std_logic;
  ch3_out_P         : out   std_logic;
  ch3_out_N         : out   std_logic;
  ch4_in_P          : in    std_logic;
  ch4_in_N          : in    std_logic;
  ch4_out_P         : out   std_logic;
  ch4_out_N         : out   std_logic;
  ch5_in_P          : in    std_logic;
  ch5_in_N          : in    std_logic;
  ch5_out_P         : out   std_logic;
  ch5_out_N         : out   std_logic;
  SEUA_in           : inout std_logic_vector(5 downto 1);
  SEUD_in           : out   std_logic_vector(5 downto 1);
  PllLock_in        : inout std_logic_vector(5 downto 1);
  CalBusy_in        : in    std_logic_vector(5 downto 1);
  OVF_in            : in    std_logic_vector(5 downto 1);
  Pwup_reset_out    : out   std_logic;
  ReSync_out_P      : out   std_logic;
  ReSync_out_N      : out   std_logic;
  VFE_IO            : inout std_logic;
  APD_temp_ref      : in    std_logic;
  CATIA_temp_in     : in    std_logic;
  CATIA_temp_ref    : in    std_logic;
  I2C_sda_in        : inout std_logic;
  I2C_scl_in        : inout std_logic;
  TP_trigger_out    : out   std_logic;
  Calib_mode_out    : out   std_logic;
  Test_mode_out     : out   std_logic;

  SEUA_out          : out   std_logic_vector(5 downto 1);
  SEUD_out          : out   std_logic_vector(5 downto 1);
  PllLock_out       : out   std_logic_vector(5 downto 1);
  CalBusy_out       : out   std_logic_vector(5 downto 1);
  OVF_out           : out   std_logic_vector(5 downto 1);
  ReSync_in_P       : in    std_logic;
  ReSync_in_N       : in    std_logic;

--  Test_mode_in      : in    std_logic;
--  Pwup_reset_in     : in    std_logic;
--  Calib_trigger_in  : in    std_logic;
--  APD_temp_out      : out   std_logic;
--  CATIA_temp_out    : out   std_logic;
--  Calib_mode_in     : in    std_logic;
  Ctrl_in           : in  std_logic_vector(3 downto 1);
  Ctrl_ref          : in  std_logic_vector(3 downto 1);
  Ctrl_out          : out std_logic_vector(4 downto 1);

  I2C_sda_VFE       : inout std_logic;
  I2C_scl_VFE       : inout std_logic;
--  I2C_sda_LVRB      : inout std_logic;
--  I2C_scl_LVRB      : inout std_logic;
--  SPI_data_DAC      : out   std_logic;
--  SPI_strobe_DAC    : out   std_logic;
--  SPI_csb_DAC       : out   std_logic;

  HP_CLK_160_P      : in    std_logic;
  HP_CLK_160_N      : in    std_logic;
  CLK_160_P         : in    std_logic;
  CLK_160_N         : in    std_logic;
  CLK_select        : out   std_logic;

  GbE_refclk_N      : in  std_logic;
  GbE_refclk_P      : in  std_logic;
  GbE_RXN           : in  std_logic;
  GbE_RXP           : in  std_logic;
  GbE_TXN           : out std_logic;
  GbE_TXP           : out std_logic;

  SFP_Present       : in    std_logic;
  SFP_LOS           : in    std_logic;
  SFP_Tx_Fault      : in    std_logic;
  SFP_SCL           : out   std_logic;
  SFP_SDA           : inout std_logic;
  SFP_Rate          : out   std_logic;
  EQEN_25           : out   std_logic;

  LED               : out   std_logic_vector(3 downto 0);
  GPIO              : inout std_logic_vector(5 downto 0);
--  GPIO              : out   std_logic_vector(5 downto 0);
  ADDR              : in    std_logic_vector(7 downto 0);   -- Pins opened on FPGA. Pulled down = "0000"
  Switch            : in    std_logic_vector(3 downto 0);
--  V2P5_in           : in    std_logic;
--  V2P5_ref          : in    std_logic;
--  V1P2_in           : in    std_logic;
--  V1P2_ref          : in    std_logic
  );
end SAFE;

architecture rtl of SAFE is

component clk_generator_iserdes is
  port (
    clk_160_in      : in  std_logic;
    clk_20          : out std_logic;
    clk_40          : out std_logic;
    clk_160         : out std_logic;
    clk_640         : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_iserdes;

component clk_generator_delay_ctrl is
  port (
    clk_160_in      : in  std_logic;
    clk_delay_ctrl  : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_delay_ctrl;

component gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p             : in  std_logic;
    gtrefclk_n             : in  std_logic;
    gtrefclk_out           : out std_logic;
    gtrefclk_bufg_out      : out std_logic;
    txp                    : out std_logic;
    txn                    : out std_logic;
    rxp                    : in  std_logic;
    rxn                    : in  std_logic;
    resetdone              : out std_logic;
    userclk_out            : out std_logic;
    userclk2_out           : out std_logic;
    rxuserclk_out          : out std_logic;
    rxuserclk2_out         : out std_logic;
    pma_reset_out          : out std_logic;
    mmcm_locked_out        : out std_logic;
    independent_clock_bufg : in  std_logic;
    gmii_txd               : in  std_logic_vector(7 downto 0);
    gmii_tx_en             : in  std_logic;
    gmii_tx_er             : in  std_logic;
    gmii_rxd               : out std_logic_vector(7 downto 0);
    gmii_rx_dv             : out std_logic;
    gmii_rx_er             : out std_logic;
    gmii_isolate           : out std_logic;
    configuration_vector   : in  std_logic_vector(4 downto 0);
    status_vector          : out std_logic_vector(15 downto 0);
    reset                  : in  std_logic;
    signal_detect          : in  std_logic;
    gt0_qplloutclk_out     : out std_logic;
    gt0_qplloutrefclk_out  : out std_logic
);
end component gig_ethernet_pcs_pma_0;

component tri_mode_ethernet_mac_0 is
  port (
    gtx_clk                 : in  std_logic;
      -- asynchronous reset
    glbl_rstn               : in  std_logic;
    rx_axi_rstn             : in  std_logic;
    tx_axi_rstn             : in  std_logic;
    rx_statistics_vector    : out std_logic_vector(27 downto 0);
    rx_statistics_valid     : out std_logic;
    rx_mac_aclk             : out std_logic;
    rx_reset                : out std_logic;
    rx_axis_mac_tdata       : out std_logic_vector(7 downto 0);
    rx_axis_mac_tvalid      : out std_logic;
    rx_axis_mac_tlast       : out std_logic;
    rx_axis_mac_tuser       : out std_logic;
    tx_ifg_delay            : in  std_logic_vector(7 downto 0);

    tx_statistics_vector    : out std_logic_vector(31 downto 0);
    tx_statistics_valid     : out std_logic;
    tx_mac_aclk             : out std_logic;
    tx_reset                : out std_logic;
    tx_axis_mac_tdata       : in  std_logic_vector(7 downto 0);
    tx_axis_mac_tvalid      : in  std_logic;
    tx_axis_mac_tlast       : in  std_logic;
    tx_axis_mac_tuser       : in  std_logic_vector(0 downto 0);
    tx_axis_mac_tready      : out std_logic;
    pause_req               : in  std_logic;
    pause_val               : in  std_logic_vector(15 downto 0);
    speedis100              : out std_logic;
    speedis10100            : out std_logic;
--    rx_enable                  => rx_enable,
--    tx_enable                  => tx_enable,
--    tx_ifg_delay               => tx_ifg_delay,
--    gmii_tx_clk                => gmii_tx_clk,
--    gmii_rx_clk                => gmii_rx_clk,
--    mii_tx_clk                 => mii_tx_clk,

    gmii_txd                : out std_logic_vector(7 downto 0);
    gmii_tx_en              : out std_logic;
    gmii_tx_er              : out std_logic;
    gmii_rxd                : in  std_logic_vector(7 downto 0);
    gmii_rx_dv              : in  std_logic;
    gmii_rx_er              : in  std_logic;
    rx_configuration_vector : in  std_logic_vector(79 downto 0);
    tx_configuration_vector : in  std_logic_vector(79 downto 0));
end component tri_mode_ethernet_mac_0;

component ipbus_ctrl is
  generic(
    MAC_CFG       : ipb_mac_cfg                   := EXTERNAL;
    IP_CFG        : ipb_ip_cfg                    := EXTERNAL;
    BUFWIDTH      : natural                       := 4;
    INTERNALWIDTH : natural                       := 1;
    ADDRWIDTH     : natural                       := 11;
    IPBUSPORT     : std_logic_vector(15 downto 0) := x"C351";
    SECONDARYPORT : std_logic                     := '0';
    N_OOB         : natural                       := 0
  );
  port (
    mac_clk      : in  std_logic;
    rst_macclk   : in  std_logic;
    ipb_clk      : in  std_logic;
    rst_ipb      : in  std_logic;
    mac_rx_data  : in  std_logic_vector(7 downto 0);
    mac_rx_valid : in  std_logic;
    mac_rx_last  : in  std_logic;
    mac_rx_error : in  std_logic;
    mac_tx_data  : out std_logic_vector(7 downto 0);
    mac_tx_valid : out std_logic;
    mac_tx_last  : out std_logic;
    mac_tx_error : out std_logic;
    mac_tx_ready : in  std_logic;
    ipb_out      : out ipb_wbus;
    ipb_in       : in  ipb_rbus;
    ipb_req      : out std_logic;
    ipb_grant    : in  std_logic                                := '1';
    mac_addr     : in  std_logic_vector(47 downto 0)            := X"000000000000";
    ip_addr      : in  std_logic_vector(31 downto 0)            := X"00000000";
    enable       : in  std_logic                                := '1';
    RARP_select  : in  std_logic                                := '0';
    pkt          : out std_logic;
    pkt_oob      : out std_logic;
    oob_in       : in  ipbus_trans_in_array(N_OOB - 1 downto 0) := (others => ('0', X"00000000", '0'));
    oob_out      : out ipbus_trans_out_array(N_OOB - 1 downto 0)
	);
end component ipbus_ctrl;

component slaves is
  port (
    pwup_rst      : in  std_logic;
    DAQ_busy      : out std_logic;
    ipb_clk       : in  std_logic;
    ipb_in        : in  ipb_wbus;
    ipb_out       : out ipb_rbus;
    SAFE_Monitor  : in  SAFE_Monitor_t;
    SAFE_Control  : out SAFE_Control_t;
    VFE_Monitor   : inout VFE_Monitor_t;
    VFE_Control   : out VFE_Control_t;
    clk_40        : in  std_logic;
    clk_160       : in  std_logic;
    clk_shift_reg : in  std_logic;
    clk_memory    : in  std_logic;
    ch1_in        : in  std_logic_vector(31 downto 0);
    ch2_in        : in  std_logic_vector(31 downto 0);
    ch3_in        : in  std_logic_vector(31 downto 0);
    ch4_in        : in  std_logic_vector(31 downto 0);
    ch5_in        : in  std_logic_vector(31 downto 0);
    ch1_out       : out std_logic_vector(31 downto 0);
    ch2_out       : out std_logic_vector(31 downto 0);
    ch3_out       : out std_logic_vector(31 downto 0);
    ch4_out       : out std_logic_vector(31 downto 0);
    ch5_out       : out std_logic_vector(31 downto 0);
    trig_local    : out std_logic;
    trig_signal   : in  std_logic;
    CRC_reset     : in  std_logic;
    CRC_error     : out UInt32_t(5 downto 1);
    APD_temp_in   : in  std_logic;
    APD_temp_ref  : in  std_logic;
    Vdummy_in     : in  std_logic;
    Vdummy_ref    : in  std_logic;
    CATIA_temp_in : in  std_logic;
    CATIA_temp_ref: in  std_logic;
    V2P5_in       : in  std_logic;
    V2P5_ref      : in  std_logic;
    V1P2_in       : in  std_logic;
    V1P2_ref      : in  std_logic;
    Switch        : in  std_logic_vector(3 downto 0)
  );
end component slaves;

signal gmii_txd            : std_logic_vector(7 downto 0);
signal gmii_tx_en          : std_logic;
signal gmii_tx_er          : std_logic;
signal gmii_rxd            : std_logic_vector(7 downto 0);
signal gmii_rx_dv          : std_logic;
signal gmii_rx_er          : std_logic;
signal GbE_user_clk        : std_logic;
signal fake_packet_counter : integer range 0 to 1536 := 0;

signal mac_tx_data, mac_rx_data                                                         : std_logic_vector(7 downto 0);
signal mac_tx_valid, mac_tx_last, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
signal mac_tx_error                                                                     : std_logic_vector(0 downto 0);
signal ipb_master_out                                                                   : ipb_wbus;
signal ipb_master_in                                                                    : ipb_rbus;
signal mac_addr                                                                         : std_logic_vector(47 downto 0);
signal ip_addr                                                                          : std_logic_vector(31 downto 0);
signal pkt, pkt_oob                                                                     : std_logic;
signal frame_size              : std_logic_vector(15 downto 0);
signal rx_configuration_vector : std_logic_vector(79 downto 0);
signal tx_configuration_vector : std_logic_vector(79 downto 0);

component LVRB_Ctrl is
  port (
    SAFE_Control             : in     SAFE_Control_t;
    VFE_Control              : in     VFE_Control_t;
    LVRB_Busy                : out    std_logic;
    I2C_Reg_data_LVRB        : out    std_logic_vector(15 downto 0);
    I2C_Scan_Fault_LVRB_2V5  : buffer std_logic_vector(7 downto 0);
    I2C_Scan_ISense_LVRB_2V5 : out    std_logic_vector(15 downto 0);
    I2C_Scan_VSense_LVRB_2V5 : out    std_logic_vector(15 downto 0);
    I2C_Scan_Fault_LVRB_1V2  : buffer std_logic_vector(7 downto 0);
    I2C_Scan_ISense_LVRB_1V2 : out    std_logic_vector(15 downto 0);
    I2C_Scan_VSense_LVRB_1V2 : out    std_logic_vector(15 downto 0);
    reset                    : in     std_logic;
    I2C_scl                  : inout  std_logic;
    I2C_sda                  : inout  std_logic;
    I2C_clk                  : in     std_logic
  );
end component LVRB_ctrl;

component DAC_Ctrl is
  port (
    SPI_Access_DAC      : in    std_logic;
    SPI_DAC_data        : in    std_logic_vector(15 downto 0);
    SPI_DAC_number      : in    std_logic_vector(3 downto 0);
    SPI_DAC_action      : in    std_logic_vector(3 downto 0);
    reset               : in    std_logic;
    SPI_clk             : in    std_logic;
    SPI_csb             : out   std_logic;
    SPI_data            : out   std_logic;
    SPI_strobe          : out   std_logic
  );
end component DAC_ctrl;

--Array mappings to simplify instances
type slv32_array_t is array (integer range <>) of std_logic_vector(31 downto 0);
signal captured_stream : slv32_array_t(5 downto 1);
signal output_stream   : slv32_array_t(5 downto 1);

signal loc_clk_in          : std_logic := '0';
signal Clk_ipbus           : std_logic := '0';
signal Clk_Gb_eth          : std_logic := '0';
signal Clk_I2C             : std_logic := '0';
signal System_clk          : std_logic := '0';
signal Sequence_clk        : std_logic := '0';
signal shift_reg_clk       : std_logic := '0';
signal memory_clk          : std_logic := '0';
signal Resync_clk          : std_logic := '0';
signal Clk_delay_ctrl      : std_logic := '0';
signal Clk_delay_ctrl_loc  : std_logic := '0';
signal iserdes_clock_locked: std_logic := '0';
signal delay_clock_locked  : std_logic := '0';
signal MMCM_locked         : std_logic := '0';
signal delay_locked_debug  : std_logic := '0';

signal HP_Clk_in           : std_logic := '0';
signal HP_Clk_160          : std_logic := '0';
signal HP_Clk_20           : std_logic := '0';
signal HP_Clk_40           : std_logic := '0';
signal HP_Clk_640          : std_logic := '0';

signal I2C_data_LVRB       : std_logic_vector(15 downto 0) := (others => '0');
signal I2C_data_VFE        : std_logic_vector(15 downto 0) := (others => '0');
signal dbg_i2c_clk         : std_logic;
signal dbg_sda             : std_logic;
signal dbg_scl             : std_logic;

--Monitoring
signal SAFE_Monitor           : SAFE_Monitor_t;
signal SAFE_Control           : SAFE_Control_t  := DEFAULT_SAFE_Control;
signal VFE_Monitor            : VFE_Monitor_t;
signal VFE_Control            : VFE_Control_t   := DEFAULT_VFE_Control;
signal led_counter            : unsigned(31 downto 0) := (others => '0');
signal pwup_reset             : std_logic := '1';
signal VFE_reset              : std_logic := '0'; -- Pwup_resetb sent to VFE
signal SAFE_reset             : std_logic := '0';
signal CLK_reset              : std_logic := '0';
signal Xpoint_ready           : std_logic := '0';
signal trig_signal            : std_logic := '0'; -- Single event trigger
signal trig_software          : std_logic := '0'; -- single event trigger received on ipbus
signal trig_sw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_out               : std_logic := '0'; -- output signal generated from trig_software after some delay
signal trig_in                : std_logic := '0'; -- single event  trigger receives on GPIO pin
signal trig_hardware          : std_logic := '0'; -- input signal generated from trig_hardware after some delay
signal trig_hw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_local             : std_logic := '0'; -- trigger signal generated looking at input data (required trig_self=1)
signal trig_local_outb        : std_logic := 'Z'; -- trigger signal generated looking at input data (required trig_self=1) sent to FE
signal trig_in_prev1          : std_logic := '0';
signal trig_in_prev2          : std_logic := '0';
signal trigger_prev1          : std_logic := '0';
signal trigger_prev2          : std_logic := '0';
signal delay_out_counter      : unsigned(15 downto 0) := x"0000";
signal start_out_counter      : std_logic := '0';
signal delay_in_counter       : unsigned(15 downto 0) := x"0000";
signal start_in_counter       : std_logic := '0';
signal gen_trigger            : std_logic := '0';
signal DAQ_busy               : std_logic := '0';
signal BC0                    : std_logic := '0';
signal ipbus_counter          : std_logic_vector(2 downto 0) := "000";
signal clk_counter            : unsigned(13 downto 0) := (others => '0');

signal SEUA_loc               : std_logic_vector(5 downto 1) := (others => '0');
signal SEUD_loc               : std_logic_vector(5 downto 1) := (others => '0');
signal G10_calib_pulse        : std_logic := '0';
signal G10_trigger_prev1      : std_logic := '0';
signal G10_trigger_prev2      : std_logic := '0';
signal start_G10_delay        : std_logic := '0';
signal G10_duration_counter   : unsigned(15 downto 0) := x"0000";
signal G10_delay_counter      : unsigned(15 downto 0) := x"0000";
signal G10_DAQ_start          : std_logic := '0';
signal G10_DAQ_start_width    : unsigned(1 downto 0) := "00";
signal G1_calib_pulse         : std_logic := '0';
signal G1_trigger_prev1       : std_logic := '0';
signal G1_trigger_prev2       : std_logic := '0';
signal start_G1_delay         : std_logic := '0';
signal G1_duration_counter    : unsigned(15 downto 0) := x"0000";
signal G1_delay_counter       : unsigned(15 downto 0) := x"0000";
signal G1_DAQ_start           : std_logic := '0';
signal G1_DAQ_start_width     : unsigned(1 downto 0) := "00";

signal ADC_start_Resync       : std_logic := '0';
signal ADC_start_Resync_del   : std_logic := '0';
signal WADC_start_Resync       : std_logic := '0';
signal WTP_trigger            : std_logic := '0';
signal TP_trigger             : std_logic := '0';
signal TP_trigger_merged      : std_logic := '0';
signal TP_trigger_prev        : std_logic := '0';
signal start_TP_delay         : std_logic := '0';
signal TP_duration_counter    : unsigned(15 downto 0) := x"0000";
signal TP_delay_counter       : unsigned(15 downto 0) := x"0000";
signal TP_DAQ_start           : std_logic := '0';
signal TP_DAQ_start_width     : unsigned(1 downto 0) := "00";

signal do_IDELAY_sync         : std_logic := '0';
signal start_idelay_sync_or   : std_logic := '0';
signal idelay_sync_counter    : unsigned(15 downto 0) := (others => '0');
signal CalBusy_delayed        : std_logic := '0';
signal CalBusy_or             : std_logic := '0';
signal CalBusy_delay          : unsigned(5 downto 0) := (others => '1');
signal CalBusy_delay_counter  : unsigned(5 downto 0) := (others => '0');

signal sig_monitor            : std_logic_vector(3 downto 0) := "0000";
signal FE_trigger             : std_logic := '0';
signal FE_TP_trigger          : std_logic := '0';
signal FE_veto                : std_logic := '0';

signal ReSync_out             : std_logic := '0';
signal ReSync_out_R           : std_logic := '0';
signal ReSync_out_F           : std_logic := '0';
signal ReSync_out_loc         : std_logic := '0';

signal Vdummy_in              : std_logic;
signal Vdummy_ref             : std_logic;
signal APD_Temp_in            : std_logic;
signal V2P5_in                : std_logic;
signal V2P5_ref               : std_logic;
signal V1P2_in                : std_logic;
signal V1P2_ref               : std_logic;
signal I2C_sda_LVRB           : std_logic;
signal I2C_scl_LVRB           : std_logic;
signal SPI_data_DAC           : std_logic;
signal SPI_strobe_DAC         : std_logic;
signal SPI_csb_DAC            : std_logic;

attribute mark_debug : string;
attribute keep : string;
attribute mark_debug of XPoint_ready : signal is "true";
attribute mark_debug of MMCM_locked  : signal is "true";

begin

inst_APD_Temp : if not USE_PED_MUX generate
  APD_Temp_in <= VFE_IO;
end generate;
inst_Mux : if USE_PED_MUX generate
  VFE_IO      <= VFE_control.ADC_Ped_Mux;
  APD_temp_in <= '0';
end generate;

--Mux_Calib_iobuf_g    : IOBUF  port map (IO => VFE_IO,     I => VFE_monitor.ADC_Ped_Mux, O=> APD_Temp_in, T => not SAFE_control.calib_mux_enabled);     -- Drive extenal MUX for ADC calibration
Mux_Calib_iobuf_1    : IOBUF  port map (IO => SEUD_in(1), I => VFE_monitor.Vref_MUX(1), O=> SEUD_loc(1), T => not SAFE_control.calib_mux_enabled); -- Drive extenal MUX for Vref measurement
Mux_Calib_iobuf_2    : IOBUF  port map (IO => SEUD_in(2), I => VFE_monitor.Vref_MUX(2), O=> SEUD_loc(2), T => not SAFE_control.calib_mux_enabled);
Mux_Calib_iobuf_3    : IOBUF  port map (IO => SEUD_in(3), I => VFE_monitor.Vref_MUX(3), O=> SEUD_loc(3), T => not SAFE_control.calib_mux_enabled);
Mux_Calib_iobuf_4    : IOBUF  port map (IO => SEUD_in(4), I => VFE_monitor.Vref_MUX(4), O=> SEUD_loc(4), T => not SAFE_control.calib_mux_enabled);
Mux_Calib_iobuf_5    : IOBUF  port map (IO => SEUD_in(5), I => VFE_monitor.Vref_MUX(5), O=> SEUD_loc(5), T => not SAFE_control.calib_mux_enabled);

  Xpoint_ready   <= '1';

inst_CLK_DEBUG: if USE_GPIO_WITH_DBG generate
  --clk_test_OBUF : OBUFDS port map (O => GPIO(0), OB => GPIO(1), I => clk_160);
  GPIO(0) <= 'Z';
  GPIO(1) <= 'Z';
  GPIO(2) <= trig_hardware;
--  GPIO(3) <= trig_software;
  GPIO(3) <= MMCM_locked;
  GPIO(4) <= 'Z';
  GPIO(5) <= 'Z';
end generate;

inst_LVR: if USE_GPIO_WITH_LVR generate
--  GPIO_0_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(0), I => I2C_sda_LVRB);
--  GPIO_1_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(1), I => I2C_scl_LVRB);
--  GPIO_2_ibuf   : IBUF  port map (I => GPIO(2), O => V1P2_in);
--  GPIO_2R_ibuf  : IBUF  port map (I => GPIO(4), O => V1P2_ref);
--  GPIO_3_ibuf   : IBUF  port map (I => GPIO(3), O => V2P5_in);
--  GPIO_3R_ibuf  : IBUF  port map (I => GPIO(5), O => V2P5_ref);
  GPIO(0) <= I2C_sda_LVRB;
  GPIO(1) <= I2C_scl_LVRB;
  GPIO(2) <= V1P2_in;
  GPIO(4) <= V1P2_ref;
  GPIO(3) <= V2P5_in;
  GPIO(5) <= V2P5_ref;

  Inst_LVRB_Ctrl : LVRB_Ctrl
  port map (
    SAFE_control             => SAFE_control,
    VFE_control              => VFE_control,
    LVRB_Busy                => VFE_monitor.I2C_LVRB_Busy,
    I2C_Reg_data_LVRB        => VFE_monitor.I2C_Reg_data_LVRB,
    I2C_Scan_Fault_LVRB_2V5  => VFE_monitor.I2C_Scan_Fault_LVRB_2V5,
    I2C_Scan_ISense_LVRB_2V5 => VFE_monitor.I2C_Scan_ISense_LVRB_2V5,
    I2C_Scan_VSense_LVRB_2V5 => VFE_monitor.I2C_Scan_VSense_LVRB_2V5,
    I2C_Scan_Fault_LVRB_1V2  => VFE_monitor.I2C_Scan_Fault_LVRB_1V2,
    I2C_Scan_ISense_LVRB_1V2 => VFE_monitor.I2C_Scan_ISense_LVRB_1V2,
    I2C_Scan_VSense_LVRB_1V2 => VFE_monitor.I2C_Scan_VSense_LVRB_1V2,
    reset                    => SAFE_reset,
    I2C_clk                  => clk_I2C,
    I2C_scl                  => I2C_scl_LVRB,
    I2C_sda                  => I2C_sda_LVRB
  );
  VFE_monitor.I2C_n_ack_LVRB           <= (others => '0');
  VFE_monitor.I2C_LVRB_error           <= '0';
end generate;
inst_noLVR: if not USE_GPIO_WITH_LVR generate
  VFE_monitor.I2C_LVRB_Busy <= '0';
  VFE_monitor.I2C_Reg_data_LVRB <= (others => '0');
  VFE_monitor.I2C_Scan_Fault_LVRB_2V5  <= (others => '0');
  VFE_monitor.I2C_Scan_ISense_LVRB_2V5 <= (others => '0');
  VFE_monitor.I2C_Scan_VSense_LVRB_2V5 <= (others => '0');
  VFE_monitor.I2C_Scan_Fault_LVRB_1V2  <= (others => '0');
  VFE_monitor.I2C_Scan_ISense_LVRB_1V2 <= (others => '0');
  VFE_monitor.I2C_Scan_VSense_LVRB_1V2 <= (others => '0');
  VFE_monitor.I2C_n_ack_LVRB           <= (others => '0');
  VFE_monitor.I2C_LVRB_error           <= '0';
  V2P5_in                              <= '0';
  V2P5_ref                             <= '0';
  V1P2_in                              <= '0';
  V1P2_ref                             <= '0';
end generate;

inst_DAC: if USE_GPIO_WITH_DAC generate
--  GPIO_0_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(0), I => SPI_data_DAC);
--  GPIO_1_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(1), I => SPI_strobe_DAC);
--  GPIO_2_obuf   : OBUF  generic map (DRIVE => 8, SLEW => "SLOW")
--                        port map (O => GPIO(2), I => SPI_csb_DAC);
  GPIO(0) <= SPI_data_DAC;
  GPIO(1) <= SPI_strobe_DAC;
  GPIO(2) <= SPI_csb_DAC;
  GPIO(3) <= 'Z';
  GPIO(4) <= 'Z';
  GPIO(5) <= 'Z';

  Inst_DAC_Ctrl : DAC_Ctrl
  port map (
    SPI_Access_DAC      => VFE_control.SPI_Access_DAC,
    SPI_DAC_data        => VFE_control.SPI_DAC_data,
    SPI_DAC_number      => VFE_control.SPI_DAC_number,
    SPI_DAC_action      => VFE_control.SPI_DAC_action,
    reset               => SAFE_reset,
    SPI_clk             => clk_I2C,
    SPI_csb             => SPI_csb_DAC,
    SPI_data            => SPI_data_DAC,
    SPI_strobe          => SPI_strobe_DAC
  );
end generate;
--  Calib_ADC       <= SAFE_monitor.ADC_calib_mode;
--  I2C_sRst        <= '0';
--  I2C_SM_aRstb    <= '1';
  Test_mode_out                    <= VFE_Monitor.ADC_test_mode;
  Calib_mode_out                   <= '0';
  CLK_select                       <= '1';
  SEUA_out                         <= (others => '0');
  SEUD_out                         <= (others => '0');
  PllLock_out                      <= (others => '0');
  CalBusy_out                      <= (others => '0');
  OVF_out                          <= (others => '0');

--  reset                            <= pwup_reset or SAFE_Monitor.reset;
  VFE_reset                        <= Pwup_reset or VFE_monitor.VFE_reset;
  Pwup_reset_out                   <= not VFE_reset;
  CLK_reset                        <= not Xpoint_ready;
  SAFE_reset                       <= Pwup_reset or SAFE_Monitor.reset;
  
  gen_pwup_reset : process (system_clk)
  variable counter : unsigned(15 downto 0) := (others => '0');
  begin  -- process test_counter
--    if system_clock_locked = '0' then
--      counter    := (others => '0');
    if rising_edge(system_clk) then
      pwup_reset <= '1';
      if counter =  x"FFFF" then
        pwup_reset <= '0';
      else
        counter := counter + 1;
      end if;
    end if;
  end process gen_pwup_reset;

--  DCIRESET_inst : DCIRESET
--  port map (
--    LOCKED => SAFE_monitor.DCI_locked, -- 1-bit output: LOCK status output
--    RST    => pwup_reset               -- 1-bit input: Active-high asynchronous reset input
--  );
  SAFE_monitor.DCI_locked         <= '1';

-- SAFE monitoring :
  SAFE_Monitor.firmware_ver        <= x"22101201";
  --SAFE_Monitor.firmware_ver        <= x"22041701";
  SAFE_Monitor.board_SN            <= ADDR;
--  SAFE_Monitor.clock_select        <= SAFE_Control.clock_select;
  SAFE_Monitor.seq_clock_phase     <= SAFE_Control.seq_clock_phase;
  SAFE_Monitor.IO_clock_phase      <= SAFE_Control.IO_clock_phase;
  SAFE_Monitor.reg_clock_phase     <= SAFE_Control.reg_clock_phase;
  SAFE_Monitor.mem_clock_phase     <= SAFE_Control.mem_clock_phase;
  SAFE_Monitor.resync_clock_phase  <= SAFE_Control.resync_clock_phase;
  SAFE_Monitor.clock_reset         <= SAFE_Control.clock_reset;
  SAFE_Monitor.reset               <= SAFE_Control.reset;
  SAFE_Monitor.LED_on              <= SAFE_Control.LED_on;
  SAFE_Monitor.FIFO_mode           <= SAFE_Control.FIFO_mode;
  SAFE_Monitor.trig_loop           <= SAFE_Control.trig_loop;
  SAFE_Monitor.trig_self           <= SAFE_Control.trig_self;
  SAFE_Monitor.trig_self_mode      <= SAFE_Control.trig_self_mode;
  SAFE_Monitor.trig_self_thres     <= SAFE_Control.trig_self_thres;
  SAFE_Monitor.trig_self_mask      <= SAFE_Control.trig_self_mask;
  SAFE_Monitor.trigger_SW_delay    <= SAFE_Control.trigger_SW_delay;
  SAFE_Monitor.trigger_HW_delay    <= SAFE_Control.trigger_HW_delay;
  SAFE_Monitor.TP_mode             <= SAFE_Control.TP_mode;
  SAFE_Monitor.TP_duration         <= SAFE_Control.TP_duration;
  SAFE_Monitor.TP_delay            <= SAFE_Control.TP_delay;
  SAFE_Monitor.TP_dummyb           <= SAFE_Control.TP_dummyb;
  SAFE_Monitor.start_IDELAY_sync   <= SAFE_Control.start_IDELAY_sync;
  SAFE_Monitor.clock_locked        <= MMCM_locked;
  SAFE_Monitor.calib_pulse_enabled <= SAFE_Control.calib_pulse_enabled;
  SAFE_Monitor.calib_mux_enabled   <= SAFE_Control.calib_mux_enabled;

-- VFE monitoring
  VFE_monitor.I2C_access_LVRB      <= VFE_control.I2C_access_LVRB;
  VFE_monitor.I2C_access_VFE       <= VFE_control.I2C_access_VFE;
  VFE_monitor.I2C_bulky_DTU        <= VFE_control.I2C_bulky_DTU;
  VFE_monitor.I2C_lpGBT_mode       <= VFE_control.I2C_lpGBT_mode;
  VFE_monitor.I2C_bulk_length      <= VFE_control.I2C_bulk_length;
  VFE_monitor.I2C_bulk_data_ref    <= VFE_control.I2C_bulk_data_ref;
  VFE_monitor.I2C_R_Wb             <= VFE_control.I2C_R_Wb;
  VFE_monitor.I2C_Reset            <= VFE_control.I2C_Reset;
  VFE_monitor.I2C_Reg_number       <= VFE_Control.I2C_Reg_number;  
  VFE_monitor.I2C_Device_number    <= VFE_Control.I2C_Device_number;
  VFE_Monitor.I2C_long_transfer    <= VFE_control.I2C_long_transfer;
  VFE_Monitor.DTU_auto_sync        <= VFE_control.DTU_auto_sync;
  VFE_Monitor.ADC_Resync_idle      <= VFE_control.ADC_Resync_idle;
  VFE_Monitor.ADC_Resync_data      <= VFE_control.ADC_Resync_data;
  VFE_Monitor.DTU_Sync_pattern     <= VFE_control.DTU_Sync_pattern;
  VFE_Monitor.ADC_invert_data      <= VFE_control.ADC_invert_data;
  VFE_Monitor.ADC_test_mode        <= VFE_control.ADC_test_mode;
  VFE_Monitor.ADC_calib_mode       <= VFE_control.ADC_calib_mode;
  VFE_Monitor.ADC_MEM_mode         <= VFE_control.ADC_MEM_mode;
  VFE_Monitor.ADC_Ped_mux          <= VFE_control.ADC_Ped_mux;
  VFE_Monitor.Vref_Mux             <= VFE_control.Vref_Mux;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA or I2C_ack_spy_DTU;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_DTU;
--  VFE_monitor.I2C_ack_spy          <= I2C_ack_spy_CATIA   when VFE_control.I2C_buggy_DTU   = '0' else
--                                      I2C_ack_spy_DTU;
  VFE_monitor.eLink_active         <= VFE_control.eLink_active;
  VFE_monitor.VFE_reset            <= VFE_control.VFE_reset;
  --VFE_monitor.I2C_n_ack_LVRB       <= x"FF";
  VFE_monitor.SPI_access_DAC       <= VFE_control.SPI_access_DAC;
  VFE_monitor.SPI_DAC_data         <= VFE_control.SPI_DAC_data;
  VFE_monitor.SPI_DAC_number       <= VFE_control.SPI_DAC_number;
  VFE_monitor.SPI_DAC_action       <= VFE_control.SPI_DAC_action;

  inst_G10_calib_pulse: if CATIA_TEST_BOARD generate
    G10_Calib_obuf    : OBUFT  port map (O => SEUA_in(1),    I => G10_calib_pulse, T => not  SAFE_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  end generate;
  inst_SEU_A: if not CATIA_TEST_BOARD generate
    G10_Calib_obuf    : IBUF   port map (I => SEUA_in(1),    O => SEUA_loc(1));
  end generate;
  --G10_Calib_iobuf    : IOBUF  port map (IO => SEUA_in(1),    I => G10_calib_pulse, O=> SEUA_loc(1), T => SAFE_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  G1_Calib_iobuf     : IOBUF  port map (IO => PLLlock_in(2), I => G1_calib_pulse,  O=> VFE_Monitor.ADC_PLL_lock(2), T => SAFE_control.calib_pulse_enabled); -- Calib pulse for G10 on calibration board
  VFE_Monitor.ADC_PLL_lock(1)      <= PllLock_in(1);
  VFE_Monitor.ADC_PLL_lock(3)      <= PllLock_in(3);
  VFE_Monitor.ADC_PLL_lock(4)      <= PllLock_in(4);
  VFE_Monitor.ADC_PLL_lock(5)      <= PllLock_in(5);

-- Trick for VFE board with 1 LiTE-DTU. Only Cal_busy(4) is connected to SAFE
-- Recopy this bit on all channels to be able to use LiTE-DTU in test mode or in normal mode.
  CalBusy_or    <=  CalBusy_in(1) or CalBusy_in(2) or CalBusy_in(3) or CalBusy_in(4) or CalBusy_in(5);
  delay_CalBusy: process(CalBusy_or, sequence_clk)
  begin
  if CalBusy_or='0' then
    CalBusy_delayed         <= '0';
    CalBusy_delay_counter   <= (others =>'0');
  elsif rising_edge(sequence_clk) then
    if CalBusy_delay_counter < CalBusy_delay then
      CalBusy_delay_counter <= CalBusy_delay_counter+1;
    else
      CalBusy_delayed <= '1';
    end if;
  end if;
  end process delay_CalBusy;

  start_idelay_sync_or <= SAFE_monitor.start_idelay_sync(1) or SAFE_monitor.start_idelay_sync(2) or SAFE_monitor.start_idelay_sync(3) or
                          SAFE_monitor.start_idelay_sync(4) or SAFE_monitor.start_idelay_sync(5); 
  gen_do_idelay_sync : process(start_idelay_sync_or, sequence_clk, SAFE_Control.trigger)
  begin
    if start_idelay_sync_or = '0' then
      do_IDELAY_sync        <= '0';
      idelay_sync_counter   <= (others=> '0');
    elsif rising_edge(sequence_clk) then
      if idelay_sync_counter < x"5000" then
        do_IDELAY_sync      <= '1';
        idelay_sync_counter <= idelay_sync_counter + 1;
      else
        do_IDELAY_sync      <= '0';
      end if;
    end if;
  end process gen_do_idelay_sync;


  VFE_Monitor.ADC_cal_busy(1)      <= CalBusy_delayed;
  VFE_Monitor.ADC_cal_busy(2)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(3)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(4)      <= CalBusy_delayed; 
  VFE_Monitor.ADC_cal_busy(5)      <= CalBusy_delayed; 
--  VFE_Monitor.ADC_cal_busy(1)      <= CalBusy_in(1);
--  VFE_Monitor.ADC_cal_busy(2)      <= CalBusy_in(2); 
--  VFE_Monitor.ADC_cal_busy(3)      <= CalBusy_in(3); 
--  VFE_Monitor.ADC_cal_busy(4)      <= CalBusy_in(4); 
--  VFE_Monitor.ADC_cal_busy(5)      <= CalBusy_in(5); 

  Calib_obuf    : OBUF  port map (O => TP_trigger_out, I => TP_trigger); -- Calib trigger for current injection

  --GPIO          <= sig_monitor;
--  GPIO_0_obuf   : OBUF  port map (O => GPIO_O(0), I => sig_monitor(0)); -- GPIO0 on schematics
--  GPIO_1_obuf   : OBUF  port map (O => GPIO_O(1), I => sig_monitor(3)); -- GPIO2 on schematics
-- With LVRB_noMB_Isense, GPIO2 and GPIO3 are connected to XADC
  --GPIO_2_obuf   : OBUF  port map (O => GPIO_O(2), I => sig_monitor(1)); -- GPIO1 on schematics
  --GPIO_3_obuf   : OBUF  port map (O => GPIO_O(3), I => sig_monitor(2)); -- GPIO3 on schematics

-- Signals coming/going from/to fake FE board : 
-- Ctrl_1 and Ctrl_2 are 2.5V compatible
-- Ctrl_0, 3, 4 and 5 are 1.2V compatible
  ctrl_out(4) <= '0' when trig_local='1' and FE_veto='0' else 'Z';
  ctrl_out(3) <= '0' when DAQ_busy='1' else 'Z';
  --CTRL_4_obuf   : OBUF  generic map (DRIVE => 8, IOSTANDARD => "LVCMOS12", SLEW => "FAST")
  --                      port map (O => Ctrl_out(4), I => trig_local_outb); -- trig_local active low
  --CTRL_3_obuf   : OBUF  generic map (DRIVE => 8, IOSTANDARD => "LVCMOS12", SLEW => "SLOW")
  --                      port map (O => Ctrl_out(3), I => DAQ_busy_b); -- DAQ_busy_b active low
  CTRL_2_obuf   : OBUF  port map (O => Ctrl_out(2), I => '0');
  CTRL_1_obuf   : OBUF  port map (O => Ctrl_out(1), I => '0');

  Vdummy_in     <= Ctrl_in(2);
  Vdummy_ref    <= Ctrl_ref(2);
  
  ReSync_out_obuf : OBUFDS port map (O => ReSync_out_P, OB => ReSync_out_N, I => ReSync_out);
  
  sig_monitor(0)       <= sequence_clk;
  sig_monitor(2)       <= SAFE_control.delay_ADC_number(1);
  sig_monitor(3)       <= SAFE_control.IO_reset;
--  trig_signal          <= calib_DAQ_start or (trig_software and SAFE_Monitor.trig_loop) or (trig_hardware and not SAFE_Monitor.trig_loop);
  trig_signal          <= G10_DAQ_start or G1_DAQ_start or TP_DAQ_start or trig_software or trig_hardware;
  trig_in              <= (FE_trigger or (trig_local and SAFE_monitor.trig_loop)) and (not FE_veto);

--  calib_trigger_merged <= SAFE_Control.calib_trigger or (SAFE_Monitor.calib_mode and FE_trigger(4));
  TP_trigger_merged    <= SAFE_Control.TP_trigger or FE_TP_trigger;
  
-- Hold ADC_start_Resync signal if we are near BC0. Hold TP trigger as well
  hold_ReSync : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      ADC_start_Resync      <= '0';
      WADC_start_ReSync     <= '0';
    elsif rising_edge(sequence_clk) then
      ADC_start_ReSync_del  <= VFE_control.ADC_Start_ReSync;
      if VFE_control.ADC_start_ReSync='1' and ADC_Start_ReSync_del='0' then
        WADC_Start_ReSync   <= '1';
--        SAFE_monitor.debug1(0) <= (others => '0');
--        SAFE_monitor.debug1(0)(13 downto 0) <= std_logic_vector(clk_counter);
      end if;
      if WADC_Start_Resync = '1' then
        if clk_counter>1 and clk_counter<14200 and VFE_monitor.ReSync_busy='0' then
          ADC_start_ReSync  <= '1';
          WADC_Start_ReSync <= '0';
--          SAFE_monitor.debug1(1) <= (others => '0');
--          SAFE_monitor.debug1(1)(13 downto 0) <= std_logic_vector(clk_counter);
        end if;
      else
        ADC_start_ReSync      <= '0';
      end if;
    end if;
  end process;

  gen_calib_trigger : process(MMCM_locked, pwup_reset, sequence_clk, TP_trigger_merged)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      WTP_trigger           <= '0';
      TP_trigger            <= '0';
      start_TP_delay        <= '0';
      TP_DAQ_start          <= '0';
      TP_duration_counter   <= x"0000";
      TP_delay_counter      <= x"0000";
      TP_DAQ_start_width    <= "00";
    elsif rising_edge(sequence_clk) then
      TP_trigger_prev       <= TP_trigger_merged;
      if TP_trigger_merged='1' and TP_trigger_prev='0' then
--        SAFE_monitor.debug1(2) <= (others => '0');
--        SAFE_monitor.debug1(2)(13 downto 0) <= std_logic_vector(clk_counter);
        WTP_trigger          <= '1';
        TP_DAQ_start         <= '0';
        TP_duration_counter  <= x"0000";
        TP_delay_counter     <= x"0000";
        TP_DAQ_start_width   <= "00";
      end if;
      if WTP_trigger = '1' then
        if clk_counter>1 and clk_counter<14200 and VFE_monitor.ReSync_busy='0' then  -- Don't send TP trigger around BC0 
--          SAFE_monitor.debug1(3) <= (others => '0');
--          SAFE_monitor.debug1(3)(13 downto 0) <= std_logic_vector(clk_counter);
          TP_trigger         <= '1';
          start_TP_delay     <= '1';
          WTP_trigger        <= '0';
        end if;
      end if;
      if TP_trigger = '1' then
        if TP_duration_counter < unsigned(SAFE_monitor.TP_duration) then        
          TP_duration_counter <= TP_duration_counter+1;
        else
          TP_trigger         <= '0';
        end if;
      end if;
      if start_TP_delay = '1' then
        if TP_delay_counter < unsigned(SAFE_monitor.TP_delay) then
          TP_delay_counter    <= TP_delay_counter+1;
        else
          TP_DAQ_start        <= '1';
          start_TP_delay      <= '0';
        end if;
      end if;
      if TP_DAQ_start = '1' then
        if TP_DAQ_start_width < "11" then
          TP_DAQ_start_width  <= TP_DAQ_start_width+1;
        else
          TP_DAQ_start        <= '0';
        end if;
      end if;
    end if;
  end process gen_calib_trigger;

  gen_G10_calib_pulse : process(MMCM_locked, pwup_reset, sequence_clk, SAFE_control.G10_calib_trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      G10_calib_pulse          <= '0';
      start_G10_delay          <= '0';
      G10_DAQ_start            <= '0';
      G10_duration_counter     <= x"0000";
      G10_delay_counter        <= x"0000";
      G10_DAQ_start_width      <= "00";
    elsif rising_edge(sequence_clk) then
      G10_trigger_prev1        <= SAFE_control.G10_calib_trigger;
      G10_trigger_prev2        <= G10_trigger_prev1;
      if G10_trigger_prev1='1' and G10_trigger_prev2='0' then
        G10_calib_pulse        <= '0';
        start_G10_delay        <= '1';
        G10_DAQ_start          <= '0';
        G10_duration_counter   <= x"0000";
        G10_delay_counter      <= x"0000";
        G10_DAQ_start_width    <= "00";
      end if;
      if G10_calib_pulse = '0' then
        if G10_duration_counter < unsigned(SAFE_monitor.TP_duration) then        
          G10_duration_counter <= G10_duration_counter+1;
        else
          G10_calib_pulse      <= '1';
        end if;
      end if;
      if start_G10_delay = '1' then
        if G10_delay_counter < unsigned(SAFE_monitor.TP_delay) then
          G10_delay_counter    <= G10_delay_counter+1;
        else
          G10_DAQ_start        <= '1';
          start_G10_delay      <= '0';
        end if;
      end if;
      if G10_DAQ_start = '1' then
        if G10_DAQ_start_width < "11" then
          G10_DAQ_start_width  <= G10_DAQ_start_width+1;
        else
          G10_DAQ_start        <= '0';
        end if;
      end if;
    end if;
  end process gen_G10_calib_pulse;

  gen_G1_calib_pulse : process(MMCM_locked, pwup_reset, sequence_clk, SAFE_control.G1_calib_trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      G1_calib_pulse           <= '1';
      start_G1_delay           <= '0';
      G1_DAQ_start             <= '0';
      G1_duration_counter      <= x"0000";
      G1_delay_counter         <= x"0000";
      G1_DAQ_start_width       <= "00";
    elsif rising_edge(sequence_clk) then
      G1_trigger_prev1         <= SAFE_control.G1_calib_trigger;
      G1_trigger_prev2         <= G1_trigger_prev1;
      if G1_trigger_prev1='1' and G1_trigger_prev2='0' then
        G1_calib_pulse         <= '0';
        start_G1_delay         <= '1';
        G1_DAQ_start           <= '0';
        G1_duration_counter    <= x"0000";
        G1_delay_counter       <= x"0000";
        G1_DAQ_start_width     <= "00";
      end if;
      if G1_calib_pulse = '1' then
        if G1_duration_counter < unsigned(SAFE_monitor.TP_duration) then        
          G1_duration_counter  <= G1_duration_counter+1;
        else
          G1_calib_pulse       <= '1';
        end if;
      end if;
      if start_G1_delay = '1' then
        if G1_delay_counter < unsigned(SAFE_monitor.TP_delay) then
          G1_delay_counter     <= G1_delay_counter+1;
        else
          G1_DAQ_start         <= '1';
          start_G1_delay       <= '0';
        end if;
      end if;
      if G1_DAQ_start = '1' then
        if G1_DAQ_start_width < "11" then
          G1_DAQ_start_width   <= G1_DAQ_start_width+1;
        else
          G1_DAQ_start         <= '0';
        end if;
      end if;
    end if;
  end process gen_G1_calib_pulse;

  gen_trig_out : process(MMCM_locked, pwup_reset, sequence_clk, SAFE_Control.trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      delay_out_counter     <= x"0000";
      start_out_counter     <= '0';
      trig_software         <= '0';
      trig_sw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trigger_prev1         <= SAFE_Control.trigger;
      trigger_prev2         <= trigger_prev1;
      if trigger_prev1='1' and trigger_prev2='0' then
        start_out_counter   <= '1';
        delay_out_counter   <= x"0000";
      end if;
      if start_out_counter = '1' then
        if delay_out_counter < unsigned(SAFE_control.trigger_SW_delay) then
          delay_out_counter <= delay_out_counter+1;
        else
          trig_software     <= '1';
          trig_sw_width     <= (others => '0');
          start_out_counter <= '0';
        end if;
      end if;
      if trig_software = '1' then
        if trig_sw_width < x"4" then
          trig_sw_width     <= trig_sw_width+1;
        else
          trig_software     <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_out;

  gen_100Hz : process(MMCM_locked, pwup_reset, sequence_clk, SAFE_control.gen_100Hz)
  variable gen_counter : unsigned(20 downto 0);
  variable gen_width   : unsigned(7 downto 0);
  begin
    if MMCM_locked='0'or pwup_reset='1' then
      gen_counter           := (others => '0');
      gen_width             := (others => '0');
      gen_trigger           <= '0';
    elsif rising_edge(sequence_clk) then
      if SAFE_control.gen_100Hz = '1' then
        gen_trigger           <= '0';
        gen_counter := gen_counter+1;
        if gen_counter = 0 then
          gen_width           := x"FF";
        end if;
        if gen_width /= 0 then
          gen_trigger         <= '1';
          gen_width           := gen_width -1;
        end if; 
      end if; 
    end if;
  end process gen_100Hz;
  
  gen_BC0 : process(MMCM_locked, pwup_reset, sequence_clk)
  begin
    if MMCM_locked='0'or pwup_reset='1' then
      clk_counter           <= (others => '0');
      BC0                   <= '0';
    elsif rising_edge(sequence_clk) then
      BC0                   <= '0';
      if clk_counter < 14255 then -- LHC orbit = 3564 clock at 40 MHz
        clk_counter         <= clk_counter+1;
      else
        BC0                 <= '1';
        clk_counter         <= (others => '0');
      end if; 
    end if;
  end process gen_BC0;
  
  gen_trig_in : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      delay_in_counter      <= x"0000";
      start_in_counter      <= '0';
      trig_hardware         <= '0';
      trig_in_Prev1         <= '0';
      trig_in_Prev2         <= '0';
      trig_hw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trig_in_prev1         <= trig_in;
      trig_in_prev2         <= trig_in_prev1;
      if trig_in_prev1='1' and trig_in_prev2='0' then
        start_in_counter    <= '1';
        delay_in_counter    <= x"0000";
      end if;
      if start_in_counter = '1' then
        if delay_in_counter < unsigned(SAFE_monitor.trigger_HW_delay) then
          delay_in_counter    <= delay_in_counter+1;
        else
          trig_hardware       <= '1';
          trig_hw_width       <= (others => '0');
          start_in_counter    <= '0';
        end if;
      end if;
      if trig_hardware = '1' then
        if trig_hw_width < x"4" then
          trig_hw_width         <= trig_hw_width+1;
        else
          trig_hardware       <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_in;

  -------------------------------------------------------------------------------
  -- Clocking
---- Clk port from local oscillator
  clk_IBUFGDS :    unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => TRUE) port map (O => loc_clk_in, I  => CLK_160_P, IB => CLK_160_N);
  HP_CLK_in <= loc_clk_in;

  Inst_clk_delay_ctrl : clk_generator_delay_ctrl
  port map
  (
    clk_160_in      => HP_clk_in,
    clk_delay_ctrl  => clk_delay_ctrl,
    reset           => CLK_reset,
    locked          => delay_clock_locked
  );
  clk_Gb_eth        <= Clk_delay_ctrl;

  Inst_clk_generator_iserdes : clk_generator_iserdes
  port map
  (
    clk_160_in      => HP_Clk_in,
    clk_20          => HP_clk_20,
    clk_40          => HP_Clk_40,
    clk_160         => HP_Clk_160,
    clk_640         => HP_Clk_640,
    reset           => CLK_reset,
    locked          => iserdes_clock_locked
  );

  MMCM_locked       <= iserdes_clock_locked and delay_clock_locked;

--clock for ADC and VFE capture
  Clk_IPbus         <= HP_CLK_20;
  system_clk        <= HP_CLK_160;
  sequence_clk      <= HP_CLK_160;
  shift_reg_clk     <= HP_CLK_160;
  memory_clk        <= HP_CLK_160;
  resync_clk        <= HP_CLK_160 when SAFE_monitor.resync_clock_phase="000" else not HP_CLK_160;
  --resync_clk        <= HP_CLK_160 xor SAFE_monitor.resync_clock_phase(0);
  clk_I2C           <= HP_CLK_160;

--  LED(0) <= reset and SAFE_monitor.LED_on;
--  LED(0) <= reset;
--  LED(2) <= SAFE_monitor.delay_locked;
--  LED(0) <= trig_local;

  LED(1) <= CalBusy_delayed;
  LED(0) <= do_idelay_sync;
  --LED(1) <= MMCM_locked;
  --LED(3) <= Pwup_reset;

--            VFE_monitor.ADC_test_mode or CalBusy_delayed;
--  LED(1) <= SAFE_monitor.Link_idelay_sync(1) or VFE_monitor.LVRB_busy;
--  LED(2) <= SAFE_monitor.Link_byte_sync(1) or VFE_monitor.CATIA_busy;
--  LED(3) <= SAFE_monitor.Link_bit_sync(1) or VFE_monitor.DTU_busy;
--  LED(1) <= SAFE_monitor.Link_idelay_sync(1);
--  LED(2) <= SAFE_monitor.Link_byte_sync(1);
--  LED(3) <= SAFE_monitor.Link_bit_sync(1);
--  LED(3) <= VFE_monitor.I2C_n_ack_DTU(0);

  -------------------------------------------------------------------------------
  -- VFE and FE interfaces
  -------------------------------------------------------------------------------  
  Inst_FE_VFE_IO : if not FOR_SEU generate
    Inst_VFE_Input : entity work.VFE_Input
    port map (
      IO_reset            => SAFE_control.IO_reset,
      delay_reset         => SAFE_control.delay_reset,
      bitslip_ADC_number  => SAFE_control.bitslip_ADC_number,
      byteslip_ADC_number => SAFE_control.byteslip_ADC_number,
      delay_ADC_number    => SAFE_control.delay_ADC_number,
      delay_tap_dir       => SAFE_control.delay_tap_dir,

      delay_locked        => SAFE_monitor.delay_locked,
      VFE_synchronized    => SAFE_monitor.VFE_synchronized,
      Link_idelay_sync    => SAFE_monitor.Link_idelay_sync,
      Link_byte_sync      => SAFE_monitor.Link_byte_sync,
      Link_bit_sync       => SAFE_monitor.Link_bit_sync,
      Link_idelay_pos     => SAFE_monitor.Link_idelay_pos,
      Sync_duration       => SAFE_monitor.Sync_duration,
      Link_error_sync     => SAFE_monitor.Link_error_sync,
      do_IDELAY_sync      => do_IDELAY_sync,

      ADC_cal_busy        => VFE_monitor.ADC_cal_busy,
      eLink_active        => VFE_monitor.eLink_active,
      DTU_auto_sync       => VFE_monitor.DTU_auto_sync,
      ADC_MEM_mode        => VFE_monitor.ADC_MEM_mode,
      ADC_test_mode       => VFE_monitor.ADC_test_mode,
      DTU_Sync_pattern    => VFE_monitor.DTU_Sync_pattern,

      MMCM_locked         => MMCM_locked,
      clk_delay_ctrl      => Clk_delay_ctrl,
      Clk_160             => HP_Clk_160,
      Clk_640             => HP_Clk_640,

      ch1_in_P            => ch1_in_P,
      ch1_in_N            => ch1_in_N,
      ch2_in_P            => ch2_in_P,
      ch2_in_N            => ch2_in_N,
      ch3_in_P            => ch3_in_P,
      ch3_in_N            => ch3_in_N,
      ch4_in_P            => ch4_in_P,
      ch4_in_N            => ch4_in_N,
      ch5_in_P            => ch5_in_P,
      ch5_in_N            => ch5_in_N,
      ch1_captured_stream => captured_stream(1),
      ch2_captured_stream => captured_stream(2),
      ch3_captured_stream => captured_stream(3),
      ch4_captured_stream => captured_stream(4),
      ch5_captured_stream => captured_stream(5)
    );
  
    Inst_FE_Output : entity work.FE_Output
    port map (
      synchronized        => SAFE_monitor.FE_synchronized,
      clk_160             => HP_Clk_160,
      clk_640             => HP_Clk_640,
      reset               => SAFE_reset,
      ch1_out_P           => ch1_out_P,
      ch1_out_N           => ch1_out_N,
      ch2_out_P           => ch2_out_P,
      ch2_out_N           => ch2_out_N,
      ch3_out_P           => ch3_out_P,
      ch3_out_N           => ch3_out_N,
      ch4_out_P           => ch4_out_P,
      ch4_out_N           => ch4_out_N,
      ch5_out_P           => ch5_out_P,
      ch5_out_N           => ch5_out_N,
      ch1_output_data     => output_stream(1),
      ch2_output_data     => output_stream(2),
      ch3_output_data     => output_stream(3),
      ch4_output_data     => output_stream(4),
      ch5_output_data     => output_stream(5)
    );
  end generate;

  Inst_VFE_Ctrl : entity work.VFE_Ctrl
  port map (
    I2C_access          => VFE_control.I2C_Access_VFE,
    I2C_R_Wb            => VFE_control.I2C_R_Wb,
    I2C_Device_number   => VFE_control.I2C_Device_number,
    I2C_Reg_number      => VFE_control.I2C_Reg_number,
    I2C_long_transfer   => VFE_control.I2C_long_transfer,
    I2C_Bulky           => VFE_control.I2C_Bulky,
    I2C_Bulk_length     => VFE_control.I2C_Bulk_length,
    I2C_Bulk_data_in    => VFE_control.I2C_Bulk_data,
    I2C_lpGBT_mode      => VFE_control.I2C_lpGBT_mode,

    I2C_busy_out        => VFE_monitor.I2C_VFE_busy,
    I2C_error           => VFE_monitor.I2C_VFE_error,
    I2C_reg_data_out    => VFE_monitor.I2C_Reg_data_VFE,
    I2C_n_ack           => VFE_monitor.I2C_n_ack_VFE,
    I2C_Bulk_data_out   => VFE_monitor.I2C_Bulk_data,

    ADC_start_Resync    => ADC_start_Resync,
    ADC_invert_Resync   => VFE_control.ADC_invert_Resync,
    ADC_ReSync_idle     => VFE_control.ADC_ReSync_idle,
    ADC_ReSync_data     => VFE_control.ADC_ReSync_data,
    ReSync_busy_out     => VFE_monitor.ReSync_busy,

    reset               => SAFE_reset,
    BC0                 => BC0,
    I2C_clk             => clk_I2C,
    I2C_scl             => I2C_scl_VFE,
    I2C_sda             => I2C_sda_VFE,
    I2C_ack_spy         => VFE_monitor.I2C_ack_spy,

    sequence_clk        => sequence_clk,
    ReSync_DTU          => resync_out_loc
  );
  ReSync_out_R          <= ReSync_out_loc when rising_edge(sequence_clk) else ReSync_out_R;
  ReSync_out_F          <= ReSync_out_loc when falling_edge(sequence_clk) else ReSync_out_F;
  ReSync_out            <= ReSync_out_R when SAFE_monitor.resync_clock_phase/="000" else
                           ReSync_out_F;

  -------------------------------------------------------------------------------
  -- Networking and IPBUS interface
  -------------------------------------------------------------------------------
  ip_addr  <= x"0A0000"&"1"&ADDR(6 downto 0);                             -- 10.0.0.(128..255)
  mac_addr <= x"060264BEEF"&not(ADDR); -- 06:02:64:BE:EF:XX


  SFP_Rate <= '0';
  EQEN_25  <= '1';
  Inst_gig_ethernet_pcs_pma_0 : gig_ethernet_pcs_pma_0
  port map (
    gtrefclk_p             => GbE_refclk_P,
    gtrefclk_n             => GbE_refclk_N,
    gtrefclk_out           => open,
    gtrefclk_bufg_out      => open,
    txp                    => Gbe_TxP,
    txn                    => Gbe_TxN,
    rxp                    => Gbe_RxP,
    rxn                    => Gbe_RxN,
    resetdone              => open,     --GbE_ready
    userclk_out            => open,
    userclk2_out           => GbE_user_clk,
    rxuserclk_out          => open,
    rxuserclk2_out         => open,
    pma_reset_out          => open,
--    mmcm_locked_out        => LED(0),
--    independent_clock_bufg => sequence_clk,--CLK_LHC,
    independent_clock_bufg => CLK_Gb_eth,--CLK_LHC,
    gmii_txd               => gmii_txd,
    gmii_tx_en             => gmii_tx_en,
    gmii_tx_er             => gmii_tx_er,
    gmii_rxd               => gmii_rxd,
    gmii_rx_dv             => gmii_rx_dv,
    gmii_rx_er             => gmii_rx_er,
    gmii_isolate           => open,
    configuration_vector   => "00000",  --"00001",
    status_vector          => open,
    reset                  => '0',
    signal_detect          => '1',
    gt0_qplloutclk_out     => open,
    gt0_qplloutrefclk_out  => open
  );

  frame_size              <= x"05EE";
--  rx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "0X00X0000010";
--  tx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "XXX0X0000010";
  rx_configuration_vector <= X"0000_0000_0000_0000_0812";
  tx_configuration_vector <= X"0000_0000_0000_0000_0012";
  Inst_tri_mode_ethernet_mac : tri_mode_ethernet_mac_0
  port map (
    gtx_clk                 => GbE_user_clk,
    glbl_rstn               => '1',
    rx_axi_rstn             => '1',
    tx_axi_rstn             => '1',
    rx_statistics_vector    => open,
    rx_statistics_valid     => open,
    rx_mac_aclk             => open,
    rx_reset                => open,
    rx_axis_mac_tdata       => mac_rx_data,
    rx_axis_mac_tvalid      => mac_rx_valid,
    rx_axis_mac_tlast       => mac_rx_last,
    rx_axis_mac_tuser       => mac_rx_error,
    tx_ifg_delay            => x"00",  --x"04",
    tx_statistics_vector    => open,
    tx_statistics_valid     => open,
    tx_mac_aclk             => open,
    tx_reset                => open,
    tx_axis_mac_tdata       => mac_tx_data,
    tx_axis_mac_tvalid      => mac_tx_valid,
    tx_axis_mac_tlast       => mac_tx_last,
    tx_axis_mac_tuser       => mac_tx_error,
    tx_axis_mac_tready      => mac_tx_ready,
    pause_req               => '0',
    pause_val               => (others => '0'),
    speedis100              => open,
    speedis10100            => open,
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    rx_configuration_vector => rx_configuration_vector,
    tx_configuration_vector => tx_configuration_vector
  );

  inst_ipbus : ipbus_ctrl
  port map(
    mac_clk      => GbE_user_clk,
    rst_macclk   => '0',
    ipb_clk      => clk_ipbus,
    rst_ipb      => '0',
    mac_rx_data  => mac_rx_data,
    mac_rx_valid => mac_rx_valid,
    mac_rx_last  => mac_rx_last,
    mac_rx_error => mac_rx_error,
    mac_tx_data  => mac_tx_data,
    mac_tx_valid => mac_tx_valid,
    mac_tx_last  => mac_tx_last,
    mac_tx_error => mac_tx_error(0),
    mac_tx_ready => mac_tx_ready,
    ipb_out      => ipb_master_out,
    ipb_in       => ipb_master_in,
    mac_addr     => mac_addr,
    ip_addr      => ip_addr,
    pkt_oob      => pkt_oob,
    pkt          => pkt
  );

  -------------------------------------------------------------------------------
  -- Translation from VFE to FE
  -------------------------------------------------------------------------------  

  inst_slaves : slaves
  port map(
    pwup_rst      => pwup_reset,
    DAQ_busy      => DAQ_busy,
    ipb_clk       => clk_ipbus,
    ipb_in        => ipb_master_out,
    ipb_out       => ipb_master_in,
    SAFE_Monitor  => SAFE_Monitor,
    SAFE_Control  => SAFE_Control,
    VFE_Monitor   => VFE_Monitor,
    VFE_Control   => VFE_Control,
    clk_40        => HP_Clk_40,
    clk_160       => HP_Clk_160,
    clk_shift_reg => shift_reg_clk,
    clk_memory    => memory_clk,
    ch1_in        => captured_stream(1),
    ch2_in        => captured_stream(2),
    ch3_in        => captured_stream(3),
    ch4_in        => captured_stream(4),
    ch5_in        => captured_stream(5),
    ch1_out       => output_stream(1),
    ch2_out       => output_stream(2),
    ch3_out       => output_stream(3),
    ch4_out       => output_stream(4),
    ch5_out       => output_stream(5),
    trig_local    => trig_local,
    trig_signal   => trig_signal,
    CRC_reset     => SAFE_Control.CRC_reset,
    CRC_error     => SAFE_Monitor.CRC_error,
    APD_temp_in   => APD_temp_in,
    APD_temp_ref  => APD_temp_ref,
    Vdummy_in     => Vdummy_in,
    Vdummy_ref    => Vdummy_ref,
    CATIA_temp_in => CATIA_temp_in,
    CATIA_temp_ref=> CATIA_temp_ref,
    V2P5_in       => V2P5_in,
    V2P5_ref      => V2P5_ref,
    V1P2_in       => V1P2_in,
    V1P2_ref      => V1P2_ref,
    Switch        => Switch
  );

--  LED      <= ADDR;
  test_counter : process (sequence_clk)
  begin  -- process test_counter
    if rising_edge(sequence_clk) then
      led_counter <= led_counter + 1;
--      LED(0) <= led_counter(0) and SAFE_Monitor.LED_on;
--      LED(1) <= led_counter(8) and SAFE_Monitor.LED_on;
--      LED(2) <= led_counter(16) and SAFE_Monitor.LED_on;
--      LED(3) <= led_counter(24) and SAFE_Monitor.LED_on;
    end if;
  end process test_counter;

--  GPIO     <= "ZZZZ";--i2c_clk_in&SFP_LOS & SFP_TX_Fault & SFP_Present;
  --JTAG_OUT <= JTAG_IN;

  --i2c bus for interaction between VFE AND FE
  i2c_sda_in  <= 'Z';

  -- SFP i2c bus
  SFP_SCL     <= 'Z';
  SFP_SDA     <= 'Z';

end rtl;
