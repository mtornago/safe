-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Jun  5 15:26:31 2023
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 36 (Thirty Six)
-- Command     : write_vhdl -force -mode synth_stub
--               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/clk_generator_delay_ctrl/clk_generator_delay_ctrl_stub.vhdl
-- Design      : clk_generator_delay_ctrl
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_generator_delay_ctrl is
  Port ( 
    clk_delay_ctrl : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_160_in : in STD_LOGIC
  );

end clk_generator_delay_ctrl;

architecture stub of clk_generator_delay_ctrl is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_delay_ctrl,reset,locked,clk_160_in";
begin
end;
