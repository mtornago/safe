// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Jun  5 15:26:31 2023
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 36 (Thirty Six)
// Command     : write_verilog -force -mode synth_stub
//               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/clk_generator_delay_ctrl/clk_generator_delay_ctrl_stub.v
// Design      : clk_generator_delay_ctrl
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_generator_delay_ctrl(clk_delay_ctrl, reset, locked, clk_160_in)
/* synthesis syn_black_box black_box_pad_pin="clk_delay_ctrl,reset,locked,clk_160_in" */;
  output clk_delay_ctrl;
  input reset;
  output locked;
  input clk_160_in;
endmodule
