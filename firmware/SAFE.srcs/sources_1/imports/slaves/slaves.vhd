-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.ALL;
use work.SAFE_IO.all;

entity slaves is
  port(
    pwup_rst      : in    std_logic;
    DAQ_busy      : out   std_logic;
    ipb_clk       : in    std_logic;
    ipb_in        : in    ipb_wbus;
    ipb_out       : out   ipb_rbus;
    SAFE_Monitor  : in    SAFE_Monitor_t;
    SAFE_Control  : out   SAFE_Control_t;
    VFE_Monitor   : inout VFE_Monitor_t;
    VFE_Control   : out   VFE_Control_t;
    clk_40        : in    std_logic;
    clk_160       : in    std_logic;
    clk_shift_reg : in    std_logic;
    clk_memory    : in    std_logic;
    ch_in         : in    Word_t(Nb_of_Lines downto 1);
    trig_local    : out   std_logic;
    trig_signal   : in    std_logic;
    CRC_reset     : in    std_logic;
    CRC_error     : out   UInt32_t(Nb_of_Lines downto 1);
    APD_temp_in   : in    std_logic_vector(Nb_of_VFE downto 1);
    APD_temp_ref  : in    std_logic_vector(Nb_of_VFE downto 1);
    CATIA_temp_in : in    std_logic_vector(Nb_of_VFE downto 1);
    CATIA_temp_ref: in    std_logic_vector(Nb_of_VFE downto 1)
  );
end slaves;

architecture rtl of slaves is

constant NSLV       : positive := 2;
signal ipbw         : ipb_wbus_array(NSLV-1 downto 0);
signal ipbr, ipbr_d : ipb_rbus_array(NSLV-1 downto 0);
signal ctrl_reg     : std_logic_vector(31 downto 0);
signal inj_stat     : std_logic_vector(63 downto 0);
signal inj_ctrl     : std_logic_vector(63 downto 0);

begin

  fabric: entity work.ipbus_fabric
  generic map(NSLV => NSLV)
  port map(
    ipb_in          => ipb_in,
    ipb_out         => ipb_out,
    ipb_to_slaves   => ipbw,
    ipb_from_slaves => ipbr
  );

-- Slave 0: id / rst reg
  slave0: entity work.SAFE_ctrlreg
  port map (
    clk            => ipb_clk,
    reset          => pwup_rst,
    ipbus_in       => ipbw(0),
    ipbus_out      => ipbr(0),
    SAFE_monitor   => SAFE_Monitor,
    SAFE_control   => SAFE_Control,
    VFE_monitor    => VFE_Monitor,
    VFE_control    => VFE_Control,
    APD_temp_in    => APD_temp_in,
    APD_temp_ref   => APD_temp_ref,
    CATIA_temp_in  => CATIA_temp_in,
    CATIA_temp_ref => CATIA_temp_ref
  );
  
-- Slave 1: VFE capture
  slave1: entity work.VFE_capture
  port map(
    DAQ_busy          => DAQ_busy,
    clk_ipbus         => ipb_clk,
    reset             => pwup_rst,
    ipbus_in          => ipbw(1),
    ipbus_out         => ipbr(1),
    clk_40            => clk_40,
    clk_shift_reg     => clk_shift_reg,
    ch_in             => ch_in,
    trig_local        => trig_local,
    trig_self         => SAFE_Monitor.trig_self,
    trig_self_mode    => SAFE_Monitor.trig_self_mode,
    trig_self_mask    => SAFE_Monitor.trig_self_mask,
    trig_self_thres   => SAFE_Monitor.trig_self_thres,
    FIFO_mode         => SAFE_Monitor.FIFO_mode,
    DTU_test_mode     => VFE_Monitor.DTU_test_mode,
    DTU_MEM_mode      => VFE_Monitor.DTU_MEM_mode,
    trig_signal       => trig_signal,
    CRC_reset         => CRC_reset,
    CRC_error         => CRC_error
  );

end rtl;
