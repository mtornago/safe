----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for SAFE control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.SAFE_IO.all;

entity SAFE_ctrlreg is
  generic (
    USE_GPIO_WITH_LVR : boolean := USE_LVRB;
    USE_GPIO_WITH_DAC : boolean := not USE_LVRB
  );

  port(
    clk            : in  std_logic;
    reset          : in  std_logic;
    ipbus_in       : in  ipb_wbus;
    ipbus_out      : out ipb_rbus;
    SAFE_monitor   : in  SAFE_Monitor_t;
    SAFE_control   : out SAFE_Control_t;
    VFE_monitor    : in  VFE_Monitor_t;
    VFE_control    : out VFE_Control_t;
    APD_temp_in    : in std_logic_vector(Nb_of_VFE downto 1);
    APD_temp_ref   : in std_logic_vector(Nb_of_VFE downto 1);
    CATIA_temp_in  : in std_logic_vector(Nb_of_VFE downto 1);
    CATIA_temp_ref : in std_logic_vector(Nb_of_VFE downto 1)
  );
end SAFE_ctrlreg;

architecture rtl of SAFE_ctrlreg is

  signal ack : std_logic := '0';

    --* State type of the ipbus process
  type   ipbus_rw_type is (ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_ack, ipbus_finished);
  signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_idle;

  type   XADC_status_type is (XADC_idle, XADC_read, XADC_write);
  signal XADC_status           : XADC_status_type := XADC_idle;

  signal XADC_control          : XADC_control_t := default_XADC_control;
  signal XADC_monitor          : XADC_monitor_t := default_XADC_monitor;
  signal loc_XADC_data         : std_logic_vector(15 downto 0) := (others => '0');
  signal reading               : std_logic_vector(15 downto 0) := (others => '0');
  signal muxaddr               : std_logic_vector( 4 downto 0) := (others => '0');
  signal channel               : std_logic_vector( 4 downto 0) := (others => '0');
  signal vauxn                 : std_logic_vector(15 downto 0) := (others => '0');
  signal vauxp                 : std_logic_vector(15 downto 0) := (others => '0');
  signal wait_for_XADC_ready   : std_logic := '0';
  signal wait_for_DTU_ready    : std_logic := '0';
  signal wait_for_I2C_ready    : std_logic := '0';
  signal total_seq             : unsigned(31 downto 0) := (others => '0');
  signal total_conv            : unsigned(31 downto 0) := (others => '0');
  signal total_access          : unsigned(31 downto 0) := (others => '0');
  signal XADC_eos              : std_logic := '0';
  signal XADC_eos_del          : std_logic := '0';
  signal XADC_eoc              : std_logic := '0';
  signal XADC_eoc_del          : std_logic := '0';
  signal XADC_ready_del        : std_logic := '0';
  signal XADC_clk              : std_logic := '0';

begin
  reg: process (reset, clk) is
  variable ReSync_busy     : std_logic := '0';
  begin  -- process reg
    if reset = '1' then
      SAFE_control                            <= DEFAULT_SAFE_Control;
      VFE_control                             <= DEFAULT_VFE_Control;
      ipbus_rw_state                          <= ipbus_rw_idle;
      ipbus_out.ipb_rdata                     <= (others => '0'); --zero the response before the actual case response
      wait_for_XADC_ready                     <= '0';
      wait_for_DTU_ready                      <= '0';
    elsif rising_edge(clk) then
      SAFE_control.trigger                    <= '0';
      SAFE_control.TP_trigger                 <= '0';
      SAFE_control.AWG_trigger                <= '0';
      SAFE_control.clock_reset                <= '0';
      SAFE_control.reset                      <= '0';
--      SAFE_control.delay_reset                <= '0';
      SAFE_control.tapslip_DTU_map            <= (others => '0');
      SAFE_control.bitslip_DTU_map            <= (others => '0');
      SAFE_control.byteslip_DTU_map           <= (others => '0');
      SAFE_control.start_idelay_sync          <= (others => '0');
      XADC_control.XADC_access                <= '0';
      VFE_control.DTU_Start_ReSync            <= '0';
      VFE_control.I2C_reset                   <= '0';
--      VFE_control.VFE_reset                   <= '0';
      SAFE_control.IO_reset                   <= '0';
      SAFE_control.delay_reset                <= '0';
      SAFE_control.CRC_reset                  <= '0';

      case ipbus_rw_state is
      when ipbus_rw_idle =>
        VFE_control.I2C_access_VFE            <= '0';
        ack <= '0';
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state                    <= ipbus_write_start;
          else
            ipbus_rw_state                    <= ipbus_read_start;
          end if;
        end if;
      when ipbus_read_start =>
        ipbus_out.ipb_rdata                   <= (others => '0'); --zero the response before the actual case response
        case ipbus_in.ipb_addr(7 downto 0) is
        when x"00" =>                                                                   -- FW_VER
          ipbus_out.ipb_rdata                 <= SAFE_monitor.firmware_ver;
        when x"01"=> -- Clock selection (0=160 MHz, 1=120 MHz, 2=80 MHz, 3=40 MHz)         SAFE_CTRL
          ipbus_out.ipb_rdata(0)              <= SAFE_monitor.reset;
          ipbus_out.ipb_rdata(1)              <= SAFE_monitor.FIFO_mode;
          ipbus_out.ipb_rdata(2)              <= SAFE_monitor.trig_loop;
          ipbus_out.ipb_rdata(3)              <= SAFE_monitor.trig_self;
          ipbus_out.ipb_rdata(7)              <= SAFE_monitor.clock_reset;
          ipbus_out.ipb_rdata(20 downto 8)    <= SAFE_monitor.trig_self_thres;
          ipbus_out.ipb_rdata(31 downto 28)   <= SAFE_monitor.board_sn(3 downto 0);
        when x"02" =>                                                                   -- TRIG_DELAY
          ipbus_out.ipb_rdata(15 downto 0)    <= SAFE_monitor.trigger_HW_delay;
          ipbus_out.ipb_rdata(31 downto 16)   <= SAFE_monitor.trigger_SW_delay;
        when x"03" => -- I2C and Calib status                                           -- VFE_CTRL
          ipbus_out.ipb_rdata(1)              <= VFE_monitor.DTU_test_mode;
          ipbus_out.ipb_rdata(2)              <= VFE_monitor.DTU_MEM_mode;
          ipbus_out.ipb_rdata(21 downto 16)   <= VFE_monitor.VFE_LV; 
          ipbus_out.ipb_rdata(27)             <= VFE_monitor.I2C_lpGBT_mode;
          ipbus_out.ipb_rdata(28)             <= VFE_monitor.I2C_VFE_busy;
          ipbus_out.ipb_rdata(29)             <= VFE_monitor.I2C_VFE_error;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_reset;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.VFE_reset;
        when x"04" => -- Calib trigger settting                                         -- CALIB_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= SAFE_monitor.TP_duration;
          ipbus_out.ipb_rdata(30 downto 16)   <= SAFE_monitor.TP_delay(14 downto 0);
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.TP_dummyb;
        when x"05" =>                                                                   -- TRIGGER_MASK
          ipbus_out.ipb_rdata(24 downto 0)    <= SAFE_monitor.trig_self_mask;
        when x"06" =>                                                                   -- CLK_SETTING
          ipbus_out.ipb_rdata(24 downto 0)    <= VFE_monitor.eLink_active; 
          ipbus_out.ipb_rdata(29 downto 25)   <= SAFE_monitor.resync_clock_phase; 
        when x"07" =>                                                                   -- TAPSLIP
          ipbus_out.ipb_rdata(24 downto 0)    <= (others => '0'); -- No readback 
        when x"08" =>                                                                   -- BITSLIP
          ipbus_out.ipb_rdata(24 downto 0)    <= (others => '0'); -- No readback
        when x"09" =>                                                                   -- BYTESLIP
          ipbus_out.ipb_rdata(24 downto 0)    <= (others => '0'); -- No readback 
        when x"0a" => -- Delay locked signals                                           -- DELAY_CTRL1
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.sync_link_bit_OK(1)&SAFE_monitor.Link_idelay_pos(1);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.sync_link_bit_OK(2)&SAFE_monitor.Link_idelay_pos(2);
          ipbus_out.ipb_rdata(17  downto 12)  <= SAFE_monitor.sync_link_bit_OK(3)&SAFE_monitor.Link_idelay_pos(3);
          ipbus_out.ipb_rdata(23  downto 18)  <= SAFE_monitor.sync_link_bit_OK(4)&SAFE_monitor.Link_idelay_pos(4);
          ipbus_out.ipb_rdata(29  downto 24)  <= SAFE_monitor.sync_link_bit_OK(5)&SAFE_monitor.Link_idelay_pos(5);
          ipbus_out.ipb_rdata(30)             <= SAFE_monitor.FE_synchronized;
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.VFE_synchronized;
        when x"0b" => -- Delay locked signals                                           -- DELAY_CTRL2
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.sync_link_bit_OK(6)&SAFE_monitor.Link_idelay_pos(6);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.sync_link_bit_OK(7)&SAFE_monitor.Link_idelay_pos(7);
          ipbus_out.ipb_rdata(17  downto  12) <= SAFE_monitor.sync_link_bit_OK(8)&SAFE_monitor.Link_idelay_pos(8);
          ipbus_out.ipb_rdata(23  downto  18) <= SAFE_monitor.sync_link_bit_OK(9)&SAFE_monitor.Link_idelay_pos(9);
          ipbus_out.ipb_rdata(29  downto  24) <= SAFE_monitor.sync_link_bit_OK(10)&SAFE_monitor.Link_idelay_pos(10);
        when x"0c" => -- Delay locked signals                                           -- DELAY_CTRL3
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.sync_link_bit_OK(11)&SAFE_monitor.Link_idelay_pos(11);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.sync_link_bit_OK(12)&SAFE_monitor.Link_idelay_pos(12);
          ipbus_out.ipb_rdata(17  downto  12) <= SAFE_monitor.sync_link_bit_OK(13)&SAFE_monitor.Link_idelay_pos(13);
          ipbus_out.ipb_rdata(23  downto  18) <= SAFE_monitor.sync_link_bit_OK(14)&SAFE_monitor.Link_idelay_pos(14);
          ipbus_out.ipb_rdata(29  downto  24) <= SAFE_monitor.sync_link_bit_OK(15)&SAFE_monitor.Link_idelay_pos(15);
        when x"0d" => -- Delay locked signals                                           -- DELAY_CTRL4
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.sync_link_bit_OK(16)&SAFE_monitor.Link_idelay_pos(16);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.sync_link_bit_OK(17)&SAFE_monitor.Link_idelay_pos(17);
          ipbus_out.ipb_rdata(17  downto  12) <= SAFE_monitor.sync_link_bit_OK(18)&SAFE_monitor.Link_idelay_pos(18);
          ipbus_out.ipb_rdata(23  downto  18) <= SAFE_monitor.sync_link_bit_OK(19)&SAFE_monitor.Link_idelay_pos(19);
          ipbus_out.ipb_rdata(29  downto  24) <= SAFE_monitor.sync_link_bit_OK(20)&SAFE_monitor.Link_idelay_pos(20);
        when x"0e" => -- Delay locked signals                                           -- DELAY_CTRL5
          ipbus_out.ipb_rdata(5   downto  0)  <= SAFE_monitor.sync_link_bit_OK(21)&SAFE_monitor.Link_idelay_pos(21);
          ipbus_out.ipb_rdata(11  downto  6)  <= SAFE_monitor.sync_link_bit_OK(22)&SAFE_monitor.Link_idelay_pos(22);
          ipbus_out.ipb_rdata(17  downto  12) <= SAFE_monitor.sync_link_bit_OK(23)&SAFE_monitor.Link_idelay_pos(23);
          ipbus_out.ipb_rdata(23  downto  18) <= SAFE_monitor.sync_link_bit_OK(24)&SAFE_monitor.Link_idelay_pos(24);
          ipbus_out.ipb_rdata(29  downto  24) <= SAFE_monitor.sync_link_bit_OK(25)&SAFE_monitor.Link_idelay_pos(25);
        when x"0f" => -- PLL lock signals                                               -- PLL_LOCK
          ipbus_out.ipb_rdata(24 downto 0)    <= VFE_monitor.DTU_PLL_lock;         
        when x"10" => -- XADC alarm register                                            -- DRP_XADC
          ipbus_out.ipb_rdata(22 downto 16)   <= XADC_monitor.XADC_addr; 
          ipbus_out.ipb_rdata(15 downto 0)    <= XADC_monitor.XADC_data;
        when x"11" => -- Sequence counter (32 bits)                                     -- CONV_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_conv);
        when x"12" => -- Sequence counter (32 bits)                                     -- SEQ_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_seq);
        when x"13" => -- Sequence counter (32 bits)                                     -- ACCESS_XADC
          ipbus_out.ipb_rdata                 <= std_logic_vector(total_access);
        when x"14" =>                                                                   -- VFE_PATTERN
          ipbus_out.ipb_rdata(4 downto 0)     <= VFE_monitor.I2C_VFE_pattern;
        when x"15" =>                                                                   -- I2C1_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE(1);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when x"16" => -- I2C spy acknowledge byte 6                                     -- I2C1_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when x"17" =>                                                                   -- I2C2_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE(2);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when x"18" => -- I2C spy acknowledge byte 6                                     -- I2C2_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when x"19" =>                                                                   -- I2C3_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE(3);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when x"1a" => -- I2C spy acknowledge byte 6                                     -- I2C3_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when x"1b" =>                                                                   -- I2C4_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE(4);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when x"1c" => -- I2C spy acknowledge byte 6                                     -- I2C4_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when x"1d" =>                                                                   -- I2C5_CTRL
          ipbus_out.ipb_rdata(15 downto 0)    <= VFE_monitor.I2C_Reg_data_VFE(5);
          ipbus_out.ipb_rdata(22 downto 16)   <= VFE_monitor.I2C_Reg_Number;
          ipbus_out.ipb_rdata(29 downto 23)   <= VFE_monitor.I2C_Device_Number;
          ipbus_out.ipb_rdata(30)             <= VFE_monitor.I2C_long_transfer;
          ipbus_out.ipb_rdata(31)             <= VFE_monitor.I2C_R_Wb;
        when x"1e" => -- I2C spy acknowledge byte 6                                     -- I2C5_NACK
          ipbus_out.ipb_rdata(15 downto 8)    <= std_logic_vector(VFE_monitor.I2C_n_ack_VFE);
        when x"1f" => -- LiTE-DTU Resync command                                       -- DTU_RESYNC
          ipbus_out.ipb_rdata                 <= VFE_monitor.DTU_ReSync_data;
        when x"20" =>                                                                  -- DTU_SYNC_PATTERN
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.DTU_Sync_pattern;
        when x"21" => -- LiTE-DTU Resync idle pattern                                  -- RESYNC_IDLE
          ipbus_out.ipb_rdata(7 downto 0)     <= VFE_monitor.DTU_ReSync_idle;
        when x"22" => -- Debug register. Put here what you want                         -- MMCM
          ipbus_out.ipb_rdata(30)             <= SAFE_monitor.DCI_locked; -- DelayCTRL locked signal
          ipbus_out.ipb_rdata(31)             <= SAFE_monitor.delay_locked; -- DelayCTRL locked signal
        when x"23" => -- Debug register. Put here what you want                         -- SYNC
          ipbus_out.ipb_rdata                 <= SAFE_monitor.sync_duration;
        when x"24" => -- CRC error counters                                             -- CRC1
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(1));
        when x"25" => -- CRC error counters                                             -- CRC2
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(2));
        when x"26" => -- CRC error counters                                             -- CRC3
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(3));
        when x"27" => -- CRC error counters                                             -- CRC4
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(4));
        when x"28" => -- CRC error counters                                             -- CRC5
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(5));
        when x"29" => -- CRC error counters                                             -- CRC6
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(6));
        when x"2a" => -- CRC error counters                                             -- CRC7
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(7));
        when x"2b" => -- CRC error counters                                             -- CRC8
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(8));
        when x"2c" => -- CRC error counters                                             -- CRC9
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(9));
        when x"2d" => -- CRC error counters                                             -- CRC10
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(10));
        when x"2e" => -- CRC error counters                                             -- CRC11
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(11));
        when x"2f" => -- CRC error counters                                             -- CRC12
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(12));
        when x"30" => -- CRC error counters                                             -- CRC13
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(13));
        when x"31" => -- CRC error counters                                             -- CRC14
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(14));
        when x"32" => -- CRC error counters                                             -- CRC15
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(15));
        when x"33" => -- CRC error counters                                             -- CRC16
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(16));
        when x"34" => -- CRC error counters                                             -- CRC17
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(17));
        when x"35" => -- CRC error counters                                             -- CRC18
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(18));
        when x"36" => -- CRC error counters                                             -- CRC19
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(19));
        when x"37" => -- CRC error counters                                             -- CRC20
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(20));
        when x"38" => -- CRC error counters                                             -- CRC21
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(21));
        when x"39" => -- CRC error counters                                             -- CRC22
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(22));
        when x"3a" => -- CRC error counters                                             -- CRC23
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(23));
        when x"3b" => -- CRC error counters                                             -- CRC24
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(24));
        when x"3c" => -- CRC error counters                                             -- CRC25
          ipbus_out.ipb_rdata                 <= std_logic_vector(SAFE_monitor.CRC_error(25));
        when x"40" => -- I2C spy acknowledge byte 0                                     -- I2C_ACK1
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(31 downto 0);
        when x"41" => -- I2C spy acknowledge byte 1                                     -- I2C_ACK2
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(63 downto 32);
        when x"42" => -- I2C spy acknowledge byte 2                                     -- I2C_ACK3
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(95 downto 64);
        when x"43" => -- I2C spy acknowledge byte 3                                     -- I2C_ACK4
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(127 downto 96);
        when x"44" => -- I2C spy acknowledge byte 4                                     -- I2C_ACK5
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(159 downto 128);
        when x"45" => -- I2C spy acknowledge byte 5                                     -- I2C_ACK6
          ipbus_out.ipb_rdata(31 downto 0)    <= VFE_monitor.I2C_ack_spy(191 downto 160);
        when x"46" => -- WTE position                                                   -- I2C_POS
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(SAFE_monitor.WTE_command_pos);
        when x"47" => -- ReSync position                                                   -- I2C_POS
          ipbus_out.ipb_rdata(13 downto 0)    <= std_logic_vector(SAFE_monitor.ReSync_command_pos);
        when x"60" => -- DEBUG_0
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(0);
        when x"61" => -- DEBUG_1
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(1);
        when x"62" => -- DEBUG_2
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(2);
        when x"63" => -- DEBUG_3
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(3);
        when x"64" => -- DEBUG_4
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(4);
        when x"65" => -- DEBUG_5
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(5);
        when x"66" => -- DEBUG_6
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(6);
        when x"67" => -- DEBUG_7
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(7);
        when x"68" => -- DEBUG_8
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(8);
        when x"69" => -- DEBUG_9
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(9);
        when x"6a" => -- DEBUG_10
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(10);
        when x"6b" => -- DEBUG_11
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(11);
        when x"6c" => -- DEBUG_12
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(12);
        when x"6d" => -- DEBUG_13
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(13);
        when x"6e" => -- DEBUG_14
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(14);
        when x"6f" => -- DEBUG_15
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(15);
        when x"70" => -- DEBUG_16
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(16);
        when x"71" => -- DEBUG_17
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(17);
        when x"72" => -- DEBUG_18
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(18);
        when x"73" => -- DEBUG_19
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(19);
        when x"74" => -- DEBUG_20
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(20);
        when x"75" => -- DEBUG_21
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(21);
        when x"76" => -- DEBUG_22
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(22);
        when x"77" => -- DEBUG_23
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(23);
        when x"78" => -- DEBUG_24
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(24);
        when x"79" => -- DEBUG_25
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(25);
        when x"7a" => -- DEBUG_26
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(26);
        when x"7b" => -- DEBUG_27
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(27);
        when x"7c" => -- DEBUG_28
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(28);
        when x"7d" => -- DEBUG_29
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(29);
        when x"7e" => -- DEBUG_30
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(30);
        when x"7f" => -- DEBUG_31
          ipbus_out.ipb_rdata                 <= SAFE_monitor.debug(31);
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(6 downto 0) is
        when x"00" =>
          SAFE_control.trigger                <= ipbus_in.ipb_wdata(0);
          SAFE_control.TP_trigger             <= ipbus_in.ipb_wdata(1) or ipbus_in.ipb_wdata(7);
          if ipbus_in.ipb_wdata(1) = '1' then -- Generate a ReSync transaction for TP trigger with LiTE-DTU version >= 2.0
            VFE_control.DTU_ReSync_data       <= x"0000000d";
            VFE_control.DTU_Start_ReSync      <= '1';
            wait_for_DTU_ready                <= '1';
          end if;
          if ipbus_in.ipb_wdata(7) = '1' then -- Generate a ReSync transaction with LiTE-DTU version >= 2.0
            VFE_control.DTU_Start_ReSync      <= '1';
            wait_for_DTU_ready                <= '1';
          end if; 
          SAFE_control.gen_WTE                <= ipbus_in.ipb_wdata(2);
          SAFE_control.LED_on                 <= ipbus_in.ipb_wdata(3);
          SAFE_control.gen_BC0                <= ipbus_in.ipb_wdata(5);
          SAFE_control.AWG_trigger            <= ipbus_in.ipb_wdata(8);
          SAFE_control.CRC_reset              <= ipbus_in.ipb_wdata(31); -- Reset CRC counters
        when x"01" =>
          SAFE_control.reset                  <= ipbus_in.ipb_wdata(0);
          SAFE_control.FIFO_mode              <= ipbus_in.ipb_wdata(1); -- single event (0) or FIFO (1) DAQ mode
          SAFE_control.trig_loop              <= ipbus_in.ipb_wdata(2); -- external trigger (0) or loop local software trigger (1)
          SAFE_control.trig_self              <= ipbus_in.ipb_wdata(3); -- generate self trigger looking at input data
          SAFE_control.clock_reset            <= ipbus_in.ipb_wdata(7);
          SAFE_control.trig_self_thres        <= ipbus_in.ipb_wdata(20 downto 8);
          SAFE_control.trig_self_mode         <= ipbus_in.ipb_wdata(31);
        when x"02" =>
          SAFE_control.trigger_HW_delay       <= ipbus_in.ipb_wdata(15 downto 0);
          SAFE_control.trigger_SW_delay       <= ipbus_in.ipb_wdata(31 downto 16);
        when x"03" => -- VFE config
          VFE_control.DTU_test_mode           <= ipbus_in.ipb_wdata(1);
          VFE_control.DTU_MEM_mode            <= ipbus_in.ipb_wdata(2);
          VFE_control.VFE_LV                  <= ipbus_in.ipb_wdata(21 downto 16);
          VFE_control.I2C_lpGBT_mode          <= ipbus_in.ipb_wdata(27);
          VFE_control.I2C_reset               <= ipbus_in.ipb_wdata(30);
          VFE_control.VFE_reset               <= ipbus_in.ipb_wdata(31);
        when x"04" => -- Calib trigger settting
          SAFE_control.TP_duration            <= ipbus_in.ipb_wdata(15 downto 0);
          SAFE_control.TP_delay               <= '0'&ipbus_in.ipb_wdata(30 downto 16);
          SAFE_control.TP_dummyb              <= ipbus_in.ipb_wdata(31);
        when x"05" => -- Set self trigger mask
          SAFE_control.trig_self_mask         <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);
        when x"06" => -- Active links and clock phase for each ReSync signal
          VFE_control.eLink_active            <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);
          SAFE_control.resync_clock_phase     <= ipbus_in.ipb_wdata(29 downto 25);
        when x"07" => -- ADC lines delay settting : tapslip
          SAFE_control.tapslip_DTU_map        <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);    -- Bit pattern for ADC delay setting (b0 to b24)
        when x"08" => -- ADC lines delay settting : bitslip
          SAFE_control.bitslip_DTU_map        <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);    -- Bit pattern for ADC bislip (b0 to b24)
        when x"09" => -- ADC lines delay settting : byteslip
          SAFE_control.byteslip_DTU_map       <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);    -- Bit pattern for ADC byteslip (b0 to b24)
        when x"0a" => -- ADC lines delay settting : What to do
          SAFE_control.start_idelay_sync      <= ipbus_in.ipb_wdata(Nb_of_lines-1 downto 0);    -- Ask for Idelay tuning on ADC streams
          SAFE_control.delay_tap_dir          <= ipbus_in.ipb_wdata(25);
          SAFE_control.delay_reset            <= ipbus_in.ipb_wdata(26);
          SAFE_control.IO_reset               <= ipbus_in.ipb_wdata(27);
        when x"10" => -- Ask to read (b31=1) or write (b31=0) content of register b23..b16 in/with data b15..b0
          XADC_control.XADC_access            <= '1';
          XADC_control.XADC_WRb               <= ipbus_in.ipb_wdata(31);
          XADC_control.XADC_addr              <= ipbus_in.ipb_wdata(22 downto 16); 
          XADC_control.XADC_data              <= ipbus_in.ipb_wdata(15 downto 0); 
          wait_for_XADC_ready                 <= '1';
        when x"14" => -- Which VFEs to access with I2C/ReSync
          VFE_control.I2C_VFE_pattern         <= ipbus_in.ipb_wdata(4 downto 0);
        when x"15" => -- Read/Write registers with I2C (I2C1_CTRL)
          if ipbus_in.ipb_wdata(30) = '1' then
            VFE_control.I2C_Reg_data(7 downto 0)  <= ipbus_in.ipb_wdata(15 downto 8); -- Send msB first then lsB
            VFE_control.I2C_Reg_data(15 downto 8) <= ipbus_in.ipb_wdata(7 downto 0);
          else
            VFE_control.I2C_Reg_data(7 downto 0)  <= ipbus_in.ipb_wdata(7 downto 0); -- Only one byte. Should be first
            VFE_control.I2C_Reg_data(15 downto 8) <= (others => '0');
          end if;
          -- VFE_control.I2C_Reg_data         <= ipbus_in.ipb_wdata(15 downto 0);
          VFE_control.I2C_Reg_number          <= ipbus_in.ipb_wdata(22 downto 16);
          VFE_control.I2C_Device_number       <= ipbus_in.ipb_wdata(29 downto 23);
          VFE_control.I2C_long_transfer       <= ipbus_in.ipb_wdata(30);
          VFE_control.I2C_R_Wb                <= ipbus_in.ipb_wdata(31);
-- Set or reset MEM mode if we   write in                reg1 of the                                    LiTE-DTU :
          if ipbus_in.ipb_wdata(31)='0' and ipbus_in.ipb_wdata(22 downto 16)="0000001" and ipbus_in.ipb_wdata(24 downto 23)="10" then
            VFE_control.DTU_MEM_mode          <= ipbus_in.ipb_wdata(5); -- b5
          end if; 
          VFE_control.I2C_access_VFE          <= '1';
          wait_for_I2C_ready                  <= '1';
        when x"1f" => -- Start LiTE-DTU ReSync transaction
          VFE_control.DTU_ReSync_data         <= ipbus_in.ipb_wdata;
          VFE_control.DTU_Start_ReSync        <= '1';
          wait_for_DTU_ready                  <= '1';
        when x"20" => -- Idle word from DTU in sync mode
          VFE_control.DTU_Sync_pattern        <= ipbus_in.ipb_wdata(31 downto 0);
        when x"21" => -- Start LiTE-DTU ReSync transaction
          VFE_control.DTU_ReSync_idle         <= ipbus_in.ipb_wdata(7 downto 0);
        when x"3f" => -- Reset CRC counters
          SAFE_control.CRC_reset              <= ipbus_in.ipb_wdata(31);
        when x"46" => -- Reset CRC counters
          SAFE_control.WTE_command_pos        <= unsigned(ipbus_in.ipb_wdata(13 downto 0));
        when x"47" => -- Reset CRC counters
          SAFE_control.ReSync_command_pos     <= unsigned(ipbus_in.ipb_wdata(13 downto 0));
        when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_ack =>
        if wait_for_XADC_ready='1' and XADC_monitor.XADC_ready='1' then
          wait_for_XADC_ready               <= '0';
        end if;
--        ReSync_busy                      := '0';
--        for i in 1 to Nb_of_VFE loop
--          ReSync_busy                    := ReSync_busy or VFE_monitor.ReSync_busy(i);
--        end loop;
        if wait_for_DTU_ready='1'  and VFE_monitor.ReSync_busy='0' then
          wait_for_DTU_ready                <= '0';
        end if;
        if wait_for_I2C_ready='1'  and VFE_monitor.I2C_VFE_busy='0' then
          wait_for_I2C_ready                <= '0';
        end if;
        if wait_for_XADC_ready='0' and wait_for_DTU_ready='0' then
          ack <= '1';
          ipbus_rw_state                    <= ipbus_finished;
        end if;
      when ipbus_finished =>
        ack <= '0';
        ipbus_rw_state <= ipbus_rw_idle;
      end case;
    end if;
  end process reg;    

  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';

-- XADC stuff
  XADC_inst : XADC
  generic map
  (
-- INIT_40 - INIT_42: XADC configuration registers
    INIT_40 => X"b000", -- No calibration average, average data on 256 samples, Unipolar, Continuous conversion, multiple channels
    INIT_41 => X"2000", -- Sequence mode, independant ADCs, No calibration, alarm On
    INIT_42 => X"0200", -- ACLK = DCLK/2 = 40MHz / 2 = 20 MHz 
-- INIT_48 - INIT_4F: Sequence Registers
    INIT_48 => X"47e0", -- CHSEL1 - VccBRAM, VccAux, VccInt, On-chip Temp, VccoDDR, VccPAux, VccPInt
    INIT_49 => XADC_VAUX_PATTERN, -- CHSEL2 - Vaux9 (GPIO3), Vaux3 (GPIO2), Vaux1(CATIA_temp), Vaux0(APD temp) : 0000 0010 0000 1011
    INIT_4A => X"47e0", -- SEQAVG1 : same as 0x48
    INIT_4B => X"0003", -- SEQAVG2 : same as 0x49
    INIT_4C => X"0000", -- SEQINMODE0 - All internal channels are unipolar
    INIT_4D => X"0000", -- SEQINMODE1 - All Vaux channels are unipolar
    INIT_4E => X"0000", -- SEQACQ0 - No extra settling time all channels
    INIT_4F => X"0000", -- SEQACQ1 - No extra settling time all channels
-- INIT_50 - INIT_58, INIT5C: Alarm Limit Registers 
    INIT_50 => X"b5ed", -- Temp upper alarm trigger 85°C
    INIT_51 => X"5999", -- Vccint upper alarm limit 1.05V
    INIT_52 => X"A147", -- Vccaux upper alarm limit 1.89V
    INIT_53 => X"dddd", -- OT upper alarm limit 125°C - see Thermal Management
    INIT_54 => X"a93a", -- Temp lower alarm reset 60°C
    INIT_55 => X"5111", -- Vccint lower alarm limit 0.95V
    INIT_56 => X"91Eb", -- Vccaux lower alarm limit 1.71V
    INIT_57 => X"ae4e", -- OT lower alarm reset 70°C - see Thermal Management
    INIT_58 => X"5999", -- VCCBRAM upper alarm limit 1.05V
    INIT_5C => X"5111", -- VCCBRAM lower alarm limit 0.95V

-- Simulation attributes: Set for proper simulation behavior
    SIM_DEVICE       => "7SERIES",    -- Select target device (values)
    SIM_MONITOR_FILE => "design.txt"  -- Analog simulation data file name
 )
 port map
 (
-- ALARMS: 8-bit (each) output: ALM, OT
    ALM          => open,             -- 8-bit output: Output alarm for temp, Vccint, Vccaux and Vccbram
    OT           => open,             -- 1-bit output: Over-Temperature alarm
-- STATUS: 1-bit (each) output: XADC status ports
    BUSY         => open,             -- 1-bit output: ADC busy output
    CHANNEL      => channel,          -- 5-bit output: Channel selection outputs
    EOC          => XADC_eoc,         -- 1-bit output: End of Conversion
    EOS          => XADC_eos,         -- 1-bit output: End of Sequence
    JTAGBUSY     => open,             -- 1-bit output: JTAG DRP transaction in progress output
    JTAGLOCKED   => open,             -- 1-bit output: JTAG requested DRP port lock
    JTAGMODIFIED => open,             -- 1-bit output: JTAG Write to the DRP has occurred
    MUXADDR      => open,          -- 5-bit output: External MUX channel decode
    
-- Auxiliary Analog-Input Pairs: 16-bit (each) input: VAUXP[15:0], VAUXN[15:0]
    VAUXN        => vauxn,            -- 16-bit input: N-side auxiliary analog input
    VAUXP        => vauxp,            -- 16-bit input: P-side auxiliary analog input
    
-- CONTROL and CLOCK: 1-bit (each) input: Reset, conversion start and clock inputs
    CONVST       => '0',              -- 1-bit input: Convert start input
    CONVSTCLK    => '0',              -- 1-bit input: Convert start input
    RESET        => '0',              -- 1-bit input: Active-high reset
    
-- Dedicated Analog Input Pair: 1-bit (each) input: VP/VN
    VN           => '0', -- 1-bit input: N-side analog input
    VP           => '0', -- 1-bit input: P-side analog input
      
-- Dynamic Reconfiguration Port (DRP)
    DO           => loc_XADC_data,
    DRDY         => XADC_monitor.XADC_ready,
    DADDR        => XADC_control.XADC_addr,
    DCLK         => XADC_clk,
    DEN          => XADC_control.XADC_access,
    DI           => XADC_control.XADC_data,
    DWE          => XADC_control.XADC_WRb
  );
  XADC_clk <= not clk;
  vauxp(0) <= APD_temp_in(1);
  vauxn(0) <= APD_temp_ref(1);
  vauxp(1) <= CATIA_temp_in(1);
  vauxn(1) <= CATIA_temp_ref(1);
  vauxp(2) <= APD_temp_in(3);
  vauxn(2) <= APD_temp_ref(3);
  vauxp(3) <= CATIA_temp_in(2);
  vauxn(3) <= CATIA_temp_ref(2);
  vauxp(4) <= APD_temp_in(4);
  vauxn(4) <= APD_temp_ref(4);
  vauxp(5) <= CATIA_temp_in(4);
  vauxn(5) <= CATIA_temp_ref(4);
  vauxp(8) <= CATIA_temp_in(3);
  vauxn(8) <= CATIA_temp_ref(3);
  vauxp(9) <= CATIA_temp_in(5);
  vauxn(9) <= CATIA_temp_ref(5);
  vauxp(10) <= APD_temp_in(2);
  vauxn(10) <= APD_temp_ref(2);
  vauxp(11) <= APD_temp_in(5);
  vauxn(11) <= APD_temp_ref(5);
  
  XADC_eos_del <= XADC_eos when rising_edge(clk) else XADC_eos_del;
  seq_counter : process (reset, clk, XADC_eos, XADC_eos_del) is
  begin  -- process reg
    if reset = '1' then
      total_seq <= (others => '0');
    elsif rising_edge(clk) then
      if XADC_eos='1' and XADC_eos_del='0' then
        total_seq <= total_seq+1;
      end if;
    end if;
  end process seq_counter;
  
  conv_counter : process (reset, clk) is
  begin
    if reset = '1' then
      total_conv <= (others => '0');
    elsif rising_edge(clk) then
      XADC_eoc_del <= XADC_eoc;
      if XADC_eoc='1' and XADC_eoc_del='0' then
        total_conv <= total_conv+1;
      end if;
    end if;
  end process conv_counter;

  access_counter : process (reset, clk) is
  begin
    if reset = '1' then
      total_access <= (others => '0');
    elsif rising_edge(clk) then
      XADC_ready_del <= XADC_monitor.XADC_ready;
      if XADC_monitor.XADC_ready='1' and XADC_ready_del='0' then
        XADC_monitor.XADC_data <= loc_XADC_data;
        total_access <= total_access+1;
      end if;
    end if;
  end process access_counter;
  XADC_monitor.XADC_addr <= XADC_control.XADC_addr;

end architecture rtl;
