library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.SAFE_IO.all;

library UNISIM;
use UNISIM.VComponents.all;

entity VFE_Input is
  port (
    IO_reset            : in  std_logic;
    delay_reset         : in  std_logic;
    tapslip_DTU_map     : in  std_logic_vector(Nb_of_Lines downto 1);
    bitslip_DTU_map     : in  std_logic_vector(Nb_of_Lines downto 1);
    byteslip_DTU_map    : in  std_logic_vector(Nb_of_Lines downto 1);
    delay_tap_dir       : in  std_logic;

    delay_locked        : out std_logic;
    VFE_synchronized    : out std_logic;
    Sync_link_idelay_OK : out std_logic_vector(Nb_of_Lines downto 1);
    Sync_link_byte_OK   : out std_logic_vector(Nb_of_Lines downto 1);
    Sync_link_bit_OK    : out std_logic_vector(Nb_of_Lines downto 1);
    Link_idelay_pos     : out idelay_pos_t(Nb_of_Lines downto 1);
    Sync_duration       : out std_logic_vector(31 downto 0);
    Sync_link_OK        : out std_logic_vector(Nb_of_Lines downto 1);
    Sync_link_error     : out std_logic_vector(Nb_of_Lines downto 1);
    Sync_link_busy      : out std_logic;

    start_IDELAY_sync   : in  std_logic_vector(Nb_of_lines downto 1);
    eLink_active        : in  std_logic_vector(Nb_of_Lines downto 1);
    DTU_MEM_mode        : in  std_logic;
    DTU_test_mode       : in  std_logic;
    DTU_SYnc_pattern    : in  std_logic_vector(31 downto 0);

    MMCM_locked         : in  std_logic;
    clk_delay_ctrl      : in  std_logic; -- 200 MHz clock for delay control
    clk_160             : in  std_logic; -- Capture clock to latch 8-bits words from streams
    clk_640             : in  std_logic; -- fast clock for bit capture of input streams (640 MHz DDR)

    ch_in_P             : in  std_logic_vector(Nb_of_Lines downto 1);
    ch_in_N             : in  std_logic_vector(Nb_of_Lines downto 1);
 
    ch_captured_stream  : out Word_t(Nb_of_Lines downto 1);
    debug               : out Word_t(31 downto 0)
  );

end entity VFE_Input;

architecture rtl of VFE_Input is

  component selectio_eLink_8 is
  port (
    data_in_from_pins_p : in  STD_LOGIC;
    data_in_from_pins_n : in  STD_LOGIC;
    data_in_to_device   : out STD_LOGIC_VECTOR(7 downto 0);
    in_delay_reset      : in  STD_LOGIC;                        -- Active high synchronous reset for input delay
    in_delay_data_ce    : in  STD_LOGIC;                        -- Enable signal for delay
    in_delay_data_inc   : in  STD_LOGIC;                        -- Delay increment (high), decrement (low) signal
    bitslip             : in  STD_LOGIC;                        -- Bitslip module is enabled in NETWORKING mode. User should tie it to '0' if not needed
    clk_in              : in  STD_LOGIC;                        -- Fast clock input from PLL/MMCM
    clk_div_in          : in  STD_LOGIC;                        -- Slow clock input from PLL/MMCM
    io_reset            : in  STD_LOGIC
  );
  end component selectio_eLink_8;

--  signal reg_add              : std_logic_vector(7 downto 0) := x"00"; 
--  signal reg_data_in          : std_logic_vector(7 downto 0) := x"00"; 
--  signal reg_data_out         : std_logic_vector(7 downto 0) := x"00"; 
  signal channels_in_N            : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal channels_in_P            : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal channels_in              : std_logic_vector(Nb_of_Lines-1 downto 0);
  signal captured_stream          : std_logic_vector(Nb_of_bits-1 downto 0);
  signal ch_captured_stream_part  : Byte_t(Nb_of_Lines downto 1);
  signal ch_captured_stream_swap  : Byte_t(Nb_of_Lines downto 1);
  signal ch_captured_stream_loc   : Word_t(Nb_of_Lines downto 1);
  signal delay_locked_loc         : std_logic := '0';
  signal tapslip_data_CE_shift    : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal tapslip_data_CE          : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal tapslip_data_CE_del1     : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal tapslip_data_CE_del2     : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_shift            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip                  : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_del1             : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal bitslip_del2             : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip                 : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip_del1            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  signal byteslip_del2            : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');
  type   cycle_pos_t is array (integer range <>) of unsigned(1 downto 0);
  signal cycle_pos_ch             : cycle_pos_t(Nb_of_Lines downto 1) := (others => (others => '0'));
  signal clk_40_pos               : unsigned(1 downto 0) := "00";
  signal synchronized             : std_logic := '0';
  signal loc_delay_reset          : std_logic := '0';
  signal loc_IO_reset             : std_logic := '0';
  signal delayctrl_reset          : std_logic := '0';
  signal DCI_locked               : std_logic := '1';
  signal ich                      : natural range Nb_of_Lines+1 downto 0      := 1;
  signal first_bad_pos            : std_logic_vector(31 downto 0) := (others => '0');
  signal n_good_taps              : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal n_good_evt               : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal n_test_evt               : UInt8_t(31 downto 0)          := (others => (others => '0'));
  signal pos_error                : std_logic_vector(31 downto 0) := (others => '0');
  signal do_bit_slip              : std_logic := '0';
  signal do_tap_shift             : std_logic := '0';
  signal start_idelay_sync_or     : std_logic := '0';
  signal start_idelay_sync_or_del : std_logic := '0';
  signal sync_channel_pattern     : std_logic_vector(nb_of_lines downto 1) := (others => '0');

  signal ref_word                 : Word_t(31 downto 0)  := (others => (others => '1'));
  signal ref_mask                 : Word_t(31 downto 0)  := (others => (others => '1'));
  signal work_stream              : Word_t(Nb_of_Lines downto 1);
  type   header_t is array (integer range <>) of std_logic_vector(3 downto 0);
  signal VFE_header1              : header_t(5 downto 1) := ("0011","0011","0011","0011","0011");
  signal VFE_header2              : header_t(5 downto 1) := ("1001","1001","1001","1001","1001");

  type   SYNC_state_t is (SYNC_idle,          SYNC_init,       SYNC_set_headers, SYNC_set_ref_words, SYNC_start,      SYNC_test_error,
                          SYNC_shift_or_slip, SYNC_shift_wait, SYNC_do_bitslip,  SYNC_do_byteslip,   SYNC_error,      SYNC_get_final_word);
  signal SYNC_state       : SYNC_state_t := SYNC_idle;
  
--  attribute IODELAY_GROUP : STRING;
--  attribute IODELAY_GROUP of inst_IDELAYCTRL:   label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch1: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch2: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch3: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch4: label is "In_delay_group";
--  attribute IODELAY_GROUP of inst_ISERDES8_ch5: label is "In_delay_group";
 
begin  -- architecture behavioral

  delayctrl_reset     <= not MMCM_locked;
  inst_IDELAYCTRL : IDELAYCTRL
  port map (
    RDY    => delay_locked_loc,   -- 1-bit output: Ready output
    REFCLK => clk_delay_ctrl,     -- 1-bit input: Reference clock input
    RST    => delayctrl_reset     -- 1-bit input: Active high reset input
  );
  delay_locked <= delay_locked_loc;


  inst_IO_reset : process(IO_reset, delay_locked_loc, clk_160) is
  variable counter : unsigned(3 downto 0) := (others => '0');
  begin
    if IO_reset='1' or delay_locked_loc='0' then
      loc_IO_reset   <= '1';
      counter        := (others => '0');
    elsif rising_edge(clk_160) then
      if counter = x"f" then
        loc_IO_reset <= '0';
      else
        counter      := counter + 1;
      end if;
    end if;
  end process inst_IO_reset;

  inst_delay_reset : process(delay_reset, delay_locked_loc, clk_160) is
  variable counter : unsigned(3 downto 0) := (others => '0');
  begin
    if delay_reset='1' or delay_locked_loc='0' then
      loc_delay_reset   <= '1';
      counter        := (others => '0');
    elsif rising_edge(clk_160) then
      if counter = x"7" then
        loc_delay_reset <= '0';
      else
        counter      := counter + 1;
      end if;
    end if;
  end process inst_delay_reset;

  SERDES : for i in 1 to Nb_of_Lines generate
  begin
    inst_ISERDES8_ch : selectio_eLink_8
    port map(
      data_in_from_pins_p => ch_in_P(i),
      data_in_from_pins_n => ch_in_N(i),
      data_in_to_device   => ch_captured_stream_part(i),
      in_delay_reset      => loc_delay_reset,
      in_delay_data_ce    => tapslip_data_CE_shift(i),
      in_delay_data_inc   => delay_tap_dir,
      bitslip             => bitslip_shift(i),
      clk_in              => clk_640,
      clk_div_in          => clk_160,
      io_reset            => loc_IO_reset
    );
  end generate;

  VFE_synchronized            <= synchronized;
  Swap : for i in 1 to Nb_of_Lines generate
  begin
    ch_captured_stream_swap(i)(0) <= ch_captured_stream_part(i)(7);
    ch_captured_stream_swap(i)(1) <= ch_captured_stream_part(i)(6);
    ch_captured_stream_swap(i)(2) <= ch_captured_stream_part(i)(5);
    ch_captured_stream_swap(i)(3) <= ch_captured_stream_part(i)(4);
    ch_captured_stream_swap(i)(4) <= ch_captured_stream_part(i)(3);
    ch_captured_stream_swap(i)(5) <= ch_captured_stream_part(i)(2);
    ch_captured_stream_swap(i)(6) <= ch_captured_stream_part(i)(1);
    ch_captured_stream_swap(i)(7) <= ch_captured_stream_part(i)(0);
  end generate;

-- Output signals to device
  ISERDES_cycle : process (loc_delay_reset, clk_160) is
  begin
    if loc_delay_reset = '1' then
      work_stream                                 <= (others => (others => '0'));
      cycle_pos_ch                                <= (others => (others => '0'));
      clk_40_pos                                  <= "00";
      synchronized                                <= '0';
    elsif rising_edge(clk_160) then
      clk_40_pos                                  <= clk_40_pos+1;
      for i in 1 to Nb_of_Lines loop
        if cycle_pos_ch(i) = "00" then
          ch_captured_stream_loc(i)(31 downto 24) <= ch_captured_stream_swap(i);
        elsif cycle_pos_ch(i) = "01" then
          ch_captured_stream_loc(i)(23 downto 16) <= ch_captured_stream_swap(i);
        elsif cycle_pos_ch(i) = "10" then
          ch_captured_stream_loc(i)(15 downto 8)  <= ch_captured_stream_swap(i);
        elsif cycle_pos_ch(i) = "11" then
          work_stream(i)(7 downto 0)              <= ch_captured_stream_swap(i);
          work_stream(i)(31 downto 8)             <= ch_captured_stream_loc(i)(31 downto 8);
        end if;
      
        ch_captured_stream(i)                     <= work_stream(i);
      end loop;

      if synchronized='0' and clk_40_pos="00" then
        synchronized                              <= '1';
        cycle_pos_ch                              <= (others => "01");
      elsif synchronized='1' then
        for i in 1 to Nb_of_Lines loop
          if byteslip(i)='1' then
            cycle_pos_ch(i)                       <= cycle_pos_ch(i) + 2;
          else
            cycle_pos_ch(i)                       <= cycle_pos_ch(i) + 1;
          end if;
        end loop;
      else
        cycle_pos_ch                              <= (others => "00");
      end if;
    end if;
  end process ISERDES_cycle;

--  ch1_captured_stream <= "11101010101010101010101010101010";
--  ch2_captured_stream <= "11101010101010101010101010101010";
--  ch3_captured_stream <= "11101010101010101010101010101010";
--  ch4_captured_stream <= "11101010101010101010101010101010";
--  ch5_captured_stream <= "11101010101010101010101010101010";

--  Synchronise signals on clock falling edge for iserdes :
  tapslip_data_CE_shift <= tapslip_data_CE when falling_edge(clk_160) else tapslip_data_CE_shift;
  bitslip_shift         <= bitslip         when falling_edge(clk_160) else bitslip_shift;

  optimize_delay : process(loc_delay_reset, start_IDELAY_sync, clk_160)
    variable pos             : unsigned(4 downto 0)          := (others => '0');
    variable n_byteslip      : unsigned(3 downto 0)          := (others => '0');
    variable n_bitslip       : unsigned(3 downto 0)          := (others => '0');
    variable duration        : unsigned(31 downto 0)         := (others => '0');
    variable skip_clock      : unsigned(1 downto 0)          := (others => '0');             -- Skip 1 full 40 MHz clock afetr tap/byte or bit slip
  begin
    if loc_delay_reset= '1' then
      tapslip_data_CE                          <= (others => '0');
      bitslip                                  <= (others => '0');
      byteslip                                 <= (others => '0');
      Sync_link_idelay_OK                      <= (others => '0');
      Sync_link_byte_OK                        <= (others => '0');
      Sync_link_bit_OK                         <= (others => '0');
      Sync_link_OK                             <= (others => '0');
      Link_idelay_pos                          <= (others => (others => '0'));
      SYNC_state                               <= SYNC_idle;
      duration                                 := (others => '0');
      pos_error                                <= (others => '0');
      ref_word                                 <= (others => (others => '0'));
      pos                                      := (others => '0');
      skip_clock                               := (others => '0');
      ich                                      <= 1;
      Sync_link_error                          <= (others => '1');
      Sync_link_busy                           <= '0';
    elsif Rising_Edge(clk_160) then
      tapslip_data_CE                          <= (others => '0');
      byteslip                                 <= (others => '0');
      bitslip                                  <= (others => '0');
      tapslip_data_CE_del1                     <= tapslip_DTU_map;
      tapslip_data_CE_del2                     <= tapslip_data_CE_del1;
      bitslip_del1                             <= bitslip_DTU_map;
      bitslip_del2                             <= bitslip_del1;
      byteslip_del1                            <= byteslip_DTU_map;
      byteslip_del2                            <= byteslip_del1;
      start_idelay_sync_or                     <= or start_idelay_sync;
      start_idelay_sync_or_del                 <= start_idelay_sync_or;
--      for i in 31 downto 2 loop
--        debug(i) <= debug(i-1);
--      end loop;
      
      if sync_link_busy = '1' then 
        duration                               := duration+1;
      end if;
      case SYNC_state is
      when SYNC_idle =>
--        debug(1) <= debug(1)(27 downto 0)&x"1";
-- Take into account requests from IPbus for tap changing, bitslip and byteslip:
        for i in 1 to Nb_of_Lines loop
          if tapslip_data_CE_del1(i)='1' and tapslip_data_CE_del2(i)='0' then
            tapslip_data_CE(i)                 <= '1';
          end if;
          if bitslip_del1(i)='1' and bitslip_del2(i)='0' then
            bitslip(i)                         <= '1';
          end if;
          if byteslip_del1(i)='1' and byteslip_del2(i)='0' then
            byteslip(i)                        <= '1';
          end if;
        end loop;

-- Start idelay optimization if the link is not yet synchronized
-- Be carefull : in test mode, we have 4 links but only one ADC.
-- The delayctrl clock is 200 MHz, which gives 78 ps per tap
-- With 1280 MHz clock, we have 781 ps per bit, thus 10 taps
-- We will select a tap after 4 identical ref bytes and no reading error on 255 samples
        sync_link_busy                         <= '0';
        if start_idelay_sync_or = '1' and start_idelay_sync_or_del = '0' then
          ich                                    <= 1;
          SYNC_state                             <= SYNC_init;
          sync_channel_pattern                   <= start_idelay_sync;
          sync_link_busy                         <= '1';
        end if; 
      when SYNC_init =>
--        debug(0) <= std_logic_vector(to_unsigned(ich,32));
--        debug(1) <= debug(1)(27 downto 0)&x"2";
        if ich <= nb_of_lines then
          if sync_channel_pattern(ich) = '1' and eLink_active(ich) = '1' and sync_link_OK(ich) = '0' then 
            SYNC_state                           <= SYNC_set_headers;
            pos                                  := (others => '0');
            pos_error                            <= (others => '0');
            skip_clock                           := "10";
            ref_word                             <= (others => (others => '0'));
            ref_mask                             <= (others => (others => '0'));
            duration                             := (others => '0');
          else
            if eLink_active(ich) = '0' then -- If link not active, put it as synchronized
              Sync_link_idelay_OK(ich)           <= '0';
              Sync_link_byte_OK(ich)             <= '0';
              Sync_link_bit_OK(ich)              <= '0';
              Sync_link_OK(ich)                  <= '0';
            end if;
            ich                                  <= ich+1;
          end if;
        else
          SYNC_state                             <= SYNC_idle;
          Sync_duration                          <= std_logic_vector(duration);
        end if;
      when SYNC_set_headers =>
--        debug(1) <= debug(1)(27 downto 0)&x"3";
        if DTU_test_mode = '0' then
          ref_word(0)                          <= DTU_Sync_pattern and x"F000F000";
        else
          ref_word(0)                          <= VFE_header1(ich)(3 downto 0)&x"000"&VFE_header2(ich)(3 downto 0)&x"000";
        end if;
        ref_mask(0)                            <= x"F000F000";
        first_bad_pos(0)                       <= '0';
        n_good_taps(0)                         <= (others =>'0');
        n_good_evt(0)                          <= (others => '0');
        n_test_evt(0)                          <= (others => '0');
        pos_error(0)                           <= '1';
        SYNC_state                             <= SYNC_set_ref_words;
      when SYNC_set_ref_words =>
--        debug(1) <= debug(1)(27 downto 0)&x"4";
        for i in 1 to 31 loop
          ref_word(i)                          <= ref_word(0) rol i;
          ref_mask(i)                          <= ref_mask(0) rol i;
          first_bad_pos(i)                     <= '0';
          n_good_taps(i)                       <= (others => '0');
          n_good_evt(i)                        <= (others => '0');
          n_test_evt(i)                        <= (others => '0');
        end loop;
        SYNC_state                             <= SYNC_start;
      when SYNC_start =>
--        debug(1) <= debug(1)(27 downto 0)&x"5";
-- New data is aligned with the 40 MHz clock (4 bytes @ 160 MHz)
-- At each new data compare headers with reference ones and increment error counter
-- Do it 16 times, then shift idelay by one tap. We assume that if we have no error for 16 clocks, the tap is good.
        if clk_40_pos="00" then
          if skip_clock /= 0 then
            skip_clock                         := skip_clock -1;
          else
            for i in 0 to 31 loop
              if (work_stream(ich) and ref_mask(i)) /= ref_word(i) then
                first_bad_pos(i)               <= '1';
                n_good_taps(i)                 <= (others =>'0');
                n_good_evt(i)                  <= (others =>'0');
                pos_error(i)                   <= '1';
              elsif first_bad_pos(i) = '1' then
                if n_good_evt(i) = 0 then
                  n_good_taps(i)               <= n_good_taps(i) + 1;
                end if;
                n_good_evt(i)                  <= n_good_evt(i) + 1;
                pos_error(i)                   <= '0';
              else
                pos_error(i)                   <= '1';
              end if;
            end loop;
            SYNC_state                         <= SYNC_test_error;        -- test if we are at the right position or not, then try another tap
          end if;
        end if;
      when SYNC_test_error =>
--        debug(1) <= debug(1)(27 downto 0)&x"6";
-- If we had an error tap, memorize position and if we had 2 error position found, stop the loop
        if pos="11111" then                                            -- Don't loop for ever. If we are here it is certainly due to lack of data
          SYNC_state                           <= SYNC_error;
        else
          SYNC_state                           <= SYNC_shift_or_slip;
          do_tap_shift                         <= '1';
          do_bit_slip                          <= '0';
          for i in 0 to 31 loop
            if pos_error(i) = '0' and n_good_taps(i) >= 4 then
              if n_good_evt(i) = x"FF" then
                do_bit_slip                    <= '1';
                do_tap_shift                   <= '0';
                link_idelay_pos(ich)           <= std_logic_vector(pos);
                n_bitslip                      := (others => '0');
                n_byteslip                     := (others => '0');
              elsif n_test_evt(i) < x"FF" then
                n_test_evt(i)                  <= n_test_evt(i)+1;
                skip_clock                     := "00";
                do_bit_slip                    <= '0';
                do_tap_shift                   <= '0';
              end if;
            else
              n_good_evt(i)                    <= (others => '0');
              n_test_evt(i)                    <= (others => '0');
            end if;
          end loop;
        end if;
      when SYNC_shift_or_slip  =>                                       -- Decide if we a tap shift or go to bit slip or do nothing (loop on events with same setting)
--        debug(1) <= debug(1)(27 downto 0)&x"7";
        if do_bit_slip = '1' then
          Sync_link_idelay_OK(ich)             <= '1';
          SYNC_state                           <= SYNC_do_bitslip;
        elsif do_tap_shift = '0' then
          skip_clock                           := "00";
          SYNC_state                           <= SYNC_start;
        else
          tapslip_data_CE(ich)                 <= '1';                  -- Error position : shift by one tap 
          SYNC_state                           <= SYNC_shift_wait;
        end if;
      when SYNC_shift_wait  =>                                          -- Wait for IDELAYCTRL to get ready
--        debug(1) <= debug(1)(27 downto 0)&x"8";
        if delay_locked_loc = '1' then
          pos_error                            <= (others => '0');
          pos                                  := pos+1;
          skip_clock                           := "10";
          SYNC_state                           <= SYNC_start;           -- We restart test with a new delay setting
        end if;
      when SYNC_do_bitslip =>                                          -- Slip bits to get the header at Byte edge
--        debug(1) <= debug(1)(27 downto 0)&x"9";
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          elsif ((work_stream(ich) and ref_mask(0))  = ref_word(0)) or
                ((work_stream(ich) and ref_mask(8))  = ref_word(8)) or
                ((work_stream(ich) and ref_mask(16)) = ref_word(16)) or
                ((work_stream(ich) and ref_mask(24)) = ref_word(24)) then
            Sync_link_bit_OK(ich)              <= '1';
            SYNC_state                         <= SYNC_do_byteslip;
            skip_clock                         := "10";
          else
            bitslip(ich)                       <= '1';
            n_bitslip                          := n_bitslip+1;
            skip_clock                         := "10";
          end if;
          if n_bitslip = 15 then                                        -- Don't loop forever
            SYNC_state                         <= SYNC_error;
          end if;
        end if;
      when SYNC_do_byteslip =>                                         -- Slip bytes to get header synchronous with 40 MHz
--        debug(1) <= debug(1)(27 downto 0)&x"a";
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          elsif ((work_stream(ich) and ref_mask(0))  = ref_word(0)) then
            Sync_link_byte_OK(ich)             <= '1';
            skip_clock                         := "10";
            SYNC_state                         <= SYNC_get_final_word;
            Sync_link_error(ich)               <= '0';
          else
            byteslip(ich)                      <= '1';
            n_byteslip                         := n_byteslip+1;
            skip_clock                         := "10";
          end if;
          if n_byteslip = 15 then                                        -- Don't loop forever
            SYNC_state                         <= SYNC_error;
          end if;
          Sync_duration                        <= std_logic_vector(duration);
        end if;
      when SYNC_get_final_word =>
--        debug(1) <= debug(1)(27 downto 0)&x"b";
        if clk_40_pos="00" then
          if skip_clock/=0 then
            skip_clock                         := skip_clock-1;
          else
            ich                                <= ich+1;
            SYNC_state                         <= SYNC_init;
          end if;
        end if;
      when SYNC_error =>                                               -- Didn't succeed to tune Idelay.
--        debug(1) <= debug(1)(27 downto 0)&x"c";
        Sync_link_error(ich)                   <= '1';
        Sync_Link_idelay_OK(ich)               <= '0';                 -- Stay in this state until reset
        Link_idelay_pos(ich)                   <= (others => '1');
        Sync_duration                          <= std_logic_vector(duration);
        ich                                    <= ich+1; 
        SYNC_state                             <= SYNC_init;
      end case;
    end if;
  end process optimize_delay;

end architecture rtl;