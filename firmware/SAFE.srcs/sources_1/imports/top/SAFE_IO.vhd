----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
--
-- package for SAFE control intefrace to IPBUS
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package SAFE_IO is

-- General setup for different SAFE flavors :
  constant USE_LVRB         : boolean := true;
  constant USE_MEM          : boolean := false;
  constant USE_LMB          : boolean := false;
  constant Nb_of_VFE        : natural := 5;              -- Normally, we have 5 VFE board connected to a SAFE board
  constant Nb_of_Lines      : natural := Nb_of_VFE*5;    -- So, 25 data streams
  constant Nb_of_Bits       : natural := Nb_of_Lines*32; -- And 32 bits per word in these streams (aligned with 40 MHz clock)
  constant N_I2C_spy_bits   : Natural := 192;

  -- XADC measurements :
  -- CATIA Temps on Vaux 9, 5, 8, 3, 1
  -- APD   Temps on Vaux 11,4, 2,10, 0
  constant XADC_VAUX_PATTERN  : bit_vector(15 downto 0) := x"0F3F";

  type UInt32_t is array (integer range <>) of unsigned(31 downto 0);
  type UInt16_t is array (integer range <>) of unsigned(15 downto 0);
  type UInt8_t  is array (integer range <>) of unsigned(7 downto 0);
  type Int32_t  is array (integer range <>) of signed(31 downto 0);
  type Int16_t  is array (integer range <>) of signed(15 downto 0);
  type Int8_t   is array (integer range <>) of signed(7 downto 0);
  type Byte_t   is array (integer range <>) of std_logic_vector(7 downto 0);
  type Short_t  is array (integer range <>) of std_logic_vector(15 downto 0);
  type Word_t   is array (integer range <>) of std_logic_vector(31 downto 0);
  type Word192_t is array (integer range <>) of std_logic_vector(N_I2C_spy_bits-1 downto 0);

  type Idelay_pos_t is array (integer range <>) of std_logic_vector(4 downto 0);
  type SAFE_Monitor_t is record
    resync_clock_phase   : std_logic_vector(5 downto 1);
    reset                : std_logic;
    FIFO_mode            : std_logic;                    -- 0: single event mode, 1: FIFO mode
    trig_loop            : std_logic;                    -- 0: external trigger, 1: internal trigger loop
    clock_reset          : std_logic;
    clock_locked         : std_logic;
    LED_on               : std_logic;
    firmware_ver         : std_logic_vector(31 downto 0);
    board_SN             : std_logic_vector(3  downto 0);
    trig_self_mode       : std_logic;                    -- Absolute trigger level (0) or delta between 2 samples  (1)
    trig_self            : std_logic;
    trig_self_mask       : std_logic_vector(Nb_of_Lines downto 1); -- signal threshold to generate self trigger
    trig_self_thres      : std_logic_vector(12 downto 0); -- signal threshold to generate self trigger
    trigger_HW_delay     : std_logic_vector(15 downto 0); -- delay between received HW trigger and the capture start (x 160 MHz)
    trigger_SW_delay     : std_logic_vector(15 downto 0); -- delay between generated SW trigger and the capture start (x 160 MHz)
    TP_duration          : std_logic_vector(15 downto 0); -- duration of the calibration trigger pulse
    TP_delay             : std_logic_vector(15 downto 0); -- delay between icalibration trigger and DAQ start
    TP_dummyb            : std_logic;                     -- Always send Iinj to TIA, not TIA dummy
    delay_locked         : std_logic;                     -- DELAYCTRL lock signals
    FE_synchronized      : std_logic;
    VFE_synchronized     : std_logic;
    sync_link_busy       : std_logic;                     -- Active during link synchronization
    sync_link_idelay_OK  : std_logic_vector(Nb_of_Lines downto 1);   -- IDELAY setting in the middle of the eye has been done 
    link_idelay_pos      : idelay_pos_t(Nb_of_Lines downto 1);       -- Final Idelay tap position found during autoconfig
    sync_link_byte_OK    : std_logic_vector(Nb_of_Lines downto 1);   -- byte alignement to get header sync with 40 MHz clock is done
    sync_link_bit_OK     : std_logic_vector(Nb_of_Lines downto 1);   -- bit alignement to get header at byte start is done
    sync_link_error      : std_logic_vector(Nb_of_Lines downto 1);   -- Failed to synchronize links
    sync_link_OK         : std_logic_vector(Nb_of_Lines downto 1);   -- Failed to synchronize links
    sync_duration        : std_logic_vector(31 downto 0);
    start_idelay_sync    : std_logic_vector(Nb_of_Lines downto 1);  -- Launch Idelay tuning to sample in the eyes of ADC link 
    DCI_locked           : std_logic;
    WTE_command_pos      : unsigned(13 downto 0);
    ReSync_command_pos   : unsigned(13 downto 0);
    CRC_error            : UInt32_t(Nb_of_Lines downto 1);           -- CRC error counter for each channel
    debug                : word_t(31 downto 0);                      -- Debug words
  end record SAFE_Monitor_t;

  type SAFE_Control_t is record
    resync_clock_phase   : std_logic_vector(5 downto 1);
    clock_reset          : std_logic;
    CRC_reset            : std_logic;
    reset                : std_logic;
    FIFO_mode            : std_logic;
    trig_loop            : std_logic;
    trigger              : std_logic;
    AWG_trigger          : std_logic;
    gen_BC0              : std_logic;
    gen_WTE              : std_logic;
    WTE_command_pos      : unsigned(13 downto 0);
    ReSync_command_pos   : unsigned(13 downto 0);
    LED_on               : std_logic;
    trig_self            : std_logic;
    trig_self_mode       : std_logic;                    -- Absolute trigger level (0) or delta between 2 sampes (1)
    trig_self_mask       : std_logic_vector(Nb_of_Lines downto 1);
    trig_self_thres      : std_logic_vector(12 downto 0);
    trigger_HW_delay     : std_logic_vector(15 downto 0); -- delay between received HW trigger and the capture start (x 160 MHz)
    trigger_SW_delay     : std_logic_vector(15 downto 0); -- delay between generated SW trigger and the capture start (x 160 MHz)
    TP_trigger           : std_logic;
    TP_duration          : std_logic_vector(15 downto 0); -- duration of the calibration trigger pulse
    TP_delay             : std_logic_vector(15 downto 0); -- delay between icalibration trigger and DAQ start
    TP_dummyb            : std_logic;                     -- Always send Inj current to TIA and not TIA_dummy
    delay_tap_dir        : std_logic;
    IO_reset             : std_logic;                     -- Reset delay to minimal value on ADC_number and reset iserdes
    delay_reset          : std_logic;                     -- Reset delay to minimal value on ADC_number and reset iserdes
    tapslip_DTU_map      : std_logic_vector(Nb_of_Lines downto 1);  -- ADC number on which we want to tune the delay
    bitslip_DTU_map      : std_logic_vector(Nb_of_Lines downto 1);  -- ADC number on which we want to slip input bits
    byteslip_DTU_map     : std_logic_vector(Nb_of_Lines downto 1);  -- ADC number on which we want to slip bytes
    start_idelay_sync    : std_logic_vector(Nb_of_Lines downto 1);  -- Launch Idelay tuning to sample in the eyes of ADC link 
  end record SAFE_Control_t;
  constant DEFAULT_SAFE_Control : SAFE_Control_t := (
                                                     resync_clock_phase  => (others => '0'),
                                                     clock_reset         => '0',
                                                     CRC_reset           => '0',
                                                     reset               => '0',
                                                     FIFO_mode           => '1',
                                                     trig_self_mode      => '0',
                                                     trig_self           => '1',
                                                     trig_loop           => '0',
                                                     trigger             => '0',
                                                     AWG_trigger         => '0',
                                                     gen_BC0             => '1',
                                                     gen_WTE             => '0',
                                                     WTE_command_pos     => "11"&x"16f", -- 10 us before BC0
                                                     ReSync_command_pos  => "11"&x"70f", --  1 us before BC0
                                                     LED_on              => '1',
                                                     trig_self_mask      => (others => '0'),
                                                     trig_self_thres     => (others => '1'),
                                                     trigger_HW_delay    => x"0000",
                                                     trigger_SW_delay    => x"0000",
                                                     TP_trigger          => '0',
                                                     TP_duration         => x"00FF",
                                                     TP_delay            => x"0000",
                                                     TP_dummyb           => '1',
                                                     delay_tap_dir       => '1',
                                                     tapslip_DTU_map     => (others => '0'),
                                                     bitslip_DTU_map     => (others => '0'),
                                                     byteslip_DTU_map    => (others => '0'),
                                                     IO_reset            => '0',
                                                     delay_reset         => '0',
                                                     start_idelay_sync   => (others => '0')
                                                     );

  type VFE_Monitor_t is record
    I2C_Reg_data_VFE         : Short_t(Nb_of_VFE downto 1);                 -- Data read from register
    I2C_VFE_pattern          : std_logic_vector(Nb_of_VFE downto 1);        -- VFE number to access
    I2C_Reg_number           : std_logic_vector(6 downto 0);                -- Register number accessed
    I2C_Device_number        : std_logic_vector(6 downto 0);                -- Register number accessed
    I2C_access_VFE           : std_logic;                                   -- Access VFE chips registers through I2C bus
    I2C_VFE_error            : std_logic;                                   -- Error during I2C access to CATIA (missing ack)
    I2C_lpGBT_mode           : std_logic;                                   -- Mimick lpGBT bug in READ transactions
    I2C_n_ack_VFE            : unsigned(7 downto 0);                        -- Number of I2C acknowledge received during last transaction
    I2C_VFE_busy             : std_logic;                                   -- VFE I2C is running
    ReSync_busy              : std_logic;                                   -- ReSync broadcast in progress
    I2C_R_Wb                 : std_logic;                                   -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                                   -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    I2C_ack_spy              : std_logic_vector(N_I2C_spy_bits-1 downto 0); -- Spy register of I2C protocol for CATIA1
    DTU_test_mode            : std_logic;                                   -- Put DTU in test_mode : all data of both ADC are read out on 4 elinks
    DTU_MEM_mode             : std_logic;                                   -- Put DTU in MEM_mode : 80 MHz mode of LiTE-DTU
    DTU_PLL_lock             : std_logic_vector(Nb_of_lines downto 1);      -- DTU PLL is locked (1) or not (0)
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);               -- DTU word transmitted in sync mode (V2.0)
    DTU_ReSync_data          : std_logic_vector(31 downto 0);               -- Latest resync code used
    DTU_ReSync_idle          : std_logic_vector(7 downto 0);                -- resync idle patttern
    eLink_Active             : std_logic_vector(Nb_of_Lines downto 1);      -- Bit pattern for elinks actually connected to SAFE board
    I2C_reset                : std_logic;
    VFE_reset                : std_logic;
    VFE_LV                   : std_logic_vector(6 downto 1);                -- Control DCDC converters on LVRB
  end record VFE_Monitor_t;
  type VFE_Control_t is record
    I2C_Reg_data             : std_logic_vector(15 downto 0);          -- Data to read/write in register
    I2C_VFE_pattern          : std_logic_vector(Nb_of_VFE downto 1);   -- VFE number to access
    I2C_Reg_number           : std_logic_vector(6 downto 0);           -- Register number to access
    I2C_Device_number        : std_logic_vector(6 downto 0);           -- Register number to access
    I2C_access_VFE           : std_logic;                              -- Access to VFE chips (CATIA, LiTE-DTU) registers through I2C bus
    I2C_lpGBT_mode           : std_logic;                              -- Mimick lpGBT bug in READ transactions
    I2C_R_Wb                 : std_logic;                              -- Write (0) to VFE register or Read (1) VFE registers
    I2C_long_transfer        : std_logic;                              -- 1 byte (0) or 2 bytes (1) I2C/SPI transaction
    DTU_test_mode            : std_logic;                              -- Put ADC in test_mode : all data of both ADC are read out on 4 elinks
    DTU_MEM_mode             : std_logic;                              -- ADC run in MEM_mode : 80MHz without compression (b5 of reg1 of LiTE-DTU)
    DTU_ReSync_data          : std_logic_vector(31 downto 0);          -- Resync code to be sent to LiTE-DTU after Hamming encoding
    DTU_Sync_pattern         : std_logic_vector(31 downto 0);          -- DTU word transmitted in sync mode (V2.0)
    DTU_ReSync_idle          : std_logic_vector(7 downto 0);           -- Resync idle patterm
    DTU_start_ReSync         : std_logic;                              -- Start ReSync transaction
    eLink_Active             : std_logic_vector(Nb_of_LInes downto 1); -- Bit pattern for elinks actually connected to SAFE board
    I2C_reset                : std_logic;
    VFE_reset                : std_logic;
    VFE_LV                   : std_logic_vector(6 downto 1);   -- Control DCDC converters on LVRB
  end record VFE_Control_t;
  constant DEFAULT_VFE_Control : VFE_Control_t := (
                                                   I2C_Reg_data        => (others => '0'),
                                                   I2C_VFE_pattern     => (others => '1'),
                                                   I2C_Reg_Number      => (others => '0'),
                                                   I2C_Device_Number   => (others => '0'),
                                                   I2C_access_VFE      => '0',
                                                   I2C_long_transfer   => '0',
                                                   I2C_R_Wb            => '0',
                                                   I2C_lpGBT_mode      => '0',
                                                   DTU_test_mode       => '1',
                                                   DTU_MEM_mode        => '0',
                                                   DTU_ReSync_data     => (others => '0'),
                                                   DTU_ReSync_idle     => x"66",
                                                   DTU_Sync_pattern    => x"eaaaaaaa",
                                                   DTU_start_ReSync    => '0',
                                                   eLink_Active        => (others => '0'),
                                                   I2C_reset           => '0',
                                                   VFE_reset           => '0',
                                                   VFE_LV             => (others =>'0')
                                                   );
  type XADC_Monitor_t is record
    XADC_ready           : std_logic;
    XADC_addr            : std_logic_vector(6 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Monitor_t;
  type XADC_Control_t is record
    XADC_access          : std_logic;
    XADC_WRb             : std_logic;
    XADC_addr            : std_logic_vector(6 downto 0);
    XADC_data            : std_logic_vector(15 downto 0);
  end record XADC_Control_t;
  constant DEFAULT_XADC_Control : XADC_Control_t := (
                                                     XADC_access         => '0',
                                                     XADC_WRb            => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));
  constant DEFAULT_XADC_Monitor : XADC_Monitor_t := (
                                                     XADC_ready          => '0',
                                                     XADC_addr           => (others => '0'),
                                                     XADC_data           => (others => '0'));

  constant DTU_ReSync_code : Byte_t(15 downto 0) := (x"7F",x"78",x"66",x"61",x"55",x"52",x"4C",x"4B",
                                                     x"34",x"33",x"2D",x"2A",x"1E",x"19",x"07",x"00");

end SAFE_IO;
